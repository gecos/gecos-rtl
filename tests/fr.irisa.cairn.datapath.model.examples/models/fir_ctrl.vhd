
library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all; 
use ieee.numeric_std.all; 
 
entity fir_ctrl is port (
	
	-- global signals 
	clk : in std_logic;
	rst : in std_logic;
	
	-- Input ports
	endLoop : in std_logic;
	inEmpty : in std_logic;
	outFull : in std_logic;
	
	-- output ports	
	fifo_rd : out std_logic ;
				
	xwr_en : out std_logic ;
				
	xrd_en : out std_logic ;
				
	adx_en : out std_logic ;
				
	crd_en : out std_logic ;
				
	cpt_en : out std_logic ;
				
	cpt_init : out std_logic ;
				
	mul_en : out std_logic ;
				
	acc_en : out std_logic ;
				
	fifo_wr : out std_logic 
			
 
  );
end fir_ctrl ;

architecture RTL of fir_ctrl is

	type fsm_fir_ctrl_type is (
   		idle,
   		waitNotEmpty,
   		readFIFO,
   		step1,
   		step2,
   		steady,
   		flush1,
   		flush2,
   		waitsState1,
   		writeFIFO
	);
   	
	signal NS,CS : fsm_fir_ctrl_type;

begin

	process(CS, endLoop,  inEmpty,  outFull)
	begin
	
			fifo_rd <= '0'; 
			
			xwr_en <= '0'; 
			
			xrd_en <= '0'; 
			
			adx_en <= '0'; 
			
			crd_en <= '0'; 
			
			cpt_en <= '0'; 
			
			cpt_init <= '0'; 
			
			mul_en <= '0'; 
			
			acc_en <= '0'; 
			
			fifo_wr <= '0'; 
					
 			NS <= idle;

			case CS is
		 
			-------------------------
			-- State idle
			-------------------------
			when idle =>
				-- State transitions
				-- Default ? NS <= nidle;
	
					-- Predicate 1
			
						NS <= waitNotEmpty;
					
				-- Output commands
 
			-------------------------
			-- State waitNotEmpty
			-------------------------
			when waitNotEmpty =>
				-- State transitions
				-- Default ? NS <= nwaitNotEmpty;
	
					-- Predicate !inEmpty
			
					if (inEmpty='0') then 
						NS <= readFIFO;
					end if; 
					-- Predicate !(!(inEmpty))
			
					if not((inEmpty='0')) then 
						NS <= waitNotEmpty;
					end if; 
				-- Output commands
 
			-------------------------
			-- State readFIFO
			-------------------------
			when readFIFO =>
				-- State transitions
				-- Default ? NS <= nreadFIFO;
	
					-- Predicate 1
			
						NS <= step1;
					
				-- Output commands
		xwr_en <= '1';		fifo_rd <= '1'; 
			-------------------------
			-- State step1
			-------------------------
			when step1 =>
				-- State transitions
				-- Default ? NS <= nstep1;
	
					-- Predicate 1
			
						NS <= step2;
					
				-- Output commands
		crd_en <= '1';		cpt_en <= '1'; 
			-------------------------
			-- State step2
			-------------------------
			when step2 =>
				-- State transitions
				-- Default ? NS <= nstep2;
	
					-- Predicate 1
			
						NS <= steady;
					
				-- Output commands
		mul_en <= '1';		xrd_en <= '1';		crd_en <= '1';		cpt_en <= '1'; 
			-------------------------
			-- State steady
			-------------------------
			when steady =>
				-- State transitions
				-- Default ? NS <= nsteady;
	
					-- Predicate endLoop
			
					if (endLoop='1') then 
						NS <= flush1;
					end if; 
					-- Predicate !(endLoop)
			
					if not((endLoop='1')) then 
						NS <= steady;
					end if; 
				-- Output commands
		acc_en <= '1';		mul_en <= '1';		xrd_en <= '1';		crd_en <= '1';		cpt_en <= '1'; 
			-------------------------
			-- State flush1
			-------------------------
			when flush1 =>
				-- State transitions
				-- Default ? NS <= nflush1;
	
					-- Predicate 1
			
						NS <= flush2;
					
				-- Output commands
		acc_en <= '1';		mul_en <= '1'; 
			-------------------------
			-- State flush2
			-------------------------
			when flush2 =>
				-- State transitions
				-- Default ? NS <= nflush2;
	
					-- Predicate 1
			
						NS <= waitsState1;
					
				-- Output commands
		acc_en <= '1';		mul_en <= '1'; 
			-------------------------
			-- State waitsState1
			-------------------------
			when waitsState1 =>
				-- State transitions
				-- Default ? NS <= nwaitsState1;
	
					-- Predicate !outFull
			
					if (outFull='0') then 
						NS <= writeFIFO;
					end if; 
					-- Predicate !(!(outFull))
			
					if not((outFull='0')) then 
						NS <= waitsState1;
					end if; 
				-- Output commands
 
			-------------------------
			-- State writeFIFO
			-------------------------
			when writeFIFO =>
				-- State transitions
				-- Default ? NS <= nwriteFIFO;
	
					-- Predicate 1
			
						NS <= idle;
					
				-- Output commands
		fifo_wr <= '1'; 
			when others =>
				NS <= idle;
			end case;
	end process;

	process(clk,rst)
	begin
		if rising_edge(clk) then
			if rst='1' then
				CS <= idle;
			else
				CS <= NS;
			end if;
		end if;
	end process;

end RTL;
