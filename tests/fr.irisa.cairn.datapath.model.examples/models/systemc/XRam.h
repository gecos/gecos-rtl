// Function(header) : Synchronous read/write RAM

#ifndef XRAM
#define XRAM
#include "systemc.h"



#define DATA_WIDTH        8 
#define ADDR_WIDTH        8 
#define RAM_DEPTH         1 << ADDR_WIDTH



SC_MODULE (XRam) {
  sc_in<sc_lv<ADDR_WIDTH> > 		address ;
  sc_in<sc_lv<1> > 				  	re ;
  sc_in<sc_lv<1> > 				  	we ;
  sc_in<sc_lv<DATA_WIDTH> > 	  	dataIn ;
  sc_out<sc_lv<DATA_WIDTH> > 	  	dataOut ;
  
  sc_in<sc_clock > clk;
  sc_in<sc_lv<1> > rst;

  // Internal variables
  sc_uint <DATA_WIDTH> mem [RAM_DEPTH];
  int i;
  

  // Methods
  void  read_data ();
  void  write_data ();
  
  void reset();

  // Constructor
  SC_CTOR(XRam) {
  
    SC_METHOD(read_data);
      	sensitive << address << we << re << dataIn;
      	sensitive_pos << clk;
      
    SC_METHOD(write_data);
      	sensitive << address << we << re << dataIn;
      	sensitive_pos << clk;
      	
    SC_METHOD(reset);
    	sensitive << rst;
      
    for( i=0; i<RAM_DEPTH; i++){
		mem[i] = 0;
	}
  }
};
#endif
