//include header files( always include the SystemC library)
#include "systemc.h"
	
	

SC_MODULE(Gate16) {
	//port declarations
	
	//input data ports
			
	sc_in<sc_lv<16> > A;
			
	sc_in<sc_lv<16> > B;
			
		
	//output data ports
			
	sc_out<sc_lv<16> > R;
	
	//global signals
	sc_in<sc_logic > rst;
	sc_clock clk;
	
	
	//signal declarations
	//Dataflow wires (named after the source port and the sink port)
	
	
	//Controlflow wires (named after the source port and the sink port)
	
	
	//Auxilliary variables and signals
	
	
	sc_signal<sc_lv<16> > gate00_O;
	sc_signal<sc_lv<16> > gate01_O;
	
	//module instance declarations
	
	
	//process/function declarations
	void Gate00();
	void Gate01();
	void Gate10();
	
	
	
	//constructor
 	SC_CTOR(Gate16){
	//process registration and sensitivity list
	
		SC_METHOD(Gate00);
			sensitive << A;
			sensitive << B;
			
		SC_METHOD(Gate01);
			sensitive << A;
			sensitive << B;
			
		SC_METHOD(Gate10);
			
	
	}
};
