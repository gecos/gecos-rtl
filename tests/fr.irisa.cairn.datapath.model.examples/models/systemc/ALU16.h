//include header files( always include the SystemC library)
#include "systemc.h"
	
	

SC_MODULE(ALU16) {
	//port declarations
	
	//input control ports
			
	sc_in<sc_lv<3> > OP; 
			
		
	
	
		
	
	
	//input data ports
			
	sc_in<sc_lv<16> > A;
			
	sc_in<sc_lv<16> > B;
			
		
	//output data ports
			
	sc_out<sc_lv<16> > R;
			
		
	
	
	//global signals
			sc_in<sc_logic > rst;
			sc_clock clk;
	
	
	//signal declarations
	//Dataflow wires (named after the source port and the sink port)
	
	
	//Controlflow wires (named after the source port and the sink port)
	
	
	//Auxilliary variables and signals
	
	
			sc_signal<sc_int<16> > ADD_I0;
			sc_signal<sc_int<16> > ADD_I1;
		
			sc_signal<sc_int<16> > ADD_O;
		
			sc_signal<sc_int<16> > SUB_I0;
			sc_signal<sc_int<16> > SUB_I1;
		
			sc_signal<sc_int<16> > SUB_O;
		
			sc_signal<sc_int<16> > AND_I0;
			sc_signal<sc_int<16> > AND_I1;
		
			sc_signal<sc_int<16> > AND_O;
		
			sc_signal<sc_int<16> > OR_I0;
			sc_signal<sc_int<16> > OR_I1;
		
			sc_signal<sc_int<16> > OR_O;
		
			sc_signal<sc_int<16> > XOR_I0;
			sc_signal<sc_int<16> > XOR_I1;
		
			sc_signal<sc_int<16> > XOR_O;
		
		
			sc_signal<sc_int<3> > C2D6_OP_O;
		
			sc_signal<sc_int<1> > D2C7_sel_op5_I;
		
		
			sc_signal<sc_int<16> > cmux_mux00_I0;
			sc_signal<sc_int<16> > cmux_mux00_I1;
		
			sc_signal<sc_int<16> > cmux_mux00_O;
		
		
			sc_signal<sc_int<3> > C2D9_OP_O;
		
			sc_signal<sc_int<1> > D2C10_sel_op8_I;
		
		
			sc_signal<sc_int<16> > cmux_mux01_I0;
			sc_signal<sc_int<16> > cmux_mux01_I1;
		
			sc_signal<sc_int<16> > cmux_mux01_O;
		
		
			sc_signal<sc_int<3> > C2D12_OP_O;
		
			sc_signal<sc_int<1> > D2C13_sel_op11_I;
		
		
			sc_signal<sc_int<16> > cmux_mux10_I0;
			sc_signal<sc_int<16> > cmux_mux10_I1;
		
			sc_signal<sc_int<16> > cmux_mux10_O;
		
		
			sc_signal<sc_int<3> > C2D15_OP_O;
		
			sc_signal<sc_int<1> > D2C16_sel_op14_I;
		
		
			sc_signal<sc_int<16> > cmux_mux20_I0;
			sc_signal<sc_int<16> > cmux_mux20_I1;
		
			sc_signal<sc_int<16> > cmux_mux20_O;
		
	
	
			sc_signal<sc_int<3> > C2D6_OP_I;
		
			sc_signal<sc_int<1> > cmux_mux00_S;
		
			sc_signal<sc_int<3> > C2D9_OP_I;
		
			sc_signal<sc_int<1> > cmux_mux01_S;
		
			sc_signal<sc_int<3> > C2D12_OP_I;
		
			sc_signal<sc_int<1> > cmux_mux10_S;
		
			sc_signal<sc_int<3> > C2D15_OP_I;
		
			sc_signal<sc_int<1> > cmux_mux20_S;
		
	
	
			sc_signal<sc_int<1> > D2C7_sel_op5_O;
		
			sc_signal<sc_int<1> > D2C10_sel_op8_O;
		
			sc_signal<sc_int<1> > D2C13_sel_op11_O;
		
			sc_signal<sc_int<1> > D2C16_sel_op14_O;
		
	

	
	//module instance declarations
	
	
	//process/function declarations
	void ADD();
	
	void SUB();
	
	void AND();
	
	void OR();
	
	void XOR();
	
	void C2D6_OP();
	
	void D2C7_sel_op5();
	
	void cmux_mux00();
	
	void C2D9_OP();
	
	void D2C10_sel_op8();
	
	void cmux_mux01();
	
	void C2D12_OP();
	
	void D2C13_sel_op11();
	
	void cmux_mux10();
	
	void C2D15_OP();
	
	void D2C16_sel_op14();
	
	void cmux_mux20();
	
	
	
	//signal interconnection
	
	void signalConnect();
	
	//constructor
 	SC_CTOR(ALU16)   
 	
	
	
	{
	//process registration and sensitivity list
	
	
		SC_METHOD(ADD);
			sensitive << ADD_I0;
			sensitive << ADD_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(SUB);
			sensitive << SUB_I0;
			sensitive << SUB_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(AND);
			sensitive << AND_I0;
			sensitive << AND_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(OR);
			sensitive << OR_I0;
			sensitive << OR_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(XOR);
			sensitive << XOR_I0;
			sensitive << XOR_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(C2D6_OP);
		
			sensitive << C2D6_OP_I;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(D2C7_sel_op5);
			sensitive << D2C7_sel_op5_I;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_mux00);
			sensitive << cmux_mux00_I0;
			sensitive << cmux_mux00_I1;
		
			sensitive << cmux_mux00_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(C2D9_OP);
		
			sensitive << C2D9_OP_I;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(D2C10_sel_op8);
			sensitive << D2C10_sel_op8_I;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_mux01);
			sensitive << cmux_mux01_I0;
			sensitive << cmux_mux01_I1;
		
			sensitive << cmux_mux01_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(C2D12_OP);
		
			sensitive << C2D12_OP_I;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(D2C13_sel_op11);
			sensitive << D2C13_sel_op11_I;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_mux10);
			sensitive << cmux_mux10_I0;
			sensitive << cmux_mux10_I1;
		
			sensitive << cmux_mux10_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(C2D15_OP);
		
			sensitive << C2D15_OP_I;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(D2C16_sel_op14);
			sensitive << D2C16_sel_op14_I;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_mux20);
			sensitive << cmux_mux20_I0;
			sensitive << cmux_mux20_I1;
		
			sensitive << cmux_mux20_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
	
	//signal interconnection registration
	
	SC_METHOD(signalConnect);
				sensitive << ADD_I0;
				
				sensitive << ADD_I1;
				
			
		
		
				sensitive << SUB_I0;
				
				sensitive << SUB_I1;
				
			
		
		
				sensitive << AND_I0;
				
				sensitive << AND_I1;
				
			
		
		
				sensitive << OR_I0;
				
				sensitive << OR_I1;
				
			
		
		
				sensitive << XOR_I0;
				
				sensitive << XOR_I1;
				
			
		
		
			
		
				sensitive << C2D6_OP_I;
				
			
		
				sensitive << D2C7_sel_op5_I;
				
			
		
		
				sensitive << cmux_mux00_I0;
				
				sensitive << cmux_mux00_I1;
				
			
		
				sensitive << cmux_mux00_S;
				
			
		
			
		
				sensitive << C2D9_OP_I;
				
			
		
				sensitive << D2C10_sel_op8_I;
				
			
		
		
				sensitive << cmux_mux01_I0;
				
				sensitive << cmux_mux01_I1;
				
			
		
				sensitive << cmux_mux01_S;
				
			
		
			
		
				sensitive << C2D12_OP_I;
				
			
		
				sensitive << D2C13_sel_op11_I;
				
			
		
		
				sensitive << cmux_mux10_I0;
				
				sensitive << cmux_mux10_I1;
				
			
		
				sensitive << cmux_mux10_S;
				
			
		
			
		
				sensitive << C2D15_OP_I;
				
			
		
				sensitive << D2C16_sel_op14_I;
				
			
		
		
				sensitive << cmux_mux20_I0;
				
				sensitive << cmux_mux20_I1;
				
			
		
				sensitive << cmux_mux20_S;
				
			
		
	
				sensitive << A;
				sensitive << B;
		
	
				sensitive << OP;
		
	
				sensitive << rst;
				sensitive_pos << clk;
	
	
	//component interconnection
	
	
	}
};
