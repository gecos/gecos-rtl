// Function(implementation) : Synchronous read/write RAM
#include "XRam.h"

void  XRam::read_data () {
 	
 	if(clk.event() && clk.read() == '1'){
	 	if ((re.read() == "1") && (we.read() == "0")) {
	    	dataOut.write( mem[ (address.read()).to_uint() ] );
	  	}
  	}
}

void  XRam::write_data () {
	
	if(clk.event() && clk.read() == '1'){
		if ((re.read() == "0") && (we.read() == "1")) {
		   	mem[ (address.read()).to_uint() ] = dataIn.read();
		} 
	}
}

void XRam::reset(){
	int i;
	
	if(rst.read() == '1'){
		for( i=0; i<RAM_DEPTH; i++){
			mem[i] = 0;
		}
	}
}

