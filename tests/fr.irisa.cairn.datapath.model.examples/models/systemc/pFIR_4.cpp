
//include the header of the module
#include "pFIR_4.h"
#include "stdio.h"
#include "math.h"

	//constructor implementation -- needed for constructor with multiple parameters

	//process/function implementation
	
	
	void pFIR_4::reg_1(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		reg_1_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( reg_1_CE.read() == '1'){
				reg_1_val = reg_1_D;
			
		}
	}
	
	reg_1_Q = reg_1_val;
	
	}
	
	void pFIR_4::reg_2(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		reg_2_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( reg_2_CE.read() == '1'){
				reg_2_val = reg_2_D;
			
		}
	}
	
	reg_2_Q = reg_2_val;
	
	}
	
	void pFIR_4::reg_3(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		reg_3_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( reg_3_CE.read() == '1'){
				reg_3_val = reg_3_D;
			
		}
	}
	
	reg_3_Q = reg_3_val;
	
	}
	

	
	//signal interconnection
	void pFIR_4::signalConnect(){
		
		
					reg_1_D = X.read();
			
		
					Y.write( ( (sc_lv<32>)  ( ( reg_3_Q.read().to_uint() * (56) ) + ( ( reg_2_Q.read().to_uint() * (12) ) + ( ( reg_1_Q.read().to_uint() * (7) ) + ( X.read().to_uint() * (1) ) ) ) ) ) .range(15 , 0) );
				
		
					reg_1_CE = CE.read();
					reg_2_CE = CE.read();
					reg_3_CE = CE.read();
			
		
		
		
			
				
			
				
			
				
			
				
			
				
			
		
	};


