
//include the header of the module
#include "ALU16.h"
#include "stdio.h"
#include "math.h"

	//constructor implementation -- needed for constructor with multiple parameters

	//process/function implementation
	
	
	void ALU16::ADD(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = ADD_I0;
	I1_int = ADD_I1;
	
	
			O_int = I0_int + I1_int;
	
	
	ADD_O = O_int;
	
	}
	
	void ALU16::SUB(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = SUB_I0;
	I1_int = SUB_I1;
	
	
			O_int = I0_int - I1_int;
	
	
	SUB_O = O_int;
	
	}
	
	void ALU16::AND(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = AND_I0;
	I1_int = AND_I1;
	
	
			O_int = I0_int & I1_int;
	
	
	AND_O = O_int;
	
	}
	
	void ALU16::OR(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = OR_I0;
	I1_int = OR_I1;
	
	
			O_int = I0_int | I1_int;
	
	
	OR_O = O_int;
	
	}
	
	void ALU16::XOR(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = XOR_I0;
	I1_int = XOR_I1;
	
	
			O_int = I0_int ^ I1_int;
	
	
	XOR_O = O_int;
	
	}
	
	void ALU16::C2D6_OP(){
		//generate intermediate signals for arithmetic operations
		sc_int<3> I_int; 
	
		sc_int<3> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	
	I_int = C2D6_OP_I;
	
			O_int = I_int;
	
	
	C2D6_OP_O = O_int;
	
	}
	
	void ALU16::D2C7_sel_op5(){
		//generate intermediate signals for arithmetic operations
		sc_int<1> I_int; 
	
		sc_int<1> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleInputDataFlowBlock
	I_int = D2C7_sel_op5_I;
	
	
	O_int = I_int
	
	D2C7_sel_op5_O = O_int;
	
	}
	
	void ALU16::cmux_mux00(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_mux00_I0;
	I1_int = cmux_mux00_I1;
	
		S_int = cmux_mux00_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_mux00_O = O_int;
	
	}
	
	void ALU16::C2D9_OP(){
		//generate intermediate signals for arithmetic operations
		sc_int<3> I_int; 
	
		sc_int<3> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	
	I_int = C2D9_OP_I;
	
			O_int = I_int;
	
	
	C2D9_OP_O = O_int;
	
	}
	
	void ALU16::D2C10_sel_op8(){
		//generate intermediate signals for arithmetic operations
		sc_int<1> I_int; 
	
		sc_int<1> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleInputDataFlowBlock
	I_int = D2C10_sel_op8_I;
	
	
	O_int = I_int
	
	D2C10_sel_op8_O = O_int;
	
	}
	
	void ALU16::cmux_mux01(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_mux01_I0;
	I1_int = cmux_mux01_I1;
	
		S_int = cmux_mux01_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_mux01_O = O_int;
	
	}
	
	void ALU16::C2D12_OP(){
		//generate intermediate signals for arithmetic operations
		sc_int<3> I_int; 
	
		sc_int<3> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	
	I_int = C2D12_OP_I;
	
			O_int = I_int;
	
	
	C2D12_OP_O = O_int;
	
	}
	
	void ALU16::D2C13_sel_op11(){
		//generate intermediate signals for arithmetic operations
		sc_int<1> I_int; 
	
		sc_int<1> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleInputDataFlowBlock
	I_int = D2C13_sel_op11_I;
	
	
	O_int = I_int
	
	D2C13_sel_op11_O = O_int;
	
	}
	
	void ALU16::cmux_mux10(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_mux10_I0;
	I1_int = cmux_mux10_I1;
	
		S_int = cmux_mux10_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_mux10_O = O_int;
	
	}
	
	void ALU16::C2D15_OP(){
		//generate intermediate signals for arithmetic operations
		sc_int<3> I_int; 
	
		sc_int<3> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	
	I_int = C2D15_OP_I;
	
			O_int = I_int;
	
	
	C2D15_OP_O = O_int;
	
	}
	
	void ALU16::D2C16_sel_op14(){
		//generate intermediate signals for arithmetic operations
		sc_int<1> I_int; 
	
		sc_int<1> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleInputDataFlowBlock
	I_int = D2C16_sel_op14_I;
	
	
	O_int = I_int
	
	D2C16_sel_op14_O = O_int;
	
	}
	
	void ALU16::cmux_mux20(){
		//generate intermediate signals for arithmetic operations
		sc_int<16> I0_int; 
		sc_int<16> I1_int; 
	
		sc_int<16> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_mux20_I0;
	I1_int = cmux_mux20_I1;
	
		S_int = cmux_mux20_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_mux20_O = O_int;
	
	}
	

	
	//signal interconnection
	void ALU16::signalConnect(){
				
					D2C7_sel_op5_I = sel_op5_O;
				
			
			
					cmux_mux00_I0 = ADD_O;
					cmux_mux00_I1 = SUB_O;
				
			
					cmux_mux00_S = D2C7_sel_op5_O;
				
			
				
			
				
			
					D2C10_sel_op8_I = sel_op8_O;
				
			
			
					cmux_mux01_I0 = AND_O;
					cmux_mux01_I1 = OR_O;
				
			
					cmux_mux01_S = D2C10_sel_op8_O;
				
			
				
			
				
			
					D2C13_sel_op11_I = sel_op11_O;
				
			
			
					cmux_mux10_I0 = cmux_mux00_O;
					cmux_mux10_I1 = cmux_mux01_O;
				
			
					cmux_mux10_S = D2C13_sel_op11_O;
				
			
				
			
				
			
					D2C16_sel_op14_I = sel_op14_O;
				
			
			
					cmux_mux20_I0 = cmux_mux10_O;
					cmux_mux20_I1 = XOR_O;
				
			
					cmux_mux20_S = D2C16_sel_op14_O;
				
			
		
		
				ADD_I0 = A.read();
				SUB_I0 = A.read();
				AND_I0 = A.read();
				OR_I0 = A.read();
				XOR_I0 = A.read();
			
				ADD_I1 = B.read();
				SUB_I1 = B.read();
				AND_I1 = B.read();
				OR_I1 = B.read();
				XOR_I1 = B.read();
			
		
					R.write( cmux_mux20_O );
				
		
				C2D6_OP_I = OP.read();
				C2D9_OP_I = OP.read();
				C2D12_OP_I = OP.read();
				C2D15_OP_I = OP.read();
			
		
		
		
		
	};


