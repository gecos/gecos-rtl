//include header files( always include the SystemC library)
#include "systemc.h"
		#include "fir_ctrl_FSM.h"
		#include "CRom.h"
		#include "XRam.h"
	
	









	
	








	
	








	

SC_MODULE(FIR2_8) {
	//port declarations
	
		
	
	
		
	
	
	//input data ports
	sc_in<sc_lv<8> > X;
			
		
	//output data ports
	sc_out<sc_lv<8> > Y;
			
		
	
	
	//global signals
			sc_in<sc_lv<1> > rst;
			sc_in<sc_clock > clk;
	
	
	//signal declarations
	//Dataflow wires (named after the source port and the sink port)
			sc_signal<sc_lv<8> > cpt_Q_CRom_address;
		
					sc_signal<sc_lv<8> > CRom_dataOut_op17_I0;
				
			
		
			sc_signal<sc_lv<8> > adx_Q_XRam_address;
			sc_signal<sc_lv<8> > X_X_XRam_dataIn;
		
					sc_signal<sc_lv<8> > XRam_dataOut_op17_I1;
				
			
		
	
	
	//Controlflow wires (named after the source port and the sink port)
			sc_signal<sc_lv<1> > fir_ctrl_endLoop_;
			sc_signal<sc_lv<1> > fir_ctrl_inEmpty_;
			sc_signal<sc_lv<1> > fir_ctrl_outFull_;
		
		
		
	
				sc_signal<sc_lv<1> > fir_ctrl_fifo_rd_;
			
					sc_signal<sc_lv<1> > fir_ctrl_xwr_en_XRam_we;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_xrd_en_XRam_re;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_adx_en_adx_CE;
					sc_signal<sc_lv<1> > fir_ctrl_adx_en_cmux_op16_S;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_crd_en_CRom_re;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_cpt_en_cpt_CE;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_cpt_init_cmux_op11_S;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_mul_en_mul_reg_CE;
				
			
					sc_signal<sc_lv<1> > fir_ctrl_acc_en_acc_reg_CE;
				
			
				sc_signal<sc_lv<1> > fir_ctrl_fifo_wr_;
			
		
	
	
	//Auxilliary variables and signals
		sc_lv<8> cpt_val ;
		sc_lv<8> adx_val ;
		sc_lv<16> mul_reg_val ;
		sc_lv<16> acc_reg_val ;
	
	
			sc_signal<sc_int<8> > cpt_D;
		
			sc_signal<sc_int<8> > cpt_Q;
		
			sc_signal<sc_int<8> > add_9_I0;
			sc_signal<sc_int<8> > add_9_I1;
		
			sc_signal<sc_int<8> > add_9_O;
		
		
			sc_signal<sc_int<8> > C_V0_op10_O;
		
			sc_signal<sc_int<8> > cmux_op11_I0;
			sc_signal<sc_int<8> > cmux_op11_I1;
		
			sc_signal<sc_int<8> > cmux_op11_O;
		
			sc_signal<sc_int<8> > adx_D;
		
			sc_signal<sc_int<8> > adx_Q;
		
			sc_signal<sc_int<8> > add_13_I0;
			sc_signal<sc_int<8> > add_13_I1;
		
			sc_signal<sc_int<8> > add_13_O;
		
			sc_signal<sc_int<8> > sub_15_I0;
			sc_signal<sc_int<8> > sub_15_I1;
		
			sc_signal<sc_int<8> > sub_15_O;
		
			sc_signal<sc_int<8> > cmux_op16_I0;
			sc_signal<sc_int<8> > cmux_op16_I1;
		
			sc_signal<sc_int<8> > cmux_op16_O;
		
			sc_signal<sc_int<16> > mul_reg_D;
		
			sc_signal<sc_int<16> > mul_reg_Q;
		
			sc_signal<sc_int<16> > acc_reg_D;
		
			sc_signal<sc_int<16> > acc_reg_Q;
		
	
	
			sc_signal<sc_lv<1> > cpt_CE;
		
			sc_signal<sc_lv<1> > cmux_op11_S;
		
			sc_signal<sc_lv<1> > adx_CE;
		
			sc_signal<sc_lv<1> > cmux_op16_S;
		
			sc_signal<sc_lv<1> > mul_reg_CE;
		
			sc_signal<sc_lv<1> > acc_reg_CE;
		
	
	
	

	
	//module instance declarations
			fir_ctrl_FSM fir_ctrl_inst;
		
			CRom CRom_inst;
		
			XRam XRam_inst;
		
	
	
	//process/function declarations
	void cpt();
	
	void add_9();
	
	void C_V0_op10();
	
	void cmux_op11();
	
	void adx();
	
	void add_13();
	
	void sub_15();
	
	void cmux_op16();
	
	void mul_reg();
	
	void acc_reg();
	
	
	
	//signal interconnection
	
	void signalConnect();
	
	//constructor
 	SC_CTOR(FIR2_8)   
 	
	:
	
		fir_ctrl_inst("fir_ctrl_inst")
	,		CRom_inst("CRom_inst")
	,		XRam_inst("XRam_inst")
	
	{
	//process registration and sensitivity list
	
	
		SC_METHOD(cpt);
			sensitive << cpt_D;
		
			sensitive << cpt_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(add_9);
			sensitive << add_9_I0;
			sensitive << add_9_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(C_V0_op10);
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_op11);
			sensitive << cmux_op11_I0;
			sensitive << cmux_op11_I1;
		
			sensitive << cmux_op11_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(adx);
			sensitive << adx_D;
		
			sensitive << adx_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(add_13);
			sensitive << add_13_I0;
			sensitive << add_13_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(sub_15);
			sensitive << sub_15_I0;
			sensitive << sub_15_I1;
		
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(cmux_op16);
			sensitive << cmux_op16_I0;
			sensitive << cmux_op16_I1;
		
			sensitive << cmux_op16_S;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(mul_reg);
			sensitive << mul_reg_D;
		
			sensitive << mul_reg_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(acc_reg);
			sensitive << acc_reg_D;
		
			sensitive << acc_reg_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
	
	//signal interconnection registration
	
	SC_METHOD(signalConnect);
				sensitive << add_9_I0;
				
				sensitive << add_9_I1;
				
			
		
		
			
		
		
				sensitive << cmux_op11_I0;
				
				sensitive << cmux_op11_I1;
				
			
		
				sensitive << cmux_op11_S;
				
			
		
				sensitive << add_13_I0;
				
				sensitive << add_13_I1;
				
			
		
		
				sensitive << sub_15_I0;
				
				sensitive << sub_15_I1;
				
			
		
		
				sensitive << cmux_op16_I0;
				
				sensitive << cmux_op16_I1;
				
			
		
				sensitive << cmux_op16_S;
				
			
		
	
				sensitive << X;
		
	
		
	
				sensitive << rst;
				sensitive_pos << clk;
	
	
	//component interconnection
	
	// module instance fir_ctrl_inst port connections
	// connection of global clock and reset signals
		fir_ctrl_inst.clk(clk);
		fir_ctrl_inst.rst(rst);
	
	
			//connection of input control ports of component fir_ctrl
		
			fir_ctrl_inst.endLoop(fir_ctrl_endLoop_);
			fir_ctrl_inst.inEmpty(fir_ctrl_inEmpty_);
			fir_ctrl_inst.outFull(fir_ctrl_outFull_);
		
	
			//connection of output control ports of component fir_ctrl
		
			fir_ctrl_inst.fifo_rd(fir_ctrl_fifo_rd_);
			fir_ctrl_inst.xwr_en(fir_ctrl_xwr_en_XRam_we);
			fir_ctrl_inst.xrd_en(fir_ctrl_xrd_en_XRam_re);
			fir_ctrl_inst.adx_en(fir_ctrl_adx_en_adx_CE);
			fir_ctrl_inst.crd_en(fir_ctrl_crd_en_CRom_re);
			fir_ctrl_inst.cpt_en(fir_ctrl_cpt_en_cpt_CE);
			fir_ctrl_inst.cpt_init(fir_ctrl_cpt_init_cmux_op11_S);
			fir_ctrl_inst.mul_en(fir_ctrl_mul_en_mul_reg_CE);
			fir_ctrl_inst.acc_en(fir_ctrl_acc_en_acc_reg_CE);
			fir_ctrl_inst.fifo_wr(fir_ctrl_fifo_wr_);
		
	
	// module instance CRom_inst port connections
	// connection of global clock and reset signals
		CRom_inst.clk(clk);
		CRom_inst.rst(rst);
	
			//connection of input data ports of component CRom
		
			CRom_inst.address(cpt_Q_CRom_address);
		
			//connection of output data ports of component CRom
		
			CRom_inst.dataOut(CRom_dataOut_op17_I0);
		
	
			//connection of input control ports of component CRom
		
			CRom_inst.re(fir_ctrl_crd_en_CRom_re);
		
	
	
	// module instance XRam_inst port connections
	// connection of global clock and reset signals
		XRam_inst.clk(clk);
		XRam_inst.rst(rst);
	
			//connection of input data ports of component XRam
		
			XRam_inst.address(adx_Q_XRam_address);
			XRam_inst.dataIn(X_X_XRam_dataIn);
		
			//connection of output data ports of component XRam
		
			XRam_inst.dataOut(XRam_dataOut_op17_I1);
		
	
			//connection of input control ports of component XRam
		
			XRam_inst.re(fir_ctrl_xrd_en_XRam_re);
			XRam_inst.we(fir_ctrl_xwr_en_XRam_we);
		
	
	
	
	}
};
