//include header files( always include the SystemC library)
#include "systemc.h"
	
	

SC_MODULE(pFIR_4) {
	//port declarations
	
	//input control ports
	sc_in<sc_lv<1> > CE; 
			
		
	
	
		
	
	
	//input data ports
	sc_in<sc_lv<16> > X;
			
		
	//output data ports
	sc_out<sc_lv<16> > Y;
			
		
	
	
	//global signals
			sc_in<sc_lv<1> > rst;
			sc_in<sc_clock > clk;
	
	
	//signal declarations
	//Dataflow wires (named after the source port and the sink port)
	
	
	//Controlflow wires (named after the source port and the sink port)
	
	
	
	//Auxilliary variables and signals
		sc_lv<16> reg_1_val ;
		sc_lv<16> reg_2_val ;
		sc_lv<16> reg_3_val ;
	
	
			sc_signal<sc_int<16> > reg_1_D;
		
			sc_signal<sc_int<16> > reg_1_Q;
		
			sc_signal<sc_int<16> > reg_2_D;
		
			sc_signal<sc_int<16> > reg_2_Q;
		
			sc_signal<sc_int<16> > reg_3_D;
		
			sc_signal<sc_int<16> > reg_3_Q;
		
	
	
			sc_signal<sc_lv<1> > reg_1_CE;
		
			sc_signal<sc_lv<1> > reg_2_CE;
		
			sc_signal<sc_lv<1> > reg_3_CE;
		
	
	
	

	
	//module instance declarations
	
	
	//process/function declarations
	void reg_1();
	
	void reg_2();
	
	void reg_3();
	
	
	
	//signal interconnection
	
	void signalConnect();
	
	//constructor
 	SC_CTOR(pFIR_4)   
 	
	
	
	{
	//process registration and sensitivity list
	
	
		SC_METHOD(reg_1);
			sensitive << reg_1_D;
		
			sensitive << reg_1_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(reg_2);
			sensitive << reg_2_D;
		
			sensitive << reg_2_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
		SC_METHOD(reg_3);
			sensitive << reg_3_D;
		
			sensitive << reg_3_CE;
			
		
			sensitive << rst;
			sensitive_pos << clk;
	
	
	
	
	//signal interconnection registration
	
	SC_METHOD(signalConnect);
	
				sensitive << X;
		
	
				sensitive << CE;
		
	
				sensitive << rst;
				sensitive_pos << clk;
	
	
	//component interconnection
	
	
	}
};
