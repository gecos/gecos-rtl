
#ifndef FIR_CTRL_FSM
#define FIR_CTRL_FSM

//iclude SystemC header
#include "systemc.h"

SC_MODULE(fir_ctrl_FSM) {

	enum fir_ctrl_enum {
	
		idle	,
		waitNotEmpty	,
		readFIFO	,
		step1	,
		step2	,
		steady	,
		flush1	,
		flush2	,
		waitsState1	,
		writeFIFO	
	};
	
	
	sc_in<sc_lv<1> > rst;
	sc_in_clk clk;
	


	// ports (input)
	
	sc_in<sc_lv<1> > endLoop;
	
	sc_in<sc_lv<1> > inEmpty;
	
	sc_in<sc_lv<1> > outFull;
	
	
	

	// ports (output)
	
	sc_out<sc_lv<1> > fifo_rd;
	
	sc_out<sc_lv<1> > xwr_en;
	
	sc_out<sc_lv<1> > xrd_en;
	
	sc_out<sc_lv<1> > adx_en;
	
	sc_out<sc_lv<1> > crd_en;
	
	sc_out<sc_lv<1> > cpt_en;
	
	sc_out<sc_lv<1> > cpt_init;
	
	sc_out<sc_lv<1> > mul_en;
	
	sc_out<sc_lv<1> > acc_en;
	
	sc_out<sc_lv<1> > fifo_wr;
	
	
	
	
	// local member data
	enum fir_ctrl_enum current;
	enum fir_ctrl_enum next;
	
	
	
	// local member functions
	void nextstate();
	void updatecommand();
	void updatestate();
	
	
	
	// constructor
	SC_CTOR(fir_ctrl_FSM) {
	
	    SC_METHOD (nextstate);
      		sensitive << endLoop << inEmpty << outFull;
      		sensitive_pos << clk;
      		
	    SC_METHOD (updatecommand);
      		sensitive << endLoop << inEmpty << outFull;
      		sensitive_pos << clk;
      		
	    SC_METHOD (updatestate);
      		sensitive_pos << clk ;
      		
      	current = idle;
	}
	
};

#endif
