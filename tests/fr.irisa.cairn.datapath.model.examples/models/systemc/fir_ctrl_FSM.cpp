
#include <fir_ctrl_FSM.h> 

void fir_ctrl_FSM::nextstate()	{

	switch(current) {
	case idle: 
			
		// Predicate 1			
		next  = waitNotEmpty ;
		
		
	
		
	
		break;
	case waitNotEmpty: 
			
		// Predicate !inEmpty			
		
		if (			inEmpty.read() == "0"
	
){
		   next  = readFIFO ;
		};
		
	
			
		// Predicate !(!(inEmpty))			
		
		if (	!(			inEmpty.read() == "0"
	
)
){
		   next  = waitNotEmpty ;
		};
		
	
		
	
		break;
	case readFIFO: 
			
		// Predicate 1			
		next  = step1 ;
		
		
	
		
	
		break;
	case step1: 
			
		// Predicate 1			
		next  = step2 ;
		
		
	
		
	
		break;
	case step2: 
			
		// Predicate 1			
		next  = steady ;
		
		
	
		
	
		break;
	case steady: 
			
		// Predicate endLoop			
		
		if (			endLoop.read() == "1"
	
){
		   next  = flush1 ;
		};
		
	
			
		// Predicate !(endLoop)			
		
		if (	!(			endLoop.read() == "1"
	
)
){
		   next  = steady ;
		};
		
	
		
	
		break;
	case flush1: 
			
		// Predicate 1			
		next  = flush2 ;
		
		
	
		
	
		break;
	case flush2: 
			
		// Predicate 1			
		next  = waitsState1 ;
		
		
	
		
	
		break;
	case waitsState1: 
			
		// Predicate !outFull			
		
		if (			outFull.read() == "0"
	
){
		   next  = writeFIFO ;
		};
		
	
			
		// Predicate !(!(outFull))			
		
		if (	!(			outFull.read() == "0"
	
)
){
		   next  = waitsState1 ;
		};
		
	
		
	
		break;
	case writeFIFO: 
			
		// Predicate 1			
		next  = idle ;
		
		
	
		
	
		break;
	
		default :
			break;
	}
}

void fir_ctrl_FSM::updatecommand()	{
	
	
	switch(current) {
		
	case idle: 
	
		break;
		
	case waitNotEmpty: 
	
		break;
		
	case readFIFO: 
			
				xwr_en = 1;
			
		
		
	
			
				fifo_rd = 1;
			
		
		
	
	
		break;
		
	case step1: 
			
				crd_en = 1;
			
		
		
	
			
				cpt_en = 1;
			
		
		
	
	
		break;
		
	case step2: 
			
				mul_en = 1;
			
		
		
	
			
				xrd_en = 1;
			
		
		
	
			
				crd_en = 1;
			
		
		
	
			
				cpt_en = 1;
			
		
		
	
	
		break;
		
	case steady: 
			
				acc_en = 1;
			
		
		
	
			
				mul_en = 1;
			
		
		
	
			
				xrd_en = 1;
			
		
		
	
			
				crd_en = 1;
			
		
		
	
			
				cpt_en = 1;
			
		
		
	
	
		break;
		
	case flush1: 
			
				acc_en = 1;
			
		
		
	
			
				mul_en = 1;
			
		
		
	
	
		break;
		
	case flush2: 
			
				acc_en = 1;
			
		
		
	
			
				mul_en = 1;
			
		
		
	
	
		break;
		
	case waitsState1: 
	
		break;
		
	case writeFIFO: 
			
				fifo_wr = 1;
			
		
		
	
	
		break;
	
		default :
			break;
	}
}

void fir_ctrl_FSM::updatestate()	{
	current = next;
}

