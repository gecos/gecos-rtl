
//include the header of the module
#include "Gate16.h"
#include "stdio.h"
#include "math.h"

	//constructor implementation -- needed for constructor with multiple parameters

	//process/function implementation
	
	
	
	void Gate16::Gate00(){
		
		gate00_O.write((~A.read()) & B.read());
		
		cout << "function gate00 called \n";
	}
	
	void Gate16::Gate01(){
		
		gate01_O.write(A.read() & (~B.read()));
		
		cout << "function gate01 called \n";
	}
	
	void Gate16::Gate10(){
		
		R.write(gate00_O.read() | gate01_O.read());
		
		cout << "function gate10 called \n";
	}
	
	

	
	


