
//include the header of the module
#include "FIR2_8.h"
#include "stdio.h"
#include "math.h"

	//constructor implementation -- needed for constructor with multiple parameters

	//process/function implementation
	
	
	void FIR2_8::cpt(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		cpt_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( cpt_CE.read() == '1'){
				cpt_val = cpt_D;
			
		}
	}
	
	cpt_Q = cpt_val;
	
	}
	
	void FIR2_8::add_9(){
		//generate intermediate signals for arithmetic operations
		sc_int<8> I0_int; 
		sc_int<8> I1_int; 
	
		sc_int<8> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = add_9_I0;
	I1_int = add_9_I1;
	
	
			O_int = I0_int + I1_int;
	
	
	add_9_O = O_int;
	
	}
	
	void FIR2_8::C_V0_op10(){
		//generate intermediate signals for arithmetic operations
	
		sc_int<8> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	
	
			O_int = 0;
	
	
	C_V0_op10_O = O_int;
	
	}
	
	void FIR2_8::cmux_op11(){
		//generate intermediate signals for arithmetic operations
		sc_int<8> I0_int; 
		sc_int<8> I1_int; 
	
		sc_int<8> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_op11_I0;
	I1_int = cmux_op11_I1;
	
		S_int = cmux_op11_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_op11_O = O_int;
	
	}
	
	void FIR2_8::adx(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		adx_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( adx_CE.read() == '1'){
				adx_val = adx_D;
			
		}
	}
	
	adx_Q = adx_val;
	
	}
	
	void FIR2_8::add_13(){
		//generate intermediate signals for arithmetic operations
		sc_int<8> I0_int; 
		sc_int<8> I1_int; 
	
		sc_int<8> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = add_13_I0;
	I1_int = add_13_I1;
	
	
			O_int = I0_int + I1_int;
	
	
	add_13_O = O_int;
	
	}
	
	void FIR2_8::sub_15(){
		//generate intermediate signals for arithmetic operations
		sc_int<8> I0_int; 
		sc_int<8> I1_int; 
	
		sc_int<8> O_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = sub_15_I0;
	I1_int = sub_15_I1;
	
	
			O_int = I0_int - I1_int;
	
	
	sub_15_O = O_int;
	
	}
	
	void FIR2_8::cmux_op16(){
		//generate intermediate signals for arithmetic operations
		sc_int<8> I0_int; 
		sc_int<8> I1_int; 
	
		sc_int<8> O_int; 
	
		sc_int<1> S_int; 
	
		
		//generate the operation corresponding to the operator
	//operator implementation for operators that are instances of SingleOutputDataFlowBlock
	I0_int = cmux_op16_I0;
	I1_int = cmux_op16_I1;
	
		S_int = cmux_op16_S;
	
	
	switch( S_int){
	case 0:
		O_int = I0_int;
		break;
	case 1:
		O_int = I1_int;
		break;
	};
	
	cmux_op16_O = O_int;
	
	}
	
	void FIR2_8::mul_reg(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		mul_reg_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( mul_reg_CE.read() == '1'){
				mul_reg_val =  CRom_dataOut_op17_I0.read().to_uint() * XRam_dataOut_op17_I1.read().to_uint() ;
			
		}
	}
	
	mul_reg_Q = mul_reg_val;
	
	}
	
	void FIR2_8::acc_reg(){
		//generate intermediate signals for arithmetic operations
		
		//generate the operation corresponding to the operator
		
	//implementation for ce_registers
	
	if( rst.read() == '1' ){
		acc_reg_val = 0;
	} else if( clk.event() && clk.read() == '1' ){
		if( acc_reg_CE.read() == '1'){
				acc_reg_val =  mul_reg_Q.read().to_uint() + acc_reg_Q.read().to_uint() ;
			
		}
	}
	
	acc_reg_Q = acc_reg_val;
	
	}
	

	
	//signal interconnection
	void FIR2_8::signalConnect(){
					add_9_I0 = cpt_Q;
					add_9_I1 = 1;
				
			
			
				
			
			
					cmux_op11_I0 = add_9_O;
					cmux_op11_I1 = C_V0_op10_O;
				
			
					cmux_op11_S = fir_ctrl_cpt_init_cmux_op11_S;
				
			
					add_13_I0 = cpt_Q;
					add_13_I1 = 1;
				
			
			
					sub_15_I0 = cpt_Q;
					sub_15_I1 = 1;
				
			
			
					cmux_op16_I0 = add_13_O;
					cmux_op16_I1 = sub_15_O;
				
			
					cmux_op16_S = fir_ctrl_adx_en_cmux_op16_S;
				
			
		
		
					X_X_XRam_dataIn = X.read();
			
		
					Y.write( ( (sc_lv<16>)  acc_reg_Q.read().to_uint() ) .range(7 , 0) );
				
		
		
		
		
				
			
				
					cpt_CE = fir_ctrl_cpt_en_cpt_CE;
				
				
			
				
			
				
					adx_CE = fir_ctrl_adx_en_adx_CE;
				
				
			
				
			
				
					mul_reg_CE = fir_ctrl_mul_en_mul_reg_CE;
				
				
			
				
			
				
					acc_reg_CE = fir_ctrl_acc_en_acc_reg_CE;
				
				
			
		
	};


