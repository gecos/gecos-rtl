library ieee;
library work;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ALU16 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	OP : in std_logic_vector(2 downto 0);
	-- Input Data Pads
	A : in std_logic_vector(15 downto 0);
	B : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	R : out std_logic_vector(15 downto 0)
	)
;
end ALU16;

architecture RTL of ALU16 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal A_A : std_logic_vector(15 downto 0) ; 

	signal B_B : std_logic_vector(15 downto 0) ; 

	signal R_R : std_logic_vector(15 downto 0) ; 

	signal ADD_I0 : std_logic_vector(15 downto 0) ; 
	signal ADD_I1 : std_logic_vector(15 downto 0) ; 
	signal ADD_O : std_logic_vector(15 downto 0) ; 

	signal SUB_I0 : std_logic_vector(15 downto 0) ; 
	signal SUB_I1 : std_logic_vector(15 downto 0) ; 
	signal SUB_O : std_logic_vector(15 downto 0) ; 

	signal AND_I0 : std_logic_vector(15 downto 0) ; 
	signal AND_I1 : std_logic_vector(15 downto 0) ; 
	signal AND_O : std_logic_vector(15 downto 0) ; 

	signal OR_I0 : std_logic_vector(15 downto 0) ; 
	signal OR_I1 : std_logic_vector(15 downto 0) ; 
	signal OR_O : std_logic_vector(15 downto 0) ; 

	signal XOR_I0 : std_logic_vector(15 downto 0) ; 
	signal XOR_I1 : std_logic_vector(15 downto 0) ; 
	signal XOR_O : std_logic_vector(15 downto 0) ; 

	signal C2D6_OP_O : std_logic_vector(2 downto 0) ; 

		
	signal D2C7_sel_op5_I : std_logic ; 

	signal cmux_mux00_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux00_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux00_O : std_logic_vector(15 downto 0) ; 

	signal C2D9_OP_O : std_logic_vector(2 downto 0) ; 

		
	signal D2C10_sel_op8_I : std_logic ; 

	signal cmux_mux01_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux01_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux01_O : std_logic_vector(15 downto 0) ; 

	signal C2D12_OP_O : std_logic_vector(2 downto 0) ; 

		
	signal D2C13_sel_op11_I : std_logic ; 

	signal cmux_mux10_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux10_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux10_O : std_logic_vector(15 downto 0) ; 

	signal C2D15_OP_O : std_logic_vector(2 downto 0) ; 

		
	signal D2C16_sel_op14_I : std_logic ; 

	signal cmux_mux20_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux20_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_mux20_O : std_logic_vector(15 downto 0) ; 

	
	signal C2D6_OP_I : std_logic_vector(2 downto 0) ; 
	signal cmux_mux00_S : std_logic ; 
	signal C2D9_OP_I : std_logic_vector(2 downto 0) ; 
	signal cmux_mux01_S : std_logic ; 
	signal C2D12_OP_I : std_logic_vector(2 downto 0) ; 
	signal cmux_mux10_S : std_logic ; 
	signal C2D15_OP_I : std_logic_vector(2 downto 0) ; 
	signal cmux_mux20_S : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal OP_OP : std_logic_vector(2 downto 0) ; 
	signal D2C7_sel_op5_O : std_logic ; 
	signal D2C10_sel_op8_O : std_logic ; 
	signal D2C13_sel_op11_O : std_logic ; 
	signal D2C16_sel_op14_O : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
	
		
	

begin
	

	

	

	ADD_O <=  A_A + B_B ;
	SUB_O <=  A_A - B_B ;
	AND_O <=  A_A and B_B ;
	OR_O <=  A_A or B_B ;
	XOR_O <=  A_A xor B_B ;
	C2D6_OP_O <= OP_OP;

	--optimized for sel_op5
	
	D2C7_sel_op5_O <= sel_op5_O;
	cmux_mux00_O <= ADD_O when D2C7_sel_op5_O= '0'	 else SUB_O;
	C2D9_OP_O <= OP_OP;

	--optimized for sel_op8
	
	D2C10_sel_op8_O <= sel_op8_O;
	cmux_mux01_O <= AND_O when D2C10_sel_op8_O= '0'	 else OR_O;
	C2D12_OP_O <= OP_OP;

	--optimized for sel_op11
	
	D2C13_sel_op11_O <= sel_op11_O;
	cmux_mux10_O <= cmux_mux00_O when D2C13_sel_op11_O= '0'	 else cmux_mux01_O;
	C2D15_OP_O <= OP_OP;

	--optimized for sel_op14
	
	D2C16_sel_op14_O <= sel_op14_O;
	cmux_mux20_O <= cmux_mux10_O when D2C16_sel_op14_O= '0'	 else XOR_O;


	-- Pads
	A_A<=A;	B_B<=B;	OP_OP<=OP;	R<=R_R;
	-- DWires
	ADD_I0 <= A_A;
	ADD_I1 <= B_B;
	SUB_I0 <= A_A;
	SUB_I1 <= B_B;
	AND_I0 <= A_A;
	AND_I1 <= B_B;
	OR_I0 <= A_A;
	OR_I1 <= B_B;
	XOR_I0 <= A_A;
	XOR_I1 <= B_B;
	cmux_mux00_I0 <= ADD_O;
	cmux_mux00_I1 <= SUB_O;
	cmux_mux01_I0 <= AND_O;
	cmux_mux01_I1 <= OR_O;
	cmux_mux10_I0 <= cmux_mux00_O;
	cmux_mux10_I1 <= cmux_mux01_O;
	cmux_mux20_I0 <= cmux_mux10_O;
	cmux_mux20_I1 <= XOR_O;
	R_R <= cmux_mux20_O;

	-- CWires
	C2D6_OP_I <= OP_OP;
	cmux_mux00_S <= D2C7_sel_op5_O;
	C2D9_OP_I <= OP_OP;
	cmux_mux01_S <= D2C10_sel_op8_O;
	C2D12_OP_I <= OP_OP;
	cmux_mux10_S <= D2C13_sel_op11_O;
	C2D15_OP_I <= OP_OP;
	cmux_mux20_S <= D2C16_sel_op14_O;


	
	
	sync_register_assignment : process(rst,clk)
	
	begin
		if (rst='1') then
			
		elsif rising_edge(clk) then
			
		end if;
	end process;
	
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	R<=R_R;	

	

end RTL;
