 


library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity FIR24_8 is port (
   	-- global signals
	
	clk : in std_logic;
	rst : in std_logic
	;

	-- Input Control Ports
	
	CE : in std_logic	;

	-- Input Data Ports 
		
	X : in std_logic_vector(7 downto 0);

	-- output Data Ports
		
	Y : out std_logic_vector(7 downto 0) 

	-- output Control Ports
	
	


);
end  FIR24_8;

architecture RTL of  FIR24_8 is

   
	
	-- outputs for Data Input Pad X[8(DataInputPadImpl) 
	
		signal X_X : std_logic_vector(7 downto 0); 
			
	-- inputs for Data Input Pad X[8(DataInputPadImpl) 
			
	
	-- inputs for Control Input Pad CE[1(ControlPadImpl) 
	
				
		signal CE_CE : std_logic;
	
	-- outputs for Data Output Pad Y[8(DataOutputPadImpl) 

			
	-- inputs for Data Output Pad Y[8(DataOutputPadImpl) 
				
		signal Y_Y : std_logic_vector(7 downto 0); 	
	-- outputs for fir_ctrl 
	
				
		signal fir_ctrl_fifo_rd : std_logic;	
		signal fir_ctrl_fifo_wr : std_logic;	
		signal fir_ctrl_acc_en : std_logic;	
		signal fir_ctrl_mul_en : std_logic;	
		signal fir_ctrl_xwr_en : std_logic;	
		signal fir_ctrl_xrd_en : std_logic;	
		signal fir_ctrl_crd_en : std_logic;	
		signal fir_ctrl_cpt_en : std_logic;	
		signal fir_ctrl_adx_en : std_logic;	
		signal fir_ctrl_cpt_init : std_logic;
			
	-- inputs for fir_ctrl 
	
				
		signal fir_ctrl_endLoop : std_logic;	
		signal fir_ctrl_inEmpty : std_logic;	
		signal fir_ctrl_outFull : std_logic;
	
	-- outputs for cpt(CERegisterImpl) 
	
		signal cpt_Q : std_logic_vector(7 downto 0); 
			
	-- inputs for cpt(CERegisterImpl) 
				
		signal cpt_D : std_logic_vector(7 downto 0); 	
		signal cpt_CE :  std_logic;
	
	-- outputs for adx(CERegisterImpl) 
	
		signal adx_Q : std_logic_vector(7 downto 0); 
			
	-- inputs for adx(CERegisterImpl) 
				
		signal adx_D : std_logic_vector(7 downto 0); 	
		signal adx_CE :  std_logic;
	
	-- outputs for C_V1_op48 (value: 1)(ConstantValueImpl) 
	
		signal C_V1_op48_O : std_logic_vector(7 downto 0); 
			
	-- inputs for C_V1_op48 (value: 1)(ConstantValueImpl) 
			
	
	-- outputs for add_49:add[8](BinaryOperatorImpl) 
	
		signal add_49_O : std_logic_vector(7 downto 0); 
			
	-- inputs for add_49:add[8](BinaryOperatorImpl) 
				
		signal add_49_I0 : std_logic_vector(7 downto 0); 	
		signal add_49_I1 : std_logic_vector(7 downto 0); 
	
	-- outputs for C_V0_op50 (value: 0)(ConstantValueImpl) 
	
		signal C_V0_op50_O : std_logic_vector(7 downto 0); 
			
	-- inputs for C_V0_op50 (value: 0)(ConstantValueImpl) 
			
	
	-- outputs for cmux_op51(ControlFlowMuxImpl) 
	
		signal cmux_op51_O : std_logic_vector(7 downto 0); 
			
	-- inputs for cmux_op51(ControlFlowMuxImpl) 
				
		signal cmux_op51_I0 : std_logic_vector(7 downto 0); 	
		signal cmux_op51_I1 : std_logic_vector(7 downto 0); 	
		signal cmux_op51_S :  std_logic;
	
	-- outputs for C_V1_op52 (value: 1)(ConstantValueImpl) 
	
		signal C_V1_op52_O : std_logic_vector(7 downto 0); 
			
	-- inputs for C_V1_op52 (value: 1)(ConstantValueImpl) 
			
	
	-- outputs for add_53:add[8](BinaryOperatorImpl) 
	
		signal add_53_O : std_logic_vector(7 downto 0); 
			
	-- inputs for add_53:add[8](BinaryOperatorImpl) 
				
		signal add_53_I0 : std_logic_vector(7 downto 0); 	
		signal add_53_I1 : std_logic_vector(7 downto 0); 
	
	-- outputs for C_V1_op54 (value: 1)(ConstantValueImpl) 
	
		signal C_V1_op54_O : std_logic_vector(7 downto 0); 
			
	-- inputs for C_V1_op54 (value: 1)(ConstantValueImpl) 
			
	
	-- outputs for sub_55:sub[8](BinaryOperatorImpl) 
	
		signal sub_55_O : std_logic_vector(7 downto 0); 
			
	-- inputs for sub_55:sub[8](BinaryOperatorImpl) 
				
		signal sub_55_I0 : std_logic_vector(7 downto 0); 	
		signal sub_55_I1 : std_logic_vector(7 downto 0); 
	
	-- outputs for cmux_op56(ControlFlowMuxImpl) 
	
		signal cmux_op56_O : std_logic_vector(7 downto 0); 
			
	-- inputs for cmux_op56(ControlFlowMuxImpl) 
				
		signal cmux_op56_I0 : std_logic_vector(7 downto 0); 	
		signal cmux_op56_I1 : std_logic_vector(7 downto 0); 	
		signal cmux_op56_S :  std_logic;
	
	-- outputs for op57:mul[15](BinaryOperatorImpl) 
	
		signal op57_O : std_logic_vector(14 downto 0); 
			
	-- inputs for op57:mul[15](BinaryOperatorImpl) 
				
		signal op57_I0 : std_logic_vector(7 downto 0); 	
		signal op57_I1 : std_logic_vector(7 downto 0); 
	
	-- outputs for mul_reg(CERegisterImpl) 
	
		signal mul_reg_Q : std_logic_vector(14 downto 0); 
			
	-- inputs for mul_reg(CERegisterImpl) 
				
		signal mul_reg_D : std_logic_vector(14 downto 0); 	
		signal mul_reg_CE :  std_logic;
	
	-- outputs for acc_reg(CERegisterImpl) 
	
		signal acc_reg_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for acc_reg(CERegisterImpl) 
				
		signal acc_reg_D : std_logic_vector(15 downto 0); 	
		signal acc_reg_CE :  std_logic;
	
	-- outputs for acc_adder:add[16](BinaryOperatorImpl) 
	
		signal acc_adder_O : std_logic_vector(15 downto 0); 
			
	-- inputs for acc_adder:add[16](BinaryOperatorImpl) 
				
		signal acc_adder_I0 : std_logic_vector(14 downto 0); 	
		signal acc_adder_I1 : std_logic_vector(15 downto 0); 
	
	-- inputs for CRom 
	
				
		signal CRom_address : std_logic_vector(7 downto 0);	
		signal CRom_address : std_logic_vector(7 downto 0);	
		signal CRom_re : std_logic;	
		signal CRom_re : std_logic;
			
	-- outputs for CRom 
	
				
		signal CRom_dataOut : std_logic_vector(7 downto 0);	
		signal CRom_dataOut : std_logic_vector(7 downto 0);
	
	-- inputs for XRam 
	
				
		signal XRam_address : std_logic_vector(7 downto 0);	
		signal XRam_dataIn : std_logic_vector(7 downto 0);	
		signal XRam_re : std_logic;	
		signal XRam_we : std_logic;
			
	-- outputs for XRam 
	
				
		signal XRam_dataOut : std_logic_vector(7 downto 0);
	
	-- outputs for sel_op59:sel[7:0](BitSelectImpl) 
	
		signal sel_op59_O : std_logic_vector(-7 downto 0); 
			
	-- inputs for sel_op59:sel[7:0](BitSelectImpl) 
				
		signal sel_op59_I : std_logic_vector(15 downto 0); 

 		component fir_ctrl is port (
  		 	-- global signals
	
	clk : in std_logic;
	rst : in std_logic
	;

	-- Input Control Ports
	
	endLoop : in std_logic	;

	inEmpty : in std_logic	;

	outFull : in std_logic	;

	-- Input Data Ports 
		
	-- output Control Ports
	
	fifo_rd : out std_logic ;

	fifo_wr : out std_logic ;

	acc_en : out std_logic ;

	mul_en : out std_logic ;

	xwr_en : out std_logic ;

	xrd_en : out std_logic ;

	crd_en : out std_logic ;

	cpt_en : out std_logic ;

	adx_en : out std_logic ;

	cpt_init : out std_logic 

	


  		);
 		 end component;  

   		component CRom is port (
  		 	-- global signals
	
	clk : in std_logic;
	-- Input Control Ports
	
	re : in std_logic	;

	re : in std_logic	;

	-- Input Data Ports 
		
	address : in std_logic_vector(7 downto 0);

	address : in std_logic_vector(7 downto 0);

	-- output Data Ports
		
	dataOut : out std_logic_vector(7 downto 0) ;

	dataOut : out std_logic_vector(7 downto 0) 

	-- output Control Ports
	
	


  		);
 		 end component;  

   		component XRam is port (
  		 	-- global signals
	
	clk : in std_logic;
	rst : in std_logic
	;

	-- Input Control Ports
	
	re : in std_logic	;

	we : in std_logic	;

	-- Input Data Ports 
		
	address : in std_logic_vector(7 downto 0);

	dataIn : in std_logic_vector(7 downto 0);

	-- output Data Ports
		
	dataOut : out std_logic_vector(7 downto 0) 

	-- output Control Ports
	
	


  		);
 		 end component;  

  

begin

	X_X <= X ;
			
	CE_CE <= CE ;
			
	Y <= Y_Y ;
			  		inst_fir_ctrl : fir_ctrl port map(
   				
	-- global signals
		
	clk,
				
	rst,
		
	-- Input Control Ports
		
	fir_ctrl_endLoop ,

	fir_ctrl_inEmpty ,

	fir_ctrl_outFull ,

	-- Input Data Ports 
			
	-- output Control Ports
		
	fir_ctrl_fifo_rd,

	fir_ctrl_fifo_wr,

	fir_ctrl_acc_en,

	fir_ctrl_mul_en,

	fir_ctrl_xwr_en,

	fir_ctrl_xrd_en,

	fir_ctrl_crd_en,

	fir_ctrl_cpt_en,

	fir_ctrl_adx_en,

	fir_ctrl_cpt_init

  		);  
  
  
   		inst_CRom : CRom port map(
   				
	-- global signals
		
	clk,
			
	-- Input Control Ports
		
	CRom_re ,

	CRom_re ,

	-- Input Data Ports 
			
	CRom_address ,

	CRom_address ,

	-- output Data Ports
			
	CRom_dataOut ,

	CRom_dataOut 

	-- output Control Ports
		
  		);  
  
  
   		inst_XRam : XRam port map(
   				
	-- global signals
		
	clk,
				
	rst,
		
	-- Input Control Ports
		
	XRam_re ,

	XRam_we ,

	-- Input Data Ports 
			
	XRam_address ,

	XRam_dataIn ,

	-- output Data Ports
			
	XRam_dataOut 

	-- output Control Ports
		
  		);  
  
  
 
	
 
		add_49_I0 <= cpt_Q;
		add_49_I1 <= C_V1_op48_O;
		cmux_op51_I0 <= add_49_O;
		cmux_op51_I1 <= C_V0_op50_O;
		cpt_D <= cmux_op51_O;
		add_53_I0 <= cpt_Q;
		add_53_I1 <= C_V1_op52_O;
		sub_55_I0 <= cpt_Q;
		sub_55_I1 <= C_V1_op54_O;
		cmux_op56_I0 <= add_53_O;
		cmux_op56_I1 <= sub_55_O;
		adx_D <= cmux_op56_O;
		mul_reg_D <= op57_O;
		acc_adder_I0 <= mul_reg_Q;
		acc_adder_I1 <= acc_reg_Q;
		acc_reg_D <= acc_adder_O;
		CRom_address <= cpt_Q;
		op57_I0 <= CRom_dataOut;
		XRam_address <= adx_Q;
		op57_I1 <= XRam_dataOut;
		XRam_dataIn <= X_X;
		sel_op59_I <= acc_reg_Q;
		Y_Y <= sel_op59_O;
	cpt_CE <= fir_ctrl_cpt_en;
	adx_CE <= fir_ctrl_adx_en;
	cmux_op51_S <= fir_ctrl_cpt_init;
	cmux_op56_S <= fir_ctrl_adx_en;
	mul_reg_CE <= fir_ctrl_mul_en;
	acc_reg_CE <= fir_ctrl_acc_en;
	CRom_re <= fir_ctrl_crd_en;
	XRam_we <= fir_ctrl_xrd_en;
	XRam_re <= fir_ctrl_xrd_en;



	   
	-- Operator mapping 
 	
	C_V1_op48_O <= "11111111";	
	add_49_O <= cpt_Q + C_V1_op48_O;	
	C_V0_op50_O <= "00000000";	
	cmux_op51_O <= add_49_O when fir_ctrl_cpt_init= '0'	 else C_V0_op50_O;	
	C_V1_op52_O <= "11111111";	
	add_53_O <= cpt_Q + C_V1_op52_O;	
	C_V1_op54_O <= "11111111";	
	sub_55_O <= cpt_Q - C_V1_op54_O;	
	cmux_op56_O <= add_53_O when fir_ctrl_adx_en= '0'	 else sub_55_O;	
	op57_O <= CRom_dataOut * XRam_dataOut;	
	acc_adder_O <= mul_reg_Q + acc_reg_Q;	
	sel_op59_O <= acc_reg_Q (7 downto 0);


	

	all_regs : process(rst,clk)

	begin
		if rst='1' then
 			cpt_Q <= (others =>'0'); 
			adx_Q <= (others =>'0'); 
			mul_reg_Q <= (others =>'0'); 
			acc_reg_Q <= (others =>'0'); 

		elsif rising_edge(clk) then
		
			if fir_ctrl_cpt_en='1' then 
				cpt_Q <= cmux_op51_O;
			end if;

			if fir_ctrl_adx_en='1' then 
				adx_Q <= cmux_op56_O;
			end if;

			if fir_ctrl_mul_en='1' then 
				mul_reg_Q <= op57_O;
			end if;

			if fir_ctrl_acc_en='1' then 
				acc_reg_Q <= acc_adder_O;
			end if;
 	
		end if;
	end process;

	
end RTL;
