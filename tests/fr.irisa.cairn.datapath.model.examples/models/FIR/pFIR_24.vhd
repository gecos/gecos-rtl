 


library ieee ;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity pFIR_24 is port (
   	-- global signals
	
	clk : in std_logic;
	rst : in std_logic
	;

	-- Input Control Ports
	
	CE : in std_logic	;

	-- Input Data Ports 
		
	X : in std_logic_vector(15 downto 0);

	-- output Data Ports
		
	Y : out std_logic_vector(15 downto 0) 

	-- output Control Ports
	
	


);
end  pFIR_24;

architecture RTL of  pFIR_24 is

   
	
	-- outputs for Data Input Pad X[16(DataInputPadImpl) 
	
		signal X_X : std_logic_vector(15 downto 0); 
			
	-- inputs for Data Input Pad X[16(DataInputPadImpl) 
			
	
	-- inputs for Control Input Pad CE[1(ControlPadImpl) 
	
				
		signal CE_CE : std_logic;
	
	-- outputs for Data Output Pad Y[16(DataOutputPadImpl) 

			
	-- inputs for Data Output Pad Y[16(DataOutputPadImpl) 
				
		signal Y_Y : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V1_C_0 (value: 1)(ConstantValueImpl) 
	
		signal C_V1_C_0_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V1_C_0 (value: 1)(ConstantValueImpl) 
			
	
	-- outputs for mul_0:mul[16](BinaryOperatorImpl) 
	
		signal mul_0_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_0:mul[16](BinaryOperatorImpl) 
				
		signal mul_0_I0 : std_logic_vector(15 downto 0); 	
		signal mul_0_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V17_C_1 (value: 17)(ConstantValueImpl) 
	
		signal C_V17_C_1_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V17_C_1 (value: 17)(ConstantValueImpl) 
			
	
	-- outputs for reg_1(CERegisterImpl) 
	
		signal reg_1_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_1(CERegisterImpl) 
				
		signal reg_1_D : std_logic_vector(15 downto 0); 	
		signal reg_1_CE :  std_logic;
	
	-- outputs for mul_1:mul[16](BinaryOperatorImpl) 
	
		signal mul_1_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_1:mul[16](BinaryOperatorImpl) 
				
		signal mul_1_I0 : std_logic_vector(15 downto 0); 	
		signal mul_1_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_2:add[16](BinaryOperatorImpl) 
	
		signal add_2_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_2:add[16](BinaryOperatorImpl) 
				
		signal add_2_I0 : std_logic_vector(15 downto 0); 	
		signal add_2_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V21_C_2 (value: 21)(ConstantValueImpl) 
	
		signal C_V21_C_2_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V21_C_2 (value: 21)(ConstantValueImpl) 
			
	
	-- outputs for reg_2(CERegisterImpl) 
	
		signal reg_2_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_2(CERegisterImpl) 
				
		signal reg_2_D : std_logic_vector(15 downto 0); 	
		signal reg_2_CE :  std_logic;
	
	-- outputs for mul_3:mul[16](BinaryOperatorImpl) 
	
		signal mul_3_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_3:mul[16](BinaryOperatorImpl) 
				
		signal mul_3_I0 : std_logic_vector(15 downto 0); 	
		signal mul_3_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_4:add[16](BinaryOperatorImpl) 
	
		signal add_4_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_4:add[16](BinaryOperatorImpl) 
				
		signal add_4_I0 : std_logic_vector(15 downto 0); 	
		signal add_4_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V32_C_3 (value: 32)(ConstantValueImpl) 
	
		signal C_V32_C_3_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V32_C_3 (value: 32)(ConstantValueImpl) 
			
	
	-- outputs for reg_3(CERegisterImpl) 
	
		signal reg_3_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_3(CERegisterImpl) 
				
		signal reg_3_D : std_logic_vector(15 downto 0); 	
		signal reg_3_CE :  std_logic;
	
	-- outputs for mul_5:mul[16](BinaryOperatorImpl) 
	
		signal mul_5_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_5:mul[16](BinaryOperatorImpl) 
				
		signal mul_5_I0 : std_logic_vector(15 downto 0); 	
		signal mul_5_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_6:add[16](BinaryOperatorImpl) 
	
		signal add_6_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_6:add[16](BinaryOperatorImpl) 
				
		signal add_6_I0 : std_logic_vector(15 downto 0); 	
		signal add_6_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V51_C_4 (value: 51)(ConstantValueImpl) 
	
		signal C_V51_C_4_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V51_C_4 (value: 51)(ConstantValueImpl) 
			
	
	-- outputs for reg_4(CERegisterImpl) 
	
		signal reg_4_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_4(CERegisterImpl) 
				
		signal reg_4_D : std_logic_vector(15 downto 0); 	
		signal reg_4_CE :  std_logic;
	
	-- outputs for mul_7:mul[16](BinaryOperatorImpl) 
	
		signal mul_7_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_7:mul[16](BinaryOperatorImpl) 
				
		signal mul_7_I0 : std_logic_vector(15 downto 0); 	
		signal mul_7_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_8:add[16](BinaryOperatorImpl) 
	
		signal add_8_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_8:add[16](BinaryOperatorImpl) 
				
		signal add_8_I0 : std_logic_vector(15 downto 0); 	
		signal add_8_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V8_C_5 (value: 8)(ConstantValueImpl) 
	
		signal C_V8_C_5_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V8_C_5 (value: 8)(ConstantValueImpl) 
			
	
	-- outputs for reg_5(CERegisterImpl) 
	
		signal reg_5_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_5(CERegisterImpl) 
				
		signal reg_5_D : std_logic_vector(15 downto 0); 	
		signal reg_5_CE :  std_logic;
	
	-- outputs for mul_9:mul[16](BinaryOperatorImpl) 
	
		signal mul_9_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_9:mul[16](BinaryOperatorImpl) 
				
		signal mul_9_I0 : std_logic_vector(15 downto 0); 	
		signal mul_9_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_10:add[16](BinaryOperatorImpl) 
	
		signal add_10_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_10:add[16](BinaryOperatorImpl) 
				
		signal add_10_I0 : std_logic_vector(15 downto 0); 	
		signal add_10_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V3_C_6 (value: 3)(ConstantValueImpl) 
	
		signal C_V3_C_6_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V3_C_6 (value: 3)(ConstantValueImpl) 
			
	
	-- outputs for reg_6(CERegisterImpl) 
	
		signal reg_6_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_6(CERegisterImpl) 
				
		signal reg_6_D : std_logic_vector(15 downto 0); 	
		signal reg_6_CE :  std_logic;
	
	-- outputs for mul_11:mul[16](BinaryOperatorImpl) 
	
		signal mul_11_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_11:mul[16](BinaryOperatorImpl) 
				
		signal mul_11_I0 : std_logic_vector(15 downto 0); 	
		signal mul_11_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_12:add[16](BinaryOperatorImpl) 
	
		signal add_12_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_12:add[16](BinaryOperatorImpl) 
				
		signal add_12_I0 : std_logic_vector(15 downto 0); 	
		signal add_12_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V5_C_7 (value: 5)(ConstantValueImpl) 
	
		signal C_V5_C_7_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V5_C_7 (value: 5)(ConstantValueImpl) 
			
	
	-- outputs for reg_7(CERegisterImpl) 
	
		signal reg_7_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_7(CERegisterImpl) 
				
		signal reg_7_D : std_logic_vector(15 downto 0); 	
		signal reg_7_CE :  std_logic;
	
	-- outputs for mul_13:mul[16](BinaryOperatorImpl) 
	
		signal mul_13_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_13:mul[16](BinaryOperatorImpl) 
				
		signal mul_13_I0 : std_logic_vector(15 downto 0); 	
		signal mul_13_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_14:add[16](BinaryOperatorImpl) 
	
		signal add_14_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_14:add[16](BinaryOperatorImpl) 
				
		signal add_14_I0 : std_logic_vector(15 downto 0); 	
		signal add_14_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V8_C_8 (value: 8)(ConstantValueImpl) 
	
		signal C_V8_C_8_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V8_C_8 (value: 8)(ConstantValueImpl) 
			
	
	-- outputs for reg_8(CERegisterImpl) 
	
		signal reg_8_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_8(CERegisterImpl) 
				
		signal reg_8_D : std_logic_vector(15 downto 0); 	
		signal reg_8_CE :  std_logic;
	
	-- outputs for mul_15:mul[16](BinaryOperatorImpl) 
	
		signal mul_15_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_15:mul[16](BinaryOperatorImpl) 
				
		signal mul_15_I0 : std_logic_vector(15 downto 0); 	
		signal mul_15_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_16:add[16](BinaryOperatorImpl) 
	
		signal add_16_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_16:add[16](BinaryOperatorImpl) 
				
		signal add_16_I0 : std_logic_vector(15 downto 0); 	
		signal add_16_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V2_C_9 (value: 2)(ConstantValueImpl) 
	
		signal C_V2_C_9_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V2_C_9 (value: 2)(ConstantValueImpl) 
			
	
	-- outputs for reg_9(CERegisterImpl) 
	
		signal reg_9_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_9(CERegisterImpl) 
				
		signal reg_9_D : std_logic_vector(15 downto 0); 	
		signal reg_9_CE :  std_logic;
	
	-- outputs for mul_17:mul[16](BinaryOperatorImpl) 
	
		signal mul_17_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_17:mul[16](BinaryOperatorImpl) 
				
		signal mul_17_I0 : std_logic_vector(15 downto 0); 	
		signal mul_17_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_18:add[16](BinaryOperatorImpl) 
	
		signal add_18_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_18:add[16](BinaryOperatorImpl) 
				
		signal add_18_I0 : std_logic_vector(15 downto 0); 	
		signal add_18_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V3_C_10 (value: 3)(ConstantValueImpl) 
	
		signal C_V3_C_10_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V3_C_10 (value: 3)(ConstantValueImpl) 
			
	
	-- outputs for reg_10(CERegisterImpl) 
	
		signal reg_10_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_10(CERegisterImpl) 
				
		signal reg_10_D : std_logic_vector(15 downto 0); 	
		signal reg_10_CE :  std_logic;
	
	-- outputs for mul_19:mul[16](BinaryOperatorImpl) 
	
		signal mul_19_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_19:mul[16](BinaryOperatorImpl) 
				
		signal mul_19_I0 : std_logic_vector(15 downto 0); 	
		signal mul_19_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_20:add[16](BinaryOperatorImpl) 
	
		signal add_20_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_20:add[16](BinaryOperatorImpl) 
				
		signal add_20_I0 : std_logic_vector(15 downto 0); 	
		signal add_20_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V2_C_11 (value: 2)(ConstantValueImpl) 
	
		signal C_V2_C_11_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V2_C_11 (value: 2)(ConstantValueImpl) 
			
	
	-- outputs for reg_11(CERegisterImpl) 
	
		signal reg_11_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_11(CERegisterImpl) 
				
		signal reg_11_D : std_logic_vector(15 downto 0); 	
		signal reg_11_CE :  std_logic;
	
	-- outputs for mul_21:mul[16](BinaryOperatorImpl) 
	
		signal mul_21_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_21:mul[16](BinaryOperatorImpl) 
				
		signal mul_21_I0 : std_logic_vector(15 downto 0); 	
		signal mul_21_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_22:add[16](BinaryOperatorImpl) 
	
		signal add_22_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_22:add[16](BinaryOperatorImpl) 
				
		signal add_22_I0 : std_logic_vector(15 downto 0); 	
		signal add_22_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V2_C_12 (value: 2)(ConstantValueImpl) 
	
		signal C_V2_C_12_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V2_C_12 (value: 2)(ConstantValueImpl) 
			
	
	-- outputs for reg_12(CERegisterImpl) 
	
		signal reg_12_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_12(CERegisterImpl) 
				
		signal reg_12_D : std_logic_vector(15 downto 0); 	
		signal reg_12_CE :  std_logic;
	
	-- outputs for mul_23:mul[16](BinaryOperatorImpl) 
	
		signal mul_23_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_23:mul[16](BinaryOperatorImpl) 
				
		signal mul_23_I0 : std_logic_vector(15 downto 0); 	
		signal mul_23_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_24:add[16](BinaryOperatorImpl) 
	
		signal add_24_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_24:add[16](BinaryOperatorImpl) 
				
		signal add_24_I0 : std_logic_vector(15 downto 0); 	
		signal add_24_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V1_C_13 (value: 1)(ConstantValueImpl) 
	
		signal C_V1_C_13_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V1_C_13 (value: 1)(ConstantValueImpl) 
			
	
	-- outputs for reg_13(CERegisterImpl) 
	
		signal reg_13_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_13(CERegisterImpl) 
				
		signal reg_13_D : std_logic_vector(15 downto 0); 	
		signal reg_13_CE :  std_logic;
	
	-- outputs for mul_25:mul[16](BinaryOperatorImpl) 
	
		signal mul_25_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_25:mul[16](BinaryOperatorImpl) 
				
		signal mul_25_I0 : std_logic_vector(15 downto 0); 	
		signal mul_25_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_26:add[16](BinaryOperatorImpl) 
	
		signal add_26_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_26:add[16](BinaryOperatorImpl) 
				
		signal add_26_I0 : std_logic_vector(15 downto 0); 	
		signal add_26_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V4_C_14 (value: 4)(ConstantValueImpl) 
	
		signal C_V4_C_14_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V4_C_14 (value: 4)(ConstantValueImpl) 
			
	
	-- outputs for reg_14(CERegisterImpl) 
	
		signal reg_14_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_14(CERegisterImpl) 
				
		signal reg_14_D : std_logic_vector(15 downto 0); 	
		signal reg_14_CE :  std_logic;
	
	-- outputs for mul_27:mul[16](BinaryOperatorImpl) 
	
		signal mul_27_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_27:mul[16](BinaryOperatorImpl) 
				
		signal mul_27_I0 : std_logic_vector(15 downto 0); 	
		signal mul_27_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_28:add[16](BinaryOperatorImpl) 
	
		signal add_28_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_28:add[16](BinaryOperatorImpl) 
				
		signal add_28_I0 : std_logic_vector(15 downto 0); 	
		signal add_28_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V54_C_15 (value: 54)(ConstantValueImpl) 
	
		signal C_V54_C_15_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V54_C_15 (value: 54)(ConstantValueImpl) 
			
	
	-- outputs for reg_15(CERegisterImpl) 
	
		signal reg_15_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_15(CERegisterImpl) 
				
		signal reg_15_D : std_logic_vector(15 downto 0); 	
		signal reg_15_CE :  std_logic;
	
	-- outputs for mul_29:mul[16](BinaryOperatorImpl) 
	
		signal mul_29_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_29:mul[16](BinaryOperatorImpl) 
				
		signal mul_29_I0 : std_logic_vector(15 downto 0); 	
		signal mul_29_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_30:add[16](BinaryOperatorImpl) 
	
		signal add_30_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_30:add[16](BinaryOperatorImpl) 
				
		signal add_30_I0 : std_logic_vector(15 downto 0); 	
		signal add_30_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V35_C_16 (value: 35)(ConstantValueImpl) 
	
		signal C_V35_C_16_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V35_C_16 (value: 35)(ConstantValueImpl) 
			
	
	-- outputs for reg_16(CERegisterImpl) 
	
		signal reg_16_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_16(CERegisterImpl) 
				
		signal reg_16_D : std_logic_vector(15 downto 0); 	
		signal reg_16_CE :  std_logic;
	
	-- outputs for mul_31:mul[16](BinaryOperatorImpl) 
	
		signal mul_31_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_31:mul[16](BinaryOperatorImpl) 
				
		signal mul_31_I0 : std_logic_vector(15 downto 0); 	
		signal mul_31_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_32:add[16](BinaryOperatorImpl) 
	
		signal add_32_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_32:add[16](BinaryOperatorImpl) 
				
		signal add_32_I0 : std_logic_vector(15 downto 0); 	
		signal add_32_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V4354_C_17 (value: 4354)(ConstantValueImpl) 
	
		signal C_V4354_C_17_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V4354_C_17 (value: 4354)(ConstantValueImpl) 
			
	
	-- outputs for reg_17(CERegisterImpl) 
	
		signal reg_17_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_17(CERegisterImpl) 
				
		signal reg_17_D : std_logic_vector(15 downto 0); 	
		signal reg_17_CE :  std_logic;
	
	-- outputs for mul_33:mul[16](BinaryOperatorImpl) 
	
		signal mul_33_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_33:mul[16](BinaryOperatorImpl) 
				
		signal mul_33_I0 : std_logic_vector(15 downto 0); 	
		signal mul_33_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_34:add[16](BinaryOperatorImpl) 
	
		signal add_34_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_34:add[16](BinaryOperatorImpl) 
				
		signal add_34_I0 : std_logic_vector(15 downto 0); 	
		signal add_34_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V3_C_18 (value: 3)(ConstantValueImpl) 
	
		signal C_V3_C_18_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V3_C_18 (value: 3)(ConstantValueImpl) 
			
	
	-- outputs for reg_18(CERegisterImpl) 
	
		signal reg_18_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_18(CERegisterImpl) 
				
		signal reg_18_D : std_logic_vector(15 downto 0); 	
		signal reg_18_CE :  std_logic;
	
	-- outputs for mul_35:mul[16](BinaryOperatorImpl) 
	
		signal mul_35_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_35:mul[16](BinaryOperatorImpl) 
				
		signal mul_35_I0 : std_logic_vector(15 downto 0); 	
		signal mul_35_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_36:add[16](BinaryOperatorImpl) 
	
		signal add_36_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_36:add[16](BinaryOperatorImpl) 
				
		signal add_36_I0 : std_logic_vector(15 downto 0); 	
		signal add_36_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V35_C_19 (value: 35)(ConstantValueImpl) 
	
		signal C_V35_C_19_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V35_C_19 (value: 35)(ConstantValueImpl) 
			
	
	-- outputs for reg_19(CERegisterImpl) 
	
		signal reg_19_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_19(CERegisterImpl) 
				
		signal reg_19_D : std_logic_vector(15 downto 0); 	
		signal reg_19_CE :  std_logic;
	
	-- outputs for mul_37:mul[16](BinaryOperatorImpl) 
	
		signal mul_37_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_37:mul[16](BinaryOperatorImpl) 
				
		signal mul_37_I0 : std_logic_vector(15 downto 0); 	
		signal mul_37_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_38:add[16](BinaryOperatorImpl) 
	
		signal add_38_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_38:add[16](BinaryOperatorImpl) 
				
		signal add_38_I0 : std_logic_vector(15 downto 0); 	
		signal add_38_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V434_C_20 (value: 434)(ConstantValueImpl) 
	
		signal C_V434_C_20_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V434_C_20 (value: 434)(ConstantValueImpl) 
			
	
	-- outputs for reg_20(CERegisterImpl) 
	
		signal reg_20_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_20(CERegisterImpl) 
				
		signal reg_20_D : std_logic_vector(15 downto 0); 	
		signal reg_20_CE :  std_logic;
	
	-- outputs for mul_39:mul[16](BinaryOperatorImpl) 
	
		signal mul_39_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_39:mul[16](BinaryOperatorImpl) 
				
		signal mul_39_I0 : std_logic_vector(15 downto 0); 	
		signal mul_39_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_40:add[16](BinaryOperatorImpl) 
	
		signal add_40_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_40:add[16](BinaryOperatorImpl) 
				
		signal add_40_I0 : std_logic_vector(15 downto 0); 	
		signal add_40_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V34_C_21 (value: 34)(ConstantValueImpl) 
	
		signal C_V34_C_21_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V34_C_21 (value: 34)(ConstantValueImpl) 
			
	
	-- outputs for reg_21(CERegisterImpl) 
	
		signal reg_21_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_21(CERegisterImpl) 
				
		signal reg_21_D : std_logic_vector(15 downto 0); 	
		signal reg_21_CE :  std_logic;
	
	-- outputs for mul_41:mul[16](BinaryOperatorImpl) 
	
		signal mul_41_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_41:mul[16](BinaryOperatorImpl) 
				
		signal mul_41_I0 : std_logic_vector(15 downto 0); 	
		signal mul_41_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_42:add[16](BinaryOperatorImpl) 
	
		signal add_42_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_42:add[16](BinaryOperatorImpl) 
				
		signal add_42_I0 : std_logic_vector(15 downto 0); 	
		signal add_42_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V3_C_22 (value: 3)(ConstantValueImpl) 
	
		signal C_V3_C_22_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V3_C_22 (value: 3)(ConstantValueImpl) 
			
	
	-- outputs for reg_22(CERegisterImpl) 
	
		signal reg_22_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_22(CERegisterImpl) 
				
		signal reg_22_D : std_logic_vector(15 downto 0); 	
		signal reg_22_CE :  std_logic;
	
	-- outputs for mul_43:mul[16](BinaryOperatorImpl) 
	
		signal mul_43_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_43:mul[16](BinaryOperatorImpl) 
				
		signal mul_43_I0 : std_logic_vector(15 downto 0); 	
		signal mul_43_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_44:add[16](BinaryOperatorImpl) 
	
		signal add_44_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_44:add[16](BinaryOperatorImpl) 
				
		signal add_44_I0 : std_logic_vector(15 downto 0); 	
		signal add_44_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for C_V434_C_23 (value: 434)(ConstantValueImpl) 
	
		signal C_V434_C_23_O : std_logic_vector(15 downto 0); 
			
	-- inputs for C_V434_C_23 (value: 434)(ConstantValueImpl) 
			
	
	-- outputs for reg_23(CERegisterImpl) 
	
		signal reg_23_Q : std_logic_vector(15 downto 0); 
			
	-- inputs for reg_23(CERegisterImpl) 
				
		signal reg_23_D : std_logic_vector(15 downto 0); 	
		signal reg_23_CE :  std_logic;
	
	-- outputs for mul_45:mul[16](BinaryOperatorImpl) 
	
		signal mul_45_O : std_logic_vector(15 downto 0); 
			
	-- inputs for mul_45:mul[16](BinaryOperatorImpl) 
				
		signal mul_45_I0 : std_logic_vector(15 downto 0); 	
		signal mul_45_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for add_46:add[16](BinaryOperatorImpl) 
	
		signal add_46_O : std_logic_vector(15 downto 0); 
			
	-- inputs for add_46:add[16](BinaryOperatorImpl) 
				
		signal add_46_I0 : std_logic_vector(15 downto 0); 	
		signal add_46_I1 : std_logic_vector(15 downto 0); 
	
	-- outputs for sel_op47:sel[15:0](BitSelectImpl) 
	
		signal sel_op47_O : std_logic_vector(15 downto 0); 
			
	-- inputs for sel_op47:sel[15:0](BitSelectImpl) 
				
		signal sel_op47_I : std_logic_vector(15 downto 0); 



begin

	X_X <= X ;
			
	CE_CE <= CE ;
			
	Y <= Y_Y ;
			
	
 
		mul_0_I0 <= X_X;
		mul_0_I1 <= C_V1_C_0_O;
		reg_1_D <= X_X;
		mul_1_I0 <= reg_1_Q;
		mul_1_I1 <= C_V17_C_1_O;
		add_2_I0 <= mul_1_O;
		add_2_I1 <= mul_0_O;
		reg_2_D <= reg_1_Q;
		mul_3_I0 <= reg_2_Q;
		mul_3_I1 <= C_V21_C_2_O;
		add_4_I0 <= mul_3_O;
		add_4_I1 <= add_2_O;
		reg_3_D <= reg_2_Q;
		mul_5_I0 <= reg_3_Q;
		mul_5_I1 <= C_V32_C_3_O;
		add_6_I0 <= mul_5_O;
		add_6_I1 <= add_4_O;
		reg_4_D <= reg_3_Q;
		mul_7_I0 <= reg_4_Q;
		mul_7_I1 <= C_V51_C_4_O;
		add_8_I0 <= mul_7_O;
		add_8_I1 <= add_6_O;
		reg_5_D <= reg_4_Q;
		mul_9_I0 <= reg_5_Q;
		mul_9_I1 <= C_V8_C_5_O;
		add_10_I0 <= mul_9_O;
		add_10_I1 <= add_8_O;
		reg_6_D <= reg_5_Q;
		mul_11_I0 <= reg_6_Q;
		mul_11_I1 <= C_V3_C_6_O;
		add_12_I0 <= mul_11_O;
		add_12_I1 <= add_10_O;
		reg_7_D <= reg_6_Q;
		mul_13_I0 <= reg_7_Q;
		mul_13_I1 <= C_V5_C_7_O;
		add_14_I0 <= mul_13_O;
		add_14_I1 <= add_12_O;
		reg_8_D <= reg_7_Q;
		mul_15_I0 <= reg_8_Q;
		mul_15_I1 <= C_V8_C_8_O;
		add_16_I0 <= mul_15_O;
		add_16_I1 <= add_14_O;
		reg_9_D <= reg_8_Q;
		mul_17_I0 <= reg_9_Q;
		mul_17_I1 <= C_V2_C_9_O;
		add_18_I0 <= mul_17_O;
		add_18_I1 <= add_16_O;
		reg_10_D <= reg_9_Q;
		mul_19_I0 <= reg_10_Q;
		mul_19_I1 <= C_V3_C_10_O;
		add_20_I0 <= mul_19_O;
		add_20_I1 <= add_18_O;
		reg_11_D <= reg_10_Q;
		mul_21_I0 <= reg_11_Q;
		mul_21_I1 <= C_V2_C_11_O;
		add_22_I0 <= mul_21_O;
		add_22_I1 <= add_20_O;
		reg_12_D <= reg_11_Q;
		mul_23_I0 <= reg_12_Q;
		mul_23_I1 <= C_V2_C_12_O;
		add_24_I0 <= mul_23_O;
		add_24_I1 <= add_22_O;
		reg_13_D <= reg_12_Q;
		mul_25_I0 <= reg_13_Q;
		mul_25_I1 <= C_V1_C_13_O;
		add_26_I0 <= mul_25_O;
		add_26_I1 <= add_24_O;
		reg_14_D <= reg_13_Q;
		mul_27_I0 <= reg_14_Q;
		mul_27_I1 <= C_V4_C_14_O;
		add_28_I0 <= mul_27_O;
		add_28_I1 <= add_26_O;
		reg_15_D <= reg_14_Q;
		mul_29_I0 <= reg_15_Q;
		mul_29_I1 <= C_V54_C_15_O;
		add_30_I0 <= mul_29_O;
		add_30_I1 <= add_28_O;
		reg_16_D <= reg_15_Q;
		mul_31_I0 <= reg_16_Q;
		mul_31_I1 <= C_V35_C_16_O;
		add_32_I0 <= mul_31_O;
		add_32_I1 <= add_30_O;
		reg_17_D <= reg_16_Q;
		mul_33_I0 <= reg_17_Q;
		mul_33_I1 <= C_V4354_C_17_O;
		add_34_I0 <= mul_33_O;
		add_34_I1 <= add_32_O;
		reg_18_D <= reg_17_Q;
		mul_35_I0 <= reg_18_Q;
		mul_35_I1 <= C_V3_C_18_O;
		add_36_I0 <= mul_35_O;
		add_36_I1 <= add_34_O;
		reg_19_D <= reg_18_Q;
		mul_37_I0 <= reg_19_Q;
		mul_37_I1 <= C_V35_C_19_O;
		add_38_I0 <= mul_37_O;
		add_38_I1 <= add_36_O;
		reg_20_D <= reg_19_Q;
		mul_39_I0 <= reg_20_Q;
		mul_39_I1 <= C_V434_C_20_O;
		add_40_I0 <= mul_39_O;
		add_40_I1 <= add_38_O;
		reg_21_D <= reg_20_Q;
		mul_41_I0 <= reg_21_Q;
		mul_41_I1 <= C_V34_C_21_O;
		add_42_I0 <= mul_41_O;
		add_42_I1 <= add_40_O;
		reg_22_D <= reg_21_Q;
		mul_43_I0 <= reg_22_Q;
		mul_43_I1 <= C_V3_C_22_O;
		add_44_I0 <= mul_43_O;
		add_44_I1 <= add_42_O;
		reg_23_D <= reg_22_Q;
		mul_45_I0 <= reg_23_Q;
		mul_45_I1 <= C_V434_C_23_O;
		add_46_I0 <= mul_45_O;
		add_46_I1 <= add_44_O;
		sel_op47_I <= add_46_O;
		Y_Y <= sel_op47_O;
	reg_1_CE <= CE_CE;
	reg_2_CE <= CE_CE;
	reg_3_CE <= CE_CE;
	reg_4_CE <= CE_CE;
	reg_5_CE <= CE_CE;
	reg_6_CE <= CE_CE;
	reg_7_CE <= CE_CE;
	reg_8_CE <= CE_CE;
	reg_9_CE <= CE_CE;
	reg_10_CE <= CE_CE;
	reg_11_CE <= CE_CE;
	reg_12_CE <= CE_CE;
	reg_13_CE <= CE_CE;
	reg_14_CE <= CE_CE;
	reg_15_CE <= CE_CE;
	reg_16_CE <= CE_CE;
	reg_17_CE <= CE_CE;
	reg_18_CE <= CE_CE;
	reg_19_CE <= CE_CE;
	reg_20_CE <= CE_CE;
	reg_21_CE <= CE_CE;
	reg_22_CE <= CE_CE;
	reg_23_CE <= CE_CE;



	   
	-- Operator mapping 
 	
	C_V1_C_0_O <= "1111111111111111";	
	mul_0_O <= X_X * C_V1_C_0_O;	
	C_V17_C_1_O <= "1000111111111111";	
	mul_1_O <= reg_1_Q * C_V17_C_1_O;	
	add_2_O <= mul_1_O + mul_0_O;	
	C_V21_C_2_O <= "1010111111111111";	
	mul_3_O <= reg_2_Q * C_V21_C_2_O;	
	add_4_O <= mul_3_O + add_2_O;	
	C_V32_C_3_O <= "1000000000000000";	
	mul_5_O <= reg_3_Q * C_V32_C_3_O;	
	add_6_O <= mul_5_O + add_4_O;	
	C_V51_C_4_O <= "1100111111111111";	
	mul_7_O <= reg_4_Q * C_V51_C_4_O;	
	add_8_O <= mul_7_O + add_6_O;	
	C_V8_C_5_O <= "1000000000000000";	
	mul_9_O <= reg_5_Q * C_V8_C_5_O;	
	add_10_O <= mul_9_O + add_8_O;	
	C_V3_C_6_O <= "1111111111111111";	
	mul_11_O <= reg_6_Q * C_V3_C_6_O;	
	add_12_O <= mul_11_O + add_10_O;	
	C_V5_C_7_O <= "1011111111111111";	
	mul_13_O <= reg_7_Q * C_V5_C_7_O;	
	add_14_O <= mul_13_O + add_12_O;	
	C_V8_C_8_O <= "1000000000000000";	
	mul_15_O <= reg_8_Q * C_V8_C_8_O;	
	add_16_O <= mul_15_O + add_14_O;	
	C_V2_C_9_O <= "1000000000000000";	
	mul_17_O <= reg_9_Q * C_V2_C_9_O;	
	add_18_O <= mul_17_O + add_16_O;	
	C_V3_C_10_O <= "1111111111111111";	
	mul_19_O <= reg_10_Q * C_V3_C_10_O;	
	add_20_O <= mul_19_O + add_18_O;	
	C_V2_C_11_O <= "1000000000000000";	
	mul_21_O <= reg_11_Q * C_V2_C_11_O;	
	add_22_O <= mul_21_O + add_20_O;	
	C_V2_C_12_O <= "1000000000000000";	
	mul_23_O <= reg_12_Q * C_V2_C_12_O;	
	add_24_O <= mul_23_O + add_22_O;	
	C_V1_C_13_O <= "1111111111111111";	
	mul_25_O <= reg_13_Q * C_V1_C_13_O;	
	add_26_O <= mul_25_O + add_24_O;	
	C_V4_C_14_O <= "1000000000000000";	
	mul_27_O <= reg_14_Q * C_V4_C_14_O;	
	add_28_O <= mul_27_O + add_26_O;	
	C_V54_C_15_O <= "1101100000000000";	
	mul_29_O <= reg_15_Q * C_V54_C_15_O;	
	add_30_O <= mul_29_O + add_28_O;	
	C_V35_C_16_O <= "1000111111111111";	
	mul_31_O <= reg_16_Q * C_V35_C_16_O;	
	add_32_O <= mul_31_O + add_30_O;	
	C_V4354_C_17_O <= "1000100000010000";	
	mul_33_O <= reg_17_Q * C_V4354_C_17_O;	
	add_34_O <= mul_33_O + add_32_O;	
	C_V3_C_18_O <= "1111111111111111";	
	mul_35_O <= reg_18_Q * C_V3_C_18_O;	
	add_36_O <= mul_35_O + add_34_O;	
	C_V35_C_19_O <= "1000111111111111";	
	mul_37_O <= reg_19_Q * C_V35_C_19_O;	
	add_38_O <= mul_37_O + add_36_O;	
	C_V434_C_20_O <= "1101100100000000";	
	mul_39_O <= reg_20_Q * C_V434_C_20_O;	
	add_40_O <= mul_39_O + add_38_O;	
	C_V34_C_21_O <= "1000100000000000";	
	mul_41_O <= reg_21_Q * C_V34_C_21_O;	
	add_42_O <= mul_41_O + add_40_O;	
	C_V3_C_22_O <= "1111111111111111";	
	mul_43_O <= reg_22_Q * C_V3_C_22_O;	
	add_44_O <= mul_43_O + add_42_O;	
	C_V434_C_23_O <= "1101100100000000";	
	mul_45_O <= reg_23_Q * C_V434_C_23_O;	
	add_46_O <= mul_45_O + add_44_O;	
	sel_op47_O <= add_46_O (15 downto 0);


	

	all_regs : process(rst,clk)

	begin
		if rst='1' then
 			reg_1_Q <= (others =>'0'); 
			reg_2_Q <= (others =>'0'); 
			reg_3_Q <= (others =>'0'); 
			reg_4_Q <= (others =>'0'); 
			reg_5_Q <= (others =>'0'); 
			reg_6_Q <= (others =>'0'); 
			reg_7_Q <= (others =>'0'); 
			reg_8_Q <= (others =>'0'); 
			reg_9_Q <= (others =>'0'); 
			reg_10_Q <= (others =>'0'); 
			reg_11_Q <= (others =>'0'); 
			reg_12_Q <= (others =>'0'); 
			reg_13_Q <= (others =>'0'); 
			reg_14_Q <= (others =>'0'); 
			reg_15_Q <= (others =>'0'); 
			reg_16_Q <= (others =>'0'); 
			reg_17_Q <= (others =>'0'); 
			reg_18_Q <= (others =>'0'); 
			reg_19_Q <= (others =>'0'); 
			reg_20_Q <= (others =>'0'); 
			reg_21_Q <= (others =>'0'); 
			reg_22_Q <= (others =>'0'); 
			reg_23_Q <= (others =>'0'); 

		elsif rising_edge(clk) then
		
			if CE_CE='1' then 
				reg_1_Q <= X_X;
			end if;

			if CE_CE='1' then 
				reg_2_Q <= reg_1_Q;
			end if;

			if CE_CE='1' then 
				reg_3_Q <= reg_2_Q;
			end if;

			if CE_CE='1' then 
				reg_4_Q <= reg_3_Q;
			end if;

			if CE_CE='1' then 
				reg_5_Q <= reg_4_Q;
			end if;

			if CE_CE='1' then 
				reg_6_Q <= reg_5_Q;
			end if;

			if CE_CE='1' then 
				reg_7_Q <= reg_6_Q;
			end if;

			if CE_CE='1' then 
				reg_8_Q <= reg_7_Q;
			end if;

			if CE_CE='1' then 
				reg_9_Q <= reg_8_Q;
			end if;

			if CE_CE='1' then 
				reg_10_Q <= reg_9_Q;
			end if;

			if CE_CE='1' then 
				reg_11_Q <= reg_10_Q;
			end if;

			if CE_CE='1' then 
				reg_12_Q <= reg_11_Q;
			end if;

			if CE_CE='1' then 
				reg_13_Q <= reg_12_Q;
			end if;

			if CE_CE='1' then 
				reg_14_Q <= reg_13_Q;
			end if;

			if CE_CE='1' then 
				reg_15_Q <= reg_14_Q;
			end if;

			if CE_CE='1' then 
				reg_16_Q <= reg_15_Q;
			end if;

			if CE_CE='1' then 
				reg_17_Q <= reg_16_Q;
			end if;

			if CE_CE='1' then 
				reg_18_Q <= reg_17_Q;
			end if;

			if CE_CE='1' then 
				reg_19_Q <= reg_18_Q;
			end if;

			if CE_CE='1' then 
				reg_20_Q <= reg_19_Q;
			end if;

			if CE_CE='1' then 
				reg_21_Q <= reg_20_Q;
			end if;

			if CE_CE='1' then 
				reg_22_Q <= reg_21_Q;
			end if;

			if CE_CE='1' then 
				reg_23_Q <= reg_22_Q;
			end if;
 	
		end if;
	end process;

	
end RTL;
