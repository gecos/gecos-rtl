library ieee;
library work;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FIR2_8 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Data Pads
	X : in std_logic_vector(7 downto 0);
	-- Output Data Pads
	Y : out std_logic_vector(7 downto 0)
	)
;
end FIR2_8;

architecture RTL of FIR2_8 is

	-- Components 




	



	




	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal X_X : std_logic_vector(7 downto 0) ; 

	signal Y_Y : std_logic_vector(7 downto 0) ; 

	signal cpt_D : std_logic_vector(7 downto 0) ; 
	signal cpt_Q : std_logic_vector(7 downto 0) ; 

		
	signal add_9_I0 : std_logic_vector(7 downto 0) ; 
	signal add_9_I1 : std_logic_vector(7 downto 0) ; 
	signal add_9_O : std_logic_vector(7 downto 0) ; 

	signal C_V0_op10_O : std_logic_vector(7 downto 0) ; 

	signal cmux_op11_I0 : std_logic_vector(7 downto 0) ; 
	signal cmux_op11_I1 : std_logic_vector(7 downto 0) ; 
	signal cmux_op11_O : std_logic_vector(7 downto 0) ; 

	signal adx_D : std_logic_vector(7 downto 0) ; 
	signal adx_Q : std_logic_vector(7 downto 0) ; 

		
	signal add_13_I0 : std_logic_vector(7 downto 0) ; 
	signal add_13_I1 : std_logic_vector(7 downto 0) ; 
	signal add_13_O : std_logic_vector(7 downto 0) ; 

		
	signal sub_15_I0 : std_logic_vector(7 downto 0) ; 
	signal sub_15_I1 : std_logic_vector(7 downto 0) ; 
	signal sub_15_O : std_logic_vector(7 downto 0) ; 

	signal cmux_op16_I0 : std_logic_vector(7 downto 0) ; 
	signal cmux_op16_I1 : std_logic_vector(7 downto 0) ; 
	signal cmux_op16_O : std_logic_vector(7 downto 0) ; 

		
	signal mul_reg_D : std_logic_vector(15 downto 0) ; 
	signal mul_reg_Q : std_logic_vector(15 downto 0) ; 

	signal acc_reg_D : std_logic_vector(15 downto 0) ; 
	signal acc_reg_Q : std_logic_vector(15 downto 0) ; 

		
		
	signal CRom_address : std_logic_vector(7 downto 0) ; 
	signal CRom_dataOut : std_logic_vector(7 downto 0) ; 

	signal XRam_address : std_logic_vector(7 downto 0) ; 
	signal XRam_dataIn : std_logic_vector(7 downto 0) ; 
	signal XRam_dataOut : std_logic_vector(7 downto 0) ; 

	
	signal fir_ctrl_endLoop : std_logic ; 
	signal fir_ctrl_inEmpty : std_logic ; 
	signal fir_ctrl_outFull : std_logic ; 
	signal cpt_CE : std_logic ; 
	signal cmux_op11_S : std_logic ; 
	signal adx_CE : std_logic ; 
	signal cmux_op16_S : std_logic ; 
	signal mul_reg_CE : std_logic ; 
	signal acc_reg_CE : std_logic ; 
	signal CRom_re : std_logic ; 
	signal XRam_re : std_logic ; 
	signal XRam_we : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal fir_ctrl_fifo_rd : std_logic ; 
	signal fir_ctrl_xwr_en : std_logic ; 
	signal fir_ctrl_xrd_en : std_logic ; 
	signal fir_ctrl_adx_en : std_logic ; 
	signal fir_ctrl_crd_en : std_logic ; 
	signal fir_ctrl_cpt_en : std_logic ; 
	signal fir_ctrl_cpt_init : std_logic ; 
	signal fir_ctrl_mul_en : std_logic ; 
	signal fir_ctrl_acc_en : std_logic ; 
	signal fir_ctrl_fifo_wr : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
	
		signal Y_Y_int : std_logic_vector(15 downto 0) ;
		
	


component fir_ctrl port
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	endLoop : in std_logic;
	inEmpty : in std_logic;
	outFull : in std_logic;
	-- Output Control Pads
	fifo_rd : out std_logic;
	xwr_en : out std_logic;
	xrd_en : out std_logic;
	adx_en : out std_logic;
	crd_en : out std_logic;
	cpt_en : out std_logic;
	cpt_init : out std_logic;
	mul_en : out std_logic;
	acc_en : out std_logic;
	fifo_wr : out std_logic
	)
;
end component;

component CRom port
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	re : in std_logic;
	-- Input Data Pads
	address : in std_logic_vector(7 downto 0);
	-- Output Data Pads
	dataOut : out std_logic_vector(7 downto 0)
	)
;
end component;

component XRam port
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	re : in std_logic;
	we : in std_logic;
	-- Input Data Pads
	address : in std_logic_vector(7 downto 0);
	dataIn : in std_logic_vector(7 downto 0);
	-- Output Data Pads
	dataOut : out std_logic_vector(7 downto 0)
	)
;
end component;
begin
	

	

	inst_fir_ctrl : fir_ctrl port map(
	
	
	--global signals
			clk,
			rst
	
,
	--input control ports
			
			fir_ctrl_endLoop			,
			fir_ctrl_inEmpty			,
			fir_ctrl_outFull			
		
	
,
	--output control ports
			
			fir_ctrl_fifo_rd			,
			fir_ctrl_xwr_en			,
			fir_ctrl_xrd_en			,
			fir_ctrl_adx_en			,
			fir_ctrl_crd_en			,
			fir_ctrl_cpt_en			,
			fir_ctrl_cpt_init			,
			fir_ctrl_mul_en			,
			fir_ctrl_acc_en			,
			fir_ctrl_fifo_wr			
		
	
		
	

  	);
	


	--optimized for C_V1_op8
	
add_9_O <=  cpt_Q + ( "11111111" ) ;
	C_V0_op10_O <= "00000000";
	cmux_op11_O <= add_9_O when fir_ctrl_cpt_init= '0'	 else C_V0_op10_O;
	


	--optimized for C_V1_op12
	
add_13_O <=  cpt_Q + ( "11111111" ) ;

	--optimized for C_V1_op14
	
sub_15_O <=  cpt_Q - ( "11111111" ) ;
	cmux_op16_O <= add_13_O when fir_ctrl_adx_en= '0'	 else sub_15_O;

	--optimized for op17
	
	

	


	--optimized for acc_adder
	

	--optimized for sel_op19
	
	inst_CRom : CRom port map(
	
	
	--global signals
			clk,
			rst
	
,
	--input control ports
			
			CRom_re			
		
	
	
,
	--input data ports
			
			CRom_address			
		
,
	--output data ports
			
			CRom_dataOut			
		
		
	

  	);
	inst_XRam : XRam port map(
	
	
	--global signals
			clk,
			rst
	
,
	--input control ports
			
			XRam_re			,
			XRam_we			
		
	
	
,
	--input data ports
			
			XRam_address			,
			XRam_dataIn			
		
,
	--output data ports
			
			XRam_dataOut			
		
		
	

  	);


	-- Pads
	X_X<=X;	Y<=Y_Y;
	-- DWires
	add_9_I0 <= cpt_Q;
	cmux_op11_I0 <= add_9_O;
	cmux_op11_I1 <= C_V0_op10_O;
	cpt_D <= cmux_op11_O;
	add_13_I0 <= cpt_Q;
	sub_15_I0 <= cpt_Q;
	cmux_op16_I0 <= add_13_O;
	cmux_op16_I1 <= sub_15_O;
	adx_D <= cmux_op16_O;
	CRom_address <= cpt_Q;
	XRam_address <= adx_Q;
	XRam_dataIn <= X_X;

	-- CWires
	cpt_CE <= fir_ctrl_cpt_en;
	cmux_op11_S <= fir_ctrl_cpt_init;
	adx_CE <= fir_ctrl_adx_en;
	cmux_op16_S <= fir_ctrl_adx_en;
	mul_reg_CE <= fir_ctrl_mul_en;
	acc_reg_CE <= fir_ctrl_acc_en;
	CRom_re <= fir_ctrl_crd_en;
	XRam_we <= fir_ctrl_xwr_en;
	XRam_re <= fir_ctrl_xrd_en;


	
	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	cpt_Q <= (others => '0');

	adx_Q <= (others => '0');

	mul_reg_Q <= (others => '0');

	acc_reg_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (fir_ctrl_cpt_en='1') then
			
			--could not optimize
			cpt_Q <= cmux_op11_O;
			
	end if;

	if (fir_ctrl_adx_en='1') then
			
			--could not optimize
			adx_Q <= cmux_op16_O;
			
	end if;

	if (fir_ctrl_mul_en='1') then
				
					mul_reg_Q <=  CRom_dataOut * XRam_dataOut ;
				
			
	end if;

	if (fir_ctrl_acc_en='1') then
				
					acc_reg_Q <=  mul_reg_Q + acc_reg_Q ;
				
			
	end if;

		end if;
	end process;
	
	
	
	

	
	 
		
			Y_Y_int <=  acc_reg_Q ;
			Y_Y <= Y_Y_int (7 downto 0);
		
	

	

end RTL;
