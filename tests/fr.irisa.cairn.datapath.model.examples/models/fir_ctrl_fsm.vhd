library ieee;
library work;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fir_ctrl is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	endLoop : in std_logic;
	inEmpty : in std_logic;
	outFull : in std_logic;
	-- Output Control Pads
	fifo_rd : out std_logic;
	xwr_en : out std_logic;
	xrd_en : out std_logic;
	adx_en : out std_logic;
	crd_en : out std_logic;
	cpt_en : out std_logic;
	cpt_init : out std_logic;
	mul_en : out std_logic;
	acc_en : out std_logic;
	fifo_wr : out std_logic
	)
;
end fir_ctrl;


architecture RTL of fir_ctrl is

	type T_Fir_ctrl is (
idle,waitNotEmpty,readFIFO,step1,step2,steady,flush1,flush2,waitsState1,writeFIFO
	);

	signal ns,cs : T_Fir_ctrl;

	constant ONE : std_logic:='1';
	constant ZERO : std_logic:='0';
	
	
begin

	SYNC: process(clk,rst) 
	begin
		if (rst='1') then
			cs <= idle;
		elsif rising_edge(clk) then
			cs <= ns;
		end if; 
	end process;

	STATE_TRANSITIONS: process(cs,endLoop,inEmpty,outFull) 
	begin
		case cs is
	when idle => 
	
			
		--Predicate 1
		
		ns  <= waitNotEmpty ;
		
	
		
	
	when waitNotEmpty => 
	
			
		--Predicate !inEmpty
		
		if (inEmpty='0') then
		   ns  <= readFIFO ;
		end if;
		
	
			
		--Predicate !(!(inEmpty))
		
		if (not(inEmpty='0')) then
		   ns  <= waitNotEmpty ;
		end if;
		
	
		
	
	when readFIFO => 
	
			
		--Predicate 1
		
		ns  <= step1 ;
		
	
		
	
	when step1 => 
	
			
		--Predicate 1
		
		ns  <= step2 ;
		
	
		
	
	when step2 => 
	
			
		--Predicate 1
		
		ns  <= steady ;
		
	
		
	
	when steady => 
	
			
		--Predicate endLoop
		
		if (endLoop='1') then
		   ns  <= flush1 ;
		end if;
		
	
			
		--Predicate !(endLoop)
		
		if (not(endLoop='1')) then
		   ns  <= steady ;
		end if;
		
	
		
	
	when flush1 => 
	
			
		--Predicate 1
		
		ns  <= flush2 ;
		
	
		
	
	when flush2 => 
	
			
		--Predicate 1
		
		ns  <= waitsState1 ;
		
	
		
	
	when waitsState1 => 
	
			
		--Predicate !outFull
		
		if (outFull='0') then
		   ns  <= writeFIFO ;
		end if;
		
	
			
		--Predicate !(!(outFull))
		
		if (not(outFull='0')) then
		   ns  <= waitsState1 ;
		end if;
		
	
		
	
	when writeFIFO => 
	
			
		--Predicate 1
		
		ns  <= idle ;
		
	
		
	
			
			when others =>
				null;
		end case;
	end process;

	STATE_COMMANDS: process(cs,endLoop,inEmpty,outFull) 
	begin
		
		case cs is
				
	when idle => 
	
				
	when waitNotEmpty => 
	
				
	when readFIFO => 
		
			
				xwr_en <= '1';
				
			
		
	
		
			
				fifo_rd <= '1';
				
			
		
	
	
				
	when step1 => 
		
			
				crd_en <= '1';
				
			
		
	
		
			
				cpt_en <= '1';
				
			
		
	
	
				
	when step2 => 
		
			
				mul_en <= '1';
				
			
		
	
		
			
				xrd_en <= '1';
				
			
		
	
		
			
				crd_en <= '1';
				
			
		
	
		
			
				cpt_en <= '1';
				
			
		
	
	
				
	when steady => 
		
			
				acc_en <= '1';
				
			
		
	
		
			
				mul_en <= '1';
				
			
		
	
		
			
				xrd_en <= '1';
				
			
		
	
		
			
				crd_en <= '1';
				
			
		
	
		
			
				cpt_en <= '1';
				
			
		
	
	
				
	when flush1 => 
		
			
				acc_en <= '1';
				
			
		
	
		
			
				mul_en <= '1';
				
			
		
	
	
				
	when flush2 => 
		
			
				acc_en <= '1';
				
			
		
	
		
			
				mul_en <= '1';
				
			
		
	
	
				
	when waitsState1 => 
	
				
	when writeFIFO => 
		
			
				fifo_wr <= '1';
				
			
		
	
	
			when others =>
				null;
		end case;
	end process;
end RTL;
