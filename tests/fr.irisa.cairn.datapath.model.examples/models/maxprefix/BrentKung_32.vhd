library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity BrentKung_32 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end BrentKung_32;

architecture RTL of BrentKung_32 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal inreg8_D : std_logic_vector(15 downto 0) ; 
	signal inreg8_Q : std_logic_vector(15 downto 0) ; 

	signal inreg9_D : std_logic_vector(15 downto 0) ; 
	signal inreg9_Q : std_logic_vector(15 downto 0) ; 

	signal inreg10_D : std_logic_vector(15 downto 0) ; 
	signal inreg10_Q : std_logic_vector(15 downto 0) ; 

	signal inreg11_D : std_logic_vector(15 downto 0) ; 
	signal inreg11_Q : std_logic_vector(15 downto 0) ; 

	signal inreg12_D : std_logic_vector(15 downto 0) ; 
	signal inreg12_Q : std_logic_vector(15 downto 0) ; 

	signal inreg13_D : std_logic_vector(15 downto 0) ; 
	signal inreg13_Q : std_logic_vector(15 downto 0) ; 

	signal inreg14_D : std_logic_vector(15 downto 0) ; 
	signal inreg14_Q : std_logic_vector(15 downto 0) ; 

	signal inreg15_D : std_logic_vector(15 downto 0) ; 
	signal inreg15_Q : std_logic_vector(15 downto 0) ; 

	signal inreg16_D : std_logic_vector(15 downto 0) ; 
	signal inreg16_Q : std_logic_vector(15 downto 0) ; 

	signal inreg17_D : std_logic_vector(15 downto 0) ; 
	signal inreg17_Q : std_logic_vector(15 downto 0) ; 

	signal inreg18_D : std_logic_vector(15 downto 0) ; 
	signal inreg18_Q : std_logic_vector(15 downto 0) ; 

	signal inreg19_D : std_logic_vector(15 downto 0) ; 
	signal inreg19_Q : std_logic_vector(15 downto 0) ; 

	signal inreg20_D : std_logic_vector(15 downto 0) ; 
	signal inreg20_Q : std_logic_vector(15 downto 0) ; 

	signal inreg21_D : std_logic_vector(15 downto 0) ; 
	signal inreg21_Q : std_logic_vector(15 downto 0) ; 

	signal inreg22_D : std_logic_vector(15 downto 0) ; 
	signal inreg22_Q : std_logic_vector(15 downto 0) ; 

	signal inreg23_D : std_logic_vector(15 downto 0) ; 
	signal inreg23_Q : std_logic_vector(15 downto 0) ; 

	signal inreg24_D : std_logic_vector(15 downto 0) ; 
	signal inreg24_Q : std_logic_vector(15 downto 0) ; 

	signal inreg25_D : std_logic_vector(15 downto 0) ; 
	signal inreg25_Q : std_logic_vector(15 downto 0) ; 

	signal inreg26_D : std_logic_vector(15 downto 0) ; 
	signal inreg26_Q : std_logic_vector(15 downto 0) ; 

	signal inreg27_D : std_logic_vector(15 downto 0) ; 
	signal inreg27_Q : std_logic_vector(15 downto 0) ; 

	signal inreg28_D : std_logic_vector(15 downto 0) ; 
	signal inreg28_Q : std_logic_vector(15 downto 0) ; 

	signal inreg29_D : std_logic_vector(15 downto 0) ; 
	signal inreg29_Q : std_logic_vector(15 downto 0) ; 

	signal inreg30_D : std_logic_vector(15 downto 0) ; 
	signal inreg30_Q : std_logic_vector(15 downto 0) ; 

	signal inreg31_D : std_logic_vector(15 downto 0) ; 
	signal inreg31_Q : std_logic_vector(15 downto 0) ; 

	signal max_211_I0 : std_logic_vector(15 downto 0) ; 
	signal max_211_I1 : std_logic_vector(15 downto 0) ; 
	signal max_211_O : std_logic_vector(15 downto 0) ; 

	signal max_212_I0 : std_logic_vector(15 downto 0) ; 
	signal max_212_I1 : std_logic_vector(15 downto 0) ; 
	signal max_212_O : std_logic_vector(15 downto 0) ; 

	signal max_213_I0 : std_logic_vector(15 downto 0) ; 
	signal max_213_I1 : std_logic_vector(15 downto 0) ; 
	signal max_213_O : std_logic_vector(15 downto 0) ; 

	signal max_214_I0 : std_logic_vector(15 downto 0) ; 
	signal max_214_I1 : std_logic_vector(15 downto 0) ; 
	signal max_214_O : std_logic_vector(15 downto 0) ; 

	signal max_215_I0 : std_logic_vector(15 downto 0) ; 
	signal max_215_I1 : std_logic_vector(15 downto 0) ; 
	signal max_215_O : std_logic_vector(15 downto 0) ; 

	signal max_216_I0 : std_logic_vector(15 downto 0) ; 
	signal max_216_I1 : std_logic_vector(15 downto 0) ; 
	signal max_216_O : std_logic_vector(15 downto 0) ; 

	signal max_217_I0 : std_logic_vector(15 downto 0) ; 
	signal max_217_I1 : std_logic_vector(15 downto 0) ; 
	signal max_217_O : std_logic_vector(15 downto 0) ; 

	signal max_218_I0 : std_logic_vector(15 downto 0) ; 
	signal max_218_I1 : std_logic_vector(15 downto 0) ; 
	signal max_218_O : std_logic_vector(15 downto 0) ; 

	signal max_219_I0 : std_logic_vector(15 downto 0) ; 
	signal max_219_I1 : std_logic_vector(15 downto 0) ; 
	signal max_219_O : std_logic_vector(15 downto 0) ; 

	signal max_220_I0 : std_logic_vector(15 downto 0) ; 
	signal max_220_I1 : std_logic_vector(15 downto 0) ; 
	signal max_220_O : std_logic_vector(15 downto 0) ; 

	signal max_221_I0 : std_logic_vector(15 downto 0) ; 
	signal max_221_I1 : std_logic_vector(15 downto 0) ; 
	signal max_221_O : std_logic_vector(15 downto 0) ; 

	signal max_222_I0 : std_logic_vector(15 downto 0) ; 
	signal max_222_I1 : std_logic_vector(15 downto 0) ; 
	signal max_222_O : std_logic_vector(15 downto 0) ; 

	signal max_223_I0 : std_logic_vector(15 downto 0) ; 
	signal max_223_I1 : std_logic_vector(15 downto 0) ; 
	signal max_223_O : std_logic_vector(15 downto 0) ; 

	signal max_224_I0 : std_logic_vector(15 downto 0) ; 
	signal max_224_I1 : std_logic_vector(15 downto 0) ; 
	signal max_224_O : std_logic_vector(15 downto 0) ; 

	signal max_225_I0 : std_logic_vector(15 downto 0) ; 
	signal max_225_I1 : std_logic_vector(15 downto 0) ; 
	signal max_225_O : std_logic_vector(15 downto 0) ; 

	signal max_226_I0 : std_logic_vector(15 downto 0) ; 
	signal max_226_I1 : std_logic_vector(15 downto 0) ; 
	signal max_226_O : std_logic_vector(15 downto 0) ; 

	signal max_227_I0 : std_logic_vector(15 downto 0) ; 
	signal max_227_I1 : std_logic_vector(15 downto 0) ; 
	signal max_227_O : std_logic_vector(15 downto 0) ; 

	signal max_228_I0 : std_logic_vector(15 downto 0) ; 
	signal max_228_I1 : std_logic_vector(15 downto 0) ; 
	signal max_228_O : std_logic_vector(15 downto 0) ; 

	signal max_229_I0 : std_logic_vector(15 downto 0) ; 
	signal max_229_I1 : std_logic_vector(15 downto 0) ; 
	signal max_229_O : std_logic_vector(15 downto 0) ; 

	signal max_230_I0 : std_logic_vector(15 downto 0) ; 
	signal max_230_I1 : std_logic_vector(15 downto 0) ; 
	signal max_230_O : std_logic_vector(15 downto 0) ; 

	signal max_231_I0 : std_logic_vector(15 downto 0) ; 
	signal max_231_I1 : std_logic_vector(15 downto 0) ; 
	signal max_231_O : std_logic_vector(15 downto 0) ; 

	signal max_232_I0 : std_logic_vector(15 downto 0) ; 
	signal max_232_I1 : std_logic_vector(15 downto 0) ; 
	signal max_232_O : std_logic_vector(15 downto 0) ; 

	signal max_233_I0 : std_logic_vector(15 downto 0) ; 
	signal max_233_I1 : std_logic_vector(15 downto 0) ; 
	signal max_233_O : std_logic_vector(15 downto 0) ; 

	signal max_234_I0 : std_logic_vector(15 downto 0) ; 
	signal max_234_I1 : std_logic_vector(15 downto 0) ; 
	signal max_234_O : std_logic_vector(15 downto 0) ; 

	signal max_235_I0 : std_logic_vector(15 downto 0) ; 
	signal max_235_I1 : std_logic_vector(15 downto 0) ; 
	signal max_235_O : std_logic_vector(15 downto 0) ; 

	signal max_236_I0 : std_logic_vector(15 downto 0) ; 
	signal max_236_I1 : std_logic_vector(15 downto 0) ; 
	signal max_236_O : std_logic_vector(15 downto 0) ; 

	signal max_237_I0 : std_logic_vector(15 downto 0) ; 
	signal max_237_I1 : std_logic_vector(15 downto 0) ; 
	signal max_237_O : std_logic_vector(15 downto 0) ; 

	signal max_238_I0 : std_logic_vector(15 downto 0) ; 
	signal max_238_I1 : std_logic_vector(15 downto 0) ; 
	signal max_238_O : std_logic_vector(15 downto 0) ; 

	signal max_239_I0 : std_logic_vector(15 downto 0) ; 
	signal max_239_I1 : std_logic_vector(15 downto 0) ; 
	signal max_239_O : std_logic_vector(15 downto 0) ; 

	signal max_240_I0 : std_logic_vector(15 downto 0) ; 
	signal max_240_I1 : std_logic_vector(15 downto 0) ; 
	signal max_240_O : std_logic_vector(15 downto 0) ; 

	signal max_241_I0 : std_logic_vector(15 downto 0) ; 
	signal max_241_I1 : std_logic_vector(15 downto 0) ; 
	signal max_241_O : std_logic_vector(15 downto 0) ; 

	signal max_242_I0 : std_logic_vector(15 downto 0) ; 
	signal max_242_I1 : std_logic_vector(15 downto 0) ; 
	signal max_242_O : std_logic_vector(15 downto 0) ; 

	signal max_243_I0 : std_logic_vector(15 downto 0) ; 
	signal max_243_I1 : std_logic_vector(15 downto 0) ; 
	signal max_243_O : std_logic_vector(15 downto 0) ; 

	signal max_244_I0 : std_logic_vector(15 downto 0) ; 
	signal max_244_I1 : std_logic_vector(15 downto 0) ; 
	signal max_244_O : std_logic_vector(15 downto 0) ; 

	signal max_245_I0 : std_logic_vector(15 downto 0) ; 
	signal max_245_I1 : std_logic_vector(15 downto 0) ; 
	signal max_245_O : std_logic_vector(15 downto 0) ; 

	signal max_246_I0 : std_logic_vector(15 downto 0) ; 
	signal max_246_I1 : std_logic_vector(15 downto 0) ; 
	signal max_246_O : std_logic_vector(15 downto 0) ; 

	signal max_247_I0 : std_logic_vector(15 downto 0) ; 
	signal max_247_I1 : std_logic_vector(15 downto 0) ; 
	signal max_247_O : std_logic_vector(15 downto 0) ; 

	signal max_248_I0 : std_logic_vector(15 downto 0) ; 
	signal max_248_I1 : std_logic_vector(15 downto 0) ; 
	signal max_248_O : std_logic_vector(15 downto 0) ; 

	signal max_249_I0 : std_logic_vector(15 downto 0) ; 
	signal max_249_I1 : std_logic_vector(15 downto 0) ; 
	signal max_249_O : std_logic_vector(15 downto 0) ; 

	signal max_250_I0 : std_logic_vector(15 downto 0) ; 
	signal max_250_I1 : std_logic_vector(15 downto 0) ; 
	signal max_250_O : std_logic_vector(15 downto 0) ; 

	signal max_251_I0 : std_logic_vector(15 downto 0) ; 
	signal max_251_I1 : std_logic_vector(15 downto 0) ; 
	signal max_251_O : std_logic_vector(15 downto 0) ; 

	signal max_252_I0 : std_logic_vector(15 downto 0) ; 
	signal max_252_I1 : std_logic_vector(15 downto 0) ; 
	signal max_252_O : std_logic_vector(15 downto 0) ; 

	signal max_253_I0 : std_logic_vector(15 downto 0) ; 
	signal max_253_I1 : std_logic_vector(15 downto 0) ; 
	signal max_253_O : std_logic_vector(15 downto 0) ; 

	signal max_254_I0 : std_logic_vector(15 downto 0) ; 
	signal max_254_I1 : std_logic_vector(15 downto 0) ; 
	signal max_254_O : std_logic_vector(15 downto 0) ; 

	signal max_255_I0 : std_logic_vector(15 downto 0) ; 
	signal max_255_I1 : std_logic_vector(15 downto 0) ; 
	signal max_255_O : std_logic_vector(15 downto 0) ; 

	signal max_256_I0 : std_logic_vector(15 downto 0) ; 
	signal max_256_I1 : std_logic_vector(15 downto 0) ; 
	signal max_256_O : std_logic_vector(15 downto 0) ; 

	signal max_257_I0 : std_logic_vector(15 downto 0) ; 
	signal max_257_I1 : std_logic_vector(15 downto 0) ; 
	signal max_257_O : std_logic_vector(15 downto 0) ; 

	signal max_258_I0 : std_logic_vector(15 downto 0) ; 
	signal max_258_I1 : std_logic_vector(15 downto 0) ; 
	signal max_258_O : std_logic_vector(15 downto 0) ; 

	signal max_259_I0 : std_logic_vector(15 downto 0) ; 
	signal max_259_I1 : std_logic_vector(15 downto 0) ; 
	signal max_259_O : std_logic_vector(15 downto 0) ; 

	signal max_260_I0 : std_logic_vector(15 downto 0) ; 
	signal max_260_I1 : std_logic_vector(15 downto 0) ; 
	signal max_260_O : std_logic_vector(15 downto 0) ; 

	signal max_261_I0 : std_logic_vector(15 downto 0) ; 
	signal max_261_I1 : std_logic_vector(15 downto 0) ; 
	signal max_261_O : std_logic_vector(15 downto 0) ; 

	signal max_262_I0 : std_logic_vector(15 downto 0) ; 
	signal max_262_I1 : std_logic_vector(15 downto 0) ; 
	signal max_262_O : std_logic_vector(15 downto 0) ; 

	signal max_263_I0 : std_logic_vector(15 downto 0) ; 
	signal max_263_I1 : std_logic_vector(15 downto 0) ; 
	signal max_263_O : std_logic_vector(15 downto 0) ; 

	signal max_264_I0 : std_logic_vector(15 downto 0) ; 
	signal max_264_I1 : std_logic_vector(15 downto 0) ; 
	signal max_264_O : std_logic_vector(15 downto 0) ; 

	signal max_265_I0 : std_logic_vector(15 downto 0) ; 
	signal max_265_I1 : std_logic_vector(15 downto 0) ; 
	signal max_265_O : std_logic_vector(15 downto 0) ; 

	signal max_266_I0 : std_logic_vector(15 downto 0) ; 
	signal max_266_I1 : std_logic_vector(15 downto 0) ; 
	signal max_266_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op267_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op267_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op267_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op268_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op268_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op268_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op269_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op269_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op269_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op270_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op270_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op270_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op271_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op271_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op271_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op272_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op272_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op272_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op273_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op273_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op273_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op274_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op274_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op274_O : std_logic_vector(15 downto 0) ; 

	signal outreg8_D : std_logic_vector(15 downto 0) ; 
	signal outreg8_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op275_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op275_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op275_O : std_logic_vector(15 downto 0) ; 

	signal outreg9_D : std_logic_vector(15 downto 0) ; 
	signal outreg9_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op276_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op276_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op276_O : std_logic_vector(15 downto 0) ; 

	signal outreg10_D : std_logic_vector(15 downto 0) ; 
	signal outreg10_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op277_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op277_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op277_O : std_logic_vector(15 downto 0) ; 

	signal outreg11_D : std_logic_vector(15 downto 0) ; 
	signal outreg11_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op278_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op278_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op278_O : std_logic_vector(15 downto 0) ; 

	signal outreg12_D : std_logic_vector(15 downto 0) ; 
	signal outreg12_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op279_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op279_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op279_O : std_logic_vector(15 downto 0) ; 

	signal outreg13_D : std_logic_vector(15 downto 0) ; 
	signal outreg13_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op280_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op280_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op280_O : std_logic_vector(15 downto 0) ; 

	signal outreg14_D : std_logic_vector(15 downto 0) ; 
	signal outreg14_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op281_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op281_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op281_O : std_logic_vector(15 downto 0) ; 

	signal outreg15_D : std_logic_vector(15 downto 0) ; 
	signal outreg15_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op282_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op282_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op282_O : std_logic_vector(15 downto 0) ; 

	signal outreg16_D : std_logic_vector(15 downto 0) ; 
	signal outreg16_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op283_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op283_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op283_O : std_logic_vector(15 downto 0) ; 

	signal outreg17_D : std_logic_vector(15 downto 0) ; 
	signal outreg17_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op284_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op284_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op284_O : std_logic_vector(15 downto 0) ; 

	signal outreg18_D : std_logic_vector(15 downto 0) ; 
	signal outreg18_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op285_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op285_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op285_O : std_logic_vector(15 downto 0) ; 

	signal outreg19_D : std_logic_vector(15 downto 0) ; 
	signal outreg19_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op286_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op286_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op286_O : std_logic_vector(15 downto 0) ; 

	signal outreg20_D : std_logic_vector(15 downto 0) ; 
	signal outreg20_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op287_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op287_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op287_O : std_logic_vector(15 downto 0) ; 

	signal outreg21_D : std_logic_vector(15 downto 0) ; 
	signal outreg21_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op288_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op288_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op288_O : std_logic_vector(15 downto 0) ; 

	signal outreg22_D : std_logic_vector(15 downto 0) ; 
	signal outreg22_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op289_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op289_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op289_O : std_logic_vector(15 downto 0) ; 

	signal outreg23_D : std_logic_vector(15 downto 0) ; 
	signal outreg23_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op290_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op290_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op290_O : std_logic_vector(15 downto 0) ; 

	signal outreg24_D : std_logic_vector(15 downto 0) ; 
	signal outreg24_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op291_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op291_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op291_O : std_logic_vector(15 downto 0) ; 

	signal outreg25_D : std_logic_vector(15 downto 0) ; 
	signal outreg25_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op292_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op292_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op292_O : std_logic_vector(15 downto 0) ; 

	signal outreg26_D : std_logic_vector(15 downto 0) ; 
	signal outreg26_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op293_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op293_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op293_O : std_logic_vector(15 downto 0) ; 

	signal outreg27_D : std_logic_vector(15 downto 0) ; 
	signal outreg27_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op294_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op294_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op294_O : std_logic_vector(15 downto 0) ; 

	signal outreg28_D : std_logic_vector(15 downto 0) ; 
	signal outreg28_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op295_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op295_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op295_O : std_logic_vector(15 downto 0) ; 

	signal outreg29_D : std_logic_vector(15 downto 0) ; 
	signal outreg29_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op296_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op296_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op296_O : std_logic_vector(15 downto 0) ; 

	signal outreg30_D : std_logic_vector(15 downto 0) ; 
	signal outreg30_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op297_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op297_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op297_O : std_logic_vector(15 downto 0) ; 

	signal outreg31_D : std_logic_vector(15 downto 0) ; 
	signal outreg31_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal inreg8_CE : std_logic ; 
	signal inreg9_CE : std_logic ; 
	signal inreg10_CE : std_logic ; 
	signal inreg11_CE : std_logic ; 
	signal inreg12_CE : std_logic ; 
	signal inreg13_CE : std_logic ; 
	signal inreg14_CE : std_logic ; 
	signal inreg15_CE : std_logic ; 
	signal inreg16_CE : std_logic ; 
	signal inreg17_CE : std_logic ; 
	signal inreg18_CE : std_logic ; 
	signal inreg19_CE : std_logic ; 
	signal inreg20_CE : std_logic ; 
	signal inreg21_CE : std_logic ; 
	signal inreg22_CE : std_logic ; 
	signal inreg23_CE : std_logic ; 
	signal inreg24_CE : std_logic ; 
	signal inreg25_CE : std_logic ; 
	signal inreg26_CE : std_logic ; 
	signal inreg27_CE : std_logic ; 
	signal inreg28_CE : std_logic ; 
	signal inreg29_CE : std_logic ; 
	signal inreg30_CE : std_logic ; 
	signal inreg31_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op267_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op268_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op269_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op270_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op271_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op272_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op273_S : std_logic ; 
	signal outreg7_CE : std_logic ; 
	signal cmux_op274_S : std_logic ; 
	signal outreg8_CE : std_logic ; 
	signal cmux_op275_S : std_logic ; 
	signal outreg9_CE : std_logic ; 
	signal cmux_op276_S : std_logic ; 
	signal outreg10_CE : std_logic ; 
	signal cmux_op277_S : std_logic ; 
	signal outreg11_CE : std_logic ; 
	signal cmux_op278_S : std_logic ; 
	signal outreg12_CE : std_logic ; 
	signal cmux_op279_S : std_logic ; 
	signal outreg13_CE : std_logic ; 
	signal cmux_op280_S : std_logic ; 
	signal outreg14_CE : std_logic ; 
	signal cmux_op281_S : std_logic ; 
	signal outreg15_CE : std_logic ; 
	signal cmux_op282_S : std_logic ; 
	signal outreg16_CE : std_logic ; 
	signal cmux_op283_S : std_logic ; 
	signal outreg17_CE : std_logic ; 
	signal cmux_op284_S : std_logic ; 
	signal outreg18_CE : std_logic ; 
	signal cmux_op285_S : std_logic ; 
	signal outreg19_CE : std_logic ; 
	signal cmux_op286_S : std_logic ; 
	signal outreg20_CE : std_logic ; 
	signal cmux_op287_S : std_logic ; 
	signal outreg21_CE : std_logic ; 
	signal cmux_op288_S : std_logic ; 
	signal outreg22_CE : std_logic ; 
	signal cmux_op289_S : std_logic ; 
	signal outreg23_CE : std_logic ; 
	signal cmux_op290_S : std_logic ; 
	signal outreg24_CE : std_logic ; 
	signal cmux_op291_S : std_logic ; 
	signal outreg25_CE : std_logic ; 
	signal cmux_op292_S : std_logic ; 
	signal outreg26_CE : std_logic ; 
	signal cmux_op293_S : std_logic ; 
	signal outreg27_CE : std_logic ; 
	signal cmux_op294_S : std_logic ; 
	signal outreg28_CE : std_logic ; 
	signal cmux_op295_S : std_logic ; 
	signal outreg29_CE : std_logic ; 
	signal cmux_op296_S : std_logic ; 
	signal outreg30_CE : std_logic ; 
	signal cmux_op297_S : std_logic ; 
	signal outreg31_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

max_211_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_212_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_213_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_214_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_215_O <= inreg8_Q when inreg8_Q>inreg9_Q else inreg9_Q;
max_216_O <= inreg10_Q when inreg10_Q>inreg11_Q else inreg11_Q;
max_217_O <= inreg12_Q when inreg12_Q>inreg13_Q else inreg13_Q;
max_218_O <= inreg14_Q when inreg14_Q>inreg15_Q else inreg15_Q;
max_219_O <= inreg16_Q when inreg16_Q>inreg17_Q else inreg17_Q;
max_220_O <= inreg18_Q when inreg18_Q>inreg19_Q else inreg19_Q;
max_221_O <= inreg20_Q when inreg20_Q>inreg21_Q else inreg21_Q;
max_222_O <= inreg22_Q when inreg22_Q>inreg23_Q else inreg23_Q;
max_223_O <= inreg24_Q when inreg24_Q>inreg25_Q else inreg25_Q;
max_224_O <= inreg26_Q when inreg26_Q>inreg27_Q else inreg27_Q;
max_225_O <= inreg28_Q when inreg28_Q>inreg29_Q else inreg29_Q;
max_226_O <= inreg30_Q when inreg30_Q>inreg31_Q else inreg31_Q;
max_227_O <= max_211_O when max_211_O>max_212_O else max_212_O;
max_228_O <= max_213_O when max_213_O>max_214_O else max_214_O;
max_229_O <= max_215_O when max_215_O>max_216_O else max_216_O;
max_230_O <= max_217_O when max_217_O>max_218_O else max_218_O;
max_231_O <= max_219_O when max_219_O>max_220_O else max_220_O;
max_232_O <= max_221_O when max_221_O>max_222_O else max_222_O;
max_233_O <= max_223_O when max_223_O>max_224_O else max_224_O;
max_234_O <= max_225_O when max_225_O>max_226_O else max_226_O;
max_235_O <= max_227_O when max_227_O>max_228_O else max_228_O;
max_236_O <= max_229_O when max_229_O>max_230_O else max_230_O;
max_237_O <= max_231_O when max_231_O>max_232_O else max_232_O;
max_238_O <= max_233_O when max_233_O>max_234_O else max_234_O;
max_239_O <= max_235_O when max_235_O>max_236_O else max_236_O;
max_240_O <= max_237_O when max_237_O>max_238_O else max_238_O;
max_241_O <= max_236_O when max_236_O>max_240_O else max_240_O;
max_242_O <= max_228_O when max_228_O>max_241_O else max_241_O;
max_243_O <= max_230_O when max_230_O>max_240_O else max_240_O;
max_244_O <= max_232_O when max_232_O>max_238_O else max_238_O;
max_245_O <= max_212_O when max_212_O>max_242_O else max_242_O;
max_246_O <= max_214_O when max_214_O>max_241_O else max_241_O;
max_247_O <= max_216_O when max_216_O>max_243_O else max_243_O;
max_248_O <= max_218_O when max_218_O>max_240_O else max_240_O;
max_249_O <= max_220_O when max_220_O>max_244_O else max_244_O;
max_250_O <= max_222_O when max_222_O>max_238_O else max_238_O;
max_251_O <= max_224_O when max_224_O>max_234_O else max_234_O;
max_252_O <= inreg1_Q when inreg1_Q>max_245_O else max_245_O;
max_253_O <= inreg3_Q when inreg3_Q>max_242_O else max_242_O;
max_254_O <= inreg5_Q when inreg5_Q>max_246_O else max_246_O;
max_255_O <= inreg7_Q when inreg7_Q>max_241_O else max_241_O;
max_256_O <= inreg9_Q when inreg9_Q>max_247_O else max_247_O;
max_257_O <= inreg11_Q when inreg11_Q>max_243_O else max_243_O;
max_258_O <= inreg13_Q when inreg13_Q>max_248_O else max_248_O;
max_259_O <= inreg15_Q when inreg15_Q>max_240_O else max_240_O;
max_260_O <= inreg17_Q when inreg17_Q>max_249_O else max_249_O;
max_261_O <= inreg19_Q when inreg19_Q>max_244_O else max_244_O;
max_262_O <= inreg21_Q when inreg21_Q>max_250_O else max_250_O;
max_263_O <= inreg23_Q when inreg23_Q>max_238_O else max_238_O;
max_264_O <= inreg25_Q when inreg25_Q>max_251_O else max_251_O;
max_265_O <= inreg27_Q when inreg27_Q>max_234_O else max_234_O;
max_266_O <= inreg29_Q when inreg29_Q>max_226_O else max_226_O;
	

	cmux_op267_O <= max_252_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op268_O <= max_245_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op269_O <= max_253_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op270_O <= max_242_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op271_O <= max_254_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op272_O <= max_246_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op273_O <= max_255_O when Shift_Shift= '0'	 else outreg6_Q;
	

	cmux_op274_O <= max_241_O when Shift_Shift= '0'	 else outreg7_Q;
	

	cmux_op275_O <= max_256_O when Shift_Shift= '0'	 else outreg8_Q;
	

	cmux_op276_O <= max_247_O when Shift_Shift= '0'	 else outreg9_Q;
	

	cmux_op277_O <= max_257_O when Shift_Shift= '0'	 else outreg10_Q;
	

	cmux_op278_O <= max_243_O when Shift_Shift= '0'	 else outreg11_Q;
	

	cmux_op279_O <= max_258_O when Shift_Shift= '0'	 else outreg12_Q;
	

	cmux_op280_O <= max_248_O when Shift_Shift= '0'	 else outreg13_Q;
	

	cmux_op281_O <= max_259_O when Shift_Shift= '0'	 else outreg14_Q;
	

	cmux_op282_O <= max_240_O when Shift_Shift= '0'	 else outreg15_Q;
	

	cmux_op283_O <= max_260_O when Shift_Shift= '0'	 else outreg16_Q;
	

	cmux_op284_O <= max_249_O when Shift_Shift= '0'	 else outreg17_Q;
	

	cmux_op285_O <= max_261_O when Shift_Shift= '0'	 else outreg18_Q;
	

	cmux_op286_O <= max_244_O when Shift_Shift= '0'	 else outreg19_Q;
	

	cmux_op287_O <= max_262_O when Shift_Shift= '0'	 else outreg20_Q;
	

	cmux_op288_O <= max_250_O when Shift_Shift= '0'	 else outreg21_Q;
	

	cmux_op289_O <= max_263_O when Shift_Shift= '0'	 else outreg22_Q;
	

	cmux_op290_O <= max_238_O when Shift_Shift= '0'	 else outreg23_Q;
	

	cmux_op291_O <= max_264_O when Shift_Shift= '0'	 else outreg24_Q;
	

	cmux_op292_O <= max_251_O when Shift_Shift= '0'	 else outreg25_Q;
	

	cmux_op293_O <= max_265_O when Shift_Shift= '0'	 else outreg26_Q;
	

	cmux_op294_O <= max_234_O when Shift_Shift= '0'	 else outreg27_Q;
	

	cmux_op295_O <= max_266_O when Shift_Shift= '0'	 else outreg28_Q;
	

	cmux_op296_O <= max_226_O when Shift_Shift= '0'	 else outreg29_Q;
	

	cmux_op297_O <= inreg31_Q when Shift_Shift= '0'	 else outreg30_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	inreg8_D <= inreg7_Q;
	inreg9_D <= inreg8_Q;
	inreg10_D <= inreg9_Q;
	inreg11_D <= inreg10_Q;
	inreg12_D <= inreg11_Q;
	inreg13_D <= inreg12_Q;
	inreg14_D <= inreg13_Q;
	inreg15_D <= inreg14_Q;
	inreg16_D <= inreg15_Q;
	inreg17_D <= inreg16_Q;
	inreg18_D <= inreg17_Q;
	inreg19_D <= inreg18_Q;
	inreg20_D <= inreg19_Q;
	inreg21_D <= inreg20_Q;
	inreg22_D <= inreg21_Q;
	inreg23_D <= inreg22_Q;
	inreg24_D <= inreg23_Q;
	inreg25_D <= inreg24_Q;
	inreg26_D <= inreg25_Q;
	inreg27_D <= inreg26_Q;
	inreg28_D <= inreg27_Q;
	inreg29_D <= inreg28_Q;
	inreg30_D <= inreg29_Q;
	inreg31_D <= inreg30_Q;
	max_211_I0 <= inreg0_Q;
	max_211_I1 <= inreg1_Q;
	max_212_I0 <= inreg2_Q;
	max_212_I1 <= inreg3_Q;
	max_213_I0 <= inreg4_Q;
	max_213_I1 <= inreg5_Q;
	max_214_I0 <= inreg6_Q;
	max_214_I1 <= inreg7_Q;
	max_215_I0 <= inreg8_Q;
	max_215_I1 <= inreg9_Q;
	max_216_I0 <= inreg10_Q;
	max_216_I1 <= inreg11_Q;
	max_217_I0 <= inreg12_Q;
	max_217_I1 <= inreg13_Q;
	max_218_I0 <= inreg14_Q;
	max_218_I1 <= inreg15_Q;
	max_219_I0 <= inreg16_Q;
	max_219_I1 <= inreg17_Q;
	max_220_I0 <= inreg18_Q;
	max_220_I1 <= inreg19_Q;
	max_221_I0 <= inreg20_Q;
	max_221_I1 <= inreg21_Q;
	max_222_I0 <= inreg22_Q;
	max_222_I1 <= inreg23_Q;
	max_223_I0 <= inreg24_Q;
	max_223_I1 <= inreg25_Q;
	max_224_I0 <= inreg26_Q;
	max_224_I1 <= inreg27_Q;
	max_225_I0 <= inreg28_Q;
	max_225_I1 <= inreg29_Q;
	max_226_I0 <= inreg30_Q;
	max_226_I1 <= inreg31_Q;
	max_227_I0 <= max_211_O;
	max_227_I1 <= max_212_O;
	max_228_I0 <= max_213_O;
	max_228_I1 <= max_214_O;
	max_229_I0 <= max_215_O;
	max_229_I1 <= max_216_O;
	max_230_I0 <= max_217_O;
	max_230_I1 <= max_218_O;
	max_231_I0 <= max_219_O;
	max_231_I1 <= max_220_O;
	max_232_I0 <= max_221_O;
	max_232_I1 <= max_222_O;
	max_233_I0 <= max_223_O;
	max_233_I1 <= max_224_O;
	max_234_I0 <= max_225_O;
	max_234_I1 <= max_226_O;
	max_235_I0 <= max_227_O;
	max_235_I1 <= max_228_O;
	max_236_I0 <= max_229_O;
	max_236_I1 <= max_230_O;
	max_237_I0 <= max_231_O;
	max_237_I1 <= max_232_O;
	max_238_I0 <= max_233_O;
	max_238_I1 <= max_234_O;
	max_239_I0 <= max_235_O;
	max_239_I1 <= max_236_O;
	max_240_I0 <= max_237_O;
	max_240_I1 <= max_238_O;
	max_241_I0 <= max_236_O;
	max_241_I1 <= max_240_O;
	max_242_I0 <= max_228_O;
	max_242_I1 <= max_241_O;
	max_243_I0 <= max_230_O;
	max_243_I1 <= max_240_O;
	max_244_I0 <= max_232_O;
	max_244_I1 <= max_238_O;
	max_245_I0 <= max_212_O;
	max_245_I1 <= max_242_O;
	max_246_I0 <= max_214_O;
	max_246_I1 <= max_241_O;
	max_247_I0 <= max_216_O;
	max_247_I1 <= max_243_O;
	max_248_I0 <= max_218_O;
	max_248_I1 <= max_240_O;
	max_249_I0 <= max_220_O;
	max_249_I1 <= max_244_O;
	max_250_I0 <= max_222_O;
	max_250_I1 <= max_238_O;
	max_251_I0 <= max_224_O;
	max_251_I1 <= max_234_O;
	max_252_I0 <= inreg1_Q;
	max_252_I1 <= max_245_O;
	max_253_I0 <= inreg3_Q;
	max_253_I1 <= max_242_O;
	max_254_I0 <= inreg5_Q;
	max_254_I1 <= max_246_O;
	max_255_I0 <= inreg7_Q;
	max_255_I1 <= max_241_O;
	max_256_I0 <= inreg9_Q;
	max_256_I1 <= max_247_O;
	max_257_I0 <= inreg11_Q;
	max_257_I1 <= max_243_O;
	max_258_I0 <= inreg13_Q;
	max_258_I1 <= max_248_O;
	max_259_I0 <= inreg15_Q;
	max_259_I1 <= max_240_O;
	max_260_I0 <= inreg17_Q;
	max_260_I1 <= max_249_O;
	max_261_I0 <= inreg19_Q;
	max_261_I1 <= max_244_O;
	max_262_I0 <= inreg21_Q;
	max_262_I1 <= max_250_O;
	max_263_I0 <= inreg23_Q;
	max_263_I1 <= max_238_O;
	max_264_I0 <= inreg25_Q;
	max_264_I1 <= max_251_O;
	max_265_I0 <= inreg27_Q;
	max_265_I1 <= max_234_O;
	max_266_I0 <= inreg29_Q;
	max_266_I1 <= max_226_O;
	outreg0_D <= max_239_O;
	cmux_op267_I0 <= max_252_O;
	cmux_op267_I1 <= outreg0_Q;
	outreg1_D <= cmux_op267_O;
	cmux_op268_I0 <= max_245_O;
	cmux_op268_I1 <= outreg1_Q;
	outreg2_D <= cmux_op268_O;
	cmux_op269_I0 <= max_253_O;
	cmux_op269_I1 <= outreg2_Q;
	outreg3_D <= cmux_op269_O;
	cmux_op270_I0 <= max_242_O;
	cmux_op270_I1 <= outreg3_Q;
	outreg4_D <= cmux_op270_O;
	cmux_op271_I0 <= max_254_O;
	cmux_op271_I1 <= outreg4_Q;
	outreg5_D <= cmux_op271_O;
	cmux_op272_I0 <= max_246_O;
	cmux_op272_I1 <= outreg5_Q;
	outreg6_D <= cmux_op272_O;
	cmux_op273_I0 <= max_255_O;
	cmux_op273_I1 <= outreg6_Q;
	outreg7_D <= cmux_op273_O;
	cmux_op274_I0 <= max_241_O;
	cmux_op274_I1 <= outreg7_Q;
	outreg8_D <= cmux_op274_O;
	cmux_op275_I0 <= max_256_O;
	cmux_op275_I1 <= outreg8_Q;
	outreg9_D <= cmux_op275_O;
	cmux_op276_I0 <= max_247_O;
	cmux_op276_I1 <= outreg9_Q;
	outreg10_D <= cmux_op276_O;
	cmux_op277_I0 <= max_257_O;
	cmux_op277_I1 <= outreg10_Q;
	outreg11_D <= cmux_op277_O;
	cmux_op278_I0 <= max_243_O;
	cmux_op278_I1 <= outreg11_Q;
	outreg12_D <= cmux_op278_O;
	cmux_op279_I0 <= max_258_O;
	cmux_op279_I1 <= outreg12_Q;
	outreg13_D <= cmux_op279_O;
	cmux_op280_I0 <= max_248_O;
	cmux_op280_I1 <= outreg13_Q;
	outreg14_D <= cmux_op280_O;
	cmux_op281_I0 <= max_259_O;
	cmux_op281_I1 <= outreg14_Q;
	outreg15_D <= cmux_op281_O;
	cmux_op282_I0 <= max_240_O;
	cmux_op282_I1 <= outreg15_Q;
	outreg16_D <= cmux_op282_O;
	cmux_op283_I0 <= max_260_O;
	cmux_op283_I1 <= outreg16_Q;
	outreg17_D <= cmux_op283_O;
	cmux_op284_I0 <= max_249_O;
	cmux_op284_I1 <= outreg17_Q;
	outreg18_D <= cmux_op284_O;
	cmux_op285_I0 <= max_261_O;
	cmux_op285_I1 <= outreg18_Q;
	outreg19_D <= cmux_op285_O;
	cmux_op286_I0 <= max_244_O;
	cmux_op286_I1 <= outreg19_Q;
	outreg20_D <= cmux_op286_O;
	cmux_op287_I0 <= max_262_O;
	cmux_op287_I1 <= outreg20_Q;
	outreg21_D <= cmux_op287_O;
	cmux_op288_I0 <= max_250_O;
	cmux_op288_I1 <= outreg21_Q;
	outreg22_D <= cmux_op288_O;
	cmux_op289_I0 <= max_263_O;
	cmux_op289_I1 <= outreg22_Q;
	outreg23_D <= cmux_op289_O;
	cmux_op290_I0 <= max_238_O;
	cmux_op290_I1 <= outreg23_Q;
	outreg24_D <= cmux_op290_O;
	cmux_op291_I0 <= max_264_O;
	cmux_op291_I1 <= outreg24_Q;
	outreg25_D <= cmux_op291_O;
	cmux_op292_I0 <= max_251_O;
	cmux_op292_I1 <= outreg25_Q;
	outreg26_D <= cmux_op292_O;
	cmux_op293_I0 <= max_265_O;
	cmux_op293_I1 <= outreg26_Q;
	outreg27_D <= cmux_op293_O;
	cmux_op294_I0 <= max_234_O;
	cmux_op294_I1 <= outreg27_Q;
	outreg28_D <= cmux_op294_O;
	cmux_op295_I0 <= max_266_O;
	cmux_op295_I1 <= outreg28_Q;
	outreg29_D <= cmux_op295_O;
	cmux_op296_I0 <= max_226_O;
	cmux_op296_I1 <= outreg29_Q;
	outreg30_D <= cmux_op296_O;
	cmux_op297_I0 <= inreg31_Q;
	cmux_op297_I1 <= outreg30_Q;
	outreg31_D <= cmux_op297_O;
	out_data_out_data <= outreg31_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	inreg8_CE <= Shift_Shift;
	inreg9_CE <= Shift_Shift;
	inreg10_CE <= Shift_Shift;
	inreg11_CE <= Shift_Shift;
	inreg12_CE <= Shift_Shift;
	inreg13_CE <= Shift_Shift;
	inreg14_CE <= Shift_Shift;
	inreg15_CE <= Shift_Shift;
	inreg16_CE <= Shift_Shift;
	inreg17_CE <= Shift_Shift;
	inreg18_CE <= Shift_Shift;
	inreg19_CE <= Shift_Shift;
	inreg20_CE <= Shift_Shift;
	inreg21_CE <= Shift_Shift;
	inreg22_CE <= Shift_Shift;
	inreg23_CE <= Shift_Shift;
	inreg24_CE <= Shift_Shift;
	inreg25_CE <= Shift_Shift;
	inreg26_CE <= Shift_Shift;
	inreg27_CE <= Shift_Shift;
	inreg28_CE <= Shift_Shift;
	inreg29_CE <= Shift_Shift;
	inreg30_CE <= Shift_Shift;
	inreg31_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op267_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op268_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op269_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op270_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op271_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op272_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op273_S <= Shift_Shift;
	outreg7_CE <= CE_CE;
	cmux_op274_S <= Shift_Shift;
	outreg8_CE <= CE_CE;
	cmux_op275_S <= Shift_Shift;
	outreg9_CE <= CE_CE;
	cmux_op276_S <= Shift_Shift;
	outreg10_CE <= CE_CE;
	cmux_op277_S <= Shift_Shift;
	outreg11_CE <= CE_CE;
	cmux_op278_S <= Shift_Shift;
	outreg12_CE <= CE_CE;
	cmux_op279_S <= Shift_Shift;
	outreg13_CE <= CE_CE;
	cmux_op280_S <= Shift_Shift;
	outreg14_CE <= CE_CE;
	cmux_op281_S <= Shift_Shift;
	outreg15_CE <= CE_CE;
	cmux_op282_S <= Shift_Shift;
	outreg16_CE <= CE_CE;
	cmux_op283_S <= Shift_Shift;
	outreg17_CE <= CE_CE;
	cmux_op284_S <= Shift_Shift;
	outreg18_CE <= CE_CE;
	cmux_op285_S <= Shift_Shift;
	outreg19_CE <= CE_CE;
	cmux_op286_S <= Shift_Shift;
	outreg20_CE <= CE_CE;
	cmux_op287_S <= Shift_Shift;
	outreg21_CE <= CE_CE;
	cmux_op288_S <= Shift_Shift;
	outreg22_CE <= CE_CE;
	cmux_op289_S <= Shift_Shift;
	outreg23_CE <= CE_CE;
	cmux_op290_S <= Shift_Shift;
	outreg24_CE <= CE_CE;
	cmux_op291_S <= Shift_Shift;
	outreg25_CE <= CE_CE;
	cmux_op292_S <= Shift_Shift;
	outreg26_CE <= CE_CE;
	cmux_op293_S <= Shift_Shift;
	outreg27_CE <= CE_CE;
	cmux_op294_S <= Shift_Shift;
	outreg28_CE <= CE_CE;
	cmux_op295_S <= Shift_Shift;
	outreg29_CE <= CE_CE;
	cmux_op296_S <= Shift_Shift;
	outreg30_CE <= CE_CE;
	cmux_op297_S <= Shift_Shift;
	outreg31_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	inreg8_Q <= (others => '0');

	inreg9_Q <= (others => '0');

	inreg10_Q <= (others => '0');

	inreg11_Q <= (others => '0');

	inreg12_Q <= (others => '0');

	inreg13_Q <= (others => '0');

	inreg14_Q <= (others => '0');

	inreg15_Q <= (others => '0');

	inreg16_Q <= (others => '0');

	inreg17_Q <= (others => '0');

	inreg18_Q <= (others => '0');

	inreg19_Q <= (others => '0');

	inreg20_Q <= (others => '0');

	inreg21_Q <= (others => '0');

	inreg22_Q <= (others => '0');

	inreg23_Q <= (others => '0');

	inreg24_Q <= (others => '0');

	inreg25_Q <= (others => '0');

	inreg26_Q <= (others => '0');

	inreg27_Q <= (others => '0');

	inreg28_Q <= (others => '0');

	inreg29_Q <= (others => '0');

	inreg30_Q <= (others => '0');

	inreg31_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

	outreg8_Q <= (others => '0');

	outreg9_Q <= (others => '0');

	outreg10_Q <= (others => '0');

	outreg11_Q <= (others => '0');

	outreg12_Q <= (others => '0');

	outreg13_Q <= (others => '0');

	outreg14_Q <= (others => '0');

	outreg15_Q <= (others => '0');

	outreg16_Q <= (others => '0');

	outreg17_Q <= (others => '0');

	outreg18_Q <= (others => '0');

	outreg19_Q <= (others => '0');

	outreg20_Q <= (others => '0');

	outreg21_Q <= (others => '0');

	outreg22_Q <= (others => '0');

	outreg23_Q <= (others => '0');

	outreg24_Q <= (others => '0');

	outreg25_Q <= (others => '0');

	outreg26_Q <= (others => '0');

	outreg27_Q <= (others => '0');

	outreg28_Q <= (others => '0');

	outreg29_Q <= (others => '0');

	outreg30_Q <= (others => '0');

	outreg31_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg8_Q <= inreg7_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg9_Q <= inreg8_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg10_Q <= inreg9_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg11_Q <= inreg10_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg12_Q <= inreg11_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg13_Q <= inreg12_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg14_Q <= inreg13_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg15_Q <= inreg14_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg16_Q <= inreg15_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg17_Q <= inreg16_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg18_Q <= inreg17_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg19_Q <= inreg18_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg20_Q <= inreg19_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg21_Q <= inreg20_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg22_Q <= inreg21_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg23_Q <= inreg22_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg24_Q <= inreg23_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg25_Q <= inreg24_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg26_Q <= inreg25_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg27_Q <= inreg26_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg28_Q <= inreg27_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg29_Q <= inreg28_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg30_Q <= inreg29_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg31_Q <= inreg30_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_239_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op267_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op268_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op269_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op270_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op271_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op272_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op273_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg8_Q <= cmux_op274_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg9_Q <= cmux_op275_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg10_Q <= cmux_op276_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg11_Q <= cmux_op277_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg12_Q <= cmux_op278_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg13_Q <= cmux_op279_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg14_Q <= cmux_op280_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg15_Q <= cmux_op281_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg16_Q <= cmux_op282_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg17_Q <= cmux_op283_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg18_Q <= cmux_op284_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg19_Q <= cmux_op285_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg20_Q <= cmux_op286_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg21_Q <= cmux_op287_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg22_Q <= cmux_op288_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg23_Q <= cmux_op289_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg24_Q <= cmux_op290_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg25_Q <= cmux_op291_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg26_Q <= cmux_op292_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg27_Q <= cmux_op293_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg28_Q <= cmux_op294_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg29_Q <= cmux_op295_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg30_Q <= cmux_op296_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg31_Q <= cmux_op297_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
