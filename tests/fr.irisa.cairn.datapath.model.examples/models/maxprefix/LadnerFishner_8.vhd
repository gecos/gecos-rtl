library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity LadnerFishner_8 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end LadnerFishner_8;

architecture RTL of LadnerFishner_8 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal max_17_I0 : std_logic_vector(15 downto 0) ; 
	signal max_17_I1 : std_logic_vector(15 downto 0) ; 
	signal max_17_O : std_logic_vector(15 downto 0) ; 

	signal max_18_I0 : std_logic_vector(15 downto 0) ; 
	signal max_18_I1 : std_logic_vector(15 downto 0) ; 
	signal max_18_O : std_logic_vector(15 downto 0) ; 

	signal max_19_I0 : std_logic_vector(15 downto 0) ; 
	signal max_19_I1 : std_logic_vector(15 downto 0) ; 
	signal max_19_O : std_logic_vector(15 downto 0) ; 

	signal max_20_I0 : std_logic_vector(15 downto 0) ; 
	signal max_20_I1 : std_logic_vector(15 downto 0) ; 
	signal max_20_O : std_logic_vector(15 downto 0) ; 

	signal max_21_I0 : std_logic_vector(15 downto 0) ; 
	signal max_21_I1 : std_logic_vector(15 downto 0) ; 
	signal max_21_O : std_logic_vector(15 downto 0) ; 

	signal max_22_I0 : std_logic_vector(15 downto 0) ; 
	signal max_22_I1 : std_logic_vector(15 downto 0) ; 
	signal max_22_O : std_logic_vector(15 downto 0) ; 

	signal max_23_I0 : std_logic_vector(15 downto 0) ; 
	signal max_23_I1 : std_logic_vector(15 downto 0) ; 
	signal max_23_O : std_logic_vector(15 downto 0) ; 

	signal max_24_I0 : std_logic_vector(15 downto 0) ; 
	signal max_24_I1 : std_logic_vector(15 downto 0) ; 
	signal max_24_O : std_logic_vector(15 downto 0) ; 

	signal max_25_I0 : std_logic_vector(15 downto 0) ; 
	signal max_25_I1 : std_logic_vector(15 downto 0) ; 
	signal max_25_O : std_logic_vector(15 downto 0) ; 

	signal max_26_I0 : std_logic_vector(15 downto 0) ; 
	signal max_26_I1 : std_logic_vector(15 downto 0) ; 
	signal max_26_O : std_logic_vector(15 downto 0) ; 

	signal max_27_I0 : std_logic_vector(15 downto 0) ; 
	signal max_27_I1 : std_logic_vector(15 downto 0) ; 
	signal max_27_O : std_logic_vector(15 downto 0) ; 

	signal max_28_I0 : std_logic_vector(15 downto 0) ; 
	signal max_28_I1 : std_logic_vector(15 downto 0) ; 
	signal max_28_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op29_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op29_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op29_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op30_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op30_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op30_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op31_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op31_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op31_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op32_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op32_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op32_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op33_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op33_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op33_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op34_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op34_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op34_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op35_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op35_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op35_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op29_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op30_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op31_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op32_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op33_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op34_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op35_S : std_logic ; 
	signal outreg7_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

max_17_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_18_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_19_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_20_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_21_O <= max_17_O when max_17_O>max_18_O else max_18_O;
max_22_O <= inreg1_Q when inreg1_Q>max_18_O else max_18_O;
max_23_O <= max_19_O when max_19_O>max_20_O else max_20_O;
max_24_O <= inreg5_Q when inreg5_Q>max_20_O else max_20_O;
max_25_O <= max_21_O when max_21_O>max_23_O else max_23_O;
max_26_O <= max_22_O when max_22_O>max_23_O else max_23_O;
max_27_O <= max_18_O when max_18_O>max_23_O else max_23_O;
max_28_O <= inreg3_Q when inreg3_Q>max_23_O else max_23_O;
	

	cmux_op29_O <= max_26_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op30_O <= max_27_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op31_O <= max_28_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op32_O <= max_23_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op33_O <= max_24_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op34_O <= max_20_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op35_O <= inreg7_Q when Shift_Shift= '0'	 else outreg6_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	max_17_I0 <= inreg0_Q;
	max_17_I1 <= inreg1_Q;
	max_18_I0 <= inreg2_Q;
	max_18_I1 <= inreg3_Q;
	max_19_I0 <= inreg4_Q;
	max_19_I1 <= inreg5_Q;
	max_20_I0 <= inreg6_Q;
	max_20_I1 <= inreg7_Q;
	max_21_I0 <= max_17_O;
	max_21_I1 <= max_18_O;
	max_22_I0 <= inreg1_Q;
	max_22_I1 <= max_18_O;
	max_23_I0 <= max_19_O;
	max_23_I1 <= max_20_O;
	max_24_I0 <= inreg5_Q;
	max_24_I1 <= max_20_O;
	max_25_I0 <= max_21_O;
	max_25_I1 <= max_23_O;
	max_26_I0 <= max_22_O;
	max_26_I1 <= max_23_O;
	max_27_I0 <= max_18_O;
	max_27_I1 <= max_23_O;
	max_28_I0 <= inreg3_Q;
	max_28_I1 <= max_23_O;
	outreg0_D <= max_25_O;
	cmux_op29_I0 <= max_26_O;
	cmux_op29_I1 <= outreg0_Q;
	outreg1_D <= cmux_op29_O;
	cmux_op30_I0 <= max_27_O;
	cmux_op30_I1 <= outreg1_Q;
	outreg2_D <= cmux_op30_O;
	cmux_op31_I0 <= max_28_O;
	cmux_op31_I1 <= outreg2_Q;
	outreg3_D <= cmux_op31_O;
	cmux_op32_I0 <= max_23_O;
	cmux_op32_I1 <= outreg3_Q;
	outreg4_D <= cmux_op32_O;
	cmux_op33_I0 <= max_24_O;
	cmux_op33_I1 <= outreg4_Q;
	outreg5_D <= cmux_op33_O;
	cmux_op34_I0 <= max_20_O;
	cmux_op34_I1 <= outreg5_Q;
	outreg6_D <= cmux_op34_O;
	cmux_op35_I0 <= inreg7_Q;
	cmux_op35_I1 <= outreg6_Q;
	outreg7_D <= cmux_op35_O;
	out_data_out_data <= outreg7_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op29_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op30_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op31_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op32_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op33_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op34_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op35_S <= Shift_Shift;
	outreg7_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_25_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op29_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op30_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op31_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op32_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op33_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op34_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op35_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
