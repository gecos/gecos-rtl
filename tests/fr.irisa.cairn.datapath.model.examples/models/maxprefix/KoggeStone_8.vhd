library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity KoggeStone_8 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end KoggeStone_8;

architecture RTL of KoggeStone_8 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal max_36_I0 : std_logic_vector(15 downto 0) ; 
	signal max_36_I1 : std_logic_vector(15 downto 0) ; 
	signal max_36_O : std_logic_vector(15 downto 0) ; 

	signal max_37_I0 : std_logic_vector(15 downto 0) ; 
	signal max_37_I1 : std_logic_vector(15 downto 0) ; 
	signal max_37_O : std_logic_vector(15 downto 0) ; 

	signal max_38_I0 : std_logic_vector(15 downto 0) ; 
	signal max_38_I1 : std_logic_vector(15 downto 0) ; 
	signal max_38_O : std_logic_vector(15 downto 0) ; 

	signal max_39_I0 : std_logic_vector(15 downto 0) ; 
	signal max_39_I1 : std_logic_vector(15 downto 0) ; 
	signal max_39_O : std_logic_vector(15 downto 0) ; 

	signal max_40_I0 : std_logic_vector(15 downto 0) ; 
	signal max_40_I1 : std_logic_vector(15 downto 0) ; 
	signal max_40_O : std_logic_vector(15 downto 0) ; 

	signal max_41_I0 : std_logic_vector(15 downto 0) ; 
	signal max_41_I1 : std_logic_vector(15 downto 0) ; 
	signal max_41_O : std_logic_vector(15 downto 0) ; 

	signal max_42_I0 : std_logic_vector(15 downto 0) ; 
	signal max_42_I1 : std_logic_vector(15 downto 0) ; 
	signal max_42_O : std_logic_vector(15 downto 0) ; 

	signal max_43_I0 : std_logic_vector(15 downto 0) ; 
	signal max_43_I1 : std_logic_vector(15 downto 0) ; 
	signal max_43_O : std_logic_vector(15 downto 0) ; 

	signal max_44_I0 : std_logic_vector(15 downto 0) ; 
	signal max_44_I1 : std_logic_vector(15 downto 0) ; 
	signal max_44_O : std_logic_vector(15 downto 0) ; 

	signal max_45_I0 : std_logic_vector(15 downto 0) ; 
	signal max_45_I1 : std_logic_vector(15 downto 0) ; 
	signal max_45_O : std_logic_vector(15 downto 0) ; 

	signal max_46_I0 : std_logic_vector(15 downto 0) ; 
	signal max_46_I1 : std_logic_vector(15 downto 0) ; 
	signal max_46_O : std_logic_vector(15 downto 0) ; 

	signal max_47_I0 : std_logic_vector(15 downto 0) ; 
	signal max_47_I1 : std_logic_vector(15 downto 0) ; 
	signal max_47_O : std_logic_vector(15 downto 0) ; 

	signal max_48_I0 : std_logic_vector(15 downto 0) ; 
	signal max_48_I1 : std_logic_vector(15 downto 0) ; 
	signal max_48_O : std_logic_vector(15 downto 0) ; 

	signal max_49_I0 : std_logic_vector(15 downto 0) ; 
	signal max_49_I1 : std_logic_vector(15 downto 0) ; 
	signal max_49_O : std_logic_vector(15 downto 0) ; 

	signal max_50_I0 : std_logic_vector(15 downto 0) ; 
	signal max_50_I1 : std_logic_vector(15 downto 0) ; 
	signal max_50_O : std_logic_vector(15 downto 0) ; 

	signal max_51_I0 : std_logic_vector(15 downto 0) ; 
	signal max_51_I1 : std_logic_vector(15 downto 0) ; 
	signal max_51_O : std_logic_vector(15 downto 0) ; 

	signal max_52_I0 : std_logic_vector(15 downto 0) ; 
	signal max_52_I1 : std_logic_vector(15 downto 0) ; 
	signal max_52_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op53_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op53_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op53_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op54_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op54_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op54_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op55_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op55_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op55_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op56_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op56_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op56_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op57_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op57_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op57_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op58_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op58_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op58_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op59_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op59_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op59_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op53_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op54_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op55_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op56_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op57_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op58_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op59_S : std_logic ; 
	signal outreg7_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

max_36_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_37_O <= inreg1_Q when inreg1_Q>inreg2_Q else inreg2_Q;
max_38_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_39_O <= inreg3_Q when inreg3_Q>inreg4_Q else inreg4_Q;
max_40_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_41_O <= inreg5_Q when inreg5_Q>inreg6_Q else inreg6_Q;
max_42_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_43_O <= max_36_O when max_36_O>max_38_O else max_38_O;
max_44_O <= max_37_O when max_37_O>max_39_O else max_39_O;
max_45_O <= max_38_O when max_38_O>max_40_O else max_40_O;
max_46_O <= max_39_O when max_39_O>max_41_O else max_41_O;
max_47_O <= max_40_O when max_40_O>max_42_O else max_42_O;
max_48_O <= max_41_O when max_41_O>inreg7_Q else inreg7_Q;
max_49_O <= max_43_O when max_43_O>max_47_O else max_47_O;
max_50_O <= max_44_O when max_44_O>max_48_O else max_48_O;
max_51_O <= max_45_O when max_45_O>max_42_O else max_42_O;
max_52_O <= max_46_O when max_46_O>inreg7_Q else inreg7_Q;
	

	cmux_op53_O <= max_50_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op54_O <= max_51_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op55_O <= max_52_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op56_O <= max_47_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op57_O <= max_48_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op58_O <= max_42_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op59_O <= inreg7_Q when Shift_Shift= '0'	 else outreg6_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	max_36_I0 <= inreg0_Q;
	max_36_I1 <= inreg1_Q;
	max_37_I0 <= inreg1_Q;
	max_37_I1 <= inreg2_Q;
	max_38_I0 <= inreg2_Q;
	max_38_I1 <= inreg3_Q;
	max_39_I0 <= inreg3_Q;
	max_39_I1 <= inreg4_Q;
	max_40_I0 <= inreg4_Q;
	max_40_I1 <= inreg5_Q;
	max_41_I0 <= inreg5_Q;
	max_41_I1 <= inreg6_Q;
	max_42_I0 <= inreg6_Q;
	max_42_I1 <= inreg7_Q;
	max_43_I0 <= max_36_O;
	max_43_I1 <= max_38_O;
	max_44_I0 <= max_37_O;
	max_44_I1 <= max_39_O;
	max_45_I0 <= max_38_O;
	max_45_I1 <= max_40_O;
	max_46_I0 <= max_39_O;
	max_46_I1 <= max_41_O;
	max_47_I0 <= max_40_O;
	max_47_I1 <= max_42_O;
	max_48_I0 <= max_41_O;
	max_48_I1 <= inreg7_Q;
	max_49_I0 <= max_43_O;
	max_49_I1 <= max_47_O;
	max_50_I0 <= max_44_O;
	max_50_I1 <= max_48_O;
	max_51_I0 <= max_45_O;
	max_51_I1 <= max_42_O;
	max_52_I0 <= max_46_O;
	max_52_I1 <= inreg7_Q;
	outreg0_D <= max_49_O;
	cmux_op53_I0 <= max_50_O;
	cmux_op53_I1 <= outreg0_Q;
	outreg1_D <= cmux_op53_O;
	cmux_op54_I0 <= max_51_O;
	cmux_op54_I1 <= outreg1_Q;
	outreg2_D <= cmux_op54_O;
	cmux_op55_I0 <= max_52_O;
	cmux_op55_I1 <= outreg2_Q;
	outreg3_D <= cmux_op55_O;
	cmux_op56_I0 <= max_47_O;
	cmux_op56_I1 <= outreg3_Q;
	outreg4_D <= cmux_op56_O;
	cmux_op57_I0 <= max_48_O;
	cmux_op57_I1 <= outreg4_Q;
	outreg5_D <= cmux_op57_O;
	cmux_op58_I0 <= max_42_O;
	cmux_op58_I1 <= outreg5_Q;
	outreg6_D <= cmux_op58_O;
	cmux_op59_I0 <= inreg7_Q;
	cmux_op59_I1 <= outreg6_Q;
	outreg7_D <= cmux_op59_O;
	out_data_out_data <= outreg7_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op53_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op54_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op55_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op56_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op57_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op58_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op59_S <= Shift_Shift;
	outreg7_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_49_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op53_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op54_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op55_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op56_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op57_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op58_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op59_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
