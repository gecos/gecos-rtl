library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity BrentKung_8 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end BrentKung_8;

architecture RTL of BrentKung_8 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal max_0_I0 : std_logic_vector(15 downto 0) ; 
	signal max_0_I1 : std_logic_vector(15 downto 0) ; 
	signal max_0_O : std_logic_vector(15 downto 0) ; 

	signal max_1_I0 : std_logic_vector(15 downto 0) ; 
	signal max_1_I1 : std_logic_vector(15 downto 0) ; 
	signal max_1_O : std_logic_vector(15 downto 0) ; 

	signal max_2_I0 : std_logic_vector(15 downto 0) ; 
	signal max_2_I1 : std_logic_vector(15 downto 0) ; 
	signal max_2_O : std_logic_vector(15 downto 0) ; 

	signal max_3_I0 : std_logic_vector(15 downto 0) ; 
	signal max_3_I1 : std_logic_vector(15 downto 0) ; 
	signal max_3_O : std_logic_vector(15 downto 0) ; 

	signal max_4_I0 : std_logic_vector(15 downto 0) ; 
	signal max_4_I1 : std_logic_vector(15 downto 0) ; 
	signal max_4_O : std_logic_vector(15 downto 0) ; 

	signal max_5_I0 : std_logic_vector(15 downto 0) ; 
	signal max_5_I1 : std_logic_vector(15 downto 0) ; 
	signal max_5_O : std_logic_vector(15 downto 0) ; 

	signal max_6_I0 : std_logic_vector(15 downto 0) ; 
	signal max_6_I1 : std_logic_vector(15 downto 0) ; 
	signal max_6_O : std_logic_vector(15 downto 0) ; 

	signal max_7_I0 : std_logic_vector(15 downto 0) ; 
	signal max_7_I1 : std_logic_vector(15 downto 0) ; 
	signal max_7_O : std_logic_vector(15 downto 0) ; 

	signal max_8_I0 : std_logic_vector(15 downto 0) ; 
	signal max_8_I1 : std_logic_vector(15 downto 0) ; 
	signal max_8_O : std_logic_vector(15 downto 0) ; 

	signal max_9_I0 : std_logic_vector(15 downto 0) ; 
	signal max_9_I1 : std_logic_vector(15 downto 0) ; 
	signal max_9_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op10_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op10_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op10_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op11_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op11_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op11_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op12_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op12_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op12_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op13_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op13_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op13_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op14_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op14_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op14_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op15_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op15_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op15_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op16_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op16_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op16_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op10_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op11_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op12_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op13_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op14_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op15_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op16_S : std_logic ; 
	signal outreg7_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

max_0_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_1_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_2_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_3_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_4_O <= max_0_O when max_0_O>max_1_O else max_1_O;
max_5_O <= max_2_O when max_2_O>max_3_O else max_3_O;
max_6_O <= max_1_O when max_1_O>max_5_O else max_5_O;
max_7_O <= inreg1_Q when inreg1_Q>max_6_O else max_6_O;
max_8_O <= inreg3_Q when inreg3_Q>max_5_O else max_5_O;
max_9_O <= inreg5_Q when inreg5_Q>max_3_O else max_3_O;
	

	cmux_op10_O <= max_7_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op11_O <= max_6_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op12_O <= max_8_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op13_O <= max_5_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op14_O <= max_9_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op15_O <= max_3_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op16_O <= inreg7_Q when Shift_Shift= '0'	 else outreg6_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	max_0_I0 <= inreg0_Q;
	max_0_I1 <= inreg1_Q;
	max_1_I0 <= inreg2_Q;
	max_1_I1 <= inreg3_Q;
	max_2_I0 <= inreg4_Q;
	max_2_I1 <= inreg5_Q;
	max_3_I0 <= inreg6_Q;
	max_3_I1 <= inreg7_Q;
	max_4_I0 <= max_0_O;
	max_4_I1 <= max_1_O;
	max_5_I0 <= max_2_O;
	max_5_I1 <= max_3_O;
	max_6_I0 <= max_1_O;
	max_6_I1 <= max_5_O;
	max_7_I0 <= inreg1_Q;
	max_7_I1 <= max_6_O;
	max_8_I0 <= inreg3_Q;
	max_8_I1 <= max_5_O;
	max_9_I0 <= inreg5_Q;
	max_9_I1 <= max_3_O;
	outreg0_D <= max_4_O;
	cmux_op10_I0 <= max_7_O;
	cmux_op10_I1 <= outreg0_Q;
	outreg1_D <= cmux_op10_O;
	cmux_op11_I0 <= max_6_O;
	cmux_op11_I1 <= outreg1_Q;
	outreg2_D <= cmux_op11_O;
	cmux_op12_I0 <= max_8_O;
	cmux_op12_I1 <= outreg2_Q;
	outreg3_D <= cmux_op12_O;
	cmux_op13_I0 <= max_5_O;
	cmux_op13_I1 <= outreg3_Q;
	outreg4_D <= cmux_op13_O;
	cmux_op14_I0 <= max_9_O;
	cmux_op14_I1 <= outreg4_Q;
	outreg5_D <= cmux_op14_O;
	cmux_op15_I0 <= max_3_O;
	cmux_op15_I1 <= outreg5_Q;
	outreg6_D <= cmux_op15_O;
	cmux_op16_I0 <= inreg7_Q;
	cmux_op16_I1 <= outreg6_Q;
	outreg7_D <= cmux_op16_O;
	out_data_out_data <= outreg7_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op10_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op11_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op12_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op13_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op14_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op15_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op16_S <= Shift_Shift;
	outreg7_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_4_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op10_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op11_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op12_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op13_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op14_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op15_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op16_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
