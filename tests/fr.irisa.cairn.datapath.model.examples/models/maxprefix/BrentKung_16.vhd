library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity BrentKung_16 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end BrentKung_16;

architecture RTL of BrentKung_16 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal inreg8_D : std_logic_vector(15 downto 0) ; 
	signal inreg8_Q : std_logic_vector(15 downto 0) ; 

	signal inreg9_D : std_logic_vector(15 downto 0) ; 
	signal inreg9_Q : std_logic_vector(15 downto 0) ; 

	signal inreg10_D : std_logic_vector(15 downto 0) ; 
	signal inreg10_Q : std_logic_vector(15 downto 0) ; 

	signal inreg11_D : std_logic_vector(15 downto 0) ; 
	signal inreg11_Q : std_logic_vector(15 downto 0) ; 

	signal inreg12_D : std_logic_vector(15 downto 0) ; 
	signal inreg12_Q : std_logic_vector(15 downto 0) ; 

	signal inreg13_D : std_logic_vector(15 downto 0) ; 
	signal inreg13_Q : std_logic_vector(15 downto 0) ; 

	signal inreg14_D : std_logic_vector(15 downto 0) ; 
	signal inreg14_Q : std_logic_vector(15 downto 0) ; 

	signal inreg15_D : std_logic_vector(15 downto 0) ; 
	signal inreg15_Q : std_logic_vector(15 downto 0) ; 

	signal max_60_I0 : std_logic_vector(15 downto 0) ; 
	signal max_60_I1 : std_logic_vector(15 downto 0) ; 
	signal max_60_O : std_logic_vector(15 downto 0) ; 

	signal max_61_I0 : std_logic_vector(15 downto 0) ; 
	signal max_61_I1 : std_logic_vector(15 downto 0) ; 
	signal max_61_O : std_logic_vector(15 downto 0) ; 

	signal max_62_I0 : std_logic_vector(15 downto 0) ; 
	signal max_62_I1 : std_logic_vector(15 downto 0) ; 
	signal max_62_O : std_logic_vector(15 downto 0) ; 

	signal max_63_I0 : std_logic_vector(15 downto 0) ; 
	signal max_63_I1 : std_logic_vector(15 downto 0) ; 
	signal max_63_O : std_logic_vector(15 downto 0) ; 

	signal max_64_I0 : std_logic_vector(15 downto 0) ; 
	signal max_64_I1 : std_logic_vector(15 downto 0) ; 
	signal max_64_O : std_logic_vector(15 downto 0) ; 

	signal max_65_I0 : std_logic_vector(15 downto 0) ; 
	signal max_65_I1 : std_logic_vector(15 downto 0) ; 
	signal max_65_O : std_logic_vector(15 downto 0) ; 

	signal max_66_I0 : std_logic_vector(15 downto 0) ; 
	signal max_66_I1 : std_logic_vector(15 downto 0) ; 
	signal max_66_O : std_logic_vector(15 downto 0) ; 

	signal max_67_I0 : std_logic_vector(15 downto 0) ; 
	signal max_67_I1 : std_logic_vector(15 downto 0) ; 
	signal max_67_O : std_logic_vector(15 downto 0) ; 

	signal max_68_I0 : std_logic_vector(15 downto 0) ; 
	signal max_68_I1 : std_logic_vector(15 downto 0) ; 
	signal max_68_O : std_logic_vector(15 downto 0) ; 

	signal max_69_I0 : std_logic_vector(15 downto 0) ; 
	signal max_69_I1 : std_logic_vector(15 downto 0) ; 
	signal max_69_O : std_logic_vector(15 downto 0) ; 

	signal max_70_I0 : std_logic_vector(15 downto 0) ; 
	signal max_70_I1 : std_logic_vector(15 downto 0) ; 
	signal max_70_O : std_logic_vector(15 downto 0) ; 

	signal max_71_I0 : std_logic_vector(15 downto 0) ; 
	signal max_71_I1 : std_logic_vector(15 downto 0) ; 
	signal max_71_O : std_logic_vector(15 downto 0) ; 

	signal max_72_I0 : std_logic_vector(15 downto 0) ; 
	signal max_72_I1 : std_logic_vector(15 downto 0) ; 
	signal max_72_O : std_logic_vector(15 downto 0) ; 

	signal max_73_I0 : std_logic_vector(15 downto 0) ; 
	signal max_73_I1 : std_logic_vector(15 downto 0) ; 
	signal max_73_O : std_logic_vector(15 downto 0) ; 

	signal max_74_I0 : std_logic_vector(15 downto 0) ; 
	signal max_74_I1 : std_logic_vector(15 downto 0) ; 
	signal max_74_O : std_logic_vector(15 downto 0) ; 

	signal max_75_I0 : std_logic_vector(15 downto 0) ; 
	signal max_75_I1 : std_logic_vector(15 downto 0) ; 
	signal max_75_O : std_logic_vector(15 downto 0) ; 

	signal max_76_I0 : std_logic_vector(15 downto 0) ; 
	signal max_76_I1 : std_logic_vector(15 downto 0) ; 
	signal max_76_O : std_logic_vector(15 downto 0) ; 

	signal max_77_I0 : std_logic_vector(15 downto 0) ; 
	signal max_77_I1 : std_logic_vector(15 downto 0) ; 
	signal max_77_O : std_logic_vector(15 downto 0) ; 

	signal max_78_I0 : std_logic_vector(15 downto 0) ; 
	signal max_78_I1 : std_logic_vector(15 downto 0) ; 
	signal max_78_O : std_logic_vector(15 downto 0) ; 

	signal max_79_I0 : std_logic_vector(15 downto 0) ; 
	signal max_79_I1 : std_logic_vector(15 downto 0) ; 
	signal max_79_O : std_logic_vector(15 downto 0) ; 

	signal max_80_I0 : std_logic_vector(15 downto 0) ; 
	signal max_80_I1 : std_logic_vector(15 downto 0) ; 
	signal max_80_O : std_logic_vector(15 downto 0) ; 

	signal max_81_I0 : std_logic_vector(15 downto 0) ; 
	signal max_81_I1 : std_logic_vector(15 downto 0) ; 
	signal max_81_O : std_logic_vector(15 downto 0) ; 

	signal max_82_I0 : std_logic_vector(15 downto 0) ; 
	signal max_82_I1 : std_logic_vector(15 downto 0) ; 
	signal max_82_O : std_logic_vector(15 downto 0) ; 

	signal max_83_I0 : std_logic_vector(15 downto 0) ; 
	signal max_83_I1 : std_logic_vector(15 downto 0) ; 
	signal max_83_O : std_logic_vector(15 downto 0) ; 

	signal max_84_I0 : std_logic_vector(15 downto 0) ; 
	signal max_84_I1 : std_logic_vector(15 downto 0) ; 
	signal max_84_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op85_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op85_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op85_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op86_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op86_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op86_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op87_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op87_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op87_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op88_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op88_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op88_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op89_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op89_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op89_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op90_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op90_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op90_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op91_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op91_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op91_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op92_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op92_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op92_O : std_logic_vector(15 downto 0) ; 

	signal outreg8_D : std_logic_vector(15 downto 0) ; 
	signal outreg8_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op93_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op93_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op93_O : std_logic_vector(15 downto 0) ; 

	signal outreg9_D : std_logic_vector(15 downto 0) ; 
	signal outreg9_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op94_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op94_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op94_O : std_logic_vector(15 downto 0) ; 

	signal outreg10_D : std_logic_vector(15 downto 0) ; 
	signal outreg10_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op95_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op95_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op95_O : std_logic_vector(15 downto 0) ; 

	signal outreg11_D : std_logic_vector(15 downto 0) ; 
	signal outreg11_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op96_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op96_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op96_O : std_logic_vector(15 downto 0) ; 

	signal outreg12_D : std_logic_vector(15 downto 0) ; 
	signal outreg12_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op97_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op97_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op97_O : std_logic_vector(15 downto 0) ; 

	signal outreg13_D : std_logic_vector(15 downto 0) ; 
	signal outreg13_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op98_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op98_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op98_O : std_logic_vector(15 downto 0) ; 

	signal outreg14_D : std_logic_vector(15 downto 0) ; 
	signal outreg14_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op99_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op99_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op99_O : std_logic_vector(15 downto 0) ; 

	signal outreg15_D : std_logic_vector(15 downto 0) ; 
	signal outreg15_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal inreg8_CE : std_logic ; 
	signal inreg9_CE : std_logic ; 
	signal inreg10_CE : std_logic ; 
	signal inreg11_CE : std_logic ; 
	signal inreg12_CE : std_logic ; 
	signal inreg13_CE : std_logic ; 
	signal inreg14_CE : std_logic ; 
	signal inreg15_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op85_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op86_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op87_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op88_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op89_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op90_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op91_S : std_logic ; 
	signal outreg7_CE : std_logic ; 
	signal cmux_op92_S : std_logic ; 
	signal outreg8_CE : std_logic ; 
	signal cmux_op93_S : std_logic ; 
	signal outreg9_CE : std_logic ; 
	signal cmux_op94_S : std_logic ; 
	signal outreg10_CE : std_logic ; 
	signal cmux_op95_S : std_logic ; 
	signal outreg11_CE : std_logic ; 
	signal cmux_op96_S : std_logic ; 
	signal outreg12_CE : std_logic ; 
	signal cmux_op97_S : std_logic ; 
	signal outreg13_CE : std_logic ; 
	signal cmux_op98_S : std_logic ; 
	signal outreg14_CE : std_logic ; 
	signal cmux_op99_S : std_logic ; 
	signal outreg15_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

max_60_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_61_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_62_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_63_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_64_O <= inreg8_Q when inreg8_Q>inreg9_Q else inreg9_Q;
max_65_O <= inreg10_Q when inreg10_Q>inreg11_Q else inreg11_Q;
max_66_O <= inreg12_Q when inreg12_Q>inreg13_Q else inreg13_Q;
max_67_O <= inreg14_Q when inreg14_Q>inreg15_Q else inreg15_Q;
max_68_O <= max_60_O when max_60_O>max_61_O else max_61_O;
max_69_O <= max_62_O when max_62_O>max_63_O else max_63_O;
max_70_O <= max_64_O when max_64_O>max_65_O else max_65_O;
max_71_O <= max_66_O when max_66_O>max_67_O else max_67_O;
max_72_O <= max_68_O when max_68_O>max_69_O else max_69_O;
max_73_O <= max_70_O when max_70_O>max_71_O else max_71_O;
max_74_O <= max_69_O when max_69_O>max_73_O else max_73_O;
max_75_O <= max_61_O when max_61_O>max_74_O else max_74_O;
max_76_O <= max_63_O when max_63_O>max_73_O else max_73_O;
max_77_O <= max_65_O when max_65_O>max_71_O else max_71_O;
max_78_O <= inreg1_Q when inreg1_Q>max_75_O else max_75_O;
max_79_O <= inreg3_Q when inreg3_Q>max_74_O else max_74_O;
max_80_O <= inreg5_Q when inreg5_Q>max_76_O else max_76_O;
max_81_O <= inreg7_Q when inreg7_Q>max_73_O else max_73_O;
max_82_O <= inreg9_Q when inreg9_Q>max_77_O else max_77_O;
max_83_O <= inreg11_Q when inreg11_Q>max_71_O else max_71_O;
max_84_O <= inreg13_Q when inreg13_Q>max_67_O else max_67_O;
	

	cmux_op85_O <= max_78_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op86_O <= max_75_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op87_O <= max_79_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op88_O <= max_74_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op89_O <= max_80_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op90_O <= max_76_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op91_O <= max_81_O when Shift_Shift= '0'	 else outreg6_Q;
	

	cmux_op92_O <= max_73_O when Shift_Shift= '0'	 else outreg7_Q;
	

	cmux_op93_O <= max_82_O when Shift_Shift= '0'	 else outreg8_Q;
	

	cmux_op94_O <= max_77_O when Shift_Shift= '0'	 else outreg9_Q;
	

	cmux_op95_O <= max_83_O when Shift_Shift= '0'	 else outreg10_Q;
	

	cmux_op96_O <= max_71_O when Shift_Shift= '0'	 else outreg11_Q;
	

	cmux_op97_O <= max_84_O when Shift_Shift= '0'	 else outreg12_Q;
	

	cmux_op98_O <= max_67_O when Shift_Shift= '0'	 else outreg13_Q;
	

	cmux_op99_O <= inreg15_Q when Shift_Shift= '0'	 else outreg14_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	inreg8_D <= inreg7_Q;
	inreg9_D <= inreg8_Q;
	inreg10_D <= inreg9_Q;
	inreg11_D <= inreg10_Q;
	inreg12_D <= inreg11_Q;
	inreg13_D <= inreg12_Q;
	inreg14_D <= inreg13_Q;
	inreg15_D <= inreg14_Q;
	max_60_I0 <= inreg0_Q;
	max_60_I1 <= inreg1_Q;
	max_61_I0 <= inreg2_Q;
	max_61_I1 <= inreg3_Q;
	max_62_I0 <= inreg4_Q;
	max_62_I1 <= inreg5_Q;
	max_63_I0 <= inreg6_Q;
	max_63_I1 <= inreg7_Q;
	max_64_I0 <= inreg8_Q;
	max_64_I1 <= inreg9_Q;
	max_65_I0 <= inreg10_Q;
	max_65_I1 <= inreg11_Q;
	max_66_I0 <= inreg12_Q;
	max_66_I1 <= inreg13_Q;
	max_67_I0 <= inreg14_Q;
	max_67_I1 <= inreg15_Q;
	max_68_I0 <= max_60_O;
	max_68_I1 <= max_61_O;
	max_69_I0 <= max_62_O;
	max_69_I1 <= max_63_O;
	max_70_I0 <= max_64_O;
	max_70_I1 <= max_65_O;
	max_71_I0 <= max_66_O;
	max_71_I1 <= max_67_O;
	max_72_I0 <= max_68_O;
	max_72_I1 <= max_69_O;
	max_73_I0 <= max_70_O;
	max_73_I1 <= max_71_O;
	max_74_I0 <= max_69_O;
	max_74_I1 <= max_73_O;
	max_75_I0 <= max_61_O;
	max_75_I1 <= max_74_O;
	max_76_I0 <= max_63_O;
	max_76_I1 <= max_73_O;
	max_77_I0 <= max_65_O;
	max_77_I1 <= max_71_O;
	max_78_I0 <= inreg1_Q;
	max_78_I1 <= max_75_O;
	max_79_I0 <= inreg3_Q;
	max_79_I1 <= max_74_O;
	max_80_I0 <= inreg5_Q;
	max_80_I1 <= max_76_O;
	max_81_I0 <= inreg7_Q;
	max_81_I1 <= max_73_O;
	max_82_I0 <= inreg9_Q;
	max_82_I1 <= max_77_O;
	max_83_I0 <= inreg11_Q;
	max_83_I1 <= max_71_O;
	max_84_I0 <= inreg13_Q;
	max_84_I1 <= max_67_O;
	outreg0_D <= max_72_O;
	cmux_op85_I0 <= max_78_O;
	cmux_op85_I1 <= outreg0_Q;
	outreg1_D <= cmux_op85_O;
	cmux_op86_I0 <= max_75_O;
	cmux_op86_I1 <= outreg1_Q;
	outreg2_D <= cmux_op86_O;
	cmux_op87_I0 <= max_79_O;
	cmux_op87_I1 <= outreg2_Q;
	outreg3_D <= cmux_op87_O;
	cmux_op88_I0 <= max_74_O;
	cmux_op88_I1 <= outreg3_Q;
	outreg4_D <= cmux_op88_O;
	cmux_op89_I0 <= max_80_O;
	cmux_op89_I1 <= outreg4_Q;
	outreg5_D <= cmux_op89_O;
	cmux_op90_I0 <= max_76_O;
	cmux_op90_I1 <= outreg5_Q;
	outreg6_D <= cmux_op90_O;
	cmux_op91_I0 <= max_81_O;
	cmux_op91_I1 <= outreg6_Q;
	outreg7_D <= cmux_op91_O;
	cmux_op92_I0 <= max_73_O;
	cmux_op92_I1 <= outreg7_Q;
	outreg8_D <= cmux_op92_O;
	cmux_op93_I0 <= max_82_O;
	cmux_op93_I1 <= outreg8_Q;
	outreg9_D <= cmux_op93_O;
	cmux_op94_I0 <= max_77_O;
	cmux_op94_I1 <= outreg9_Q;
	outreg10_D <= cmux_op94_O;
	cmux_op95_I0 <= max_83_O;
	cmux_op95_I1 <= outreg10_Q;
	outreg11_D <= cmux_op95_O;
	cmux_op96_I0 <= max_71_O;
	cmux_op96_I1 <= outreg11_Q;
	outreg12_D <= cmux_op96_O;
	cmux_op97_I0 <= max_84_O;
	cmux_op97_I1 <= outreg12_Q;
	outreg13_D <= cmux_op97_O;
	cmux_op98_I0 <= max_67_O;
	cmux_op98_I1 <= outreg13_Q;
	outreg14_D <= cmux_op98_O;
	cmux_op99_I0 <= inreg15_Q;
	cmux_op99_I1 <= outreg14_Q;
	outreg15_D <= cmux_op99_O;
	out_data_out_data <= outreg15_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	inreg8_CE <= Shift_Shift;
	inreg9_CE <= Shift_Shift;
	inreg10_CE <= Shift_Shift;
	inreg11_CE <= Shift_Shift;
	inreg12_CE <= Shift_Shift;
	inreg13_CE <= Shift_Shift;
	inreg14_CE <= Shift_Shift;
	inreg15_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op85_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op86_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op87_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op88_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op89_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op90_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op91_S <= Shift_Shift;
	outreg7_CE <= CE_CE;
	cmux_op92_S <= Shift_Shift;
	outreg8_CE <= CE_CE;
	cmux_op93_S <= Shift_Shift;
	outreg9_CE <= CE_CE;
	cmux_op94_S <= Shift_Shift;
	outreg10_CE <= CE_CE;
	cmux_op95_S <= Shift_Shift;
	outreg11_CE <= CE_CE;
	cmux_op96_S <= Shift_Shift;
	outreg12_CE <= CE_CE;
	cmux_op97_S <= Shift_Shift;
	outreg13_CE <= CE_CE;
	cmux_op98_S <= Shift_Shift;
	outreg14_CE <= CE_CE;
	cmux_op99_S <= Shift_Shift;
	outreg15_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	inreg8_Q <= (others => '0');

	inreg9_Q <= (others => '0');

	inreg10_Q <= (others => '0');

	inreg11_Q <= (others => '0');

	inreg12_Q <= (others => '0');

	inreg13_Q <= (others => '0');

	inreg14_Q <= (others => '0');

	inreg15_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

	outreg8_Q <= (others => '0');

	outreg9_Q <= (others => '0');

	outreg10_Q <= (others => '0');

	outreg11_Q <= (others => '0');

	outreg12_Q <= (others => '0');

	outreg13_Q <= (others => '0');

	outreg14_Q <= (others => '0');

	outreg15_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg8_Q <= inreg7_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg9_Q <= inreg8_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg10_Q <= inreg9_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg11_Q <= inreg10_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg12_Q <= inreg11_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg13_Q <= inreg12_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg14_Q <= inreg13_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg15_Q <= inreg14_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_72_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op85_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op86_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op87_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op88_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op89_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op90_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op91_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg8_Q <= cmux_op92_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg9_Q <= cmux_op93_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg10_Q <= cmux_op94_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg11_Q <= cmux_op95_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg12_Q <= cmux_op96_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg13_Q <= cmux_op97_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg14_Q <= cmux_op98_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg15_Q <= cmux_op99_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
