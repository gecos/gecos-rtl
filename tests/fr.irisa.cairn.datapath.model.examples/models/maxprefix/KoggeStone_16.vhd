library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity KoggeStone_16 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end KoggeStone_16;

architecture RTL of KoggeStone_16 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal inreg8_D : std_logic_vector(15 downto 0) ; 
	signal inreg8_Q : std_logic_vector(15 downto 0) ; 

	signal inreg9_D : std_logic_vector(15 downto 0) ; 
	signal inreg9_Q : std_logic_vector(15 downto 0) ; 

	signal inreg10_D : std_logic_vector(15 downto 0) ; 
	signal inreg10_Q : std_logic_vector(15 downto 0) ; 

	signal inreg11_D : std_logic_vector(15 downto 0) ; 
	signal inreg11_Q : std_logic_vector(15 downto 0) ; 

	signal inreg12_D : std_logic_vector(15 downto 0) ; 
	signal inreg12_Q : std_logic_vector(15 downto 0) ; 

	signal inreg13_D : std_logic_vector(15 downto 0) ; 
	signal inreg13_Q : std_logic_vector(15 downto 0) ; 

	signal inreg14_D : std_logic_vector(15 downto 0) ; 
	signal inreg14_Q : std_logic_vector(15 downto 0) ; 

	signal inreg15_D : std_logic_vector(15 downto 0) ; 
	signal inreg15_Q : std_logic_vector(15 downto 0) ; 

	signal max_147_I0 : std_logic_vector(15 downto 0) ; 
	signal max_147_I1 : std_logic_vector(15 downto 0) ; 
	signal max_147_O : std_logic_vector(15 downto 0) ; 

	signal max_148_I0 : std_logic_vector(15 downto 0) ; 
	signal max_148_I1 : std_logic_vector(15 downto 0) ; 
	signal max_148_O : std_logic_vector(15 downto 0) ; 

	signal max_149_I0 : std_logic_vector(15 downto 0) ; 
	signal max_149_I1 : std_logic_vector(15 downto 0) ; 
	signal max_149_O : std_logic_vector(15 downto 0) ; 

	signal max_150_I0 : std_logic_vector(15 downto 0) ; 
	signal max_150_I1 : std_logic_vector(15 downto 0) ; 
	signal max_150_O : std_logic_vector(15 downto 0) ; 

	signal max_151_I0 : std_logic_vector(15 downto 0) ; 
	signal max_151_I1 : std_logic_vector(15 downto 0) ; 
	signal max_151_O : std_logic_vector(15 downto 0) ; 

	signal max_152_I0 : std_logic_vector(15 downto 0) ; 
	signal max_152_I1 : std_logic_vector(15 downto 0) ; 
	signal max_152_O : std_logic_vector(15 downto 0) ; 

	signal max_153_I0 : std_logic_vector(15 downto 0) ; 
	signal max_153_I1 : std_logic_vector(15 downto 0) ; 
	signal max_153_O : std_logic_vector(15 downto 0) ; 

	signal max_154_I0 : std_logic_vector(15 downto 0) ; 
	signal max_154_I1 : std_logic_vector(15 downto 0) ; 
	signal max_154_O : std_logic_vector(15 downto 0) ; 

	signal max_155_I0 : std_logic_vector(15 downto 0) ; 
	signal max_155_I1 : std_logic_vector(15 downto 0) ; 
	signal max_155_O : std_logic_vector(15 downto 0) ; 

	signal max_156_I0 : std_logic_vector(15 downto 0) ; 
	signal max_156_I1 : std_logic_vector(15 downto 0) ; 
	signal max_156_O : std_logic_vector(15 downto 0) ; 

	signal max_157_I0 : std_logic_vector(15 downto 0) ; 
	signal max_157_I1 : std_logic_vector(15 downto 0) ; 
	signal max_157_O : std_logic_vector(15 downto 0) ; 

	signal max_158_I0 : std_logic_vector(15 downto 0) ; 
	signal max_158_I1 : std_logic_vector(15 downto 0) ; 
	signal max_158_O : std_logic_vector(15 downto 0) ; 

	signal max_159_I0 : std_logic_vector(15 downto 0) ; 
	signal max_159_I1 : std_logic_vector(15 downto 0) ; 
	signal max_159_O : std_logic_vector(15 downto 0) ; 

	signal max_160_I0 : std_logic_vector(15 downto 0) ; 
	signal max_160_I1 : std_logic_vector(15 downto 0) ; 
	signal max_160_O : std_logic_vector(15 downto 0) ; 

	signal max_161_I0 : std_logic_vector(15 downto 0) ; 
	signal max_161_I1 : std_logic_vector(15 downto 0) ; 
	signal max_161_O : std_logic_vector(15 downto 0) ; 

	signal max_162_I0 : std_logic_vector(15 downto 0) ; 
	signal max_162_I1 : std_logic_vector(15 downto 0) ; 
	signal max_162_O : std_logic_vector(15 downto 0) ; 

	signal max_163_I0 : std_logic_vector(15 downto 0) ; 
	signal max_163_I1 : std_logic_vector(15 downto 0) ; 
	signal max_163_O : std_logic_vector(15 downto 0) ; 

	signal max_164_I0 : std_logic_vector(15 downto 0) ; 
	signal max_164_I1 : std_logic_vector(15 downto 0) ; 
	signal max_164_O : std_logic_vector(15 downto 0) ; 

	signal max_165_I0 : std_logic_vector(15 downto 0) ; 
	signal max_165_I1 : std_logic_vector(15 downto 0) ; 
	signal max_165_O : std_logic_vector(15 downto 0) ; 

	signal max_166_I0 : std_logic_vector(15 downto 0) ; 
	signal max_166_I1 : std_logic_vector(15 downto 0) ; 
	signal max_166_O : std_logic_vector(15 downto 0) ; 

	signal max_167_I0 : std_logic_vector(15 downto 0) ; 
	signal max_167_I1 : std_logic_vector(15 downto 0) ; 
	signal max_167_O : std_logic_vector(15 downto 0) ; 

	signal max_168_I0 : std_logic_vector(15 downto 0) ; 
	signal max_168_I1 : std_logic_vector(15 downto 0) ; 
	signal max_168_O : std_logic_vector(15 downto 0) ; 

	signal max_169_I0 : std_logic_vector(15 downto 0) ; 
	signal max_169_I1 : std_logic_vector(15 downto 0) ; 
	signal max_169_O : std_logic_vector(15 downto 0) ; 

	signal max_170_I0 : std_logic_vector(15 downto 0) ; 
	signal max_170_I1 : std_logic_vector(15 downto 0) ; 
	signal max_170_O : std_logic_vector(15 downto 0) ; 

	signal max_171_I0 : std_logic_vector(15 downto 0) ; 
	signal max_171_I1 : std_logic_vector(15 downto 0) ; 
	signal max_171_O : std_logic_vector(15 downto 0) ; 

	signal max_172_I0 : std_logic_vector(15 downto 0) ; 
	signal max_172_I1 : std_logic_vector(15 downto 0) ; 
	signal max_172_O : std_logic_vector(15 downto 0) ; 

	signal max_173_I0 : std_logic_vector(15 downto 0) ; 
	signal max_173_I1 : std_logic_vector(15 downto 0) ; 
	signal max_173_O : std_logic_vector(15 downto 0) ; 

	signal max_174_I0 : std_logic_vector(15 downto 0) ; 
	signal max_174_I1 : std_logic_vector(15 downto 0) ; 
	signal max_174_O : std_logic_vector(15 downto 0) ; 

	signal max_175_I0 : std_logic_vector(15 downto 0) ; 
	signal max_175_I1 : std_logic_vector(15 downto 0) ; 
	signal max_175_O : std_logic_vector(15 downto 0) ; 

	signal max_176_I0 : std_logic_vector(15 downto 0) ; 
	signal max_176_I1 : std_logic_vector(15 downto 0) ; 
	signal max_176_O : std_logic_vector(15 downto 0) ; 

	signal max_177_I0 : std_logic_vector(15 downto 0) ; 
	signal max_177_I1 : std_logic_vector(15 downto 0) ; 
	signal max_177_O : std_logic_vector(15 downto 0) ; 

	signal max_178_I0 : std_logic_vector(15 downto 0) ; 
	signal max_178_I1 : std_logic_vector(15 downto 0) ; 
	signal max_178_O : std_logic_vector(15 downto 0) ; 

	signal max_179_I0 : std_logic_vector(15 downto 0) ; 
	signal max_179_I1 : std_logic_vector(15 downto 0) ; 
	signal max_179_O : std_logic_vector(15 downto 0) ; 

	signal max_180_I0 : std_logic_vector(15 downto 0) ; 
	signal max_180_I1 : std_logic_vector(15 downto 0) ; 
	signal max_180_O : std_logic_vector(15 downto 0) ; 

	signal max_181_I0 : std_logic_vector(15 downto 0) ; 
	signal max_181_I1 : std_logic_vector(15 downto 0) ; 
	signal max_181_O : std_logic_vector(15 downto 0) ; 

	signal max_182_I0 : std_logic_vector(15 downto 0) ; 
	signal max_182_I1 : std_logic_vector(15 downto 0) ; 
	signal max_182_O : std_logic_vector(15 downto 0) ; 

	signal max_183_I0 : std_logic_vector(15 downto 0) ; 
	signal max_183_I1 : std_logic_vector(15 downto 0) ; 
	signal max_183_O : std_logic_vector(15 downto 0) ; 

	signal max_184_I0 : std_logic_vector(15 downto 0) ; 
	signal max_184_I1 : std_logic_vector(15 downto 0) ; 
	signal max_184_O : std_logic_vector(15 downto 0) ; 

	signal max_185_I0 : std_logic_vector(15 downto 0) ; 
	signal max_185_I1 : std_logic_vector(15 downto 0) ; 
	signal max_185_O : std_logic_vector(15 downto 0) ; 

	signal max_186_I0 : std_logic_vector(15 downto 0) ; 
	signal max_186_I1 : std_logic_vector(15 downto 0) ; 
	signal max_186_O : std_logic_vector(15 downto 0) ; 

	signal max_187_I0 : std_logic_vector(15 downto 0) ; 
	signal max_187_I1 : std_logic_vector(15 downto 0) ; 
	signal max_187_O : std_logic_vector(15 downto 0) ; 

	signal max_188_I0 : std_logic_vector(15 downto 0) ; 
	signal max_188_I1 : std_logic_vector(15 downto 0) ; 
	signal max_188_O : std_logic_vector(15 downto 0) ; 

	signal max_189_I0 : std_logic_vector(15 downto 0) ; 
	signal max_189_I1 : std_logic_vector(15 downto 0) ; 
	signal max_189_O : std_logic_vector(15 downto 0) ; 

	signal max_190_I0 : std_logic_vector(15 downto 0) ; 
	signal max_190_I1 : std_logic_vector(15 downto 0) ; 
	signal max_190_O : std_logic_vector(15 downto 0) ; 

	signal max_191_I0 : std_logic_vector(15 downto 0) ; 
	signal max_191_I1 : std_logic_vector(15 downto 0) ; 
	signal max_191_O : std_logic_vector(15 downto 0) ; 

	signal max_192_I0 : std_logic_vector(15 downto 0) ; 
	signal max_192_I1 : std_logic_vector(15 downto 0) ; 
	signal max_192_O : std_logic_vector(15 downto 0) ; 

	signal max_193_I0 : std_logic_vector(15 downto 0) ; 
	signal max_193_I1 : std_logic_vector(15 downto 0) ; 
	signal max_193_O : std_logic_vector(15 downto 0) ; 

	signal max_194_I0 : std_logic_vector(15 downto 0) ; 
	signal max_194_I1 : std_logic_vector(15 downto 0) ; 
	signal max_194_O : std_logic_vector(15 downto 0) ; 

	signal max_195_I0 : std_logic_vector(15 downto 0) ; 
	signal max_195_I1 : std_logic_vector(15 downto 0) ; 
	signal max_195_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op196_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op196_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op196_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op197_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op197_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op197_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op198_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op198_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op198_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op199_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op199_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op199_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op200_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op200_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op200_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op201_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op201_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op201_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op202_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op202_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op202_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op203_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op203_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op203_O : std_logic_vector(15 downto 0) ; 

	signal outreg8_D : std_logic_vector(15 downto 0) ; 
	signal outreg8_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op204_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op204_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op204_O : std_logic_vector(15 downto 0) ; 

	signal outreg9_D : std_logic_vector(15 downto 0) ; 
	signal outreg9_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op205_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op205_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op205_O : std_logic_vector(15 downto 0) ; 

	signal outreg10_D : std_logic_vector(15 downto 0) ; 
	signal outreg10_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op206_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op206_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op206_O : std_logic_vector(15 downto 0) ; 

	signal outreg11_D : std_logic_vector(15 downto 0) ; 
	signal outreg11_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op207_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op207_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op207_O : std_logic_vector(15 downto 0) ; 

	signal outreg12_D : std_logic_vector(15 downto 0) ; 
	signal outreg12_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op208_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op208_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op208_O : std_logic_vector(15 downto 0) ; 

	signal outreg13_D : std_logic_vector(15 downto 0) ; 
	signal outreg13_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op209_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op209_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op209_O : std_logic_vector(15 downto 0) ; 

	signal outreg14_D : std_logic_vector(15 downto 0) ; 
	signal outreg14_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op210_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op210_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op210_O : std_logic_vector(15 downto 0) ; 

	signal outreg15_D : std_logic_vector(15 downto 0) ; 
	signal outreg15_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal inreg8_CE : std_logic ; 
	signal inreg9_CE : std_logic ; 
	signal inreg10_CE : std_logic ; 
	signal inreg11_CE : std_logic ; 
	signal inreg12_CE : std_logic ; 
	signal inreg13_CE : std_logic ; 
	signal inreg14_CE : std_logic ; 
	signal inreg15_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op196_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op197_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op198_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op199_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op200_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op201_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op202_S : std_logic ; 
	signal outreg7_CE : std_logic ; 
	signal cmux_op203_S : std_logic ; 
	signal outreg8_CE : std_logic ; 
	signal cmux_op204_S : std_logic ; 
	signal outreg9_CE : std_logic ; 
	signal cmux_op205_S : std_logic ; 
	signal outreg10_CE : std_logic ; 
	signal cmux_op206_S : std_logic ; 
	signal outreg11_CE : std_logic ; 
	signal cmux_op207_S : std_logic ; 
	signal outreg12_CE : std_logic ; 
	signal cmux_op208_S : std_logic ; 
	signal outreg13_CE : std_logic ; 
	signal cmux_op209_S : std_logic ; 
	signal outreg14_CE : std_logic ; 
	signal cmux_op210_S : std_logic ; 
	signal outreg15_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

max_147_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_148_O <= inreg1_Q when inreg1_Q>inreg2_Q else inreg2_Q;
max_149_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_150_O <= inreg3_Q when inreg3_Q>inreg4_Q else inreg4_Q;
max_151_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_152_O <= inreg5_Q when inreg5_Q>inreg6_Q else inreg6_Q;
max_153_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_154_O <= inreg7_Q when inreg7_Q>inreg8_Q else inreg8_Q;
max_155_O <= inreg8_Q when inreg8_Q>inreg9_Q else inreg9_Q;
max_156_O <= inreg9_Q when inreg9_Q>inreg10_Q else inreg10_Q;
max_157_O <= inreg10_Q when inreg10_Q>inreg11_Q else inreg11_Q;
max_158_O <= inreg11_Q when inreg11_Q>inreg12_Q else inreg12_Q;
max_159_O <= inreg12_Q when inreg12_Q>inreg13_Q else inreg13_Q;
max_160_O <= inreg13_Q when inreg13_Q>inreg14_Q else inreg14_Q;
max_161_O <= inreg14_Q when inreg14_Q>inreg15_Q else inreg15_Q;
max_162_O <= max_147_O when max_147_O>max_149_O else max_149_O;
max_163_O <= max_148_O when max_148_O>max_150_O else max_150_O;
max_164_O <= max_149_O when max_149_O>max_151_O else max_151_O;
max_165_O <= max_150_O when max_150_O>max_152_O else max_152_O;
max_166_O <= max_151_O when max_151_O>max_153_O else max_153_O;
max_167_O <= max_152_O when max_152_O>max_154_O else max_154_O;
max_168_O <= max_153_O when max_153_O>max_155_O else max_155_O;
max_169_O <= max_154_O when max_154_O>max_156_O else max_156_O;
max_170_O <= max_155_O when max_155_O>max_157_O else max_157_O;
max_171_O <= max_156_O when max_156_O>max_158_O else max_158_O;
max_172_O <= max_157_O when max_157_O>max_159_O else max_159_O;
max_173_O <= max_158_O when max_158_O>max_160_O else max_160_O;
max_174_O <= max_159_O when max_159_O>max_161_O else max_161_O;
max_175_O <= max_160_O when max_160_O>inreg15_Q else inreg15_Q;
max_176_O <= max_162_O when max_162_O>max_166_O else max_166_O;
max_177_O <= max_163_O when max_163_O>max_167_O else max_167_O;
max_178_O <= max_164_O when max_164_O>max_168_O else max_168_O;
max_179_O <= max_165_O when max_165_O>max_169_O else max_169_O;
max_180_O <= max_166_O when max_166_O>max_170_O else max_170_O;
max_181_O <= max_167_O when max_167_O>max_171_O else max_171_O;
max_182_O <= max_168_O when max_168_O>max_172_O else max_172_O;
max_183_O <= max_169_O when max_169_O>max_173_O else max_173_O;
max_184_O <= max_170_O when max_170_O>max_174_O else max_174_O;
max_185_O <= max_171_O when max_171_O>max_175_O else max_175_O;
max_186_O <= max_172_O when max_172_O>max_161_O else max_161_O;
max_187_O <= max_173_O when max_173_O>inreg15_Q else inreg15_Q;
max_188_O <= max_176_O when max_176_O>max_184_O else max_184_O;
max_189_O <= max_177_O when max_177_O>max_185_O else max_185_O;
max_190_O <= max_178_O when max_178_O>max_186_O else max_186_O;
max_191_O <= max_179_O when max_179_O>max_187_O else max_187_O;
max_192_O <= max_180_O when max_180_O>max_174_O else max_174_O;
max_193_O <= max_181_O when max_181_O>max_175_O else max_175_O;
max_194_O <= max_182_O when max_182_O>max_161_O else max_161_O;
max_195_O <= max_183_O when max_183_O>inreg15_Q else inreg15_Q;
	

	cmux_op196_O <= max_189_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op197_O <= max_190_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op198_O <= max_191_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op199_O <= max_192_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op200_O <= max_193_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op201_O <= max_194_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op202_O <= max_195_O when Shift_Shift= '0'	 else outreg6_Q;
	

	cmux_op203_O <= max_184_O when Shift_Shift= '0'	 else outreg7_Q;
	

	cmux_op204_O <= max_185_O when Shift_Shift= '0'	 else outreg8_Q;
	

	cmux_op205_O <= max_186_O when Shift_Shift= '0'	 else outreg9_Q;
	

	cmux_op206_O <= max_187_O when Shift_Shift= '0'	 else outreg10_Q;
	

	cmux_op207_O <= max_174_O when Shift_Shift= '0'	 else outreg11_Q;
	

	cmux_op208_O <= max_175_O when Shift_Shift= '0'	 else outreg12_Q;
	

	cmux_op209_O <= max_161_O when Shift_Shift= '0'	 else outreg13_Q;
	

	cmux_op210_O <= inreg15_Q when Shift_Shift= '0'	 else outreg14_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	inreg8_D <= inreg7_Q;
	inreg9_D <= inreg8_Q;
	inreg10_D <= inreg9_Q;
	inreg11_D <= inreg10_Q;
	inreg12_D <= inreg11_Q;
	inreg13_D <= inreg12_Q;
	inreg14_D <= inreg13_Q;
	inreg15_D <= inreg14_Q;
	max_147_I0 <= inreg0_Q;
	max_147_I1 <= inreg1_Q;
	max_148_I0 <= inreg1_Q;
	max_148_I1 <= inreg2_Q;
	max_149_I0 <= inreg2_Q;
	max_149_I1 <= inreg3_Q;
	max_150_I0 <= inreg3_Q;
	max_150_I1 <= inreg4_Q;
	max_151_I0 <= inreg4_Q;
	max_151_I1 <= inreg5_Q;
	max_152_I0 <= inreg5_Q;
	max_152_I1 <= inreg6_Q;
	max_153_I0 <= inreg6_Q;
	max_153_I1 <= inreg7_Q;
	max_154_I0 <= inreg7_Q;
	max_154_I1 <= inreg8_Q;
	max_155_I0 <= inreg8_Q;
	max_155_I1 <= inreg9_Q;
	max_156_I0 <= inreg9_Q;
	max_156_I1 <= inreg10_Q;
	max_157_I0 <= inreg10_Q;
	max_157_I1 <= inreg11_Q;
	max_158_I0 <= inreg11_Q;
	max_158_I1 <= inreg12_Q;
	max_159_I0 <= inreg12_Q;
	max_159_I1 <= inreg13_Q;
	max_160_I0 <= inreg13_Q;
	max_160_I1 <= inreg14_Q;
	max_161_I0 <= inreg14_Q;
	max_161_I1 <= inreg15_Q;
	max_162_I0 <= max_147_O;
	max_162_I1 <= max_149_O;
	max_163_I0 <= max_148_O;
	max_163_I1 <= max_150_O;
	max_164_I0 <= max_149_O;
	max_164_I1 <= max_151_O;
	max_165_I0 <= max_150_O;
	max_165_I1 <= max_152_O;
	max_166_I0 <= max_151_O;
	max_166_I1 <= max_153_O;
	max_167_I0 <= max_152_O;
	max_167_I1 <= max_154_O;
	max_168_I0 <= max_153_O;
	max_168_I1 <= max_155_O;
	max_169_I0 <= max_154_O;
	max_169_I1 <= max_156_O;
	max_170_I0 <= max_155_O;
	max_170_I1 <= max_157_O;
	max_171_I0 <= max_156_O;
	max_171_I1 <= max_158_O;
	max_172_I0 <= max_157_O;
	max_172_I1 <= max_159_O;
	max_173_I0 <= max_158_O;
	max_173_I1 <= max_160_O;
	max_174_I0 <= max_159_O;
	max_174_I1 <= max_161_O;
	max_175_I0 <= max_160_O;
	max_175_I1 <= inreg15_Q;
	max_176_I0 <= max_162_O;
	max_176_I1 <= max_166_O;
	max_177_I0 <= max_163_O;
	max_177_I1 <= max_167_O;
	max_178_I0 <= max_164_O;
	max_178_I1 <= max_168_O;
	max_179_I0 <= max_165_O;
	max_179_I1 <= max_169_O;
	max_180_I0 <= max_166_O;
	max_180_I1 <= max_170_O;
	max_181_I0 <= max_167_O;
	max_181_I1 <= max_171_O;
	max_182_I0 <= max_168_O;
	max_182_I1 <= max_172_O;
	max_183_I0 <= max_169_O;
	max_183_I1 <= max_173_O;
	max_184_I0 <= max_170_O;
	max_184_I1 <= max_174_O;
	max_185_I0 <= max_171_O;
	max_185_I1 <= max_175_O;
	max_186_I0 <= max_172_O;
	max_186_I1 <= max_161_O;
	max_187_I0 <= max_173_O;
	max_187_I1 <= inreg15_Q;
	max_188_I0 <= max_176_O;
	max_188_I1 <= max_184_O;
	max_189_I0 <= max_177_O;
	max_189_I1 <= max_185_O;
	max_190_I0 <= max_178_O;
	max_190_I1 <= max_186_O;
	max_191_I0 <= max_179_O;
	max_191_I1 <= max_187_O;
	max_192_I0 <= max_180_O;
	max_192_I1 <= max_174_O;
	max_193_I0 <= max_181_O;
	max_193_I1 <= max_175_O;
	max_194_I0 <= max_182_O;
	max_194_I1 <= max_161_O;
	max_195_I0 <= max_183_O;
	max_195_I1 <= inreg15_Q;
	outreg0_D <= max_188_O;
	cmux_op196_I0 <= max_189_O;
	cmux_op196_I1 <= outreg0_Q;
	outreg1_D <= cmux_op196_O;
	cmux_op197_I0 <= max_190_O;
	cmux_op197_I1 <= outreg1_Q;
	outreg2_D <= cmux_op197_O;
	cmux_op198_I0 <= max_191_O;
	cmux_op198_I1 <= outreg2_Q;
	outreg3_D <= cmux_op198_O;
	cmux_op199_I0 <= max_192_O;
	cmux_op199_I1 <= outreg3_Q;
	outreg4_D <= cmux_op199_O;
	cmux_op200_I0 <= max_193_O;
	cmux_op200_I1 <= outreg4_Q;
	outreg5_D <= cmux_op200_O;
	cmux_op201_I0 <= max_194_O;
	cmux_op201_I1 <= outreg5_Q;
	outreg6_D <= cmux_op201_O;
	cmux_op202_I0 <= max_195_O;
	cmux_op202_I1 <= outreg6_Q;
	outreg7_D <= cmux_op202_O;
	cmux_op203_I0 <= max_184_O;
	cmux_op203_I1 <= outreg7_Q;
	outreg8_D <= cmux_op203_O;
	cmux_op204_I0 <= max_185_O;
	cmux_op204_I1 <= outreg8_Q;
	outreg9_D <= cmux_op204_O;
	cmux_op205_I0 <= max_186_O;
	cmux_op205_I1 <= outreg9_Q;
	outreg10_D <= cmux_op205_O;
	cmux_op206_I0 <= max_187_O;
	cmux_op206_I1 <= outreg10_Q;
	outreg11_D <= cmux_op206_O;
	cmux_op207_I0 <= max_174_O;
	cmux_op207_I1 <= outreg11_Q;
	outreg12_D <= cmux_op207_O;
	cmux_op208_I0 <= max_175_O;
	cmux_op208_I1 <= outreg12_Q;
	outreg13_D <= cmux_op208_O;
	cmux_op209_I0 <= max_161_O;
	cmux_op209_I1 <= outreg13_Q;
	outreg14_D <= cmux_op209_O;
	cmux_op210_I0 <= inreg15_Q;
	cmux_op210_I1 <= outreg14_Q;
	outreg15_D <= cmux_op210_O;
	out_data_out_data <= outreg15_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	inreg8_CE <= Shift_Shift;
	inreg9_CE <= Shift_Shift;
	inreg10_CE <= Shift_Shift;
	inreg11_CE <= Shift_Shift;
	inreg12_CE <= Shift_Shift;
	inreg13_CE <= Shift_Shift;
	inreg14_CE <= Shift_Shift;
	inreg15_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op196_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op197_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op198_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op199_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op200_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op201_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op202_S <= Shift_Shift;
	outreg7_CE <= CE_CE;
	cmux_op203_S <= Shift_Shift;
	outreg8_CE <= CE_CE;
	cmux_op204_S <= Shift_Shift;
	outreg9_CE <= CE_CE;
	cmux_op205_S <= Shift_Shift;
	outreg10_CE <= CE_CE;
	cmux_op206_S <= Shift_Shift;
	outreg11_CE <= CE_CE;
	cmux_op207_S <= Shift_Shift;
	outreg12_CE <= CE_CE;
	cmux_op208_S <= Shift_Shift;
	outreg13_CE <= CE_CE;
	cmux_op209_S <= Shift_Shift;
	outreg14_CE <= CE_CE;
	cmux_op210_S <= Shift_Shift;
	outreg15_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	inreg8_Q <= (others => '0');

	inreg9_Q <= (others => '0');

	inreg10_Q <= (others => '0');

	inreg11_Q <= (others => '0');

	inreg12_Q <= (others => '0');

	inreg13_Q <= (others => '0');

	inreg14_Q <= (others => '0');

	inreg15_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

	outreg8_Q <= (others => '0');

	outreg9_Q <= (others => '0');

	outreg10_Q <= (others => '0');

	outreg11_Q <= (others => '0');

	outreg12_Q <= (others => '0');

	outreg13_Q <= (others => '0');

	outreg14_Q <= (others => '0');

	outreg15_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg8_Q <= inreg7_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg9_Q <= inreg8_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg10_Q <= inreg9_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg11_Q <= inreg10_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg12_Q <= inreg11_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg13_Q <= inreg12_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg14_Q <= inreg13_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg15_Q <= inreg14_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_188_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op196_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op197_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op198_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op199_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op200_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op201_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op202_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg8_Q <= cmux_op203_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg9_Q <= cmux_op204_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg10_Q <= cmux_op205_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg11_Q <= cmux_op206_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg12_Q <= cmux_op207_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg13_Q <= cmux_op208_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg14_Q <= cmux_op209_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg15_Q <= cmux_op210_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
