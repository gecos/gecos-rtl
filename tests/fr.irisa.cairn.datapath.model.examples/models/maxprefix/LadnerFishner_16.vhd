library ieee;
library work;

use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity LadnerFishner_16 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	Shift : in std_logic;
	-- Input Data Pads
	in_data : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	out_data : out std_logic_vector(15 downto 0)
	)
;
end LadnerFishner_16;

architecture RTL of LadnerFishner_16 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal in_data_in_data : std_logic_vector(15 downto 0) ; 

	signal out_data_out_data : std_logic_vector(15 downto 0) ; 

	signal inreg0_D : std_logic_vector(15 downto 0) ; 
	signal inreg0_Q : std_logic_vector(15 downto 0) ; 

	signal inreg1_D : std_logic_vector(15 downto 0) ; 
	signal inreg1_Q : std_logic_vector(15 downto 0) ; 

	signal inreg2_D : std_logic_vector(15 downto 0) ; 
	signal inreg2_Q : std_logic_vector(15 downto 0) ; 

	signal inreg3_D : std_logic_vector(15 downto 0) ; 
	signal inreg3_Q : std_logic_vector(15 downto 0) ; 

	signal inreg4_D : std_logic_vector(15 downto 0) ; 
	signal inreg4_Q : std_logic_vector(15 downto 0) ; 

	signal inreg5_D : std_logic_vector(15 downto 0) ; 
	signal inreg5_Q : std_logic_vector(15 downto 0) ; 

	signal inreg6_D : std_logic_vector(15 downto 0) ; 
	signal inreg6_Q : std_logic_vector(15 downto 0) ; 

	signal inreg7_D : std_logic_vector(15 downto 0) ; 
	signal inreg7_Q : std_logic_vector(15 downto 0) ; 

	signal inreg8_D : std_logic_vector(15 downto 0) ; 
	signal inreg8_Q : std_logic_vector(15 downto 0) ; 

	signal inreg9_D : std_logic_vector(15 downto 0) ; 
	signal inreg9_Q : std_logic_vector(15 downto 0) ; 

	signal inreg10_D : std_logic_vector(15 downto 0) ; 
	signal inreg10_Q : std_logic_vector(15 downto 0) ; 

	signal inreg11_D : std_logic_vector(15 downto 0) ; 
	signal inreg11_Q : std_logic_vector(15 downto 0) ; 

	signal inreg12_D : std_logic_vector(15 downto 0) ; 
	signal inreg12_Q : std_logic_vector(15 downto 0) ; 

	signal inreg13_D : std_logic_vector(15 downto 0) ; 
	signal inreg13_Q : std_logic_vector(15 downto 0) ; 

	signal inreg14_D : std_logic_vector(15 downto 0) ; 
	signal inreg14_Q : std_logic_vector(15 downto 0) ; 

	signal inreg15_D : std_logic_vector(15 downto 0) ; 
	signal inreg15_Q : std_logic_vector(15 downto 0) ; 

	signal max_100_I0 : std_logic_vector(15 downto 0) ; 
	signal max_100_I1 : std_logic_vector(15 downto 0) ; 
	signal max_100_O : std_logic_vector(15 downto 0) ; 

	signal max_101_I0 : std_logic_vector(15 downto 0) ; 
	signal max_101_I1 : std_logic_vector(15 downto 0) ; 
	signal max_101_O : std_logic_vector(15 downto 0) ; 

	signal max_102_I0 : std_logic_vector(15 downto 0) ; 
	signal max_102_I1 : std_logic_vector(15 downto 0) ; 
	signal max_102_O : std_logic_vector(15 downto 0) ; 

	signal max_103_I0 : std_logic_vector(15 downto 0) ; 
	signal max_103_I1 : std_logic_vector(15 downto 0) ; 
	signal max_103_O : std_logic_vector(15 downto 0) ; 

	signal max_104_I0 : std_logic_vector(15 downto 0) ; 
	signal max_104_I1 : std_logic_vector(15 downto 0) ; 
	signal max_104_O : std_logic_vector(15 downto 0) ; 

	signal max_105_I0 : std_logic_vector(15 downto 0) ; 
	signal max_105_I1 : std_logic_vector(15 downto 0) ; 
	signal max_105_O : std_logic_vector(15 downto 0) ; 

	signal max_106_I0 : std_logic_vector(15 downto 0) ; 
	signal max_106_I1 : std_logic_vector(15 downto 0) ; 
	signal max_106_O : std_logic_vector(15 downto 0) ; 

	signal max_107_I0 : std_logic_vector(15 downto 0) ; 
	signal max_107_I1 : std_logic_vector(15 downto 0) ; 
	signal max_107_O : std_logic_vector(15 downto 0) ; 

	signal max_108_I0 : std_logic_vector(15 downto 0) ; 
	signal max_108_I1 : std_logic_vector(15 downto 0) ; 
	signal max_108_O : std_logic_vector(15 downto 0) ; 

	signal max_109_I0 : std_logic_vector(15 downto 0) ; 
	signal max_109_I1 : std_logic_vector(15 downto 0) ; 
	signal max_109_O : std_logic_vector(15 downto 0) ; 

	signal max_110_I0 : std_logic_vector(15 downto 0) ; 
	signal max_110_I1 : std_logic_vector(15 downto 0) ; 
	signal max_110_O : std_logic_vector(15 downto 0) ; 

	signal max_111_I0 : std_logic_vector(15 downto 0) ; 
	signal max_111_I1 : std_logic_vector(15 downto 0) ; 
	signal max_111_O : std_logic_vector(15 downto 0) ; 

	signal max_112_I0 : std_logic_vector(15 downto 0) ; 
	signal max_112_I1 : std_logic_vector(15 downto 0) ; 
	signal max_112_O : std_logic_vector(15 downto 0) ; 

	signal max_113_I0 : std_logic_vector(15 downto 0) ; 
	signal max_113_I1 : std_logic_vector(15 downto 0) ; 
	signal max_113_O : std_logic_vector(15 downto 0) ; 

	signal max_114_I0 : std_logic_vector(15 downto 0) ; 
	signal max_114_I1 : std_logic_vector(15 downto 0) ; 
	signal max_114_O : std_logic_vector(15 downto 0) ; 

	signal max_115_I0 : std_logic_vector(15 downto 0) ; 
	signal max_115_I1 : std_logic_vector(15 downto 0) ; 
	signal max_115_O : std_logic_vector(15 downto 0) ; 

	signal max_116_I0 : std_logic_vector(15 downto 0) ; 
	signal max_116_I1 : std_logic_vector(15 downto 0) ; 
	signal max_116_O : std_logic_vector(15 downto 0) ; 

	signal max_117_I0 : std_logic_vector(15 downto 0) ; 
	signal max_117_I1 : std_logic_vector(15 downto 0) ; 
	signal max_117_O : std_logic_vector(15 downto 0) ; 

	signal max_118_I0 : std_logic_vector(15 downto 0) ; 
	signal max_118_I1 : std_logic_vector(15 downto 0) ; 
	signal max_118_O : std_logic_vector(15 downto 0) ; 

	signal max_119_I0 : std_logic_vector(15 downto 0) ; 
	signal max_119_I1 : std_logic_vector(15 downto 0) ; 
	signal max_119_O : std_logic_vector(15 downto 0) ; 

	signal max_120_I0 : std_logic_vector(15 downto 0) ; 
	signal max_120_I1 : std_logic_vector(15 downto 0) ; 
	signal max_120_O : std_logic_vector(15 downto 0) ; 

	signal max_121_I0 : std_logic_vector(15 downto 0) ; 
	signal max_121_I1 : std_logic_vector(15 downto 0) ; 
	signal max_121_O : std_logic_vector(15 downto 0) ; 

	signal max_122_I0 : std_logic_vector(15 downto 0) ; 
	signal max_122_I1 : std_logic_vector(15 downto 0) ; 
	signal max_122_O : std_logic_vector(15 downto 0) ; 

	signal max_123_I0 : std_logic_vector(15 downto 0) ; 
	signal max_123_I1 : std_logic_vector(15 downto 0) ; 
	signal max_123_O : std_logic_vector(15 downto 0) ; 

	signal max_124_I0 : std_logic_vector(15 downto 0) ; 
	signal max_124_I1 : std_logic_vector(15 downto 0) ; 
	signal max_124_O : std_logic_vector(15 downto 0) ; 

	signal max_125_I0 : std_logic_vector(15 downto 0) ; 
	signal max_125_I1 : std_logic_vector(15 downto 0) ; 
	signal max_125_O : std_logic_vector(15 downto 0) ; 

	signal max_126_I0 : std_logic_vector(15 downto 0) ; 
	signal max_126_I1 : std_logic_vector(15 downto 0) ; 
	signal max_126_O : std_logic_vector(15 downto 0) ; 

	signal max_127_I0 : std_logic_vector(15 downto 0) ; 
	signal max_127_I1 : std_logic_vector(15 downto 0) ; 
	signal max_127_O : std_logic_vector(15 downto 0) ; 

	signal max_128_I0 : std_logic_vector(15 downto 0) ; 
	signal max_128_I1 : std_logic_vector(15 downto 0) ; 
	signal max_128_O : std_logic_vector(15 downto 0) ; 

	signal max_129_I0 : std_logic_vector(15 downto 0) ; 
	signal max_129_I1 : std_logic_vector(15 downto 0) ; 
	signal max_129_O : std_logic_vector(15 downto 0) ; 

	signal max_130_I0 : std_logic_vector(15 downto 0) ; 
	signal max_130_I1 : std_logic_vector(15 downto 0) ; 
	signal max_130_O : std_logic_vector(15 downto 0) ; 

	signal max_131_I0 : std_logic_vector(15 downto 0) ; 
	signal max_131_I1 : std_logic_vector(15 downto 0) ; 
	signal max_131_O : std_logic_vector(15 downto 0) ; 

	signal outreg0_D : std_logic_vector(15 downto 0) ; 
	signal outreg0_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op132_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op132_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op132_O : std_logic_vector(15 downto 0) ; 

	signal outreg1_D : std_logic_vector(15 downto 0) ; 
	signal outreg1_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op133_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op133_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op133_O : std_logic_vector(15 downto 0) ; 

	signal outreg2_D : std_logic_vector(15 downto 0) ; 
	signal outreg2_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op134_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op134_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op134_O : std_logic_vector(15 downto 0) ; 

	signal outreg3_D : std_logic_vector(15 downto 0) ; 
	signal outreg3_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op135_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op135_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op135_O : std_logic_vector(15 downto 0) ; 

	signal outreg4_D : std_logic_vector(15 downto 0) ; 
	signal outreg4_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op136_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op136_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op136_O : std_logic_vector(15 downto 0) ; 

	signal outreg5_D : std_logic_vector(15 downto 0) ; 
	signal outreg5_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op137_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op137_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op137_O : std_logic_vector(15 downto 0) ; 

	signal outreg6_D : std_logic_vector(15 downto 0) ; 
	signal outreg6_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op138_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op138_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op138_O : std_logic_vector(15 downto 0) ; 

	signal outreg7_D : std_logic_vector(15 downto 0) ; 
	signal outreg7_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op139_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op139_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op139_O : std_logic_vector(15 downto 0) ; 

	signal outreg8_D : std_logic_vector(15 downto 0) ; 
	signal outreg8_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op140_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op140_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op140_O : std_logic_vector(15 downto 0) ; 

	signal outreg9_D : std_logic_vector(15 downto 0) ; 
	signal outreg9_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op141_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op141_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op141_O : std_logic_vector(15 downto 0) ; 

	signal outreg10_D : std_logic_vector(15 downto 0) ; 
	signal outreg10_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op142_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op142_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op142_O : std_logic_vector(15 downto 0) ; 

	signal outreg11_D : std_logic_vector(15 downto 0) ; 
	signal outreg11_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op143_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op143_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op143_O : std_logic_vector(15 downto 0) ; 

	signal outreg12_D : std_logic_vector(15 downto 0) ; 
	signal outreg12_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op144_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op144_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op144_O : std_logic_vector(15 downto 0) ; 

	signal outreg13_D : std_logic_vector(15 downto 0) ; 
	signal outreg13_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op145_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op145_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op145_O : std_logic_vector(15 downto 0) ; 

	signal outreg14_D : std_logic_vector(15 downto 0) ; 
	signal outreg14_Q : std_logic_vector(15 downto 0) ; 

	signal cmux_op146_I0 : std_logic_vector(15 downto 0) ; 
	signal cmux_op146_I1 : std_logic_vector(15 downto 0) ; 
	signal cmux_op146_O : std_logic_vector(15 downto 0) ; 

	signal outreg15_D : std_logic_vector(15 downto 0) ; 
	signal outreg15_Q : std_logic_vector(15 downto 0) ; 

	
	signal inreg0_CE : std_logic ; 
	signal inreg1_CE : std_logic ; 
	signal inreg2_CE : std_logic ; 
	signal inreg3_CE : std_logic ; 
	signal inreg4_CE : std_logic ; 
	signal inreg5_CE : std_logic ; 
	signal inreg6_CE : std_logic ; 
	signal inreg7_CE : std_logic ; 
	signal inreg8_CE : std_logic ; 
	signal inreg9_CE : std_logic ; 
	signal inreg10_CE : std_logic ; 
	signal inreg11_CE : std_logic ; 
	signal inreg12_CE : std_logic ; 
	signal inreg13_CE : std_logic ; 
	signal inreg14_CE : std_logic ; 
	signal inreg15_CE : std_logic ; 
	signal outreg0_CE : std_logic ; 
	signal cmux_op132_S : std_logic ; 
	signal outreg1_CE : std_logic ; 
	signal cmux_op133_S : std_logic ; 
	signal outreg2_CE : std_logic ; 
	signal cmux_op134_S : std_logic ; 
	signal outreg3_CE : std_logic ; 
	signal cmux_op135_S : std_logic ; 
	signal outreg4_CE : std_logic ; 
	signal cmux_op136_S : std_logic ; 
	signal outreg5_CE : std_logic ; 
	signal cmux_op137_S : std_logic ; 
	signal outreg6_CE : std_logic ; 
	signal cmux_op138_S : std_logic ; 
	signal outreg7_CE : std_logic ; 
	signal cmux_op139_S : std_logic ; 
	signal outreg8_CE : std_logic ; 
	signal cmux_op140_S : std_logic ; 
	signal outreg9_CE : std_logic ; 
	signal cmux_op141_S : std_logic ; 
	signal outreg10_CE : std_logic ; 
	signal cmux_op142_S : std_logic ; 
	signal outreg11_CE : std_logic ; 
	signal cmux_op143_S : std_logic ; 
	signal outreg12_CE : std_logic ; 
	signal cmux_op144_S : std_logic ; 
	signal outreg13_CE : std_logic ; 
	signal cmux_op145_S : std_logic ; 
	signal outreg14_CE : std_logic ; 
	signal cmux_op146_S : std_logic ; 
	signal outreg15_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 
	signal Shift_Shift : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
	
	
	function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
	variable tmp : std_logic_vector(ub downto lb);
	begin
		tmp:=  a(ub downto lb);
		return tmp;
	end std_range; 
	
begin
	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

	

max_100_O <= inreg0_Q when inreg0_Q>inreg1_Q else inreg1_Q;
max_101_O <= inreg2_Q when inreg2_Q>inreg3_Q else inreg3_Q;
max_102_O <= inreg4_Q when inreg4_Q>inreg5_Q else inreg5_Q;
max_103_O <= inreg6_Q when inreg6_Q>inreg7_Q else inreg7_Q;
max_104_O <= inreg8_Q when inreg8_Q>inreg9_Q else inreg9_Q;
max_105_O <= inreg10_Q when inreg10_Q>inreg11_Q else inreg11_Q;
max_106_O <= inreg12_Q when inreg12_Q>inreg13_Q else inreg13_Q;
max_107_O <= inreg14_Q when inreg14_Q>inreg15_Q else inreg15_Q;
max_108_O <= max_100_O when max_100_O>max_101_O else max_101_O;
max_109_O <= inreg1_Q when inreg1_Q>max_101_O else max_101_O;
max_110_O <= max_102_O when max_102_O>max_103_O else max_103_O;
max_111_O <= inreg5_Q when inreg5_Q>max_103_O else max_103_O;
max_112_O <= max_104_O when max_104_O>max_105_O else max_105_O;
max_113_O <= inreg9_Q when inreg9_Q>max_105_O else max_105_O;
max_114_O <= max_106_O when max_106_O>max_107_O else max_107_O;
max_115_O <= inreg13_Q when inreg13_Q>max_107_O else max_107_O;
max_116_O <= max_108_O when max_108_O>max_110_O else max_110_O;
max_117_O <= max_109_O when max_109_O>max_110_O else max_110_O;
max_118_O <= max_101_O when max_101_O>max_110_O else max_110_O;
max_119_O <= inreg3_Q when inreg3_Q>max_110_O else max_110_O;
max_120_O <= max_112_O when max_112_O>max_114_O else max_114_O;
max_121_O <= max_113_O when max_113_O>max_114_O else max_114_O;
max_122_O <= max_105_O when max_105_O>max_114_O else max_114_O;
max_123_O <= inreg11_Q when inreg11_Q>max_114_O else max_114_O;
max_124_O <= max_116_O when max_116_O>max_120_O else max_120_O;
max_125_O <= max_117_O when max_117_O>max_120_O else max_120_O;
max_126_O <= max_118_O when max_118_O>max_120_O else max_120_O;
max_127_O <= max_119_O when max_119_O>max_120_O else max_120_O;
max_128_O <= max_110_O when max_110_O>max_120_O else max_120_O;
max_129_O <= max_111_O when max_111_O>max_120_O else max_120_O;
max_130_O <= max_103_O when max_103_O>max_120_O else max_120_O;
max_131_O <= inreg7_Q when inreg7_Q>max_120_O else max_120_O;
	

	cmux_op132_O <= max_125_O when Shift_Shift= '0'	 else outreg0_Q;
	

	cmux_op133_O <= max_126_O when Shift_Shift= '0'	 else outreg1_Q;
	

	cmux_op134_O <= max_127_O when Shift_Shift= '0'	 else outreg2_Q;
	

	cmux_op135_O <= max_128_O when Shift_Shift= '0'	 else outreg3_Q;
	

	cmux_op136_O <= max_129_O when Shift_Shift= '0'	 else outreg4_Q;
	

	cmux_op137_O <= max_130_O when Shift_Shift= '0'	 else outreg5_Q;
	

	cmux_op138_O <= max_131_O when Shift_Shift= '0'	 else outreg6_Q;
	

	cmux_op139_O <= max_120_O when Shift_Shift= '0'	 else outreg7_Q;
	

	cmux_op140_O <= max_121_O when Shift_Shift= '0'	 else outreg8_Q;
	

	cmux_op141_O <= max_122_O when Shift_Shift= '0'	 else outreg9_Q;
	

	cmux_op142_O <= max_123_O when Shift_Shift= '0'	 else outreg10_Q;
	

	cmux_op143_O <= max_114_O when Shift_Shift= '0'	 else outreg11_Q;
	

	cmux_op144_O <= max_115_O when Shift_Shift= '0'	 else outreg12_Q;
	

	cmux_op145_O <= max_107_O when Shift_Shift= '0'	 else outreg13_Q;
	

	cmux_op146_O <= inreg15_Q when Shift_Shift= '0'	 else outreg14_Q;
	


	-- Pads
	CE_CE<=CE;	Shift_Shift<=Shift;	in_data_in_data<=in_data;	out_data<=out_data_out_data;
	-- DWires
	inreg0_D <= in_data_in_data;
	inreg1_D <= inreg0_Q;
	inreg2_D <= inreg1_Q;
	inreg3_D <= inreg2_Q;
	inreg4_D <= inreg3_Q;
	inreg5_D <= inreg4_Q;
	inreg6_D <= inreg5_Q;
	inreg7_D <= inreg6_Q;
	inreg8_D <= inreg7_Q;
	inreg9_D <= inreg8_Q;
	inreg10_D <= inreg9_Q;
	inreg11_D <= inreg10_Q;
	inreg12_D <= inreg11_Q;
	inreg13_D <= inreg12_Q;
	inreg14_D <= inreg13_Q;
	inreg15_D <= inreg14_Q;
	max_100_I0 <= inreg0_Q;
	max_100_I1 <= inreg1_Q;
	max_101_I0 <= inreg2_Q;
	max_101_I1 <= inreg3_Q;
	max_102_I0 <= inreg4_Q;
	max_102_I1 <= inreg5_Q;
	max_103_I0 <= inreg6_Q;
	max_103_I1 <= inreg7_Q;
	max_104_I0 <= inreg8_Q;
	max_104_I1 <= inreg9_Q;
	max_105_I0 <= inreg10_Q;
	max_105_I1 <= inreg11_Q;
	max_106_I0 <= inreg12_Q;
	max_106_I1 <= inreg13_Q;
	max_107_I0 <= inreg14_Q;
	max_107_I1 <= inreg15_Q;
	max_108_I0 <= max_100_O;
	max_108_I1 <= max_101_O;
	max_109_I0 <= inreg1_Q;
	max_109_I1 <= max_101_O;
	max_110_I0 <= max_102_O;
	max_110_I1 <= max_103_O;
	max_111_I0 <= inreg5_Q;
	max_111_I1 <= max_103_O;
	max_112_I0 <= max_104_O;
	max_112_I1 <= max_105_O;
	max_113_I0 <= inreg9_Q;
	max_113_I1 <= max_105_O;
	max_114_I0 <= max_106_O;
	max_114_I1 <= max_107_O;
	max_115_I0 <= inreg13_Q;
	max_115_I1 <= max_107_O;
	max_116_I0 <= max_108_O;
	max_116_I1 <= max_110_O;
	max_117_I0 <= max_109_O;
	max_117_I1 <= max_110_O;
	max_118_I0 <= max_101_O;
	max_118_I1 <= max_110_O;
	max_119_I0 <= inreg3_Q;
	max_119_I1 <= max_110_O;
	max_120_I0 <= max_112_O;
	max_120_I1 <= max_114_O;
	max_121_I0 <= max_113_O;
	max_121_I1 <= max_114_O;
	max_122_I0 <= max_105_O;
	max_122_I1 <= max_114_O;
	max_123_I0 <= inreg11_Q;
	max_123_I1 <= max_114_O;
	max_124_I0 <= max_116_O;
	max_124_I1 <= max_120_O;
	max_125_I0 <= max_117_O;
	max_125_I1 <= max_120_O;
	max_126_I0 <= max_118_O;
	max_126_I1 <= max_120_O;
	max_127_I0 <= max_119_O;
	max_127_I1 <= max_120_O;
	max_128_I0 <= max_110_O;
	max_128_I1 <= max_120_O;
	max_129_I0 <= max_111_O;
	max_129_I1 <= max_120_O;
	max_130_I0 <= max_103_O;
	max_130_I1 <= max_120_O;
	max_131_I0 <= inreg7_Q;
	max_131_I1 <= max_120_O;
	outreg0_D <= max_124_O;
	cmux_op132_I0 <= max_125_O;
	cmux_op132_I1 <= outreg0_Q;
	outreg1_D <= cmux_op132_O;
	cmux_op133_I0 <= max_126_O;
	cmux_op133_I1 <= outreg1_Q;
	outreg2_D <= cmux_op133_O;
	cmux_op134_I0 <= max_127_O;
	cmux_op134_I1 <= outreg2_Q;
	outreg3_D <= cmux_op134_O;
	cmux_op135_I0 <= max_128_O;
	cmux_op135_I1 <= outreg3_Q;
	outreg4_D <= cmux_op135_O;
	cmux_op136_I0 <= max_129_O;
	cmux_op136_I1 <= outreg4_Q;
	outreg5_D <= cmux_op136_O;
	cmux_op137_I0 <= max_130_O;
	cmux_op137_I1 <= outreg5_Q;
	outreg6_D <= cmux_op137_O;
	cmux_op138_I0 <= max_131_O;
	cmux_op138_I1 <= outreg6_Q;
	outreg7_D <= cmux_op138_O;
	cmux_op139_I0 <= max_120_O;
	cmux_op139_I1 <= outreg7_Q;
	outreg8_D <= cmux_op139_O;
	cmux_op140_I0 <= max_121_O;
	cmux_op140_I1 <= outreg8_Q;
	outreg9_D <= cmux_op140_O;
	cmux_op141_I0 <= max_122_O;
	cmux_op141_I1 <= outreg9_Q;
	outreg10_D <= cmux_op141_O;
	cmux_op142_I0 <= max_123_O;
	cmux_op142_I1 <= outreg10_Q;
	outreg11_D <= cmux_op142_O;
	cmux_op143_I0 <= max_114_O;
	cmux_op143_I1 <= outreg11_Q;
	outreg12_D <= cmux_op143_O;
	cmux_op144_I0 <= max_115_O;
	cmux_op144_I1 <= outreg12_Q;
	outreg13_D <= cmux_op144_O;
	cmux_op145_I0 <= max_107_O;
	cmux_op145_I1 <= outreg13_Q;
	outreg14_D <= cmux_op145_O;
	cmux_op146_I0 <= inreg15_Q;
	cmux_op146_I1 <= outreg14_Q;
	outreg15_D <= cmux_op146_O;
	out_data_out_data <= outreg15_Q;

	-- CWires
	inreg0_CE <= Shift_Shift;
	inreg1_CE <= Shift_Shift;
	inreg2_CE <= Shift_Shift;
	inreg3_CE <= Shift_Shift;
	inreg4_CE <= Shift_Shift;
	inreg5_CE <= Shift_Shift;
	inreg6_CE <= Shift_Shift;
	inreg7_CE <= Shift_Shift;
	inreg8_CE <= Shift_Shift;
	inreg9_CE <= Shift_Shift;
	inreg10_CE <= Shift_Shift;
	inreg11_CE <= Shift_Shift;
	inreg12_CE <= Shift_Shift;
	inreg13_CE <= Shift_Shift;
	inreg14_CE <= Shift_Shift;
	inreg15_CE <= Shift_Shift;
	outreg0_CE <= CE_CE;
	cmux_op132_S <= Shift_Shift;
	outreg1_CE <= CE_CE;
	cmux_op133_S <= Shift_Shift;
	outreg2_CE <= CE_CE;
	cmux_op134_S <= Shift_Shift;
	outreg3_CE <= CE_CE;
	cmux_op135_S <= Shift_Shift;
	outreg4_CE <= CE_CE;
	cmux_op136_S <= Shift_Shift;
	outreg5_CE <= CE_CE;
	cmux_op137_S <= Shift_Shift;
	outreg6_CE <= CE_CE;
	cmux_op138_S <= Shift_Shift;
	outreg7_CE <= CE_CE;
	cmux_op139_S <= Shift_Shift;
	outreg8_CE <= CE_CE;
	cmux_op140_S <= Shift_Shift;
	outreg9_CE <= CE_CE;
	cmux_op141_S <= Shift_Shift;
	outreg10_CE <= CE_CE;
	cmux_op142_S <= Shift_Shift;
	outreg11_CE <= CE_CE;
	cmux_op143_S <= Shift_Shift;
	outreg12_CE <= CE_CE;
	cmux_op144_S <= Shift_Shift;
	outreg13_CE <= CE_CE;
	cmux_op145_S <= Shift_Shift;
	outreg14_CE <= CE_CE;
	cmux_op146_S <= Shift_Shift;
	outreg15_CE <= CE_CE;

	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	inreg0_Q <= (others => '0');

	inreg1_Q <= (others => '0');

	inreg2_Q <= (others => '0');

	inreg3_Q <= (others => '0');

	inreg4_Q <= (others => '0');

	inreg5_Q <= (others => '0');

	inreg6_Q <= (others => '0');

	inreg7_Q <= (others => '0');

	inreg8_Q <= (others => '0');

	inreg9_Q <= (others => '0');

	inreg10_Q <= (others => '0');

	inreg11_Q <= (others => '0');

	inreg12_Q <= (others => '0');

	inreg13_Q <= (others => '0');

	inreg14_Q <= (others => '0');

	inreg15_Q <= (others => '0');

	outreg0_Q <= (others => '0');

	outreg1_Q <= (others => '0');

	outreg2_Q <= (others => '0');

	outreg3_Q <= (others => '0');

	outreg4_Q <= (others => '0');

	outreg5_Q <= (others => '0');

	outreg6_Q <= (others => '0');

	outreg7_Q <= (others => '0');

	outreg8_Q <= (others => '0');

	outreg9_Q <= (others => '0');

	outreg10_Q <= (others => '0');

	outreg11_Q <= (others => '0');

	outreg12_Q <= (others => '0');

	outreg13_Q <= (others => '0');

	outreg14_Q <= (others => '0');

	outreg15_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (Shift_Shift='1') then
			
			--could not optimize
			inreg0_Q <= in_data_in_data;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg1_Q <= inreg0_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg2_Q <= inreg1_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg3_Q <= inreg2_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg4_Q <= inreg3_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg5_Q <= inreg4_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg6_Q <= inreg5_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg7_Q <= inreg6_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg8_Q <= inreg7_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg9_Q <= inreg8_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg10_Q <= inreg9_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg11_Q <= inreg10_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg12_Q <= inreg11_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg13_Q <= inreg12_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg14_Q <= inreg13_Q;
			
	end if;

	if (Shift_Shift='1') then
			
			--could not optimize
			inreg15_Q <= inreg14_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg0_Q <= max_124_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg1_Q <= cmux_op132_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg2_Q <= cmux_op133_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg3_Q <= cmux_op134_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg4_Q <= cmux_op135_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg5_Q <= cmux_op136_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg6_Q <= cmux_op137_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg7_Q <= cmux_op138_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg8_Q <= cmux_op139_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg9_Q <= cmux_op140_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg10_Q <= cmux_op141_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg11_Q <= cmux_op142_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg12_Q <= cmux_op143_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg13_Q <= cmux_op144_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg14_Q <= cmux_op145_O;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			outreg15_Q <= cmux_op146_O;
			
	end if;

		end if;
	end process;
	
	
	

	
	

	
	

	
	 
	
	--could not optimize
	out_data<=out_data_out_data;	

	
end RTL;
