library ieee;
library work;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pFIR_4 is port 
 (

	-- Global signals
	rst : in std_logic;
	clk : in std_logic;
	-- Input Control Pads
	CE : in std_logic;
	-- Input Data Pads
	X : in std_logic_vector(15 downto 0);
	-- Output Data Pads
	Y : out std_logic_vector(15 downto 0)
	)
;
end pFIR_4;

architecture RTL of pFIR_4 is

	-- Components 


	-- Wires 
	------------------------------------------------------
	-- 		Dataflow wires (named after their source port)
	------------------------------------------------------
	signal X_X : std_logic_vector(15 downto 0) ; 

	signal Y_Y : std_logic_vector(15 downto 0) ; 

		
		
		
	signal reg_1_D : std_logic_vector(15 downto 0) ; 
	signal reg_1_Q : std_logic_vector(15 downto 0) ; 

		
		
		
	signal reg_2_D : std_logic_vector(15 downto 0) ; 
	signal reg_2_Q : std_logic_vector(15 downto 0) ; 

		
		
		
	signal reg_3_D : std_logic_vector(15 downto 0) ; 
	signal reg_3_Q : std_logic_vector(15 downto 0) ; 

		
		
		
	
	signal reg_1_CE : std_logic ; 
	signal reg_2_CE : std_logic ; 
	signal reg_3_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Controlflow wires (named after their source port)
	---------------------------------------------------------
	signal CE_CE : std_logic ; 

	---------------------------------------------------------
	-- 		Auxilliary wires (named after their source port)
	---------------------------------------------------------
		
		
		
	
		signal Y_Y_int : std_logic_vector(31 downto 0) ;
		
	

begin
	

	


	--optimized for C_V1_C_0
	

	--optimized for mul_0
	

	--optimized for C_V7_C_1
	
	


	--optimized for mul_1
	

	--optimized for add_2
	

	--optimized for C_V12_C_2
	
	


	--optimized for mul_3
	

	--optimized for add_4
	

	--optimized for C_V56_C_3
	
	


	--optimized for mul_5
	

	--optimized for add_6
	

	--optimized for sel_op7
	


	-- Pads
	X_X<=X;	CE_CE<=CE;	Y<=Y_Y;
	-- DWires
	reg_1_D <= X_X;
	reg_2_D <= reg_1_Q;
	reg_3_D <= reg_2_Q;

	-- CWires
	reg_1_CE <= CE_CE;
	reg_2_CE <= CE_CE;
	reg_3_CE <= CE_CE;


	
	
	sync_register_assignment : process(rst,clk)
	
		

	
		

	
		

	
	begin
		if (rst='1') then
			
	reg_1_Q <= (others => '0');

	reg_2_Q <= (others => '0');

	reg_3_Q <= (others => '0');

		elsif rising_edge(clk) then
			
	if (CE_CE='1') then
			
			--could not optimize
			reg_1_Q <= X_X;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			reg_2_Q <= reg_1_Q;
			
	end if;

	if (CE_CE='1') then
			
			--could not optimize
			reg_3_Q <= reg_2_Q;
			
	end if;

		end if;
	end process;
	
	
	
	

	
	

	
	 
		
			Y_Y_int <=  ( ( reg_3_Q * ( "1110000000000000" ) ) + ( ( reg_2_Q * ( "1100000000000000" ) ) + ( ( reg_1_Q * ( "1111111111111111" ) ) + ( X_X * ( "1111111111111111" ) ) ) ) ) ;
			Y_Y <= Y_Y_int (15 downto 0);
		
	

	

end RTL;
