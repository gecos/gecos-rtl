
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY XRam IS
	PORT
	(
		clock			: IN  std_logic;
		din				: IN  std_logic_vector(7 DOWNTO 0);
		re				: IN  std_logic;
		write_address	: IN  std_logic_vector(7 DOWNTO 0);
		read_address	: IN  std_logic_vector(7 DOWNTO 0);
		we				: IN  std_logic;
		dout			: OUT std_logic_vector(7 DOWNTO 0)
	);
END XRam;

ARCHITECTURE rtl OF XRam IS
	TYPE RAM IS ARRAY(0 TO 2 ** 8 - 1) OF std_logic_vector(7 DOWNTO 0);

	SIGNAL ram_block : RAM;
BEGIN
	PROCESS (clock)
	BEGIN
		IF (clock'event AND clock = '1') THEN
			IF (we = '1') THEN
			    ram_block(to_integer(unsigned(write_address))) <= din;
			END IF;
			IF (re = '1') THEN
				dout <= ram_block(to_integer(unsigned(read_address)));
			END IF;
		END IF;
	END PROCESS;
END rtl;
