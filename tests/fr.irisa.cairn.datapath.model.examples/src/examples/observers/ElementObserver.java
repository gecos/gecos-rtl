package examples.observers;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathFactory;
 
public class ElementObserver {

	private Datapath datapath;
 
	public ElementObserver() {
		DatapathFactory factory = DatapathFactory.eINSTANCE;
		datapath = factory.createDatapath();
 
		Adapter adapter = new AdapterImpl() {
			public void notifyChanged(Notification notification) {
				System.out
						.println("Notification received from the data model. Data model has changed!!!");
			}
		};
		datapath .eAdapters().add(adapter);
	}
 
	public void doStuff() {
		DatapathFactory factory = DatapathFactory.eINSTANCE;
		Datapath subdatapath = factory.createDatapath();
		subdatapath.setName("Lars");
		System.out.println("I'm adding a person.");
		datapath.getComponents().add(subdatapath);
		System.out.println("I'm changing a entry");
		datapath.getComponents().get(0).setName("Lars2");
	}
}
