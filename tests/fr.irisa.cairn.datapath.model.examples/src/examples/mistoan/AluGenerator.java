package examples.mistoan;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;

public class AluGenerator {
	public static void main(String[] args) {
		//declaration of the datapath that will represent the ALU circuit
		Datapath myAlu;
		//declaration of the component used for generating the model corresponding to the ALU
		DatapathXMLWriter xmlWriter;
		//declaration of the component responsible for generating the VHDL code
		DatapathVHDLGenerator vhdlGenerator;
		//set the width of the component to be created
		int bitwidth = 16;

		/**
		 * Generate a parallel (unrolled) pipelined FIR implementation
		 */
		try {
			//create a new ALU
			myAlu = Alu.generate(bitwidth);
			//create the xml writer and create the model from the circuit
			xmlWriter = DatapathXMLWriter.getXMLWriter();
			xmlWriter.save(myAlu, "models/m" + myAlu.getName() + ".datapath");
			//create the VHDL writer and the file containing the component
			vhdlGenerator = new DatapathVHDLGenerator(myAlu,"models", myAlu.getName());
			vhdlGenerator.generate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
