package examples.mistoan;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandVHDLGenerator;

public class AluGeneratorXPand {
	public static void main(String[] args) {
		//declaration of the datapath that will represent the ALU circuit
		Datapath myAlu;
		//declaration of the component used for generating the model corresponding to the ALU
		DatapathXMLWriter xmlWriter;
		//declaration of the component responsible for generating the VHDL code
		XPandVHDLGenerator vhdlGenerator;
		//set the width of the component to be created
		int bitwidth = 16;
		//path to the model
		String path = "./models/";

		/**
		 * Generate an Arithmetic Logic Unit
		 */
		try {
			//create a new ALU
			System.out.println("Started processing");
			System.out.println("Step1: generating the datapath from the circuit description...");
			myAlu = Alu.generate(bitwidth);
			//create the xml writer and create the model from the circuit
			xmlWriter = DatapathXMLWriter.getXMLWriter();
			String modelfilename = "models/m" + myAlu.getName() + ".datapath";
			System.out.println("step2: saving the datapath as "+modelfilename);
			xmlWriter.save(myAlu, modelfilename);
			//create the VHDL writer and the file containing the component
			System.out.println("Step3: generating the VHDL files...");
			vhdlGenerator = new XPandVHDLGenerator(myAlu, path);
			vhdlGenerator.compute();
			System.out.println("Processing finished");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
