package examples.mistoan;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandSCGenerator;

public class AluGeneratorXPandSC {
	public static void main(String[] args) {
		//declaration of the datapath that will represent the ALU circuit
		Datapath myAlu;
		//declaration of the component used for generating the model corresponding to the ALU
		DatapathXMLWriter xmlWriter;
		//declaration of the component responsible for generating the VHDL code
		XPandSCGenerator scGenerator;
		//set the width of the component to be created
		int bitwidth = 16;
		//path to the model
		String path = "E:/Work/an3plus/Work/fr.irisa.cairn.model.datapath.examples/models/systemc/";

		/**
		 * Generate an Arithmetic Logic Unit
		 */
		try {
			//create a new ALU
			System.out.println("Started processing");
			System.out.println("Step1: generating the datapath from the circuit description...");
			myAlu = Alu.generate(bitwidth);
			//create the xml writer and create the model from the circuit
			System.out.println("step2: saving the datapath...");
			xmlWriter = DatapathXMLWriter.getXMLWriter();
			xmlWriter.save(myAlu, "models/systemc/" + myAlu.getName() + ".datapath");
			//create the VHDL writer and the file containing the component
			System.out.println("Step3: generating the SystemC files...");
			scGenerator = new XPandSCGenerator(myAlu, path);
			scGenerator.compute();
			System.out.println("Processing finished");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
