package examples.mistoan;

import static fr.irisa.cairn.model.datapath.user.FSMDFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.operators.*;
import fr.irisa.cairn.model.datapath.pads.*;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;
import fr.irisa.cairn.model.datapath.user.FSMDPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.*;

public class Gate {

	public static Datapath generate(int width) {
		//datapath declaration
		Datapath datapath = null;
		
		//ports' declaration
		DataInputPad a;		//input port A
		DataInputPad b;		//input port B
		DataOutputPad r;	//output port R
		
		//operators' declaration
		BinaryOperator operation_and;
		
		//create the input ports
		a = FSMDPadsFactory.IDPad("A", width);
		b = FSMDPadsFactory.IDPad("B", width);
		r = FSMDPadsFactory.ODPad("R", width);		//control port has the width corresponding to the number of operations
													// that can be performed by the arithmetic unit
		
		//create the datapath
		//this is a container that will hold all the elements of the circuit
		datapath = FSMDFactory.datapath("Gate" + width, a, b, r);
		//set the current datapath
		FSMDFactory.pushDatapath(datapath);
		
		//create the different operators and map the input ports accordingly
		operation_and = FSMDOperatorFactory.and(a, b);
		operation_and.setName("AND");
		
		r.connect(operation_and);
		
		//return the completed datapath
		return datapath;
	}
}

