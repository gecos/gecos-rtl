package examples.mistoan;

import static fr.irisa.cairn.model.datapath.user.FSMDFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.operators.*;
import fr.irisa.cairn.model.datapath.pads.*;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;
import fr.irisa.cairn.model.datapath.user.FSMDPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.*;

public class Alu {

	public static Datapath generate(int width) {
		//datapath declaration
		Datapath datapath = null;
		
		//ports' declaration
		DataInputPad a;		//input port A
		DataInputPad b;		//input port B
		DataOutputPad r;	//output port R
		ControlPad op;		//control port OP
		
		//operators' declaration
		BinaryOperator operation_add;
		BinaryOperator operation_sub;
		BinaryOperator operation_and;
		BinaryOperator operation_or;
//		UnaryOperator operation_not;
		BinaryOperator operation_xor;
//		BinaryOperator operation_shr;
//		BinaryOperator operation_shl;
		
		//multiplexers' declaration
//		ControlFlowMux mux;
		ControlFlowMux mux00;
		ControlFlowMux mux01;
		ControlFlowMux mux10;
		ControlFlowMux mux20;
		
		//create the input ports
		a = FSMDPadsFactory.IDPad("A", width);
		b = FSMDPadsFactory.IDPad("B", width);
		r = FSMDPadsFactory.ODPad("R", width);
		op = FSMDPadsFactory.ICPad("OP", 3);		//control port has the width corresponding to the number of operations
													// that can be performed by the arithmetic unit
		
		//create the datapath
		//this is a container that will hold all the elements of the circuit
		datapath = FSMDFactory.datapath("ALU" + width, a, b, op, r);
		//set the current datapath
		FSMDFactory.pushDatapath(datapath);
		
		//create the different operators and map the input ports accordingly
		operation_add = FSMDOperatorFactory.add(a, b);
		operation_add.setName("ADD");
		operation_sub = FSMDOperatorFactory.sub(a, b);
		operation_sub.setName("SUB");
		operation_and = FSMDOperatorFactory.and(a, b);
		operation_and.setName("AND");
		operation_or = FSMDOperatorFactory.or(a, b);
		operation_or.setName("OR");
//		operation_not = FSMDOperatorFactory.not(a);
//		operation_not.setName("NOT");
		operation_xor = FSMDOperatorFactory.xor(a, b);
		operation_xor.setName("XOR");
//		operation_shr = FSMDOperatorFactory.shr(a, b);
//		operation_shr.setName("SHR");
//		operation_shl = FSMDOperatorFactory.shl(a, b);
//		operation_shl.setName("SHL");
		
		//mux = FSMDOperatorFactory.cmuxtree(op.getFlags().get(0), operation_add, operation_sub, operation_and, operation_or, operation_xor);
//		mux = FSMDOperatorFactory.cmuxtree(op.getFlags().get(0), operation_add.getOutput(), operation_sub.getOutput(), operation_and.getOutput(), operation_or.getOutput(), operation_xor.getOutput());
//		mux.setName("OUT_MUX");
		
//		r.connect(mux);
		
		//create the tree of multiplexers that will help select the operation
		//to be performed by the arithmetic unit, in relation to the OP control signal
		mux00 = FSMDOperatorFactory.cmux("mux00", operation_add, operation_sub, FSMDOperatorFactory.select(op.getFlag(), 0));
		mux01 = FSMDOperatorFactory.cmux("mux01", operation_and, operation_or, FSMDOperatorFactory.select(op.getFlag(), 0));
		mux10 = FSMDOperatorFactory.cmux("mux10", mux00, mux01, FSMDOperatorFactory.select(op.getFlag(), 1));
		mux20 = FSMDOperatorFactory.cmux("mux20", mux10, operation_xor, FSMDOperatorFactory.select(op.getFlag(), 2));
		
		//connect the output of the last multiplexer in the tree to the output port R of the unit
		r.connect(mux20);
		
		//return the completed datapath
		return datapath;
	}
}
