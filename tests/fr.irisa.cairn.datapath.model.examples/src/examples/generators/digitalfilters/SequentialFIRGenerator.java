package examples.generators.digitalfilters;

import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.boolCommand;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.branch;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.fsm;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.icps;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.nT;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.T;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.ocps;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.sequence;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.state;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.states;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.ICP;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.OCP;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.cereg;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDMemoryFactory.*;

import org.omg.Messaging.SyncScopeHelper;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.pads.*;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.State;

/**
 * Implementation as a model generator for sequential FIR filter implementation, 
 * in the form of a DSP processor core (1 MAC + 2 Memory) controlled by an FSM. 
 * For a N-TAP filter, the architecture processes a new Sample every N+3 cycle. 
 */

public class SequentialFIRGenerator  {
	
	public static Datapath generate(int coef[], int width) {
		assert(coef!=null);

		String name="FIR"  +coef.length + "_" + width;

		DataInputPad X = IDPad( "X", width);
		//ControlPad CE  = ICPad( "CE", 1);
		DataOutputPad Y = ODPad( "Y", width);
		
		//pushDatapath( datapath( name, X, CE, Y)); 
		pushDatapath( datapath( name, X, Y));

		InControlPort endLoop, inEmpty, outFull; 
        
		OutControlPort fifoRead, fifoWrite, accEnable, mulEnable, xwrEnable, xrdEnable, crdEnable; 
		OutControlPort cptEnable, adxEnable, cptReset; 

		// Create Input Ports
		endLoop    = ICP( "endLoop", 1);
		inEmpty    = ICP( "inEmpty", 1);
		outFull    = ICP( "outFull", 1);
		
		// Create Output Ports
		fifoRead  = OCP( "fifo_rd", 1); 
		fifoWrite = OCP( "fifo_wr", 1);
		accEnable = OCP( "acc_en", 1);   
		mulEnable = OCP( "mul_en", 1);    
		xwrEnable = OCP( "xwr_en", 1);    
		xrdEnable = OCP( "xrd_en", 1);    
		crdEnable = OCP( "crd_en", 1);    
		cptEnable = OCP( "cpt_en" ,1);    
		adxEnable = OCP( "adx_en", 1);    
		cptReset  = OCP( "cpt_init", 1);  
		

		State idle = state("idle");
		
		State waitState0  = state("waitNotEmpty");
		
		sequence( idle, waitState0);
		
		boolCommand( xrdEnable, true);

		State readFIFO  = state("readFIFO",
				boolCommand( xwrEnable, true),
				boolCommand( fifoRead, true)
		);
		
		branch( waitState0, readFIFO, waitState0, nT(inEmpty));
		
		State step1 = state("step1",
			boolCommand( crdEnable, true),
			boolCommand( cptEnable, true)
		);

		State step2 = state("step2",
			boolCommand(mulEnable,true),
			boolCommand(xrdEnable,true),
			boolCommand(crdEnable,true),
			boolCommand(cptEnable,true)
		);
		
		State steady = state("steady",
			boolCommand(accEnable,true),
			boolCommand(mulEnable,true),
			boolCommand(xrdEnable,true),
			boolCommand(crdEnable,true),
			boolCommand(cptEnable,true)
		);
		
		sequence(readFIFO, step1, step2, steady);
		
		State flush1 = state("flush1",
			boolCommand(accEnable,true),
			boolCommand(mulEnable,true)
		);
		
		branch(steady, flush1, steady, T(endLoop));
		
		State flush2 = state("flush2",
			boolCommand(accEnable,true),
			boolCommand(mulEnable,true)
		);
		
		State waitState1  = state("waitsState1");
		
		sequence(flush1, flush2, waitState1);
		
		State writeFIFO  = state("writeFIFO", 
				boolCommand(fifoWrite,true)
		);
		
		branch(waitState1, writeFIFO, waitState1, nT(outFull));
		
		sequence(writeFIFO,idle);

		FSM fsm = fsm("fir_ctrl", 
				icps(
						endLoop,inEmpty,outFull
				),
				ocps(	
						fifoRead,  
						xwrEnable, xrdEnable,
						adxEnable,
						crdEnable, 
						cptEnable, cptReset,
						mulEnable,
						accEnable,
						fifoWrite
				), 
				states(
						idle,
						waitState0,
						readFIFO,
						step1,
						step2,
						steady,
						flush1,
						flush2,
						waitState1,
						writeFIFO
				)
		);

		CERegister cpt = cereg( "cpt", 8, cptEnable);
		cpt.connect(
				cmux(
						add(
								cpt,
								constant(1, 8)
						),
						constant(0, 8),
						cptReset
				)
		);
		
		CERegister adx = cereg( "adx", 8, adxEnable);
		adx.connect(
				cmux(
						add(
								cpt,
								constant(1, 8)
						),
						sub(
								cpt,
								constant(1, 8)
						),
						adxEnable
				)
		);

		// 16x16->31 Multiplier
		BinaryOperator mul = mul(width);
		
		// Multiplier pipeline register
		CERegister mul_reg = cereg( "mul_reg" , mul, mulEnable);
		
		// Accumulator
		CERegister acc_reg = cereg( "acc_reg", 2*width, accEnable);
		acc_reg.connect(
				add( "acc_adder", 
						mul_reg, 
						acc_reg
				)
		);
		
		Y.getInput().connect(FSMDOperatorFactory.select(acc_reg,0,7));
		

		/* Rom containing the filter coefficients */ 
		SinglePortRom crom = syncRom( "CRom", width, 256, cpt.getOutput(), crdEnable, mul.getInput(0));
		
		/* Ram containing the N=256 last samples */ 
		SinglePortRam xram = syncReadSPRam( "XRam", width, 256, adx.getOutput(), X.getOutput(), xrdEnable, xwrEnable, mul.getInput(1));


		Datapath datapath = FSMDFactory.popDatapath();
		if( datapath==null ) 
			throw new RuntimeException("Empty datapath");
		
		return datapath;
	}

}
