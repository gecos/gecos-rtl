package examples.generators.digitalfilters;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandVHDLGenerator;

public class FIRGeneratorXPandVHDL {
	public static void main(String[] args) {
		
		/**
		 * Generate a parallel (unrolled) pipelined FIR implementation
		 */
		try {
			String path = "./models/";
			Datapath pfir = ParallelFIR.generate(new int[]{1,5,12,56},16);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(pfir, "models/parFIR16.datapath");
			fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandVHDLGenerator project = new XPandVHDLGenerator(pfir,path);
			project.compute();
//			DatapathRemoteQuartusSynthesis synthesis = new DatapathRemoteQuartusSynthesis(pfir, "./quartus/");
//			synthesis.compute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Generate a Sequential pipelined FIR implementation (à la TMSC320C25)
		 */
		try {
			String path = "./models/";
			Datapath seqfir = SequentialFIRGenerator.generate(new int[]{1,7},8);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(seqfir, "models/seqFIR16.datapath");
			XPandVHDLGenerator project = new XPandVHDLGenerator(seqfir,path);
			project.compute();
//			DatapathRemoteQuartusSynthesis synthesis = new DatapathRemoteQuartusSynthesis(seqfir, "./quartus/");
//			synthesis.compute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
