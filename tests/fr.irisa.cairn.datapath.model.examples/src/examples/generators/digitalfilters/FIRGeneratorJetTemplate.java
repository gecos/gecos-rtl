package examples.generators.digitalfilters;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;

public class FIRGeneratorJetTemplate {
	public static void main(String[] args) {
		
		/**
		 * Generate a parallel (unrolled) pipelined FIR implementation
		 */
		try {
			Datapath pfir = ParallelFIR.generate(new int[]{1,7,12,56},16);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(pfir, "models/parFIR16.datapath");
			DatapathVHDLGenerator project = new DatapathVHDLGenerator(pfir,"models",pfir.getName());
			project.generate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Generate a Sequential pipelined FIR implementation (à la TMSC320C25)
		 */
		try {
			Datapath seqfir = SequentialFIRGenerator.generate(new int[]{1,7},8);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(seqfir, "models/seqFIR16.datapath");
			DatapathVHDLGenerator project = new DatapathVHDLGenerator(seqfir,"models",seqfir.getName());
			project.generate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
