	package examples.generators.digitalfilters;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.impl.DatapathImpl;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserStorageFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserWireFactory;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.boolCommand;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.branch;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.fsm;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.icps;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.nT;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.ocps;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.sequence;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.state;
import static fr.irisa.cairn.model.datapath.user.FSMDFSMFactory.states;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.ICP;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.OCP;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.cereg;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDMemoryFactory.*;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.pads.*;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.State;

/**
 * Implementation a a model generator for parallel FIR filter implementation.
 * This example is a pure data-flow datapath without any control FSM, it 
 * processes a new sample every cycle. 
 */
public class ParallelFIR  {

	
	public static Datapath generate(int coef[], int width) {
		assert(coef!=null);
		int order = coef.length;

		DataInputPad X = IDPad( "X", width);
		ControlPad CE = ICPad( "CE", 1);
		DataOutputPad Y = ODPad( "Y", width);

		pushDatapath(datapath( "pFIR_"+coef.length, X, CE, Y));

		ConstantValue[] C = new ConstantValue[order];
		CERegister[] reg = new CERegister[order];
		BinaryOperator[] add = new BinaryOperator[order];

		for (int i=0; i<order; i++) {
			C[i] = constant("C_"+i, coef[i], width);
			
			if (i==0) {
				add[0] =mul( X, C[i]);
			} else {
				if (i>1) {
					reg[i] = cereg("reg_" + i, reg[i-1], CE);
				} else {
					reg[i] = cereg("reg_" + i, X, CE);
				}	
				add[i] = shl(
							mul(
									reg[i], 
									C[i]
							), 
							add[i-1]
						);
			}
		}
		
		Y.connect(FSMDOperatorFactory.select(add[order-1], 0, 15));
		
		return FSMDFactory.popDatapath();
	}

}
