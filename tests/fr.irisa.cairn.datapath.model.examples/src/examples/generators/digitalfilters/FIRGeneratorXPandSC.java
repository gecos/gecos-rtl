package examples.generators.digitalfilters;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandSCGenerator;

public class FIRGeneratorXPandSC {
	public static void main(String[] args) {
		
		/**
		 * Generate a parallel (unrolled) pipelined FIR implementation
		 */
		try {
			System.out.println("Started processing for parallel filter");
			System.out.println("Step1: generating the datapath from the circuit description...");
			String path = "./models/systemc/";
			Datapath pfir = ParallelFIR.generate(new int[]{1,7,12,56},16);
			System.out.println("step2: saving the datapath...");
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(pfir, "models/systemc/parFIR16.datapath");
			System.out.println("Step3: generating the SystemC files...");
			XPandSCGenerator project = new XPandSCGenerator(pfir,path);
	 		project.compute();
			System.out.println("Processing finished");
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Generate a Sequential pipelined FIR implementation (à la TMSC320C25)
		 */
		try {
			System.out.println("Started processing for sequential filter");
			System.out.println("Step1: generating the datapath from the circuit description...");
			String path = "./models/systemc/";
			Datapath seqfir = SequentialFIRGenerator.generate(new int[]{1,7},8);
			System.out.println("step2: saving the datapath...");
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(seqfir, "models/systemc/seqFIR16.datapath");
			System.out.println("Step3: generating the SystemC files...");
			XPandSCGenerator project = new XPandSCGenerator(seqfir,path);
			project.compute();
			System.out.println("Processing finished");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
