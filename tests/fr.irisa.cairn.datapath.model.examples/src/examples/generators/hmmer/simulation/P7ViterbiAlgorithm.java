package examples.generators.hmmer.simulation;

public class P7ViterbiAlgorithm {
	/*
	 * Function: P7Viterbi()
	 * 
	 * Purpose: The Viterbi dynamic programming algorithm. Identical to
	 * Forward() except that max's replace sum's.
	 * 
	 * Args: dsq - sequence in digitized form L - length of dsq hmm - the model
	 * ret_tr - RETURN: traceback; pass NULL if it's not wanted
	 * 
	 * Return: log P(S|M)/P(S|R), as a bit score
	 */
	public class Plan7Structure {
		int M;
		int msc[][];
		int isc[][];
		int xsc[][];
		int tsc[][];
		int bsc[];
		int esc[];
	}

	public class Plan7Trace {
	}

	private static final int XMN = 0;
	private static final int XMB = 1;
	private static final int XMJ = 2;
	private static final int XME = 3;
	private static final int XMC = 4;
	
	private static final int INFTY = 0;
	private static final int XTN = 0;
	private static final int MOVE = 0;
	private static final int TMM = 0;
	private static final int TIM = 0;
	private static final int TDM = 0;
	private static final int TMD = 0;
	private static final int TDD = 0;
	private static final int TMI = 0;
	private static final int TII = 0;
	private static final int LOOP = 0;
	private static final int XTJ = 0;
	private static final int XTE = 0;
	private static final int XTC = 0;

	int P7Viterbi(char dsq[], int L, Plan7Structure hmm, Plan7Trace ret_tr[]) {
		// struct dpmatrix_s *mx;
		// struct p7trace_s *tr;
		int xmx[][] = new int[hmm.M][L];
		int mmx[][] = new int[hmm.M][L];
		int imx[][] = new int[hmm.M][L];
		int dmx[][] = new int[hmm.M][L];
		int i, k;
		int sc;

		/*
		 * Allocate a DP matrix with 0..L rows, 0..M-1 columns.
		 */
		// mx = AllocPlan7Matrix(L+1, hmm.M, &xmx, &mmx, &imx, &dmx);

		/*
		 * Initialization of the zero row.
		 */
		xmx[0][XMN] = 0; /* S.N, p=1 */
		xmx[0][XMB] = hmm.xsc[XTN][MOVE]; /* S.N.B, no N-tail */
		xmx[0][XME] = xmx[0][XMC] = xmx[0][XMJ] = -INFTY; /* need seq to get here */
		for (k = 0; k <= hmm.M; k++)
			mmx[0][k] = imx[0][k] = dmx[0][k] = -INFTY; /* need seq to get here */

		/*
		 * Recursion. Done as a pull. Note some slightly wasteful boundary
		 * conditions: tsc[0] = -INFTY for all eight transitions (no node 0) D_M
		 * and I_M are wastefully calculated (they don't exist)
		 */
		for (i = 1; i <= L; i++) {
			mmx[i][0] = imx[i][0] = dmx[i][0] = -INFTY;

			for (k = 1; k <= hmm.M; k++) {
				/* match state */
				mmx[i][k] = -INFTY;
				if ((sc = mmx[i - 1][k - 1] + hmm.tsc[k - 1][TMM]) > mmx[i][k])
					mmx[i][k] = sc;
				if ((sc = imx[i - 1][k - 1] + hmm.tsc[k - 1][TIM]) > mmx[i][k])
					mmx[i][k] = sc;
				if ((sc = xmx[i - 1][XMB] + hmm.bsc[k]) > mmx[i][k])
					mmx[i][k] = sc;
				if ((sc = dmx[i - 1][k - 1] + hmm.tsc[k - 1][TDM]) > mmx[i][k])
					mmx[i][k] = sc;
				if (hmm.msc[(int) dsq[i]][k] != -INFTY)
					mmx[i][k] += hmm.msc[(int) dsq[i]][k];
				else
					mmx[i][k] = -INFTY;

				/* delete state */
				dmx[i][k] = -INFTY;
				if ((sc = mmx[i][k - 1] + hmm.tsc[k - 1][TMD]) > dmx[i][k])
					dmx[i][k] = sc;
				if ((sc = dmx[i][k - 1] + hmm.tsc[k - 1][TDD]) > dmx[i][k])
					dmx[i][k] = sc;

				/* insert state */
				if (k < hmm.M) {
					imx[i][k] = -INFTY;
					if ((sc = mmx[i - 1][k] + hmm.tsc[k][TMI]) > imx[i][k])
						imx[i][k] = sc;
					if ((sc = imx[i - 1][k] + hmm.tsc[k][TII]) > imx[i][k])
						imx[i][k] = sc;
					if (hmm.isc[(int) dsq[i]][k] != -INFTY)
						imx[i][k] += hmm.isc[(int) dsq[i]][k];
					else
						imx[i][k] = -INFTY;
				}
			}

			/*
			 * Now the special states. Order is important here. remember, C and
			 * J emissions are zero score by definition,
			 */
			/* N state */
			xmx[i][XMN] = -INFTY;
			if ((sc = xmx[i - 1][XMN] + hmm.xsc[XTN][LOOP]) > -INFTY)
				xmx[i][XMN] = sc;

			/* E state */
			xmx[i][XME] = -INFTY;
			for (k = 1; k <= hmm.M; k++)
				if ((sc = mmx[i][k] + hmm.esc[k]) > xmx[i][XME])
					xmx[i][XME] = sc;
			/* J state */
			xmx[i][XMJ] = -INFTY;
			if ((sc = xmx[i - 1][XMJ] + hmm.xsc[XTJ][LOOP]) > -INFTY)
				xmx[i][XMJ] = sc;
			if ((sc = xmx[i][XME] + hmm.xsc[XTE][LOOP]) > xmx[i][XMJ])
				xmx[i][XMJ] = sc;

			/* B state */
			xmx[i][XMB] = -INFTY;
			if ((sc = xmx[i][XMN] + hmm.xsc[XTN][MOVE]) > -INFTY)
				xmx[i][XMB] = sc;
			if ((sc = xmx[i][XMJ] + hmm.xsc[XTJ][MOVE]) > xmx[i][XMB])
				xmx[i][XMB] = sc;

			/* C state */
			xmx[i][XMC] = -INFTY;
			if ((sc = xmx[i - 1][XMC] + hmm.xsc[XTC][LOOP]) > -INFTY)
				xmx[i][XMC] = sc;
			if ((sc = xmx[i][XME] + hmm.xsc[XTE][MOVE]) > xmx[i][XMC])
				xmx[i][XMC] = sc;
		}
		/* T state (not stored) */
		sc = xmx[L][XMC] + hmm.xsc[XTC][MOVE];

		return sc; /* the total Viterbi score. */
	}

}
