package examples.generators.hmmer;

import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.add;
import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.createConstantValue;
import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.dMux;
import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.init;
import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.max;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.user.factory.UserDatapathFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserStorageFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserWireFactory;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;


public class HMMEROriginal  {

	int bitwidth = 16;
	
	//UserOperatorFactory op;
	UserStorageFactory regs;
	UserPadsFactory pads;
	UserWireFactory wireFactory; 	
	
	private BinaryOperator max3(SingleOutputDataFlowBlock a, SingleOutputDataFlowBlock b, SingleOutputDataFlowBlock c) {
		return max(max(a,b), c); 
	}

	private ShiftRegister loadableShiftRegister(SingleOutputDataFlowBlock prev, SingleOutputDataFlowBlock shiftCMD, int depth) {
		ShiftRegister sreg = regs.createShiftRegister(bitwidth, depth);
		SingleOutputDataFlowBlock dmux = dMux(prev,sreg, shiftCMD);
		wireFactory.connect(sreg.getInput(0),dmux.getOutput());
		wireFactory.connect(dmux.getInput(0),prev.getOutput());
		wireFactory.connect(dmux.getInput(1),sreg.getOutput());
		wireFactory.connect(dmux.getInput(2),shiftCMD.getOutput());
		return sreg; 
	}

	private ShiftRegister loadableShiftRegister(String name, SingleOutputDataFlowBlock prev, SingleOutputDataFlowBlock shiftCMD, int depth) {
		ShiftRegister sreg = regs.createShiftRegister(bitwidth, depth);
		sreg.setName("sreg_"+name);
		SingleOutputDataFlowBlock dmux = dMux(prev,sreg, shiftCMD);
		dmux.setName("dmux_"+name);
		wireFactory.connect(sreg.getInput(0),dmux.getOutput());
		wireFactory.connect(dmux.getInput(0),prev.getOutput());
		wireFactory.connect(dmux.getInput(1),sreg.getOutput());
		wireFactory.connect(dmux.getInput(2),shiftCMD.getOutput());
		return sreg; 
	}

	private BinaryOperator max5(SingleOutputDataFlowBlock a, SingleOutputDataFlowBlock b, SingleOutputDataFlowBlock c,SingleOutputDataFlowBlock d, SingleOutputDataFlowBlock e) {
		return max(max(a,b), max3(c,d,e)); 
	}

	
	public static void main(String[] args) {
		try {
		    HMMEROriginal hmmergen = new HMMEROriginal();
		    Datapath res  =hmmergen.generate(32, 8,16);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			if (args.length==0) {
				writer.save(res, "models/hmmer.datapath");
				DatapathVHDLGenerator project = new DatapathVHDLGenerator(res,"models",res.getName());
				project.generate();
			} else {
				writer.save(res, args[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	
	public Datapath generate (int M, int SIGMA, int width) {
		
		bitwidth=width;

		
		Datapath hmmer = UserDatapathFactory.eINSTANCE.createDatapath();
		hmmer.setName("HMMER");
		init(hmmer);
		
		regs = UserStorageFactory.getFactory(hmmer) ;
		pads = UserPadsFactory.getFactory(hmmer) ;
		wireFactory = UserWireFactory.getInstance(hmmer);

		DataInputPad Q = pads.createInputDataPad("Q", 5);
		
		DataInputPad loadTMM = pads.createInputDataPad("loadTMM", 1);
		DataInputPad loadTIM = pads.createInputDataPad("loadTIM", 1);
		DataInputPad loadTMI = pads.createInputDataPad("loadTMI", 1);
		DataInputPad loadTMD = pads.createInputDataPad("loadTMD", 1);
		DataInputPad loadTDM = pads.createInputDataPad("loadTDM", 1);
		
		DataInputPad TMMPipeIn = pads.createInputDataPad("TMMPipeIn", bitwidth);
		
		ControlPad CE = pads.createInputControlPad("CE", 1);

		int NBPE = M/SIGMA;

		ConstantValue[] C = new ConstantValue[NBPE];
		ConstantValue ZERO = createConstantValue("0",0,bitwidth);
		ConstantValue M_INFTY = createConstantValue("0",-32000,bitwidth);

		BinaryOperator[] xmx_next = new BinaryOperator[NBPE];
		BinaryOperator[] dmx_next = new BinaryOperator[NBPE];
		BinaryOperator[] imx_next = new BinaryOperator[NBPE];
		BinaryOperator[] mmx_next = new BinaryOperator[NBPE];
		
		ShiftRegister mmx_reg[] = new ShiftRegister[NBPE];
		ShiftRegister dmx_reg[] = new ShiftRegister[NBPE];
		ShiftRegister imx_reg[] = new ShiftRegister[NBPE];
		ShiftRegister xmx_reg[] = new ShiftRegister[NBPE];
		
		ShiftRegister TMM[] = new ShiftRegister[NBPE];
		ShiftRegister TIM[] = new ShiftRegister[NBPE];
		ShiftRegister TDM[] = new ShiftRegister[NBPE];
		ShiftRegister TMD[] = new ShiftRegister[NBPE];
		ShiftRegister TMI[] = new ShiftRegister[NBPE];
		ShiftRegister BSC[] = new ShiftRegister[NBPE];
		ShiftRegister ESC[] = new ShiftRegister[NBPE];

		for (int i=0;i<NBPE;i++) {
			mmx_reg[i]=  regs.shiftRegisterWithEnable("mmx_reg_"+i,bitwidth, SIGMA);
			imx_reg[i]=  regs.shiftRegisterWithEnable("imx_reg_"+i,bitwidth, SIGMA);
			dmx_reg[i]=  regs.shiftRegisterWithEnable("dmx_reg_"+i,bitwidth, SIGMA);
			xmx_reg[i]=  regs.shiftRegisterWithEnable("xmx_reg_"+i,bitwidth, SIGMA);
			wireFactory.connect(xmx_reg[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(imx_reg[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(dmx_reg[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(mmx_reg[i].getActivate().get(0), CE.getFlag(0));

		} 


		TMM[0] = loadableShiftRegister("TMMInitPipe_0",TMMPipeIn, loadTMM, SIGMA);
		TIM[0] = loadableShiftRegister("TIMInitPipe_0",TMMPipeIn, loadTIM, SIGMA);
		TMI[0] = loadableShiftRegister("TMIInitPipe_0",TMMPipeIn, loadTMI, SIGMA);
		TMD[0] = loadableShiftRegister("TMDInitPipe_0",TMMPipeIn, loadTMD, SIGMA);
		TDM[0] = loadableShiftRegister("TDMInitPipe_0",TMMPipeIn, loadTDM, SIGMA);
		
		for (int i=1;i<NBPE;i++) {
			TMM[i] = loadableShiftRegister("TMMInitPipe_"+i,TMM[i-1], loadTMM, SIGMA);
			TIM[i] = loadableShiftRegister("TIMInitPipe_"+i,TIM[i-1], loadTIM, SIGMA);
			TMI[i] = loadableShiftRegister("TMIInitPipe_"+i,TMI[i-1], loadTMI, SIGMA);
			TMD[i] = loadableShiftRegister("TMDInitPipe_"+i,TMD[i-1], loadTMD, SIGMA);
			TDM[i] = loadableShiftRegister("TDMInitPipe_"+i,TDM[i-1], loadTDM, SIGMA);
		}			

		for (int i=0;i<NBPE;i++) {
			wireFactory.connect(TMM[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(TIM[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(TMI[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(TMD[i].getActivate().get(0), CE.getFlag(0));
			wireFactory.connect(TDM[i].getActivate().get(0), CE.getFlag(0));

			xmx_next[i] = max5(
						M_INFTY,
						add(xmx_reg[i],TMM[i]), 
						add(imx_reg[i],TMI[i]),
						add(mmx_reg[i],TIM[i]), 
						add(dmx_reg[i],TDM[i])
					);
			
			if (i>1){
				dmx_next[i] = max(add(dmx_reg[i],dmx_reg[i-1]), M_INFTY);
				imx_next[i] = max(add(imx_reg[i],dmx_reg[i-1]), M_INFTY);
				mmx_next[i] = max(add(mmx_reg[i],dmx_reg[i-1]), M_INFTY);
			} else {
				dmx_next[i] = max(dmx_reg[i], M_INFTY);
				imx_next[i] = max(imx_reg[i], M_INFTY);
				mmx_next[i] = max(mmx_reg[i], M_INFTY);
			}
			
			wireFactory.connect(xmx_reg[i], xmx_next[i]);
			wireFactory.connect(imx_reg[i], imx_next[i]);
			wireFactory.connect(dmx_reg[i], dmx_next[i]);
			wireFactory.connect(mmx_reg[i], mmx_next[i]);
			
		}
		
//		DataOutputPad Y = pads.createAndConnectPad((SingleOutputDataFlowBlock)mmx[NBPE-1], "Y");
		return hmmer;
	}

}
