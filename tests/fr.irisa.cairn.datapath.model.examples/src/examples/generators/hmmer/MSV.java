package examples.generators.hmmer;




import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDMemoryFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.C_D;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.D_C;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.add;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.and;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.cmux;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.constant;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.max2;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.reduction;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.select;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.ICPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.IDPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.ODPad;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.cereg;

import fr.irisa.cairn.model.datapath.BlackBoxBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.analysis.DanglingPortDatapathAnalyzer;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SyncReadMemory;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.factory.systolic.FSMDSystolicFactory;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;

public class MSV {

	/**
	 * Naeem, plesae fill out these constant according to their actual value 
	 * in the source code
	 */
	private static final int TEJ = 125;
	private static final int TEC = 125;
	private static final int TLOOP = 125;
	private static final int TBMK= 125;
	private static final int TMOVE= 125;

	int bitwidth = 16;

	public static void main(String[] args) {
		try {
			MSV msv = new MSV();
			Datapath res = msv.generate(8, 8);
			DanglingPortDatapathAnalyzer analyzer = new DanglingPortDatapathAnalyzer(false);
			analyzer.analyze(res);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			if (args.length == 0) {
				writer.save(res, "models/MSV/msv.datapath");
/*				DatapathToDataflow dp2df = new DatapathToDataflow(res);
				DataflowModel model = dp2df.generate();
				DataflowXMLWriter.getXMLWriter().save(model, "models/MSV/msv.dataflow");
*/				DatapathVHDLGenerator project = new DatapathVHDLGenerator(res,"models/MSV/", res.getName());
				project.generate();
			} else {
				writer.save(res, args[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	public Datapath generate(int M,  int bitwidth ) {

		/*
		 * Defining I/O interface
		 */
		DataInputPad mode = IDPad("i1", 4);
		DataInputPad input = IDPad("i2", bitwidth);
		DataInputPad result = IDPad("i3", bitwidth);
		DataOutputPad output = ODPad("r_e_t_u_r_n", bitwidth);
		ControlPad CE = ICPad("CE", 1);
	
		/*
		 * Defining component
		 */
		Datapath hmmer = datapath("MSV", mode,input,CE,result, output);
		pushDatapath(hmmer);

		/*
		 * These are wire vectors containing cobinational signals which are going 
		 * to drive the registers containing the scores of mmx, xme and max.
		 */
		SingleOutputDataFlowBlock[] mmx_next = new SingleOutputDataFlowBlock[M+1];
		SingleOutputDataFlowBlock[] xme_next= new SingleOutputDataFlowBlock[M+1];
		SingleOutputDataFlowBlock[] max_prev= new SingleOutputDataFlowBlock[M+1];

		/*
		 * This is an array of memory components corresponding to the scalarized 
		 * version of the msc matrix.
		 */
		SyncReadSPRAM[] msc = new SyncReadSPRAM[M+1];
		
		CERegister mmx_reg[] = new CERegister[M+1];
		
			
		CERegister tloop = cereg(constant(TLOOP, bitwidth),CE);
		CERegister tej = cereg(constant(TEJ, bitwidth),CE);
		CERegister tec = cereg(constant(TEC, bitwidth),CE);
		CERegister tmove = cereg(constant(TMOVE, bitwidth),CE);
		CERegister tbmk = cereg(constant(TBMK, bitwidth),CE);
		ConstantValue MINFTNY = constant(123,bitwidth);

		/* Here we declare (and decode) the control signals which will
		 * drive the execution of the components. the control signal are 
		 * decoded from the 'mode' input port. 
		 * 
		 * Bit 1 : shiftDataCommand is used to shift configuration data 
		 * within the pipeline registers.
		 * 
		 * Bit 2 : writeRamCommand is used to write current prof_reg[] data values 
		 * into the  M distincts msc[] memory blocks.
		 * 
		 * Bit 3 : runMSVStage is used to command the execution of a MSV stage.
		 * 
		 * Bit 4 : selectOutputCommand is used to select what is output by the 
		 * component (should always be 0).
		 */
		SingleFlagBlock shiftDataCommand = D_C(and(select(mode,1),C_D(CE)));
		SingleFlagBlock writeRamCommand = D_C(and(select(mode,2),C_D(CE)));
		SingleFlagBlock	runMSVStage = D_C(and(select(mode,3),C_D(CE)));
		SingleFlagBlock selectOutputCommand = D_C(and(select(mode,4),C_D(CE)));


		// 
		// Profile input data pipeline
		//
		CERegister prof_reg[] = FSMDSystolicFactory.inputPipeline("prof_reg", input, shiftDataCommand, M);
		
		// 
		// Creating msc[] look-up tables
		//
		BitSelect mscAddress = select(input,0,4);
		for (int i = 1; i <= M; i++) {
			msc[i]= syncReadSPRam("msc_"+i,bitwidth, 32);
			msc[i].getAddressPort().connect(mscAddress);
			msc[i].getWritePort().connect(prof_reg[i]);
			msc[i].getWriteEnable().connect(writeRamCommand);
			msc[i].getReadEnable().connect(runMSVStage);
		}

		CERegister xmb_reg =cereg("xmb_reg",bitwidth,runMSVStage);
		CERegister xmn_reg = cereg("xmn_reg",bitwidth,runMSVStage);
		CERegister xmc_reg = cereg("xmc_reg",bitwidth,runMSVStage);
		CERegister xmj_reg = cereg("xmj_reg",bitwidth,runMSVStage);
		// 
		// Update MMX in parallel  
		//
		for (int i = 0; i <= M; i++) {
				mmx_reg[i] = cereg("mmx_reg_"+i,bitwidth,runMSVStage);
				if(i>0) {
					// To handle profiles size<M we simply need to initialize 
					// msc[i], xmb_reg and tbmk to M_INFTY so that we guarantee
					// 			mmx_next[i]= mmx_reg[i-1]
					
					mmx_next[i]= add(msc[i],max2(mmx_reg[i-1], add(xmb_reg,tbmk)));
				} else {
					mmx_next[i]= MINFTNY;
				}
				mmx_reg[i].connect(mmx_next[i]);
				max_prev[i]=mmx_next[i];
		}

		// 
		// Generates the max-Tree for computing XME
		//	
		SingleOutputDataFlowBlock xme ;
		for (int j = (int) Math.ceil(M/2.0); j >= 1; j=j/2) {
			for (int k = 0; k < j; k++) {
				SingleOutputDataFlowBlock left = max_prev[2*k];
				SingleOutputDataFlowBlock right = max_prev[2*k+1];
				xme_next[k]=max2(left,right);
			}
			max_prev= xme_next;
		}
		xme = xme_next[0];
		
 
		SingleOutputDataFlowBlock xmj_next = max2(add(xmj_reg,tloop), add(xme,tej));
		SingleOutputDataFlowBlock xmc_next = cmux(max2(add(xmc_reg,tloop), add(xme,tec)),MINFTNY,shiftDataCommand);
		SingleOutputDataFlowBlock xmn_next = add(xmn_reg,tloop);
		SingleOutputDataFlowBlock xmb_next = max2(add(xmn_next ,tmove), add(xmj_next , tmove));

		/* xmc, xmn, xmj and xmb registers are automaticcly initialized ot MINFTY when 
		 * in the configuration mode (that is when shiftDataCommand=1), otherwise they 
		 * are updated after each execution stage (that is when runCommand=1).
		 */
		xmc_reg.connect(cmux(xmc_next,MINFTNY,shiftDataCommand));
		xmn_reg.connect(cmux(xmn_next,MINFTNY,shiftDataCommand));
		xmb_reg.connect(cmux(xmb_next,MINFTNY,shiftDataCommand));
		xmj_reg.connect(cmux(xmj_next,MINFTNY,shiftDataCommand));
		
		ControlFlowMux cmux = cmux(mmx_reg[M-1],result,selectOutputCommand);
		cmux.setName("cmux");
		output.connect(cmux);
		return hmmer;
	}

}
