package examples.generators.hmmer.paralleprefix;

import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;

public class MaxPrefixOperator implements IPrefixOperator {

	@Override
	public SingleOutputDataFlowBlock prefix(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		return FSMDOperatorFactory.max2(A,B);
	}

}
