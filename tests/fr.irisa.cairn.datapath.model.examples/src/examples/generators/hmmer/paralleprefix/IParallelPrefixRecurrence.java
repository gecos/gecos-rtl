package examples.generators.hmmer.paralleprefix;

import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;

public interface IParallelPrefixRecurrence {
	
	public String getName(); 
	public SingleOutputDataFlowBlock[] generate(SingleOutputDataFlowBlock[] input); 

}
