package examples.generators.hmmer.paralleprefix;

import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.cmux;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.ICPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.IDPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.ODPad;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.cereg;

import java.io.IOException;

import org.eclipse.core.runtime.CoreException;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandVHDLGenerator;

/**
 * This example generates a Ladner/Fisher max-prefix parallel network.
 * 
 * @author sderrien
 */
public class ParallelPrefixGenerator {

	int bitwidth = 16;

	public static void main(String[] args) {
		try {

			for (int i = 8; i < 256; i = i * 2) {
				Datapath res = build(i, 16, new BrentKung(
						new MaxPrefixOperator()));
				save(res);
				res = build(i, 16,
						new LadnerFishnerGen(new MaxPrefixOperator()));
				save(res);
				res = build(i, 16, new KoggeStoneGen(new MaxPrefixOperator()));
				save(res);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void save(Datapath res) throws IOException, CoreException {
		DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
		writer.save(res, "./models/maxprefix/" + res.getName() + ".datapath");
		XPandVHDLGenerator project = new XPandVHDLGenerator(res,"./models/maxprefix/");
		project.compute();
	}

	public static Datapath build(int M, int width,
			IParallelPrefixRecurrence builder) {
		/*
		 * Defining I/O interface
		 */
		SingleOutputDataFlowBlock in[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock regout[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock out[];

		ControlPad CE = ICPad("CE", 1);
		ControlPad SHIFT = ICPad("Shift", 1);
		DataInputPad input = IDPad("in_data", width);
		DataOutputPad odPad = ODPad("out_data", width);

		Datapath parallelPrefix = datapath("DefaultName_" + M + "_" + width,
				CE, SHIFT, input, odPad);
		pushDatapath(parallelPrefix);
		parallelPrefix.setName(builder.getName() + "_" + M);
		for (int i = 0; i < M; i++) {
			if (i == 0) {
				in[i] = cereg("inreg"+i,input, SHIFT);
			} else {
				in[i] = cereg("inreg"+i,in[i - 1], SHIFT);
			}
		}
		/*
		 * Defining component
		 */

		out = builder.generate(in);

		for (int i = 0; i < M; i++) {
			if (i == 0) {
				regout[0] = cereg("outreg"+i,out[0], CE);
			} else {
				regout[i] = cereg("outreg"+i,cmux(out[i], regout[i - 1], SHIFT), CE);
			}
		}

		odPad.connect(regout[M-1]);
		return parallelPrefix;
	}

	public static int log2(double num) {
		return (int) Math.ceil(Math.log(num) / Math.log(2));
	}

}
