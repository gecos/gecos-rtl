package examples.generators.hmmer.paralleprefix;

import java.io.FileNotFoundException;

import prettyprint.SVGPrettyPrinter;
import top.Color;
import top.Diagram;
import static factory.SVGFactory.*;

public class ParallelPrefixSVGRenderer {
	
	private static final int DEFAULT_WIDTH = 640;
	private static final int DEFAULT_HEIGHT = 480;
	private int scaleX = 10;
	private int scaleY = 10;

	Diagram diagram;
	String name ;
	private int offsetY;
	private int offsetX;
	
	public ParallelPrefixSVGRenderer(String name, int width, int depth) {
		diagram = diagram(DEFAULT_WIDTH+scaleX, DEFAULT_HEIGHT+scaleY);
		scaleX=DEFAULT_WIDTH/width;
		scaleY=DEFAULT_HEIGHT/(depth);
		offsetX = scaleX;
		offsetY= scaleY;
		this.name=name;
	}

	public void passThrough(int x, int y) {
		scaledLine(x, y,x,y-1);
		scaledCircle(x,y,Color.WHITE);
	}
	
	public void prefix(int x, int y,int x1) {
		scaledLine(x, y,x1,y-1);
		scaledLine(x, y,x,y-1);
		scaledCircle(x,y,Color.BLACK);
	}

	private void scaledLine(int x, int y, int x1, int y1) {
		diagram.getElements().add(line(scaleX*(x)+offsetX, scaleY*(y)+offsetY, scaleX*(x1)+offsetX, scaleY*(y1)+offsetY));
	}

	private boolean scaledCircle(int x, int y,Color fill) {
		return diagram.getElements().add(circle(scaleX*(x)+offsetX, scaleY*(y)+offsetY, scaleX/5,fill));
	}
	
	public void save() throws FileNotFoundException {
		SVGPrettyPrinter pp = new SVGPrettyPrinter(diagram, name);
		pp.generate();
	}
}
