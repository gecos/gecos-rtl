package examples.generators.hmmer.paralleprefix;

import java.io.FileNotFoundException;

import prettyprint.SVGPrettyPrinter;
import top.Diagram;
import factory.SVGFactory;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;

public class BrentKung implements IParallelPrefixRecurrence {

	int depth=0;
	
	IPrefixOperator myprefix;
	ParallelPrefixSVGRenderer svg;
	public BrentKung(IPrefixOperator myprefix) {
		this.myprefix=myprefix;
	}
	
	public static int log2(int num)	{
		return (int) Math.ceil(Math.log(num)/Math.log(2));
	} 

	@Override
	public String getName() {
		return "BrentKung";
	}

	@Override
	public SingleOutputDataFlowBlock[] generate(SingleOutputDataFlowBlock[] input) {
		depth=1;
		svg = new ParallelPrefixSVGRenderer("models/maxprefix/BrentKung_"+input.length, input.length, 2*log2(input.length)-1);
		SingleOutputDataFlowBlock[] res = generateRec(input, 1);
		try {
			svg.save();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	public SingleOutputDataFlowBlock[] generateRec(SingleOutputDataFlowBlock[] input, int step) {
		int M = input.length;
		if(step<(M/2)) {
			SingleOutputDataFlowBlock[] res= new SingleOutputDataFlowBlock[M];
			SingleOutputDataFlowBlock[] stageTop= new SingleOutputDataFlowBlock[M];
			SingleOutputDataFlowBlock[] stageBottom= new SingleOutputDataFlowBlock[M];
			for(int i=0;i<M;i+=1) {
				if(i%(2*step)==0 && i+step<M) {
					// even index
					stageTop[i]=myprefix.prefix(input[i], input[i+step]);
					svg.prefix(i, depth, i+step);
				} else {
					stageTop[i]=input[i];
					svg.passThrough(i,depth);
				}
			}
			depth++;
			// recursive call
			stageBottom = generateRec(stageTop, step*2);
			
			for(int i=0;i<M;i+=1) {
				if(i%(2*step)==step && i+step<M) {
					// even index
					res[i]=myprefix.prefix(stageBottom[i], stageBottom[i+step]);
					svg.prefix(i, depth, i+step);
				} else {
					res[i] = stageBottom[i];
					svg.passThrough(i,depth);

				}
			}
			depth++;
			return res;
		} else {
			return input;
		}
	}

}
