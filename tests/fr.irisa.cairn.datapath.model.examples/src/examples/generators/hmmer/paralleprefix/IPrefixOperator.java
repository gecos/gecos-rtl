package examples.generators.hmmer.paralleprefix;

import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;

public interface IPrefixOperator {

	public SingleOutputDataFlowBlock prefix(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B);

}
