#include <stdio.h>
#include <stdlib.h>

#define max2fn(a,b) (a>b)?a:b

#define M 256
#define H_Psize M/2
#define Y 8 // Log2(M)
#define INFTY  987654321

int filter_msv(int in_data,int *out_data,int mode){

	static int counter=0;
	static int i=0;
	static int j=0;
	int ii;

	static int tbmk;
	static int tej;
	static int tec;
	static int tmove;
	static int tloop;

	static int hmm_msc[24][M];
	static int mmx[M];
	static int tmp_mmx[M];
	static int tmp_xme[H_Psize];

	static int xme, xmj,xmc,xmn,xmb;

	static int curr_dsq;

	switch(mode){
	case 0: // Data Receiving Mode
			hmm_msc[j][i]= in_data;
			if(i==M-1){
				j++;
			}
		counter++;
		i++;
		if(counter%M==0){
			i=0;
		}
		break;
	case 1: // Intialization Mode
		for(i=0;i<M;i++){
			mmx[i]= -INFTY;
		}
		break;
	case 2: // Computation Mode
		curr_dsq=in_data;
		for(i=1;i<M;i++){
			tmp_mmx[i] = hmm_msc[curr_dsq][i] + max2fn(mmx[i-1],xmb+tbmk);
		}

		xme=-INFTY;
		for(j=0;j<H_Psize;j++){
			tmp_xme[j] = max2fn(tmp_mmx[j*2] , tmp_mmx[j*2+1]);
		}

		for(ii=1;ii<=Y;ii++){
			for(j=0;j<(H_Psize >> ii);j++){
				tmp_xme[j] = max2fn(tmp_xme[j*2] , tmp_xme[j*2+1]);
			}
		}
		xme = max2fn(xme , tmp_xme[0]);

		for (j=0;j<M;j++){
			mmx[j]= tmp_mmx[j];
		}


		xmj = max2fn(xmj+tloop, xme+tej);
		xmc = max2fn(xmc+tloop, xme+tec);
		xmn = xmn + tloop;
		xmb = max2fn(xmn + tmove, xmj + tmove);
		break;
	case 3: // Results Mode
		*out_data=xmc;
		break;
	}
	return 1;
}



int main()
{
	int in_data,out_data,mode,status;
	out_data=0;
	status=filter_msv(in_data,&out_data,mode);
	printf("%d",out_data);
	return 1;
}