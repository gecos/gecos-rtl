package examples.generators.hmmer;

import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.*;
import static fr.irisa.cairn.model.datapath.user.FSMDMemoryFactory.*;
import static fr.irisa.cairn.model.datapath.user.factory.systolic.FSMDSystolicFactory.*;

import javax.management.RuntimeErrorException;

import examples.generators.hmmer.paralleprefix.BrentKung;
import examples.generators.hmmer.paralleprefix.MaxPrefixOperator;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.analysis.DanglingPortDatapathAnalyzer;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;
import fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory;
import fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserDatapathFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserStorageFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserWireFactory;

import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;


public class P7Viterbi  {

	private static final int XMN = 0;
	private static final int XMB = 1;
	private static final int XMJ = 2;
	private static final int XME = 3;
	private static final int XMC = 4;

	private static final int TMM = 0;
	private static final int TIM = 1;
	private static final int TDM = 2;
	private static final int TMD = 3;
	private static final int TDD = 4;
	private static final int TMI = 5;
	private static final int TII = 7;
	
	private static final int LOOP = 0;
	private static final int MOVE = 1;
	
	private static final int XTN = 0;
	private static final int XTJ = 1;
	private static final int XTC = 2;
	private static final int XTE = 3;

	public static void main(String[] args) {
		try {
			P7Viterbi p7viterbi = new P7Viterbi();
			Datapath res = p7viterbi.generate(16, 16);
			DanglingPortDatapathAnalyzer analyzer = new DanglingPortDatapathAnalyzer(false);
			analyzer.analyze(res);
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			if (args.length == 0) {
				writer.save(res, "models/MSV/p7viterbi.datapath");
//				DatapathToDataflow dp2df = new DatapathToDataflow(res);
//				DataflowModel model = dp2df.generate();
//				DataflowXMLWriter.getXMLWriter().save(model, "models/MSV/msv.dataflow");
				DatapathVHDLGenerator project = new DatapathVHDLGenerator(res,"models/MSV/", res.getName());
				project.generate();
			} else {
				writer.save(res, args[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public P7Viterbi() {
		// TODO Auto-generated constructor stub
	}
	
	Datapath hmmer ;
	
	private SingleOutputDataFlowBlock max5(
			SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B,
			SingleOutputDataFlowBlock C,
			SingleOutputDataFlowBlock D,
			SingleOutputDataFlowBlock E) {
		return max2(max2(max2(A,B),max(C,D)),E);
	}

	private SingleOutputDataFlowBlock max4(
			SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B,
			SingleOutputDataFlowBlock C,
			SingleOutputDataFlowBlock D) {
		return max2(max2(A,B),max(C,D));
	}

	private SingleOutputDataFlowBlock max3(
			SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B,
			SingleOutputDataFlowBlock C) {
		return max2(max2(A,B),C);
	}

	private SingleOutputDataFlowBlock max(SingleOutputDataFlowBlock... terms) {
		// 
		// Generates the max-Tree for computing XME
		//	
		int M = terms.length;
		SingleOutputDataFlowBlock res_prev[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock res_next[] = new SingleOutputDataFlowBlock[M];
		res_prev=terms;
		int powerof2bound = (int) Math.ceil(M/2.0);
		for (int j = powerof2bound; j >= 1; j=j/2) {
			for (int k = 0; k < j; k++) {
				int left_index = 2*k;
				if(left_index<M-1) {
					SingleOutputDataFlowBlock left = res_prev[left_index];
					SingleOutputDataFlowBlock right = res_prev[left_index+1];
					res_next[k]=max2(left,right);
				} else {
					res_next[k]=res_prev[left_index];
				}
			}
			res_prev= res_next;
		}
		for(SingleOutputDataFlowBlock term :terms) {
			if (term.getOutput().getWires().size()==0) {
				throw new RuntimeException();
			}
		} 
		return res_next[0];

	}
	public Datapath generate(int bitwidth, int M) {
		DataInputPad mode = IDPad("i1", 4);
		DataInputPad input = IDPad("i2", bitwidth);
		DataInputPad result = IDPad("i3", bitwidth);
		DataOutputPad output = ODPad("r_e_t_u_r_n", bitwidth);
		ControlPad CE = ICPad("CE", 1);
	
		/*
		 * Defining component
		 */
		hmmer = datapath("P7Viterbi", mode,input,CE,result, output);
		pushDatapath(hmmer);

		SingleFlagBlock shiftConfig = D_C(select(mode, 0));
		
		CERegister bsc[] = inputPipeline("bsc", input, shiftConfig, M);
		CERegister esc[] = inputPipeline("esc", bsc[M-1], shiftConfig, M);
		CERegister tsc[][] = new CERegister[8][];
		CERegister xsc[][] = new CERegister[4][2];
		for(int i=0;i<8;i++) {
			tsc[i]=inputPipeline("tsc_"+i, i>0?tsc[i-1][M-1]:esc[M-1], shiftConfig, M);
		}
		for(int i=0;i<4;i++) {
			xsc[i]=inputPipeline("xsc_"+i, i>0?xsc[i-1][1]:tsc[7][M-1], shiftConfig, 2);
		}
		CERegister isc_msc_din[] = inputPipeline("isc_msc_din", input, shiftConfig, M);

		SingleOutputDataFlowBlock mmx_next[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock dmx_next[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock imx_next[] = new SingleOutputDataFlowBlock[M];
		
		SingleOutputDataFlowBlock xmx_next[] = new SingleOutputDataFlowBlock[5];

		CERegister mmx_reg[] = ceregVector("mmx_reg", bitwidth, M,CE);
		CERegister dmx_reg[] = ceregVector("dmx_reg", bitwidth, M,CE);
		CERegister imx_reg[] = ceregVector("imx_reg", bitwidth, M,CE);

		CERegister xmx_reg[] = ceregVector("xmx_reg", bitwidth, 5,CE);

		SyncReadSPRAM msc[] = new SyncReadSPRAM[M];
		SyncReadSPRAM isc[] = new SyncReadSPRAM[M];

		/**
		 * Instancianting the isc/isc look-up tables 
		 */
		BitSelect ramAddress = select(input,0,4);
		BitSelect msc_we = select(mode,0);
		BitSelect lookupTableRe = select(mode,1);
		BitSelect isc_we = select(mode,2);
		//BitSelect resetRegs = 
		Data2CtrlBuffer resetRegs = D_C(select(mode, 3));

		
		for (int k=0;k<M;k++) {
			CERegister din = isc_msc_din[k]; 
			msc[k] = syncReadSPRam("msc_"+k, bitwidth, 32,ramAddress,din,D_C(lookupTableRe),D_C(msc_we));
			isc[k] = syncReadSPRam("isc_"+k, bitwidth, 32,ramAddress,din,D_C(lookupTableRe),D_C(isc_we));
		}		
		SingleOutputDataFlowBlock M_INFTY = constant(-30000, bitwidth);

		for (int k=0;k<M;k++) {
			if(k<1) {
				mmx_next[k] = max2(M_INFTY,	add(xmx_reg[XMB],bsc[k]));
			} else {
				mmx_next[k] = max5(M_INFTY,
						add(mmx_reg[k-1],tsc[TMM][k]),
						add(imx_reg[k-1],tsc[TIM][k]),
						add(dmx_reg[k-1],tsc[TDM][k]),
						add(xmx_reg[XMB],bsc[k]));

			}
			mmx_next[k]=add(mmx_next[k],msc[k]);		
			mmx_reg[k].connect(cmux(mmx_next[k],M_INFTY,resetRegs));
		}
		

		// 
		// Generates the max-Tree for computing DMX
		//	
		SingleOutputDataFlowBlock[] dmx_prefix = new BrentKung(new MaxPrefixOperator()).generate(mmx_next);

		for (int k=0;k<M;k++) {
			/* Delete state */
			dmx_next[k] = max3(M_INFTY,
					add(mmx_next[k],tsc[TMD][k]),
					add(dmx_prefix[k],tsc[TDD][k]));
			dmx_reg[k].connect(cmux(dmx_next[k],M_INFTY,resetRegs));
		}
		
		for (int k=0;k<M;k++) {
			/* Insert state */
			imx_next[k] = max3(M_INFTY,
					add(mmx_reg[k],tsc[TMI][k]),
					add(imx_reg[k],tsc[TII][k]));
			
			imx_next[k]=add(imx_next[k],isc[k]);		
			imx_reg[k].connect(cmux(imx_next[k],M_INFTY,resetRegs));
		}
		
		
		/*
		 * Now the special states. Order is important here. remember, C and
		 * J emissions are zero score by definition,
		 */

		/* N state */
		xmx_next[XMN] = max2(M_INFTY,add(xmx_reg[XMN] , xsc[XTN][LOOP]));

		/* E state */
		// 
		// Generates a max-Tree for computing XME
		//	
		xmx_next[XME] = maxTree(M, mmx_next);

		/* J state */
		xmx_next[XMJ] = max2(M_INFTY,add(xmx_reg[XMJ] , xsc[XTJ][LOOP]));

		/* B state */
		xmx_next[XMB] = max2(add(xmx_next[XMN],xsc[XTN][MOVE]),add(xmx_next[XMJ],xsc[XTJ][MOVE])); 

		/* C state */
		xmx_next[XMC] = max2(add(xmx_reg[XMC] , xsc[XTC][LOOP]),add(xmx_next[XME] , xsc[XTE][MOVE]));

		xmx_reg[XMN].connect(cmux(xmx_next[XMN],M_INFTY,resetRegs));
		xmx_reg[XME].connect(cmux(xmx_next[XME],M_INFTY,resetRegs));
		xmx_reg[XMJ].connect(cmux(xmx_next[XMJ],M_INFTY,resetRegs));
		xmx_reg[XMB].connect(cmux(xmx_next[XMB],M_INFTY,resetRegs));
		xmx_reg[XMC].connect(cmux(xmx_next[XMC],M_INFTY,resetRegs));
			
		output.connect(add(xmx_next[XMC] , xsc[XTC][MOVE]));

		return hmmer;
	}
	
	private SingleOutputDataFlowBlock maxTree(int M, SingleOutputDataFlowBlock[] mmx_next) {
		SingleOutputDataFlowBlock xmx_next[] = new SingleOutputDataFlowBlock[M];
		SingleOutputDataFlowBlock xmx_prev[] = new SingleOutputDataFlowBlock[M];
		xmx_prev= mmx_next;
		for (int j = (int) Math.ceil(M/2.0); j >= 1; j=j/2) {
			for (int k = 0; k < j; k++) {
				SingleOutputDataFlowBlock left = xmx_prev[2*k];
				SingleOutputDataFlowBlock right = xmx_prev[2*k+1];
				xmx_next[k]=max2(left,right);
			}
			xmx_prev= xmx_next;
		}
		return xmx_next[0];
	}
}
