package examples.generators.fft;

import fr.irisa.cairn.model.datapath.impl.DatapathImpl;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserStorageFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserWireFactory;

/**
 * Incomplete implementation of a FFT core using the FSMS MetaModel 
 * @author sderrien
 */
@Deprecated
public class ParallelFFT extends DatapathImpl {

//	private void ButterFly(
//			OutputDataPort inA,
//			SingleOutputDataFlowBlock inB,
//			SingleInputDataFlowBlock outA,
//			SingleInputtDataFlowBlock outB,
//			
//	) {
//		
//	}
	
	int bitwidth = 16;
	
	UserOperatorFactory op;
	UserStorageFactory regs;
	UserPadsFactory pads;
	UserWireFactory wireFactory; 	
	
	public ParallelFFT(int size, int width) {
		this.bitwidth=width;
		this.name="FFT"+size;

		op = UserOperatorFactory .getFactory(this);
		regs = UserStorageFactory.getFactory(this) ;
		pads = UserPadsFactory.getFactory(this) ;
		wireFactory = UserWireFactory.getInstance(this);

		DataInputPad[] X = new DataInputPad[size];
		ConstantValue[] C = new ConstantValue[size];
		for(int i=0;i<size;i++) {
			X[i] = pads.createInputDataPad("X_"+i+"", bitwidth);
			int QsizeCos = (int)((1<<(size-1))*Math.cos(i*Math.PI/size));
			C[i] = op.createConstantValue("COS_"+i, QsizeCos);
		}
		
		StatusPad CE = pads.createOutputControlPad("CE", 1);

		CERegister[] reg = new CERegister[size];
		BinaryOperator[] add = new BinaryOperator[size];

		for (int i=0;i<size;i++) {
//			C[i] = op.createConstantValue("C_"+i, coef[i]);
//			this.getComponents().add(C[i]);
//			if (i==0) {
//				add[0] =op.muls(X, C[i]);
//			} else {
//				reg[i] = regs.createCERegister("reg_"+i+"", bitwidth);
//				wireFactory.connect(reg[i].getActivate().get(0), CE.getFlag(0));
//				if (i>1) {
//					wireFactory.connect(reg[i], reg[i-1]);
//				} else {
//					wireFactory.connect(reg[i], X);
//				}
//				add[i] = op.add(op.muls(reg[i], C[i]), add[i-1]);
//			}
		}
		
		DataOutputPad Y = pads.createAndConnectPad((SingleOutputDataFlowBlock)add[size-1], "Y"); 
	}

}
