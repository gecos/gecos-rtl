package examples.generators.hardwaretask.datapath.components;

import fr.irisa.cairn.model.datapath.impl.DatapathImpl;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPadsFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserWireFactory;


public class AluBlock extends DatapathImpl {

	int bitwidth = 8;
	
	UserOperatorFactory operators;
	UserPadsFactory iopads;
	UserWireFactory wires;
		
	
	public AluBlock() {
		

		operators = UserOperatorFactory .getFactory(this);
		
		
		iopads = UserPadsFactory.getFactory(this) ;
		wires = UserWireFactory.getInstance(this);
		
		this.setName("ALU8");
		
		DataInputPad op1 = iopads.createInputDataPad("OP1", bitwidth);
		DataInputPad op2 = iopads.createInputDataPad("OP2", bitwidth);
		DataOutputPad aluOut = iopads.createOutputDataPad("aluOut", bitwidth);
		//DataOutputPad ltgt = iopads.createDataOutputPad("LT_GT", 3);
		
		ControlPad aluOp0 = iopads.createInputControlPad("aluOp0",1);
		ControlPad aluOp1 = iopads.createInputControlPad("aluOp1",1);
		ControlPad aluOp2 = iopads.createInputControlPad("aluOp2",1);
		
		BinaryOperator add = operators.add(op1, op2);
		add.setName("ADD");
		
		BinaryOperator sub = operators.sub(op1, op2);
		sub.setName("SUB");
		
		UnaryOperator not = operators.not(op1);
		not.setName("NOT");
		
		BinaryOperator and = operators.and(op1, op2);
		and.setName("AND");
		
		BinaryOperator or = operators.or(op1, op2);
		or.setName("OR");
		
		BinaryOperator xor = operators.xor(op1, op2);
		xor.setName("XOR");
		
		BinaryOperator shl = operators.shl(op1, 1);
		shl.setName("SHL");
		
		BinaryOperator shr = operators.shr(op1, 1);
		shr.setName("SHR");
				
		ControlFlowMux aluOutMUX0 = operators.cMux(add, not, aluOp0.getFlags().get(0));
		aluOutMUX0.setName("outMUX0");
		
		ControlFlowMux aluOutMUX1 = operators.cMux(sub, and, aluOp0.getFlags().get(0));
		aluOutMUX1.setName("outMUX1");
		
		ControlFlowMux aluOutMUX2 = operators.cMux(or, xor, aluOp0.getFlags().get(0));
		aluOutMUX2.setName("outMUX2");
		
		ControlFlowMux aluOutMUX3 = operators.cMux(shl, shr, aluOp0.getFlags().get(0));
		aluOutMUX3.setName("outMUX3");
		
		ControlFlowMux aluOutMUX4 = operators.cMux(aluOutMUX0, aluOutMUX1, aluOp1.getFlags().get(0));
		aluOutMUX4.setName("outMUX4");
		
		ControlFlowMux aluOutMUX5 = operators.cMux(aluOutMUX2, aluOutMUX3, aluOp1.getFlags().get(0));
		aluOutMUX5.setName("outMUX5");
		
		ControlFlowMux aluOutMUX6 = operators.cMux(aluOutMUX4, aluOutMUX5, aluOp2.getFlags().get(0));
		aluOutMUX6.setName("outMUX6");
		
		wires.connect(aluOut, aluOutMUX6);
	}

}
