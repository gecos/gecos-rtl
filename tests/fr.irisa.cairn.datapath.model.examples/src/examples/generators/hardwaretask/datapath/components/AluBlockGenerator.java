package examples.generators.hardwaretask.datapath.components;

import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;
import fr.irisa.cairn.model.datapath.xpand.codegen.modules.XPandVHDLGenerator;

public class AluBlockGenerator {
	
	public static void main(String[] args) {
		try {
			AluBlock alu = new AluBlock();
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			if (args.length==0) {
				writer.save(alu, "models/"+alu.getName()+".datapath");
				XPandVHDLGenerator project = new XPandVHDLGenerator(alu,"./models/");
				project.compute();
			} else {
				writer.save(alu, args[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
