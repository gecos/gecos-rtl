package examples.generators.cpus;

import static fr.irisa.cairn.model.datapath.user.FSMDFactory.datapath;
import static fr.irisa.cairn.model.datapath.user.FSMDFactory.pushDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.D_C;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.add;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.and;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.constant;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.decode;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.dmux;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.eq;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.merge;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.select;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.sub;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.IDPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.OCPad;
import static fr.irisa.cairn.model.datapath.user.FSMDPadsFactory.ODPad;
import static fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory.cereg;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.user.FSMDFactory;
import fr.irisa.cairn.model.datapath.user.FSMDMemoryFactory;

public class SimpleDLXCPUGenerator {
	
	
	public static void main(String[] args) {
		
	}


	public static Datapath generate(int width) {

		String name="DLX"  + "_" + width;

		DataInputPad IMEM_OUT = IDPad( "IMEM_DOUT", width);
		DataInputPad DMEM_OUT = IDPad( "DMEM_DOUT", width);

		DataOutputPad IMEM_AD = ODPad( "IMEM_AD", width);
		StatusPad IMEM_RE = OCPad( "IMEM_RE", width);

		DataOutputPad DMEM_AD = ODPad( "IMEM_AD", width);
		DataOutputPad DMEM_IN = ODPad( "DMEM_DIN", width);
		DataOutputPad DMEM_WE = ODPad( "DMEM_WE", 1);
		DataOutputPad DMEM_RE = ODPad( "DMEM_RE", 1);

		
		pushDatapath( datapath( name, IMEM_AD, IMEM_OUT, IMEM_RE, DMEM_AD,DMEM_OUT,DMEM_IN,DMEM_RE,DMEM_WE));

		InControlPort endLoop, inEmpty, outFull; 
        
		
		SingleFlagBlock pipe_enable = D_C(and(1));

	
		

		
		CERegister fetch_reg = cereg( "FETCH_reg", IMEM_OUT,pipe_enable);

		/**
		 * 
		 * 				DC stage 
		 *
		 */
		
		// Instanciating the REgister file as a dual port RAM
		DualPortRam rf = FSMDMemoryFactory.dpram("RF", width, 32); 

		rf.getAddressPort(0).connect(select(fetch_reg,3,0));
		rf.getAddressPort(1).connect(select(fetch_reg,7,4));
		

		SingleOutputDataFlowBlock dc_imm32 = merge(
				dmux(
						constant(1<<16-1,16),
						constant(0, 16),
						select(fetch_reg,15)
					),
				select(fetch_reg,15,0)
		);
		
		
		SingleOutputDataFlowBlock opA = dmux(
				dc_imm32.getOutput(),
				rf.getReadPort(0),
				select(fetch_reg,3,0).getOutput());
		
		OutDataPort opB = rf.getReadPort(1);

			
		BitSelect fetch_RDest = select(fetch_reg,11,8);
		BitSelect fetch_IR = select(fetch_reg,11,8);
		
		CERegister DC_reg = 
			cereg("DC_reg",	
				merge("",
					fetch_reg.getOutput(), 
					opA.getOutput(),
					opB
				),
				pipe_enable
			);

		/**
		 * 
		 * 				EX stage 
		 *
		 */
		SingleOutputDataFlowBlock exe_alu_out = dmux(
				
				select(DC_reg,3,0),
				add(opA.getOutput(),opB),	
				sub(opA.getOutput(),opB)//,	
//				and(opA.getOutput(),opB),	
//				or(opA.getOutput(),opB),	
//				xor(opA.getOutput(),opB)	
			);
		
		SingleOutputDataFlowBlock zero =eq(constant(0, 32), rf.getReadPort(0)); 

		CERegister EX_reg = cereg( "EX_reg", 
				merge(exe_alu_out,select(DC_reg,31,0)), pipe_enable);

		

		
		/**
		 * 
		 * 				EX stage 
		 *
		 */
		CERegister MEM_reg = cereg("MEM_reg", merge(select(EX_reg,63,0),DMEM_OUT),pipe_enable);

		

		BitSelect mem_read_data = select(MEM_reg,95,64);
		rf.getWritePort(0).connect(mem_read_data);
		rf.getWritePort(1).connect(mem_read_data);

		BitSelect MEM_opcode = select(MEM_reg,31,28);
		rf.getWriteEnable(0).connect(D_C(
				decode("dec",
						MEM_opcode.getOutput(), 
						0x5,0x2,0x1,0x3 // opcode that cause a memory write to teh processor
					)));
		

		
		/* Ram containing the N=256 last samples */ 
//		SinglePortRam xram = syncReadSPRam( "XRam", width, 256, adx.getOutput(), X.getOutput(), xrdEnable, xwrEnable, mul.getInput(1));


		Datapath datapath = FSMDFactory.popDatapath();
		if( datapath==null ) 
			throw new RuntimeException("Empty datapath");
		
		return datapath;

	}
}
