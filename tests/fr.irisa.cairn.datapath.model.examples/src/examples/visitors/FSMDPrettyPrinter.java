package examples.visitors;

import org.eclipse.emf.ecore.EObject;

import examples.generators.digitalfilters.SequentialFIRGenerator;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.user.visitors.AbstractFSMDVisitor;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.State;

public class FSMDPrettyPrinter extends AbstractFSMDVisitor{

	public static void main(String[] args) {
		FSMDPrettyPrinter obj = new FSMDPrettyPrinter();
		obj.printTree(SequentialFIRGenerator.generate(new int[]{1,57,23,324,45}, 15));
	}
	int tab=0;

	
	public void printTree(EObject current) {
		Object txt = doSwitch(current);
		if (txt!=null) {
			for(int i=0;i<tab;i++) 
				System.out.print("  ");
			System.out.println("-"+txt);
		}
		tab++;
		for (EObject child : current.eContents()) {
			printTree(child);
		}
		tab--;
	}

	@Override
	public Object caseBooleanCommandValue(BooleanCommandValue object) {
		return "visited " +object.toString();
	}

	@Override
	public Object caseCERegister(CERegister object) {
		return "visited " +object.toString();
	}

	@Override
	public Object caseControlFlowWire(ControlFlowWire object) {
		return "visited " +object.toString();
	}

	@Override
	public Object caseDataInputPad(DataInputPad object) {
		return "visited " +object.toString();
	}

	@Override
	public Object caseInDataPort(InDataPort object) {
		return "visited " +object.toString();
	}

	@Override
	public Object caseState(State object) {
		return "visited " +object.toString();
	}

	
}
