
#include <stdio.h>
#include "co.h"
#include "cosim_log.h"
#include "template.h"

// NAEEM, pls check this.
APPCOTYPE FUNCMSV(APPCOTYPE a, APPCOTYPE b,APPCOTYPE c, APPCOTYPE d){
#pragma CO implementation funcmsv pipeline latency=1 
	return (a==EOS)?(max(c+d,b):M_INFTY;
}


/*

To ease control/synchronisation, I suggest we use a packet format for communication 
between the host and the FPGA. The idea is to have a constant size atomic communictaion 
unit which will make communication synchronisation much easier.

For a 4 interleaved instance, with a chunk  size of 64 the stream shoudl look like this
chunk number 1:
	size[0]=123  // Size of sequence for the MSV instance n�1
	size[1]=-1   // we are still within the same sequence in MSV instance n�2
	size[2]=35   // Size of sequence for the MSV instance n�3
	size[3]=-1   // we are still within the same sequence in MSV instance n�4
	seq[0][0]    // first base of new sequence for MSV instance n�1
	seq[0][1]    // n^th base of current sequence for MSV instance n�2
	seq[0][2]    // first base of new sequence for MSV instance n�3
	seq[0][3]    // m^th base of current sequence for MSV instance n�4
	seq[1][0]    // second base of new sequence for MSV instance n�1
	seq[1][1]    // (n+1)^th base of current sequence for MSV instance n�2
	...
	seq[63][0]    // 64th base of sequence for MSV instance n�1
	seq[63][1]    // n+64^th base of current sequence for MSV instance n�2
	seq[63][2]    // padding data (EOS tag) as sequence for MSV instance n�3 is nonly 35 in size
	
chunk number 2:
	size[0]=-1    // we are still within the same sequence in MSV instance n�1 (size was 123)
	size[1]=-1   // we are still within the same sequence in MSV instance n�2
	size[2]=456  // Size of new sequence for the MSV instance n�3 as previous one is finisehd
	size[3]=-1   // we are still within the same sequence in MSV instance n�4
	seq[0][0]    // first base of new sequence for MSV instance n�1
	seq[0][1]    // n^th base of current sequence for MSV instance n�2
	seq[0][2]    // first base of new sequence for MSV instance n�3
	seq[0][3]    // m^th base of current sequence for MSV instance n�4
	seq[1][0]    // second base of new sequence for MSV instance n�1
	seq[1][1]    // (n+1)^th base of current sequence for MSV instance n�2
	...
	seq[63][0]    // last base (or padded data) of sequence for MSV instance n�1
	seq[63][1]    // second base of new sequence for MSV instance n�2

To make is short, in the following we assume, that *any* sequence will have its first character base 
at offset seq[0][i] in a chunck, to ensure so we pad sequences with EOS tags so as to make them fill chucnk
2) I assume (didn't had time to write it) that you use the CO NONRECURSIVE Pragma 
for all mmx arrays in the loop.

*/
void MSV_process() {

	while(1) {
		/**
			HMM configuration
		**/
		// Reading target HMM profile size 
		co_stream_read(stream_hmm_profile, &M, sizeof(APPCOTYPE));
		// Loading tsc profile info into all tsc_ registers
		for(int i=0;i<M;i++){ 
			#pragma CO PIPELINE
			co_stream_read(stream_hmm_profile, &tsc[0], sizeof(APPCOTYPE));
			#pragma GCS_UNROLL_AND_FLATTEN
			// Shift the values in the pipeline register
			for(int j=M;j>0;j--){ 
				tsc[j]=tsc[j-1];
			}
		}
		// Reads the number of chunks in the sequence database
		co_stream_read(stream_hmm_profile, &nb_chunck, sizeof(APPCOTYPE));

		/**
			Starting MSV computations
		**/
		// Read values from the stream
		for(i=0;i<nb_chunck;i++){ 
			
			// This loop does nothing useful in terms of computation, it 
			// is just there to store the sizes of the interleaved sequences 
			// currently handled by the architecture (here we have 4 interleaved 
			// sequences). It actual overhead is very small (<2%) for values of 
			// CHUNKSIZE > 64 .
			for(j=0;j<5;j++){
			#pragma CO PIPELINE
				co_stream_read(stream_seq, &curr_size[j], sizeof(APPCOTYPE));
			}
			
			// This loop runs for (CHUNKSIZE+1)*5 iterations, and have a rate of 1 iteration per 
			// cycle with a short stagedelay value thanks to the use of pipeline and interleaving.
			// NB : in the first run, before doing any usefull computation we have to initialize all 
			// values of mmx to M_INFTY, to do so we simply an EOS base in each interleaved stream
			// (EOS will then force mmx to M_INFTY as specified in FUNC_MSV)
			for(j=0;j<(CHUNKSIZE+1)*5;j++){
			#pragma CO PIPELINE

			// Read current base of interleaved sequence n�(j%5) ans save it within 
				// a scalar temporary variable.
				co_stream_read(stream_seq, &dsq, sizeof(APPCOTYPE));

				// In the following stages 1, 2, 3, 4 and 5 run in a pipeline fashion, 
				// We expressed the pipeline using reversed order of statements so 
				// as to avoid read after write dependency within the loop body 
				// (we could also have used additionnal variables such as mmx_0_next, 
				// etc.). 
				
				// Please also note that the pipeline boundaries are explicitely specified 
				// by using the co-par_break() statement.

				/******************************************************************
								X Computation Block (cut into log2(8)=3 stage pipeline)
				******************************************************************/
				
				// Pipeline stage 5 : in MSV, there is a few more computations to obtain 
				// the actual value used in the feed-back loop. We therefore slightly 
				// accomodated the number of interleaved instance, so as to hide this extra 
				// latency (in practice we should add even more cycles as computing xmb 
				// involves 2 add and 2 max).

				xmj = MAX2(xmj + tloop, xme + tej);
				xmc = MAX2(xmc + tloop, xme + tec);
				xmn = xmn + tloop;
				xmb = max2fn(xmn + tmove, xmj + tmove);

				/**	
				#pragma GCS_UNROLL_AND_FLATTEN
				for(p=0;p<log2(M);p++) {
					#pragma GCS_UNROLL_AND_FLATTEN
					// indices to check
					for(k=0;k<(m-(1<<p))/2;k++) {
						tmp_xme[p][k]= MAX2(tmp_xme[p-1][2*k],tmp_xme[p-1][2*k+1];
					}
					co_par_break(); 
				}
				**/
				// Pipeline stage 4 : we update xme with the final max tree result 
				// from stage 2. This latter result will then be used next iteration
				// to compute the new value of mmx. 
				xme = MAX2(tmp_xme_0_stage2, tmp_xme_1_stage2);
				co_par_break(); 
				
				// Pipeline stage 3 . we compute partial max tree stage from stage 1 
				// results.
				tmp_xme_0_stage2 = MAX2(tmp_xme_0_stage0, tmp_xme_1_stage0);
				tmp_xme_1_stage2 = MAX2(tmp_xme_2_stage0, tmp_xme_3_stage0);
				co_par_break(); 
				
				// Pipeline stage 2  : we start to compute the value of xme that is 
				// going to be used 3 iterations later from the mmx_score that were 
				// computed at previous iteration (this is why we have chosen to 
				// interleave 3+1 MSV problem instance so as to hide this latency).
				tmp_xme_0_stage0 = MAX2(tmp_mmx_0, tmp_mmx_1);
				tmp_xme_1_stage0 = MAX2(tmp_mmx_2, tmp_mmx_3);
				tmp_xme_2_stage0 = MAX2(tmp_mmx_4, tmp_mmx_5);
				tmp_xme_3_stage0 = MAX2(tmp_mmx_6, tmp_mmx_7);
				
				// As these tmp_mmx values will be overwritten in stage 1, we must 
				// save them in memory (here a circular buffer of depth = 5 equals to 
				// the number of interleaved instances) as they will be needed in 5 
				// iterations/cycles from now. 
				
				// As we are also reading this memory in stage 1, we must tell 
				// Impulse-C that we always read/write to different memory locations 
				// and that there is no recursive use of a memory cell value in the 
				// loop. We do so by using the "CO NONRECURSIVE" Pragma as defined in 
				// Impulse-C documentation.
				
				/**	
				#pragma GCS_UNROLL_AND_FLATTEN
				for(k=0;k<M) {
					mmx[k] [[(j-1)%5]= tmp_mmx[k];
				}
				**/
				// Memory update
				mmx_0[(j-1)%5] = tmp_mmx_0;
				mmx_1[(j-1)%5] = tmp_mmx_1;
				mmx_2[(j-1)%5] = tmp_mmx_2;
				mmx_3[(j-1)%5] = tmp_mmx_3;
				mmx_5[(j-1)%5] = tmp_mmx_4;
				mmx_5[(j-1)%5] = tmp_mmx_5;
				mmx_6[(j-1)%5] = tmp_mmx_6;
				mmx_7[(j-1)%5] = tmp_mmx_7;
				co_par_break(); 

				/******************************************************************
								M Computation Block (1 stage pipeline)
				******************************************************************/

				// Pipeline stage 1 : in this stage we simply compute the 
				// new values of the mmx vector from it previous value (as 
				// we are dealing with an 5-interlaved implemenation, we 
				// actually need the mmx vector that was computed 5 iterations 
				// ago, and which was saved in the mmx circular buffer
				/**	
				#pragma GCS_UNROLL_AND_FLATTEN
				for(k=0;k<M) {
					tmp_dsq[k] = msc[k];
				}
				**/
				msc_dsq_0 = msc_0[dsq];
				msc_dsq_1 = msc_1[dsq];
				msc_dsq_2 = msc_2[dsq];
				msc_dsq_3 = msc_3[dsq];
				msc_dsq_4 = msc_4[dsq];
				msc_dsq_5 = msc_5[dsq];
				msc_dsq_6 = msc_6[dsq];
				msc_dsq_7 = msc_7[dsq];

				/**	
				#pragma GCS_UNROLL_AND_FLATTEN
				for(k=0;k<M) {
					tmp_mmx[k] = FUNC_MSV(dsq,msc_dsq[k],mmx[k][j%5],xme);
				}
				**/
				tmp_mmx_0 = FUNC_MSV(dsq,msc_dsq_0,mmx_0[j%5],xme);
				tmp_mmx_1 = FUNC_MSV(dsq,msc_dsq_1,mmx_1[j%5],xme);
				tmp_mmx_2 = FUNC_MSV(dsq,msc_dsq_2,mmx_2[j%5],xme);
				tmp_mmx_3 = FUNC_MSV(dsq,msc_dsq_3,mmx_3[j%5],xme);
				tmp_mmx_4 = FUNC_MSV(dsq,msc_dsq_4,mmx_4[j%5],xme);
				tmp_mmx_5 = FUNC_MSV(dsq,msc_dsq_5,mmx_5[j%5],xme);
				tmp_mmx_6 = FUNC_MSV(dsq,msc_dsq_6,mmx_6[j%5],xme);
				tmp_mmx_7 = FUNC_MSV(dsq,msc_dsq_7,mmx_7[j%5],xme);
				co_par_break(); 
			}
		}
	}
	

}
