package fr.irisa.cairn.datapath.model.generator

import fr.irisa.cairn.model.fsm.FSM
import java.util.Iterator
import fr.irisa.cairn.model.fsm.State
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLStateTransition
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLCommandValue
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils

class StateMachineVHDLGenerator {
	
	
	public static def generate(FSM fsm) {
		'''
		library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all; 
use ieee.numeric_std.all; 
 
entity «fsm.getName()» is port (
	
	-- global signals 
	clk : in std_logic;
	rst : in std_logic;
	
	-- Input ports
	«FOR flag:fsm.getActivate() SEPARATOR ';'»
	«flag.getName()» : in std_logic;
	«ENDFOR»
	-- output ports
	«FOR op:fsm.getFlags() SEPARATOR ';'»
	«
	if (op.getWidth()==1) 
		op.getName() + ": out std_logic "
	else
	    op.getName() + ": out std_logic_vector("+(op.getWidth()-1)+"  downto 0)" 
	» : out std_logic;
	«ENDFOR»
 
  );
end «fsm.getName()» ;

architecture RTL of «fsm.getName()» is

	type fsm_«fsm.getName()»_type is («
   	FOR state : fsm.getStates() SEPARATOR ","»
   	«state.getLabel()»
   	«ENDFOR»
	);
   	
	signal NS,CS : fsm_«fsm.getName()»_type;

begin

	process(CS,«
	FOR flag : fsm.getActivate() SEPARATOR ","» «flag.getName()»«ENDFOR»)
	begin
	
«		
	
	FOR com : fsm.getFlags()»
		«
   		if (com.getWidth()!=1) {
 			'''«com.getName()» <= "«VHDLGenUtils.binaryValue(0,com.getWidth())»";''' 
 			
		} else {
			'''«com.getName()» <= '0'; '''
			
		}

 		»«ENDFOR»		
 			NS <= idle;

			case CS is
		
		
		«FOR state: fsm.getStates()»
		    -------------------------
			-- State «state.getLabel()»
			-------------------------
			when «state.getLabel()» =>
				-- State transitions
«VHDLStateTransition.create("\n").generate(state)»
				-- Output commands
«VHDLCommandValue.create("\n").generate(state)»
«ENDFOR
		
		» 
			when others =>
				NS <= «fsm.getStart().getLabel()»;
			end case;
	end process;

	process(clk,rst)
	begin
		if rising_edge(clk) then
			if rst='1' then
				CS <= «fsm.getStart().getLabel()»;
			else
				CS <= NS;
			end if;
		end if;
	end process;

end RTL;
		'''
	}
}