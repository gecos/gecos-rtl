package fr.irisa.cairn.datapath.model.generator;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLCommandValue;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLStateTransition;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.State;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class StateMachineVHDLGenerator {
  public static CharSequence generate(final FSM fsm) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t");
    _builder.append("library ieee; ");
    _builder.newLine();
    _builder.append("use ieee.std_logic_1164.all;");
    _builder.newLine();
    _builder.append("use ieee.numeric_std.all;");
    _builder.newLine();
    _builder.append("use ieee.std_logic_unsigned.all; ");
    _builder.newLine();
    _builder.append("use ieee.numeric_std.all; ");
    _builder.newLine();
    _builder.append(" ");
    _builder.newLine();
    _builder.append("entity ");
    String _name = fsm.getName();
    _builder.append(_name);
    _builder.append(" is port (");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-- global signals ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("clk : in std_logic;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("rst : in std_logic;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-- Input ports");
    _builder.newLine();
    {
      EList<InControlPort> _activate = fsm.getActivate();
      boolean _hasElements = false;
      for(final InControlPort flag : _activate) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(";", "\t");
        }
        _builder.append("\t");
        String _name_1 = flag.getName();
        _builder.append(_name_1, "\t");
        _builder.append(" : in std_logic;");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append("-- output ports");
    _builder.newLine();
    {
      EList<OutControlPort> _flags = fsm.getFlags();
      boolean _hasElements_1 = false;
      for(final OutControlPort op : _flags) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(";", "\t");
        }
        _builder.append("\t");
        String _xifexpression = null;
        int _width = op.getWidth();
        boolean _equals = (_width == 1);
        if (_equals) {
          String _name_2 = op.getName();
          _xifexpression = (_name_2 + ": out std_logic ");
        } else {
          String _name_3 = op.getName();
          String _plus = (_name_3 + ": out std_logic_vector(");
          int _width_1 = op.getWidth();
          int _minus = (_width_1 - 1);
          String _plus_1 = (_plus + Integer.valueOf(_minus));
          _xifexpression = (_plus_1 + "  downto 0)");
        }
        _builder.append(_xifexpression, "\t");
        _builder.append(" : out std_logic;");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append(" ");
    _builder.newLine();
    _builder.append("  ");
    _builder.append(");");
    _builder.newLine();
    _builder.append("end ");
    String _name_4 = fsm.getName();
    _builder.append(_name_4);
    _builder.append(" ;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("architecture RTL of ");
    String _name_5 = fsm.getName();
    _builder.append(_name_5);
    _builder.append(" is");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("type fsm_");
    String _name_6 = fsm.getName();
    _builder.append(_name_6, "\t");
    _builder.append("_type is (");
    {
      EList<State> _states = fsm.getStates();
      boolean _hasElements_2 = false;
      for(final State state : _states) {
        if (!_hasElements_2) {
          _hasElements_2 = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        _builder.newLineIfNotEmpty();
        String _label = state.getLabel();
        _builder.append(_label);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    _builder.append(");");
    _builder.newLine();
    _builder.append("   \t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("signal NS,CS : fsm_");
    String _name_7 = fsm.getName();
    _builder.append(_name_7, "\t");
    _builder.append("_type;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("begin");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("process(CS,");
    {
      EList<InControlPort> _activate_1 = fsm.getActivate();
      boolean _hasElements_3 = false;
      for(final InControlPort flag_1 : _activate_1) {
        if (!_hasElements_3) {
          _hasElements_3 = true;
        } else {
          _builder.appendImmediate(",", "\t");
        }
        _builder.append(" ");
        String _name_8 = flag_1.getName();
        _builder.append(_name_8, "\t");
      }
    }
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    {
      EList<OutControlPort> _flags_1 = fsm.getFlags();
      for(final OutControlPort com : _flags_1) {
        CharSequence _xifexpression_1 = null;
        int _width_2 = com.getWidth();
        boolean _notEquals = (_width_2 != 1);
        if (_notEquals) {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _name_9 = com.getName();
          _builder_1.append(_name_9);
          _builder_1.append(" <= \"");
          String _binaryValue = VHDLGenUtils.binaryValue(0, com.getWidth());
          _builder_1.append(_binaryValue);
          _builder_1.append("\";");
          _xifexpression_1 = _builder_1;
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          String _name_10 = com.getName();
          _builder_2.append(_name_10);
          _builder_2.append(" <= \'0\'; ");
          _xifexpression_1 = _builder_2;
        }
        _builder.append(_xifexpression_1);
      }
    }
    _builder.append("\t\t");
    _builder.newLineIfNotEmpty();
    _builder.append(" \t\t\t");
    _builder.append("NS <= idle;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("case CS is");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    {
      EList<State> _states_1 = fsm.getStates();
      for(final State state_1 : _states_1) {
        _builder.append("\t\t    ");
        _builder.append("-------------------------");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("-- State ");
        String _label_1 = state_1.getLabel();
        _builder.append(_label_1, "\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("-------------------------");
        _builder.newLine();
        _builder.append("\t\t");
        _builder.append("when ");
        String _label_2 = state_1.getLabel();
        _builder.append(_label_2, "\t\t");
        _builder.append(" =>");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("-- State transitions");
        _builder.newLine();
        String _generate = VHDLStateTransition.create("\n").generate(state_1);
        _builder.append(_generate);
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.append("\t");
        _builder.append("-- Output commands");
        _builder.newLine();
        String _generate_1 = VHDLCommandValue.create("\n").generate(state_1);
        _builder.append(_generate_1);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t\t");
    _builder.append("when others =>");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("NS <= ");
    String _label_3 = fsm.getStart().getLabel();
    _builder.append(_label_3, "\t\t\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("end case;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("end process;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("process(clk,rst)");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("if rising_edge(clk) then");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if rst=\'1\' then");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("CS <= ");
    String _label_4 = fsm.getStart().getLabel();
    _builder.append(_label_4, "\t\t\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("CS <= NS;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("end if;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end if;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("end process;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("end RTL;");
    _builder.newLine();
    return _builder;
  }
}
