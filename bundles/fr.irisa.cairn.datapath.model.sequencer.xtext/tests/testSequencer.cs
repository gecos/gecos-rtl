debug(1);


for name in FindFiles("../tests/src/",".seqdsl") do
	echo("target file is "+name);
	# xtext parser 
	sequencer = BuildSequencerModel(name);
	base = basename(stripext(name));
 
	
	# save as xmi 
	xmifile = "../tests/xmi/"+base+".sequencerdsl"; 
	SaveSequencerModel(sequencer,xmifile);

	# export FSM as dotty 
	optdotty = "../tests/dotty/"+base+"opt.dot"; 
	unoptdotty = "../tests/dotty/"+base+".dot" ;
	fsm = TransformSequencerModel(sequencer,optdotty,unoptdotty);

	#SaveD
	# export FSM as vhdl
 
# 	VerilogCodegenerator(fsm,"../tests/verilog/");
#	VHDLDatapathXPandGenerator(fsm,"../tests/vhdl/xpand/");
#	SystemCDatapathXPandGenerator(fsm,"../tests/systemc/xpand/");
#	VHDLDatapathExport(fsm,"../tests/vhdl/jet/");
done; 