/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.CallState#getFunction <em>Function</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getCallState()
 * @model
 * @generated
 */
public interface CallState extends AbstractState
{
  /**
   * Returns the value of the '<em><b>Function</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function</em>' reference.
   * @see #setFunction(SubSequencer)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getCallState_Function()
   * @model
   * @generated
   */
  SubSequencer getFunction();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CallState#getFunction <em>Function</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function</em>' reference.
   * @see #getFunction()
   * @generated
   */
  void setFunction(SubSequencer value);

} // CallState
