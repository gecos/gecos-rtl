package fr.irisa.cairn.datapath.model.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import fr.irisa.cairn.datapath.model.services.SequencerDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalSequencerDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_THREEVALUEDLOGIC", "RULE_COMPAREOPCODERULE", "RULE_HEXINT", "RULE_BININT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'sequencer'", "'is'", "'('", "';'", "')'", "'begin'", "'end'", "'procedure'", "':'", "'while '", "'{'", "'}'", "'repeat '", "'mode'", "'='", "'count'", "'enum'", "'if'", "'else'", "'switch'", "'default:'", "'case'", "'input'", "'int'", "'<'", "'>'", "'boolean'", "':='", "'output'", "'call'", "'nop'", "'set'", "','", "'goto'", "'when'", "'|'", "'&'", "'!'", "'['", "']'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int RULE_COMPAREOPCODERULE=7;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_THREEVALUEDLOGIC=6;
    public static final int RULE_STRING=10;
    public static final int RULE_SL_COMMENT=12;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=13;
    public static final int RULE_ANY_OTHER=14;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_BININT=9;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int RULE_HEXINT=8;

    // delegates
    // delegators


        public InternalSequencerDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSequencerDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSequencerDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */
     
     	private SequencerDSLGrammarAccess grammarAccess;
     	
        public InternalSequencerDSLParser(TokenStream input, SequencerDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Sequencer";	
       	}
       	
       	@Override
       	protected SequencerDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleSequencer"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:73:1: entryRuleSequencer returns [EObject current=null] : iv_ruleSequencer= ruleSequencer EOF ;
    public final EObject entryRuleSequencer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSequencer = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:74:2: (iv_ruleSequencer= ruleSequencer EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:75:2: iv_ruleSequencer= ruleSequencer EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSequencerRule()); 
            }
            pushFollow(FOLLOW_ruleSequencer_in_entryRuleSequencer81);
            iv_ruleSequencer=ruleSequencer();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSequencer; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSequencer91); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSequencer"


    // $ANTLR start "ruleSequencer"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:82:1: ruleSequencer returns [EObject current=null] : (otherlv_0= 'sequencer' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' otherlv_3= '(' ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) ) (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )* otherlv_9= ')' ( (lv_subTasks_10_0= ruleSubSequencer ) )? otherlv_11= 'begin' ( (lv_blocks_12_0= ruleBlock ) )+ otherlv_13= 'end' ) ;
    public final EObject ruleSequencer() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_in_4_0 = null;

        EObject lv_out_5_0 = null;

        EObject lv_in_7_0 = null;

        EObject lv_out_8_0 = null;

        EObject lv_subTasks_10_0 = null;

        EObject lv_blocks_12_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:85:28: ( (otherlv_0= 'sequencer' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' otherlv_3= '(' ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) ) (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )* otherlv_9= ')' ( (lv_subTasks_10_0= ruleSubSequencer ) )? otherlv_11= 'begin' ( (lv_blocks_12_0= ruleBlock ) )+ otherlv_13= 'end' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:86:1: (otherlv_0= 'sequencer' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' otherlv_3= '(' ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) ) (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )* otherlv_9= ')' ( (lv_subTasks_10_0= ruleSubSequencer ) )? otherlv_11= 'begin' ( (lv_blocks_12_0= ruleBlock ) )+ otherlv_13= 'end' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:86:1: (otherlv_0= 'sequencer' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' otherlv_3= '(' ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) ) (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )* otherlv_9= ')' ( (lv_subTasks_10_0= ruleSubSequencer ) )? otherlv_11= 'begin' ( (lv_blocks_12_0= ruleBlock ) )+ otherlv_13= 'end' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:86:3: otherlv_0= 'sequencer' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' otherlv_3= '(' ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) ) (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )* otherlv_9= ')' ( (lv_subTasks_10_0= ruleSubSequencer ) )? otherlv_11= 'begin' ( (lv_blocks_12_0= ruleBlock ) )+ otherlv_13= 'end'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleSequencer128); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSequencerAccess().getSequencerKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:90:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:91:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:91:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:92:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSequencer145); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getSequencerAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSequencerRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_16_in_ruleSequencer162); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getSequencerAccess().getIsKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,17,FOLLOW_17_in_ruleSequencer174); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSequencerAccess().getLeftParenthesisKeyword_3());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:116:1: ( ( (lv_in_4_0= ruleInputPortRule ) ) | ( (lv_out_5_0= ruleOutputPortRule ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==37) ) {
                alt1=1;
            }
            else if ( (LA1_0==43) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:116:2: ( (lv_in_4_0= ruleInputPortRule ) )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:116:2: ( (lv_in_4_0= ruleInputPortRule ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:117:1: (lv_in_4_0= ruleInputPortRule )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:117:1: (lv_in_4_0= ruleInputPortRule )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:118:3: lv_in_4_0= ruleInputPortRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSequencerAccess().getInInputPortRuleParserRuleCall_4_0_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleInputPortRule_in_ruleSequencer196);
                    lv_in_4_0=ruleInputPortRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSequencerRule());
                      	        }
                             		add(
                             			current, 
                             			"in",
                              		lv_in_4_0, 
                              		"InputPortRule");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:135:6: ( (lv_out_5_0= ruleOutputPortRule ) )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:135:6: ( (lv_out_5_0= ruleOutputPortRule ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:136:1: (lv_out_5_0= ruleOutputPortRule )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:136:1: (lv_out_5_0= ruleOutputPortRule )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:137:3: lv_out_5_0= ruleOutputPortRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSequencerAccess().getOutOutputPortRuleParserRuleCall_4_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOutputPortRule_in_ruleSequencer223);
                    lv_out_5_0=ruleOutputPortRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSequencerRule());
                      	        }
                             		add(
                             			current, 
                             			"out",
                              		lv_out_5_0, 
                              		"OutputPortRule");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:153:3: (otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==18) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:153:5: otherlv_6= ';' ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) )
            	    {
            	    otherlv_6=(Token)match(input,18,FOLLOW_18_in_ruleSequencer237); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_6, grammarAccess.getSequencerAccess().getSemicolonKeyword_5_0());
            	          
            	    }
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:157:1: ( ( (lv_in_7_0= ruleInputPortRule ) ) | ( (lv_out_8_0= ruleOutputPortRule ) ) )
            	    int alt2=2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0==37) ) {
            	        alt2=1;
            	    }
            	    else if ( (LA2_0==43) ) {
            	        alt2=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 2, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt2) {
            	        case 1 :
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:157:2: ( (lv_in_7_0= ruleInputPortRule ) )
            	            {
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:157:2: ( (lv_in_7_0= ruleInputPortRule ) )
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:158:1: (lv_in_7_0= ruleInputPortRule )
            	            {
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:158:1: (lv_in_7_0= ruleInputPortRule )
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:159:3: lv_in_7_0= ruleInputPortRule
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getSequencerAccess().getInInputPortRuleParserRuleCall_5_1_0_0()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleInputPortRule_in_ruleSequencer259);
            	            lv_in_7_0=ruleInputPortRule();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getSequencerRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"in",
            	                      		lv_in_7_0, 
            	                      		"InputPortRule");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:176:6: ( (lv_out_8_0= ruleOutputPortRule ) )
            	            {
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:176:6: ( (lv_out_8_0= ruleOutputPortRule ) )
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:177:1: (lv_out_8_0= ruleOutputPortRule )
            	            {
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:177:1: (lv_out_8_0= ruleOutputPortRule )
            	            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:178:3: lv_out_8_0= ruleOutputPortRule
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getSequencerAccess().getOutOutputPortRuleParserRuleCall_5_1_1_0()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleOutputPortRule_in_ruleSequencer286);
            	            lv_out_8_0=ruleOutputPortRule();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getSequencerRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"out",
            	                      		lv_out_8_0, 
            	                      		"OutputPortRule");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_9=(Token)match(input,19,FOLLOW_19_in_ruleSequencer301); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getSequencerAccess().getRightParenthesisKeyword_6());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:198:1: ( (lv_subTasks_10_0= ruleSubSequencer ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==22) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:199:1: (lv_subTasks_10_0= ruleSubSequencer )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:199:1: (lv_subTasks_10_0= ruleSubSequencer )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:200:3: lv_subTasks_10_0= ruleSubSequencer
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSequencerAccess().getSubTasksSubSequencerParserRuleCall_7_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleSubSequencer_in_ruleSequencer322);
                    lv_subTasks_10_0=ruleSubSequencer();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSequencerRule());
                      	        }
                             		add(
                             			current, 
                             			"subTasks",
                              		lv_subTasks_10_0, 
                              		"SubSequencer");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,20,FOLLOW_20_in_ruleSequencer335); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_11, grammarAccess.getSequencerAccess().getBeginKeyword_8());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:220:1: ( (lv_blocks_12_0= ruleBlock ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID||LA5_0==24||LA5_0==27||LA5_0==32||LA5_0==34||(LA5_0>=44 && LA5_0<=46)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:221:1: (lv_blocks_12_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:221:1: (lv_blocks_12_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:222:3: lv_blocks_12_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSequencerAccess().getBlocksBlockParserRuleCall_9_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleSequencer356);
            	    lv_blocks_12_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSequencerRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_12_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            otherlv_13=(Token)match(input,21,FOLLOW_21_in_ruleSequencer369); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_13, grammarAccess.getSequencerAccess().getEndKeyword_10());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSequencer"


    // $ANTLR start "entryRuleBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:250:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:251:2: (iv_ruleBlock= ruleBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:252:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_entryRuleBlock405);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlock415); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:259:1: ruleBlock returns [EObject current=null] : (this_SimpleBlock_0= ruleSimpleBlock | this_WhileBlock_1= ruleWhileBlock | this_RepeatBlock_2= ruleRepeatBlock | this_IfBlock_3= ruleIfBlock | this_SwitchBlock_4= ruleSwitchBlock ) ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleBlock_0 = null;

        EObject this_WhileBlock_1 = null;

        EObject this_RepeatBlock_2 = null;

        EObject this_IfBlock_3 = null;

        EObject this_SwitchBlock_4 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:262:28: ( (this_SimpleBlock_0= ruleSimpleBlock | this_WhileBlock_1= ruleWhileBlock | this_RepeatBlock_2= ruleRepeatBlock | this_IfBlock_3= ruleIfBlock | this_SwitchBlock_4= ruleSwitchBlock ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:263:1: (this_SimpleBlock_0= ruleSimpleBlock | this_WhileBlock_1= ruleWhileBlock | this_RepeatBlock_2= ruleRepeatBlock | this_IfBlock_3= ruleIfBlock | this_SwitchBlock_4= ruleSwitchBlock )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:263:1: (this_SimpleBlock_0= ruleSimpleBlock | this_WhileBlock_1= ruleWhileBlock | this_RepeatBlock_2= ruleRepeatBlock | this_IfBlock_3= ruleIfBlock | this_SwitchBlock_4= ruleSwitchBlock )
            int alt6=5;
            switch ( input.LA(1) ) {
            case RULE_ID:
            case 44:
            case 45:
            case 46:
                {
                alt6=1;
                }
                break;
            case 24:
                {
                alt6=2;
                }
                break;
            case 27:
                {
                alt6=3;
                }
                break;
            case 32:
                {
                alt6=4;
                }
                break;
            case 34:
                {
                alt6=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:264:2: this_SimpleBlock_0= ruleSimpleBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getSimpleBlockParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSimpleBlock_in_ruleBlock465);
                    this_SimpleBlock_0=ruleSimpleBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SimpleBlock_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:277:2: this_WhileBlock_1= ruleWhileBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getWhileBlockParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWhileBlock_in_ruleBlock495);
                    this_WhileBlock_1=ruleWhileBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WhileBlock_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:290:2: this_RepeatBlock_2= ruleRepeatBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getRepeatBlockParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRepeatBlock_in_ruleBlock525);
                    this_RepeatBlock_2=ruleRepeatBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RepeatBlock_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:303:2: this_IfBlock_3= ruleIfBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getIfBlockParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIfBlock_in_ruleBlock555);
                    this_IfBlock_3=ruleIfBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IfBlock_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:316:2: this_SwitchBlock_4= ruleSwitchBlock
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getSwitchBlockParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSwitchBlock_in_ruleBlock585);
                    this_SwitchBlock_4=ruleSwitchBlock();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SwitchBlock_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleSubSequencer"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:335:1: entryRuleSubSequencer returns [EObject current=null] : iv_ruleSubSequencer= ruleSubSequencer EOF ;
    public final EObject entryRuleSubSequencer() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSubSequencer = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:336:2: (iv_ruleSubSequencer= ruleSubSequencer EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:337:2: iv_ruleSubSequencer= ruleSubSequencer EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSubSequencerRule()); 
            }
            pushFollow(FOLLOW_ruleSubSequencer_in_entryRuleSubSequencer620);
            iv_ruleSubSequencer=ruleSubSequencer();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSubSequencer; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSubSequencer630); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSubSequencer"


    // $ANTLR start "ruleSubSequencer"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:344:1: ruleSubSequencer returns [EObject current=null] : (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= 'begin' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= 'end' ) ;
    public final EObject ruleSubSequencer() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_blocks_5_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:347:28: ( (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= 'begin' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= 'end' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:348:1: (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= 'begin' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= 'end' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:348:1: (otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= 'begin' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= 'end' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:348:3: otherlv_0= 'procedure' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' otherlv_3= ')' otherlv_4= 'begin' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= 'end'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleSubSequencer667); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSubSequencerAccess().getProcedureKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:352:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:353:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:353:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:354:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSubSequencer684); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getSubSequencerAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSubSequencerRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleSubSequencer701); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getSubSequencerAccess().getLeftParenthesisKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleSubSequencer713); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSubSequencerAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,20,FOLLOW_20_in_ruleSubSequencer725); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getSubSequencerAccess().getBeginKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:382:1: ( (lv_blocks_5_0= ruleBlock ) )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID||LA7_0==24||LA7_0==27||LA7_0==32||LA7_0==34||(LA7_0>=44 && LA7_0<=46)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:383:1: (lv_blocks_5_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:383:1: (lv_blocks_5_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:384:3: lv_blocks_5_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSubSequencerAccess().getBlocksBlockParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleSubSequencer746);
            	    lv_blocks_5_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSubSequencerRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_5_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            otherlv_6=(Token)match(input,21,FOLLOW_21_in_ruleSubSequencer759); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getSubSequencerAccess().getEndKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSubSequencer"


    // $ANTLR start "entryRuleSimpleBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:412:1: entryRuleSimpleBlock returns [EObject current=null] : iv_ruleSimpleBlock= ruleSimpleBlock EOF ;
    public final EObject entryRuleSimpleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:413:2: (iv_ruleSimpleBlock= ruleSimpleBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:414:2: iv_ruleSimpleBlock= ruleSimpleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleBlockRule()); 
            }
            pushFollow(FOLLOW_ruleSimpleBlock_in_entryRuleSimpleBlock795);
            iv_ruleSimpleBlock=ruleSimpleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleBlock805); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleBlock"


    // $ANTLR start "ruleSimpleBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:421:1: ruleSimpleBlock returns [EObject current=null] : ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )? ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' ) ) ;
    public final EObject ruleSimpleBlock() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_states_2_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:424:28: ( ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )? ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:1: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )? ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:1: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )? ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )? ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':'
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:425:3: ( (lv_name_0_0= RULE_ID ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:426:1: (lv_name_0_0= RULE_ID )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:426:1: (lv_name_0_0= RULE_ID )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:427:3: lv_name_0_0= RULE_ID
                    {
                    lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSimpleBlock848); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_name_0_0, grammarAccess.getSimpleBlockAccess().getNameIDTerminalRuleCall_0_0_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getSimpleBlockRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"name",
                              		lv_name_0_0, 
                              		"ID");
                      	    
                    }

                    }


                    }

                    otherlv_1=(Token)match(input,23,FOLLOW_23_in_ruleSimpleBlock865); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getSimpleBlockAccess().getColonKeyword_0_1());
                          
                    }

                    }
                    break;

            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:447:3: ( ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:447:4: ( (lv_states_2_0= ruleAbstractState ) ) otherlv_3= ';'
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:447:4: ( (lv_states_2_0= ruleAbstractState ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:448:1: (lv_states_2_0= ruleAbstractState )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:448:1: (lv_states_2_0= ruleAbstractState )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:449:3: lv_states_2_0= ruleAbstractState
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSimpleBlockAccess().getStatesAbstractStateParserRuleCall_1_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleAbstractState_in_ruleSimpleBlock889);
            lv_states_2_0=ruleAbstractState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSimpleBlockRule());
              	        }
                     		add(
                     			current, 
                     			"states",
                      		lv_states_2_0, 
                      		"AbstractState");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,18,FOLLOW_18_in_ruleSimpleBlock901); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSimpleBlockAccess().getSemicolonKeyword_1_1());
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleBlock"


    // $ANTLR start "entryRuleWhileBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:477:1: entryRuleWhileBlock returns [EObject current=null] : iv_ruleWhileBlock= ruleWhileBlock EOF ;
    public final EObject entryRuleWhileBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhileBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:478:2: (iv_ruleWhileBlock= ruleWhileBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:479:2: iv_ruleWhileBlock= ruleWhileBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhileBlockRule()); 
            }
            pushFollow(FOLLOW_ruleWhileBlock_in_entryRuleWhileBlock938);
            iv_ruleWhileBlock=ruleWhileBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhileBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhileBlock948); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhileBlock"


    // $ANTLR start "ruleWhileBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:486:1: ruleWhileBlock returns [EObject current=null] : (otherlv_0= 'while ' otherlv_1= '(' ( (lv_pred_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= '}' ) ;
    public final EObject ruleWhileBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_pred_2_0 = null;

        EObject lv_blocks_5_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:489:28: ( (otherlv_0= 'while ' otherlv_1= '(' ( (lv_pred_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= '}' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:490:1: (otherlv_0= 'while ' otherlv_1= '(' ( (lv_pred_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= '}' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:490:1: (otherlv_0= 'while ' otherlv_1= '(' ( (lv_pred_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= '}' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:490:3: otherlv_0= 'while ' otherlv_1= '(' ( (lv_pred_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_blocks_5_0= ruleBlock ) )+ otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleWhileBlock985); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWhileBlockAccess().getWhileKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleWhileBlock997); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getWhileBlockAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:498:1: ( (lv_pred_2_0= ruleOrExpression ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:499:1: (lv_pred_2_0= ruleOrExpression )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:499:1: (lv_pred_2_0= ruleOrExpression )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:500:3: lv_pred_2_0= ruleOrExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWhileBlockAccess().getPredOrExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleOrExpression_in_ruleWhileBlock1018);
            lv_pred_2_0=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWhileBlockRule());
              	        }
                     		set(
                     			current, 
                     			"pred",
                      		lv_pred_2_0, 
                      		"OrExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleWhileBlock1030); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getWhileBlockAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleWhileBlock1042); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getWhileBlockAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:524:1: ( (lv_blocks_5_0= ruleBlock ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID||LA9_0==24||LA9_0==27||LA9_0==32||LA9_0==34||(LA9_0>=44 && LA9_0<=46)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:525:1: (lv_blocks_5_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:525:1: (lv_blocks_5_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:526:3: lv_blocks_5_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getWhileBlockAccess().getBlocksBlockParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleWhileBlock1063);
            	    lv_blocks_5_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getWhileBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_5_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);

            otherlv_6=(Token)match(input,26,FOLLOW_26_in_ruleWhileBlock1076); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getWhileBlockAccess().getRightCurlyBracketKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhileBlock"


    // $ANTLR start "entryRuleRepeatBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:554:1: entryRuleRepeatBlock returns [EObject current=null] : iv_ruleRepeatBlock= ruleRepeatBlock EOF ;
    public final EObject entryRuleRepeatBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepeatBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:555:2: (iv_ruleRepeatBlock= ruleRepeatBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:556:2: iv_ruleRepeatBlock= ruleRepeatBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRepeatBlockRule()); 
            }
            pushFollow(FOLLOW_ruleRepeatBlock_in_entryRuleRepeatBlock1112);
            iv_ruleRepeatBlock=ruleRepeatBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRepeatBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatBlock1122); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepeatBlock"


    // $ANTLR start "ruleRepeatBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:563:1: ruleRepeatBlock returns [EObject current=null] : (otherlv_0= 'repeat ' (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )? ( (lv_iter_6_0= RULE_INT ) ) otherlv_7= '{' ( (lv_blocks_8_0= ruleBlock ) )+ otherlv_9= '}' ) ;
    public final EObject ruleRepeatBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_mode_4_1=null;
        Token lv_mode_4_2=null;
        Token otherlv_5=null;
        Token lv_iter_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_blocks_8_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:566:28: ( (otherlv_0= 'repeat ' (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )? ( (lv_iter_6_0= RULE_INT ) ) otherlv_7= '{' ( (lv_blocks_8_0= ruleBlock ) )+ otherlv_9= '}' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:567:1: (otherlv_0= 'repeat ' (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )? ( (lv_iter_6_0= RULE_INT ) ) otherlv_7= '{' ( (lv_blocks_8_0= ruleBlock ) )+ otherlv_9= '}' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:567:1: (otherlv_0= 'repeat ' (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )? ( (lv_iter_6_0= RULE_INT ) ) otherlv_7= '{' ( (lv_blocks_8_0= ruleBlock ) )+ otherlv_9= '}' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:567:3: otherlv_0= 'repeat ' (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )? ( (lv_iter_6_0= RULE_INT ) ) otherlv_7= '{' ( (lv_blocks_8_0= ruleBlock ) )+ otherlv_9= '}'
            {
            otherlv_0=(Token)match(input,27,FOLLOW_27_in_ruleRepeatBlock1159); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRepeatBlockAccess().getRepeatKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:571:1: (otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:571:3: otherlv_1= '(' otherlv_2= 'mode' otherlv_3= '=' ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) ) otherlv_5= ')'
                    {
                    otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleRepeatBlock1172); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getRepeatBlockAccess().getLeftParenthesisKeyword_1_0());
                          
                    }
                    otherlv_2=(Token)match(input,28,FOLLOW_28_in_ruleRepeatBlock1184); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getRepeatBlockAccess().getModeKeyword_1_1());
                          
                    }
                    otherlv_3=(Token)match(input,29,FOLLOW_29_in_ruleRepeatBlock1196); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getRepeatBlockAccess().getEqualsSignKeyword_1_2());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:583:1: ( ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:584:1: ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:584:1: ( (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:585:1: (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:585:1: (lv_mode_4_1= 'count' | lv_mode_4_2= 'enum' )
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0==30) ) {
                        alt10=1;
                    }
                    else if ( (LA10_0==31) ) {
                        alt10=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 0, input);

                        throw nvae;
                    }
                    switch (alt10) {
                        case 1 :
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:586:3: lv_mode_4_1= 'count'
                            {
                            lv_mode_4_1=(Token)match(input,30,FOLLOW_30_in_ruleRepeatBlock1216); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_mode_4_1, grammarAccess.getRepeatBlockAccess().getModeCountKeyword_1_3_0_0());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRepeatBlockRule());
                              	        }
                                     		setWithLastConsumed(current, "mode", lv_mode_4_1, null);
                              	    
                            }

                            }
                            break;
                        case 2 :
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:598:8: lv_mode_4_2= 'enum'
                            {
                            lv_mode_4_2=(Token)match(input,31,FOLLOW_31_in_ruleRepeatBlock1245); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                      newLeafNode(lv_mode_4_2, grammarAccess.getRepeatBlockAccess().getModeEnumKeyword_1_3_0_1());
                                  
                            }
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElement(grammarAccess.getRepeatBlockRule());
                              	        }
                                     		setWithLastConsumed(current, "mode", lv_mode_4_2, null);
                              	    
                            }

                            }
                            break;

                    }


                    }


                    }

                    otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleRepeatBlock1273); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_5, grammarAccess.getRepeatBlockAccess().getRightParenthesisKeyword_1_4());
                          
                    }

                    }
                    break;

            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:617:3: ( (lv_iter_6_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:618:1: (lv_iter_6_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:618:1: (lv_iter_6_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:619:3: lv_iter_6_0= RULE_INT
            {
            lv_iter_6_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleRepeatBlock1292); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_iter_6_0, grammarAccess.getRepeatBlockAccess().getIterINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getRepeatBlockRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"iter",
                      		lv_iter_6_0, 
                      		"INT");
              	    
            }

            }


            }

            otherlv_7=(Token)match(input,25,FOLLOW_25_in_ruleRepeatBlock1309); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_7, grammarAccess.getRepeatBlockAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:639:1: ( (lv_blocks_8_0= ruleBlock ) )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID||LA12_0==24||LA12_0==27||LA12_0==32||LA12_0==34||(LA12_0>=44 && LA12_0<=46)) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:640:1: (lv_blocks_8_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:640:1: (lv_blocks_8_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:641:3: lv_blocks_8_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getRepeatBlockAccess().getBlocksBlockParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleRepeatBlock1330);
            	    lv_blocks_8_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getRepeatBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_8_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);

            otherlv_9=(Token)match(input,26,FOLLOW_26_in_ruleRepeatBlock1343); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_9, grammarAccess.getRepeatBlockAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepeatBlock"


    // $ANTLR start "entryRuleIfBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:669:1: entryRuleIfBlock returns [EObject current=null] : iv_ruleIfBlock= ruleIfBlock EOF ;
    public final EObject entryRuleIfBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:670:2: (iv_ruleIfBlock= ruleIfBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:671:2: iv_ruleIfBlock= ruleIfBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfBlockRule()); 
            }
            pushFollow(FOLLOW_ruleIfBlock_in_entryRuleIfBlock1379);
            iv_ruleIfBlock=ruleIfBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfBlock1389); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfBlock"


    // $ANTLR start "ruleIfBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:678:1: ruleIfBlock returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_cond_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleBlock ) )+ otherlv_6= '}' (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )? ) ;
    public final EObject ruleIfBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        EObject lv_cond_2_0 = null;

        EObject lv_then_5_0 = null;

        EObject lv_else_9_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:681:28: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_cond_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleBlock ) )+ otherlv_6= '}' (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )? ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:682:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_cond_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleBlock ) )+ otherlv_6= '}' (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )? )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:682:1: (otherlv_0= 'if' otherlv_1= '(' ( (lv_cond_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleBlock ) )+ otherlv_6= '}' (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )? )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:682:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_cond_2_0= ruleOrExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_then_5_0= ruleBlock ) )+ otherlv_6= '}' (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )?
            {
            otherlv_0=(Token)match(input,32,FOLLOW_32_in_ruleIfBlock1426); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIfBlockAccess().getIfKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleIfBlock1438); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIfBlockAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:690:1: ( (lv_cond_2_0= ruleOrExpression ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:691:1: (lv_cond_2_0= ruleOrExpression )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:691:1: (lv_cond_2_0= ruleOrExpression )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:692:3: lv_cond_2_0= ruleOrExpression
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfBlockAccess().getCondOrExpressionParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleOrExpression_in_ruleIfBlock1459);
            lv_cond_2_0=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfBlockRule());
              	        }
                     		set(
                     			current, 
                     			"cond",
                      		lv_cond_2_0, 
                      		"OrExpression");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleIfBlock1471); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIfBlockAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleIfBlock1483); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIfBlockAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:716:1: ( (lv_then_5_0= ruleBlock ) )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID||LA13_0==24||LA13_0==27||LA13_0==32||LA13_0==34||(LA13_0>=44 && LA13_0<=46)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:717:1: (lv_then_5_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:717:1: (lv_then_5_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:718:3: lv_then_5_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getIfBlockAccess().getThenBlockParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleIfBlock1504);
            	    lv_then_5_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getIfBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"then",
            	              		lv_then_5_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);

            otherlv_6=(Token)match(input,26,FOLLOW_26_in_ruleIfBlock1517); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getIfBlockAccess().getRightCurlyBracketKeyword_6());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:738:1: (otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}' )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==33) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:738:3: otherlv_7= 'else' otherlv_8= '{' ( (lv_else_9_0= ruleBlock ) )+ otherlv_10= '}'
                    {
                    otherlv_7=(Token)match(input,33,FOLLOW_33_in_ruleIfBlock1530); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getIfBlockAccess().getElseKeyword_7_0());
                          
                    }
                    otherlv_8=(Token)match(input,25,FOLLOW_25_in_ruleIfBlock1542); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getIfBlockAccess().getLeftCurlyBracketKeyword_7_1());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:746:1: ( (lv_else_9_0= ruleBlock ) )+
                    int cnt14=0;
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==RULE_ID||LA14_0==24||LA14_0==27||LA14_0==32||LA14_0==34||(LA14_0>=44 && LA14_0<=46)) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:747:1: (lv_else_9_0= ruleBlock )
                    	    {
                    	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:747:1: (lv_else_9_0= ruleBlock )
                    	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:748:3: lv_else_9_0= ruleBlock
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getIfBlockAccess().getElseBlockParserRuleCall_7_2_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleBlock_in_ruleIfBlock1563);
                    	    lv_else_9_0=ruleBlock();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getIfBlockRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"else",
                    	              		lv_else_9_0, 
                    	              		"Block");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt14 >= 1 ) break loop14;
                    	    if (state.backtracking>0) {state.failed=true; return current;}
                                EarlyExitException eee =
                                    new EarlyExitException(14, input);
                                throw eee;
                        }
                        cnt14++;
                    } while (true);

                    otherlv_10=(Token)match(input,26,FOLLOW_26_in_ruleIfBlock1576); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_10, grammarAccess.getIfBlockAccess().getRightCurlyBracketKeyword_7_3());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfBlock"


    // $ANTLR start "entryRuleSwitchBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:776:1: entryRuleSwitchBlock returns [EObject current=null] : iv_ruleSwitchBlock= ruleSwitchBlock EOF ;
    public final EObject entryRuleSwitchBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSwitchBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:777:2: (iv_ruleSwitchBlock= ruleSwitchBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:778:2: iv_ruleSwitchBlock= ruleSwitchBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSwitchBlockRule()); 
            }
            pushFollow(FOLLOW_ruleSwitchBlock_in_entryRuleSwitchBlock1614);
            iv_ruleSwitchBlock=ruleSwitchBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSwitchBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSwitchBlock1624); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSwitchBlock"


    // $ANTLR start "ruleSwitchBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:785:1: ruleSwitchBlock returns [EObject current=null] : (otherlv_0= 'switch' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCaseBlock ) )+ otherlv_6= 'default:' ( (lv_default_7_0= ruleBlock ) )+ otherlv_8= '}' ) ;
    public final EObject ruleSwitchBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_cases_5_0 = null;

        EObject lv_default_7_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:788:28: ( (otherlv_0= 'switch' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCaseBlock ) )+ otherlv_6= 'default:' ( (lv_default_7_0= ruleBlock ) )+ otherlv_8= '}' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:789:1: (otherlv_0= 'switch' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCaseBlock ) )+ otherlv_6= 'default:' ( (lv_default_7_0= ruleBlock ) )+ otherlv_8= '}' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:789:1: (otherlv_0= 'switch' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCaseBlock ) )+ otherlv_6= 'default:' ( (lv_default_7_0= ruleBlock ) )+ otherlv_8= '}' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:789:3: otherlv_0= 'switch' otherlv_1= '(' ( (otherlv_2= RULE_ID ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_cases_5_0= ruleCaseBlock ) )+ otherlv_6= 'default:' ( (lv_default_7_0= ruleBlock ) )+ otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_34_in_ruleSwitchBlock1661); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSwitchBlockAccess().getSwitchKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleSwitchBlock1673); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getSwitchBlockAccess().getLeftParenthesisKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:797:1: ( (otherlv_2= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:798:1: (otherlv_2= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:798:1: (otherlv_2= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:799:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getSwitchBlockRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleSwitchBlock1697); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getSwitchBlockAccess().getChoiceInControlPortCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_3=(Token)match(input,19,FOLLOW_19_in_ruleSwitchBlock1709); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getSwitchBlockAccess().getRightParenthesisKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleSwitchBlock1721); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getSwitchBlockAccess().getLeftCurlyBracketKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:821:1: ( (lv_cases_5_0= ruleCaseBlock ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==36) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:822:1: (lv_cases_5_0= ruleCaseBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:822:1: (lv_cases_5_0= ruleCaseBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:823:3: lv_cases_5_0= ruleCaseBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSwitchBlockAccess().getCasesCaseBlockParserRuleCall_5_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleCaseBlock_in_ruleSwitchBlock1742);
            	    lv_cases_5_0=ruleCaseBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSwitchBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"cases",
            	              		lv_cases_5_0, 
            	              		"CaseBlock");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);

            otherlv_6=(Token)match(input,35,FOLLOW_35_in_ruleSwitchBlock1755); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getSwitchBlockAccess().getDefaultKeyword_6());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:843:1: ( (lv_default_7_0= ruleBlock ) )+
            int cnt17=0;
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==RULE_ID||LA17_0==24||LA17_0==27||LA17_0==32||LA17_0==34||(LA17_0>=44 && LA17_0<=46)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:844:1: (lv_default_7_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:844:1: (lv_default_7_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:845:3: lv_default_7_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSwitchBlockAccess().getDefaultBlockParserRuleCall_7_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleSwitchBlock1776);
            	    lv_default_7_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSwitchBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"default",
            	              		lv_default_7_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt17 >= 1 ) break loop17;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(17, input);
                        throw eee;
                }
                cnt17++;
            } while (true);

            otherlv_8=(Token)match(input,26,FOLLOW_26_in_ruleSwitchBlock1789); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getSwitchBlockAccess().getRightCurlyBracketKeyword_8());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSwitchBlock"


    // $ANTLR start "entryRuleCaseBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:873:1: entryRuleCaseBlock returns [EObject current=null] : iv_ruleCaseBlock= ruleCaseBlock EOF ;
    public final EObject entryRuleCaseBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCaseBlock = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:874:2: (iv_ruleCaseBlock= ruleCaseBlock EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:875:2: iv_ruleCaseBlock= ruleCaseBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCaseBlockRule()); 
            }
            pushFollow(FOLLOW_ruleCaseBlock_in_entryRuleCaseBlock1825);
            iv_ruleCaseBlock=ruleCaseBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCaseBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCaseBlock1835); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCaseBlock"


    // $ANTLR start "ruleCaseBlock"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:882:1: ruleCaseBlock returns [EObject current=null] : (otherlv_0= 'case' ( (lv_value_1_0= RULE_INT ) ) otherlv_2= ':' ( (lv_blocks_3_0= ruleBlock ) )+ ) ;
    public final EObject ruleCaseBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_1_0=null;
        Token otherlv_2=null;
        EObject lv_blocks_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:885:28: ( (otherlv_0= 'case' ( (lv_value_1_0= RULE_INT ) ) otherlv_2= ':' ( (lv_blocks_3_0= ruleBlock ) )+ ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:886:1: (otherlv_0= 'case' ( (lv_value_1_0= RULE_INT ) ) otherlv_2= ':' ( (lv_blocks_3_0= ruleBlock ) )+ )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:886:1: (otherlv_0= 'case' ( (lv_value_1_0= RULE_INT ) ) otherlv_2= ':' ( (lv_blocks_3_0= ruleBlock ) )+ )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:886:3: otherlv_0= 'case' ( (lv_value_1_0= RULE_INT ) ) otherlv_2= ':' ( (lv_blocks_3_0= ruleBlock ) )+
            {
            otherlv_0=(Token)match(input,36,FOLLOW_36_in_ruleCaseBlock1872); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCaseBlockAccess().getCaseKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:890:1: ( (lv_value_1_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:891:1: (lv_value_1_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:891:1: (lv_value_1_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:892:3: lv_value_1_0= RULE_INT
            {
            lv_value_1_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleCaseBlock1889); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_1_0, grammarAccess.getCaseBlockAccess().getValueINTTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCaseBlockRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"INT");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleCaseBlock1906); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getCaseBlockAccess().getColonKeyword_2());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:912:1: ( (lv_blocks_3_0= ruleBlock ) )+
            int cnt18=0;
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==RULE_ID||LA18_0==24||LA18_0==27||LA18_0==32||LA18_0==34||(LA18_0>=44 && LA18_0<=46)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:913:1: (lv_blocks_3_0= ruleBlock )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:913:1: (lv_blocks_3_0= ruleBlock )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:914:3: lv_blocks_3_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCaseBlockAccess().getBlocksBlockParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleCaseBlock1927);
            	    lv_blocks_3_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCaseBlockRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_3_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt18 >= 1 ) break loop18;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(18, input);
                        throw eee;
                }
                cnt18++;
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCaseBlock"


    // $ANTLR start "entryRuleInputPortRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:938:1: entryRuleInputPortRule returns [EObject current=null] : iv_ruleInputPortRule= ruleInputPortRule EOF ;
    public final EObject entryRuleInputPortRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputPortRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:939:2: (iv_ruleInputPortRule= ruleInputPortRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:940:2: iv_ruleInputPortRule= ruleInputPortRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputPortRuleRule()); 
            }
            pushFollow(FOLLOW_ruleInputPortRule_in_entryRuleInputPortRule1964);
            iv_ruleInputPortRule=ruleInputPortRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputPortRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleInputPortRule1974); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputPortRule"


    // $ANTLR start "ruleInputPortRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:947:1: ruleInputPortRule returns [EObject current=null] : (this_IntegerInputRule_0= ruleIntegerInputRule | this_BooleanInputRule_1= ruleBooleanInputRule ) ;
    public final EObject ruleInputPortRule() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerInputRule_0 = null;

        EObject this_BooleanInputRule_1 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:950:28: ( (this_IntegerInputRule_0= ruleIntegerInputRule | this_BooleanInputRule_1= ruleBooleanInputRule ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:951:1: (this_IntegerInputRule_0= ruleIntegerInputRule | this_BooleanInputRule_1= ruleBooleanInputRule )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:951:1: (this_IntegerInputRule_0= ruleIntegerInputRule | this_BooleanInputRule_1= ruleBooleanInputRule )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==37) ) {
                int LA19_1 = input.LA(2);

                if ( (LA19_1==RULE_ID) ) {
                    int LA19_2 = input.LA(3);

                    if ( (LA19_2==23) ) {
                        int LA19_3 = input.LA(4);

                        if ( (LA19_3==38) ) {
                            alt19=1;
                        }
                        else if ( (LA19_3==41) ) {
                            alt19=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 19, 3, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 19, 2, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 19, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:952:2: this_IntegerInputRule_0= ruleIntegerInputRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInputPortRuleAccess().getIntegerInputRuleParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerInputRule_in_ruleInputPortRule2024);
                    this_IntegerInputRule_0=ruleIntegerInputRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerInputRule_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:965:2: this_BooleanInputRule_1= ruleBooleanInputRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getInputPortRuleAccess().getBooleanInputRuleParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBooleanInputRule_in_ruleInputPortRule2054);
                    this_BooleanInputRule_1=ruleBooleanInputRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanInputRule_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputPortRule"


    // $ANTLR start "entryRuleIntegerInputRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:984:1: entryRuleIntegerInputRule returns [EObject current=null] : iv_ruleIntegerInputRule= ruleIntegerInputRule EOF ;
    public final EObject entryRuleIntegerInputRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerInputRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:985:2: (iv_ruleIntegerInputRule= ruleIntegerInputRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:986:2: iv_ruleIntegerInputRule= ruleIntegerInputRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerInputRuleRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerInputRule_in_entryRuleIntegerInputRule2089);
            iv_ruleIntegerInputRule=ruleIntegerInputRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerInputRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerInputRule2099); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerInputRule"


    // $ANTLR start "ruleIntegerInputRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:993:1: ruleIntegerInputRule returns [EObject current=null] : (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' ) ;
    public final EObject ruleIntegerInputRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_width_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:996:28: ( (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:997:1: (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:997:1: (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:997:3: otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_37_in_ruleIntegerInputRule2136); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIntegerInputRuleAccess().getInputKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1001:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1002:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1002:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1003:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIntegerInputRule2153); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getIntegerInputRuleAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerInputRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleIntegerInputRule2170); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getIntegerInputRuleAccess().getColonKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,38,FOLLOW_38_in_ruleIntegerInputRule2182); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIntegerInputRuleAccess().getIntKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,39,FOLLOW_39_in_ruleIntegerInputRule2194); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIntegerInputRuleAccess().getLessThanSignKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1031:1: ( (lv_width_5_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1032:1: (lv_width_5_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1032:1: (lv_width_5_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1033:3: lv_width_5_0= RULE_INT
            {
            lv_width_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerInputRule2211); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_width_5_0, grammarAccess.getIntegerInputRuleAccess().getWidthINTTerminalRuleCall_5_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerInputRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"width",
                      		lv_width_5_0, 
                      		"INT");
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,40,FOLLOW_40_in_ruleIntegerInputRule2228); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getIntegerInputRuleAccess().getGreaterThanSignKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerInputRule"


    // $ANTLR start "entryRuleBooleanInputRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1061:1: entryRuleBooleanInputRule returns [EObject current=null] : iv_ruleBooleanInputRule= ruleBooleanInputRule EOF ;
    public final EObject entryRuleBooleanInputRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanInputRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1062:2: (iv_ruleBooleanInputRule= ruleBooleanInputRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1063:2: iv_ruleBooleanInputRule= ruleBooleanInputRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanInputRuleRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanInputRule_in_entryRuleBooleanInputRule2264);
            iv_ruleBooleanInputRule=ruleBooleanInputRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanInputRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanInputRule2274); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanInputRule"


    // $ANTLR start "ruleBooleanInputRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1070:1: ruleBooleanInputRule returns [EObject current=null] : (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' ) ;
    public final EObject ruleBooleanInputRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1073:28: ( (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1074:1: (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1074:1: (otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1074:3: otherlv_0= 'input' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_37_in_ruleBooleanInputRule2311); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getBooleanInputRuleAccess().getInputKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1078:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1079:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1079:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1080:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBooleanInputRule2328); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getBooleanInputRuleAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanInputRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleBooleanInputRule2345); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getBooleanInputRuleAccess().getColonKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,41,FOLLOW_41_in_ruleBooleanInputRule2357); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getBooleanInputRuleAccess().getBooleanKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanInputRule"


    // $ANTLR start "entryRuleOutputPortRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1112:1: entryRuleOutputPortRule returns [EObject current=null] : iv_ruleOutputPortRule= ruleOutputPortRule EOF ;
    public final EObject entryRuleOutputPortRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputPortRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1113:2: (iv_ruleOutputPortRule= ruleOutputPortRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1114:2: iv_ruleOutputPortRule= ruleOutputPortRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputPortRuleRule()); 
            }
            pushFollow(FOLLOW_ruleOutputPortRule_in_entryRuleOutputPortRule2393);
            iv_ruleOutputPortRule=ruleOutputPortRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputPortRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOutputPortRule2403); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputPortRule"


    // $ANTLR start "ruleOutputPortRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1121:1: ruleOutputPortRule returns [EObject current=null] : (this_IntegerOutputDef_0= ruleIntegerOutputDef | this_BooleanOutputDef_1= ruleBooleanOutputDef ) ;
    public final EObject ruleOutputPortRule() throws RecognitionException {
        EObject current = null;

        EObject this_IntegerOutputDef_0 = null;

        EObject this_BooleanOutputDef_1 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1124:28: ( (this_IntegerOutputDef_0= ruleIntegerOutputDef | this_BooleanOutputDef_1= ruleBooleanOutputDef ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1125:1: (this_IntegerOutputDef_0= ruleIntegerOutputDef | this_BooleanOutputDef_1= ruleBooleanOutputDef )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1125:1: (this_IntegerOutputDef_0= ruleIntegerOutputDef | this_BooleanOutputDef_1= ruleBooleanOutputDef )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==43) ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1==RULE_ID) ) {
                    int LA20_2 = input.LA(3);

                    if ( (LA20_2==23) ) {
                        int LA20_3 = input.LA(4);

                        if ( (LA20_3==38) ) {
                            alt20=1;
                        }
                        else if ( (LA20_3==41) ) {
                            alt20=2;
                        }
                        else {
                            if (state.backtracking>0) {state.failed=true; return current;}
                            NoViableAltException nvae =
                                new NoViableAltException("", 20, 3, input);

                            throw nvae;
                        }
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 2, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1126:2: this_IntegerOutputDef_0= ruleIntegerOutputDef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getOutputPortRuleAccess().getIntegerOutputDefParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerOutputDef_in_ruleOutputPortRule2453);
                    this_IntegerOutputDef_0=ruleIntegerOutputDef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerOutputDef_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1139:2: this_BooleanOutputDef_1= ruleBooleanOutputDef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getOutputPortRuleAccess().getBooleanOutputDefParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBooleanOutputDef_in_ruleOutputPortRule2483);
                    this_BooleanOutputDef_1=ruleBooleanOutputDef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanOutputDef_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputPortRule"


    // $ANTLR start "entryRuleIntegerOutputDef"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1158:1: entryRuleIntegerOutputDef returns [EObject current=null] : iv_ruleIntegerOutputDef= ruleIntegerOutputDef EOF ;
    public final EObject entryRuleIntegerOutputDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerOutputDef = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1159:2: (iv_ruleIntegerOutputDef= ruleIntegerOutputDef EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1160:2: iv_ruleIntegerOutputDef= ruleIntegerOutputDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerOutputDefRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerOutputDef_in_entryRuleIntegerOutputDef2518);
            iv_ruleIntegerOutputDef=ruleIntegerOutputDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerOutputDef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerOutputDef2528); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerOutputDef"


    // $ANTLR start "ruleIntegerOutputDef"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1167:1: ruleIntegerOutputDef returns [EObject current=null] : ( ( (lv_port_0_0= ruleIntegerOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_INT ) ) ) ;
    public final EObject ruleIntegerOutputDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_value_2_0=null;
        EObject lv_port_0_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1170:28: ( ( ( (lv_port_0_0= ruleIntegerOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_INT ) ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1171:1: ( ( (lv_port_0_0= ruleIntegerOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_INT ) ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1171:1: ( ( (lv_port_0_0= ruleIntegerOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_INT ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1171:2: ( (lv_port_0_0= ruleIntegerOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_INT ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1171:2: ( (lv_port_0_0= ruleIntegerOutputPort ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1172:1: (lv_port_0_0= ruleIntegerOutputPort )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1172:1: (lv_port_0_0= ruleIntegerOutputPort )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1173:3: lv_port_0_0= ruleIntegerOutputPort
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIntegerOutputDefAccess().getPortIntegerOutputPortParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIntegerOutputPort_in_ruleIntegerOutputDef2574);
            lv_port_0_0=ruleIntegerOutputPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIntegerOutputDefRule());
              	        }
                     		set(
                     			current, 
                     			"port",
                      		lv_port_0_0, 
                      		"IntegerOutputPort");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,42,FOLLOW_42_in_ruleIntegerOutputDef2586); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIntegerOutputDefAccess().getColonEqualsSignKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1193:1: ( (lv_value_2_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1194:1: (lv_value_2_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1194:1: (lv_value_2_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1195:3: lv_value_2_0= RULE_INT
            {
            lv_value_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerOutputDef2603); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_2_0, grammarAccess.getIntegerOutputDefAccess().getValueINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerOutputDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"INT");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerOutputDef"


    // $ANTLR start "entryRuleBooleanOutputDef"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1219:1: entryRuleBooleanOutputDef returns [EObject current=null] : iv_ruleBooleanOutputDef= ruleBooleanOutputDef EOF ;
    public final EObject entryRuleBooleanOutputDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanOutputDef = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1220:2: (iv_ruleBooleanOutputDef= ruleBooleanOutputDef EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1221:2: iv_ruleBooleanOutputDef= ruleBooleanOutputDef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanOutputDefRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanOutputDef_in_entryRuleBooleanOutputDef2644);
            iv_ruleBooleanOutputDef=ruleBooleanOutputDef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanOutputDef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanOutputDef2654); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanOutputDef"


    // $ANTLR start "ruleBooleanOutputDef"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1228:1: ruleBooleanOutputDef returns [EObject current=null] : ( ( (lv_port_0_0= ruleBooleanOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) ) ) ;
    public final EObject ruleBooleanOutputDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_value_2_0=null;
        EObject lv_port_0_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1231:28: ( ( ( (lv_port_0_0= ruleBooleanOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1232:1: ( ( (lv_port_0_0= ruleBooleanOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1232:1: ( ( (lv_port_0_0= ruleBooleanOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1232:2: ( (lv_port_0_0= ruleBooleanOutputPort ) ) otherlv_1= ':=' ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1232:2: ( (lv_port_0_0= ruleBooleanOutputPort ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1233:1: (lv_port_0_0= ruleBooleanOutputPort )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1233:1: (lv_port_0_0= ruleBooleanOutputPort )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1234:3: lv_port_0_0= ruleBooleanOutputPort
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getBooleanOutputDefAccess().getPortBooleanOutputPortParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBooleanOutputPort_in_ruleBooleanOutputDef2700);
            lv_port_0_0=ruleBooleanOutputPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getBooleanOutputDefRule());
              	        }
                     		set(
                     			current, 
                     			"port",
                      		lv_port_0_0, 
                      		"BooleanOutputPort");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,42,FOLLOW_42_in_ruleBooleanOutputDef2712); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getBooleanOutputDefAccess().getColonEqualsSignKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1254:1: ( (lv_value_2_0= RULE_THREEVALUEDLOGIC ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1255:1: (lv_value_2_0= RULE_THREEVALUEDLOGIC )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1255:1: (lv_value_2_0= RULE_THREEVALUEDLOGIC )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1256:3: lv_value_2_0= RULE_THREEVALUEDLOGIC
            {
            lv_value_2_0=(Token)match(input,RULE_THREEVALUEDLOGIC,FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleBooleanOutputDef2729); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_2_0, grammarAccess.getBooleanOutputDefAccess().getValueThreeValuedLogicTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanOutputDefRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"ThreeValuedLogic");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanOutputDef"


    // $ANTLR start "entryRuleIntegerOutputPort"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1280:1: entryRuleIntegerOutputPort returns [EObject current=null] : iv_ruleIntegerOutputPort= ruleIntegerOutputPort EOF ;
    public final EObject entryRuleIntegerOutputPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerOutputPort = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1281:2: (iv_ruleIntegerOutputPort= ruleIntegerOutputPort EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1282:2: iv_ruleIntegerOutputPort= ruleIntegerOutputPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerOutputPortRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerOutputPort_in_entryRuleIntegerOutputPort2770);
            iv_ruleIntegerOutputPort=ruleIntegerOutputPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerOutputPort; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerOutputPort2780); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerOutputPort"


    // $ANTLR start "ruleIntegerOutputPort"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1289:1: ruleIntegerOutputPort returns [EObject current=null] : (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' ) ;
    public final EObject ruleIntegerOutputPort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_width_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1292:28: ( (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1293:1: (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1293:1: (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1293:3: otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'int' otherlv_4= '<' ( (lv_width_5_0= RULE_INT ) ) otherlv_6= '>'
            {
            otherlv_0=(Token)match(input,43,FOLLOW_43_in_ruleIntegerOutputPort2817); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIntegerOutputPortAccess().getOutputKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1297:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1298:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1298:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1299:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIntegerOutputPort2834); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getIntegerOutputPortAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerOutputPortRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleIntegerOutputPort2851); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getIntegerOutputPortAccess().getColonKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,38,FOLLOW_38_in_ruleIntegerOutputPort2863); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIntegerOutputPortAccess().getIntKeyword_3());
                  
            }
            otherlv_4=(Token)match(input,39,FOLLOW_39_in_ruleIntegerOutputPort2875); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getIntegerOutputPortAccess().getLessThanSignKeyword_4());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1327:1: ( (lv_width_5_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1328:1: (lv_width_5_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1328:1: (lv_width_5_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1329:3: lv_width_5_0= RULE_INT
            {
            lv_width_5_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerOutputPort2892); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_width_5_0, grammarAccess.getIntegerOutputPortAccess().getWidthINTTerminalRuleCall_5_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerOutputPortRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"width",
                      		lv_width_5_0, 
                      		"INT");
              	    
            }

            }


            }

            otherlv_6=(Token)match(input,40,FOLLOW_40_in_ruleIntegerOutputPort2909); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_6, grammarAccess.getIntegerOutputPortAccess().getGreaterThanSignKeyword_6());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerOutputPort"


    // $ANTLR start "entryRuleBooleanOutputPort"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1357:1: entryRuleBooleanOutputPort returns [EObject current=null] : iv_ruleBooleanOutputPort= ruleBooleanOutputPort EOF ;
    public final EObject entryRuleBooleanOutputPort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanOutputPort = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1358:2: (iv_ruleBooleanOutputPort= ruleBooleanOutputPort EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1359:2: iv_ruleBooleanOutputPort= ruleBooleanOutputPort EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanOutputPortRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanOutputPort_in_entryRuleBooleanOutputPort2945);
            iv_ruleBooleanOutputPort=ruleBooleanOutputPort();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanOutputPort; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanOutputPort2955); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanOutputPort"


    // $ANTLR start "ruleBooleanOutputPort"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1366:1: ruleBooleanOutputPort returns [EObject current=null] : (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' ) ;
    public final EObject ruleBooleanOutputPort() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1369:28: ( (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1370:1: (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1370:1: (otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1370:3: otherlv_0= 'output' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ':' otherlv_3= 'boolean'
            {
            otherlv_0=(Token)match(input,43,FOLLOW_43_in_ruleBooleanOutputPort2992); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getBooleanOutputPortAccess().getOutputKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1374:1: ( (lv_name_1_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1375:1: (lv_name_1_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1375:1: (lv_name_1_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1376:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBooleanOutputPort3009); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getBooleanOutputPortAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanOutputPortRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleBooleanOutputPort3026); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getBooleanOutputPortAccess().getColonKeyword_2());
                  
            }
            otherlv_3=(Token)match(input,41,FOLLOW_41_in_ruleBooleanOutputPort3038); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getBooleanOutputPortAccess().getBooleanKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanOutputPort"


    // $ANTLR start "entryRuleAbstractState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1408:1: entryRuleAbstractState returns [EObject current=null] : iv_ruleAbstractState= ruleAbstractState EOF ;
    public final EObject entryRuleAbstractState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractState = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1409:2: (iv_ruleAbstractState= ruleAbstractState EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1410:2: iv_ruleAbstractState= ruleAbstractState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAbstractStateRule()); 
            }
            pushFollow(FOLLOW_ruleAbstractState_in_entryRuleAbstractState3074);
            iv_ruleAbstractState=ruleAbstractState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAbstractState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstractState3084); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractState"


    // $ANTLR start "ruleAbstractState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1417:1: ruleAbstractState returns [EObject current=null] : (this_CallState_0= ruleCallState | this_SimpleState_1= ruleSimpleState ) ;
    public final EObject ruleAbstractState() throws RecognitionException {
        EObject current = null;

        EObject this_CallState_0 = null;

        EObject this_SimpleState_1 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1420:28: ( (this_CallState_0= ruleCallState | this_SimpleState_1= ruleSimpleState ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1421:1: (this_CallState_0= ruleCallState | this_SimpleState_1= ruleSimpleState )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1421:1: (this_CallState_0= ruleCallState | this_SimpleState_1= ruleSimpleState )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==44) ) {
                alt21=1;
            }
            else if ( ((LA21_0>=45 && LA21_0<=46)) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1422:2: this_CallState_0= ruleCallState
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractStateAccess().getCallStateParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleCallState_in_ruleAbstractState3134);
                    this_CallState_0=ruleCallState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_CallState_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1435:2: this_SimpleState_1= ruleSimpleState
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getAbstractStateAccess().getSimpleStateParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSimpleState_in_ruleAbstractState3164);
                    this_SimpleState_1=ruleSimpleState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SimpleState_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractState"


    // $ANTLR start "entryRuleSimpleState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1454:1: entryRuleSimpleState returns [EObject current=null] : iv_ruleSimpleState= ruleSimpleState EOF ;
    public final EObject entryRuleSimpleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleState = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1455:2: (iv_ruleSimpleState= ruleSimpleState EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1456:2: iv_ruleSimpleState= ruleSimpleState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSimpleStateRule()); 
            }
            pushFollow(FOLLOW_ruleSimpleState_in_entryRuleSimpleState3199);
            iv_ruleSimpleState=ruleSimpleState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSimpleState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSimpleState3209); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleState"


    // $ANTLR start "ruleSimpleState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1463:1: ruleSimpleState returns [EObject current=null] : (this_SetState_0= ruleSetState | this_NopState_1= ruleNopState ) ;
    public final EObject ruleSimpleState() throws RecognitionException {
        EObject current = null;

        EObject this_SetState_0 = null;

        EObject this_NopState_1 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1466:28: ( (this_SetState_0= ruleSetState | this_NopState_1= ruleNopState ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1467:1: (this_SetState_0= ruleSetState | this_NopState_1= ruleNopState )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1467:1: (this_SetState_0= ruleSetState | this_NopState_1= ruleNopState )
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==46) ) {
                alt22=1;
            }
            else if ( (LA22_0==45) ) {
                alt22=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }
            switch (alt22) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1468:2: this_SetState_0= ruleSetState
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getSimpleStateAccess().getSetStateParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSetState_in_ruleSimpleState3259);
                    this_SetState_0=ruleSetState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SetState_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1481:2: this_NopState_1= ruleNopState
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getSimpleStateAccess().getNopStateParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNopState_in_ruleSimpleState3289);
                    this_NopState_1=ruleNopState();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NopState_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleState"


    // $ANTLR start "entryRuleCallState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1500:1: entryRuleCallState returns [EObject current=null] : iv_ruleCallState= ruleCallState EOF ;
    public final EObject entryRuleCallState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallState = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1501:2: (iv_ruleCallState= ruleCallState EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1502:2: iv_ruleCallState= ruleCallState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallStateRule()); 
            }
            pushFollow(FOLLOW_ruleCallState_in_entryRuleCallState3324);
            iv_ruleCallState=ruleCallState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCallState3334); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallState"


    // $ANTLR start "ruleCallState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1509:1: ruleCallState returns [EObject current=null] : ( ( (lv_opcode_0_0= 'call' ) ) ( (otherlv_1= RULE_ID ) ) ) ;
    public final EObject ruleCallState() throws RecognitionException {
        EObject current = null;

        Token lv_opcode_0_0=null;
        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1512:28: ( ( ( (lv_opcode_0_0= 'call' ) ) ( (otherlv_1= RULE_ID ) ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1513:1: ( ( (lv_opcode_0_0= 'call' ) ) ( (otherlv_1= RULE_ID ) ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1513:1: ( ( (lv_opcode_0_0= 'call' ) ) ( (otherlv_1= RULE_ID ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1513:2: ( (lv_opcode_0_0= 'call' ) ) ( (otherlv_1= RULE_ID ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1513:2: ( (lv_opcode_0_0= 'call' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1514:1: (lv_opcode_0_0= 'call' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1514:1: (lv_opcode_0_0= 'call' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1515:3: lv_opcode_0_0= 'call'
            {
            lv_opcode_0_0=(Token)match(input,44,FOLLOW_44_in_ruleCallState3377); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_opcode_0_0, grammarAccess.getCallStateAccess().getOpcodeCallKeyword_0_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCallStateRule());
              	        }
                     		setWithLastConsumed(current, "opcode", lv_opcode_0_0, "call");
              	    
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1528:2: ( (otherlv_1= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1529:1: (otherlv_1= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1529:1: (otherlv_1= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1530:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getCallStateRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCallState3414); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getCallStateAccess().getFunctionSubSequencerCrossReference_1_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallState"


    // $ANTLR start "entryRuleNopState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1552:1: entryRuleNopState returns [EObject current=null] : iv_ruleNopState= ruleNopState EOF ;
    public final EObject entryRuleNopState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNopState = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1553:2: (iv_ruleNopState= ruleNopState EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1554:2: iv_ruleNopState= ruleNopState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNopStateRule()); 
            }
            pushFollow(FOLLOW_ruleNopState_in_entryRuleNopState3450);
            iv_ruleNopState=ruleNopState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNopState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNopState3460); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNopState"


    // $ANTLR start "ruleNopState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1561:1: ruleNopState returns [EObject current=null] : ( ( (lv_opcode_0_0= 'nop' ) ) ( (lv_jump_1_0= ruleJump ) )? ) ;
    public final EObject ruleNopState() throws RecognitionException {
        EObject current = null;

        Token lv_opcode_0_0=null;
        EObject lv_jump_1_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1564:28: ( ( ( (lv_opcode_0_0= 'nop' ) ) ( (lv_jump_1_0= ruleJump ) )? ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1565:1: ( ( (lv_opcode_0_0= 'nop' ) ) ( (lv_jump_1_0= ruleJump ) )? )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1565:1: ( ( (lv_opcode_0_0= 'nop' ) ) ( (lv_jump_1_0= ruleJump ) )? )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1565:2: ( (lv_opcode_0_0= 'nop' ) ) ( (lv_jump_1_0= ruleJump ) )?
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1565:2: ( (lv_opcode_0_0= 'nop' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1566:1: (lv_opcode_0_0= 'nop' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1566:1: (lv_opcode_0_0= 'nop' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1567:3: lv_opcode_0_0= 'nop'
            {
            lv_opcode_0_0=(Token)match(input,45,FOLLOW_45_in_ruleNopState3503); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_opcode_0_0, grammarAccess.getNopStateAccess().getOpcodeNopKeyword_0_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNopStateRule());
              	        }
                     		setWithLastConsumed(current, "opcode", lv_opcode_0_0, "nop");
              	    
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1580:2: ( (lv_jump_1_0= ruleJump ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==48) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1581:1: (lv_jump_1_0= ruleJump )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1581:1: (lv_jump_1_0= ruleJump )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1582:3: lv_jump_1_0= ruleJump
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getNopStateAccess().getJumpJumpParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJump_in_ruleNopState3537);
                    lv_jump_1_0=ruleJump();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getNopStateRule());
                      	        }
                             		set(
                             			current, 
                             			"jump",
                              		lv_jump_1_0, 
                              		"Jump");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNopState"


    // $ANTLR start "entryRuleSetState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1606:1: entryRuleSetState returns [EObject current=null] : iv_ruleSetState= ruleSetState EOF ;
    public final EObject entryRuleSetState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSetState = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1607:2: (iv_ruleSetState= ruleSetState EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1608:2: iv_ruleSetState= ruleSetState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSetStateRule()); 
            }
            pushFollow(FOLLOW_ruleSetState_in_entryRuleSetState3574);
            iv_ruleSetState=ruleSetState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSetState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSetState3584); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSetState"


    // $ANTLR start "ruleSetState"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1615:1: ruleSetState returns [EObject current=null] : ( ( (lv_opcode_0_0= 'set' ) ) ( (lv_commands_1_0= ruleOutControl ) ) (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )* ( (lv_jump_4_0= ruleJump ) )? ) ;
    public final EObject ruleSetState() throws RecognitionException {
        EObject current = null;

        Token lv_opcode_0_0=null;
        Token otherlv_2=null;
        EObject lv_commands_1_0 = null;

        EObject lv_commands_3_0 = null;

        EObject lv_jump_4_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1618:28: ( ( ( (lv_opcode_0_0= 'set' ) ) ( (lv_commands_1_0= ruleOutControl ) ) (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )* ( (lv_jump_4_0= ruleJump ) )? ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1619:1: ( ( (lv_opcode_0_0= 'set' ) ) ( (lv_commands_1_0= ruleOutControl ) ) (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )* ( (lv_jump_4_0= ruleJump ) )? )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1619:1: ( ( (lv_opcode_0_0= 'set' ) ) ( (lv_commands_1_0= ruleOutControl ) ) (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )* ( (lv_jump_4_0= ruleJump ) )? )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1619:2: ( (lv_opcode_0_0= 'set' ) ) ( (lv_commands_1_0= ruleOutControl ) ) (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )* ( (lv_jump_4_0= ruleJump ) )?
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1619:2: ( (lv_opcode_0_0= 'set' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1620:1: (lv_opcode_0_0= 'set' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1620:1: (lv_opcode_0_0= 'set' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1621:3: lv_opcode_0_0= 'set'
            {
            lv_opcode_0_0=(Token)match(input,46,FOLLOW_46_in_ruleSetState3627); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      newLeafNode(lv_opcode_0_0, grammarAccess.getSetStateAccess().getOpcodeSetKeyword_0_0());
                  
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSetStateRule());
              	        }
                     		setWithLastConsumed(current, "opcode", lv_opcode_0_0, "set");
              	    
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1634:2: ( (lv_commands_1_0= ruleOutControl ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1635:1: (lv_commands_1_0= ruleOutControl )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1635:1: (lv_commands_1_0= ruleOutControl )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1636:3: lv_commands_1_0= ruleOutControl
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSetStateAccess().getCommandsOutControlParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleOutControl_in_ruleSetState3661);
            lv_commands_1_0=ruleOutControl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSetStateRule());
              	        }
                     		add(
                     			current, 
                     			"commands",
                      		lv_commands_1_0, 
                      		"OutControl");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1652:2: (otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) ) )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==47) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1652:4: otherlv_2= ',' ( (lv_commands_3_0= ruleOutControl ) )
            	    {
            	    otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleSetState3674); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getSetStateAccess().getCommaKeyword_2_0());
            	          
            	    }
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1656:1: ( (lv_commands_3_0= ruleOutControl ) )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1657:1: (lv_commands_3_0= ruleOutControl )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1657:1: (lv_commands_3_0= ruleOutControl )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1658:3: lv_commands_3_0= ruleOutControl
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getSetStateAccess().getCommandsOutControlParserRuleCall_2_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleOutControl_in_ruleSetState3695);
            	    lv_commands_3_0=ruleOutControl();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getSetStateRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"commands",
            	              		lv_commands_3_0, 
            	              		"OutControl");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1674:4: ( (lv_jump_4_0= ruleJump ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==48) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1675:1: (lv_jump_4_0= ruleJump )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1675:1: (lv_jump_4_0= ruleJump )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1676:3: lv_jump_4_0= ruleJump
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSetStateAccess().getJumpJumpParserRuleCall_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleJump_in_ruleSetState3718);
                    lv_jump_4_0=ruleJump();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSetStateRule());
                      	        }
                             		set(
                             			current, 
                             			"jump",
                              		lv_jump_4_0, 
                              		"Jump");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSetState"


    // $ANTLR start "entryRuleJump"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1700:1: entryRuleJump returns [EObject current=null] : iv_ruleJump= ruleJump EOF ;
    public final EObject entryRuleJump() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJump = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1701:2: (iv_ruleJump= ruleJump EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1702:2: iv_ruleJump= ruleJump EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJumpRule()); 
            }
            pushFollow(FOLLOW_ruleJump_in_entryRuleJump3755);
            iv_ruleJump=ruleJump();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJump; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleJump3765); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJump"


    // $ANTLR start "ruleJump"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1709:1: ruleJump returns [EObject current=null] : (otherlv_0= 'goto' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )? ) ;
    public final EObject ruleJump() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_predicate_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1712:28: ( (otherlv_0= 'goto' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )? ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1713:1: (otherlv_0= 'goto' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )? )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1713:1: (otherlv_0= 'goto' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )? )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1713:3: otherlv_0= 'goto' ( (otherlv_1= RULE_ID ) ) (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )?
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleJump3802); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getJumpAccess().getGotoKeyword_0());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1717:1: ( (otherlv_1= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1718:1: (otherlv_1= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1718:1: (otherlv_1= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1719:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getJumpRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleJump3826); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getJumpAccess().getTargetSimpleBlockCrossReference_1_0()); 
              	
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1733:2: (otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==49) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1733:4: otherlv_2= 'when' ( (lv_predicate_3_0= ruleOrExpression ) )
                    {
                    otherlv_2=(Token)match(input,49,FOLLOW_49_in_ruleJump3839); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getJumpAccess().getWhenKeyword_2_0());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1737:1: ( (lv_predicate_3_0= ruleOrExpression ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1738:1: (lv_predicate_3_0= ruleOrExpression )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1738:1: (lv_predicate_3_0= ruleOrExpression )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1739:3: lv_predicate_3_0= ruleOrExpression
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getJumpAccess().getPredicateOrExpressionParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleOrExpression_in_ruleJump3860);
                    lv_predicate_3_0=ruleOrExpression();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getJumpRule());
                      	        }
                             		set(
                             			current, 
                             			"predicate",
                              		lv_predicate_3_0, 
                              		"OrExpression");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJump"


    // $ANTLR start "entryRuleOutControl"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1763:1: entryRuleOutControl returns [EObject current=null] : iv_ruleOutControl= ruleOutControl EOF ;
    public final EObject entryRuleOutControl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutControl = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1764:2: (iv_ruleOutControl= ruleOutControl EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1765:2: iv_ruleOutControl= ruleOutControl EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutControlRule()); 
            }
            pushFollow(FOLLOW_ruleOutControl_in_entryRuleOutControl3898);
            iv_ruleOutControl=ruleOutControl();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutControl; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOutControl3908); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutControl"


    // $ANTLR start "ruleOutControl"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1772:1: ruleOutControl returns [EObject current=null] : ( ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? ) | ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? ) ) ;
    public final EObject ruleOutControl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_value_3_0=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token lv_value_9_0=null;
        Token otherlv_10=null;
        EObject lv_predicate_5_0 = null;

        EObject lv_predicate_11_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1775:28: ( ( ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? ) | ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:1: ( ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? ) | ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:1: ( ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? ) | ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_ID) ) {
                int LA29_1 = input.LA(2);

                if ( (LA29_1==29) ) {
                    int LA29_2 = input.LA(3);

                    if ( (LA29_2==RULE_INT) ) {
                        alt29=2;
                    }
                    else if ( (LA29_2==RULE_THREEVALUEDLOGIC) ) {
                        alt29=1;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 29, 2, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 29, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:2: ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:2: ( () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )? )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:3: () ( (otherlv_1= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) ) (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )?
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1776:3: ()
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1777:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getOutControlAccess().getBooleanCommandValueAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1785:2: ( (otherlv_1= RULE_ID ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1786:1: (otherlv_1= RULE_ID )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1786:1: (otherlv_1= RULE_ID )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1787:3: otherlv_1= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getOutControlRule());
                      	        }
                              
                    }
                    otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOutControl3970); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_1, grammarAccess.getOutControlAccess().getCommandOutControlPortCrossReference_0_1_0()); 
                      	
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,29,FOLLOW_29_in_ruleOutControl3982); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getOutControlAccess().getEqualsSignKeyword_0_2());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1805:1: ( (lv_value_3_0= RULE_THREEVALUEDLOGIC ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1806:1: (lv_value_3_0= RULE_THREEVALUEDLOGIC )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1806:1: (lv_value_3_0= RULE_THREEVALUEDLOGIC )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1807:3: lv_value_3_0= RULE_THREEVALUEDLOGIC
                    {
                    lv_value_3_0=(Token)match(input,RULE_THREEVALUEDLOGIC,FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleOutControl3999); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_3_0, grammarAccess.getOutControlAccess().getValueThreeValuedLogicTerminalRuleCall_0_3_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getOutControlRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_3_0, 
                              		"ThreeValuedLogic");
                      	    
                    }

                    }


                    }

                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1823:2: (otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) ) )?
                    int alt27=2;
                    int LA27_0 = input.LA(1);

                    if ( (LA27_0==49) ) {
                        alt27=1;
                    }
                    switch (alt27) {
                        case 1 :
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1823:4: otherlv_4= 'when' ( (lv_predicate_5_0= ruleOrExpression ) )
                            {
                            otherlv_4=(Token)match(input,49,FOLLOW_49_in_ruleOutControl4017); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_4, grammarAccess.getOutControlAccess().getWhenKeyword_0_4_0());
                                  
                            }
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1827:1: ( (lv_predicate_5_0= ruleOrExpression ) )
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1828:1: (lv_predicate_5_0= ruleOrExpression )
                            {
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1828:1: (lv_predicate_5_0= ruleOrExpression )
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1829:3: lv_predicate_5_0= ruleOrExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getOutControlAccess().getPredicateOrExpressionParserRuleCall_0_4_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleOrExpression_in_ruleOutControl4038);
                            lv_predicate_5_0=ruleOrExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getOutControlRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"predicate",
                                      		lv_predicate_5_0, 
                                      		"OrExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1846:6: ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1846:6: ( () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )? )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1846:7: () ( (otherlv_7= RULE_ID ) ) otherlv_8= '=' ( (lv_value_9_0= RULE_INT ) ) (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )?
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1846:7: ()
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1847:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getOutControlAccess().getIntegerCommandValueAction_1_0(),
                                  current);
                          
                    }

                    }

                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1855:2: ( (otherlv_7= RULE_ID ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1856:1: (otherlv_7= RULE_ID )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1856:1: (otherlv_7= RULE_ID )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1857:3: otherlv_7= RULE_ID
                    {
                    if ( state.backtracking==0 ) {
                       
                      		  /* */ 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getOutControlRule());
                      	        }
                              
                    }
                    otherlv_7=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOutControl4084); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_7, grammarAccess.getOutControlAccess().getCommandOutControlPortCrossReference_1_1_0()); 
                      	
                    }

                    }


                    }

                    otherlv_8=(Token)match(input,29,FOLLOW_29_in_ruleOutControl4096); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_8, grammarAccess.getOutControlAccess().getEqualsSignKeyword_1_2());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1875:1: ( (lv_value_9_0= RULE_INT ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1876:1: (lv_value_9_0= RULE_INT )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1876:1: (lv_value_9_0= RULE_INT )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1877:3: lv_value_9_0= RULE_INT
                    {
                    lv_value_9_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleOutControl4113); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			newLeafNode(lv_value_9_0, grammarAccess.getOutControlAccess().getValueINTTerminalRuleCall_1_3_0()); 
                      		
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getOutControlRule());
                      	        }
                             		setWithLastConsumed(
                             			current, 
                             			"value",
                              		lv_value_9_0, 
                              		"INT");
                      	    
                    }

                    }


                    }

                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1893:2: (otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) ) )?
                    int alt28=2;
                    int LA28_0 = input.LA(1);

                    if ( (LA28_0==49) ) {
                        alt28=1;
                    }
                    switch (alt28) {
                        case 1 :
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1893:4: otherlv_10= 'when' ( (lv_predicate_11_0= ruleOrExpression ) )
                            {
                            otherlv_10=(Token)match(input,49,FOLLOW_49_in_ruleOutControl4131); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                                  	newLeafNode(otherlv_10, grammarAccess.getOutControlAccess().getWhenKeyword_1_4_0());
                                  
                            }
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1897:1: ( (lv_predicate_11_0= ruleOrExpression ) )
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1898:1: (lv_predicate_11_0= ruleOrExpression )
                            {
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1898:1: (lv_predicate_11_0= ruleOrExpression )
                            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1899:3: lv_predicate_11_0= ruleOrExpression
                            {
                            if ( state.backtracking==0 ) {
                               
                              	        newCompositeNode(grammarAccess.getOutControlAccess().getPredicateOrExpressionParserRuleCall_1_4_1_0()); 
                              	    
                            }
                            pushFollow(FOLLOW_ruleOrExpression_in_ruleOutControl4152);
                            lv_predicate_11_0=ruleOrExpression();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              	        if (current==null) {
                              	            current = createModelElementForParent(grammarAccess.getOutControlRule());
                              	        }
                                     		set(
                                     			current, 
                                     			"predicate",
                                      		lv_predicate_11_0, 
                                      		"OrExpression");
                              	        afterParserOrEnumRuleCall();
                              	    
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutControl"


    // $ANTLR start "entryRuleOrExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1923:1: entryRuleOrExpression returns [EObject current=null] : iv_ruleOrExpression= ruleOrExpression EOF ;
    public final EObject entryRuleOrExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrExpression = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1924:2: (iv_ruleOrExpression= ruleOrExpression EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1925:2: iv_ruleOrExpression= ruleOrExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleOrExpression_in_entryRuleOrExpression4191);
            iv_ruleOrExpression=ruleOrExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOrExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOrExpression4201); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrExpression"


    // $ANTLR start "ruleOrExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1932:1: ruleOrExpression returns [EObject current=null] : (this_AndExpression_0= ruleAndExpression ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )* ) ;
    public final EObject ruleOrExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_AndExpression_0 = null;

        EObject lv_terms_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1935:28: ( (this_AndExpression_0= ruleAndExpression ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )* ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1936:1: (this_AndExpression_0= ruleAndExpression ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )* )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1936:1: (this_AndExpression_0= ruleAndExpression ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )* )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1937:2: this_AndExpression_0= ruleAndExpression ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getOrExpressionAccess().getAndExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleAndExpression_in_ruleOrExpression4251);
            this_AndExpression_0=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_AndExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1948:1: ( () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==50) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1948:2: () (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1948:2: ()
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1949:2: 
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	  /* */ 
            	      	
            	    }
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndAdd(
            	                  grammarAccess.getOrExpressionAccess().getOrExpressionTermsAction_1_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1957:2: (otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) ) )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1957:4: otherlv_2= '|' ( (lv_terms_3_0= ruleAndExpression ) )
            	    {
            	    otherlv_2=(Token)match(input,50,FOLLOW_50_in_ruleOrExpression4276); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getOrExpressionAccess().getVerticalLineKeyword_1_1_0());
            	          
            	    }
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1961:1: ( (lv_terms_3_0= ruleAndExpression ) )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1962:1: (lv_terms_3_0= ruleAndExpression )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1962:1: (lv_terms_3_0= ruleAndExpression )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1963:3: lv_terms_3_0= ruleAndExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getOrExpressionAccess().getTermsAndExpressionParserRuleCall_1_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleAndExpression_in_ruleOrExpression4297);
            	    lv_terms_3_0=ruleAndExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getOrExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"terms",
            	              		lv_terms_3_0, 
            	              		"AndExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrExpression"


    // $ANTLR start "entryRuleAndExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1987:1: entryRuleAndExpression returns [EObject current=null] : iv_ruleAndExpression= ruleAndExpression EOF ;
    public final EObject entryRuleAndExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAndExpression = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1988:2: (iv_ruleAndExpression= ruleAndExpression EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1989:2: iv_ruleAndExpression= ruleAndExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleAndExpression_in_entryRuleAndExpression4336);
            iv_ruleAndExpression=ruleAndExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAndExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAndExpression4346); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAndExpression"


    // $ANTLR start "ruleAndExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1996:1: ruleAndExpression returns [EObject current=null] : (this_UnaryExpression_0= ruleUnaryExpression ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )* ) ;
    public final EObject ruleAndExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_UnaryExpression_0 = null;

        EObject lv_terms_3_0 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:1999:28: ( (this_UnaryExpression_0= ruleUnaryExpression ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )* ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2000:1: (this_UnaryExpression_0= ruleUnaryExpression ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )* )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2000:1: (this_UnaryExpression_0= ruleUnaryExpression ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )* )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2001:2: this_UnaryExpression_0= ruleUnaryExpression ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )*
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getAndExpressionAccess().getUnaryExpressionParserRuleCall_0()); 
                  
            }
            pushFollow(FOLLOW_ruleUnaryExpression_in_ruleAndExpression4396);
            this_UnaryExpression_0=ruleUnaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_UnaryExpression_0; 
                      afterParserOrEnumRuleCall();
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2012:1: ( () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==51) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2012:2: () (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2012:2: ()
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2013:2: 
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	  /* */ 
            	      	
            	    }
            	    if ( state.backtracking==0 ) {

            	              current = forceCreateModelElementAndAdd(
            	                  grammarAccess.getAndExpressionAccess().getAndExpressionTermsAction_1_0(),
            	                  current);
            	          
            	    }

            	    }

            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2021:2: (otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) ) )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2021:4: otherlv_2= '&' ( (lv_terms_3_0= ruleUnaryExpression ) )
            	    {
            	    otherlv_2=(Token)match(input,51,FOLLOW_51_in_ruleAndExpression4421); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_2, grammarAccess.getAndExpressionAccess().getAmpersandKeyword_1_1_0());
            	          
            	    }
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2025:1: ( (lv_terms_3_0= ruleUnaryExpression ) )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2026:1: (lv_terms_3_0= ruleUnaryExpression )
            	    {
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2026:1: (lv_terms_3_0= ruleUnaryExpression )
            	    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2027:3: lv_terms_3_0= ruleUnaryExpression
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getAndExpressionAccess().getTermsUnaryExpressionParserRuleCall_1_1_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleUnaryExpression_in_ruleAndExpression4442);
            	    lv_terms_3_0=ruleUnaryExpression();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getAndExpressionRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"terms",
            	              		lv_terms_3_0, 
            	              		"UnaryExpression");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAndExpression"


    // $ANTLR start "entryRuleUnaryExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2051:1: entryRuleUnaryExpression returns [EObject current=null] : iv_ruleUnaryExpression= ruleUnaryExpression EOF ;
    public final EObject entryRuleUnaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpression = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2052:2: (iv_ruleUnaryExpression= ruleUnaryExpression EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2053:2: iv_ruleUnaryExpression= ruleUnaryExpression EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUnaryExpressionRule()); 
            }
            pushFollow(FOLLOW_ruleUnaryExpression_in_entryRuleUnaryExpression4481);
            iv_ruleUnaryExpression=ruleUnaryExpression();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUnaryExpression; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleUnaryExpression4491); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpression"


    // $ANTLR start "ruleUnaryExpression"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2060:1: ruleUnaryExpression returns [EObject current=null] : ( ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) ) | this_PredicateTermRule_3= rulePredicateTermRule ) ;
    public final EObject ruleUnaryExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_term_2_0 = null;

        EObject this_PredicateTermRule_3 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2063:28: ( ( ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) ) | this_PredicateTermRule_3= rulePredicateTermRule ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:1: ( ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) ) | this_PredicateTermRule_3= rulePredicateTermRule )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:1: ( ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) ) | this_PredicateTermRule_3= rulePredicateTermRule )
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==52) ) {
                alt32=1;
            }
            else if ( (LA32_0==RULE_ID) ) {
                alt32=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }
            switch (alt32) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:2: ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:2: ( () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:3: () (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2064:3: ()
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2065:2: 
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {

                              current = forceCreateModelElement(
                                  grammarAccess.getUnaryExpressionAccess().getNegateExpressionAction_0_0(),
                                  current);
                          
                    }

                    }

                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2073:2: (otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2073:4: otherlv_1= '!' ( (lv_term_2_0= rulePredicateTermRule ) )
                    {
                    otherlv_1=(Token)match(input,52,FOLLOW_52_in_ruleUnaryExpression4542); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_1, grammarAccess.getUnaryExpressionAccess().getExclamationMarkKeyword_0_1_0());
                          
                    }
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2077:1: ( (lv_term_2_0= rulePredicateTermRule ) )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2078:1: (lv_term_2_0= rulePredicateTermRule )
                    {
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2078:1: (lv_term_2_0= rulePredicateTermRule )
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2079:3: lv_term_2_0= rulePredicateTermRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getUnaryExpressionAccess().getTermPredicateTermRuleParserRuleCall_0_1_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_rulePredicateTermRule_in_ruleUnaryExpression4563);
                    lv_term_2_0=rulePredicateTermRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
                      	        }
                             		set(
                             			current, 
                             			"term",
                              		lv_term_2_0, 
                              		"PredicateTermRule");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2097:2: this_PredicateTermRule_3= rulePredicateTermRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getUnaryExpressionAccess().getPredicateTermRuleParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_rulePredicateTermRule_in_ruleUnaryExpression4596);
                    this_PredicateTermRule_3=rulePredicateTermRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_PredicateTermRule_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpression"


    // $ANTLR start "entryRulePredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2118:1: entryRulePredicateTermRule returns [EObject current=null] : iv_rulePredicateTermRule= rulePredicateTermRule EOF ;
    public final EObject entryRulePredicateTermRule() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredicateTermRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2119:2: (iv_rulePredicateTermRule= rulePredicateTermRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2120:2: iv_rulePredicateTermRule= rulePredicateTermRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPredicateTermRuleRule()); 
            }
            pushFollow(FOLLOW_rulePredicateTermRule_in_entryRulePredicateTermRule4633);
            iv_rulePredicateTermRule=rulePredicateTermRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePredicateTermRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePredicateTermRule4643); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredicateTermRule"


    // $ANTLR start "rulePredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2127:1: rulePredicateTermRule returns [EObject current=null] : (this_IndexedBooleanPredicateTermRule_0= ruleIndexedBooleanPredicateTermRule | this_BooleanPredicateTermRule_1= ruleBooleanPredicateTermRule | this_IntegerPredicateTermRule_2= ruleIntegerPredicateTermRule ) ;
    public final EObject rulePredicateTermRule() throws RecognitionException {
        EObject current = null;

        EObject this_IndexedBooleanPredicateTermRule_0 = null;

        EObject this_BooleanPredicateTermRule_1 = null;

        EObject this_IntegerPredicateTermRule_2 = null;


         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2130:28: ( (this_IndexedBooleanPredicateTermRule_0= ruleIndexedBooleanPredicateTermRule | this_BooleanPredicateTermRule_1= ruleBooleanPredicateTermRule | this_IntegerPredicateTermRule_2= ruleIntegerPredicateTermRule ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2131:1: (this_IndexedBooleanPredicateTermRule_0= ruleIndexedBooleanPredicateTermRule | this_BooleanPredicateTermRule_1= ruleBooleanPredicateTermRule | this_IntegerPredicateTermRule_2= ruleIntegerPredicateTermRule )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2131:1: (this_IndexedBooleanPredicateTermRule_0= ruleIndexedBooleanPredicateTermRule | this_BooleanPredicateTermRule_1= ruleBooleanPredicateTermRule | this_IntegerPredicateTermRule_2= ruleIntegerPredicateTermRule )
            int alt33=3;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_ID) ) {
                switch ( input.LA(2) ) {
                case RULE_COMPAREOPCODERULE:
                    {
                    alt33=3;
                    }
                    break;
                case 53:
                    {
                    alt33=1;
                    }
                    break;
                case EOF:
                case 18:
                case 19:
                case 47:
                case 48:
                case 50:
                case 51:
                    {
                    alt33=2;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 33, 1, input);

                    throw nvae;
                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2132:2: this_IndexedBooleanPredicateTermRule_0= ruleIndexedBooleanPredicateTermRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getPredicateTermRuleAccess().getIndexedBooleanPredicateTermRuleParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIndexedBooleanPredicateTermRule_in_rulePredicateTermRule4693);
                    this_IndexedBooleanPredicateTermRule_0=ruleIndexedBooleanPredicateTermRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IndexedBooleanPredicateTermRule_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2145:2: this_BooleanPredicateTermRule_1= ruleBooleanPredicateTermRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getPredicateTermRuleAccess().getBooleanPredicateTermRuleParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBooleanPredicateTermRule_in_rulePredicateTermRule4723);
                    this_BooleanPredicateTermRule_1=ruleBooleanPredicateTermRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BooleanPredicateTermRule_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2158:2: this_IntegerPredicateTermRule_2= ruleIntegerPredicateTermRule
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getPredicateTermRuleAccess().getIntegerPredicateTermRuleParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIntegerPredicateTermRule_in_rulePredicateTermRule4753);
                    this_IntegerPredicateTermRule_2=ruleIntegerPredicateTermRule();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IntegerPredicateTermRule_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredicateTermRule"


    // $ANTLR start "entryRuleBooleanImmediate"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2179:1: entryRuleBooleanImmediate returns [EObject current=null] : iv_ruleBooleanImmediate= ruleBooleanImmediate EOF ;
    public final EObject entryRuleBooleanImmediate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanImmediate = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2180:2: (iv_ruleBooleanImmediate= ruleBooleanImmediate EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2181:2: iv_ruleBooleanImmediate= ruleBooleanImmediate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanImmediateRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanImmediate_in_entryRuleBooleanImmediate4790);
            iv_ruleBooleanImmediate=ruleBooleanImmediate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanImmediate; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanImmediate4800); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanImmediate"


    // $ANTLR start "ruleBooleanImmediate"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2188:1: ruleBooleanImmediate returns [EObject current=null] : ( (lv_value_0_0= RULE_THREEVALUEDLOGIC ) ) ;
    public final EObject ruleBooleanImmediate() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2191:28: ( ( (lv_value_0_0= RULE_THREEVALUEDLOGIC ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2192:1: ( (lv_value_0_0= RULE_THREEVALUEDLOGIC ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2192:1: ( (lv_value_0_0= RULE_THREEVALUEDLOGIC ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2193:1: (lv_value_0_0= RULE_THREEVALUEDLOGIC )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2193:1: (lv_value_0_0= RULE_THREEVALUEDLOGIC )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2194:3: lv_value_0_0= RULE_THREEVALUEDLOGIC
            {
            lv_value_0_0=(Token)match(input,RULE_THREEVALUEDLOGIC,FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleBooleanImmediate4841); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getBooleanImmediateAccess().getValueThreeValuedLogicTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanImmediateRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"ThreeValuedLogic");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanImmediate"


    // $ANTLR start "entryRuleIntegerImmediate"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2218:1: entryRuleIntegerImmediate returns [EObject current=null] : iv_ruleIntegerImmediate= ruleIntegerImmediate EOF ;
    public final EObject entryRuleIntegerImmediate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerImmediate = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2219:2: (iv_ruleIntegerImmediate= ruleIntegerImmediate EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2220:2: iv_ruleIntegerImmediate= ruleIntegerImmediate EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerImmediateRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerImmediate_in_entryRuleIntegerImmediate4881);
            iv_ruleIntegerImmediate=ruleIntegerImmediate();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerImmediate; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerImmediate4891); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerImmediate"


    // $ANTLR start "ruleIntegerImmediate"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2227:1: ruleIntegerImmediate returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntegerImmediate() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2230:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2231:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2231:1: ( (lv_value_0_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2232:1: (lv_value_0_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2232:1: (lv_value_0_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2233:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerImmediate4932); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getIntegerImmediateAccess().getValueINTTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerImmediateRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"INT");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerImmediate"


    // $ANTLR start "entryRuleIndexedBooleanPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2257:1: entryRuleIndexedBooleanPredicateTermRule returns [EObject current=null] : iv_ruleIndexedBooleanPredicateTermRule= ruleIndexedBooleanPredicateTermRule EOF ;
    public final EObject entryRuleIndexedBooleanPredicateTermRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIndexedBooleanPredicateTermRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2258:2: (iv_ruleIndexedBooleanPredicateTermRule= ruleIndexedBooleanPredicateTermRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2259:2: iv_ruleIndexedBooleanPredicateTermRule= ruleIndexedBooleanPredicateTermRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIndexedBooleanPredicateTermRuleRule()); 
            }
            pushFollow(FOLLOW_ruleIndexedBooleanPredicateTermRule_in_entryRuleIndexedBooleanPredicateTermRule4972);
            iv_ruleIndexedBooleanPredicateTermRule=ruleIndexedBooleanPredicateTermRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIndexedBooleanPredicateTermRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIndexedBooleanPredicateTermRule4982); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIndexedBooleanPredicateTermRule"


    // $ANTLR start "ruleIndexedBooleanPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2266:1: ruleIndexedBooleanPredicateTermRule returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_offset_2_0= RULE_INT ) ) otherlv_3= ']' ) ;
    public final EObject ruleIndexedBooleanPredicateTermRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_offset_2_0=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2269:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_offset_2_0= RULE_INT ) ) otherlv_3= ']' ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2270:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_offset_2_0= RULE_INT ) ) otherlv_3= ']' )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2270:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_offset_2_0= RULE_INT ) ) otherlv_3= ']' )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2270:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '[' ( (lv_offset_2_0= RULE_INT ) ) otherlv_3= ']'
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2270:2: ( (otherlv_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2271:1: (otherlv_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2271:1: (otherlv_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2272:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getIndexedBooleanPredicateTermRuleRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIndexedBooleanPredicateTermRule5031); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getIndexedBooleanPredicateTermRuleAccess().getFlagInControlPortCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,53,FOLLOW_53_in_ruleIndexedBooleanPredicateTermRule5043); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getIndexedBooleanPredicateTermRuleAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2290:1: ( (lv_offset_2_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2291:1: (lv_offset_2_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2291:1: (lv_offset_2_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2292:3: lv_offset_2_0= RULE_INT
            {
            lv_offset_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIndexedBooleanPredicateTermRule5060); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_offset_2_0, grammarAccess.getIndexedBooleanPredicateTermRuleAccess().getOffsetINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIndexedBooleanPredicateTermRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"offset",
                      		lv_offset_2_0, 
                      		"INT");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,54,FOLLOW_54_in_ruleIndexedBooleanPredicateTermRule5077); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getIndexedBooleanPredicateTermRuleAccess().getRightSquareBracketKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIndexedBooleanPredicateTermRule"


    // $ANTLR start "entryRuleBooleanPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2320:1: entryRuleBooleanPredicateTermRule returns [EObject current=null] : iv_ruleBooleanPredicateTermRule= ruleBooleanPredicateTermRule EOF ;
    public final EObject entryRuleBooleanPredicateTermRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanPredicateTermRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2321:2: (iv_ruleBooleanPredicateTermRule= ruleBooleanPredicateTermRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2322:2: iv_ruleBooleanPredicateTermRule= ruleBooleanPredicateTermRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBooleanPredicateTermRuleRule()); 
            }
            pushFollow(FOLLOW_ruleBooleanPredicateTermRule_in_entryRuleBooleanPredicateTermRule5113);
            iv_ruleBooleanPredicateTermRule=ruleBooleanPredicateTermRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBooleanPredicateTermRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBooleanPredicateTermRule5123); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanPredicateTermRule"


    // $ANTLR start "ruleBooleanPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2329:1: ruleBooleanPredicateTermRule returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleBooleanPredicateTermRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2332:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2333:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2333:1: ( (otherlv_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2334:1: (otherlv_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2334:1: (otherlv_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2335:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getBooleanPredicateTermRuleRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBooleanPredicateTermRule5171); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getBooleanPredicateTermRuleAccess().getFlagInControlPortCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanPredicateTermRule"


    // $ANTLR start "entryRuleIntegerPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2357:1: entryRuleIntegerPredicateTermRule returns [EObject current=null] : iv_ruleIntegerPredicateTermRule= ruleIntegerPredicateTermRule EOF ;
    public final EObject entryRuleIntegerPredicateTermRule() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerPredicateTermRule = null;


        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2358:2: (iv_ruleIntegerPredicateTermRule= ruleIntegerPredicateTermRule EOF )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2359:2: iv_ruleIntegerPredicateTermRule= ruleIntegerPredicateTermRule EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntegerPredicateTermRuleRule()); 
            }
            pushFollow(FOLLOW_ruleIntegerPredicateTermRule_in_entryRuleIntegerPredicateTermRule5206);
            iv_ruleIntegerPredicateTermRule=ruleIntegerPredicateTermRule();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntegerPredicateTermRule; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIntegerPredicateTermRule5216); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerPredicateTermRule"


    // $ANTLR start "ruleIntegerPredicateTermRule"
    // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2366:1: ruleIntegerPredicateTermRule returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) ) ( (lv_value_2_0= RULE_INT ) ) ) ;
    public final EObject ruleIntegerPredicateTermRule() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_opType_1_0=null;
        Token lv_value_2_0=null;

         enterRule(); 
            
        try {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2369:28: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) ) ( (lv_value_2_0= RULE_INT ) ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2370:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) ) ( (lv_value_2_0= RULE_INT ) ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2370:1: ( ( (otherlv_0= RULE_ID ) ) ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) ) ( (lv_value_2_0= RULE_INT ) ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2370:2: ( (otherlv_0= RULE_ID ) ) ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) ) ( (lv_value_2_0= RULE_INT ) )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2370:2: ( (otherlv_0= RULE_ID ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2371:1: (otherlv_0= RULE_ID )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2371:1: (otherlv_0= RULE_ID )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2372:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerPredicateTermRuleRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleIntegerPredicateTermRule5265); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getIntegerPredicateTermRuleAccess().getFlagInControlPortCrossReference_0_0()); 
              	
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2386:2: ( (lv_opType_1_0= RULE_COMPAREOPCODERULE ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2387:1: (lv_opType_1_0= RULE_COMPAREOPCODERULE )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2387:1: (lv_opType_1_0= RULE_COMPAREOPCODERULE )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2388:3: lv_opType_1_0= RULE_COMPAREOPCODERULE
            {
            lv_opType_1_0=(Token)match(input,RULE_COMPAREOPCODERULE,FOLLOW_RULE_COMPAREOPCODERULE_in_ruleIntegerPredicateTermRule5282); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_opType_1_0, grammarAccess.getIntegerPredicateTermRuleAccess().getOpTypeCompareOpcodeRuleTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerPredicateTermRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"opType",
                      		lv_opType_1_0, 
                      		"CompareOpcodeRule");
              	    
            }

            }


            }

            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2404:2: ( (lv_value_2_0= RULE_INT ) )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2405:1: (lv_value_2_0= RULE_INT )
            {
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2405:1: (lv_value_2_0= RULE_INT )
            // ../fr.irisa.cairn.datapath.model.sequencer.xtext/src-gen/fr/irisa/cairn/datapath/model/parser/antlr/internal/InternalSequencerDSL.g:2406:3: lv_value_2_0= RULE_INT
            {
            lv_value_2_0=(Token)match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleIntegerPredicateTermRule5304); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_2_0, grammarAccess.getIntegerPredicateTermRuleAccess().getValueINTTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getIntegerPredicateTermRuleRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_2_0, 
                      		"INT");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerPredicateTermRule"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleSequencer_in_entryRuleSequencer81 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSequencer91 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleSequencer128 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSequencer145 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleSequencer162 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleSequencer174 = new BitSet(new long[]{0x0000082000000000L});
    public static final BitSet FOLLOW_ruleInputPortRule_in_ruleSequencer196 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_ruleOutputPortRule_in_ruleSequencer223 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_18_in_ruleSequencer237 = new BitSet(new long[]{0x0000082000000000L});
    public static final BitSet FOLLOW_ruleInputPortRule_in_ruleSequencer259 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_ruleOutputPortRule_in_ruleSequencer286 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_19_in_ruleSequencer301 = new BitSet(new long[]{0x0000000000500000L});
    public static final BitSet FOLLOW_ruleSubSequencer_in_ruleSequencer322 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleSequencer335 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleSequencer356 = new BitSet(new long[]{0x0000700509200010L});
    public static final BitSet FOLLOW_21_in_ruleSequencer369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_entryRuleBlock405 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlock415 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleBlock_in_ruleBlock465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhileBlock_in_ruleBlock495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatBlock_in_ruleBlock525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfBlock_in_ruleBlock555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitchBlock_in_ruleBlock585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSubSequencer_in_entryRuleSubSequencer620 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSubSequencer630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleSubSequencer667 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSubSequencer684 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleSubSequencer701 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleSubSequencer713 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleSubSequencer725 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleSubSequencer746 = new BitSet(new long[]{0x0000700509200010L});
    public static final BitSet FOLLOW_21_in_ruleSubSequencer759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleBlock_in_entryRuleSimpleBlock795 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleBlock805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSimpleBlock848 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleSimpleBlock865 = new BitSet(new long[]{0x0000700000000010L});
    public static final BitSet FOLLOW_ruleAbstractState_in_ruleSimpleBlock889 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleSimpleBlock901 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhileBlock_in_entryRuleWhileBlock938 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhileBlock948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleWhileBlock985 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleWhileBlock997 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleWhileBlock1018 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleWhileBlock1030 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleWhileBlock1042 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleWhileBlock1063 = new BitSet(new long[]{0x000070050D000010L});
    public static final BitSet FOLLOW_26_in_ruleWhileBlock1076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatBlock_in_entryRuleRepeatBlock1112 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatBlock1122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_ruleRepeatBlock1159 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_17_in_ruleRepeatBlock1172 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleRepeatBlock1184 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleRepeatBlock1196 = new BitSet(new long[]{0x00000000C0000000L});
    public static final BitSet FOLLOW_30_in_ruleRepeatBlock1216 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_31_in_ruleRepeatBlock1245 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleRepeatBlock1273 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleRepeatBlock1292 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleRepeatBlock1309 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleRepeatBlock1330 = new BitSet(new long[]{0x000070050D000010L});
    public static final BitSet FOLLOW_26_in_ruleRepeatBlock1343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfBlock_in_entryRuleIfBlock1379 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfBlock1389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleIfBlock1426 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleIfBlock1438 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleIfBlock1459 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleIfBlock1471 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleIfBlock1483 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleIfBlock1504 = new BitSet(new long[]{0x000070050D000010L});
    public static final BitSet FOLLOW_26_in_ruleIfBlock1517 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_33_in_ruleIfBlock1530 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleIfBlock1542 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleIfBlock1563 = new BitSet(new long[]{0x000070050D000010L});
    public static final BitSet FOLLOW_26_in_ruleIfBlock1576 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSwitchBlock_in_entryRuleSwitchBlock1614 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSwitchBlock1624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_ruleSwitchBlock1661 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleSwitchBlock1673 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleSwitchBlock1697 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleSwitchBlock1709 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleSwitchBlock1721 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_ruleCaseBlock_in_ruleSwitchBlock1742 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_35_in_ruleSwitchBlock1755 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleSwitchBlock1776 = new BitSet(new long[]{0x000070050D000010L});
    public static final BitSet FOLLOW_26_in_ruleSwitchBlock1789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCaseBlock_in_entryRuleCaseBlock1825 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCaseBlock1835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleCaseBlock1872 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleCaseBlock1889 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleCaseBlock1906 = new BitSet(new long[]{0x0000700509000010L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleCaseBlock1927 = new BitSet(new long[]{0x0000700509000012L});
    public static final BitSet FOLLOW_ruleInputPortRule_in_entryRuleInputPortRule1964 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInputPortRule1974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerInputRule_in_ruleInputPortRule2024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanInputRule_in_ruleInputPortRule2054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerInputRule_in_entryRuleIntegerInputRule2089 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerInputRule2099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleIntegerInputRule2136 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIntegerInputRule2153 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleIntegerInputRule2170 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleIntegerInputRule2182 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_ruleIntegerInputRule2194 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerInputRule2211 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleIntegerInputRule2228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanInputRule_in_entryRuleBooleanInputRule2264 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanInputRule2274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_ruleBooleanInputRule2311 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBooleanInputRule2328 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleBooleanInputRule2345 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_ruleBooleanInputRule2357 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutputPortRule_in_entryRuleOutputPortRule2393 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOutputPortRule2403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOutputDef_in_ruleOutputPortRule2453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOutputDef_in_ruleOutputPortRule2483 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOutputDef_in_entryRuleIntegerOutputDef2518 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerOutputDef2528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOutputPort_in_ruleIntegerOutputDef2574 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ruleIntegerOutputDef2586 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerOutputDef2603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOutputDef_in_entryRuleBooleanOutputDef2644 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanOutputDef2654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOutputPort_in_ruleBooleanOutputDef2700 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ruleBooleanOutputDef2712 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleBooleanOutputDef2729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerOutputPort_in_entryRuleIntegerOutputPort2770 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerOutputPort2780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleIntegerOutputPort2817 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIntegerOutputPort2834 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleIntegerOutputPort2851 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleIntegerOutputPort2863 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_39_in_ruleIntegerOutputPort2875 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerOutputPort2892 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleIntegerOutputPort2909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanOutputPort_in_entryRuleBooleanOutputPort2945 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanOutputPort2955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleBooleanOutputPort2992 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBooleanOutputPort3009 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleBooleanOutputPort3026 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_41_in_ruleBooleanOutputPort3038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstractState_in_entryRuleAbstractState3074 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstractState3084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallState_in_ruleAbstractState3134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleState_in_ruleAbstractState3164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSimpleState_in_entryRuleSimpleState3199 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSimpleState3209 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetState_in_ruleSimpleState3259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNopState_in_ruleSimpleState3289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCallState_in_entryRuleCallState3324 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCallState3334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleCallState3377 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCallState3414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNopState_in_entryRuleNopState3450 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNopState3460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleNopState3503 = new BitSet(new long[]{0x0001000000000002L});
    public static final BitSet FOLLOW_ruleJump_in_ruleNopState3537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetState_in_entryRuleSetState3574 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSetState3584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleSetState3627 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleOutControl_in_ruleSetState3661 = new BitSet(new long[]{0x0001800000000002L});
    public static final BitSet FOLLOW_47_in_ruleSetState3674 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleOutControl_in_ruleSetState3695 = new BitSet(new long[]{0x0001800000000002L});
    public static final BitSet FOLLOW_ruleJump_in_ruleSetState3718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleJump_in_entryRuleJump3755 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleJump3765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleJump3802 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleJump3826 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_ruleJump3839 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleJump3860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOutControl_in_entryRuleOutControl3898 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOutControl3908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOutControl3970 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleOutControl3982 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleOutControl3999 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_ruleOutControl4017 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleOutControl4038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOutControl4084 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29_in_ruleOutControl4096 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleOutControl4113 = new BitSet(new long[]{0x0002000000000002L});
    public static final BitSet FOLLOW_49_in_ruleOutControl4131 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleOrExpression_in_ruleOutControl4152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOrExpression_in_entryRuleOrExpression4191 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOrExpression4201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleOrExpression4251 = new BitSet(new long[]{0x0004000000000002L});
    public static final BitSet FOLLOW_50_in_ruleOrExpression4276 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleAndExpression_in_ruleOrExpression4297 = new BitSet(new long[]{0x0004000000000002L});
    public static final BitSet FOLLOW_ruleAndExpression_in_entryRuleAndExpression4336 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAndExpression4346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleAndExpression4396 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_51_in_ruleAndExpression4421 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_ruleAndExpression4442 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_ruleUnaryExpression_in_entryRuleUnaryExpression4481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUnaryExpression4491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_ruleUnaryExpression4542 = new BitSet(new long[]{0x0010000000000010L});
    public static final BitSet FOLLOW_rulePredicateTermRule_in_ruleUnaryExpression4563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredicateTermRule_in_ruleUnaryExpression4596 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredicateTermRule_in_entryRulePredicateTermRule4633 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePredicateTermRule4643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIndexedBooleanPredicateTermRule_in_rulePredicateTermRule4693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanPredicateTermRule_in_rulePredicateTermRule4723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerPredicateTermRule_in_rulePredicateTermRule4753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanImmediate_in_entryRuleBooleanImmediate4790 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanImmediate4800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_THREEVALUEDLOGIC_in_ruleBooleanImmediate4841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerImmediate_in_entryRuleIntegerImmediate4881 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerImmediate4891 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerImmediate4932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIndexedBooleanPredicateTermRule_in_entryRuleIndexedBooleanPredicateTermRule4972 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIndexedBooleanPredicateTermRule4982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIndexedBooleanPredicateTermRule5031 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_53_in_ruleIndexedBooleanPredicateTermRule5043 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIndexedBooleanPredicateTermRule5060 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_54_in_ruleIndexedBooleanPredicateTermRule5077 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBooleanPredicateTermRule_in_entryRuleBooleanPredicateTermRule5113 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBooleanPredicateTermRule5123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBooleanPredicateTermRule5171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIntegerPredicateTermRule_in_entryRuleIntegerPredicateTermRule5206 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIntegerPredicateTermRule5216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleIntegerPredicateTermRule5265 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_RULE_COMPAREOPCODERULE_in_ruleIntegerPredicateTermRule5282 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerPredicateTermRule5304 = new BitSet(new long[]{0x0000000000000002L});

}