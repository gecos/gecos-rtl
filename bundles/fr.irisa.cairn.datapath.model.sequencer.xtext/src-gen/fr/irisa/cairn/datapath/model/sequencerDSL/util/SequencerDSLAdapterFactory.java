/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.util;

import fr.irisa.cairn.datapath.model.sequencerDSL.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage
 * @generated
 */
public class SequencerDSLAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SequencerDSLPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencerDSLAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = SequencerDSLPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SequencerDSLSwitch<Adapter> modelSwitch =
    new SequencerDSLSwitch<Adapter>()
    {
      @Override
      public Adapter caseSequencer(Sequencer object)
      {
        return createSequencerAdapter();
      }
      @Override
      public Adapter caseBlock(Block object)
      {
        return createBlockAdapter();
      }
      @Override
      public Adapter caseSubSequencer(SubSequencer object)
      {
        return createSubSequencerAdapter();
      }
      @Override
      public Adapter caseSimpleBlock(SimpleBlock object)
      {
        return createSimpleBlockAdapter();
      }
      @Override
      public Adapter caseWhileBlock(WhileBlock object)
      {
        return createWhileBlockAdapter();
      }
      @Override
      public Adapter caseRepeatBlock(RepeatBlock object)
      {
        return createRepeatBlockAdapter();
      }
      @Override
      public Adapter caseIfBlock(IfBlock object)
      {
        return createIfBlockAdapter();
      }
      @Override
      public Adapter caseSwitchBlock(SwitchBlock object)
      {
        return createSwitchBlockAdapter();
      }
      @Override
      public Adapter caseCaseBlock(CaseBlock object)
      {
        return createCaseBlockAdapter();
      }
      @Override
      public Adapter caseOutputPortRule(OutputPortRule object)
      {
        return createOutputPortRuleAdapter();
      }
      @Override
      public Adapter caseIntegerOutputDef(IntegerOutputDef object)
      {
        return createIntegerOutputDefAdapter();
      }
      @Override
      public Adapter caseBooleanOutputDef(BooleanOutputDef object)
      {
        return createBooleanOutputDefAdapter();
      }
      @Override
      public Adapter caseAbstractState(AbstractState object)
      {
        return createAbstractStateAdapter();
      }
      @Override
      public Adapter caseSimpleState(SimpleState object)
      {
        return createSimpleStateAdapter();
      }
      @Override
      public Adapter caseCallState(CallState object)
      {
        return createCallStateAdapter();
      }
      @Override
      public Adapter caseNopState(NopState object)
      {
        return createNopStateAdapter();
      }
      @Override
      public Adapter caseSetState(SetState object)
      {
        return createSetStateAdapter();
      }
      @Override
      public Adapter caseJump(Jump object)
      {
        return createJumpAdapter();
      }
      @Override
      public Adapter caseImmediateValue(ImmediateValue object)
      {
        return createImmediateValueAdapter();
      }
      @Override
      public Adapter caseBooleanImmediate(BooleanImmediate object)
      {
        return createBooleanImmediateAdapter();
      }
      @Override
      public Adapter caseIntegerImmediate(IntegerImmediate object)
      {
        return createIntegerImmediateAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer <em>Sequencer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer
   * @generated
   */
  public Adapter createSequencerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Block <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Block
   * @generated
   */
  public Adapter createBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer <em>Sub Sequencer</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer
   * @generated
   */
  public Adapter createSubSequencerAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock <em>Simple Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock
   * @generated
   */
  public Adapter createSimpleBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock <em>While Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock
   * @generated
   */
  public Adapter createWhileBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock <em>Repeat Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock
   * @generated
   */
  public Adapter createRepeatBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock <em>If Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock
   * @generated
   */
  public Adapter createIfBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock <em>Switch Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock
   * @generated
   */
  public Adapter createSwitchBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock <em>Case Block</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock
   * @generated
   */
  public Adapter createCaseBlockAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule <em>Output Port Rule</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule
   * @generated
   */
  public Adapter createOutputPortRuleAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef <em>Integer Output Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef
   * @generated
   */
  public Adapter createIntegerOutputDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef <em>Boolean Output Def</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef
   * @generated
   */
  public Adapter createBooleanOutputDefAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState <em>Abstract State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState
   * @generated
   */
  public Adapter createAbstractStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState <em>Simple State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState
   * @generated
   */
  public Adapter createSimpleStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CallState <em>Call State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CallState
   * @generated
   */
  public Adapter createCallStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.NopState <em>Nop State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.NopState
   * @generated
   */
  public Adapter createNopStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SetState <em>Set State</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SetState
   * @generated
   */
  public Adapter createSetStateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump <em>Jump</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Jump
   * @generated
   */
  public Adapter createJumpAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue <em>Immediate Value</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue
   * @generated
   */
  public Adapter createImmediateValueAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate <em>Boolean Immediate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate
   * @generated
   */
  public Adapter createBooleanImmediateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate <em>Integer Immediate</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate
   * @generated
   */
  public Adapter createIntegerImmediateAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //SequencerDSLAdapterFactory
