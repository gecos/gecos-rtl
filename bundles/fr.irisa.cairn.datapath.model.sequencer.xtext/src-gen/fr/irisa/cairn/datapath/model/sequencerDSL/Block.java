/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getBlock()
 * @model
 * @generated
 */
public interface Block extends EObject
{
} // Block
