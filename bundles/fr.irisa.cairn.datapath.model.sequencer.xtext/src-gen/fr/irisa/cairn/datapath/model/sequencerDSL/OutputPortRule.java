/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.datapath.port.OutControlPort;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Port Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getOutputPortRule()
 * @model
 * @generated
 */
public interface OutputPortRule extends EObject
{
  /**
   * Returns the value of the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' containment reference.
   * @see #setPort(OutControlPort)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getOutputPortRule_Port()
   * @model containment="true"
   * @generated
   */
  OutControlPort getPort();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule#getPort <em>Port</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' containment reference.
   * @see #getPort()
   * @generated
   */
  void setPort(OutControlPort value);

} // OutputPortRule
