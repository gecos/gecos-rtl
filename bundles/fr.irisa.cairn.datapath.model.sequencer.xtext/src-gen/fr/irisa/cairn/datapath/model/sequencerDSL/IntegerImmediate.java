/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Immediate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getIntegerImmediate()
 * @model
 * @generated
 */
public interface IntegerImmediate extends ImmediateValue
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getIntegerImmediate_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

} // IntegerImmediate
