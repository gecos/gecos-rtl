/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.datapath.port.InControlPort;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequencer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getSubTasks <em>Sub Tasks</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer()
 * @model
 * @generated
 */
public interface Sequencer extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>In</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.model.datapath.port.InControlPort}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>In</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>In</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer_In()
   * @model containment="true"
   * @generated
   */
  EList<InControlPort> getIn();

  /**
   * Returns the value of the '<em><b>Out</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Out</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Out</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer_Out()
   * @model containment="true"
   * @generated
   */
  EList<OutputPortRule> getOut();

  /**
   * Returns the value of the '<em><b>Sub Tasks</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub Tasks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Tasks</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer_SubTasks()
   * @model containment="true"
   * @generated
   */
  EList<SubSequencer> getSubTasks();

  /**
   * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.Block}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Blocks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Blocks</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSequencer_Blocks()
   * @model containment="true"
   * @generated
   */
  EList<Block> getBlocks();

} // Sequencer
