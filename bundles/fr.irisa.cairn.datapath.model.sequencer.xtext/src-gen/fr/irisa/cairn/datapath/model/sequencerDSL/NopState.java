/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Nop State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getNopState()
 * @model
 * @generated
 */
public interface NopState extends SimpleState
{
} // NopState
