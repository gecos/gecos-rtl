/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.Block;
import fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule;
import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;
import fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer;

import fr.irisa.cairn.model.datapath.port.InControlPort;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequencer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl#getSubTasks <em>Sub Tasks</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SequencerImpl extends MinimalEObjectImpl.Container implements Sequencer
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIn()
   * @generated
   * @ordered
   */
  protected EList<InControlPort> in;

  /**
   * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOut()
   * @generated
   * @ordered
   */
  protected EList<OutputPortRule> out;

  /**
   * The cached value of the '{@link #getSubTasks() <em>Sub Tasks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubTasks()
   * @generated
   * @ordered
   */
  protected EList<SubSequencer> subTasks;

  /**
   * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBlocks()
   * @generated
   * @ordered
   */
  protected EList<Block> blocks;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SequencerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SequencerDSLPackage.Literals.SEQUENCER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SequencerDSLPackage.SEQUENCER__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<InControlPort> getIn()
  {
    if (in == null)
    {
      in = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, SequencerDSLPackage.SEQUENCER__IN);
    }
    return in;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<OutputPortRule> getOut()
  {
    if (out == null)
    {
      out = new EObjectContainmentEList<OutputPortRule>(OutputPortRule.class, this, SequencerDSLPackage.SEQUENCER__OUT);
    }
    return out;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<SubSequencer> getSubTasks()
  {
    if (subTasks == null)
    {
      subTasks = new EObjectContainmentEList<SubSequencer>(SubSequencer.class, this, SequencerDSLPackage.SEQUENCER__SUB_TASKS);
    }
    return subTasks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Block> getBlocks()
  {
    if (blocks == null)
    {
      blocks = new EObjectContainmentEList<Block>(Block.class, this, SequencerDSLPackage.SEQUENCER__BLOCKS);
    }
    return blocks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SEQUENCER__IN:
        return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
      case SequencerDSLPackage.SEQUENCER__OUT:
        return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
      case SequencerDSLPackage.SEQUENCER__SUB_TASKS:
        return ((InternalEList<?>)getSubTasks()).basicRemove(otherEnd, msgs);
      case SequencerDSLPackage.SEQUENCER__BLOCKS:
        return ((InternalEList<?>)getBlocks()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SEQUENCER__NAME:
        return getName();
      case SequencerDSLPackage.SEQUENCER__IN:
        return getIn();
      case SequencerDSLPackage.SEQUENCER__OUT:
        return getOut();
      case SequencerDSLPackage.SEQUENCER__SUB_TASKS:
        return getSubTasks();
      case SequencerDSLPackage.SEQUENCER__BLOCKS:
        return getBlocks();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SEQUENCER__NAME:
        setName((String)newValue);
        return;
      case SequencerDSLPackage.SEQUENCER__IN:
        getIn().clear();
        getIn().addAll((Collection<? extends InControlPort>)newValue);
        return;
      case SequencerDSLPackage.SEQUENCER__OUT:
        getOut().clear();
        getOut().addAll((Collection<? extends OutputPortRule>)newValue);
        return;
      case SequencerDSLPackage.SEQUENCER__SUB_TASKS:
        getSubTasks().clear();
        getSubTasks().addAll((Collection<? extends SubSequencer>)newValue);
        return;
      case SequencerDSLPackage.SEQUENCER__BLOCKS:
        getBlocks().clear();
        getBlocks().addAll((Collection<? extends Block>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SEQUENCER__NAME:
        setName(NAME_EDEFAULT);
        return;
      case SequencerDSLPackage.SEQUENCER__IN:
        getIn().clear();
        return;
      case SequencerDSLPackage.SEQUENCER__OUT:
        getOut().clear();
        return;
      case SequencerDSLPackage.SEQUENCER__SUB_TASKS:
        getSubTasks().clear();
        return;
      case SequencerDSLPackage.SEQUENCER__BLOCKS:
        getBlocks().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SEQUENCER__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case SequencerDSLPackage.SEQUENCER__IN:
        return in != null && !in.isEmpty();
      case SequencerDSLPackage.SEQUENCER__OUT:
        return out != null && !out.isEmpty();
      case SequencerDSLPackage.SEQUENCER__SUB_TASKS:
        return subTasks != null && !subTasks.isEmpty();
      case SequencerDSLPackage.SEQUENCER__BLOCKS:
        return blocks != null && !blocks.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //SequencerImpl
