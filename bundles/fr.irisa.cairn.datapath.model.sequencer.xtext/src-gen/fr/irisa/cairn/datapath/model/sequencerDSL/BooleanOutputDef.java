/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.fsm.BooleanValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Output Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getBooleanOutputDef()
 * @model
 * @generated
 */
public interface BooleanOutputDef extends OutputPortRule
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * The literals are from the enumeration {@link fr.irisa.cairn.model.fsm.BooleanValue}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see fr.irisa.cairn.model.fsm.BooleanValue
   * @see #setValue(BooleanValue)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getBooleanOutputDef_Value()
   * @model
   * @generated
   */
  BooleanValue getValue();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see fr.irisa.cairn.model.fsm.BooleanValue
   * @see #getValue()
   * @generated
   */
  void setValue(BooleanValue value);

} // BooleanOutputDef
