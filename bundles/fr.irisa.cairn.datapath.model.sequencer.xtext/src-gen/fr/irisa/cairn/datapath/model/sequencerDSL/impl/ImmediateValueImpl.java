/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Immediate Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ImmediateValueImpl extends MinimalEObjectImpl.Container implements ImmediateValue
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImmediateValueImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SequencerDSLPackage.Literals.IMMEDIATE_VALUE;
  }

} //ImmediateValueImpl
