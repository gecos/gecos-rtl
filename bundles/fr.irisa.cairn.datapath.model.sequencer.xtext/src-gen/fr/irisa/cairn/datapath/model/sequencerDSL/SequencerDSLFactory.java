/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage
 * @generated
 */
public interface SequencerDSLFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SequencerDSLFactory eINSTANCE = fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Sequencer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sequencer</em>'.
   * @generated
   */
  Sequencer createSequencer();

  /**
   * Returns a new object of class '<em>Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Block</em>'.
   * @generated
   */
  Block createBlock();

  /**
   * Returns a new object of class '<em>Sub Sequencer</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Sequencer</em>'.
   * @generated
   */
  SubSequencer createSubSequencer();

  /**
   * Returns a new object of class '<em>Simple Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple Block</em>'.
   * @generated
   */
  SimpleBlock createSimpleBlock();

  /**
   * Returns a new object of class '<em>While Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>While Block</em>'.
   * @generated
   */
  WhileBlock createWhileBlock();

  /**
   * Returns a new object of class '<em>Repeat Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repeat Block</em>'.
   * @generated
   */
  RepeatBlock createRepeatBlock();

  /**
   * Returns a new object of class '<em>If Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>If Block</em>'.
   * @generated
   */
  IfBlock createIfBlock();

  /**
   * Returns a new object of class '<em>Switch Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Switch Block</em>'.
   * @generated
   */
  SwitchBlock createSwitchBlock();

  /**
   * Returns a new object of class '<em>Case Block</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Case Block</em>'.
   * @generated
   */
  CaseBlock createCaseBlock();

  /**
   * Returns a new object of class '<em>Output Port Rule</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Output Port Rule</em>'.
   * @generated
   */
  OutputPortRule createOutputPortRule();

  /**
   * Returns a new object of class '<em>Integer Output Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Output Def</em>'.
   * @generated
   */
  IntegerOutputDef createIntegerOutputDef();

  /**
   * Returns a new object of class '<em>Boolean Output Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Output Def</em>'.
   * @generated
   */
  BooleanOutputDef createBooleanOutputDef();

  /**
   * Returns a new object of class '<em>Abstract State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Abstract State</em>'.
   * @generated
   */
  AbstractState createAbstractState();

  /**
   * Returns a new object of class '<em>Simple State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Simple State</em>'.
   * @generated
   */
  SimpleState createSimpleState();

  /**
   * Returns a new object of class '<em>Call State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call State</em>'.
   * @generated
   */
  CallState createCallState();

  /**
   * Returns a new object of class '<em>Nop State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Nop State</em>'.
   * @generated
   */
  NopState createNopState();

  /**
   * Returns a new object of class '<em>Set State</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Set State</em>'.
   * @generated
   */
  SetState createSetState();

  /**
   * Returns a new object of class '<em>Jump</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Jump</em>'.
   * @generated
   */
  Jump createJump();

  /**
   * Returns a new object of class '<em>Immediate Value</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Immediate Value</em>'.
   * @generated
   */
  ImmediateValue createImmediateValue();

  /**
   * Returns a new object of class '<em>Boolean Immediate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Boolean Immediate</em>'.
   * @generated
   */
  BooleanImmediate createBooleanImmediate();

  /**
   * Returns a new object of class '<em>Integer Immediate</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Integer Immediate</em>'.
   * @generated
   */
  IntegerImmediate createIntegerImmediate();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  SequencerDSLPackage getSequencerDSLPackage();

} //SequencerDSLFactory
