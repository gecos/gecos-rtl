/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.util;

import fr.irisa.cairn.datapath.model.sequencerDSL.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage
 * @generated
 */
public class SequencerDSLSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SequencerDSLPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencerDSLSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = SequencerDSLPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case SequencerDSLPackage.SEQUENCER:
      {
        Sequencer sequencer = (Sequencer)theEObject;
        T result = caseSequencer(sequencer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.BLOCK:
      {
        Block block = (Block)theEObject;
        T result = caseBlock(block);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.SUB_SEQUENCER:
      {
        SubSequencer subSequencer = (SubSequencer)theEObject;
        T result = caseSubSequencer(subSequencer);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.SIMPLE_BLOCK:
      {
        SimpleBlock simpleBlock = (SimpleBlock)theEObject;
        T result = caseSimpleBlock(simpleBlock);
        if (result == null) result = caseBlock(simpleBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.WHILE_BLOCK:
      {
        WhileBlock whileBlock = (WhileBlock)theEObject;
        T result = caseWhileBlock(whileBlock);
        if (result == null) result = caseBlock(whileBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.REPEAT_BLOCK:
      {
        RepeatBlock repeatBlock = (RepeatBlock)theEObject;
        T result = caseRepeatBlock(repeatBlock);
        if (result == null) result = caseBlock(repeatBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.IF_BLOCK:
      {
        IfBlock ifBlock = (IfBlock)theEObject;
        T result = caseIfBlock(ifBlock);
        if (result == null) result = caseBlock(ifBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.SWITCH_BLOCK:
      {
        SwitchBlock switchBlock = (SwitchBlock)theEObject;
        T result = caseSwitchBlock(switchBlock);
        if (result == null) result = caseBlock(switchBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.CASE_BLOCK:
      {
        CaseBlock caseBlock = (CaseBlock)theEObject;
        T result = caseCaseBlock(caseBlock);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.OUTPUT_PORT_RULE:
      {
        OutputPortRule outputPortRule = (OutputPortRule)theEObject;
        T result = caseOutputPortRule(outputPortRule);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.INTEGER_OUTPUT_DEF:
      {
        IntegerOutputDef integerOutputDef = (IntegerOutputDef)theEObject;
        T result = caseIntegerOutputDef(integerOutputDef);
        if (result == null) result = caseOutputPortRule(integerOutputDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.BOOLEAN_OUTPUT_DEF:
      {
        BooleanOutputDef booleanOutputDef = (BooleanOutputDef)theEObject;
        T result = caseBooleanOutputDef(booleanOutputDef);
        if (result == null) result = caseOutputPortRule(booleanOutputDef);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.ABSTRACT_STATE:
      {
        AbstractState abstractState = (AbstractState)theEObject;
        T result = caseAbstractState(abstractState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.SIMPLE_STATE:
      {
        SimpleState simpleState = (SimpleState)theEObject;
        T result = caseSimpleState(simpleState);
        if (result == null) result = caseAbstractState(simpleState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.CALL_STATE:
      {
        CallState callState = (CallState)theEObject;
        T result = caseCallState(callState);
        if (result == null) result = caseAbstractState(callState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.NOP_STATE:
      {
        NopState nopState = (NopState)theEObject;
        T result = caseNopState(nopState);
        if (result == null) result = caseSimpleState(nopState);
        if (result == null) result = caseAbstractState(nopState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.SET_STATE:
      {
        SetState setState = (SetState)theEObject;
        T result = caseSetState(setState);
        if (result == null) result = caseSimpleState(setState);
        if (result == null) result = caseAbstractState(setState);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.JUMP:
      {
        Jump jump = (Jump)theEObject;
        T result = caseJump(jump);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.IMMEDIATE_VALUE:
      {
        ImmediateValue immediateValue = (ImmediateValue)theEObject;
        T result = caseImmediateValue(immediateValue);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.BOOLEAN_IMMEDIATE:
      {
        BooleanImmediate booleanImmediate = (BooleanImmediate)theEObject;
        T result = caseBooleanImmediate(booleanImmediate);
        if (result == null) result = caseImmediateValue(booleanImmediate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SequencerDSLPackage.INTEGER_IMMEDIATE:
      {
        IntegerImmediate integerImmediate = (IntegerImmediate)theEObject;
        T result = caseIntegerImmediate(integerImmediate);
        if (result == null) result = caseImmediateValue(integerImmediate);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sequencer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sequencer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSequencer(Sequencer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBlock(Block object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Sequencer</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Sequencer</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubSequencer(SubSequencer object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleBlock(SimpleBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>While Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>While Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseWhileBlock(WhileBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Repeat Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Repeat Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRepeatBlock(RepeatBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>If Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>If Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIfBlock(IfBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Switch Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Switch Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSwitchBlock(SwitchBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Case Block</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Case Block</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCaseBlock(CaseBlock object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Output Port Rule</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Output Port Rule</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOutputPortRule(OutputPortRule object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Output Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Output Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntegerOutputDef(IntegerOutputDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Output Def</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Output Def</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanOutputDef(BooleanOutputDef object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Abstract State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Abstract State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAbstractState(AbstractState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleState(SimpleState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallState(CallState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Nop State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Nop State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseNopState(NopState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Set State</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Set State</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSetState(SetState object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Jump</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Jump</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseJump(Jump object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Immediate Value</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Immediate Value</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseImmediateValue(ImmediateValue object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Boolean Immediate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Boolean Immediate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseBooleanImmediate(BooleanImmediate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Integer Immediate</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Integer Immediate</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseIntegerImmediate(IntegerImmediate object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //SequencerDSLSwitch
