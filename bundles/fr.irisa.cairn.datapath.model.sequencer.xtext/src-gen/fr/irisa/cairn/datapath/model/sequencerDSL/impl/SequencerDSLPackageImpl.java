/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState;
import fr.irisa.cairn.datapath.model.sequencerDSL.Block;
import fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate;
import fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef;
import fr.irisa.cairn.datapath.model.sequencerDSL.CallState;
import fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue;
import fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate;
import fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef;
import fr.irisa.cairn.datapath.model.sequencerDSL.Jump;
import fr.irisa.cairn.datapath.model.sequencerDSL.NopState;
import fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule;
import fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLFactory;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;
import fr.irisa.cairn.datapath.model.sequencerDSL.SetState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock;

import fr.irisa.cairn.model.datapath.DatapathPackage;

import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;

import fr.irisa.cairn.model.datapath.pads.PadsPackage;

import fr.irisa.cairn.model.datapath.port.PortPackage;

import fr.irisa.cairn.model.datapath.storage.StoragePackage;

import fr.irisa.cairn.model.datapath.wires.WiresPackage;

import fr.irisa.cairn.model.fsm.FsmPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SequencerDSLPackageImpl extends EPackageImpl implements SequencerDSLPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass sequencerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass blockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subSequencerEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass whileBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass repeatBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ifBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass switchBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass caseBlockEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass outputPortRuleEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerOutputDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanOutputDefEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass abstractStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass nopStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass setStateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jumpEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass immediateValueEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass booleanImmediateEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass integerImmediateEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private SequencerDSLPackageImpl()
  {
    super(eNS_URI, SequencerDSLFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link SequencerDSLPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static SequencerDSLPackage init()
  {
    if (isInited) return (SequencerDSLPackage)EPackage.Registry.INSTANCE.getEPackage(SequencerDSLPackage.eNS_URI);

    // Obtain or create and register package
    SequencerDSLPackageImpl theSequencerDSLPackage = (SequencerDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SequencerDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SequencerDSLPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    PortPackage.eINSTANCE.eClass();
    FsmPackage.eINSTANCE.eClass();
    DatapathPackage.eINSTANCE.eClass();
    WiresPackage.eINSTANCE.eClass();
    OperatorsPackage.eINSTANCE.eClass();
    PadsPackage.eINSTANCE.eClass();
    StoragePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theSequencerDSLPackage.createPackageContents();

    // Initialize created meta-data
    theSequencerDSLPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theSequencerDSLPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(SequencerDSLPackage.eNS_URI, theSequencerDSLPackage);
    return theSequencerDSLPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSequencer()
  {
    return sequencerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSequencer_Name()
  {
    return (EAttribute)sequencerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequencer_In()
  {
    return (EReference)sequencerEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequencer_Out()
  {
    return (EReference)sequencerEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequencer_SubTasks()
  {
    return (EReference)sequencerEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSequencer_Blocks()
  {
    return (EReference)sequencerEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBlock()
  {
    return blockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubSequencer()
  {
    return subSequencerEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSubSequencer_Name()
  {
    return (EAttribute)subSequencerEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSubSequencer_Blocks()
  {
    return (EReference)subSequencerEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleBlock()
  {
    return simpleBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSimpleBlock_Name()
  {
    return (EAttribute)simpleBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleBlock_States()
  {
    return (EReference)simpleBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getWhileBlock()
  {
    return whileBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileBlock_Pred()
  {
    return (EReference)whileBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getWhileBlock_Blocks()
  {
    return (EReference)whileBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRepeatBlock()
  {
    return repeatBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRepeatBlock_Mode()
  {
    return (EAttribute)repeatBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRepeatBlock_Iter()
  {
    return (EAttribute)repeatBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRepeatBlock_Blocks()
  {
    return (EReference)repeatBlockEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIfBlock()
  {
    return ifBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIfBlock_Cond()
  {
    return (EReference)ifBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIfBlock_Then()
  {
    return (EReference)ifBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getIfBlock_Else()
  {
    return (EReference)ifBlockEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSwitchBlock()
  {
    return switchBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSwitchBlock_Choice()
  {
    return (EReference)switchBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSwitchBlock_Cases()
  {
    return (EReference)switchBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSwitchBlock_Default()
  {
    return (EReference)switchBlockEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCaseBlock()
  {
    return caseBlockEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCaseBlock_Value()
  {
    return (EAttribute)caseBlockEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCaseBlock_Blocks()
  {
    return (EReference)caseBlockEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOutputPortRule()
  {
    return outputPortRuleEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOutputPortRule_Port()
  {
    return (EReference)outputPortRuleEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerOutputDef()
  {
    return integerOutputDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerOutputDef_Value()
  {
    return (EAttribute)integerOutputDefEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanOutputDef()
  {
    return booleanOutputDefEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanOutputDef_Value()
  {
    return (EAttribute)booleanOutputDefEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAbstractState()
  {
    return abstractStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAbstractState_Opcode()
  {
    return (EAttribute)abstractStateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleState()
  {
    return simpleStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleState_Jump()
  {
    return (EReference)simpleStateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallState()
  {
    return callStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallState_Function()
  {
    return (EReference)callStateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getNopState()
  {
    return nopStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSetState()
  {
    return setStateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSetState_Commands()
  {
    return (EReference)setStateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJump()
  {
    return jumpEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJump_Target()
  {
    return (EReference)jumpEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getJump_Predicate()
  {
    return (EReference)jumpEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getImmediateValue()
  {
    return immediateValueEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getBooleanImmediate()
  {
    return booleanImmediateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getBooleanImmediate_Value()
  {
    return (EAttribute)booleanImmediateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getIntegerImmediate()
  {
    return integerImmediateEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getIntegerImmediate_Value()
  {
    return (EAttribute)integerImmediateEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencerDSLFactory getSequencerDSLFactory()
  {
    return (SequencerDSLFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    sequencerEClass = createEClass(SEQUENCER);
    createEAttribute(sequencerEClass, SEQUENCER__NAME);
    createEReference(sequencerEClass, SEQUENCER__IN);
    createEReference(sequencerEClass, SEQUENCER__OUT);
    createEReference(sequencerEClass, SEQUENCER__SUB_TASKS);
    createEReference(sequencerEClass, SEQUENCER__BLOCKS);

    blockEClass = createEClass(BLOCK);

    subSequencerEClass = createEClass(SUB_SEQUENCER);
    createEAttribute(subSequencerEClass, SUB_SEQUENCER__NAME);
    createEReference(subSequencerEClass, SUB_SEQUENCER__BLOCKS);

    simpleBlockEClass = createEClass(SIMPLE_BLOCK);
    createEAttribute(simpleBlockEClass, SIMPLE_BLOCK__NAME);
    createEReference(simpleBlockEClass, SIMPLE_BLOCK__STATES);

    whileBlockEClass = createEClass(WHILE_BLOCK);
    createEReference(whileBlockEClass, WHILE_BLOCK__PRED);
    createEReference(whileBlockEClass, WHILE_BLOCK__BLOCKS);

    repeatBlockEClass = createEClass(REPEAT_BLOCK);
    createEAttribute(repeatBlockEClass, REPEAT_BLOCK__MODE);
    createEAttribute(repeatBlockEClass, REPEAT_BLOCK__ITER);
    createEReference(repeatBlockEClass, REPEAT_BLOCK__BLOCKS);

    ifBlockEClass = createEClass(IF_BLOCK);
    createEReference(ifBlockEClass, IF_BLOCK__COND);
    createEReference(ifBlockEClass, IF_BLOCK__THEN);
    createEReference(ifBlockEClass, IF_BLOCK__ELSE);

    switchBlockEClass = createEClass(SWITCH_BLOCK);
    createEReference(switchBlockEClass, SWITCH_BLOCK__CHOICE);
    createEReference(switchBlockEClass, SWITCH_BLOCK__CASES);
    createEReference(switchBlockEClass, SWITCH_BLOCK__DEFAULT);

    caseBlockEClass = createEClass(CASE_BLOCK);
    createEAttribute(caseBlockEClass, CASE_BLOCK__VALUE);
    createEReference(caseBlockEClass, CASE_BLOCK__BLOCKS);

    outputPortRuleEClass = createEClass(OUTPUT_PORT_RULE);
    createEReference(outputPortRuleEClass, OUTPUT_PORT_RULE__PORT);

    integerOutputDefEClass = createEClass(INTEGER_OUTPUT_DEF);
    createEAttribute(integerOutputDefEClass, INTEGER_OUTPUT_DEF__VALUE);

    booleanOutputDefEClass = createEClass(BOOLEAN_OUTPUT_DEF);
    createEAttribute(booleanOutputDefEClass, BOOLEAN_OUTPUT_DEF__VALUE);

    abstractStateEClass = createEClass(ABSTRACT_STATE);
    createEAttribute(abstractStateEClass, ABSTRACT_STATE__OPCODE);

    simpleStateEClass = createEClass(SIMPLE_STATE);
    createEReference(simpleStateEClass, SIMPLE_STATE__JUMP);

    callStateEClass = createEClass(CALL_STATE);
    createEReference(callStateEClass, CALL_STATE__FUNCTION);

    nopStateEClass = createEClass(NOP_STATE);

    setStateEClass = createEClass(SET_STATE);
    createEReference(setStateEClass, SET_STATE__COMMANDS);

    jumpEClass = createEClass(JUMP);
    createEReference(jumpEClass, JUMP__TARGET);
    createEReference(jumpEClass, JUMP__PREDICATE);

    immediateValueEClass = createEClass(IMMEDIATE_VALUE);

    booleanImmediateEClass = createEClass(BOOLEAN_IMMEDIATE);
    createEAttribute(booleanImmediateEClass, BOOLEAN_IMMEDIATE__VALUE);

    integerImmediateEClass = createEClass(INTEGER_IMMEDIATE);
    createEAttribute(integerImmediateEClass, INTEGER_IMMEDIATE__VALUE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
    FsmPackage theFsmPackage = (FsmPackage)EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    simpleBlockEClass.getESuperTypes().add(this.getBlock());
    whileBlockEClass.getESuperTypes().add(this.getBlock());
    repeatBlockEClass.getESuperTypes().add(this.getBlock());
    ifBlockEClass.getESuperTypes().add(this.getBlock());
    switchBlockEClass.getESuperTypes().add(this.getBlock());
    integerOutputDefEClass.getESuperTypes().add(this.getOutputPortRule());
    booleanOutputDefEClass.getESuperTypes().add(this.getOutputPortRule());
    simpleStateEClass.getESuperTypes().add(this.getAbstractState());
    callStateEClass.getESuperTypes().add(this.getAbstractState());
    nopStateEClass.getESuperTypes().add(this.getSimpleState());
    setStateEClass.getESuperTypes().add(this.getSimpleState());
    booleanImmediateEClass.getESuperTypes().add(this.getImmediateValue());
    integerImmediateEClass.getESuperTypes().add(this.getImmediateValue());

    // Initialize classes and features; add operations and parameters
    initEClass(sequencerEClass, Sequencer.class, "Sequencer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSequencer_Name(), ecorePackage.getEString(), "name", null, 0, 1, Sequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSequencer_In(), thePortPackage.getInControlPort(), null, "in", null, 0, -1, Sequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSequencer_Out(), this.getOutputPortRule(), null, "out", null, 0, -1, Sequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSequencer_SubTasks(), this.getSubSequencer(), null, "subTasks", null, 0, -1, Sequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSequencer_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, Sequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(blockEClass, Block.class, "Block", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(subSequencerEClass, SubSequencer.class, "SubSequencer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSubSequencer_Name(), ecorePackage.getEString(), "name", null, 0, 1, SubSequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSubSequencer_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, SubSequencer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleBlockEClass, SimpleBlock.class, "SimpleBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSimpleBlock_Name(), ecorePackage.getEString(), "name", null, 0, 1, SimpleBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSimpleBlock_States(), this.getAbstractState(), null, "states", null, 0, -1, SimpleBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(whileBlockEClass, WhileBlock.class, "WhileBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getWhileBlock_Pred(), theFsmPackage.getAbstractBooleanExpression(), null, "pred", null, 0, 1, WhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getWhileBlock_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, WhileBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(repeatBlockEClass, RepeatBlock.class, "RepeatBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRepeatBlock_Mode(), ecorePackage.getEString(), "mode", null, 0, 1, RepeatBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRepeatBlock_Iter(), ecorePackage.getEInt(), "iter", null, 0, 1, RepeatBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRepeatBlock_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, RepeatBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ifBlockEClass, IfBlock.class, "IfBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getIfBlock_Cond(), theFsmPackage.getAbstractBooleanExpression(), null, "cond", null, 0, 1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIfBlock_Then(), this.getBlock(), null, "then", null, 0, -1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getIfBlock_Else(), this.getBlock(), null, "else", null, 0, -1, IfBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(switchBlockEClass, SwitchBlock.class, "SwitchBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSwitchBlock_Choice(), thePortPackage.getInControlPort(), null, "choice", null, 0, 1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSwitchBlock_Cases(), this.getCaseBlock(), null, "cases", null, 0, -1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSwitchBlock_Default(), this.getBlock(), null, "default", null, 0, -1, SwitchBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(caseBlockEClass, CaseBlock.class, "CaseBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCaseBlock_Value(), ecorePackage.getEInt(), "value", null, 0, 1, CaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCaseBlock_Blocks(), this.getBlock(), null, "blocks", null, 0, -1, CaseBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(outputPortRuleEClass, OutputPortRule.class, "OutputPortRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOutputPortRule_Port(), thePortPackage.getOutControlPort(), null, "port", null, 0, 1, OutputPortRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerOutputDefEClass, IntegerOutputDef.class, "IntegerOutputDef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntegerOutputDef_Value(), ecorePackage.getEInt(), "value", null, 0, 1, IntegerOutputDef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(booleanOutputDefEClass, BooleanOutputDef.class, "BooleanOutputDef", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanOutputDef_Value(), theFsmPackage.getBooleanValue(), "value", null, 0, 1, BooleanOutputDef.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(abstractStateEClass, AbstractState.class, "AbstractState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAbstractState_Opcode(), ecorePackage.getEString(), "opcode", null, 0, 1, AbstractState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleStateEClass, SimpleState.class, "SimpleState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimpleState_Jump(), this.getJump(), null, "jump", null, 0, 1, SimpleState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(callStateEClass, CallState.class, "CallState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCallState_Function(), this.getSubSequencer(), null, "function", null, 0, 1, CallState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(nopStateEClass, NopState.class, "NopState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(setStateEClass, SetState.class, "SetState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSetState_Commands(), theFsmPackage.getAbstractCommandValue(), null, "commands", null, 0, -1, SetState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jumpEClass, Jump.class, "Jump", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getJump_Target(), this.getSimpleBlock(), null, "target", null, 0, 1, Jump.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getJump_Predicate(), theFsmPackage.getAbstractBooleanExpression(), null, "predicate", null, 0, 1, Jump.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(immediateValueEClass, ImmediateValue.class, "ImmediateValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(booleanImmediateEClass, BooleanImmediate.class, "BooleanImmediate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getBooleanImmediate_Value(), theFsmPackage.getBooleanValue(), "value", null, 0, 1, BooleanImmediate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(integerImmediateEClass, IntegerImmediate.class, "IntegerImmediate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getIntegerImmediate_Value(), ecorePackage.getEInt(), "value", null, 0, 1, IntegerImmediate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //SequencerDSLPackageImpl
