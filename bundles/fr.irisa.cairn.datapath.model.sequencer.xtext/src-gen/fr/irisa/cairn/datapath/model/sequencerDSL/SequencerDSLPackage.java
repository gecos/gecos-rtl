/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLFactory
 * @model kind="package"
 * @generated
 */
public interface SequencerDSLPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "sequencerDSL";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.irisa.fr/cairn/model/datapath/SequencerDSL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "sequencerDSL";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SequencerDSLPackage eINSTANCE = fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl.init();

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl <em>Sequencer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSequencer()
   * @generated
   */
  int SEQUENCER = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER__NAME = 0;

  /**
   * The feature id for the '<em><b>In</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER__IN = 1;

  /**
   * The feature id for the '<em><b>Out</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER__OUT = 2;

  /**
   * The feature id for the '<em><b>Sub Tasks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER__SUB_TASKS = 3;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER__BLOCKS = 4;

  /**
   * The number of structural features of the '<em>Sequencer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SEQUENCER_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BlockImpl <em>Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBlock()
   * @generated
   */
  int BLOCK = 1;

  /**
   * The number of structural features of the '<em>Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BLOCK_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SubSequencerImpl <em>Sub Sequencer</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SubSequencerImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSubSequencer()
   * @generated
   */
  int SUB_SEQUENCER = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_SEQUENCER__NAME = 0;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_SEQUENCER__BLOCKS = 1;

  /**
   * The number of structural features of the '<em>Sub Sequencer</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_SEQUENCER_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleBlockImpl <em>Simple Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSimpleBlock()
   * @generated
   */
  int SIMPLE_BLOCK = 3;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_BLOCK__NAME = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>States</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_BLOCK__STATES = BLOCK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Simple Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.WhileBlockImpl <em>While Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.WhileBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getWhileBlock()
   * @generated
   */
  int WHILE_BLOCK = 4;

  /**
   * The feature id for the '<em><b>Pred</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_BLOCK__PRED = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_BLOCK__BLOCKS = BLOCK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>While Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WHILE_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl <em>Repeat Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getRepeatBlock()
   * @generated
   */
  int REPEAT_BLOCK = 5;

  /**
   * The feature id for the '<em><b>Mode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_BLOCK__MODE = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Iter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_BLOCK__ITER = BLOCK_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_BLOCK__BLOCKS = BLOCK_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Repeat Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEAT_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IfBlockImpl <em>If Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IfBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIfBlock()
   * @generated
   */
  int IF_BLOCK = 6;

  /**
   * The feature id for the '<em><b>Cond</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_BLOCK__COND = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Then</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_BLOCK__THEN = BLOCK_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Else</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_BLOCK__ELSE = BLOCK_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>If Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IF_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl <em>Switch Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSwitchBlock()
   * @generated
   */
  int SWITCH_BLOCK = 7;

  /**
   * The feature id for the '<em><b>Choice</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_BLOCK__CHOICE = BLOCK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Cases</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_BLOCK__CASES = BLOCK_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Default</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_BLOCK__DEFAULT = BLOCK_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Switch Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SWITCH_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.CaseBlockImpl <em>Case Block</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.CaseBlockImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getCaseBlock()
   * @generated
   */
  int CASE_BLOCK = 8;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_BLOCK__VALUE = 0;

  /**
   * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_BLOCK__BLOCKS = 1;

  /**
   * The number of structural features of the '<em>Case Block</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CASE_BLOCK_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.OutputPortRuleImpl <em>Output Port Rule</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.OutputPortRuleImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getOutputPortRule()
   * @generated
   */
  int OUTPUT_PORT_RULE = 9;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_PORT_RULE__PORT = 0;

  /**
   * The number of structural features of the '<em>Output Port Rule</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OUTPUT_PORT_RULE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerOutputDefImpl <em>Integer Output Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerOutputDefImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIntegerOutputDef()
   * @generated
   */
  int INTEGER_OUTPUT_DEF = 10;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OUTPUT_DEF__PORT = OUTPUT_PORT_RULE__PORT;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OUTPUT_DEF__VALUE = OUTPUT_PORT_RULE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Output Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_OUTPUT_DEF_FEATURE_COUNT = OUTPUT_PORT_RULE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanOutputDefImpl <em>Boolean Output Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanOutputDefImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBooleanOutputDef()
   * @generated
   */
  int BOOLEAN_OUTPUT_DEF = 11;

  /**
   * The feature id for the '<em><b>Port</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OUTPUT_DEF__PORT = OUTPUT_PORT_RULE__PORT;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OUTPUT_DEF__VALUE = OUTPUT_PORT_RULE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Output Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_OUTPUT_DEF_FEATURE_COUNT = OUTPUT_PORT_RULE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.AbstractStateImpl <em>Abstract State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.AbstractStateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getAbstractState()
   * @generated
   */
  int ABSTRACT_STATE = 12;

  /**
   * The feature id for the '<em><b>Opcode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_STATE__OPCODE = 0;

  /**
   * The number of structural features of the '<em>Abstract State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_STATE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleStateImpl <em>Simple State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleStateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSimpleState()
   * @generated
   */
  int SIMPLE_STATE = 13;

  /**
   * The feature id for the '<em><b>Opcode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_STATE__OPCODE = ABSTRACT_STATE__OPCODE;

  /**
   * The feature id for the '<em><b>Jump</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_STATE__JUMP = ABSTRACT_STATE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Simple State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.CallStateImpl <em>Call State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.CallStateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getCallState()
   * @generated
   */
  int CALL_STATE = 14;

  /**
   * The feature id for the '<em><b>Opcode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATE__OPCODE = ABSTRACT_STATE__OPCODE;

  /**
   * The feature id for the '<em><b>Function</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATE__FUNCTION = ABSTRACT_STATE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Call State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.NopStateImpl <em>Nop State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.NopStateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getNopState()
   * @generated
   */
  int NOP_STATE = 15;

  /**
   * The feature id for the '<em><b>Opcode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOP_STATE__OPCODE = SIMPLE_STATE__OPCODE;

  /**
   * The feature id for the '<em><b>Jump</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOP_STATE__JUMP = SIMPLE_STATE__JUMP;

  /**
   * The number of structural features of the '<em>Nop State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int NOP_STATE_FEATURE_COUNT = SIMPLE_STATE_FEATURE_COUNT + 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SetStateImpl <em>Set State</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SetStateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSetState()
   * @generated
   */
  int SET_STATE = 16;

  /**
   * The feature id for the '<em><b>Opcode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_STATE__OPCODE = SIMPLE_STATE__OPCODE;

  /**
   * The feature id for the '<em><b>Jump</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_STATE__JUMP = SIMPLE_STATE__JUMP;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_STATE__COMMANDS = SIMPLE_STATE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Set State</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SET_STATE_FEATURE_COUNT = SIMPLE_STATE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.JumpImpl <em>Jump</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.JumpImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getJump()
   * @generated
   */
  int JUMP = 17;

  /**
   * The feature id for the '<em><b>Target</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP__TARGET = 0;

  /**
   * The feature id for the '<em><b>Predicate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP__PREDICATE = 1;

  /**
   * The number of structural features of the '<em>Jump</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JUMP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.ImmediateValueImpl <em>Immediate Value</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.ImmediateValueImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getImmediateValue()
   * @generated
   */
  int IMMEDIATE_VALUE = 18;

  /**
   * The number of structural features of the '<em>Immediate Value</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMMEDIATE_VALUE_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanImmediateImpl <em>Boolean Immediate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanImmediateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBooleanImmediate()
   * @generated
   */
  int BOOLEAN_IMMEDIATE = 19;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_IMMEDIATE__VALUE = IMMEDIATE_VALUE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Boolean Immediate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int BOOLEAN_IMMEDIATE_FEATURE_COUNT = IMMEDIATE_VALUE_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerImmediateImpl <em>Integer Immediate</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerImmediateImpl
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIntegerImmediate()
   * @generated
   */
  int INTEGER_IMMEDIATE = 20;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_IMMEDIATE__VALUE = IMMEDIATE_VALUE_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Integer Immediate</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTEGER_IMMEDIATE_FEATURE_COUNT = IMMEDIATE_VALUE_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer <em>Sequencer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sequencer</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer
   * @generated
   */
  EClass getSequencer();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getName()
   * @see #getSequencer()
   * @generated
   */
  EAttribute getSequencer_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getIn <em>In</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>In</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getIn()
   * @see #getSequencer()
   * @generated
   */
  EReference getSequencer_In();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getOut <em>Out</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Out</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getOut()
   * @see #getSequencer()
   * @generated
   */
  EReference getSequencer_Out();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getSubTasks <em>Sub Tasks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Tasks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getSubTasks()
   * @see #getSequencer()
   * @generated
   */
  EReference getSequencer_SubTasks();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer#getBlocks()
   * @see #getSequencer()
   * @generated
   */
  EReference getSequencer_Blocks();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Block <em>Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Block
   * @generated
   */
  EClass getBlock();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer <em>Sub Sequencer</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Sequencer</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer
   * @generated
   */
  EClass getSubSequencer();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer#getName()
   * @see #getSubSequencer()
   * @generated
   */
  EAttribute getSubSequencer_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SubSequencer#getBlocks()
   * @see #getSubSequencer()
   * @generated
   */
  EReference getSubSequencer_Blocks();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock <em>Simple Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock
   * @generated
   */
  EClass getSimpleBlock();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getName()
   * @see #getSimpleBlock()
   * @generated
   */
  EAttribute getSimpleBlock_Name();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getStates <em>States</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>States</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getStates()
   * @see #getSimpleBlock()
   * @generated
   */
  EReference getSimpleBlock_States();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock <em>While Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>While Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock
   * @generated
   */
  EClass getWhileBlock();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getPred <em>Pred</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Pred</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getPred()
   * @see #getWhileBlock()
   * @generated
   */
  EReference getWhileBlock_Pred();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getBlocks()
   * @see #getWhileBlock()
   * @generated
   */
  EReference getWhileBlock_Blocks();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock <em>Repeat Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repeat Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock
   * @generated
   */
  EClass getRepeatBlock();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getMode <em>Mode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Mode</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getMode()
   * @see #getRepeatBlock()
   * @generated
   */
  EAttribute getRepeatBlock_Mode();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getIter <em>Iter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Iter</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getIter()
   * @see #getRepeatBlock()
   * @generated
   */
  EAttribute getRepeatBlock_Iter();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getBlocks()
   * @see #getRepeatBlock()
   * @generated
   */
  EReference getRepeatBlock_Blocks();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock <em>If Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>If Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock
   * @generated
   */
  EClass getIfBlock();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getCond <em>Cond</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Cond</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getCond()
   * @see #getIfBlock()
   * @generated
   */
  EReference getIfBlock_Cond();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getThen <em>Then</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Then</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getThen()
   * @see #getIfBlock()
   * @generated
   */
  EReference getIfBlock_Then();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getElse <em>Else</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Else</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock#getElse()
   * @see #getIfBlock()
   * @generated
   */
  EReference getIfBlock_Else();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock <em>Switch Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Switch Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock
   * @generated
   */
  EClass getSwitchBlock();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getChoice <em>Choice</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Choice</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getChoice()
   * @see #getSwitchBlock()
   * @generated
   */
  EReference getSwitchBlock_Choice();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getCases <em>Cases</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Cases</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getCases()
   * @see #getSwitchBlock()
   * @generated
   */
  EReference getSwitchBlock_Cases();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getDefault <em>Default</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Default</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getDefault()
   * @see #getSwitchBlock()
   * @generated
   */
  EReference getSwitchBlock_Default();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock <em>Case Block</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Case Block</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock
   * @generated
   */
  EClass getCaseBlock();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock#getValue()
   * @see #getCaseBlock()
   * @generated
   */
  EAttribute getCaseBlock_Value();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock#getBlocks <em>Blocks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Blocks</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock#getBlocks()
   * @see #getCaseBlock()
   * @generated
   */
  EReference getCaseBlock_Blocks();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule <em>Output Port Rule</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Output Port Rule</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule
   * @generated
   */
  EClass getOutputPortRule();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Port</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule#getPort()
   * @see #getOutputPortRule()
   * @generated
   */
  EReference getOutputPortRule_Port();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef <em>Integer Output Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Output Def</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef
   * @generated
   */
  EClass getIntegerOutputDef();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef#getValue()
   * @see #getIntegerOutputDef()
   * @generated
   */
  EAttribute getIntegerOutputDef_Value();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef <em>Boolean Output Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Output Def</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef
   * @generated
   */
  EClass getBooleanOutputDef();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanOutputDef#getValue()
   * @see #getBooleanOutputDef()
   * @generated
   */
  EAttribute getBooleanOutputDef_Value();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState <em>Abstract State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstract State</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState
   * @generated
   */
  EClass getAbstractState();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState#getOpcode <em>Opcode</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Opcode</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState#getOpcode()
   * @see #getAbstractState()
   * @generated
   */
  EAttribute getAbstractState_Opcode();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState <em>Simple State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple State</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState
   * @generated
   */
  EClass getSimpleState();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState#getJump <em>Jump</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Jump</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState#getJump()
   * @see #getSimpleState()
   * @generated
   */
  EReference getSimpleState_Jump();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CallState <em>Call State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call State</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CallState
   * @generated
   */
  EClass getCallState();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.CallState#getFunction <em>Function</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Function</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.CallState#getFunction()
   * @see #getCallState()
   * @generated
   */
  EReference getCallState_Function();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.NopState <em>Nop State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Nop State</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.NopState
   * @generated
   */
  EClass getNopState();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SetState <em>Set State</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Set State</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SetState
   * @generated
   */
  EClass getSetState();

  /**
   * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SetState#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SetState#getCommands()
   * @see #getSetState()
   * @generated
   */
  EReference getSetState_Commands();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump <em>Jump</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Jump</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Jump
   * @generated
   */
  EClass getJump();

  /**
   * Returns the meta object for the reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getTarget <em>Target</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Target</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getTarget()
   * @see #getJump()
   * @generated
   */
  EReference getJump_Target();

  /**
   * Returns the meta object for the containment reference '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getPredicate <em>Predicate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Predicate</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getPredicate()
   * @see #getJump()
   * @generated
   */
  EReference getJump_Predicate();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue <em>Immediate Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Immediate Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.ImmediateValue
   * @generated
   */
  EClass getImmediateValue();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate <em>Boolean Immediate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Boolean Immediate</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate
   * @generated
   */
  EClass getBooleanImmediate();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.BooleanImmediate#getValue()
   * @see #getBooleanImmediate()
   * @generated
   */
  EAttribute getBooleanImmediate_Value();

  /**
   * Returns the meta object for class '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate <em>Integer Immediate</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Integer Immediate</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate
   * @generated
   */
  EClass getIntegerImmediate();

  /**
   * Returns the meta object for the attribute '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.IntegerImmediate#getValue()
   * @see #getIntegerImmediate()
   * @generated
   */
  EAttribute getIntegerImmediate_Value();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SequencerDSLFactory getSequencerDSLFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl <em>Sequencer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSequencer()
     * @generated
     */
    EClass SEQUENCER = eINSTANCE.getSequencer();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SEQUENCER__NAME = eINSTANCE.getSequencer_Name();

    /**
     * The meta object literal for the '<em><b>In</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCER__IN = eINSTANCE.getSequencer_In();

    /**
     * The meta object literal for the '<em><b>Out</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCER__OUT = eINSTANCE.getSequencer_Out();

    /**
     * The meta object literal for the '<em><b>Sub Tasks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCER__SUB_TASKS = eINSTANCE.getSequencer_SubTasks();

    /**
     * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SEQUENCER__BLOCKS = eINSTANCE.getSequencer_Blocks();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BlockImpl <em>Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBlock()
     * @generated
     */
    EClass BLOCK = eINSTANCE.getBlock();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SubSequencerImpl <em>Sub Sequencer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SubSequencerImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSubSequencer()
     * @generated
     */
    EClass SUB_SEQUENCER = eINSTANCE.getSubSequencer();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SUB_SEQUENCER__NAME = eINSTANCE.getSubSequencer_Name();

    /**
     * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SUB_SEQUENCER__BLOCKS = eINSTANCE.getSubSequencer_Blocks();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleBlockImpl <em>Simple Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSimpleBlock()
     * @generated
     */
    EClass SIMPLE_BLOCK = eINSTANCE.getSimpleBlock();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SIMPLE_BLOCK__NAME = eINSTANCE.getSimpleBlock_Name();

    /**
     * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_BLOCK__STATES = eINSTANCE.getSimpleBlock_States();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.WhileBlockImpl <em>While Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.WhileBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getWhileBlock()
     * @generated
     */
    EClass WHILE_BLOCK = eINSTANCE.getWhileBlock();

    /**
     * The meta object literal for the '<em><b>Pred</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE_BLOCK__PRED = eINSTANCE.getWhileBlock_Pred();

    /**
     * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference WHILE_BLOCK__BLOCKS = eINSTANCE.getWhileBlock_Blocks();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl <em>Repeat Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getRepeatBlock()
     * @generated
     */
    EClass REPEAT_BLOCK = eINSTANCE.getRepeatBlock();

    /**
     * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPEAT_BLOCK__MODE = eINSTANCE.getRepeatBlock_Mode();

    /**
     * The meta object literal for the '<em><b>Iter</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPEAT_BLOCK__ITER = eINSTANCE.getRepeatBlock_Iter();

    /**
     * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPEAT_BLOCK__BLOCKS = eINSTANCE.getRepeatBlock_Blocks();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IfBlockImpl <em>If Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IfBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIfBlock()
     * @generated
     */
    EClass IF_BLOCK = eINSTANCE.getIfBlock();

    /**
     * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_BLOCK__COND = eINSTANCE.getIfBlock_Cond();

    /**
     * The meta object literal for the '<em><b>Then</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_BLOCK__THEN = eINSTANCE.getIfBlock_Then();

    /**
     * The meta object literal for the '<em><b>Else</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IF_BLOCK__ELSE = eINSTANCE.getIfBlock_Else();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl <em>Switch Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSwitchBlock()
     * @generated
     */
    EClass SWITCH_BLOCK = eINSTANCE.getSwitchBlock();

    /**
     * The meta object literal for the '<em><b>Choice</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_BLOCK__CHOICE = eINSTANCE.getSwitchBlock_Choice();

    /**
     * The meta object literal for the '<em><b>Cases</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_BLOCK__CASES = eINSTANCE.getSwitchBlock_Cases();

    /**
     * The meta object literal for the '<em><b>Default</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SWITCH_BLOCK__DEFAULT = eINSTANCE.getSwitchBlock_Default();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.CaseBlockImpl <em>Case Block</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.CaseBlockImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getCaseBlock()
     * @generated
     */
    EClass CASE_BLOCK = eINSTANCE.getCaseBlock();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CASE_BLOCK__VALUE = eINSTANCE.getCaseBlock_Value();

    /**
     * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CASE_BLOCK__BLOCKS = eINSTANCE.getCaseBlock_Blocks();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.OutputPortRuleImpl <em>Output Port Rule</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.OutputPortRuleImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getOutputPortRule()
     * @generated
     */
    EClass OUTPUT_PORT_RULE = eINSTANCE.getOutputPortRule();

    /**
     * The meta object literal for the '<em><b>Port</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OUTPUT_PORT_RULE__PORT = eINSTANCE.getOutputPortRule_Port();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerOutputDefImpl <em>Integer Output Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerOutputDefImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIntegerOutputDef()
     * @generated
     */
    EClass INTEGER_OUTPUT_DEF = eINSTANCE.getIntegerOutputDef();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_OUTPUT_DEF__VALUE = eINSTANCE.getIntegerOutputDef_Value();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanOutputDefImpl <em>Boolean Output Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanOutputDefImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBooleanOutputDef()
     * @generated
     */
    EClass BOOLEAN_OUTPUT_DEF = eINSTANCE.getBooleanOutputDef();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_OUTPUT_DEF__VALUE = eINSTANCE.getBooleanOutputDef_Value();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.AbstractStateImpl <em>Abstract State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.AbstractStateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getAbstractState()
     * @generated
     */
    EClass ABSTRACT_STATE = eINSTANCE.getAbstractState();

    /**
     * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ABSTRACT_STATE__OPCODE = eINSTANCE.getAbstractState_Opcode();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleStateImpl <em>Simple State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SimpleStateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSimpleState()
     * @generated
     */
    EClass SIMPLE_STATE = eINSTANCE.getSimpleState();

    /**
     * The meta object literal for the '<em><b>Jump</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_STATE__JUMP = eINSTANCE.getSimpleState_Jump();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.CallStateImpl <em>Call State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.CallStateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getCallState()
     * @generated
     */
    EClass CALL_STATE = eINSTANCE.getCallState();

    /**
     * The meta object literal for the '<em><b>Function</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_STATE__FUNCTION = eINSTANCE.getCallState_Function();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.NopStateImpl <em>Nop State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.NopStateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getNopState()
     * @generated
     */
    EClass NOP_STATE = eINSTANCE.getNopState();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SetStateImpl <em>Set State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SetStateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getSetState()
     * @generated
     */
    EClass SET_STATE = eINSTANCE.getSetState();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SET_STATE__COMMANDS = eINSTANCE.getSetState_Commands();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.JumpImpl <em>Jump</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.JumpImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getJump()
     * @generated
     */
    EClass JUMP = eINSTANCE.getJump();

    /**
     * The meta object literal for the '<em><b>Target</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference JUMP__TARGET = eINSTANCE.getJump_Target();

    /**
     * The meta object literal for the '<em><b>Predicate</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference JUMP__PREDICATE = eINSTANCE.getJump_Predicate();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.ImmediateValueImpl <em>Immediate Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.ImmediateValueImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getImmediateValue()
     * @generated
     */
    EClass IMMEDIATE_VALUE = eINSTANCE.getImmediateValue();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanImmediateImpl <em>Boolean Immediate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.BooleanImmediateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getBooleanImmediate()
     * @generated
     */
    EClass BOOLEAN_IMMEDIATE = eINSTANCE.getBooleanImmediate();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute BOOLEAN_IMMEDIATE__VALUE = eINSTANCE.getBooleanImmediate_Value();

    /**
     * The meta object literal for the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerImmediateImpl <em>Integer Immediate</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.IntegerImmediateImpl
     * @see fr.irisa.cairn.datapath.model.sequencerDSL.impl.SequencerDSLPackageImpl#getIntegerImmediate()
     * @generated
     */
    EClass INTEGER_IMMEDIATE = eINSTANCE.getIntegerImmediate();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTEGER_IMMEDIATE__VALUE = eINSTANCE.getIntegerImmediate_Value();

  }

} //SequencerDSLPackage
