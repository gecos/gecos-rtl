/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.NopState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Nop State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NopStateImpl extends SimpleStateImpl implements NopState
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected NopStateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SequencerDSLPackage.Literals.NOP_STATE;
  }

} //NopStateImpl
