/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState#getJump <em>Jump</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSimpleState()
 * @model
 * @generated
 */
public interface SimpleState extends AbstractState
{
  /**
   * Returns the value of the '<em><b>Jump</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Jump</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Jump</em>' containment reference.
   * @see #setJump(Jump)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSimpleState_Jump()
   * @model containment="true"
   * @generated
   */
  Jump getJump();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState#getJump <em>Jump</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Jump</em>' containment reference.
   * @see #getJump()
   * @generated
   */
  void setJump(Jump value);

} // SimpleState
