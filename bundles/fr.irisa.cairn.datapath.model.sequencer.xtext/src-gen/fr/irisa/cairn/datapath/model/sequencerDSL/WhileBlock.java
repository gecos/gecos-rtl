/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>While Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getPred <em>Pred</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getWhileBlock()
 * @model
 * @generated
 */
public interface WhileBlock extends Block
{
  /**
   * Returns the value of the '<em><b>Pred</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pred</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pred</em>' containment reference.
   * @see #setPred(AbstractBooleanExpression)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getWhileBlock_Pred()
   * @model containment="true"
   * @generated
   */
  AbstractBooleanExpression getPred();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock#getPred <em>Pred</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pred</em>' containment reference.
   * @see #getPred()
   * @generated
   */
  void setPred(AbstractBooleanExpression value);

  /**
   * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.Block}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Blocks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Blocks</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getWhileBlock_Blocks()
   * @model containment="true"
   * @generated
   */
  EList<Block> getBlocks();

} // WhileBlock
