/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SequencerDSLFactoryImpl extends EFactoryImpl implements SequencerDSLFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SequencerDSLFactory init()
  {
    try
    {
      SequencerDSLFactory theSequencerDSLFactory = (SequencerDSLFactory)EPackage.Registry.INSTANCE.getEFactory(SequencerDSLPackage.eNS_URI);
      if (theSequencerDSLFactory != null)
      {
        return theSequencerDSLFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SequencerDSLFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencerDSLFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SequencerDSLPackage.SEQUENCER: return createSequencer();
      case SequencerDSLPackage.BLOCK: return createBlock();
      case SequencerDSLPackage.SUB_SEQUENCER: return createSubSequencer();
      case SequencerDSLPackage.SIMPLE_BLOCK: return createSimpleBlock();
      case SequencerDSLPackage.WHILE_BLOCK: return createWhileBlock();
      case SequencerDSLPackage.REPEAT_BLOCK: return createRepeatBlock();
      case SequencerDSLPackage.IF_BLOCK: return createIfBlock();
      case SequencerDSLPackage.SWITCH_BLOCK: return createSwitchBlock();
      case SequencerDSLPackage.CASE_BLOCK: return createCaseBlock();
      case SequencerDSLPackage.OUTPUT_PORT_RULE: return createOutputPortRule();
      case SequencerDSLPackage.INTEGER_OUTPUT_DEF: return createIntegerOutputDef();
      case SequencerDSLPackage.BOOLEAN_OUTPUT_DEF: return createBooleanOutputDef();
      case SequencerDSLPackage.ABSTRACT_STATE: return createAbstractState();
      case SequencerDSLPackage.SIMPLE_STATE: return createSimpleState();
      case SequencerDSLPackage.CALL_STATE: return createCallState();
      case SequencerDSLPackage.NOP_STATE: return createNopState();
      case SequencerDSLPackage.SET_STATE: return createSetState();
      case SequencerDSLPackage.JUMP: return createJump();
      case SequencerDSLPackage.IMMEDIATE_VALUE: return createImmediateValue();
      case SequencerDSLPackage.BOOLEAN_IMMEDIATE: return createBooleanImmediate();
      case SequencerDSLPackage.INTEGER_IMMEDIATE: return createIntegerImmediate();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Sequencer createSequencer()
  {
    SequencerImpl sequencer = new SequencerImpl();
    return sequencer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Block createBlock()
  {
    BlockImpl block = new BlockImpl();
    return block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubSequencer createSubSequencer()
  {
    SubSequencerImpl subSequencer = new SubSequencerImpl();
    return subSequencer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleBlock createSimpleBlock()
  {
    SimpleBlockImpl simpleBlock = new SimpleBlockImpl();
    return simpleBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public WhileBlock createWhileBlock()
  {
    WhileBlockImpl whileBlock = new WhileBlockImpl();
    return whileBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepeatBlock createRepeatBlock()
  {
    RepeatBlockImpl repeatBlock = new RepeatBlockImpl();
    return repeatBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IfBlock createIfBlock()
  {
    IfBlockImpl ifBlock = new IfBlockImpl();
    return ifBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SwitchBlock createSwitchBlock()
  {
    SwitchBlockImpl switchBlock = new SwitchBlockImpl();
    return switchBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CaseBlock createCaseBlock()
  {
    CaseBlockImpl caseBlock = new CaseBlockImpl();
    return caseBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OutputPortRule createOutputPortRule()
  {
    OutputPortRuleImpl outputPortRule = new OutputPortRuleImpl();
    return outputPortRule;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerOutputDef createIntegerOutputDef()
  {
    IntegerOutputDefImpl integerOutputDef = new IntegerOutputDefImpl();
    return integerOutputDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanOutputDef createBooleanOutputDef()
  {
    BooleanOutputDefImpl booleanOutputDef = new BooleanOutputDefImpl();
    return booleanOutputDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractState createAbstractState()
  {
    AbstractStateImpl abstractState = new AbstractStateImpl();
    return abstractState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleState createSimpleState()
  {
    SimpleStateImpl simpleState = new SimpleStateImpl();
    return simpleState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallState createCallState()
  {
    CallStateImpl callState = new CallStateImpl();
    return callState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NopState createNopState()
  {
    NopStateImpl nopState = new NopStateImpl();
    return nopState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SetState createSetState()
  {
    SetStateImpl setState = new SetStateImpl();
    return setState;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Jump createJump()
  {
    JumpImpl jump = new JumpImpl();
    return jump;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ImmediateValue createImmediateValue()
  {
    ImmediateValueImpl immediateValue = new ImmediateValueImpl();
    return immediateValue;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BooleanImmediate createBooleanImmediate()
  {
    BooleanImmediateImpl booleanImmediate = new BooleanImmediateImpl();
    return booleanImmediate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public IntegerImmediate createIntegerImmediate()
  {
    IntegerImmediateImpl integerImmediate = new IntegerImmediateImpl();
    return integerImmediate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SequencerDSLPackage getSequencerDSLPackage()
  {
    return (SequencerDSLPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SequencerDSLPackage getPackage()
  {
    return SequencerDSLPackage.eINSTANCE;
  }

} //SequencerDSLFactoryImpl
