/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Output Def</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getIntegerOutputDef()
 * @model
 * @generated
 */
public interface IntegerOutputDef extends OutputPortRule
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(int)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getIntegerOutputDef_Value()
   * @model
   * @generated
   */
  int getValue();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.IntegerOutputDef#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(int value);

} // IntegerOutputDef
