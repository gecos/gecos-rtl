/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.Block;
import fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;
import fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock;

import fr.irisa.cairn.model.datapath.port.InControlPort;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl#getChoice <em>Choice</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl#getCases <em>Cases</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.SwitchBlockImpl#getDefault <em>Default</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SwitchBlockImpl extends BlockImpl implements SwitchBlock
{
  /**
   * The cached value of the '{@link #getChoice() <em>Choice</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getChoice()
   * @generated
   * @ordered
   */
  protected InControlPort choice;

  /**
   * The cached value of the '{@link #getCases() <em>Cases</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCases()
   * @generated
   * @ordered
   */
  protected EList<CaseBlock> cases;

  /**
   * The cached value of the '{@link #getDefault() <em>Default</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDefault()
   * @generated
   * @ordered
   */
  protected EList<Block> default_;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SwitchBlockImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SequencerDSLPackage.Literals.SWITCH_BLOCK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InControlPort getChoice()
  {
    if (choice != null && choice.eIsProxy())
    {
      InternalEObject oldChoice = (InternalEObject)choice;
      choice = (InControlPort)eResolveProxy(oldChoice);
      if (choice != oldChoice)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, SequencerDSLPackage.SWITCH_BLOCK__CHOICE, oldChoice, choice));
      }
    }
    return choice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InControlPort basicGetChoice()
  {
    return choice;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setChoice(InControlPort newChoice)
  {
    InControlPort oldChoice = choice;
    choice = newChoice;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SequencerDSLPackage.SWITCH_BLOCK__CHOICE, oldChoice, choice));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CaseBlock> getCases()
  {
    if (cases == null)
    {
      cases = new EObjectContainmentEList<CaseBlock>(CaseBlock.class, this, SequencerDSLPackage.SWITCH_BLOCK__CASES);
    }
    return cases;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Block> getDefault()
  {
    if (default_ == null)
    {
      default_ = new EObjectContainmentEList<Block>(Block.class, this, SequencerDSLPackage.SWITCH_BLOCK__DEFAULT);
    }
    return default_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SWITCH_BLOCK__CASES:
        return ((InternalEList<?>)getCases()).basicRemove(otherEnd, msgs);
      case SequencerDSLPackage.SWITCH_BLOCK__DEFAULT:
        return ((InternalEList<?>)getDefault()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SWITCH_BLOCK__CHOICE:
        if (resolve) return getChoice();
        return basicGetChoice();
      case SequencerDSLPackage.SWITCH_BLOCK__CASES:
        return getCases();
      case SequencerDSLPackage.SWITCH_BLOCK__DEFAULT:
        return getDefault();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SWITCH_BLOCK__CHOICE:
        setChoice((InControlPort)newValue);
        return;
      case SequencerDSLPackage.SWITCH_BLOCK__CASES:
        getCases().clear();
        getCases().addAll((Collection<? extends CaseBlock>)newValue);
        return;
      case SequencerDSLPackage.SWITCH_BLOCK__DEFAULT:
        getDefault().clear();
        getDefault().addAll((Collection<? extends Block>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SWITCH_BLOCK__CHOICE:
        setChoice((InControlPort)null);
        return;
      case SequencerDSLPackage.SWITCH_BLOCK__CASES:
        getCases().clear();
        return;
      case SequencerDSLPackage.SWITCH_BLOCK__DEFAULT:
        getDefault().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.SWITCH_BLOCK__CHOICE:
        return choice != null;
      case SequencerDSLPackage.SWITCH_BLOCK__CASES:
        return cases != null && !cases.isEmpty();
      case SequencerDSLPackage.SWITCH_BLOCK__DEFAULT:
        return default_ != null && !default_.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //SwitchBlockImpl
