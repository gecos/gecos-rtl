/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getStates <em>States</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSimpleBlock()
 * @model
 * @generated
 */
public interface SimpleBlock extends Block
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSimpleBlock_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>States</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>States</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>States</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSimpleBlock_States()
   * @model containment="true"
   * @generated
   */
  EList<AbstractState> getStates();

} // SimpleBlock
