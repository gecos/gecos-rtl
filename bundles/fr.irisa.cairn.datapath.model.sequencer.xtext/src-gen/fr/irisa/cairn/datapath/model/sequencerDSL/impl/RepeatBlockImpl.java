/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL.impl;

import fr.irisa.cairn.datapath.model.sequencerDSL.Block;
import fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Repeat Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl#getIter <em>Iter</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.impl.RepeatBlockImpl#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RepeatBlockImpl extends BlockImpl implements RepeatBlock
{
  /**
   * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMode()
   * @generated
   * @ordered
   */
  protected static final String MODE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMode()
   * @generated
   * @ordered
   */
  protected String mode = MODE_EDEFAULT;

  /**
   * The default value of the '{@link #getIter() <em>Iter</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIter()
   * @generated
   * @ordered
   */
  protected static final int ITER_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getIter() <em>Iter</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIter()
   * @generated
   * @ordered
   */
  protected int iter = ITER_EDEFAULT;

  /**
   * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBlocks()
   * @generated
   * @ordered
   */
  protected EList<Block> blocks;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RepeatBlockImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SequencerDSLPackage.Literals.REPEAT_BLOCK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMode()
  {
    return mode;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMode(String newMode)
  {
    String oldMode = mode;
    mode = newMode;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SequencerDSLPackage.REPEAT_BLOCK__MODE, oldMode, mode));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getIter()
  {
    return iter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIter(int newIter)
  {
    int oldIter = iter;
    iter = newIter;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SequencerDSLPackage.REPEAT_BLOCK__ITER, oldIter, iter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Block> getBlocks()
  {
    if (blocks == null)
    {
      blocks = new EObjectContainmentEList<Block>(Block.class, this, SequencerDSLPackage.REPEAT_BLOCK__BLOCKS);
    }
    return blocks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.REPEAT_BLOCK__BLOCKS:
        return ((InternalEList<?>)getBlocks()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.REPEAT_BLOCK__MODE:
        return getMode();
      case SequencerDSLPackage.REPEAT_BLOCK__ITER:
        return getIter();
      case SequencerDSLPackage.REPEAT_BLOCK__BLOCKS:
        return getBlocks();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.REPEAT_BLOCK__MODE:
        setMode((String)newValue);
        return;
      case SequencerDSLPackage.REPEAT_BLOCK__ITER:
        setIter((Integer)newValue);
        return;
      case SequencerDSLPackage.REPEAT_BLOCK__BLOCKS:
        getBlocks().clear();
        getBlocks().addAll((Collection<? extends Block>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.REPEAT_BLOCK__MODE:
        setMode(MODE_EDEFAULT);
        return;
      case SequencerDSLPackage.REPEAT_BLOCK__ITER:
        setIter(ITER_EDEFAULT);
        return;
      case SequencerDSLPackage.REPEAT_BLOCK__BLOCKS:
        getBlocks().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SequencerDSLPackage.REPEAT_BLOCK__MODE:
        return MODE_EDEFAULT == null ? mode != null : !MODE_EDEFAULT.equals(mode);
      case SequencerDSLPackage.REPEAT_BLOCK__ITER:
        return iter != ITER_EDEFAULT;
      case SequencerDSLPackage.REPEAT_BLOCK__BLOCKS:
        return blocks != null && !blocks.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (mode: ");
    result.append(mode);
    result.append(", iter: ");
    result.append(iter);
    result.append(')');
    return result.toString();
  }

} //RepeatBlockImpl
