/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.datapath.port.InControlPort;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Switch Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getChoice <em>Choice</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getCases <em>Cases</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getDefault <em>Default</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSwitchBlock()
 * @model
 * @generated
 */
public interface SwitchBlock extends Block
{
  /**
   * Returns the value of the '<em><b>Choice</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Choice</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Choice</em>' reference.
   * @see #setChoice(InControlPort)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSwitchBlock_Choice()
   * @model
   * @generated
   */
  InControlPort getChoice();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock#getChoice <em>Choice</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Choice</em>' reference.
   * @see #getChoice()
   * @generated
   */
  void setChoice(InControlPort value);

  /**
   * Returns the value of the '<em><b>Cases</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Cases</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Cases</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSwitchBlock_Cases()
   * @model containment="true"
   * @generated
   */
  EList<CaseBlock> getCases();

  /**
   * Returns the value of the '<em><b>Default</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.Block}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Default</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Default</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getSwitchBlock_Default()
   * @model containment="true"
   * @generated
   */
  EList<Block> getDefault();

} // SwitchBlock
