/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Immediate Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getImmediateValue()
 * @model
 * @generated
 */
public interface ImmediateValue extends EObject
{
} // ImmediateValue
