/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Jump</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getTarget <em>Target</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getPredicate <em>Predicate</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getJump()
 * @model
 * @generated
 */
public interface Jump extends EObject
{
  /**
   * Returns the value of the '<em><b>Target</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Target</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Target</em>' reference.
   * @see #setTarget(SimpleBlock)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getJump_Target()
   * @model
   * @generated
   */
  SimpleBlock getTarget();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getTarget <em>Target</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Target</em>' reference.
   * @see #getTarget()
   * @generated
   */
  void setTarget(SimpleBlock value);

  /**
   * Returns the value of the '<em><b>Predicate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Predicate</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Predicate</em>' containment reference.
   * @see #setPredicate(AbstractBooleanExpression)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getJump_Predicate()
   * @model containment="true"
   * @generated
   */
  AbstractBooleanExpression getPredicate();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.Jump#getPredicate <em>Predicate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Predicate</em>' containment reference.
   * @see #getPredicate()
   * @generated
   */
  void setPredicate(AbstractBooleanExpression value);

} // Jump
