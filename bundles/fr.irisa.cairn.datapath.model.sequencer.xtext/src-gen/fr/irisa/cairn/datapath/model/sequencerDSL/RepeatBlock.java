/**
 */
package fr.irisa.cairn.datapath.model.sequencerDSL;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repeat Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getMode <em>Mode</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getIter <em>Iter</em>}</li>
 *   <li>{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getRepeatBlock()
 * @model
 * @generated
 */
public interface RepeatBlock extends Block
{
  /**
   * Returns the value of the '<em><b>Mode</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Mode</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Mode</em>' attribute.
   * @see #setMode(String)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getRepeatBlock_Mode()
   * @model
   * @generated
   */
  String getMode();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getMode <em>Mode</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Mode</em>' attribute.
   * @see #getMode()
   * @generated
   */
  void setMode(String value);

  /**
   * Returns the value of the '<em><b>Iter</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Iter</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Iter</em>' attribute.
   * @see #setIter(int)
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getRepeatBlock_Iter()
   * @model
   * @generated
   */
  int getIter();

  /**
   * Sets the value of the '{@link fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock#getIter <em>Iter</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Iter</em>' attribute.
   * @see #getIter()
   * @generated
   */
  void setIter(int value);

  /**
   * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
   * The list contents are of type {@link fr.irisa.cairn.datapath.model.sequencerDSL.Block}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Blocks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Blocks</em>' containment reference list.
   * @see fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage#getRepeatBlock_Blocks()
   * @model containment="true"
   * @generated
   */
  EList<Block> getBlocks();

} // RepeatBlock
