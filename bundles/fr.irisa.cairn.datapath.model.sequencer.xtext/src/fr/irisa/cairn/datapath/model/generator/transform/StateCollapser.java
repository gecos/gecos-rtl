package fr.irisa.cairn.datapath.model.generator.transform;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.bdd.analysis.BDDOptimizer;
import fr.irisa.cairn.model.datapath.custom.LogicalOperations;
import fr.irisa.cairn.model.datapath.user.factory.UserFSMFactory;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;

public class StateCollapser  {

	private static final boolean VERBOSE = false;

	List<Transition> predecessors;
	List<Transition> successors;
	UserFSMFactory factory;

	public StateCollapser() {
		factory = UserFSMFactory.getFactory();
	}
	
	public void collapseState(State target) {
		int cid=0;
		FSM fsm = target.getParent();
		 
		/** build pred and succ list */
		buildPredecessorList(target);
		buildSuccessorList(target); 
		
		/** merge all paths pred->target->succ by merging the 2 transition 
		 * into a single one (we take the logical and of both predicates)**/
		for (Transition succ : successors) {
			for (Transition pred : predecessors) {
				mergeTransition(""+cid++, succ, pred);
			}
		}
		
		for (Transition pred2target : predecessors) {
			debug("\nRemoving transition "+pred2target+ " from state "+pred2target.getSrc()+"\n");
			pred2target.getSrc().getTransitions().remove(pred2target);
		}
		
		debug("\nRemoving state "+target+ " from FSM "+fsm.getName()+"\n");
		fsm.getStates().remove(target);
	}

	/**
	 * Merge two transition into a single one, by doing a logical and of their predicate
	 * @param newName : newly created transition name
	 * @param target2succ : incoming transition
	 * @param pred2target : outgoing transition
	 */
	private Transition mergeTransition(String newName, Transition target2succ, Transition pred2target) {
		debug("	Merging Transition "+pred2target+" and "+target2succ);
		AbstractBooleanExpression exp = null;
		if (pred2target.getPredicate()==null) {
			if (target2succ.getPredicate()==null) {
				BooleanConstant boolCst = FsmFactory.eINSTANCE.createBooleanConstant();
				boolCst.setValue(true);
				exp = boolCst;
			} else {
				exp = target2succ.getPredicate();
			}
		} else {
			if (target2succ.getPredicate()==null) {
				exp = pred2target.getPredicate();
			} else {
				exp = LogicalOperations.and(pred2target.getPredicate(),target2succ.getPredicate());
			}
		}
		

		
		debug("\t- New predicate "+exp);
		
		BDDOptimizer optimiser = new BDDOptimizer(exp);
		AbstractBooleanExpression optexp=optimiser.simplify();
		debug ("\t- Predicate "+exp+" simplified to "+optexp+"\n");
		
		if (optimiser.isAlwaysFalse()) {
			debug("\t- Collapsed transition trimmed away\n");
			return null;
		} else {
			Transition res= factory.createTransition("clps"+newName, pred2target.getSrc(), target2succ.getDst(),optexp);
			debug("	Resulting transition is "+res);
			return res;
		}
	}


	private void debug(String string) {
		if (VERBOSE) {
			System.out.println(string);
		}
		
	}


	private void buildSuccessorList(State target) {
		debug("Building predecessor list to state "+target+"");
		successors = new ArrayList<Transition>();
		for (Transition t : target.getTransitions()) {
			if (t.getPredicate()==null) {
				throw new RuntimeException();
			}
			successors.add(t);
		}
	}


	private void buildPredecessorList(State target) {
		predecessors = new ArrayList<Transition>();

		debug("Building predecessor list to state "+target+"");
		for (State pred : ((FSM) target.eContainer()).getStates()) {
			for (Transition t : pred.getTransitions()) {
				if (t.getDst()==target) {
					predecessors.add(t);
				}
			} 
		}
	}

}
