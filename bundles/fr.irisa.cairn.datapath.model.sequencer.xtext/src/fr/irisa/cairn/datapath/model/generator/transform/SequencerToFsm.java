package fr.irisa.cairn.datapath.model.generator.transform;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import static fr.irisa.cairn.datapath.model.generator.SequencerPrettyPrinter.pp;

import fr.irisa.cairn.datapath.model.sequencerDSL.AbstractState;
import fr.irisa.cairn.datapath.model.sequencerDSL.Block;
import fr.irisa.cairn.datapath.model.sequencerDSL.CaseBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.Jump;
import fr.irisa.cairn.datapath.model.sequencerDSL.NopState;
import fr.irisa.cairn.datapath.model.sequencerDSL.OutputPortRule;
import fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SetState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SwitchBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.util.SequencerDSLSwitch;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathFactory;
import fr.irisa.cairn.model.datapath.codegen.DottyFSMExport;
import fr.irisa.cairn.model.datapath.custom.LogicalOperations;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.user.factory.UserFSMFactory;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;



public class SequencerToFsm extends SequencerDSLSwitch<EObject>{
	
	private static final boolean DEBUG = true;
	private static final boolean ERROR_STOP = false;
	Sequencer sequencer;
	FSM fsm;
	Datapath container;
	int loopDepth=0;

	String optDottyfile=null;
	String Dottyfile=null;

	private void warning(String mess) {
		System.err.println(mess);
	}

	private void error(String mess) {
		if(ERROR_STOP) {
			throw new RuntimeException("Error :"+mess);
		} else {
			System.err.println(mess);
		}
		
	}
	/* current and previous state */
	private State currentState;
	/* dummy state used to store pending Transitions */
	
	UserFSMFactory factory = UserFSMFactory.getFactory();
	private int StateId =0;

	//private HashMap<String,State> labelMap; 
	
	private List<State> stateList;
	private int BBidx=0;

	private HashMap<SimpleState, State> gotoLists;
	private HashMap<Block, Block> flowMap;
	private HashMap<Block, State> entryMap;
	private HashMap<Block, State> exitMap;
	private String indentString="";
	private int offset;
	private HashMap<String, State> labelMap;



	public SequencerToFsm(Sequencer sequencer, 	String optDottyfile ,String Dottyfile) {
		this(sequencer);
		this.optDottyfile= optDottyfile;
		this.Dottyfile=Dottyfile;
	}
	
	public SequencerToFsm(Sequencer sequencer, 	String optDottyfile) {
		this(sequencer);
		this.optDottyfile= optDottyfile;
	}


	public SequencerToFsm(Sequencer sequencer) {
		this.sequencer=sequencer;
		fsm =factory.createNewFSM(sequencer.getName());
		container= DatapathFactory.eINSTANCE.createDatapath();
		container.setName("datapath");
		container.getComponents().add(fsm);
		
		labelMap = new HashMap<String, State>();
		stateList =new ArrayList<State> ();
		
		gotoLists = new HashMap<SimpleState, State>();
		entryMap = new HashMap<Block, State>();
		exitMap = new HashMap<Block, State>();
		flowMap = new HashMap<Block, Block>();
		//portBuilder = new FSMPortBuilder();
	}


	public Datapath compute() throws FileNotFoundException {

		State start = factory.createState();
		start.setLabel("idle");
		fsm.getStates().add(start);
		fsm.setStart(start);
		currentState=null;

		debug("*********************************");
		debug("  Transforming "+sequencer.getName());
		debug("*********************************");
		
		doSwitch(sequencer);
		
		
		/* If there are some pending Transition, we create an artificial 
		 * final state, for which we target the pendingTransitions.    
		 */
		buildControlFlow();
		
		
		if (Dottyfile!=null) {
			DottyFSMExport a= new DottyFSMExport(fsm);
			a.saveas(Dottyfile);

		}
		
		/* 
		 * Remove all artificial start/exit states
		 */
		
		simplifyControlFlow();
	
 
		/* 
		 * Remove unreachable states
		 */
		RemoveUnreachableStates remover = new RemoveUnreachableStates(fsm);
		remover.compute(); 
		
		/* 
		 * Fix state Identifiers names for VHDL compatibily
		 */
		fixIdentifierNames();
		/* 
		 * Empty predicates are considered always true
		 */
		fixEmptyPredicates();
		
		if (optDottyfile!=null) {
			DottyFSMExport a= new DottyFSMExport(fsm);
			a.saveas(optDottyfile);
		}
		
		return container;
	}

	private void fixEmptyPredicates() {
		for(State state : fsm.getStates()) {
			for(AbstractCommandValue cmd  : state.getActivatedCommands()) {
				if(cmd.getPredicate()==null) {
					BooleanConstant boolCst = FsmFactory.eINSTANCE.createBooleanConstant();
					boolCst.setValue(true);
					cmd.setPredicate(boolCst);
				}
			}
			for(Transition t : state.getTransitions()) {
				if(t.getPredicate()==null) {
					BooleanConstant boolCst = FsmFactory.eINSTANCE.createBooleanConstant();
					boolCst.setValue(true);
					t.setPredicate(boolCst);
				}
			}
			
		}
		
	}

	public Datapath getContainer() {
		return container;
	}

	public FSM getFsm() {
		return fsm;
	}

	public void setFsm(FSM fsm) {
		this.fsm = fsm;
	}

	private void fixIdentifierNames() {
		for(State state : fsm.getStates()) {
			String label = state.getLabel(); 
			while (label.contains("__")) {
				label = label.replace("__", "_");
			}
			state.setLabel(label);
		}

		for(State state : fsm.getStates()) {
			for(Transition t : state.getTransitions()) {
				String label = t.getLabel(); 
				while (label.contains("__")) {
					label = label.replace("__", "_");
				}
				t.setLabel(label);
			}
		}
	}

	private void debug(String mess) {
		if (DEBUG) {
			System.out.println(indentString+mess);
		}
		
	}
	
	@Override
	public EObject caseSequencer(Sequencer object) {
		buildFSMIOPorts(object);
		Block lastBlock = null;
		for (Block block : object.getBlocks()) {
			doSwitch(block);
			incrOffset();
			if (lastBlock!=null) {
				flowMap.put(lastBlock, block);
			}
			lastBlock= block;
		}
		return fsm;
	}

	private void buildFSMIOPorts(Sequencer object) {
		EList<InControlPort> in = object.getIn();
		BasicEList<InControlPort> res =  new BasicEList<InControlPort>();
		res.addAll(in);
		for(InControlPort icp : res) {
			System.out.println("adding :"+icp);
			fsm.getActivate().add(icp);
		}
		EList<OutputPortRule> out = object.getOut();
		for(OutputPortRule opr : out ) {
			fsm.getFlags().add(opr.getPort());
		}
	}

		


	public EObject caseRepeatBlock(RepeatBlock object) {
		List<State> localStateList = new ArrayList<State>(); 
		List<State> unrolledStateList = new ArrayList<State>(); 
		for (Block block : object.getBlocks()) {
			doSwitch(block);
			localStateList.addAll(stateList);
		}
		pushDepth();
		for (int i=0;i<object.getIter();i++) {
			for (State state : localStateList) {
				EcoreUtil.Copier copier = new EcoreUtil.Copier();
				State newState = (State) copier.copy( state);
				copier.copyReferences();
				newState.setLabel(state.getLabel()+"_"+i);
				unrolledStateList.add(newState);
			}
			
		}
		popDepth();
		stateList = localStateList;
		return null;
	}

	 private boolean hasJump(AbstractState state) {
		 if (state instanceof SimpleState) {
			 return ((SimpleState)state).getJump()!=null;
		 } else {
			 return false;
		 }
	 }

	 
	 /**
	  * Need refactoring to better support conditionnal goto
	  */
	public EObject caseSimpleBlock(SimpleBlock simpleblock) {
		stateList = new ArrayList<State>();
		
		//
		
		int bblabel = BBidx++;
		String label = buildBlockLabel(simpleblock);//"BB_"+BBidx;
		
		simpleblock.setName(label);
		
		debug("Creating new Basic Block "+bblabel+ " label " +label);

		State start = buildEntryState(simpleblock,label);
		State exit = buildExitState(simpleblock,label);

		
		if (simpleblock.getStates().size()!=1) {
			throw new UnsupportedOperationException("Simple block hjs too many states in Sequencer "+sequencer.getName());
		} else {
		
			AbstractState abstractState = simpleblock.getStates().get(0);
			
			String stateLabel= label+"_"+offset++;
			debug("\t- Creating State "+stateLabel);
			
			currentState = (State) doSwitch(abstractState);
			currentState.setLabel(stateLabel);

			labelMap.put(simpleblock.getName(), currentState);
			
			factory.createTransition(
						start.getLabel()+"->"+ 
						currentState.getLabel(), 
						start,currentState);

			if (hasJump(abstractState)) {
				SimpleState ss = (SimpleState) abstractState;
				AbstractBooleanExpression predicate = ss.getJump().getPredicate();
				if(predicate!=null) {
					factory.createTransition(
							currentState.getLabel()+"->"+ 
							exit.getLabel(), 
							currentState,exit,LogicalOperations.not(predicate));
				}
			} else  {
				factory.createTransition(
					currentState.getLabel()+"->"+ 
					exit.getLabel(), 
					currentState,exit);
			}
			
			return super.caseSimpleBlock(simpleblock);
		}
		

	}



	private String buildBlockLabel(Block block) {
		String label ;
		if (block instanceof SimpleBlock) {
			label =((SimpleBlock) block).getName();
			if (label!=null) {
				while (!isUnique(label)) {
					label = "l"+label;
				}
			} else {
				label = "BB_"+getIndex();
			}
		} else {
			label = block.eClass().getName()+"_"+getIndex();
		}
		return label;
	}

	@Override
	public EObject caseNopState(NopState ss) {
		return buildState(ss);
	}



	@Override
	public EObject caseSetState(SetState ss) {
		State state = buildState(ss);
		EList<AbstractCommandValue> commands = ss.getCommands();
		state.getActivatedCommands().addAll(commands);

		for(AbstractCommandValue abscmd : commands) {
			Copier copier = new Copier();
			AbstractCommandValue copy =(AbstractCommandValue) copier.copy(abscmd);
			copier.copyReferences();
			state.getActivatedCommands().add(copy);
		}
		return state;
	}	
	

	private State buildState(SimpleState ss) {
		State state;
		SimpleBlock sb = (SimpleBlock) ss.eContainer();
		String name  = sb.getName()+"_"+(StateId ++); 
		state = factory.createSimpleState(name);
		if (ss.getJump()!=null) {
			gotoLists.put(ss,state);
		}
		stateList.add(state); 
		return state;
	}
	
	private void fixupGoto() {
		debug("** Fixing up goto");
		for (AbstractState ss : gotoLists.keySet()) {
			Jump jump = ((SimpleState)ss).getJump();
			SimpleBlock target = (SimpleBlock) jump.getTarget();
			State src = gotoLists.get(ss);
			State dest = entryMap.get(target);

			debug("** Fixing up "+pp(src)+" goto "+pp(dest));
			factory.createTransition("goto "+dest.getLabel(), src, dest, LogicalOperations.clone(jump.getPredicate()));
		}
	}

	private boolean isUnique(String name) {
		for (State state : fsm.getStates()) {
			if (state.getLabel().equals(name)) {
				return false;
			}
		}
		return true;
	}

	
	@Override
	public EObject caseIfBlock(IfBlock ifBlock) {
		AbstractBooleanExpression predicate = ifBlock.getCond();
		String ifBlockLabel = buildBlockLabel(ifBlock);
		debug("\nCreating new If ("+predicate+") Block :"+ifBlockLabel+":");
		/*
		 *  Creating then branch
		 */
		State ifBlockEntryState = buildEntryState(ifBlock,ifBlockLabel);

		Block lastThenBlock = buildBlocks(ifBlock.getThen());
		incrOffset();

		State exit = buildExitState(ifBlock,ifBlockLabel);

		Block firstThenBlock = ifBlock.getThen().get(0);
		State thenEntry = entryMap.get(firstThenBlock);
		State thenExit = exitMap.get(lastThenBlock);

		
		/* Creates the "if " transition from the start state to the first one in the then body */ 
		AbstractBooleanExpression thenPredicate = (ifBlock.getCond()); 

		Copier copier = new Copier();
		AbstractBooleanExpression thenPredCopy =(AbstractBooleanExpression) copier.copy(thenPredicate);
		copier.copyReferences();
		
		
		factory.createTransition(ifBlockLabel+"_start2then", ifBlockEntryState, thenEntry,thenPredCopy);
		/* Creates the transition that goes to the last then body state to the exit state*/ 
		factory.createTransition(ifBlockLabel+"_then2exit", thenExit, exit);

		copier = new Copier();
		thenPredCopy =(AbstractBooleanExpression) copier.copy(thenPredicate);
		copier.copyReferences();
		AbstractBooleanExpression elsePredicate = LogicalOperations.not((thenPredCopy)); 

		if (ifBlock.getElse()!=null && !ifBlock.getElse().isEmpty()) {
			Block lastElseBlock = buildBlocks(ifBlock.getElse());
			incrOffset();
			Block firstElseBlock = ifBlock.getElse().get(0);
			State elseEntry = entryMap.get(firstElseBlock);
			State elseExit = exitMap.get(lastElseBlock);
			/* Creates the transition that takes the else branch when predicate is false*/ 
			factory.createTransition(ifBlockLabel+"_start2else", ifBlockEntryState, elseEntry,elsePredicate);
			/* Creates the transition that goes to the last body state to the exit loop sate*/ 
			factory.createTransition(ifBlockLabel+"_else2exit", elseExit, exit);
			
		} else {
			/* If there is no else statement we exit */ 
			factory.createTransition(ifBlockLabel+"_start2exit", ifBlockEntryState, thenExit,elsePredicate);
		}
		


		// State list
		
		debug("End of If Block "+ifBlockLabel+"\n");

		return null;
	}

	@Override
	public EObject caseSwitchBlock(SwitchBlock switchBlock) {
		String switchBlockLabel = buildBlockLabel(switchBlock);
		State switchBlkEntryState = buildEntryState(switchBlock,switchBlockLabel);
		State switchBlkExitState = buildExitState(switchBlock,switchBlockLabel);
		InControlPort icp = switchBlock.getChoice();
		debug("\nCreating new Switch ("+icp+") Block :"+switchBlockLabel+":");
		AbstractBooleanExpression defaultPredicate = null;
		
		for (CaseBlock caseBlkc : switchBlock.getCases()) {
			AbstractBooleanExpression predicate = factory.T(icp,caseBlkc.getValue());
			if (defaultPredicate==null) {
				defaultPredicate = factory.nT(icp,caseBlkc.getValue());
			} else {
				defaultPredicate = factory.and(factory.nT(icp,caseBlkc.getValue()),defaultPredicate);
			}
			/*
			 *  Creating case branch
			 */
			Block lastCaseBl = buildBlocks(caseBlkc.getBlocks());
			incrOffset();

			Block firstCaseBranchBlock = caseBlkc.getBlocks().get(0);
			State caseBranchEntry = entryMap.get(firstCaseBranchBlock);
			State caseBranchExit = exitMap.get(lastCaseBl);
			factory.createTransition(switchBlockLabel+"_switch2case_"+icp, switchBlkEntryState, caseBranchEntry,predicate);
			factory.createTransition(switchBlockLabel+"_case2exit", caseBranchExit, switchBlkExitState);
		}

		Block lastCaseBl = buildBlocks(switchBlock.getDefault());
		
		Block firstDefaultBranchBlock = switchBlock.getDefault().get(0);
		State defaultBranchEntry = entryMap.get(firstDefaultBranchBlock);
		State defaultBranchExit = exitMap.get(lastCaseBl);
		factory.createTransition(switchBlockLabel+"_switch2default", switchBlkEntryState, defaultBranchEntry,defaultPredicate);
		factory.createTransition(switchBlockLabel+"_default2exit", defaultBranchExit, switchBlkExitState);



		// State list
		
		debug("End of If Block "+switchBlockLabel+"\n");

		return null;
	}


	private Block buildBlocks( EList<Block> blocks) {
		Block lastBlock = null;
		pushDepth();
		for (Block block : blocks) {
			doSwitch(block);
			incrOffset();
			if (lastBlock!=null) {
				flowMap.put(lastBlock, block);
			}
			lastBlock= block;
			offset++;
		}
		popDepth();
		return lastBlock;
	}


	public EObject caseWhileBlock(WhileBlock whileBlock) {
		/* Ajouter une transition portant sur la conditon 
		 * du while() {...} et partant du dernier State construit. 
		 */
		AbstractBooleanExpression predicate = (whileBlock.getPred());
		String label = buildBlockLabel(whileBlock);

		debug("\nCreating new While ("+predicate+") Block \n"+label);

		State start = buildEntryState(whileBlock,label);
		
		
		EList<Block> bodyBlocks = whileBlock.getBlocks();
		Block lastBlock  = buildBlocks(bodyBlocks);
		
		State exit = buildExitState(whileBlock,label);
		Block firstBlock = bodyBlocks.get(0);
		State bodyEntry = entryMap.get(firstBlock);
		State bodyExit = exitMap.get(lastBlock);

		
		/* Creates the "test" transition from the start state to the first one in the body */ 
		AbstractBooleanExpression loopPredicate = (whileBlock.getPred()); 
		if(loopPredicate==null) throw new UnsupportedOperationException("NPE");
		factory.createTransition(label+"_start2body", start, bodyEntry,loopPredicate);
		debug("\t"+label+"_start2body:"+loopPredicate+"\n");

		/* Creates the transition that skips the loop when predicate is false*/ 
		AbstractBooleanExpression dontloopPredicate = LogicalOperations.not((loopPredicate)); 
		factory.createTransition(label+"_start2exit", start, exit,dontloopPredicate);
		debug("\t"+label+"_start2exit:"+dontloopPredicate+"\n");

		/* Creates the transition that goes to the last body state to the start loop sate*/ 
		factory.createTransition(label+"_body2exit", bodyExit, start);

		/* Creates the "loop" transition from the last state to the first one */ 
		//factory.createTransition(label+"_exit2start", exit, start);

		// State list
		debug("End of While Block \n"+label);
		return null;
	}


	private String getIndex() {
		String tmp="";
		for (int i=0;i<offsetStack.size();i++) {
			tmp=tmp+offsetStack.get(i);
		}
		return tmp+"_"+offset;
	}
	
	private int popDepth() {
		indentString=indentString.substring(0,(loopDepth-1)*4);
		offset = offsetStack.pop();
		return loopDepth--;
	}

	private void incrOffset() {
		offset++;
	}

	Stack<Integer> offsetStack = new Stack<Integer>();
	
	private int pushDepth() {
		indentString=indentString+"    ";
		offsetStack.push(offset);
		offset=0;
		loopDepth++;
		return loopDepth;
	}



	private State buildExitState(Block object, String name) {
		State exit= factory.createSimpleState(name+"_exit");
		//System.out.println("Adding entry state "+exit+" in "+exitMap);
		exitMap.put(object,exit);
		return exit;
	}



	private State buildEntryState(Block object,String name) {
		State start = factory.createSimpleState(name+"_entry");
		//System.out.println("Adding entry state "+start+" in "+entryMap);
		entryMap.put(object,start);
		return start;
	}

	public State findEntryState(Block wb) {
		if(!entryMap.containsKey(wb)) throw new UnsupportedOperationException("Block "+wb+" has no entry state in map "+entryMap);
		return entryMap.get(wb);
	}

	public State findExitState(Block wb) {
		if(!exitMap.containsKey(wb)) throw new UnsupportedOperationException("Block "+wb+" has no exit state in map "+exitMap);
		return exitMap.get(wb);
	}

	

	private void buildControlFlow() {

		Block firstblock = sequencer.getBlocks().get(0);
		State start = findEntryState(firstblock );
		factory.createTransition("begin", fsm.getStart(), start);
		int flow_id =0;
		/* Build sequential control flow between blocks */
		for (Block block : flowMap.keySet()) {
			Block bsrc = block;
			Block bdst = flowMap.get((block));
			debug("Linking block "+pp(bsrc)+" to "+pp(bdst));
			State src = findExitState(bsrc);
			debug("\tExit state for Block "+pp(bsrc)+" is "+pp(src));
			State dst = findEntryState(bdst);
			debug("\tExit state for Block "+pp(bdst)+" is "+pp(dst));
			factory.createTransition("flow_"+flow_id, src, dst);
		}
		fixupGoto();
	}
	
	private void simplifyControlFlow() {
		/* Simplify control flow by collapsing away entry and exit states */
		BlockFinder<Object> finder = new BlockFinder<Object>();
		for (Block block : finder.getBlocks(sequencer)) {
			debug("Post-processing block "+pp(block)+"\n");
			State src = findExitState(block);
			debug("\t- Entry state = "+pp(src)+"\n");
			State dst = findEntryState(block);
			debug("\t- Exit state = "+pp(dst)+"\n");
			/** **/ 
			StateCollapser collapser = new StateCollapser();
			collapser.collapseState(src);
			collapser.collapseState(dst);
		}
	}

	private class BlockFinder<T> extends SequencerDSLSwitch<T> {
		List<Block> res ;

		public BlockFinder() {
			res = new ArrayList<Block>();
		}	
		
		public List<Block> getBlocks(Sequencer object) {
			caseSequencer(object);
			return res;
		}
		
		
		@Override
		public T caseSequencer(Sequencer object) {
			for (Block block : object.getBlocks()) {
				doSwitch(block);
			}
			return super.caseSequencer(object);
		}

		@Override
		public T caseRepeatBlock(RepeatBlock object) {
			for (Block block : object.getBlocks()) {
				doSwitch(block);
			}
			res.add(object);
			return super.caseRepeatBlock(object);
		}

		@Override
		public T caseSimpleBlock(SimpleBlock object) {
			res.add(object);
			return super.caseSimpleBlock(object);
		}

		@Override
		public T caseWhileBlock(WhileBlock object) {
			for (Block block : object.getBlocks()) {
				doSwitch(block);
			}
			res.add(object);
			return super.caseWhileBlock(object);
		}

		@Override
		public T caseIfBlock(IfBlock object) {
			doSwitch(object.getCond());
			for (Block block : object.getThen()) {
				doSwitch(block);
			}
			if(object.getElse()!=null) {
				for (Block block : object.getElse()) {
					doSwitch(block);
				}
			}
			res.add(object);
			return super.caseIfBlock(object);
		}
		
	}

}
