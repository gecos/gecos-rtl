package fr.irisa.cairn.datapath.model;

import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractValueConverter;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.impl.AbstractNode;
import org.eclipse.xtext.util.Strings;
import fr.irisa.cairn.model.datapath.operators.*;
import com.google.inject.Inject;
import fr.irisa.cairn.model.fsm.BooleanValue;

public class SequencerDSTerminalConverters extends DefaultTerminalConverters {

	public static class HEXINTValueConverter extends
			AbstractValueConverter<Integer> {

		public String toString(Integer value) {
			if (value == null)
				throw new ValueConverterException(
						"INT-value may not be null. (null indeed, zero is ok)",
						null, null);
			if (value < 0)
				throw new ValueConverterException(
						"INT-value may not be negative. (value:" + value + ").",
						null, null);
			return value.toString();
		}

		public Integer toValue(String string, INode node) {
			if (Strings.isEmpty(string))
				throw new ValueConverterException(
						"Couldn't convert empty string to int.", node, null);
			try {
				return Integer.parseInt(string.substring(2), 16);
			} catch (NumberFormatException e) {
				throw new ValueConverterException("Couldn't convert '" + string
						+ "' to int.", node, e);
			}
		}

	}

	public static class BININTValueConverter extends
			AbstractValueConverter<Integer> {

		public String toString(Integer value) {
			if (value == null)
				throw new ValueConverterException(
						"INT-value may not be null. (null indeed, zero is ok)",
						null, null);
			if (value < 0)
				throw new ValueConverterException(
						"INT-value may not be negative. (value:" + value + ").",
						null, null);
			return value.toString();
		}

		public Integer toValue(String string, INode node) {
			if (Strings.isEmpty(string))
				throw new ValueConverterException(
						"Couldn't convert empty string to int.", node, null);
			try {
				return Integer.parseInt(string.substring(2), 2);
			} catch (NumberFormatException e) {
				throw new ValueConverterException("Couldn't convert '" + string
						+ "' to int.", node, e);
			}
		}

	}

	public static class ThreeValuedLogicValueConverter extends
			AbstractValueConverter<BooleanValue> {

		public String toString(BooleanValue value) {
			switch (value) {
			case ZERO:
				return "false";
			case ONE:
				return "true";
			default:
				return "dontcare";
			}
		}

		public BooleanValue toValue(String string, INode node) {
			if (string.equals("true"))
				return BooleanValue.ONE;
			if (string.equals("false"))
				return BooleanValue.ZERO;
			return BooleanValue.DONT_CARE;
		}

	}

	public static class CompareOpcodeValueConverter extends
			AbstractValueConverter<CompareOpcode> {

		public String toString(CompareOpcode value) {
			return value.toString();
		}

		public CompareOpcode toValue(String string, INode node) {
			if (Strings.isEmpty(string))
				throw new ValueConverterException("Couldn't convert empty string to int.", node, null);

			if (string.equals("<=")) {
				return CompareOpcode.LTE;
			} else if (string.equals(">=")) {
				return CompareOpcode.GTE;
			} else if (string.equals("<")) {
				return CompareOpcode.LT;
			} else if (string.equals(">")) {
				return CompareOpcode.GT;
			} else if (string.equals("==")) {
				return CompareOpcode.EQU;
			} else if (string.equals("!=")) {
				return CompareOpcode.NEQ;
			} else {
				throw new ValueConverterException("Couldn't convert '" + string + " to CompareOpcode", node, null);
			}
		}
	}

	@Inject
	private HEXINTValueConverter hexintValueConverter;

	@ValueConverter(rule = "HEXINT")
	public IValueConverter<Integer> HEXINT() {
		return hexintValueConverter;
	}

	@Inject
	private BININTValueConverter binintValueConverter;

	@ValueConverter(rule = "BININT")
	public IValueConverter<Integer> BININT() {
		return binintValueConverter;
	}

	@Inject
	private ThreeValuedLogicValueConverter threeValuedLogicValueConverter;

	@ValueConverter(rule = "ThreeValuedLogic")
	public IValueConverter<BooleanValue> ThreeValuedLogic() {
		return threeValuedLogicValueConverter;
	}

	@Inject
	private CompareOpcodeValueConverter compareOpcodeValueConverter;

	@ValueConverter(rule = "CompareOpcode")
	
	public IValueConverter<CompareOpcode> CompareOpcode() {
		return compareOpcodeValueConverter;
	}

}
