package fr.irisa.cairn.datapath.model.validation;

import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.validation.Check;

import fr.irisa.cairn.datapath.model.sequencerDSL.*;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
 

public class SequencerDSLJavaValidator extends AbstractSequencerDSLJavaValidator {

	@Check
	public void checkSwitchBlock(SwitchBlock blocck) {
		warning("Switch constructs are not yet supported by the back-end", SequencerDSLPackage.eINSTANCE.getSwitchBlock_Cases());
	}

	@Check
	public void checkRepeatBlock(RepeatBlock blocck) {
		if(blocck.getMode().equals("count"))
			warning("Repeat constructs with counters are not yet supported by the back-end", SequencerDSLPackage.eINSTANCE.getRepeatBlock_Mode());
	}

	@Check
	public void checkWhileBlock(WhileBlock blocck) {
		if(blocck.eContainer() instanceof WhileBlock) {
			WhileBlock cont = (WhileBlock) blocck.eContainer();
			if (cont.getBlocks().get(0)==blocck) {
				error("Perfeclty nested while are not allowed, as the resulting FSM may be inderministic", SequencerDSLPackage.eINSTANCE.getWhileBlock_Blocks());
			}
		}
	}

	@Check
	public void checkSequencer(Sequencer seq) {
		EList<Block> blocks = seq.getBlocks();
		int size = blocks.size();
		
		Block last = blocks.get(size-1);
		if(last instanceof SimpleBlock) {
			AbstractState abstractState = ( (SimpleBlock)last).getStates().get(0);
			if(abstractState instanceof SimpleState) {
				SimpleState ss = (SimpleState) abstractState;
				if (ss.getJump()==null) {
					warning("This is a sink state", SequencerDSLPackage.eINSTANCE.getSimpleState_Jump());
				}
			}
		}
	}
	
	@Check
	public void checkIndexedBooleanFlagTerm(IndexedBooleanFlagTerm seq) {
		if (seq.getOffset()>=seq.getFlag().getWidth()) {
			error("Incomptible size for port "+seq.getFlag().getName()+"", FsmPackage.eINSTANCE.getIndexedBooleanFlagTerm_Offset());
		}
	}
	
	
}
