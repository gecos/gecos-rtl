package fr.irisa.cairn.datapath.model.generator.transform;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;

public class RemoveUnreachableStates {

	EList<State> reachList;
	FSM fsm;
	
	public RemoveUnreachableStates(FSM fsm) {
		reachList = new BasicEList<State>();
		this.fsm=fsm;
	}

	public void compute() {
		EList<State> removeList = new BasicEList<State>();

		visitSuccessors(fsm.getStart());

		for (State current : fsm.getStates()) {
			if(!reachList.contains(current)) {
				removeList.add(current);
			}
		}
		fsm.getStates().removeAll(removeList);

	}
	
	public void visitSuccessors(State state) {
		reachList.add(state);
		for (Transition t : state.getTransitions()) {
			State next = t.getDst();
			if (!reachList.contains(next))
				visitSuccessors(next);
		}
	}
}
