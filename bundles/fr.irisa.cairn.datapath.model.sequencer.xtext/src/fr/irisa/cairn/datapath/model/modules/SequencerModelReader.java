package fr.irisa.cairn.datapath.model.modules;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.SequencerDSLStandaloneSetup;


public class SequencerModelReader {

	
	private static final String FILE_EXT = ".seqdsl";
	private String filename;

	private static Sequencer loadModel(String filename) throws IOException {
		InputStream in = new FileInputStream(filename);
 
		ISetup instance = new fr.irisa.cairn.datapath.model.SequencerDSLStandaloneSetup();
		Injector injector = instance.createInjectorAndDoEMFRegistration();
 
		XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);	
		IResourceFactory factory  = injector.getInstance(IResourceFactory.class);
		XtextResource r = (XtextResource) factory.createResource(URI.createURI("internal.test"));
		rs.getResources().add(r);
		r.load(in,null);
		EcoreUtil.resolveAll(r);
 
 		EObject root = r.getParseResult().getRootASTElement();
 		Sequencer toplevel= (Sequencer) root;
 		return toplevel;
	}

	public SequencerModelReader(String filename) {
		if(filename.endsWith(FILE_EXT))
			this.filename=filename;
		else
			this.filename=filename+FILE_EXT;
	}

	public Sequencer compute() throws IOException {
		Sequencer sequencer  = loadModel(filename);
		return sequencer;
	}
}
