package fr.irisa.cairn.datapath.model.modules;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;



public class SequencerModelWriter {

	private Sequencer sequencer;
	private String filename;

	public SequencerModelWriter(Sequencer sequencer,String filename) {
		this.sequencer=sequencer;
		this.filename=filename;
	}

	public void compute() throws IOException {
		saveAsXML(sequencer, filename);
	}

	public static void saveAsXML(Sequencer seq,String modelfilename) throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());

		resourceSet.getPackageRegistry().put
				(SequencerDSLPackage.eNS_URI, 
					SequencerDSLPackage.eINSTANCE);

		Resource resource = resourceSet.createResource(URI.createFileURI(modelfilename));
		resource.getContents().add(seq);
		resource.save(null); 
	}


}
