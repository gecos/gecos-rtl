package fr.irisa.cairn.datapath.model.generator.transform;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.IResourceFactory;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import fr.irisa.cairn.datapath.model.SequencerDSLStandaloneSetup;
import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;

public class TestTransform {

	private static Sequencer loadModel(String filename) throws IOException {
		InputStream in = new FileInputStream(filename);
 
		ISetup instance = new SequencerDSLStandaloneSetup();
		Injector injector = instance.createInjectorAndDoEMFRegistration();
 
		XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);	
		IResourceFactory factory  = injector.getInstance(IResourceFactory.class);
		XtextResource r = (XtextResource) factory.createResource(URI.createURI("internal.test"));
		rs.getResources().add(r);
		r.load(in,null);
		EcoreUtil.resolveAll(r);
 
 		EObject root = r.getParseResult().getRootASTElement();
 		Sequencer toplevel= (Sequencer) root;
 		return toplevel;
	}

	public static void saveAsXML(Sequencer seq,String modelfilename) throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());

		resourceSet.getPackageRegistry().put
				(SequencerDSLPackage.eNS_URI, 
					SequencerDSLPackage.eINSTANCE);

		Resource resource = resourceSet.createResource(URI.createFileURI(modelfilename));
		resource.getContents().add(seq);
		resource.save(null);
	}

	public static void saveAsText(Sequencer seq,String textfilename) throws IOException {
		ISetup instance = new SequencerDSLStandaloneSetup();
		Injector injector = instance.createInjectorAndDoEMFRegistration();
 
		XtextResourceSet rs = injector.getInstance(XtextResourceSet.class);	
		IResourceFactory factory  = injector.getInstance(IResourceFactory.class);
		XtextResource r = (XtextResource) rs.createResource(URI.createFileURI(textfilename));
		rs.getResources().add(r);
		r.getContents().add(seq);
		r.save(null);
		EcoreUtil.resolveAll(r);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String names[]= new String[]{"example1","example2","example3"};
		try {
			for (String name: names) {
				Sequencer sequencer  = loadModel("tests/src/"+name+".fsmseq");
				SequencerToFsm transform = new SequencerToFsm(sequencer,"tests/dotty/"+name+"_opt.dot","tests/dotty/"+name+".dot");
				Datapath dp= transform.compute();
				DatapathXMLWriter.getXMLWriter().save(dp, "tests/"+name+".datapath");
				saveAsXML(sequencer, "tests/"+name+".sequencerdsl");
				//saveAsText(sequencer, "tests/"+name+"_regen.fsmseq");
				DatapathVHDLGenerator project = new DatapathVHDLGenerator(dp,"tests/",dp.getName());
				project.generate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		

	}

}
