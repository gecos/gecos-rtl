package fr.irisa.cairn.datapath.model.generator;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.Jump;
import fr.irisa.cairn.datapath.model.sequencerDSL.NopState;
import fr.irisa.cairn.datapath.model.sequencerDSL.RepeatBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.Sequencer;
import fr.irisa.cairn.datapath.model.sequencerDSL.SetState;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SimpleState;
import fr.irisa.cairn.datapath.model.sequencerDSL.WhileBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.util.SequencerDSLSwitch;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;

public class SequencerPrettyPrinter extends SequencerDSLSwitch {

	static SequencerPrettyPrinter pp;
	


	@Override
	public Object defaultCase(EObject object) {
		return object.toString();
	}

	public static String pp(EObject object) {
		if(object==null) {
			throw new UnsupportedOperationException("Cannot preotty pint a null object");
		}
		return (String) getInstance().doSwitch(object);
	}
	
	private static SequencerDSLSwitch getInstance() {
		if (pp==null) {
			pp= new SequencerPrettyPrinter();
		}
		return pp;
	}

//	@Override
//	public Object caseBooleanPredicateTermRule(BooleanPredicateTermRule object) {
//		// TODO Auto-generated method stub
//		return super.caseBooleanPredicateTermRule(object);
//	}

	@Override
	public Object caseIfBlock(IfBlock object) {
		return "if ("+pp(object.getCond())+")";
	}



//	@Override
//	public Object caseIntegerPredicateTermRule(IntegerPredicateTermRule object) {
//		// TODO Auto-generated method stub
//		return super.caseIntegerPredicateTermRule(object);
//	}

	@Override
	public Object caseJump(Jump object) {
		// TODO Auto-generated method stub
		return super.caseJump(object);
	}

	@Override
	public Object caseNopState(NopState object) {
		// TODO Auto-generated method stub
		return super.caseNopState(object);
	}


	@Override
	public Object caseRepeatBlock(RepeatBlock object) {
		return "repeat ("+object.getMode()+","+object.getIter()+")";
	}

	@Override
	public Object caseSequencer(Sequencer object) {
		// TODO Auto-generated method stub
		return super.caseSequencer(object);
	}

	@Override
	public Object caseSetState(SetState object) {
		return object.getOpcode()+"()";
	}

	@Override
	public Object caseSimpleBlock(SimpleBlock object) {
		return "SimpleBlock "+object.getName();
	}


	@Override
	public Object caseWhileBlock(WhileBlock object) {
		AbstractBooleanExpression pred = object.getPred();
		if(pred!=null)
			return "While ("+pp(pred)+")";
		else
			return "While (true)";
	}

	private String printAsList(EList list) {
		StringBuffer res = new StringBuffer();
		for (Iterator i = list.iterator(); i.hasNext();) {
			EObject obj = (EObject) i.next();
			res.append(pp(obj));
			if(i.hasNext()) {
				res.append(",");
			}
		}
		return res.toString();
	}
	
}
