package fr.irisa.cairn.datapath.model.generator.transform;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.datapath.model.sequencerDSL.util.SequencerDSLSwitch;


@Deprecated
public class TransitionPredicateBuilder extends SequencerDSLSwitch<EObject> {
	

//	
//	/** 
//	 *  Predicate builder
//	 */
//	private EList<FlagTerm> flagTermList;
//	FSMPortBuilder portBuilder;
//	
//	public TransitionPredicateBuilder(FSMPortBuilder portBuilder) {
//		this.portBuilder=portBuilder;
//	}
//
//	
//	@Override
//	public EObject caseBooleanPredicateTermRule(BooleanPredicateTermRule object) {
//		flagTermList= new BasicEList<FlagTerm>();
//		InControlPort port = portBuilder.getMapping(object.getFlag());
//		if (object.isNegated()) {
//			FlagTerm res= UserFSMFactory.nT(port);
//			flagTermList.add(res);
//			return res;
//		} else {
//			FlagTerm res= UserFSMFactory.T(port);
//			flagTermList.add(res);
//			return res;
//		}
//	}
//
//
//	@Override
//	public EObject  caseIntegerPredicateTermRule(IntegerPredicateTermRule object) {
//		FlagTerm flagTermArray[]= new FlagTerm[object.getFlag().getWidth()];
//		if (object.getEqual().equals("!=")) {
//			for (int i = 0; i < flagTermArray.length; i++) {
//				flagTermArray[i] = UserFSMFactory.nT(portBuilder.getMapping(object.getFlag()));
//			}
//			return UserFSMFactory.or(flagTermArray);
//		} else if (object.getEqual().equals("=")) {
//			for (int i = 0; i < flagTermArray.length; i++) {
//				flagTermArray[i] = UserFSMFactory.T(portBuilder.getMapping(object.getFlag()));
//			}
//			return UserFSMFactory.and(flagTermArray);
//		} else {
//			throw new UnsupportedOperationException("Integer predicate "+object.getEqual()+" not supported");
//		}
//	}
//
//	@Override
//	public EObject caseProductPredicateRule(ProductPredicateRule ppr) {
//		FlagTerm[] pts = new FlagTerm[ppr.getTerms().size()];
//		int i=0;
//		for (PredicateTermRule ptr : ppr.getTerms()) {
//			doSwitch(ptr);
//			for (FlagTerm flagTerm : flagTermList) {
//				if (flagTerm .getFlag()==null){
//					throw new RuntimeException("Empty predicate");
//				}
//				pts[i++]=flagTerm ;
//			}
//		}
//		return UserFSMFactory.and(pts);
//	}
//
//	public EObject caseSumOfProductPredicateRule(
//			SumOfProductPredicateRule sopr) {
//		if (sopr != null) {
//			AbstractBooleanExpression[] pps = new AbstractBooleanExpression[sopr.getProducts().size()];
//			int i = 0;
//			for (ProductPredicateRule ppr : sopr.getProducts()) {
//				pps[i++] = (AbstractBooleanExpression) doSwitch(ppr);
//			}
//			return UserFSMFactory.or(pps);
//		} else {
//			return null;
//		}
//	}
//
//	@Override
//	public EObject caseIndexedBooleanPredicateTermRule(
//			IndexedBooleanPredicateTermRule object) {
//		throw new UnsupportedOperationException("Not yet implemented");
//	}
//
//
//	public AbstractBooleanExpression build(EObject obj) {
//		return (AbstractBooleanExpression) doSwitch(obj);
//	}
//
//	@Override
//	public EObject defaultCase(EObject object) {
//		throw new UnsupportedOperationException("Object type "+
//				object.getClass().getSimpleName()+" not supported by "+this.getClass().getSimpleName()
//				+" Visitor");
//	}
}
