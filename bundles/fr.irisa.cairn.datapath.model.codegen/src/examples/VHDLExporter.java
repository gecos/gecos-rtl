package examples;

import java.io.File;
import java.io.FileNotFoundException;

import org.eclipse.core.resources.IFile;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.MessageBox;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenerator;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class VHDLExporter {

	Display display;
	Shell shell;
	String filename;
	
	private VHDLExporter() {
		display = new Display();
		shell = new Shell(display);
//		shell.setSize(400, 400);
//		shell.setText("Datapath Model VHDL Exporter ");
	
		// TODO Auto-generated method stub
		FileDialog fdsl = new FileDialog(shell.getShell());
		fdsl.setFilterExtensions(new String[] {".datapath"});
		fdsl.setText("Select Datapath Model instance");
		String fileName = fdsl.open();
		DirectoryDialog dlg = new DirectoryDialog(shell.getShell());
		dlg.setText("Select a directory");
		dlg.setMessage("Select a directory for exporting the VHDL file(s)");
		String targetDirname = dlg.open();
		if (targetDirname != null) {
			DatapathXMLReader reader = DatapathXMLReader.getXMLReader();
			Datapath datapath = reader.load(fileName);
			String outfilename  = datapath.getName()+"_gen.vhd";
			try {
				VHDLGenerator.getInstance().export(datapath, targetDirname+"/"+outfilename);
				MessageBox messageBox = new MessageBox(shell.getShell(), SWT.OK);
				messageBox.setMessage("VHDL file "+targetDirname+"/"+outfilename+" successfully generated");
				messageBox.open();
			} catch (FileNotFoundException e) {
				MessageBox messageBox = new MessageBox(shell.getShell(), SWT.OK);
				messageBox.setMessage("Problem during export, coudl not create "+targetDirname+"/"+outfilename);
				messageBox.open();
			}
		}
		display.dispose();

	}

	public static void main(String[] argv) {
		new VHDLExporter();
	}


}
