package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class RgisterAlteraDualPortMemoryWrapper
{
  protected static String nl;
  public static synchronized RgisterAlteraDualPortMemoryWrapper create(String lineSeparator)
  {
    nl = lineSeparator;
    RgisterAlteraDualPortMemoryWrapper result = new RgisterAlteraDualPortMemoryWrapper();
    nl = null;
    return result;
  }

  protected final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl; //$NON-NLS-1$
  protected final String TEXT_1 = NL;
  protected final String TEXT_2 = NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + NL + NL + "ENTITY REGCE_"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_3 = NL + "\tPORT" + NL + "\t(" + NL + "\t  \t\trst: in std_logic; " + NL + "\t  \t\tclk : in std_logic;" + NL + "\t  \t\tce: in std_logic; " + NL + "\t  \t\tD : in std_logic_vector("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
  protected final String TEXT_4 = " downto 0); " + NL + "\t  \t\tQ : out std_logic_vector("; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_5 = " downto 0)" + NL + "\t);" + NL + "END REGCE_"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_6 = ";" + NL + NL + "ARCHITECTURE RTL OF REGCE_"; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_7 = " IS" + NL + NL + "\tSIGNAL reg : std_logic_vector("; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_8 = " DOWNTO 0);" + NL + NL + "BEGIN" + NL + NL + "\tPROCESS (rst,clk)" + NL + "\tBEGIN" + NL + "\t\tIF (rst='1') THEN" + NL + "\t\t\treg <= (others => '0');" + NL + "\t\tELSIF rising_edge(clk) then" + NL + "\t\t\tIF ce='1' THEN" + NL + "\t\t\t\treg <= D;" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + NL + "\tQ <= reg;" + NL + NL + "END rtl;" + NL + "\t\t" + NL; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
 
	//skeleton="../generator.skeleton"
	boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	Datapath datapath = (Datapath) project.getCurrentElement();
	
	int depth=45;
	int width=12;

    stringBuffer.append(TEXT_2);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_3);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_4);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_5);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_6);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_7);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_8);
    return stringBuffer.toString();
  }
}
