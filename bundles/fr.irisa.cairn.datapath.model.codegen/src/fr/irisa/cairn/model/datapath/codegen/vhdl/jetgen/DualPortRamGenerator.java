package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import org.eclipse.emf.ecore.*;

public class DualPortRamGenerator
{
  protected static String nl;
  public static synchronized DualPortRamGenerator create(String lineSeparator)
  {
    nl = lineSeparator;
    DualPortRamGenerator result = new DualPortRamGenerator();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + "USE IEEE.STD_LOGIC_UNSIGNED.ALL;" + NL + "USE IEEE.STD_LOGIC_ARITH.ALL; " + NL + "USE IEEE.NUMERIC_STD.ALL;" + NL + "" + NL + "ENTITY ";
  protected final String TEXT_3 = " IS" + NL + "\tPORT" + NL + "\t(" + NL + "\t\tclk:\t \t\tIN STD_LOGIC;" + NL + "\t\t";
  protected final String TEXT_4 = " " + NL + "\t\trst: IN STD_LOGIC;" + NL + "\t\tre_0:\t\t\t\tIN STD_LOGIC;" + NL + "\t\twe_0:\t\t\t\tIN STD_LOGIC;" + NL + "\t\tre_1:\t\t\t\tIN STD_LOGIC;" + NL + "\t\twe_1:\t\t\t\tIN STD_LOGIC;" + NL + "\t\t";
  protected final String TEXT_5 = NL + "\t\taddress_0:  IN std_logic;" + NL + "\t\t";
  protected final String TEXT_6 = NL + "\t\taddress_0:  IN std_logic_vector (";
  protected final String TEXT_7 = " DOWNTO 0);" + NL + "\t\t";
  protected final String TEXT_8 = NL + "\t\tdataIn_0:\t\t\tIN std_logic_vector (";
  protected final String TEXT_9 = NL + "\t\taddress_1:  IN std_logic;" + NL + "\t\t";
  protected final String TEXT_10 = NL + "\t\taddress_1:  IN std_logic_vector (";
  protected final String TEXT_11 = NL + "\t\tdataIn_1:\t\t\tIN std_logic_vector (";
  protected final String TEXT_12 = " DOWNTO 0);" + NL + "\t\t" + NL + "\t\tdataOut_0:\t\tOUT std_logic_vector (";
  protected final String TEXT_13 = " DOWNTO 0);" + NL + "\t\tdataOut_1:\t\tOUT std_logic_vector (";
  protected final String TEXT_14 = " DOWNTO 0)" + NL + "\t\t";
  protected final String TEXT_15 = " " + NL + "\t\tdataIn:\t\t\t\tIN std_logic_vector (";
  protected final String TEXT_16 = " DOWNTO 0);" + NL + "\t\twrite_address:\t\tIN std_logic_vector (";
  protected final String TEXT_17 = " DOWNTO 0);" + NL + "\t\tread_address0:\t\tIN std_logic_vector (";
  protected final String TEXT_18 = " DOWNTO 0);" + NL + "\t\tread_address1:\t\tIN std_logic_vector (";
  protected final String TEXT_19 = " DOWNTO 0);" + NL + "\t\twe:\t\t\t\t\tIN STD_LOGIC;  " + NL + "\t\tdataOut_0:\t\t\tOUT std_logic_vector (";
  protected final String TEXT_20 = " DOWNTO 0);" + NL + "\t\tdataOut_1:\t\t\tOUT std_logic_vector (";
  protected final String TEXT_21 = " " + NL + "\t);" + NL + "END ENTITY;" + NL + "" + NL + "ARCHITECTURE rtl OF ";
  protected final String TEXT_22 = " IS" + NL + "\t" + NL + "\tTYPE MEM IS ARRAY(0 TO ";
  protected final String TEXT_23 = ") OF std_logic_vector(";
  protected final String TEXT_24 = " DOWNTO 0);" + NL + "" + NL + "\tSIGNAL dpram : MEM;" + NL + "" + NL + "BEGIN" + NL + "\t";
  protected final String TEXT_25 = " " + NL + "\tPROCESS (clk,rst)" + NL + "\tVARIABLE adr : integer RANGE 0 to ";
  protected final String TEXT_26 = ";" + NL + "\tBEGIN" + NL + "\t\tIF rst = '1' THEN" + NL + "\t\t\tdpram <= (OTHERS => \"00000000\");" + NL + "\t\tELSIF (clk'event AND clk = '1') THEN" + NL + "\t\t\tIF (we_0 = '1' and re_0 = '0' and we_1 = '0' and re_1 = '0') THEN" + NL + "\t\t\t\tadr := CONV_INTEGER(address_0);" + NL + "\t\t\t\tdpram(adr) <= dataIn_0;" + NL + "\t\t\tELSIF (we_0 = '0' and re_0 = '1' and we_1 = '0' and re_1 = '0') THEN" + NL + "\t\t\t\tadr := CONV_INTEGER(address_0);" + NL + "\t\t\t\tdataOut_0 <= dpram(adr);" + NL + "\t\t\tELSIF (we_0 = '0' and re_0 = '0' and we_1 = '1' and re_1 = '0') THEN" + NL + "\t\t\t\tadr := CONV_INTEGER(address_1);" + NL + "\t\t\t\tdpram(adr) <= dataIn_1;" + NL + "\t\t\tELSIF (we_0 = '0' and re_0 = '1' and we_1 = '0' and re_1 = '1') THEN" + NL + "\t\t\t\tadr := CONV_INTEGER(address_1);" + NL + "\t\t\t\tdataOut_1 <= dpram(adr);" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + "\t";
  protected final String TEXT_27 = " " + NL + "\t" + NL + "\tPROCESS (clk)" + NL + "\tBEGIN" + NL + "\t\tIF (clk'event AND clk = '1') THEN" + NL + "\t\t\tIF (we = '1') THEN" + NL + "\t\t\t\tdpram(CONV_INTEGER(write_address)) <= dataIn;" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + "\t" + NL + "\tdataOut_0 <= dpram(CONV_INTEGER(read_address0));" + NL + "\tdataOut_1 <= dpram(CONV_INTEGER(read_address1));" + NL + "\t" + NL + "\t";
  protected final String TEXT_28 = " " + NL + "END rtl;" + NL + "\t\t";
  protected final String TEXT_29 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	//skeleton="../generator.skeleton"
	//boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	DualPortRam DPR = (DualPortRam) project.getCurrentElement();
	
	int depth= (int) Math.pow(2,DPR.getAddressPort(0).getWidth());
	int width= DPR.getWritePort(0).getWidth();
	boolean DPR4Xilinx = false;
	String ramName = DPR.getName();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(ramName);
    stringBuffer.append(TEXT_3);
    if (DPR4Xilinx == false) {
    stringBuffer.append(TEXT_4);
    if ((DPR.getAddressPort(0).getWidth())==1) {
    stringBuffer.append(TEXT_5);
    } else{
    stringBuffer.append(TEXT_6);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_8);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_7);
    if ((DPR.getAddressPort(0).getWidth())==1) {
    stringBuffer.append(TEXT_9);
    } else{
    stringBuffer.append(TEXT_10);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_7);
    }
    stringBuffer.append(TEXT_11);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_12);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_13);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_14);
    } else {
    stringBuffer.append(TEXT_15);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_16);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_17);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_18);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_19);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_20);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_14);
    }
    stringBuffer.append(TEXT_21);
    stringBuffer.append(ramName);
    stringBuffer.append(TEXT_22);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_23);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_24);
    if (DPR4Xilinx == false) {
    stringBuffer.append(TEXT_25);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_26);
    } else {
    stringBuffer.append(TEXT_27);
    }
    stringBuffer.append(TEXT_28);
    stringBuffer.append(TEXT_29);
    return stringBuffer.toString();
  }
}
