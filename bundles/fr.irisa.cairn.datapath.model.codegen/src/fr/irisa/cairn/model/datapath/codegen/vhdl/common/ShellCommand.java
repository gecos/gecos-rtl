package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class ShellCommand {

	static boolean verbose=true;

	public static boolean isVerbose() {
		return verbose;
	}

	public static void setVerbose(boolean verbose) {
		ShellCommand.verbose = verbose;
	}

	private static void debug(String s) {
		if (verbose) {
			System.out.println(s);
		}
	}
	
	public static void run(String command) throws IOException {

		String s = null;

			Process p = Runtime.getRuntime().exec(command);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {
				debug(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				if (verbose) {
					System.err.println(s);
				}
			}
	}

}
