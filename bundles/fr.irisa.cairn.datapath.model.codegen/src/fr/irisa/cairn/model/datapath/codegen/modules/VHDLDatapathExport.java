package fr.irisa.cairn.model.datapath.codegen.modules;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.DatapathVHDLGenerator;

@Deprecated
public class VHDLDatapathExport {
	
	String directory;
	Datapath datapath;
	
	public VHDLDatapathExport(Datapath dp, String directory) {
		this.datapath=dp;
		this.directory=directory;
	}
	
	public void compute() {
		
		DatapathVHDLGenerator project = new DatapathVHDLGenerator(datapath,directory,datapath.getName());
		project.generate();
	}

}
