package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import fr.irisa.cairn.model.datapath.pads.*;

public class VHDLDatapathTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLDatapathTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLDatapathTemplate result = new VHDLDatapathTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = " " + NL;
  protected final String TEXT_2 = NL + NL + "library ieee ;" + NL + "use ieee.std_logic_1164.all;" + NL + "use ieee.std_logic_unsigned.all;" + NL + "use ieee.numeric_std.all;" + NL + "" + NL + "entity ";
  protected final String TEXT_3 = " is port (";
  protected final String TEXT_4 = NL + "   ";
  protected final String TEXT_5 = NL + ");" + NL + "end  ";
  protected final String TEXT_6 = ";" + NL + "" + NL + "architecture RTL of  ";
  protected final String TEXT_7 = " is" + NL;
  protected final String TEXT_8 = " ";
  protected final String TEXT_9 = NL + NL + "begin" + NL;
  protected final String TEXT_10 = NL + "\t";
  protected final String TEXT_11 = " <= ";
  protected final String TEXT_12 = " ;" + NL + "\t\t\t";
  protected final String TEXT_13 = NL;
  protected final String TEXT_14 = NL + "\t" + NL + "end RTL;";

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	boolean inlinedMapping = true;
	//boolean systemMonitor = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	Datapath datapath = (Datapath) project.getCurrentElement();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(VHDLPortDefinitionTemplate.create("\n").generate(datapath));
    stringBuffer.append(TEXT_5);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(VHDLSignalDeclarationTemplate.create("\n").generate(datapath));
     
	List<AbstractBlock>  blocks = datapath.getComponents(); 
	for (AbstractBlock block : blocks) {
		if (block instanceof Datapath) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLComponentTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
		if (block instanceof FSM) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLComponentTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
		if (block instanceof AbstractMemory) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLComponentTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
	}
		

    stringBuffer.append(TEXT_9);
     
	blocks = datapath.getComponents(); 
	for (AbstractBlock block : blocks) {
		if (block instanceof Datapath) {
			
    stringBuffer.append(VHDLInstanciationTemplate.create("\n").generate(block));
    
		}
		
		if (block instanceof DataInputPad) {
			DataInputPad pad = (DataInputPad) block;
			OutDataPort oport = pad.getOutput(0);
			InDataPort iport = pad.getAssociatedPort();
			String oport_name = oport.getParentNode().getName()+"_"+oport.getName();
			String iport_name = iport.getName(); 
			
    stringBuffer.append(TEXT_10);
    stringBuffer.append(oport_name);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(iport_name);
    stringBuffer.append(TEXT_12);
    
		}
		
		if (block instanceof DataOutputPad) {
			DataOutputPad pad = (DataOutputPad) block;
			InDataPort iport = pad.getInput(0);
			OutDataPort oport = pad.getAssociatedPort();
			String iport_name = iport.getParentNode().getName()+"_"+iport.getName();
			String oport_name = oport.getName(); 
			
    stringBuffer.append(TEXT_10);
    stringBuffer.append(oport_name);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(iport_name);
    stringBuffer.append(TEXT_12);
    
		}
		if (block instanceof StatusPad) {
			StatusPad pad = (StatusPad) block;
			InControlPort iport = pad.getControlPort(0);
			OutControlPort oport = pad.getAssociatedPort();
			String iport_name = iport.getParentNode().getName()+"_"+iport.getName();
			String oport_name = oport.getName(); 
			
    stringBuffer.append(TEXT_10);
    stringBuffer.append(oport_name);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(iport_name);
    stringBuffer.append(TEXT_12);
    
		}
		if (block instanceof ControlPad) {
			ControlPad pad = (ControlPad) block;
			OutControlPort oport = pad.getFlag();
			InControlPort iport = pad.getAssociatedPort();
			String oport_name = oport.getParentNode().getName()+"_"+oport.getName();
			String iport_name = iport.getName(); 
			
    stringBuffer.append(TEXT_10);
    stringBuffer.append(oport_name);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(iport_name);
    stringBuffer.append(TEXT_12);
    
		}
		
		if (block instanceof FSM) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLInstanciationTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
		if (block instanceof AbstractMemory) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLInstanciationTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
		
		if (block instanceof RSRegister) {
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLInstanciationTemplate.create("\n").generate(block));
    stringBuffer.append(TEXT_8);
    
		}
	}

    stringBuffer.append(TEXT_10);
    stringBuffer.append(VHDLWireMapping.create("\n").generate(datapath) );
    stringBuffer.append(TEXT_13);
    	if (inlinedMapping) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(VHDLInlinedOperatorMapping.create("\n").generate(datapath) );
    
	} else {
		throw new UnsupportedOperationException();
	}

    stringBuffer.append(TEXT_10);
    stringBuffer.append(TEXT_13);
    stringBuffer.append(VHDLRegisterMapping.create("\n").generate(datapath) );
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_13);
    return stringBuffer.toString();
  }
}