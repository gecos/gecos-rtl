package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class MulitpleVisitorTemplate
{
  protected static String nl;
  public static synchronized MulitpleVisitorTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    MulitpleVisitorTemplate result = new MulitpleVisitorTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "  inst_";
  protected final String TEXT_2 = " : ";
  protected final String TEXT_3 = " port map(";
  protected final String TEXT_4 = NL + "   ";
  protected final String TEXT_5 = NL + "  );" + NL + "  ";
  protected final String TEXT_6 = NL + "  ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
      
	boolean inlinedMapping = true;
	AbstractBlock comp = (AbstractBlock) argument; 

    stringBuffer.append(TEXT_1);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(VHDLPortMappingTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}
