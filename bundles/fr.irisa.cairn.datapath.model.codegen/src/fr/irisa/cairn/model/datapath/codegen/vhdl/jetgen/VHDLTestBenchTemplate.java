package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class VHDLTestBenchTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLTestBenchTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLTestBenchTemplate result = new VHDLTestBenchTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "library ieee ;" + NL + "use ieee.std_logic_1164.all;" + NL + "use ieee.std_logic_unsigned.all;" + NL + "" + NL + "entity ";
  protected final String TEXT_3 = "_TB is" + NL + "end ";
  protected final String TEXT_4 = "_TB " + NL + "" + NL + "architecture RTL of  ";
  protected final String TEXT_5 = " is" + NL + "" + NL + "" + NL + "\t";
  protected final String TEXT_6 = NL + "\t\t" + NL + "\t-- global signals" + NL + "\tsignal clk : std_logic;" + NL + "\tsignal rst : std_logic;" + NL + "\t-- input Control Ports" + NL + "\t" + NL + "\t";
  protected final String TEXT_7 = "\tsignal ";
  protected final String TEXT_8 = " : std_logic;" + NL + "\t\t";
  protected final String TEXT_9 = " : std_logic_vector(";
  protected final String TEXT_10 = " downto 0);" + NL + "\t\t";
  protected final String TEXT_11 = NL + "\t-- Input Data Ports " + NL + "\t" + NL + "\t";
  protected final String TEXT_12 = NL + "\t-- output Data Ports" + NL + "\t";
  protected final String TEXT_13 = NL + "\t-- output Control Ports" + NL + "\t";
  protected final String TEXT_14 = NL;
  protected final String TEXT_15 = NL + NL + "begin" + NL + "" + NL + "\tDUT : ";
  protected final String TEXT_16 = " port map (" + NL + "\t\t-- global signals" + NL + "\t\tclk,rst," + NL + "\t\t-- input Control Ports" + NL + "\t\t";
  protected final String TEXT_17 = "\t";
  protected final String TEXT_18 = ", ";
  protected final String TEXT_19 = NL + "\t\t" + NL + "\t);" + NL + "" + NL + "\tclk <= not clk after 10 ns; " + NL + "\trst <= '0', '1' after 40 ns, '0' after 120 ns; " + NL + "\t" + NL + "\tprocess(clk)" + NL + "\tbegin" + NL + "\t\twait until rising_edge(clk);" + NL + "\t\twait until rising_edge(clk);" + NL + "\tend proces;" + NL + "end RTL;";

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	boolean inlinedMapping = true;
	Datapath datapath = (Datapath) argument; 
 

    stringBuffer.append(TEXT_2);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(VHDLComponentTemplate.create("\n").generate(datapath));
    stringBuffer.append(TEXT_6);
     for (Iterator i = datapath.getActivate().iterator(); i.hasNext(); ) {
		InControlPort oport = (InControlPort) i.next();
		if (oport.getWidth()==1) {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    } else {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_9);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_10);
    
		}
	}
	
    stringBuffer.append(TEXT_11);
     for (Iterator i = datapath.getIn().iterator(); i.hasNext(); ) {
		InDataPort iport = (InDataPort) i.next();
		if (iport.getWidth()==1) {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_8);
    } else {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_9);
    stringBuffer.append((iport.getWidth()-1));
    stringBuffer.append(TEXT_10);
    
		}
	}
    stringBuffer.append(TEXT_12);
     for (Iterator i = datapath.getOut().iterator(); i.hasNext(); ) {
		OutDataPort oport = (OutDataPort) i.next();
		if (oport.getWidth()==1) {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    } else {
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_9);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_10);
    
		}
	}
	
    stringBuffer.append(TEXT_13);
     for (Iterator i = datapath.getFlags().iterator(); i.hasNext(); ) {
		OutControlPort oport = (OutControlPort) i.next();
		if (oport.getWidth()==1) {
			
    stringBuffer.append(TEXT_7);
    oport.getName();
    stringBuffer.append(TEXT_8);
    } else {
			
    stringBuffer.append(TEXT_7);
    oport.getName();
    stringBuffer.append(TEXT_9);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_10);
    
		}
	}
	
    stringBuffer.append(TEXT_14);
     /* VHDLComponentDeclarationTemplate.create("\n").generate(datapath.getDataWires()); */ 
    stringBuffer.append(TEXT_15);
    stringBuffer.append(datapath.getName());
    stringBuffer.append(TEXT_16);
    
		for (Iterator i = datapath.getActivate().iterator(); i.hasNext(); ) {
			InControlPort iport = (InControlPort) i.next();
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_18);
    
		}
		for (Iterator i = datapath.getIn().iterator(); i.hasNext(); ) {
			InDataPort iport = (InDataPort) i.next();
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_18);
    
		}
		for (Iterator i = datapath.getOut().iterator(); i.hasNext(); ) {
			OutDataPort oport = (OutDataPort) i.next();
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_18);
    
		}
		for (Iterator i = datapath.getFlags().iterator(); i.hasNext(); ) {
			OutControlPort oport = (OutControlPort) i.next();
			
    stringBuffer.append(TEXT_17);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_18);
    
		}
		
    stringBuffer.append(TEXT_19);
    stringBuffer.append(TEXT_14);
    return stringBuffer.toString();
  }
}