package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class VHDLPortMappingTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLPortMappingTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLPortMappingTemplate result = new VHDLPortMappingTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t";
  protected final String TEXT_2 = NL + "\t-- global signals" + NL + "\t\t";
  protected final String TEXT_3 = NL + "\tclk," + NL + "\t\t\t";
  protected final String TEXT_4 = "\t" + NL + "\trst," + NL + "\t\t";
  protected final String TEXT_5 = NL + "\t-- Input Control Ports" + NL + "\t\t";
  protected final String TEXT_6 = NL + "\t";
  protected final String TEXT_7 = "_";
  protected final String TEXT_8 = " ";
  protected final String TEXT_9 = NL + "\t-- Input Data Ports " + NL + "\t\t\t";
  protected final String TEXT_10 = NL + "\t-- output Data Ports" + NL + "\t\t\t";
  protected final String TEXT_11 = NL + "\t-- output Control Ports" + NL + "\t\t";
  protected final String TEXT_12 = NL + "\t\tclk," + NL + "\t\t";
  protected final String TEXT_13 = "\t" + NL + "\t\trst," + NL + "\t\t";
  protected final String TEXT_14 = NL + "\t-- I/O Ports" + NL + "\t\t";
  protected final String TEXT_15 = NL + "\t\tregFile_dataIn_0," + NL + "\t\tregFile_address_0," + NL + "\t\tregFile_address_0," + NL + "\t\tregFile_address_1," + NL + "\t\tregFile_we_0," + NL + "\t\tregFile_dataOut_0," + NL + "\t\tregFile_dataOut_1" + NL + "\t\t\t";

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
	boolean inlinedMapping = true;
	AbstractBlock block = (AbstractBlock) argument;
	boolean DPR4Xilinx = false;

      if (DPR4Xilinx == false) {
    stringBuffer.append(TEXT_1);
    AbstractBlock component = (AbstractBlock) argument; 
    stringBuffer.append(TEXT_2);
     if (!block.isCombinational()) {
		
    stringBuffer.append(TEXT_3);
     if(!(block instanceof SinglePortRom) && !(block instanceof RSRegister)) 
    stringBuffer.append(TEXT_4);
    
		}
		int lastPort = 0;
		ActivableBlock ablock = null;
		FlagBearerBlock fblock = null;
		DataFlowBlock dblock = null;
		String name = block.getName();
	
		if (block instanceof ActivableBlock) {
			ablock = (ActivableBlock) block;
			lastPort+=ablock.getActivate().size();
		}
		if (block instanceof FlagBearerBlock) {
			fblock = (FlagBearerBlock) block;
			lastPort+=fblock.getFlags().size();
		}
		if (block instanceof DataFlowBlock) {
			dblock = (DataFlowBlock) block;
			lastPort+=dblock.getIn().size()+dblock.getOut().size();
		}
	
		int currentPort= 1;
		
    stringBuffer.append(TEXT_5);
     
		if (block instanceof ActivableBlock) {
			for (Iterator i = ablock.getActivate().iterator(); i.hasNext(); ) {
				InControlPort oport = (InControlPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    				if (currentPort!=lastPort) {
				
    stringBuffer.append(",\n");
    
				} else {
				
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
			
    stringBuffer.append(TEXT_9);
     
			if (block instanceof DataFlowBlock) {
			for (Iterator i = dblock.getIn().iterator(); i.hasNext(); ) {
				InDataPort iport = (InDataPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_8);
    				if (currentPort!=lastPort) {
					
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
			
    stringBuffer.append(TEXT_10);
     
			for (Iterator i = dblock.getOut().iterator(); i.hasNext(); ) {
				OutDataPort oport = (OutDataPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    					if (currentPort!=lastPort) {
				
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
		}
		
    stringBuffer.append(TEXT_11);
     
		if (block instanceof FlagBearerBlock) {
			for (Iterator i = fblock.getFlags().iterator(); i.hasNext(); ) {
				OutControlPort oport = (OutControlPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    				if (currentPort!=lastPort) {
					
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
		}
	}
}	else {
    stringBuffer.append(TEXT_6);
    AbstractBlock component = (AbstractBlock) argument; 
    stringBuffer.append(TEXT_2);
     if (!block.isCombinational()) {
		
    stringBuffer.append(TEXT_12);
     		if(!(block instanceof SinglePortRom) && !(block instanceof DualPortRam)) 
    stringBuffer.append(TEXT_13);
    
		}
		int lastPort = 0;
		ActivableBlock ablock = null;
		FlagBearerBlock fblock = null;
		DataFlowBlock dblock = null;
		String name = block.getName();
	
		if (block instanceof ActivableBlock) {
			ablock = (ActivableBlock) block;
			lastPort+=ablock.getActivate().size();
		}
		if (block instanceof FlagBearerBlock) {
			fblock = (FlagBearerBlock) block;
			lastPort+=fblock.getFlags().size();
		}
		if (block instanceof DataFlowBlock) {
			dblock = (DataFlowBlock) block;
			lastPort+=dblock.getIn().size()+dblock.getOut().size();
		}
	
		int currentPort= 1;
		
    stringBuffer.append(TEXT_14);
     
		if (block instanceof ActivableBlock) {
			if(block instanceof DualPortRam){
    stringBuffer.append(TEXT_15);
    		}else{
			for (Iterator i = ablock.getActivate().iterator(); i.hasNext(); ) {
				InControlPort oport = (InControlPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    				if (currentPort!=lastPort) {
				
    stringBuffer.append(",\n");
    
				} else {
				
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
			
    stringBuffer.append(TEXT_9);
     
			if (block instanceof DataFlowBlock && !(block instanceof DualPortRam)) {
			for (Iterator i = dblock.getIn().iterator(); i.hasNext(); ) {
				InDataPort iport = (InDataPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_8);
    				if (currentPort!=lastPort) {
					
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
			
    stringBuffer.append(TEXT_10);
     
			for (Iterator i = dblock.getOut().iterator(); i.hasNext(); ) {
				OutDataPort oport = (OutDataPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_8);
    					if (currentPort!=lastPort) {
				
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
		}
		
    stringBuffer.append(TEXT_11);
     
		if (block instanceof FlagBearerBlock) {
			for (Iterator i = fblock.getFlags().iterator(); i.hasNext(); ) {
				OutControlPort oport = (OutControlPort) i.next();
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(name);
    stringBuffer.append(TEXT_7);
    stringBuffer.append(oport.getName());
    				if (currentPort!=lastPort) {
					
    stringBuffer.append(",\n");
    
				} else {
					
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
		}
	}
}}	
    return stringBuffer.toString();
  }
}