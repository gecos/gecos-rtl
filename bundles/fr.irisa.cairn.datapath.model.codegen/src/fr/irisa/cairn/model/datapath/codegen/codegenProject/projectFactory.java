/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage
 * @generated
 */
public interface projectFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	projectFactory eINSTANCE = fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Code Generation Project</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Generation Project</em>'.
	 * @generated
	 */
	CodeGenerationProject createCodeGenerationProject();

	/**
	 * Returns a new object of class '<em>Cyclone II</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cyclone II</em>'.
	 * @generated
	 */
	CycloneII createCycloneII();

	/**
	 * Returns a new object of class '<em>Stratix II</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stratix II</em>'.
	 * @generated
	 */
	StratixII createStratixII();

	/**
	 * Returns a new object of class '<em>Stratix III</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stratix III</em>'.
	 * @generated
	 */
	StratixIII createStratixIII();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	projectPackage getprojectPackage();

} //projectFactory
