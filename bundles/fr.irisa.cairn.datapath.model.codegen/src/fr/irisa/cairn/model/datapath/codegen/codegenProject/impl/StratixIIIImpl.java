/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject.impl;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixIII;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stratix III</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StratixIIIImpl extends AbstractTargetImpl implements StratixIII {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StratixIIIImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return projectPackage.Literals.STRATIX_III;
	}

} //StratixIIIImpl
