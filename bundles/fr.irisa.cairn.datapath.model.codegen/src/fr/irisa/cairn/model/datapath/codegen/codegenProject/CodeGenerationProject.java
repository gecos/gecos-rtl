/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.Datapath;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Generation Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDirectory <em>Directory</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDatapath <em>Datapath</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getCurrentElement <em>Current Element</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDevice <em>Device</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getVhdlFiles <em>Vhdl Files</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject()
 * @model
 * @generated
 */
public interface CodeGenerationProject extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_Name()
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validFilename'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Directory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Directory</em>' attribute.
	 * @see #setDirectory(String)
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_Directory()
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validDirectory'"
	 * @generated
	 */
	String getDirectory();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDirectory <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Directory</em>' attribute.
	 * @see #getDirectory()
	 * @generated
	 */
	void setDirectory(String value);

	/**
	 * Returns the value of the '<em><b>Datapath</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datapath</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datapath</em>' containment reference.
	 * @see #setDatapath(Datapath)
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_Datapath()
	 * @model containment="true"
	 * @generated
	 */
	Datapath getDatapath();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDatapath <em>Datapath</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datapath</em>' containment reference.
	 * @see #getDatapath()
	 * @generated
	 */
	void setDatapath(Datapath value);

	/**
	 * Returns the value of the '<em><b>Current Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Element</em>' reference.
	 * @see #setCurrentElement(AbstractBlock)
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_CurrentElement()
	 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validElement'"
	 * @generated
	 */
	AbstractBlock getCurrentElement();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getCurrentElement <em>Current Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Element</em>' reference.
	 * @see #getCurrentElement()
	 * @generated
	 */
	void setCurrentElement(AbstractBlock value);

	/**
	 * Returns the value of the '<em><b>Device</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device</em>' attribute.
	 * @see #setDevice(String)
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_Device()
	 * @model required="true"
	 * @generated
	 */
	String getDevice();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDevice <em>Device</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Device</em>' attribute.
	 * @see #getDevice()
	 * @generated
	 */
	void setDevice(String value);

	/**
	 * Returns the value of the '<em><b>Vhdl Files</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vhdl Files</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vhdl Files</em>' attribute list.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCodeGenerationProject_VhdlFiles()
	 * @model
	 * @generated
	 */
	EList<String> getVhdlFiles();

} // CodeGenerationProject
