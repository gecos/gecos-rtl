package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.fsm.impl.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.port.*;

public class VHDLTestBenchTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLTestBenchTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLTestBenchTemplate result = new VHDLTestBenchTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "library ieee ;" + NL + "use ieee.std_logic_1164.all;" + NL + "use ieee.std_logic_unsigned.all;" + NL + "" + NL + "entity sim_fsm_";
  protected final String TEXT_3 = " is" + NL + "end sim_fsm_";
  protected final String TEXT_4 = "; " + NL + "" + NL + "architecture behavioral of sim_fsm_";
  protected final String TEXT_5 = " is" + NL + "\tcomponent fsm_";
  protected final String TEXT_6 = " is port (" + NL + "\t" + NL + "\t-- global signals" + NL + "\tsignal clk : std_logic;" + NL + "\tsignal rst : std_logic;" + NL + "\t" + NL + "\t-- input Control Ports";
  protected final String TEXT_7 = NL + "\t";
  protected final String TEXT_8 = " : in std_logic;";
  protected final String TEXT_9 = NL + "\t" + NL + "\t-- output ports";
  protected final String TEXT_10 = "\t" + NL + "\t";
  protected final String TEXT_11 = " : out std_logic ";
  protected final String TEXT_12 = " : out std_logic_vector(";
  protected final String TEXT_13 = " downto 0)";
  protected final String TEXT_14 = ";" + NL + "\t\t\t";
  protected final String TEXT_15 = NL + "\t\t\t";
  protected final String TEXT_16 = NL + " " + NL + "  );" + NL + "  end component ;" + NL + "  " + NL + "  signal clk : std_logic := '0';" + NL + "  signal rst : std_logic := '0';" + NL + "  ";
  protected final String TEXT_17 = NL + "\tsignal ";
  protected final String TEXT_18 = " : std_logic := '0';";
  protected final String TEXT_19 = NL + "\t" + NL + "\t";
  protected final String TEXT_20 = "\t" + NL + "\tsignal ";
  protected final String TEXT_21 = " : std_logic_vector(";
  protected final String TEXT_22 = " downto 0);";
  protected final String TEXT_23 = NL;
  protected final String TEXT_24 = NL + NL + "begin" + NL + "" + NL + "\tfsm : fsm_";
  protected final String TEXT_25 = " port map (" + NL + "\t\tclk,rst,";
  protected final String TEXT_26 = ", ";
  protected final String TEXT_27 = ",";
  protected final String TEXT_28 = NL + "\t);" + NL + "" + NL + "\tclock : process " + NL + "\t\tvariable clkTemp : std_logic := '0';" + NL + "\tbegin" + NL + "\t\tclkTemp := not clkTemp;" + NL + "\t\tclk <= clkTemp;" + NL + "\t\twait for 10 ns;" + NL + "\tend process;" + NL + "\t" + NL + "\tstimulus : process" + NL + "\tbegin" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\trst <= '1'; " + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\trst <= '0';" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait until clk = '0' and clk'event;" + NL + "\t\twait;" + NL + "\tend process;" + NL + "end architecture behavioral;";

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	FSMImpl fsmImp = (FSMImpl) argument; 
	FSM fsm = (FSM) fsmImp;
 

    stringBuffer.append(TEXT_2);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_6);
     
	for (Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
		InControlPort flag = (InControlPort) i.next();

    stringBuffer.append(TEXT_7);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_8);
    
	
	}
	
    stringBuffer.append(TEXT_9);
    
	for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
		OutControlPort command = (OutControlPort) i.next();
		if (command.getWidth()==1) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_11);
    
		} else {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append((command.getWidth()-1));
    stringBuffer.append(TEXT_13);
    
		}
		if(i.hasNext()) {
			
    stringBuffer.append(TEXT_14);
    
		} else {
			
    stringBuffer.append(TEXT_15);
    
		}
	}
	
    stringBuffer.append(TEXT_16);
     
	for (Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
		InControlPort flag = (InControlPort) i.next();

    stringBuffer.append(TEXT_17);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_18);
    
	
	}
	
    stringBuffer.append(TEXT_19);
    
	for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
		OutControlPort command = (OutControlPort) i.next();
		if (command.getWidth()==1) {

    stringBuffer.append(TEXT_20);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_18);
    
		} else {

    stringBuffer.append(TEXT_20);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_21);
    stringBuffer.append((command.getWidth()-1));
    stringBuffer.append(TEXT_22);
    
		}
	}
	
    stringBuffer.append(TEXT_23);
     /* VHDLComponentDeclarationTemplate.create("\n").generate(datapath.getDataWires()); */ 
    stringBuffer.append(TEXT_24);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_25);
    for (Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
			InControlPort flag = (InControlPort) i.next();
			
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_26);
    
		}for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
			OutControlPort command = (OutControlPort) i.next();
			
    stringBuffer.append(command.getName());
    if(i.hasNext()) {
    stringBuffer.append(TEXT_27);
    } else {
			
    stringBuffer.append(TEXT_15);
    
			}
		}
		
    stringBuffer.append(TEXT_28);
    stringBuffer.append(TEXT_23);
    return stringBuffer.toString();
  }
}