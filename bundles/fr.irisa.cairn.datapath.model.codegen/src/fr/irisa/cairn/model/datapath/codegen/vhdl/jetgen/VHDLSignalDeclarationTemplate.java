package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.pads.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;
import fr.irisa.cairn.model.fsm.FSM;;

public class VHDLSignalDeclarationTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLSignalDeclarationTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLSignalDeclarationTemplate result = new VHDLSignalDeclarationTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t" + NL + "\t-- outputs for ";
  protected final String TEXT_2 = " " + NL + "\t" + NL + "\t\t\t";
  protected final String TEXT_3 = "\t" + NL + "\t\tsignal ";
  protected final String TEXT_4 = " : std_logic;";
  protected final String TEXT_5 = " : std_logic_vector(";
  protected final String TEXT_6 = " downto 0);";
  protected final String TEXT_7 = NL + "\t\t\t" + NL + "\t-- inputs for ";
  protected final String TEXT_8 = NL + "\t" + NL + "\t-- inputs for ";
  protected final String TEXT_9 = "(";
  protected final String TEXT_10 = ") " + NL + "\t" + NL + "\t\t\t";
  protected final String TEXT_11 = NL + "\t\t\t" + NL + "\t-- outputs for ";
  protected final String TEXT_12 = NL + "\t" + NL + "\t-- outputs for ";
  protected final String TEXT_13 = ") " + NL;
  protected final String TEXT_14 = " :  std_logic;";
  protected final String TEXT_15 = " downto 0); ";
  protected final String TEXT_16 = "\t" + NL + "\t\t\t\t\tsignal ";
  protected final String TEXT_17 = ") " + NL + "\t\t\t";
  protected final String TEXT_18 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
      
	Datapath datapath = (Datapath) argument;
	
	List<AbstractBlock>  blocks = datapath.getComponents(); 
	for (AbstractBlock block : blocks) {
		if (block instanceof FSM) {
			FSM fsm = (FSM) block;
			
    stringBuffer.append(TEXT_1);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_2);
    
			for (OutControlPort op : fsm.getFlags()) {
				assert op.getParentNode()!=null; 
				String id = op.getParentNode().getName()+"_"+op.getName();
				if (op.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_2);
    
			for (InControlPort ip : fsm.getActivate()) {
				assert ip.getParentNode()!=null; 
				String id = ip.getParentNode().getName()+"_"+ip.getName();
				if (ip.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
		}else if (block instanceof StatusPad) {
			StatusPad sp = (StatusPad) block;
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(sp);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(sp.getClass().getSimpleName());
    stringBuffer.append(TEXT_10);
    
			for (InControlPort ip : sp.getActivate()) {
				assert ip.getParentNode()!=null; 
				String id = ip.getParentNode().getName()+"_"+ip.getName();
				if (ip.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
     
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
		}else if (block instanceof ControlPad) {
			ControlPad sp = (ControlPad) block;
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(sp);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(sp.getClass().getSimpleName());
    stringBuffer.append(TEXT_10);
    
			for (OutControlPort op : sp.getFlags()) {
				assert op.getParentNode()!=null; 
				String id = op.getParentNode().getName()+"_"+op.getName();
				if (op.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
     
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
		}else if (block instanceof AbstractMemory) {
			AbstractMemory am = (AbstractMemory) block;
			
    stringBuffer.append(TEXT_8);
    stringBuffer.append(am.getName());
    stringBuffer.append(TEXT_2);
    
			for (InDataPort ip : am.getIn()) {
				assert ip.getParentNode()!=null; 
				String id = ip.getParentNode().getName()+"_"+ip.getName();
				if (ip.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
			for (InControlPort ip : am.getActivate()) {
				assert ip.getParentNode()!=null; 
				String id = ip.getParentNode().getName()+"_"+ip.getName();
				if (ip.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
			
    stringBuffer.append(TEXT_11);
    stringBuffer.append(am.getName());
    stringBuffer.append(TEXT_2);
    
			for (OutDataPort op : am.getOut()) {
				assert op.getParentNode()!=null; 
				String id = op.getParentNode().getName()+"_"+op.getName();
				if (op.getWidth()==1) {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
				} else {
					
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_6);
    
				} 
			}
		}else if (block instanceof DataFlowBlock) {
			DataFlowBlock df = (DataFlowBlock) block;
			
    stringBuffer.append(TEXT_12);
    stringBuffer.append(df);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(df.getClass().getSimpleName());
    stringBuffer.append(TEXT_13);
    
			for (OutDataPort op : df.getOut()) {
				if (op.getParentNode()!=null) {
					String id = op.getParentNode().getName()+"_"+op.getName();
					if (op.getWidth()==1) {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_14);
    
					} else {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
					} 
				} else {
					String id = "Unknown"+"_"+op.getName();
					if (op.getWidth()==1) {
						
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
					} else {
						
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
					} 
				}
			}
			if(block instanceof FlagBearerBlock){
				FlagBearerBlock fb = (FlagBearerBlock) block;
				for (OutControlPort op : fb.getFlags()) {
					if (op.getParentNode()!=null) {
						String id = op.getParentNode().getName()+"_"+op.getName();
						if (op.getWidth()==1) {
							
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_14);
    
						} else {
							
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
						} 
					} else {
						String id = "Unknown"+"_"+op.getName();
						if (op.getWidth()==1) {
							
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
						} else {
							
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((op.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
						}
					} 
				}
			}
			
			
    stringBuffer.append(TEXT_7);
    stringBuffer.append(df);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(df.getClass().getSimpleName());
    stringBuffer.append(TEXT_17);
    			for (InDataPort ip : df.getIn()) {
				if (ip.getParentNode()!=null) {
					String id = ip.getParentNode().getName()+"_"+ip.getName();
					if (ip.getWidth()==1) {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_14);
    
					} else {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
					} 
				} else {
					String id = "Unknown"+"_"+ip.getName();
					if (ip.getWidth()==1) {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
					} else {
						
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
					} 
				}
			}
			if(block instanceof ActivableBlock){
				 ActivableBlock ab = (ActivableBlock) block;
				for (InControlPort ip : ab.getActivate()) {
					if (ip.getParentNode()!=null) {
						String id = ip.getParentNode().getName()+"_"+ip.getName();
						if (ip.getWidth()==1) {
							
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_14);
    
						} else {
							
    stringBuffer.append(TEXT_3);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
						} 
					} else {
						String id = "Unknown"+"_"+ip.getName();
						if (ip.getWidth()==1) {
							
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_4);
    
						} else {
							
    stringBuffer.append(TEXT_16);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_5);
    stringBuffer.append((ip.getWidth()-1));
    stringBuffer.append(TEXT_15);
    
						}
					} 
				}
			}
		} 	 
	}

    stringBuffer.append(TEXT_18);
    stringBuffer.append(TEXT_18);
    return stringBuffer.toString();
  }
}