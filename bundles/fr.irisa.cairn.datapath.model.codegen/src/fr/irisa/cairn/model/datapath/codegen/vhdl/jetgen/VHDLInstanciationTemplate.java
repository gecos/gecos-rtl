package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class VHDLInstanciationTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLInstanciationTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLInstanciationTemplate result = new VHDLInstanciationTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\tinst_";
  protected final String TEXT_2 = " : RSLatch port map(" + NL + "   \t\t";
  protected final String TEXT_3 = NL + "  \t);";
  protected final String TEXT_4 = " \t\tinst_";
  protected final String TEXT_5 = " : ";
  protected final String TEXT_6 = " port map(" + NL + "   \t\t";
  protected final String TEXT_7 = NL + "  \t\t);";
  protected final String TEXT_8 = NL + "\t\t" + NL + "\t\t\tinst_RAM16X1D_";
  protected final String TEXT_9 = " : RAM16X1D " + NL + "\t\t\t-- synthesis translate_off" + NL + "  \t\t \t\tgeneric map (INIT => hex_value)" + NL + "  \t\t\t-- synthesis translate_on " + NL + "\t\t\tport map(" + NL + "\t\t\t\tregFile_dataOut_0(";
  protected final String TEXT_10 = ")," + NL + "\t\t\t\tregFile_dataOut_1(";
  protected final String TEXT_11 = ")," + NL + "\t\t\t\tregFile_address_1(0)," + NL + "\t\t\t\tregFile_address_1(1)," + NL + "\t\t\t\tregFile_address_1(2)," + NL + "\t\t\t\tregFile_address_1(3)," + NL + "\t\t\t\tregFile_dataIn_0(";
  protected final String TEXT_12 = ")," + NL + "\t\t\t\tregFile_address_0(0)," + NL + "\t\t\t\tregFile_address_0(1)," + NL + "\t\t\t\tregFile_address_0(2)," + NL + "\t\t\t\tregFile_address_0(3)," + NL + "\t\t\t\tclk," + NL + "\t\t\t\tregFile_we_0" + NL + "\t\t\t);" + NL + "\t\t\t \t";
  protected final String TEXT_13 = NL + "  \t\t);" + NL + "\t\t";
  protected final String TEXT_14 = "  " + NL + "  " + NL + "  ";
  protected final String TEXT_15 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
      
	boolean inlinedMapping = true;
	AbstractBlock comp = (AbstractBlock) argument;
	boolean RAM16X1D = false;
	if (RAM16X1D == false) {
		if(comp instanceof RSRegister){
    stringBuffer.append(TEXT_1);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(VHDLPortMappingTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_3);
    		}else{		 

    stringBuffer.append(TEXT_4);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(VHDLPortMappingTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_7);
     		}	
	} else{
		if(comp instanceof DualPortRam){
			for(int i=0; i<8; i++){
    stringBuffer.append(TEXT_8);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_9);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_11);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_12);
    			}
		}else{

    stringBuffer.append(TEXT_4);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(VHDLPortMappingTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_13);
    		}
   	}
    stringBuffer.append(TEXT_14);
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}