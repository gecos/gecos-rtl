/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stratix III</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getStratixIII()
 * @model
 * @generated
 */
public interface StratixIII extends AbstractTarget {
} // StratixIII
