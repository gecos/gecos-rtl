/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectFactory
 * @model kind="package"
 * @generated
 */
public interface projectPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "codegenProject";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "codegenProject";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "codegenProject";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	projectPackage eINSTANCE = fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl <em>Code Generation Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getCodeGenerationProject()
	 * @generated
	 */
	int CODE_GENERATION_PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__DIRECTORY = 1;

	/**
	 * The feature id for the '<em><b>Datapath</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__DATAPATH = 2;

	/**
	 * The feature id for the '<em><b>Current Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__CURRENT_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Device</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__DEVICE = 4;

	/**
	 * The feature id for the '<em><b>Vhdl Files</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT__VHDL_FILES = 5;

	/**
	 * The number of structural features of the '<em>Code Generation Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_GENERATION_PROJECT_FEATURE_COUNT = 6;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.AbstractTargetImpl <em>Abstract Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.AbstractTargetImpl
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getAbstractTarget()
	 * @generated
	 */
	int ABSTRACT_TARGET = 1;

	/**
	 * The number of structural features of the '<em>Abstract Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TARGET_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CycloneIIImpl <em>Cyclone II</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CycloneIIImpl
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getCycloneII()
	 * @generated
	 */
	int CYCLONE_II = 2;

	/**
	 * The number of structural features of the '<em>Cyclone II</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLONE_II_FEATURE_COUNT = ABSTRACT_TARGET_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIImpl <em>Stratix II</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIImpl
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getStratixII()
	 * @generated
	 */
	int STRATIX_II = 3;

	/**
	 * The number of structural features of the '<em>Stratix II</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATIX_II_FEATURE_COUNT = ABSTRACT_TARGET_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIIImpl <em>Stratix III</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIIImpl
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getStratixIII()
	 * @generated
	 */
	int STRATIX_III = 4;

	/**
	 * The number of structural features of the '<em>Stratix III</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATIX_III_FEATURE_COUNT = ABSTRACT_TARGET_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject <em>Code Generation Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Generation Project</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject
	 * @generated
	 */
	EClass getCodeGenerationProject();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getName()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EAttribute getCodeGenerationProject_Name();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDirectory <em>Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Directory</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDirectory()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EAttribute getCodeGenerationProject_Directory();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDatapath <em>Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDatapath()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EReference getCodeGenerationProject_Datapath();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getCurrentElement <em>Current Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Element</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getCurrentElement()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EReference getCodeGenerationProject_CurrentElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDevice <em>Device</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Device</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getDevice()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EAttribute getCodeGenerationProject_Device();

	/**
	 * Returns the meta object for the attribute list '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getVhdlFiles <em>Vhdl Files</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Vhdl Files</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject#getVhdlFiles()
	 * @see #getCodeGenerationProject()
	 * @generated
	 */
	EAttribute getCodeGenerationProject_VhdlFiles();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.AbstractTarget <em>Abstract Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Target</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.AbstractTarget
	 * @generated
	 */
	EClass getAbstractTarget();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.CycloneII <em>Cyclone II</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cyclone II</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.CycloneII
	 * @generated
	 */
	EClass getCycloneII();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixII <em>Stratix II</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stratix II</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixII
	 * @generated
	 */
	EClass getStratixII();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixIII <em>Stratix III</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stratix III</em>'.
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixIII
	 * @generated
	 */
	EClass getStratixIII();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	projectFactory getprojectFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl <em>Code Generation Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getCodeGenerationProject()
		 * @generated
		 */
		EClass CODE_GENERATION_PROJECT = eINSTANCE.getCodeGenerationProject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_GENERATION_PROJECT__NAME = eINSTANCE.getCodeGenerationProject_Name();

		/**
		 * The meta object literal for the '<em><b>Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_GENERATION_PROJECT__DIRECTORY = eINSTANCE.getCodeGenerationProject_Directory();

		/**
		 * The meta object literal for the '<em><b>Datapath</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_GENERATION_PROJECT__DATAPATH = eINSTANCE.getCodeGenerationProject_Datapath();

		/**
		 * The meta object literal for the '<em><b>Current Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CODE_GENERATION_PROJECT__CURRENT_ELEMENT = eINSTANCE.getCodeGenerationProject_CurrentElement();

		/**
		 * The meta object literal for the '<em><b>Device</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_GENERATION_PROJECT__DEVICE = eINSTANCE.getCodeGenerationProject_Device();

		/**
		 * The meta object literal for the '<em><b>Vhdl Files</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_GENERATION_PROJECT__VHDL_FILES = eINSTANCE.getCodeGenerationProject_VhdlFiles();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.AbstractTargetImpl <em>Abstract Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.AbstractTargetImpl
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getAbstractTarget()
		 * @generated
		 */
		EClass ABSTRACT_TARGET = eINSTANCE.getAbstractTarget();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CycloneIIImpl <em>Cyclone II</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CycloneIIImpl
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getCycloneII()
		 * @generated
		 */
		EClass CYCLONE_II = eINSTANCE.getCycloneII();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIImpl <em>Stratix II</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIImpl
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getStratixII()
		 * @generated
		 */
		EClass STRATIX_II = eINSTANCE.getStratixII();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIIImpl <em>Stratix III</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.StratixIIIImpl
		 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.projectPackageImpl#getStratixIII()
		 * @generated
		 */
		EClass STRATIX_III = eINSTANCE.getStratixIII();

	}

} //projectPackage
