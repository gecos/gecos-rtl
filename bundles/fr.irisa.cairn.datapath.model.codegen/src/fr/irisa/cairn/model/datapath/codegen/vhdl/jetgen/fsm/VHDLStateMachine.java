package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.port.*;

public class VHDLStateMachine
{
  protected static String nl;
  public static synchronized VHDLStateMachine create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLStateMachine result = new VHDLStateMachine();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "library ieee; " + NL + "use ieee.std_logic_1164.all;" + NL + "use ieee.numeric_std.all;" + NL + "use ieee.std_logic_unsigned.all; " + NL + "use ieee.numeric_std.all; " + NL + " " + NL + "entity ";
  protected final String TEXT_3 = " is port (" + NL + "\t" + NL + "\t-- global signals " + NL + "\tclk : in std_logic;" + NL + "\trst : in std_logic;" + NL + "\t" + NL + "\t-- Input ports";
  protected final String TEXT_4 = NL + "\t";
  protected final String TEXT_5 = " : in std_logic;";
  protected final String TEXT_6 = NL + "\t" + NL + "\t-- output ports";
  protected final String TEXT_7 = "\t" + NL + "\t";
  protected final String TEXT_8 = " : out std_logic ";
  protected final String TEXT_9 = " : out std_logic_vector(";
  protected final String TEXT_10 = " downto 0)";
  protected final String TEXT_11 = ";" + NL + "\t\t\t";
  protected final String TEXT_12 = NL + "\t\t\t";
  protected final String TEXT_13 = NL + " " + NL + "  );" + NL + "end ";
  protected final String TEXT_14 = " ;" + NL + "" + NL + "architecture RTL of ";
  protected final String TEXT_15 = " is" + NL + "" + NL + "\ttype fsm_";
  protected final String TEXT_16 = "_type is (";
  protected final String TEXT_17 = NL + "   \t\t";
  protected final String TEXT_18 = ",";
  protected final String TEXT_19 = NL + "\t);" + NL + "   \t" + NL + "\tsignal NS,CS : fsm_";
  protected final String TEXT_20 = "_type;" + NL + "" + NL + "begin" + NL + "" + NL + "\tprocess(CS,";
  protected final String TEXT_21 = " ";
  protected final String TEXT_22 = ", ";
  protected final String TEXT_23 = ")" + NL + "\tbegin" + NL + "\t";
  protected final String TEXT_24 = NL + " \t\t\t";
  protected final String TEXT_25 = " <= \"";
  protected final String TEXT_26 = "\"; " + NL + " \t\t\t";
  protected final String TEXT_27 = " <= '0'; " + NL + "\t\t\t";
  protected final String TEXT_28 = "\t\t" + NL + " \t\t\tNS <= idle;" + NL + "" + NL + "\t\t\tcase CS is" + NL + "\t\t";
  protected final String TEXT_29 = " " + NL + "\t\t\t-------------------------" + NL + "\t\t\t-- State ";
  protected final String TEXT_30 = NL + "\t\t\t-------------------------" + NL + "\t\t\twhen ";
  protected final String TEXT_31 = " =>" + NL + "\t\t\t\t-- State transitions";
  protected final String TEXT_32 = NL;
  protected final String TEXT_33 = NL + "\t\t\t\t-- Output commands";
  protected final String TEXT_34 = " " + NL + "\t\t\twhen others =>" + NL + "\t\t\t\tNS <= ";
  protected final String TEXT_35 = ";" + NL + "\t\t\tend case;" + NL + "\tend process;" + NL + "" + NL + "\tprocess(clk,rst)" + NL + "\tbegin" + NL + "\t\tif rising_edge(clk) then" + NL + "\t\t\tif rst='1' then" + NL + "\t\t\t\tCS <= ";
  protected final String TEXT_36 = ";" + NL + "\t\t\telse" + NL + "\t\t\t\tCS <= NS;" + NL + "\t\t\tend if;" + NL + "\t\tend if;" + NL + "\tend process;" + NL + "" + NL + "end RTL;";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	FSM fsm = (FSM) project.getCurrentElement();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_3);
     
	for (Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
		InControlPort flag = (InControlPort) i.next();

    stringBuffer.append(TEXT_4);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_5);
    
	
	}
	
    stringBuffer.append(TEXT_6);
    
	for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
		OutControlPort command = (OutControlPort) i.next();
		if (command.getWidth()==1) {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_8);
    
		} else {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_9);
    stringBuffer.append((command.getWidth()-1));
    stringBuffer.append(TEXT_10);
    
		}
		if(i.hasNext()) {
			
    stringBuffer.append(TEXT_11);
    
		} else {
			
    stringBuffer.append(TEXT_12);
    
		}
	}
	
    stringBuffer.append(TEXT_13);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_16);
    
   	for (Iterator i = fsm.getStates().iterator(); i.hasNext(); ) {
   		State state = (State) i.next();
    stringBuffer.append(TEXT_17);
    stringBuffer.append(state.getLabel());
    if(i.hasNext()) {
    stringBuffer.append(TEXT_18);
    }
   		
    }
    stringBuffer.append(TEXT_19);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_20);
    
	for(Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
   		InControlPort flag = (InControlPort) i.next();
   		
    stringBuffer.append(TEXT_21);
    stringBuffer.append(flag.getName());
     
 		if(i.hasNext()) {
 			
    stringBuffer.append(TEXT_22);
    
 		}
	}
	
    stringBuffer.append(TEXT_23);
    		
	for(Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
   		OutControlPort com = (OutControlPort) i.next();
   		if (com.getWidth()!=1) {
    stringBuffer.append(TEXT_24);
    stringBuffer.append(com.getName());
    stringBuffer.append(TEXT_25);
    stringBuffer.append(VHDLGenUtils.binaryValue(0,com.getWidth()));
    stringBuffer.append(TEXT_26);
    
		} else {
    stringBuffer.append(TEXT_12);
    stringBuffer.append(com.getName());
    stringBuffer.append(TEXT_27);
    
		}

 		}
    stringBuffer.append(TEXT_28);
    
			for(Iterator i = fsm.getStates().iterator(); i.hasNext(); ) {
				State state = (State) i.next();
    stringBuffer.append(TEXT_29);
    stringBuffer.append(state.getLabel());
    stringBuffer.append(TEXT_30);
    stringBuffer.append(state.getLabel());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(VHDLStateTransition.create("\n").generate(state));
    stringBuffer.append(TEXT_33);
    stringBuffer.append(TEXT_32);
    stringBuffer.append(VHDLCommandValue.create("\n").generate(state));
    
		}
		
    stringBuffer.append(TEXT_34);
    stringBuffer.append(fsm.getStart().getLabel());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(fsm.getStart().getLabel());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(TEXT_32);
    return stringBuffer.toString();
  }
}
