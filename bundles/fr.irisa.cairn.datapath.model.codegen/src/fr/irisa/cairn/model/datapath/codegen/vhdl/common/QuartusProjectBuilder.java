package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;

import fr.irisa.cairn.model.datapath.Datapath;

public class QuartusProjectBuilder  {

	public Datapath getModel() {
		return model;
	}

	String toplevel = "unknown";
	String device = "Stratix III";
	Datapath model;

	public QuartusProjectBuilder(String path, Datapath model) {
	}

	public void create() throws FileNotFoundException {
//		File tclscript = addFileToProject(model.getName() + ".tcl");
//		PrintStream st = new PrintStream(tclscript);
//		st.append(QuartusTclScriptTemplate.create("\n").generate(this));
	}

	public String getToplevel() {
		return toplevel;
	}

	public String getName() {
		return model.getName();
	}

	public Collection<File> getVhdlfiles() {
		Collection<File> filtered = new ArrayList<File>();
//		for (File file : getProjectFiles()) {
//			if (file.getName().endsWith("vhd")) {
//				filtered.add(file);
//			}
//		}
		return filtered;
	}

	public void buildNewFile(Datapath model)  {
//		File vhdlfile = addFileToProject(model.getName() + ".vhd");
//		PrintStream st;
//		try {
//			st = new PrintStream(vhdlfile);
//			st.append(VHDLDatapathTemplate.create("\n").generate(this));
//		} catch (FileNotFoundException e) {
//			throw new RuntimeException(e.getMessage());
//		}
	}

	public String getDevice() {
		return device;
	}

}
