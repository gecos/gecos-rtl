package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;


import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.VHDLDatapathTemplate;

public class VHDLGenerator {
	
	VHDLDatapathTemplate template = new VHDLDatapathTemplate();
	static VHDLGenerator  generator = null;
	
	public static VHDLGenerator getInstance() {
		if (generator==null) {
			generator = new VHDLGenerator();
		}
		return generator;
	}
	
	public void export(Datapath dp, String filename) throws FileNotFoundException {
		PrintStream stream = new PrintStream(filename);
		stream.append(template.generate(dp));
		stream.close();
	}
	
	public String export(Datapath dp) {
		StringBuffer stream = new StringBuffer();
		stream.append(template.generate(dp));
		return stream.toString();
	}

	public void export(Datapath dp, File file) throws FileNotFoundException {
		PrintStream stream = new PrintStream(file);
		stream.append(template.generate(dp));
		stream.close();
	}
 }
