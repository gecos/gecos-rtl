package fr.irisa.cairn.model.datapath.codegen;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.custom.BoolExpPrettyPrint;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.fsm.util.FsmSwitch;

public class DottyFSMExport<T> extends FsmSwitch<T> {

	StringBuffer sb;
	StringBuffer tb;
	FSM fsm;
	
	
	public DottyFSMExport(FSM fsm) {
		sb = new StringBuffer();
		tb = new StringBuffer();
		this.fsm=fsm;
	}

	public String generate() {
		doSwitch(fsm);
		return sb.toString();
	}

	public void saveas(String filename) throws FileNotFoundException {
		PrintStream ps = new PrintStream(filename);
		doSwitch(fsm);
		
		ps.append(sb);
	}
	
	public T caseFSM(FSM object) {
		fsm = object;
		sb.append("digraph "+object.getName()+" {\n");
		sb.append("\n");
		sb.append("\n");
		for (State state : object.getStates()) {
			doSwitch(state);
		}
		sb.append("\n");
		sb.append("\n");
		sb.append(tb);
		sb.append("}");
		return super.caseFSM(object);
	}

	@Override
	public T caseState(State object) {
		sb.append("\t"+object.getLabel()+'[');
		if (object==fsm.getStart()) {
			sb.append("shape=doublecircle,");
		}
		sb.append("label=\""+object.getLabel()+"\\n");
		for (AbstractCommandValue acv: object.getActivatedCommands()) {
			doSwitch(acv);
		}
		sb.append("\"]\n");
		
		for (Transition t: object.getTransitions()) {
			doSwitch(t);
		}
		return super.caseState(object);
	}

	@Override
	public T caseTransition(Transition t) {
		State src = t.getSrc();
		tb.append("\t"+src.getLabel()+"->"+t.getDst().getLabel());
		if(t.getPredicate()!=null) {
			String string = t.getPredicate().toString();
			tb.append("[label=\""+t.getLabel()+":"+string+"\"]\n");
		} else {
			tb.append("[label=\""+t.getLabel()+":true\"]\n");
		}
		return super.caseTransition(t);
	}
	@Override
	public T caseBooleanCommandValue(BooleanCommandValue bcv) {
		AbstractBooleanExpression predicate = bcv.getPredicate();
		if(predicate==null || predicate.isTrue()) {
			sb.append("  -"+bcv.getCommandName()+"<="+bcv.getValue()+"\\n");
		} else {
			sb.append("  -"+bcv.getCommandName()+"<="+bcv.getValue()+" if "+predicate+"\\n");
		}
		return super.caseBooleanCommandValue(bcv);
	}
	@Override
	public T caseIntegerCommandValue(IntegerCommandValue bcv) {
		AbstractBooleanExpression predicate = bcv.getPredicate();
		if(predicate==null || predicate.isTrue()) {
			sb.append("  -"+bcv.getCommandName()+"<="+bcv.getValue()+"\\n");
		} else {
			sb.append("  -"+bcv.getCommandName()+"<="+bcv.getValue()+" if "+predicate+"\\n");
		}
		return super.caseIntegerCommandValue(bcv);
	}
	
//	@Override
//	public T caseAbstractCommandValue(AbstractCommandValue object) {
//		sb.append(object.getCommandName()+" when "+BoolExpPrettyPrint.print(object.getPredicate()));
//		return super.caseAbstractCommandValue(object);
//	}

}
