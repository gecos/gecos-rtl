package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.projectFactory;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.DualPortRamGenerator;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.SinglePortRamGenerator;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.SinglePortRomGenerator;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.VHDLDatapathTemplate;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLStateMachine;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.util.DatapathSwitch;
import fr.irisa.cairn.model.fsm.FSM;

public class DatapathVHDLGenerator extends DatapathSwitch {

	private CodeGenerationProject project;
	private int indent=0;
	private String tab="";

	protected File addFileToProject(String filename) {
		File res = new File(project.getDirectory()+"/"+filename);
		return res;
	}
	

	protected File addsubDirectoryToProject(String filename) {
		File res = new File(project.getDirectory()+"/"+filename);
		res.mkdir();
		return res;
	}

	protected void deleteFileFromProject(String filename) {
		File res = new File(project.getDirectory()+"/"+filename);
		res.delete();
	}

	@Override

	public Object defaultCase(EObject object) {
		if (object instanceof FSM) {
			FSM fsm = (FSM) object;
			project.setCurrentElement(fsm);
			print("Visiting FSM :"+fsm.getName());
			String content = VHDLStateMachine.create("\n").generate(project);
			String filename = fsm.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		
		else if(object instanceof DualPortRam){
			DualPortRam regFile = (DualPortRam) object;
			project.setCurrentElement(regFile);
			print("Visiting Dual Port Ram :"+regFile.getName());
			String content = DualPortRamGenerator.create("\n").generate(project);
			String filename = regFile.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		
		else if(object instanceof SinglePortRam){
			SinglePortRam spRam = (SinglePortRam) object;
			project.setCurrentElement(spRam);
			print("Visiting Single Port Ram :"+spRam.getName());
			String content = SinglePortRamGenerator.create("\n").generate(project);
			String filename = spRam.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		
		else if(object instanceof SinglePortRom){
			SinglePortRom spRom = (SinglePortRom) object;
			project.setCurrentElement(spRom);
			print("Visiting Single Port Rom :"+spRom.getName());
			String content = SinglePortRomGenerator.create("\n").generate(project);
			String filename = spRom.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		else{
			return null;
		}
	}

	@Override
	public Object caseDataFlowBlock(DataFlowBlock object) {
		if(object instanceof DualPortRam){
			DualPortRam regFile = (DualPortRam) object;
			project.setCurrentElement(regFile);
			print("Visiting Dual Port Ram :"+regFile.getName());
			String content = DualPortRamGenerator.create("\n").generate(project);
			String filename = regFile.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		
		else if(object instanceof SinglePortRam){
			SinglePortRam spRam = (SinglePortRam) object;
			project.setCurrentElement(spRam);
			print("Visiting Single Port Ram :"+spRam.getName());
			String content = SinglePortRamGenerator.create("\n").generate(project);
			String filename = spRam.getName() + ".vhd";
			project.getVhdlFiles().add(filename);
			File file = addFileToProject(filename);
			try {
				writeFile(file, content);
			} catch (FileNotFoundException e) {
				throw new RuntimeException(e.getMessage());
			}
			return object;
		}
		
//		else if(object instanceof RSRegister){
//			RSRegister rsReg = (RSRegister) object;
//			project.setCurrentElement(rsReg);
//			print("Visiting RS-Register :"+rsReg.getName());
//			String content = RSRegisterGenerator.create("\n").generate(project);
//			String filename = rsReg.getName() + ".vhd";
//			project.getVhdlFiles().add(filename);
//			File file = addFileToProject(filename);
//			try {
//				writeFile(file, content);
//			} catch (FileNotFoundException e) {
//				throw new RuntimeException(e.getMessage());
//			}
//			return object;
//		}
		return null;
	}


	@Override
	public Object caseDatapath(Datapath datapath) {
		
		print("Visiting Datapath :"+datapath.getName());

		String content = VHDLDatapathTemplate.create("\n").generate(project);
		File file = addFileToProject(datapath.getName() + ".vhd");
		String filename = datapath.getName()+ ".vhd";
		project.getVhdlFiles().add(filename);
		try {
			writeFile(file, content);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage());
		}
		
		
		for (AbstractBlock block : datapath.getComponents()) {
			tab();
			doSwitch(block);
			unTab();
		}
		return datapath;
	}

	private void unTab() {
		indent--;
		tab=tab.substring(2);
		
	}


	private void tab() {
		indent++;
		tab=tab+"  ";
	}

	private void print(String string) {
		System.out.println(tab+string);
	}



	private void writeFile(File file, String content)
			throws FileNotFoundException {
		PrintStream ps = new PrintStream(file);
		ps.append(content);
		ps.close();
	}

	public void generate() {
		doSwitch(project.getCurrentElement());
	}

	public void compute() {
		doSwitch(project.getCurrentElement());
	}

	public DatapathVHDLGenerator(Datapath dp, String Directory, String name) {
		project= projectFactory.eINSTANCE.createCodeGenerationProject();
		project.setDatapath(dp);
		project.setDirectory(Directory);
		project.setName(name);
		project.setCurrentElement(project.getDatapath());
	}

}
