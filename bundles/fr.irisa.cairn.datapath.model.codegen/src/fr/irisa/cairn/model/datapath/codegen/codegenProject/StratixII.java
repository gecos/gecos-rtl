/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stratix II</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getStratixII()
 * @model
 * @generated
 */
public interface StratixII extends AbstractTarget {
} // StratixII
