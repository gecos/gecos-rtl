/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject.impl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.Datapath;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Code Generation Project</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getName <em>Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getDirectory <em>Directory</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getDatapath <em>Datapath</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getCurrentElement <em>Current Element</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getDevice <em>Device</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.codegen.codegenProject.impl.CodeGenerationProjectImpl#getVhdlFiles <em>Vhdl Files</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CodeGenerationProjectImpl extends EObjectImpl implements CodeGenerationProject {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDirectory() <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDirectory() <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectory()
	 * @generated
	 * @ordered
	 */
	protected String directory = DIRECTORY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDatapath() <em>Datapath</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatapath()
	 * @generated
	 * @ordered
	 */
	protected Datapath datapath;

	/**
	 * The cached value of the '{@link #getCurrentElement() <em>Current Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentElement()
	 * @generated
	 * @ordered
	 */
	protected AbstractBlock currentElement;

	/**
	 * The default value of the '{@link #getDevice() <em>Device</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected static final String DEVICE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDevice() <em>Device</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDevice()
	 * @generated
	 * @ordered
	 */
	protected String device = DEVICE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVhdlFiles() <em>Vhdl Files</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVhdlFiles()
	 * @generated
	 * @ordered
	 */
	protected EList<String> vhdlFiles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CodeGenerationProjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return projectPackage.Literals.CODE_GENERATION_PROJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDirectory() {
		return directory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectory(String newDirectory) {
		String oldDirectory = directory;
		directory = newDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__DIRECTORY, oldDirectory, directory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getDatapath() {
		return datapath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDatapath(Datapath newDatapath, NotificationChain msgs) {
		Datapath oldDatapath = datapath;
		datapath = newDatapath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__DATAPATH, oldDatapath, newDatapath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatapath(Datapath newDatapath) {
		if (newDatapath != datapath) {
			NotificationChain msgs = null;
			if (datapath != null)
				msgs = ((InternalEObject)datapath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - projectPackage.CODE_GENERATION_PROJECT__DATAPATH, null, msgs);
			if (newDatapath != null)
				msgs = ((InternalEObject)newDatapath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - projectPackage.CODE_GENERATION_PROJECT__DATAPATH, null, msgs);
			msgs = basicSetDatapath(newDatapath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__DATAPATH, newDatapath, newDatapath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBlock getCurrentElement() {
		if (currentElement != null && currentElement.eIsProxy()) {
			InternalEObject oldCurrentElement = (InternalEObject)currentElement;
			currentElement = (AbstractBlock)eResolveProxy(oldCurrentElement);
			if (currentElement != oldCurrentElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT, oldCurrentElement, currentElement));
			}
		}
		return currentElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBlock basicGetCurrentElement() {
		return currentElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentElement(AbstractBlock newCurrentElement) {
		AbstractBlock oldCurrentElement = currentElement;
		currentElement = newCurrentElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT, oldCurrentElement, currentElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDevice() {
		return device;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevice(String newDevice) {
		String oldDevice = device;
		device = newDevice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, projectPackage.CODE_GENERATION_PROJECT__DEVICE, oldDevice, device));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getVhdlFiles() {
		if (vhdlFiles == null) {
			vhdlFiles = new EDataTypeUniqueEList<String>(String.class, this, projectPackage.CODE_GENERATION_PROJECT__VHDL_FILES);
		}
		return vhdlFiles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case projectPackage.CODE_GENERATION_PROJECT__DATAPATH:
				return basicSetDatapath(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case projectPackage.CODE_GENERATION_PROJECT__NAME:
				return getName();
			case projectPackage.CODE_GENERATION_PROJECT__DIRECTORY:
				return getDirectory();
			case projectPackage.CODE_GENERATION_PROJECT__DATAPATH:
				return getDatapath();
			case projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT:
				if (resolve) return getCurrentElement();
				return basicGetCurrentElement();
			case projectPackage.CODE_GENERATION_PROJECT__DEVICE:
				return getDevice();
			case projectPackage.CODE_GENERATION_PROJECT__VHDL_FILES:
				return getVhdlFiles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case projectPackage.CODE_GENERATION_PROJECT__NAME:
				setName((String)newValue);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DIRECTORY:
				setDirectory((String)newValue);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DATAPATH:
				setDatapath((Datapath)newValue);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT:
				setCurrentElement((AbstractBlock)newValue);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DEVICE:
				setDevice((String)newValue);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__VHDL_FILES:
				getVhdlFiles().clear();
				getVhdlFiles().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case projectPackage.CODE_GENERATION_PROJECT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DIRECTORY:
				setDirectory(DIRECTORY_EDEFAULT);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DATAPATH:
				setDatapath((Datapath)null);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT:
				setCurrentElement((AbstractBlock)null);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__DEVICE:
				setDevice(DEVICE_EDEFAULT);
				return;
			case projectPackage.CODE_GENERATION_PROJECT__VHDL_FILES:
				getVhdlFiles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case projectPackage.CODE_GENERATION_PROJECT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case projectPackage.CODE_GENERATION_PROJECT__DIRECTORY:
				return DIRECTORY_EDEFAULT == null ? directory != null : !DIRECTORY_EDEFAULT.equals(directory);
			case projectPackage.CODE_GENERATION_PROJECT__DATAPATH:
				return datapath != null;
			case projectPackage.CODE_GENERATION_PROJECT__CURRENT_ELEMENT:
				return currentElement != null;
			case projectPackage.CODE_GENERATION_PROJECT__DEVICE:
				return DEVICE_EDEFAULT == null ? device != null : !DEVICE_EDEFAULT.equals(device);
			case projectPackage.CODE_GENERATION_PROJECT__VHDL_FILES:
				return vhdlFiles != null && !vhdlFiles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", directory: ");
		result.append(directory);
		result.append(", device: ");
		result.append(device);
		result.append(", vhdlFiles: ");
		result.append(vhdlFiles);
		result.append(')');
		return result.toString();
	}

} //CodeGenerationProjectImpl
