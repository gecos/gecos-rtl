package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class VHDLRegisterMapping extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLRegisterMapping create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLRegisterMapping result = new VHDLRegisterMapping();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "\tall_regs : process(rst,clk)" + NL;
  protected final String TEXT_2 = "\t\tvariable ";
  protected final String TEXT_3 = "_reg  : array (";
  protected final String TEXT_4 = " downto 0) of std_logic_vector(";
  protected final String TEXT_5 = " downto 0);" + NL;
  protected final String TEXT_6 = NL + "\tbegin" + NL + "\t\tif rst='1' then" + NL + " ";
  protected final String TEXT_7 = "\t\t\t";
  protected final String TEXT_8 = " <= (others =>'0'); " + NL;
  protected final String TEXT_9 = "_reg <= (others => (others =>'0'));" + NL;
  protected final String TEXT_10 = NL + "\t\telsif rising_edge(clk) then" + NL + "\t\t";
  protected final String TEXT_11 = NL + "\t\t\t";
  protected final String TEXT_12 = " <= ";
  protected final String TEXT_13 = " ; " + NL;
  protected final String TEXT_14 = NL + "\t\t\tif ";
  protected final String TEXT_15 = "='1' then " + NL + "\t\t\t\t";
  protected final String TEXT_16 = ";" + NL + "\t\t\tend if;" + NL;
  protected final String TEXT_17 = "='1' then" + NL + "\t\t\t\t";
  protected final String TEXT_18 = "_reg (0) <= ";
  protected final String TEXT_19 = "; " + NL + "\t\t\t\tfor i in 1 to ";
  protected final String TEXT_20 = " loop" + NL + "\t\t\t\t\t";
  protected final String TEXT_21 = "_reg(i) <= ";
  protected final String TEXT_22 = "_reg(i-1); " + NL + "\t\t\t\tend for;  " + NL + "\t\t\t\t";
  protected final String TEXT_23 = "_reg(";
  protected final String TEXT_24 = ");" + NL + "\t\t\tend if;";
  protected final String TEXT_25 = "\t\t\t-- SR depth ";
  protected final String TEXT_26 = " \t" + NL + "\t\tend if;" + NL + "\tend process;";
  protected final String TEXT_27 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	Datapath datapath = (Datapath) argument;
 	for (AbstractBlock block : datapath.getComponents()) {
		if (block instanceof ShiftRegister) {
			ShiftRegister sreg = (ShiftRegister) block;
			if (sreg.getDepth()>1) { 
				
    stringBuffer.append(TEXT_2);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append((sreg.getDepth()-1));
    stringBuffer.append(TEXT_4);
    stringBuffer.append((sreg.getOutput().getWidth()-1));
    stringBuffer.append(TEXT_5);
     
			}
		}
	}

    stringBuffer.append(TEXT_6);
    
	
	for (AbstractBlock block : datapath.getComponents()) {
		if (block instanceof Register) {
			Register reg = (Register) block;
			EObject eobject = (EObject) block;
			String Q = VHDLGenUtils.makeOutputName(reg);
			switch(eobject.eClass().getClassifierID()) {
				case StoragePackage.REGISTER:
				case StoragePackage.CE_REGISTER:
					
    stringBuffer.append(TEXT_7);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_8);
    
					break; 
				case StoragePackage.SHIFT_REGISTER:
					ShiftRegister sreg =(ShiftRegister)reg;
					
    stringBuffer.append(TEXT_7);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_9);
     
					break; 
				default: 
			}
		}
	}

    stringBuffer.append(TEXT_10);
    	
	for (AbstractBlock block : datapath.getComponents()) {
		if (block instanceof Register) {
			Register reg = (Register) block;
			EObject eobject = (EObject) block;
			String Q = VHDLGenUtils.makeOutputName(reg);
			String D  = VHDLGenUtils.makeSourceName((DataFlowBlock)reg,0);
			switch(eobject.eClass().getClassifierID()) {
				case StoragePackage.REGISTER:
					
    stringBuffer.append(TEXT_11);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_13);
    
					break; 
				case StoragePackage.CE_REGISTER:
					String CE =VHDLGenUtils.makeSourceName((ActivableBlock)reg,0); 
					
    stringBuffer.append(TEXT_14);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_16);
    
					break; 
				case StoragePackage.SHIFT_REGISTER:
					ShiftRegister sreg = (ShiftRegister) reg;
					CE =VHDLGenUtils.makeSourceName((ActivableBlock)reg,0);
					if (sreg.getDepth()>1) { 

    stringBuffer.append(TEXT_14);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_17);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_19);
    stringBuffer.append((sreg.getDepth()-1));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_21);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_22);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(sreg.getName());
    stringBuffer.append(TEXT_23);
    stringBuffer.append((sreg.getDepth()-1));
    stringBuffer.append(TEXT_24);
    
					} else {

    stringBuffer.append(TEXT_25);
    stringBuffer.append(sreg.getDepth());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_15);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_16);
    
					}
					break; 
				default: 
			}
		}
	}

    stringBuffer.append(TEXT_26);
    stringBuffer.append(TEXT_27);
    return stringBuffer.toString();
  }
}