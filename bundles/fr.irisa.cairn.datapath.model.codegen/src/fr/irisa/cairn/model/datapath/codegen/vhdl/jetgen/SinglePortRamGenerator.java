package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class SinglePortRamGenerator
{
  protected static String nl;
  public static synchronized SinglePortRamGenerator create(String lineSeparator)
  {
    nl = lineSeparator;
    SinglePortRamGenerator result = new SinglePortRamGenerator();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "-- megafunction wizard: %RAM: 1-PORT%" + NL + "-- GENERATION: STANDARD" + NL + "-- VERSION: WM1.0" + NL + "-- MODULE: altsyncram " + NL + "" + NL + "-- ============================================================" + NL + "-- File Name: ";
  protected final String TEXT_3 = ".vhd" + NL + "-- Megafunction Name(s):" + NL + "-- \t\t\taltsyncram" + NL + "--" + NL + "-- Simulation Library Files(s):" + NL + "-- \t\t\taltera_mf" + NL + "" + NL + "--Copyright (C) 1991-2010 Altera Corporation" + NL + "--Your use of Altera Corporation's design tools, logic functions " + NL + "--and other software and tools, and its AMPP partner logic " + NL + "--functions, and any output files from any of the foregoing " + NL + "--(including device programming or simulation files), and any " + NL + "--associated documentation or information are expressly subject " + NL + "--to the terms and conditions of the Altera Program License " + NL + "--Subscription Agreement, Altera MegaCore Function License " + NL + "--Agreement, or other applicable license agreement, including, " + NL + "--without limitation, that your use is for the sole purpose of " + NL + "--programming logic devices manufactured by Altera and sold by " + NL + "--Altera or its authorized distributors.  Please refer to the " + NL + "--applicable agreement for further details." + NL + "" + NL + "" + NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.all;" + NL + "" + NL + "LIBRARY altera_mf;" + NL + "USE altera_mf.all;" + NL + "" + NL + "ENTITY ";
  protected final String TEXT_4 = " IS" + NL + "\tPORT" + NL + "\t(" + NL + "\t\tclk\t\t\t: IN STD_LOGIC ;" + NL + "\t\trst\t\t\t: IN STD_LOGIC ;" + NL + "\t\tre\t\t\t: IN STD_LOGIC ;" + NL + "\t\twe\t\t\t: IN STD_LOGIC ;" + NL + "" + NL + "\t\t";
  protected final String TEXT_5 = NL + "\t\taddress\t\t:\tIN std_logic;" + NL + "\t\t";
  protected final String TEXT_6 = NL + "\t\taddress\t\t: \tIN std_logic_vector (";
  protected final String TEXT_7 = " DOWNTO 0);" + NL + "\t\t";
  protected final String TEXT_8 = NL + "\t" + NL + "" + NL + "\t\tdatain\t\t: IN STD_LOGIC_VECTOR (";
  protected final String TEXT_9 = " DOWNTO 0);" + NL + "\t\tdataout\t\t: OUT STD_LOGIC_VECTOR (";
  protected final String TEXT_10 = " DOWNTO 0)" + NL + "\t);" + NL + "END ";
  protected final String TEXT_11 = ";" + NL + "" + NL + "" + NL + "ARCHITECTURE SYN OF ";
  protected final String TEXT_12 = " IS" + NL + "" + NL + "\tSIGNAL sub_wire0\t: STD_LOGIC_VECTOR (";
  protected final String TEXT_13 = " DOWNTO 0);" + NL + "" + NL + "" + NL + "" + NL + "\tCOMPONENT altsyncram" + NL + "\tGENERIC (" + NL + "\t\tclock_enable_input_a\t\t: STRING;" + NL + "\t\tclock_enable_output_a\t\t: STRING;" + NL + "\t\tintended_device_family\t\t: STRING;" + NL + "\t\tlpm_hint\t\t: STRING;" + NL + "\t\tlpm_type\t\t: STRING;" + NL + "\t\tnumwords_a\t\t: NATURAL;" + NL + "\t\toperation_mode\t\t: STRING;" + NL + "\t\toutdata_aclr_a\t\t: STRING;" + NL + "\t\toutdata_reg_a\t\t: STRING;" + NL + "\t\tpower_up_uninitialized\t\t: STRING;" + NL + "\t\tread_during_write_mode_port_a\t\t: STRING;" + NL + "\t\twidthad_a\t\t: NATURAL;" + NL + "\t\twidth_a\t\t: NATURAL;" + NL + "\t\twidth_byteena_a\t\t: NATURAL" + NL + "\t);" + NL + "\tPORT (" + NL + "\t\t\twren_a\t: IN STD_LOGIC ;" + NL + "\t\t\tclock0\t: IN STD_LOGIC ;" + NL + "\t\t\taddress_a\t: IN STD_LOGIC_VECTOR (";
  protected final String TEXT_14 = " DOWNTO 0);" + NL + "\t\t\trden_a\t: IN STD_LOGIC ;" + NL + "\t\t\tq_a\t: OUT STD_LOGIC_VECTOR (";
  protected final String TEXT_15 = " DOWNTO 0);" + NL + "\t\t\tdata_a\t: IN STD_LOGIC_VECTOR (";
  protected final String TEXT_16 = " DOWNTO 0)" + NL + "\t);" + NL + "\tEND COMPONENT;" + NL + "" + NL + "BEGIN" + NL + "\tdataOut    <= sub_wire0(";
  protected final String TEXT_17 = " DOWNTO 0);" + NL + "" + NL + "\taltsyncram_component : altsyncram" + NL + "\tGENERIC MAP (" + NL + "\t\tclock_enable_input_a => \"BYPASS\"," + NL + "\t\tclock_enable_output_a => \"BYPASS\"," + NL + "\t\tintended_device_family => \"Stratix III\"," + NL + "\t\tlpm_hint => \"ENABLE_RUNTIME_MOD=NO\"," + NL + "\t\tlpm_type => \"altsyncram\"," + NL + "\t\tnumwords_a => ";
  protected final String TEXT_18 = "," + NL + "\t\toperation_mode => \"SINGLE_PORT\"," + NL + "\t\toutdata_aclr_a => \"NONE\"," + NL + "\t\toutdata_reg_a => \"UNREGISTERED\"," + NL + "\t\tpower_up_uninitialized => \"FALSE\"," + NL + "\t\tread_during_write_mode_port_a => \"OLD_DATA\"," + NL + "\t\twidthad_a => ";
  protected final String TEXT_19 = "," + NL + "\t\twidth_a => ";
  protected final String TEXT_20 = "," + NL + "\t\twidth_byteena_a => 1" + NL + "\t)" + NL + "\tPORT MAP (" + NL + "\t\twren_a => we," + NL + "\t\tclock0 => clk," + NL + "\t\taddress_a => address," + NL + "\t\trden_a => re," + NL + "\t\tdata_a => dataIn," + NL + "\t\tq_a => sub_wire0" + NL + "\t);" + NL + "" + NL + "" + NL + "" + NL + "END SYN;" + NL + "" + NL + "-- ============================================================" + NL + "-- CNX file retrieval info" + NL + "-- ============================================================" + NL + "-- Retrieval info: PRIVATE: ADDRESSSTALL_A NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: AclrAddr NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: AclrByte NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: AclrData NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: AclrOutput NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: BYTE_ENABLE NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: BYTE_SIZE NUMERIC \"8\"" + NL + "-- Retrieval info: PRIVATE: BlankMemory NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: CLOCK_ENABLE_INPUT_A NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: CLOCK_ENABLE_OUTPUT_A NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: Clken NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: DataBusSeparated NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: IMPLEMENT_IN_LES NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: INIT_FILE_LAYOUT STRING \"PORT_A\"" + NL + "-- Retrieval info: PRIVATE: INIT_TO_SIM_X NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: INTENDED_DEVICE_FAMILY STRING \"Stratix III\"" + NL + "-- Retrieval info: PRIVATE: JTAG_ENABLED NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: JTAG_ID STRING \"NONE\"" + NL + "-- Retrieval info: PRIVATE: MAXIMUM_DEPTH NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: MIFfilename STRING \"\"" + NL + "-- Retrieval info: PRIVATE: NUMWORDS_A NUMERIC \"";
  protected final String TEXT_21 = "\"" + NL + "-- Retrieval info: PRIVATE: RAM_BLOCK_TYPE NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: READ_DURING_WRITE_MODE_PORT_A NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: RegAddr NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: RegData NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: RegOutput NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: SYNTH_WRAPPER_GEN_POSTFIX STRING \"0\"" + NL + "-- Retrieval info: PRIVATE: SingleClock NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: UseDQRAM NUMERIC \"1\"" + NL + "-- Retrieval info: PRIVATE: WRCONTROL_ACLR_A NUMERIC \"0\"" + NL + "-- Retrieval info: PRIVATE: WidthAddr NUMERIC \"";
  protected final String TEXT_22 = "\"" + NL + "-- Retrieval info: PRIVATE: WidthData NUMERIC \"";
  protected final String TEXT_23 = "\"" + NL + "-- Retrieval info: PRIVATE: rden NUMERIC \"1\"" + NL + "-- Retrieval info: CONSTANT: CLOCK_ENABLE_INPUT_A STRING \"BYPASS\"" + NL + "-- Retrieval info: CONSTANT: CLOCK_ENABLE_OUTPUT_A STRING \"BYPASS\"" + NL + "-- Retrieval info: CONSTANT: INTENDED_DEVICE_FAMILY STRING \"Stratix III\"" + NL + "-- Retrieval info: CONSTANT: LPM_HINT STRING \"ENABLE_RUNTIME_MOD=NO\"" + NL + "-- Retrieval info: CONSTANT: LPM_TYPE STRING \"altsyncram\"" + NL + "-- Retrieval info: CONSTANT: NUMWORDS_A NUMERIC \"";
  protected final String TEXT_24 = "\"" + NL + "-- Retrieval info: CONSTANT: OPERATION_MODE STRING \"SINGLE_PORT\"" + NL + "-- Retrieval info: CONSTANT: OUTDATA_ACLR_A STRING \"NONE\"" + NL + "-- Retrieval info: CONSTANT: OUTDATA_REG_A STRING \"UNREGISTERED\"" + NL + "-- Retrieval info: CONSTANT: POWER_UP_UNINITIALIZED STRING \"FALSE\"" + NL + "-- Retrieval info: CONSTANT: READ_DURING_WRITE_MODE_PORT_A STRING \"OLD_DATA\"" + NL + "-- Retrieval info: CONSTANT: WIDTHAD_A NUMERIC \"";
  protected final String TEXT_25 = "\"" + NL + "-- Retrieval info: CONSTANT: WIDTH_A NUMERIC ";
  protected final String TEXT_26 = "\"" + NL + "-- Retrieval info: CONSTANT: WIDTH_BYTEENA_A NUMERIC \"1\"" + NL + "-- Retrieval info: USED_PORT: address 0 0 ";
  protected final String TEXT_27 = " 0 INPUT NODEFVAL address[";
  protected final String TEXT_28 = "..0]" + NL + "-- Retrieval info: USED_PORT: clock 0 0 0 0 INPUT VCC clock" + NL + "-- Retrieval info: USED_PORT: data 0 0 ";
  protected final String TEXT_29 = " 0 INPUT NODEFVAL data[";
  protected final String TEXT_30 = "..0]" + NL + "-- Retrieval info: USED_PORT: q 0 0 ";
  protected final String TEXT_31 = " 0 OUTPUT NODEFVAL q[";
  protected final String TEXT_32 = "..0]" + NL + "-- Retrieval info: USED_PORT: rden 0 0 0 0 INPUT VCC rden" + NL + "-- Retrieval info: USED_PORT: wren 0 0 0 0 INPUT NODEFVAL wren" + NL + "-- Retrieval info: CONNECT: @address_a 0 0 ";
  protected final String TEXT_33 = " 0 address 0 0 ";
  protected final String TEXT_34 = " 0" + NL + "-- Retrieval info: CONNECT: q 0 0 ";
  protected final String TEXT_35 = " 0 @q_a 0 0 ";
  protected final String TEXT_36 = " 0" + NL + "-- Retrieval info: CONNECT: @clock0 0 0 0 0 clock 0 0 0 0" + NL + "-- Retrieval info: CONNECT: @rden_a 0 0 0 0 rden 0 0 0 0" + NL + "-- Retrieval info: CONNECT: @data_a 0 0 ";
  protected final String TEXT_37 = " 0 data 0 0 ";
  protected final String TEXT_38 = " 0" + NL + "-- Retrieval info: CONNECT: @wren_a 0 0 0 0 wren 0 0 0 0" + NL + "-- Retrieval info: LIBRARY: altera_mf altera_mf.altera_mf_components.all" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port.vhd TRUE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port.inc FALSE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port.cmp TRUE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port.bsf FALSE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port_inst.vhd FALSE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port_waveforms.html TRUE" + NL + "-- Retrieval info: GEN_FILE: TYPE_NORMAL ram1port_wave*.jpg FALSE" + NL + "-- Retrieval info: LIB_FILE: altera_mf";
  protected final String TEXT_39 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	//skeleton="../generator.skeleton"
	//boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	SinglePortRam SPR = (SinglePortRam) project.getCurrentElement();
	
	int depth= (int) Math.pow(2,SPR.getAddressPort().getWidth());
	int addrDepth = SPR.getAddressPort().getWidth();
	int width=SPR.getOutput().getWidth();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(SPR.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(SPR.getName());
    stringBuffer.append(TEXT_4);
    
		if(SPR.getAddressPort().getWidth()==1){
		
    stringBuffer.append(TEXT_5);
    }
		else{
		
    stringBuffer.append(TEXT_6);
    stringBuffer.append((SPR.getAddressPort().getWidth()-1));
    stringBuffer.append(TEXT_7);
    
		}
		
    stringBuffer.append(TEXT_8);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_9);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_10);
    stringBuffer.append(SPR.getName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(SPR.getName());
    stringBuffer.append(TEXT_12);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_13);
    stringBuffer.append((addrDepth-1));
    stringBuffer.append(TEXT_14);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_15);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_16);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_17);
    stringBuffer.append(depth);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(addrDepth);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_20);
    stringBuffer.append(depth);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(addrDepth);
    stringBuffer.append(TEXT_22);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_23);
    stringBuffer.append(depth);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(addrDepth);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_26);
    stringBuffer.append((addrDepth));
    stringBuffer.append(TEXT_27);
    stringBuffer.append((addrDepth-1));
    stringBuffer.append(TEXT_28);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_29);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_30);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_31);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_32);
    stringBuffer.append((addrDepth));
    stringBuffer.append(TEXT_33);
    stringBuffer.append((addrDepth));
    stringBuffer.append(TEXT_34);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_35);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_36);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_37);
    stringBuffer.append((width));
    stringBuffer.append(TEXT_38);
    stringBuffer.append(TEXT_39);
    return stringBuffer.toString();
  }
}
