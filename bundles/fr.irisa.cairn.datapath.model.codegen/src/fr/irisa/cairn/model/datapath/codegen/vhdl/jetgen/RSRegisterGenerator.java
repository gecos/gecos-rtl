package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class RSRegisterGenerator
{
  protected static String nl;
  public static synchronized RSRegisterGenerator create(String lineSeparator)
  {
    nl = lineSeparator;
    RSRegisterGenerator result = new RSRegisterGenerator();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + "" + NL + "" + NL + "ENTITY RSLatch IS" + NL + "\tPORT" + NL + "\t(" + NL + "\t\tclk : in std_logic;" + NL + "\t  \tS : in std_logic;" + NL + "\t  \tR : in std_logic;" + NL + "\t  \tQ : out std_logic" + NL + "\t);" + NL + "END RSLatch;" + NL + "" + NL + "ARCHITECTURE RTL OF RSLatch IS" + NL + "" + NL + "BEGIN" + NL + "\tPROCESS (S,R,clk)" + NL + "\tBEGIN" + NL + "\t\tIF (clk'event AND clk = '1') THEN" + NL + "\t\t\tIF (R = '1') THEN" + NL + "\t\t\t\tQ <= '0';" + NL + "\t\t\tELSIF (S = '1') THEN" + NL + "\t\t\t\tQ <= '1';" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + "END rtl;";
  protected final String TEXT_3 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	//skeleton="../generator.skeleton"
	boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	Datapath datapath = (Datapath) project.getCurrentElement();
	
	//int depth=45;
	//int width=1;

    stringBuffer.append(TEXT_2);
    stringBuffer.append(TEXT_3);
    return stringBuffer.toString();
  }
}
