/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Target</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getAbstractTarget()
 * @model abstract="true"
 * @generated
 */
public interface AbstractTarget extends EObject {
} // AbstractTarget
