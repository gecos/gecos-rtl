package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class ShiftRegisterGenerator
{
  protected static String nl;
  public static synchronized ShiftRegisterGenerator create(String lineSeparator)
  {
    nl = lineSeparator;
    ShiftRegisterGenerator result = new ShiftRegisterGenerator();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + "" + NL + "" + NL + "ENTITY SREG_";
  protected final String TEXT_3 = "_";
  protected final String TEXT_4 = NL + "\tPORT" + NL + "\t(" + NL + "\t  \t\trst: in std_logic; " + NL + "\t  \t\tclk : in std_logic;" + NL + "\t  \t\tce: in std_logic; " + NL + "\t  \t\tD : in std_logic_vector(";
  protected final String TEXT_5 = " downto 0); " + NL + "\t  \t\tQ : out std_logic_vector(";
  protected final String TEXT_6 = " downto 0)" + NL + "\t);" + NL + "END SREG_";
  protected final String TEXT_7 = ";" + NL + "" + NL + "ARCHITECTURE RTL OF SREG_";
  protected final String TEXT_8 = " IS" + NL + "" + NL + "\tSIGNAL reg : std_logic_vector(";
  protected final String TEXT_9 = " DOWNTO 0);" + NL + "" + NL + "BEGIN" + NL + "" + NL + "\tPROCESS (rst,clk)" + NL + "\tBEGIN" + NL + "\t\tIF (rst='1') THEN" + NL + "\t\t\treg <= (others => '0');" + NL + "\t\tELSIF rising_edge(clk) then" + NL + "\t\t\tIF ce='1' THEN" + NL + "\t\t\t\treg <= D;" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + "" + NL + "\tQ <= reg;" + NL + "" + NL + "END rtl;";
  protected final String TEXT_10 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	
	//CodeGenerationProject project = (CodeGenerationProject) argument; 
	//ShiftRegister datapath = (Datapath) project.getCurrentElement();
	
	int depth=45;
	int width=12;

    stringBuffer.append(TEXT_2);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_3);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_4);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_5);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_6);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_3);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_7);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_3);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_8);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_9);
    stringBuffer.append(TEXT_10);
    return stringBuffer.toString();
  }
}
