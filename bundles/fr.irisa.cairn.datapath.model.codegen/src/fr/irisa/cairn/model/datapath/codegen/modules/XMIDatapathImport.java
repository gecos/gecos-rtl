package fr.irisa.cairn.model.datapath.codegen.modules;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenerator;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLReader;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLWriter;

public class XMIDatapathImport {
	
	String filename;
	Datapath datapath;
	
	public XMIDatapathImport(Datapath dp, String name) {
		this.datapath=dp;
		this.filename=name;
	}
	
	public void compute() {
		Display display;
		Shell shell;
		display = new Display();
		shell = new Shell(display);

		String outfilename  = datapath.getName()+"_gen.vhd";
		try {
			DatapathXMLWriter writer = DatapathXMLWriter.getXMLWriter();
			writer.save(datapath, filename);
		} catch (IOException e) {
			MessageBox messageBox = new MessageBox(shell.getShell(), SWT.OK);
			messageBox.setMessage("Problem during export, coudl not create "+filename+"/"+outfilename);
			messageBox.open();
		}

	}

}
