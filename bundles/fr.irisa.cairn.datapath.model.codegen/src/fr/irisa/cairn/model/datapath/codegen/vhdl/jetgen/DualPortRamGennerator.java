package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class DualPortRamGennerator
{
  protected static String nl;
  public static synchronized DualPortRamGennerator create(String lineSeparator)
  {
    nl = lineSeparator;
    DualPortRamGennerator result = new DualPortRamGennerator();
    nl = null;
    return result;
  }

  protected final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl; //$NON-NLS-1$
  protected final String TEXT_1 = NL;
  protected final String TEXT_2 = NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + NL + "ENTITY ram_dual_"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_3 = "_"; //$NON-NLS-1$
  protected final String TEXT_4 = " IS" + NL + "\tPORT" + NL + "\t(" + NL + "\t\tclock1, clock2: \t\tIN STD_LOGIC;" + NL + "\t\tdata: \t\t\t\t\tIN STD_LOGIC_VECTOR ("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
  protected final String TEXT_5 = " DOWNTO 0);" + NL + "\t\twrite_address: \t\t\tIN integer RANGE 0 to "; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_6 = ";" + NL + "\t\tread_address: \t\t\tIN integer RANGE 0 to "; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_7 = ";" + NL + "\t\twe:\t\t\t\t\t\tIN std_logic;" + NL + "\t\tq: \t\t\t\t\t\tOUT STD_LOGIC_VECTOR ("; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_8 = " DOWNTO 0)" + NL + "\t);" + NL + "END ram_dual_"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_9 = "_"; //$NON-NLS-1$
  protected final String TEXT_10 = ";" + NL + NL + "ARCHITECTURE rtl OF ram_dual_"; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_11 = "_"; //$NON-NLS-1$
  protected final String TEXT_12 = " IS" + NL + "\tTYPE MEM IS ARRAY(0 TO "; //$NON-NLS-1$ //$NON-NLS-2$
  protected final String TEXT_13 = ") OF std_logic_vector("; //$NON-NLS-1$
  protected final String TEXT_14 = " DOWNTO 0);" + NL + NL + "\tSIGNAL ram_block : MEM;" + NL + "\tSIGNAL read_address_reg : integer RANGE 0 to "; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
  protected final String TEXT_15 = ";" + NL + NL + "BEGIN" + NL + "\tPROCESS (clock1)" + NL + "\tBEGIN" + NL + "\t\tIF (clock1'event AND clock1 = '1') THEN" + NL + "\t\t\tIF (we = '1') THEN" + NL + "\t\t\t\tram_block(write_address) <= data;" + NL + "\t\t\tEND IF;" + NL + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + NL + "\tPROCESS (clock2)" + NL + "\tBEGIN" + NL + "\t\tIF (clock2'event AND clock2 = '1') THEN" + NL + "\t\t\tq <= ram_block(read_address_reg);" + NL + "\t\t\tread_address_reg <= read_address;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + NL + "END rtl;" + NL + "\t\t" + NL; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
 
	//skeleton="../generator.skeleton"
	//boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	//Datapath datapath = (Datapath) project.getCurrentElement();
	
	int depth=45;
	int width=12;

    stringBuffer.append(TEXT_2);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_3);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_4);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_5);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_6);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_7);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_8);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_9);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_10);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_11);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_12);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_13);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_14);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
