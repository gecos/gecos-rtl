/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cyclone II</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#getCycloneII()
 * @model
 * @generated
 */
public interface CycloneII extends AbstractTarget {
} // CycloneII
