package fr.irisa.cairn.model.datapath.codegen.c.operators;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InDataPort;


public class InlinedCOperators extends OperatorsSwitch<String>{


	public String generate(EObject theEObject) {
		return super.doSwitch(theEObject);
	}

	public InlinedCOperators() {
		super();
	}

	@Override
	public String caseExpandSigned(ExpandSigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseExpandUnsigned(ExpandUnsigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseQuantize(Quantize object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseReductionOperator(ReductionOperator object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseBinaryOperator(BinaryOperator binop) { 
		String opA = VHDLGenUtils.makeSourceName(binop,0);
		String opB = VHDLGenUtils.makeSourceName(binop,1);
		switch (binop.getOpcode().getValue()) {
			case BinaryOpcode.ADD_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" + "+opB; 
			case BinaryOpcode.SUB_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" - "+opB; 
			case BinaryOpcode.MUL_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" * "+opB; 
			case BinaryOpcode.DIV_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = div("+opA+" , "+opB+")";
			case BinaryOpcode.MAX_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = max("+opA+" , "+opB+")"; 
			case BinaryOpcode.MIN_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = min("+opA+" , "+opB+")";
			case BinaryOpcode.NAND_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = ! ("+opA+" & "+opB+")";
			case BinaryOpcode.XOR_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" ^ "+opB; 
			case BinaryOpcode.AND_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" & "+opB;
			case BinaryOpcode.OR_VALUE :
				return VHDLGenUtils.makeOutputName(binop)+" = "+opA+" | "+opB;	
			case BinaryOpcode.SHL_VALUE :
				StringBuffer res = new StringBuffer();
				opA = VHDLGenUtils.makeShlSourceName(binop,0);
				res.append(VHDLGenUtils.makeShlOutputName(binop)+" = "+opA);
				String str2 = binop.getOutput().getParentNode().getName()+"_"+binop.getOutput().getName()+"(0)";
				res.append(";\n\t"+str2);
				res.append(" <= "+opB);
				return res.toString();
			case BinaryOpcode.SHR_VALUE :
				//return VHDLGenUtils.makeOutputName(binop)+" <= "+opA+" >> "+opB;
				StringBuffer res1 = new StringBuffer();
				opA = VHDLGenUtils.makeShrSourceName(binop,0);
				res1.append(VHDLGenUtils.makeShrOutputName(binop)+" <= "+opA);
				int bw = binop.getOutput().getWidth();
				String str1 = binop.getOutput().getParentNode().getName()+"_"+binop.getOutput().getName()+"("+ (bw-1)+")";
				res1.append(";\n\t"+str1);
				res1.append(" <= "+opB);
				return res1.toString();
			default :
				throw new UnsupportedOperationException("Unsupport BinaryOpcode for VHDL synthesis: "+binop.getOpcode());
		}
	}

	@Override
	public String caseUnaryOperator(UnaryOperator unop) { 
		String sourceName = VHDLGenUtils.makeSourceName(unop,0);
		switch (unop.getOpcode().getValue()) {
			case UnaryOpcode.INV_VALUE:
				return VHDLGenUtils.makeOutputName(unop)+" = inv("+sourceName+")";
			case UnaryOpcode.NEG_VALUE :
				return VHDLGenUtils.makeOutputName(unop)+" = -("+sourceName+")";
			case UnaryOpcode.NOT_VALUE :
				return VHDLGenUtils.makeOutputName(unop)+" = not("+sourceName+")";
			case UnaryOpcode.SQRT_VALUE :
				return VHDLGenUtils.makeOutputName(unop)+" = sqrt("+sourceName+")";
			default :
				throw new UnsupportedOperationException("Unsupported Opcode for VHDL synthesis :"+unop.getOpcode());
		}
		
	}

	@Override
	public String caseBitSelect(BitSelect select) {
		String opA = VHDLGenUtils.makeSourceName(select,0);
		int upperBound = select.getUpperBound();
		int lowerBound = select.getLowerBound();
		
		return VHDLGenUtils.makeOutputName(select)+" <= "+opA+" ("+upperBound+" downto "+lowerBound+")";
	}
	
	@Override
	public String caseMerge(Merge merge) {
		String source1 = VHDLGenUtils.makeSourceName(merge,1);
		String source0 = VHDLGenUtils.makeSourceName(merge,0);
		
		return VHDLGenUtils.makeOutputName(merge)+" <= "+source1+" & "+source0;
	}
	
	@Override
	public String caseConstantValue(ConstantValue cst) {
		String binaryValue = VHDLGenUtils.binaryValue(cst.getValue(),cst.getOutput().getWidth());
		if (cst.getOutput().getWidth()==1) {
			return VHDLGenUtils.makeOutputName(cst)+" <= '"+binaryValue+"'"; 
		} else {
			return VHDLGenUtils.makeOutputName(cst)+" <= \""+binaryValue+"\""; 
		}
	}

	@Override
	public String caseDataFlowMux(DataFlowMux dmux ) {
		String source0 = VHDLGenUtils.makeSourceName(dmux,0);
		String source1 = VHDLGenUtils.makeSourceName(dmux,1);
		String source2 = VHDLGenUtils.makeSourceName(dmux,2);
		return VHDLGenUtils.makeOutputName(dmux)+" = ("+source2+")?"+source1+" : "+source0;
	}

	@Override
	public String caseControlFlowMux(ControlFlowMux cmux) {
		StringBuffer result = new StringBuffer();
		String select = VHDLGenUtils.makeSourceName((ActivableBlock)cmux,0);
		int ctrl_bw = VHDLGenUtils.bitwidth(cmux.getIn().size());
		if (cmux.getControlPort(0).getWidth()<ctrl_bw) {
			throw new RuntimeException("Inconsistent bitwidth in cmux !");
		}
		int offset=0;
		for (InDataPort ip : cmux.getIn()) {
			String source0 = VHDLGenUtils.makeSourceName((DataFlowBlock)cmux,offset);
			if (offset<cmux.getIn().size()-1) {
				result.append(source0+ " when "+select+ "= \'"+VHDLGenUtils.binaryValue(offset, ctrl_bw)+"\'\t");
			} else {
				result.append(" else "+source0);
			}
			offset++;
		}
		String source1 = VHDLGenUtils.makeSourceName((DataFlowBlock)cmux,1);
		return VHDLGenUtils.makeOutputName(cmux)+" <= "+result.toString();  
	}

	@Override
	public String caseCompare(Compare cmp) {
		String res = VHDLGenUtils.makeOutputName(cmp)+" =  ";
		String source0 = VHDLGenUtils.makeSourceName(cmp,0);
		String source1 = VHDLGenUtils.makeSourceName(cmp,1);
		
		switch (cmp.getOpcode().getValue()) {
			case CompareOpcode.EQU_VALUE: 
				res= res+"("+source0+"=="+source1+")";
				break; 
			case CompareOpcode.GT_VALUE:
				res= res+ "("+source0+">"+source1+")";
				break; 
			case CompareOpcode.GTE_VALUE:
				res= res+ "("+source0+">="+source1+")";
				break; 
			case CompareOpcode.LT_VALUE:
				res= res+ "("+source0+"<"+source1+")";
				break; 
			case CompareOpcode.LTE_VALUE:
				res= res+ "("+source0+"<="+source1+")";
				break; 
			case CompareOpcode.NEQ_VALUE:
				res= res+ "("+source0+"!="+source1+")";
				break; 
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+cmp.getOpcode());
		}
		res = res +" else '0';\n" ;
		return res;
	}
					
	@Override
	public String caseTernaryOperator(TernaryOperator terop) {
		String src0 = VHDLGenUtils.makeSourceName(terop,0);
		String src1 = VHDLGenUtils.makeSourceName(terop,1);
		String src2 = VHDLGenUtils.makeSourceName(terop,2);
		switch (terop.getOpcode().getValue()) {
			case TernaryOpcode.MULADD_VALUE :
				return VHDLGenUtils.makeOutputName(terop)+" <= ("+src0+"*"+src1+")+"+src2+" ;";
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+terop.getOpcode());
		}
	}
	
}
