package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.custom.BoolExpPrettyPrint;
import fr.irisa.cairn.model.datapath.port.*;

public class VHDLInputPredicate
{
  protected static String nl;
  public static synchronized VHDLInputPredicate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLInputPredicate result = new VHDLInputPredicate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = " false ";
  protected final String TEXT_2 = " true ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
      
	AbstractBooleanExpression predicate = (AbstractBooleanExpression) argument;
	if (predicate.isFalse()) {
			
    stringBuffer.append(TEXT_1);
    
	} else if (predicate.isTrue()) {
			
    stringBuffer.append(TEXT_2);
    
	} else {
		BoolExpPrettyPrint bepp = new BoolExpPrettyPrint(BoolExpPrettyPrint.VHDL);
		
    stringBuffer.append(bepp.prettyPrint(predicate));
    
	}
	
    return stringBuffer.toString();
  }
}
