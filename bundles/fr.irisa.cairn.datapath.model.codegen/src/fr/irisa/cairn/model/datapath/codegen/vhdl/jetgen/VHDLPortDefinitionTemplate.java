package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class VHDLPortDefinitionTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLPortDefinitionTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLPortDefinitionTemplate result = new VHDLPortDefinitionTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t-- global signals" + NL + "\t";
  protected final String TEXT_2 = NL + "\tclk : in std_logic;";
  protected final String TEXT_3 = NL + "\trst : in std_logic" + NL + "\t";
  protected final String TEXT_4 = NL + "\t-- Input Control Ports" + NL + "\t";
  protected final String TEXT_5 = NL + "\t";
  protected final String TEXT_6 = " : in std_logic\t";
  protected final String TEXT_7 = " : in std_logic_vector(";
  protected final String TEXT_8 = " downto 0) ";
  protected final String TEXT_9 = NL + "\t-- Input Data Ports " + NL + "\t\t";
  protected final String TEXT_10 = " : in std_logic ";
  protected final String TEXT_11 = " downto 0)";
  protected final String TEXT_12 = NL + "\t-- output Data Ports" + NL + "\t\t";
  protected final String TEXT_13 = " : out std_logic ";
  protected final String TEXT_14 = " : out std_logic_vector(";
  protected final String TEXT_15 = NL + "\t-- output Control Ports" + NL + "\t";
  protected final String TEXT_16 = NL + "\t-- global signals" + NL + "\t";
  protected final String TEXT_17 = NL + "\t\t";
  protected final String TEXT_18 = NL + "\t-- Input and Output Ports" + NL + "\t";
  protected final String TEXT_19 = "\t\tdataIn:\t\t\t\tIN std_logic_vector (";
  protected final String TEXT_20 = " DOWNTO 0);" + NL + "\t\twrite_address:\t\tIN std_logic_vector (";
  protected final String TEXT_21 = " DOWNTO 0);" + NL + "\t\tread_address0:\t\tIN std_logic_vector (";
  protected final String TEXT_22 = " DOWNTO 0);" + NL + "\t\tread_address1:\t\tIN std_logic_vector (";
  protected final String TEXT_23 = " DOWNTO 0);" + NL + "\t\twe:\t\t\t\t\tIN std_logic;  " + NL + "\t\tdataOut_0:\t\t\tOUT std_logic_vector (";
  protected final String TEXT_24 = " DOWNTO 0);" + NL + "\t\tdataOut_1:\t\t\tOUT std_logic_vector (";
  protected final String TEXT_25 = " DOWNTO 0)" + NL + "\t\t";
  protected final String TEXT_26 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
	boolean inlinedMapping = true;
	AbstractBlock block = (AbstractBlock) argument;
	boolean DPR4Xilinx = false; 


     if (DPR4Xilinx == false) {
     AbstractBlock component = (AbstractBlock) argument; 
    stringBuffer.append(TEXT_1);
     if (!block.isCombinational()) {
		
    stringBuffer.append(TEXT_2);
     		if(!(block instanceof SinglePortRom) && !(block instanceof RSRegister)) 
    stringBuffer.append(TEXT_3);
    
	}
	int lastPort = 0;
	ActivableBlock ablock = null;
	FlagBearerBlock fblock = null;
	DataFlowBlock dblock = null;
	
	if (block instanceof ActivableBlock) {
		ablock = (ActivableBlock) block;
		lastPort+=ablock.getActivate().size();
	}
	if (block instanceof FlagBearerBlock) {
		fblock = (FlagBearerBlock) block;
		lastPort+=fblock.getFlags().size();
	}
	if (block instanceof DataFlowBlock) {
		dblock = (DataFlowBlock) block;
		lastPort+=dblock.getIn().size()+dblock.getOut().size();
	}
	
	if(!(block instanceof SinglePortRom)){
		if (lastPort!=0) {
			
    stringBuffer.append(";\n");
    
		} else {
			
    stringBuffer.append("");
    
		}
	}
	
	int currentPort= 1;
	
    stringBuffer.append(TEXT_4);
     
	if (block instanceof ActivableBlock) {
		for (Iterator i = ablock.getActivate().iterator(); i.hasNext(); ) {
			InControlPort oport = (InControlPort) i.next();
			if (oport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_6);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
		
    stringBuffer.append(TEXT_9);
     
		if (block instanceof DataFlowBlock) {
		for (Iterator i = dblock.getIn().iterator(); i.hasNext(); ) {
			InDataPort iport = (InDataPort) i.next();
			if (iport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_10);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append((iport.getWidth()-1));
    stringBuffer.append(TEXT_11);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
		
    stringBuffer.append(TEXT_12);
     
		for (Iterator i = dblock.getOut().iterator(); i.hasNext(); ) {
			OutDataPort oport = (OutDataPort) i.next();
			if (oport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_13);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
	}
	
    stringBuffer.append(TEXT_15);
     
	if (block instanceof FlagBearerBlock) {
		for (Iterator i = fblock.getFlags().iterator(); i.hasNext(); ) {
			OutControlPort oport = (OutControlPort) i.next();
			if (oport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_13);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
	}
	}
	
    stringBuffer.append(TEXT_5);
     } else{
    stringBuffer.append(TEXT_5);
     AbstractBlock component = (AbstractBlock) argument; 
    stringBuffer.append(TEXT_16);
     if (!block.isCombinational()) {
		
    stringBuffer.append(TEXT_2);
     		if(!(block instanceof SinglePortRom) && !(block instanceof DualPortRam) ) 
    stringBuffer.append(TEXT_3);
    
	}
	int lastPort = 0;
	ActivableBlock ablock = null;
	FlagBearerBlock fblock = null;
	DataFlowBlock dblock = null;
	
	if (block instanceof ActivableBlock) {
		ablock = (ActivableBlock) block;
		lastPort+=ablock.getActivate().size();
	}
	if (block instanceof FlagBearerBlock) {
		fblock = (FlagBearerBlock) block;
		lastPort+=fblock.getFlags().size();
	}
	if (block instanceof DataFlowBlock) {
		dblock = (DataFlowBlock) block;
		lastPort+=dblock.getIn().size()+dblock.getOut().size();
	}
	if(!(block instanceof SinglePortRom) && !(block instanceof DualPortRam)){
		if (lastPort!=0) {
    stringBuffer.append(TEXT_17);
    stringBuffer.append(";\n");
    
		} else {
			
    stringBuffer.append("");
    
		}
	}
	
	int currentPort= 1;
	
    stringBuffer.append(TEXT_18);
     
	if (block instanceof ActivableBlock) {
		if(block instanceof DualPortRam){
			DualPortRam DPR = (DualPortRam) block;
			int depth= (int) Math.pow(2,DPR.getAddressPort(0).getWidth());
			int width= DPR.getWritePort(0).getWidth();

    stringBuffer.append(TEXT_19);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_20);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_21);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_22);
    stringBuffer.append((DPR.getAddressPort(0).getWidth()-1));
    stringBuffer.append(TEXT_23);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_24);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_25);
    		}
		else {
			for (Iterator i = ablock.getActivate().iterator(); i.hasNext(); ) {
				InControlPort oport = (InControlPort) i.next();
				if (oport.getWidth()==1) {
					
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_6);
    
				} else {
					
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
				}
				if (currentPort!=lastPort) {
					
    stringBuffer.append(";\n");
    
				} else {
				
    stringBuffer.append("\n");
    
				}
				currentPort++;
			}
		}
			
    stringBuffer.append(TEXT_9);
     
		if (block instanceof DataFlowBlock && !(block instanceof DualPortRam)) {
		for (Iterator i = dblock.getIn().iterator(); i.hasNext(); ) {
			InDataPort iport = (InDataPort) i.next();
			if (iport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_10);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(iport.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append((iport.getWidth()-1));
    stringBuffer.append(TEXT_11);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
		
    stringBuffer.append(TEXT_12);
     
		for (Iterator i = dblock.getOut().iterator(); i.hasNext(); ) {
			OutDataPort oport = (OutDataPort) i.next();
			if (oport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_13);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
	}
	
    stringBuffer.append(TEXT_15);
     
	if (block instanceof FlagBearerBlock) {
		for (Iterator i = fblock.getFlags().iterator(); i.hasNext(); ) {
			OutControlPort oport = (OutControlPort) i.next();
			if (oport.getWidth()==1) {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_13);
    
			} else {
				
    stringBuffer.append(TEXT_5);
    stringBuffer.append(oport.getName());
    stringBuffer.append(TEXT_14);
    stringBuffer.append((oport.getWidth()-1));
    stringBuffer.append(TEXT_8);
    
			}
			if (currentPort!=lastPort) {
				
    stringBuffer.append(";\n");
    
			} else {
				
    stringBuffer.append("\n");
    
			}
			currentPort++;
		}
	}
	}
	
    stringBuffer.append(TEXT_5);
    }
    stringBuffer.append(TEXT_26);
    stringBuffer.append(TEXT_26);
    return stringBuffer.toString();
  }
}