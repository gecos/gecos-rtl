package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.operators.*;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class VHDLInlinedOperatorMapping extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLInlinedOperatorMapping create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLInlinedOperatorMapping result = new VHDLInlinedOperatorMapping();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "   ";
  protected final String TEXT_2 = NL + "\t-- Operator mapping " + NL + " ";
  protected final String TEXT_3 = "\t" + NL + "\t";
  protected final String TEXT_4 = ";";
  protected final String TEXT_5 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    
	Datapath datapath = (Datapath) argument;

    stringBuffer.append(TEXT_2);
    
 
	InlinedVHDLOperators switchVisitor = new InlinedVHDLOperators();
  
	for (AbstractBlock block : datapath.getComponents()) {
		EObject eobject = (EObject ) block;
		if(eobject.eClass().getEPackage() instanceof OperatorsPackage ) {

    stringBuffer.append(TEXT_3);
    stringBuffer.append(switchVisitor.generate(eobject));
    stringBuffer.append(TEXT_4);
     
		}
	}

    stringBuffer.append(TEXT_5);
    stringBuffer.append(TEXT_5);
    return stringBuffer.toString();
  }
}