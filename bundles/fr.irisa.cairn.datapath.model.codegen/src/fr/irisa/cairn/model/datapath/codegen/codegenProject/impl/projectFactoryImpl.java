/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject.impl;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class projectFactoryImpl extends EFactoryImpl implements projectFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static projectFactory init() {
		try {
			projectFactory theprojectFactory = (projectFactory)EPackage.Registry.INSTANCE.getEFactory("codegenProject"); 
			if (theprojectFactory != null) {
				return theprojectFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new projectFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public projectFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case projectPackage.CODE_GENERATION_PROJECT: return createCodeGenerationProject();
			case projectPackage.CYCLONE_II: return createCycloneII();
			case projectPackage.STRATIX_II: return createStratixII();
			case projectPackage.STRATIX_III: return createStratixIII();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeGenerationProject createCodeGenerationProject() {
		CodeGenerationProjectImpl codeGenerationProject = new CodeGenerationProjectImpl();
		return codeGenerationProject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CycloneII createCycloneII() {
		CycloneIIImpl cycloneII = new CycloneIIImpl();
		return cycloneII;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StratixII createStratixII() {
		StratixIIImpl stratixII = new StratixIIImpl();
		return stratixII;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StratixIII createStratixIII() {
		StratixIIIImpl stratixIII = new StratixIIIImpl();
		return stratixIII;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public projectPackage getprojectPackage() {
		return (projectPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static projectPackage getPackage() {
		return projectPackage.eINSTANCE;
	}

} //projectFactoryImpl
