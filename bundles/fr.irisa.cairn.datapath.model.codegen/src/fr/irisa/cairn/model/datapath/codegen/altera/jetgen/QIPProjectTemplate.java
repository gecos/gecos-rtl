package fr.irisa.cairn.model.datapath.codegen.altera.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class QIPProjectTemplate
{
  protected static String nl;
  public static synchronized QIPProjectTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    QIPProjectTemplate result = new QIPProjectTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + "set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_st_bytes_to_packets/altera_avalon_st_bytes_to_packets.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_jtag_phy/altera_jtag_phy.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_st_idle_inserter/altera_avalon_st_idle_inserter.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_packets_to_bytes.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_jtag_phy/altera_jtag_dc_streaming.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_st_idle_remover/altera_avalon_st_idle_remover.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_sc_fifo.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_channel_adapter_0.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_jtag_interface.v]" + NL + " set_global_assignment -name SDC_FILE [file join $::quartus(qip_path) cpu_1.sdc]" + NL + "set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_sc_fifo/altera_avalon_sc_fifo.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_timing_adapter.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_channel_adapter_1.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_jtag_phy/altera_jtag_streaming.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_st_packets_to_bytes/altera_avalon_st_packets_to_bytes.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_packets_to_transactions_converter.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_packets_to_master/altera_avalon_packets_to_master.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_bytes_to_packets.v]" + NL + " set_global_assignment -name VHDL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_avalon_dc_fifo/altera_synchronizer.v]" + NL + " set_global_assignment -name SDC_FILE [file join $::quartus(qip_path) cpu.sdc]" + NL + "set_global_assignment -name TCL_FILE [file join $::quartus(qip_path) E:/soft/altera/80/ip/sopc_builder_ip/altera_jtag_avalon_master/altera_jtag_avalon_master_hw.tcl]";
  protected final String TEXT_2 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    stringBuffer.append(TEXT_2);
    return stringBuffer.toString();
  }
}
