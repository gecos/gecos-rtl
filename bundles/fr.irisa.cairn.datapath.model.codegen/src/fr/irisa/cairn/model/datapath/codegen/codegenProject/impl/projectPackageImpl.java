/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.codegen.codegenProject.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.AbstractTarget;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.CycloneII;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixII;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.StratixIII;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.projectFactory;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class projectPackageImpl extends EPackageImpl implements projectPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeGenerationProjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTargetEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cycloneIIEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stratixIIEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stratixIIIEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.codegen.codegenProject.projectPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private projectPackageImpl() {
		super(eNS_URI, projectFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static projectPackage init() {
		if (isInited) return (projectPackage)EPackage.Registry.INSTANCE.getEPackage(projectPackage.eNS_URI);

		// Obtain or create and register package
		projectPackageImpl theprojectPackage = (projectPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof projectPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new projectPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DatapathPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theprojectPackage.createPackageContents();

		// Initialize created meta-data
		theprojectPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theprojectPackage.freeze();

		return theprojectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeGenerationProject() {
		return codeGenerationProjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeGenerationProject_Name() {
		return (EAttribute)codeGenerationProjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeGenerationProject_Directory() {
		return (EAttribute)codeGenerationProjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeGenerationProject_Datapath() {
		return (EReference)codeGenerationProjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCodeGenerationProject_CurrentElement() {
		return (EReference)codeGenerationProjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeGenerationProject_Device() {
		return (EAttribute)codeGenerationProjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeGenerationProject_VhdlFiles() {
		return (EAttribute)codeGenerationProjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTarget() {
		return abstractTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCycloneII() {
		return cycloneIIEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStratixII() {
		return stratixIIEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStratixIII() {
		return stratixIIIEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public projectFactory getprojectFactory() {
		return (projectFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		codeGenerationProjectEClass = createEClass(CODE_GENERATION_PROJECT);
		createEAttribute(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__NAME);
		createEAttribute(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__DIRECTORY);
		createEReference(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__DATAPATH);
		createEReference(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__CURRENT_ELEMENT);
		createEAttribute(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__DEVICE);
		createEAttribute(codeGenerationProjectEClass, CODE_GENERATION_PROJECT__VHDL_FILES);

		abstractTargetEClass = createEClass(ABSTRACT_TARGET);

		cycloneIIEClass = createEClass(CYCLONE_II);

		stratixIIEClass = createEClass(STRATIX_II);

		stratixIIIEClass = createEClass(STRATIX_III);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cycloneIIEClass.getESuperTypes().add(this.getAbstractTarget());
		stratixIIEClass.getESuperTypes().add(this.getAbstractTarget());
		stratixIIIEClass.getESuperTypes().add(this.getAbstractTarget());

		// Initialize classes and features; add operations and parameters
		initEClass(codeGenerationProjectEClass, CodeGenerationProject.class, "CodeGenerationProject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodeGenerationProject_Name(), ecorePackage.getEString(), "name", null, 1, 1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeGenerationProject_Directory(), ecorePackage.getEString(), "directory", null, 1, 1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeGenerationProject_Datapath(), theDatapathPackage.getDatapath(), null, "datapath", null, 0, 1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCodeGenerationProject_CurrentElement(), theDatapathPackage.getAbstractBlock(), null, "currentElement", null, 0, 1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeGenerationProject_Device(), ecorePackage.getEString(), "device", null, 1, 1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCodeGenerationProject_VhdlFiles(), ecorePackage.getEString(), "vhdlFiles", null, 0, -1, CodeGenerationProject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractTargetEClass, AbstractTarget.class, "AbstractTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cycloneIIEClass, CycloneII.class, "CycloneII", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stratixIIEClass, StratixII.class, "StratixII", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(stratixIIIEClass, StratixIII.class, "StratixIII", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (getCodeGenerationProject_Name(), 
		   source, 
		   new String[] {
			 "constraints", "validFilename"
		   });		
		addAnnotation
		  (getCodeGenerationProject_Directory(), 
		   source, 
		   new String[] {
			 "constraints", "validDirectory"
		   });		
		addAnnotation
		  (getCodeGenerationProject_CurrentElement(), 
		   source, 
		   new String[] {
			 "constraints", "validElement"
		   });
	}

} //projectPackageImpl
