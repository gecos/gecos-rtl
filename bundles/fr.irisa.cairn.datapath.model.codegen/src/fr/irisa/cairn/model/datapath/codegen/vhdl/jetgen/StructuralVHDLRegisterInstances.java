package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class StructuralVHDLRegisterInstances extends VHDLGenUtils {

  protected static String nl;
  public static synchronized StructuralVHDLRegisterInstances create(String lineSeparator)
  {
    nl = lineSeparator;
    StructuralVHDLRegisterInstances result = new StructuralVHDLRegisterInstances();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + "\t\treg_";
  protected final String TEXT_3 = " : REG generic map (W=>";
  protected final String TEXT_4 = ") port map (clk,rst,";
  protected final String TEXT_5 = ",";
  protected final String TEXT_6 = ") ; ";
  protected final String TEXT_7 = NL + "\t\t\t\t\t" + NL + "\t\tregce_";
  protected final String TEXT_8 = " : REG_CE generic map (W=>";
  protected final String TEXT_9 = ") ; " + NL;
  protected final String TEXT_10 = NL + "\t\tsreg_";
  protected final String TEXT_11 = " : SHIFTREG generic map (W=>";
  protected final String TEXT_12 = ",DEPTH=>";
  protected final String TEXT_13 = "\t\t\t-- SR depth ";
  protected final String TEXT_14 = NL + "\t\tregce_";
  protected final String TEXT_15 = " \t";
  protected final String TEXT_16 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    	

	boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	Datapath datapath = (Datapath) project.getCurrentElement();
  
	for (AbstractBlock block : datapath.getComponents()) {
		if (block instanceof Register) {
			Register reg = (Register) block;
			EObject eobject = (EObject) block;
			String Q = VHDLGenUtils.makeOutputName(reg);
			String D  = VHDLGenUtils.makeSourceName((DataFlowBlock)reg,0);
			int width  = Math.max(reg.getOutput().getWidth(),reg.getInput().getWidth());
			switch(eobject.eClass().getClassifierID()) {
				case StoragePackage.REGISTER:
					
    stringBuffer.append(TEXT_2);
    stringBuffer.append(reg.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_6);
    
					break; 
				case StoragePackage.CE_REGISTER:
					String CE =VHDLGenUtils.makeSourceName((ActivableBlock)reg,0);; 
					
    stringBuffer.append(TEXT_7);
    stringBuffer.append(reg.getName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_9);
    
					break; 
				case StoragePackage.SHIFT_REGISTER:
					ShiftRegister sreg = (ShiftRegister) reg;
					CE =VHDLGenUtils.makeSourceName((ActivableBlock)reg,0);
					int depth  = sreg.getDepth();
					
					 if(depth>1) {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(reg.getName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_12);
    stringBuffer.append(depth);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_6);
    
					} else {

    stringBuffer.append(TEXT_13);
    stringBuffer.append(sreg.getDepth());
    stringBuffer.append(TEXT_14);
    stringBuffer.append(reg.getName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(width);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(CE);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(D);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(Q);
    stringBuffer.append(TEXT_9);
    
					}
					break; 
				default: 
			}
		}
	}

    stringBuffer.append(TEXT_15);
    stringBuffer.append(TEXT_16);
    return stringBuffer.toString();
  }
}