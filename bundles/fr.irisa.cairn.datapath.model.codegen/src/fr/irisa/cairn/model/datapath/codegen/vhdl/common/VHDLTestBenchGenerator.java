package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;


import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm.VHDLTestBenchTemplate;

public class VHDLTestBenchGenerator {
	
	VHDLTestBenchTemplate template = new VHDLTestBenchTemplate();
	static VHDLTestBenchGenerator  generator = null;
	
	public static VHDLTestBenchGenerator getInstance() {
		if (generator==null) {
			generator = new VHDLTestBenchGenerator();
		}
		return generator;
	}
	
	public void export(FSM fsm, String filename) throws FileNotFoundException {
		PrintStream stream = new PrintStream(filename);
		stream.append(template.generate(fsm));
		stream.close();
	}

	public void export(FSM fsm, File file) throws FileNotFoundException {
		PrintStream stream = new PrintStream(file);
		stream.append(template.generate(fsm));
		stream.close();
	}
 }
