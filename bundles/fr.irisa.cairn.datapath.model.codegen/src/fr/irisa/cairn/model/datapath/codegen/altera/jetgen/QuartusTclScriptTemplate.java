package fr.irisa.cairn.model.datapath.codegen.altera.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.io.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class QuartusTclScriptTemplate
{
  protected static String nl;
  public static synchronized QuartusTclScriptTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    QuartusTclScriptTemplate result = new QuartusTclScriptTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = " " + NL + "package require ::quartus::project" + NL + "" + NL + "set need_to_close_project 0" + NL + "set make_assignments 1" + NL + "" + NL + "# Check that the right project is open" + NL + "if {[is_project_open]} {" + NL + "\tif {[string compare $quartus(project) \"";
  protected final String TEXT_3 = "\"]} { " + NL + "\t\tputs \"Project ";
  protected final String TEXT_4 = " is not open\" " + NL + "\t\tset make_assignments 0" + NL + "\t}" + NL + "} else {" + NL + "\t# Only open if not already open" + NL + "\tif {[project_exists ";
  protected final String TEXT_5 = "]} {" + NL + "\t\tproject_open -revision ";
  protected final String TEXT_6 = " ";
  protected final String TEXT_7 = NL + "\t} else {" + NL + "\t\tproject_new -revision ";
  protected final String TEXT_8 = NL + "\t}" + NL + "\tset need_to_close_project 1" + NL + "}" + NL + "" + NL + "# Make assignments " + NL + "if {$make_assignments} {" + NL + "\tset_global_assignment -name FAMILY \"";
  protected final String TEXT_9 = "\"" + NL + "\tset_global_assignment -name DEVICE AUTO" + NL + "\tset_global_assignment -name TOP_LEVEL_ENTITY ";
  protected final String TEXT_10 = NL + "\tset_global_assignment -name ORIGINAL_QUARTUS_VERSION 8.0" + NL + "\tset_global_assignment -name PROJECT_CREATION_TIME_DATE \"15:30:52  JANUARY 16, 2009\"" + NL + "\tset_global_assignment -name LAST_QUARTUS_VERSION 8.0" + NL + "\tset_global_assignment -name EDA_DESIGN_ENTRY_SYNTHESIS_TOOL \"Synplify Pro\"" + NL + "\tset_global_assignment -name EDA_LMF_FILE synplcty.lmf -section_id eda_design_synthesis" + NL + "\tset_global_assignment -name EDA_INPUT_DATA_FORMAT VQM -section_id eda_design_synthesis" + NL + "\tset_global_assignment -name USE_GENERATED_PHYSICAL_CONSTRAINTS OFF -section_id eda_palace" + NL + "\tset_global_assignment -name DEVICE_FILTER_SPEED_GRADE FASTEST" + NL + "\t";
  protected final String TEXT_11 = " " + NL + "\tset_global_assignment -name VHDL_FILE ";
  protected final String TEXT_12 = NL + "\t\t\t";
  protected final String TEXT_13 = NL + "\t" + NL + "\tset_global_assignment -name ENABLE_ADVANCED_IO_TIMING ON" + NL + "\tset_global_assignment -name TIMEQUEST_MULTICORNER_ANALYSIS ON" + NL + "\tset_global_assignment -name TIMEQUEST_DO_CCPP_REMOVAL ON" + NL + "\tset_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top" + NL + "\tset_global_assignment -name PARTITION_COLOR 14622752 -section_id Top" + NL + "\tset_global_assignment -name LL_ROOT_REGION ON -section_id \"Root Region\"" + NL + "\tset_global_assignment -name LL_MEMBER_STATE LOCKED -section_id \"Root Region\"" + NL + "\tset_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top" + NL + "\t# Commit assignments" + NL + "\texport_assignments" + NL + "" + NL + "\t# Close project" + NL + "\tif {$need_to_close_project} {" + NL + "\t\tproject_close" + NL + "\t}" + NL + "}";
  protected final String TEXT_14 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
    

	CodeGenerationProject project = (CodeGenerationProject) argument; 
		 

    stringBuffer.append(TEXT_2);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_6);
    stringBuffer.append(project.getName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(project.getDevice());
    stringBuffer.append(TEXT_9);
    stringBuffer.append(project.getDatapath().getName());
    stringBuffer.append(TEXT_10);
    
		for(String filename : project.getVhdlFiles()) {
			
    stringBuffer.append(TEXT_11);
    stringBuffer.append(filename);
    stringBuffer.append(TEXT_12);
    
		}
	
    stringBuffer.append(TEXT_13);
    stringBuffer.append(TEXT_14);
    return stringBuffer.toString();
  }
}
