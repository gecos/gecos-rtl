package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.codegenProject.CodeGenerationProject;
import fr.irisa.cairn.model.datapath.codegen.vhdl.common.VHDLGenUtils;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class SinglePortRomGenerator
{
  protected static String nl;
  public static synchronized SinglePortRomGenerator create(String lineSeparator)
  {
    nl = lineSeparator;
    SinglePortRomGenerator result = new SinglePortRomGenerator();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "LIBRARY ieee;" + NL + "USE ieee.std_logic_1164.ALL;" + NL + "USE IEEE.STD_LOGIC_UNSIGNED.ALL;" + NL + "USE IEEE.STD_LOGIC_ARITH.ALL;" + NL + "USE IEEE.NUMERIC_STD.ALL;" + NL + "" + NL + "ENTITY ";
  protected final String TEXT_3 = " IS" + NL + "\tPORT" + NL + "\t(" + NL + "\t\tclk:\t\t\t \tIN STD_LOGIC;" + NL + "\t\tre:\t\t\t \t IN std_logic;" + NL + "\t\t";
  protected final String TEXT_4 = NL + "\t\taddress: \t\t\t\tIN std_logic;" + NL + "\t\t";
  protected final String TEXT_5 = NL + "\t\taddress: \t\t\t\tIN std_logic_vector (";
  protected final String TEXT_6 = " DOWNTO 0);" + NL + "\t\t";
  protected final String TEXT_7 = NL + "\t\tdataOut: \t\t\t\tOUT std_logic_vector (";
  protected final String TEXT_8 = " DOWNTO 0)" + NL + "\t);" + NL + "END ENTITY;" + NL + "" + NL + "ARCHITECTURE rtl OF ";
  protected final String TEXT_9 = " IS" + NL + "\tTYPE MEM IS ARRAY(0 TO ";
  protected final String TEXT_10 = ") OF std_logic_vector(";
  protected final String TEXT_11 = " DOWNTO 0);" + NL + "" + NL + "\tCONSTANT rom_block : MEM := (";
  protected final String TEXT_12 = NL + "\t\t\t\"";
  protected final String TEXT_13 = "\"";
  protected final String TEXT_14 = ",";
  protected final String TEXT_15 = NL + "\t\t\t\t\"";
  protected final String TEXT_16 = "\",";
  protected final String TEXT_17 = " \"00000000\", " + NL + "\t\t\t\t";
  protected final String TEXT_18 = NL + "\t\t\t\t\"00000000\"";
  protected final String TEXT_19 = NL + "\t);" + NL + "" + NL + "BEGIN" + NL + "\tPROCESS (clk)" + NL + "\tVARIABLE adr : integer RANGE 0 to ";
  protected final String TEXT_20 = ";" + NL + "\tBEGIN" + NL + "\t\tIF (clk'event AND clk = '1') THEN" + NL + "\t\t\tadr := CONV_INTEGER(address);" + NL + "\t\t\tIF(re = '1') THEN" + NL + "\t\t\t\tdataOut <= rom_block(adr);" + NL + "\t\t\tEND IF;" + NL + "\t\tEND IF;" + NL + "\tEND PROCESS;" + NL + "" + NL + "END rtl;" + NL + "\t\t";
  protected final String TEXT_21 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     
	//skeleton="../generator.skeleton"
	//boolean inlinedMapping = true;
	CodeGenerationProject project = (CodeGenerationProject) argument; 
	SinglePortRom SPR = (SinglePortRom) project.getCurrentElement();
	
	int depth= (int) Math.pow(2,SPR.getAddressPort().getWidth());
	byte[] contents = SPR.getContent();
	int width=8;
	String romName = SPR.getName();

    stringBuffer.append(TEXT_2);
    stringBuffer.append(romName);
    stringBuffer.append(TEXT_3);
    if(SPR.getAddressPort().getWidth()==1){
    stringBuffer.append(TEXT_4);
    }else{
    stringBuffer.append(TEXT_5);
    stringBuffer.append((SPR.getAddressPort().getWidth()-1));
    stringBuffer.append(TEXT_6);
    }
    stringBuffer.append(TEXT_7);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_8);
    stringBuffer.append(romName);
    stringBuffer.append(TEXT_9);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_10);
    stringBuffer.append((width-1));
    stringBuffer.append(TEXT_11);
     for (int i=0; i<depth; i++){
		if(contents.length == depth){
			String binValue = Integer.toBinaryString(contents[i]);
			if(binValue.length()>8)
					binValue = binValue.substring(24);
			String binValueFinal = "";
			for(int j=8; j>binValue.length(); j--)
				binValueFinal+="0";
			binValueFinal+=binValue;
			
    stringBuffer.append(TEXT_12);
    stringBuffer.append((binValueFinal));
    stringBuffer.append(TEXT_13);
    			if(i!=depth-1){
    stringBuffer.append(TEXT_14);
     			}else{
    stringBuffer.append("");
    			}
		}else{
			if(i<contents.length){ 
				String binValue = Integer.toBinaryString(contents[i]);
				if(binValue.length()>8)
					binValue = binValue.substring(24);
				String binValueFinal = "";
				for(int j=8; j>binValue.length(); j--)
					binValueFinal+="0";
				binValueFinal+=binValue;
				
    stringBuffer.append(TEXT_15);
    stringBuffer.append((binValueFinal));
    stringBuffer.append(TEXT_16);
     			}else if(i>=contents.length){
				if(i!=depth-1){
					
    stringBuffer.append(TEXT_17);
    
				}else{ 
				
    stringBuffer.append(TEXT_18);
    stringBuffer.append("");
    
				}
			}
		}	
	} 

    stringBuffer.append(TEXT_19);
    stringBuffer.append((depth-1));
    stringBuffer.append(TEXT_20);
    stringBuffer.append(TEXT_21);
    return stringBuffer.toString();
  }
}
