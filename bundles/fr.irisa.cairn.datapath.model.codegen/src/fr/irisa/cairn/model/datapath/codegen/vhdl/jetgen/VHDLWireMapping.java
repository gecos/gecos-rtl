package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.*;

public class VHDLWireMapping extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLWireMapping create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLWireMapping result = new VHDLWireMapping();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = NL + " ";
  protected final String TEXT_2 = NL + "\t\t";
  protected final String TEXT_3 = " <= ";
  protected final String TEXT_4 = ";";
  protected final String TEXT_5 = NL + "\t";
  protected final String TEXT_6 = NL;

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     	  

	Datapath datapath = (Datapath) argument;
	
	for (DataFlowWire wire : datapath.getDataWires() ) { 
		OutDataPort op = wire.getSource();
		String outId = op.getParentNode().getName()+"_"+op.getName();
		InDataPort ip = wire.getSink();
		if(ip==null) {
			throw new RuntimeException("Dangling "+wire+" to "+op);
		} else if (op==null) {
			throw new RuntimeException("Dangling "+wire+" from "+ip);
		} 
		if(ip.getParentNode()!=null) {
			String inId = ip.getParentNode().getName()+"_"+ip.getName();
			
    stringBuffer.append(TEXT_2);
    stringBuffer.append(inId);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outId);
    stringBuffer.append(TEXT_4);
    
		} else {
			throw new RuntimeException("orphan port "+ip);
		}
	}

	for (ControlFlowWire wire : datapath.getControlWires() ) { 
		OutControlPort op = wire.getSource();
		String outId = op.getParentNode().getName()+"_"+op.getName();
		InControlPort ip = wire.getSink();
		String inId = ip.getParentNode().getName()+"_"+ip.getName();
		
    stringBuffer.append(TEXT_5);
    stringBuffer.append(inId);
    stringBuffer.append(TEXT_3);
    stringBuffer.append(outId);
    stringBuffer.append(TEXT_4);
    
	}

    stringBuffer.append(TEXT_6);
    stringBuffer.append(TEXT_6);
    return stringBuffer.toString();
  }
}