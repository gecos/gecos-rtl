package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.datapath.custom.*;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.port.*;

public class VHDLCommandValue
{
  protected static String nl;
  public static synchronized VHDLCommandValue create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLCommandValue result = new VHDLCommandValue();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t\t-- Unsatisfiable predicate for command ";
  protected final String TEXT_2 = " <= ";
  protected final String TEXT_3 = "; ";
  protected final String TEXT_4 = "\t\t";
  protected final String TEXT_5 = " <= '";
  protected final String TEXT_6 = "';";
  protected final String TEXT_7 = "\t\t\t\t\t\t";
  protected final String TEXT_8 = " <= \"";
  protected final String TEXT_9 = "\";";
  protected final String TEXT_10 = " \t" + NL + "\t\t\t\t";
  protected final String TEXT_11 = ";" + NL + "\t\t\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
  
	State state = (State) argument; 
	for(Iterator i = state.getActivatedCommands().iterator(); i.hasNext(); ) {
		AbstractCommandValue t = (AbstractCommandValue) i.next();
		String value = "?";
		int decValue = 0;
		if (t instanceof BooleanCommandValue) {
			value = ((BooleanCommandValue) t).getValue().getName();
			if (value.equals("one")) value="1"; else value="0";
		} else {
			IntegerCommandValue icv = (IntegerCommandValue)t;
			int bw = icv.getCommand().getWidth();
			if(bw == 1)			
				value = ""+ VHDLGenUtils.binaryValue(icv.getValue(), bw)+"";
			else
			 	value = ""+ VHDLGenUtils.binaryValue(icv.getValue(), bw)+"";
		}
		
		if (t.getPredicate().isFalse()) {

    stringBuffer.append(TEXT_1);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_3);
    
		} else if (t.getPredicate().isTrue()) {
				if (t instanceof BooleanCommandValue) {

    stringBuffer.append(TEXT_4);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_6);
    				} else { 
					if(t.getCommand().getWidth() == 1) {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_6);
    					} else {

    stringBuffer.append(TEXT_7);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_8);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_9);
    					}
			}
		} else {
			BoolExpPrettyPrint bepp = new BoolExpPrettyPrint(BoolExpPrettyPrint.VHDL);
    	
			
    stringBuffer.append(TEXT_10);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_11);
    
		}
	}

    return stringBuffer.toString();
  }
}
