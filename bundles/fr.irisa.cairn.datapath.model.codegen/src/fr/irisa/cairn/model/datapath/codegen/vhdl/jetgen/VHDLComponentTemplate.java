package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.wires.*;
import fr.irisa.cairn.model.datapath.port.*;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.operators.*;

public class VHDLComponentTemplate extends VHDLGenUtils {

  protected static String nl;
  public static synchronized VHDLComponentTemplate create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLComponentTemplate result = new VHDLComponentTemplate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\tcomponent ";
  protected final String TEXT_2 = " is port (" + NL + "  \t\t ";
  protected final String TEXT_3 = NL + "  \t\t);" + NL + " \t\t end component; ";
  protected final String TEXT_4 = NL + "\t\t" + NL + "\t\tcomponent RAM16X1D" + NL + "   \t\t-- synthesis translate_off" + NL + "   \t\t\tgeneric (INIT : bit_vector := X\"16\");" + NL + "  \t \t-- synthesis translate_on" + NL + "   \t\tport (DPO  : out STD_ULOGIC;" + NL + "         \t  SPO  : out STD_ULOGIC;" + NL + "         \t  A0  : in STD_ULOGIC;" + NL + "         \t  A1  : in STD_ULOGIC;" + NL + "         \t  A2  : in STD_ULOGIC;" + NL + "        \t  A3   : in STD_ULOGIC;" + NL + "         \t  D   : in STD_ULOGIC;" + NL + "       \t\t  DPRA0 : in STD_ULOGIC;" + NL + "       \t\t  DPRA1 : in STD_ULOGIC;" + NL + "      \t\t  DPRA2 : in STD_ULOGIC;" + NL + "       \t\t  DPRA3 : in STD_ULOGIC;" + NL + "       \t\t  WCLK : in STD_ULOGIC;" + NL + "       \t\t  WE : in STD_ULOGIC);" + NL + "\t\tend component; " + NL;
  protected final String TEXT_5 = NL + "  \t\t);" + NL + " \t\t end component; " + NL;
  protected final String TEXT_6 = " " + NL;
  protected final String TEXT_7 = NL + " ";

	public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
       
	boolean inlinedMapping = true;
	AbstractBlock comp = (AbstractBlock) argument;
	boolean RAM16X1D = false;
	if (RAM16X1D == false){ 

    stringBuffer.append(TEXT_1);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(VHDLPortDefinitionTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_3);
    	} else { 
		if(comp instanceof DualPortRam){
    stringBuffer.append(TEXT_4);
    		}else{

    stringBuffer.append(TEXT_1);
    stringBuffer.append(comp.getName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(VHDLPortDefinitionTemplate.create("\n").generate(comp));
    stringBuffer.append(TEXT_5);
    		}		
	}
    stringBuffer.append(TEXT_6);
    stringBuffer.append(TEXT_7);
    return stringBuffer.toString();
  }
}