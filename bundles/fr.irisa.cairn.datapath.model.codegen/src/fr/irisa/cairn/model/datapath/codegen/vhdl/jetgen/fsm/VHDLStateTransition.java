package fr.irisa.cairn.model.datapath.codegen.vhdl.jetgen.fsm;

import fr.irisa.cairn.model.datapath.codegen.vhdl.common.*;
import fr.irisa.cairn.model.datapath.codegen.codegenProject.*;
import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.*;
import fr.irisa.cairn.model.datapath.port.*;

public class VHDLStateTransition
{
  protected static String nl;
  public static synchronized VHDLStateTransition create(String lineSeparator)
  {
    nl = lineSeparator;
    VHDLStateTransition result = new VHDLStateTransition();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t\t-- Default ? NS <= n";
  protected final String TEXT_2 = ";" + NL + "\t";
  protected final String TEXT_3 = NL + "\t\t\t\t\t-- Predicate ";
  protected final String TEXT_4 = NL + "\t\t\t";
  protected final String TEXT_5 = NL + "\t\t\t\t\t-- Unreachable state ";
  protected final String TEXT_6 = NL + "\t\t\t\t\t\tNS <= ";
  protected final String TEXT_7 = ";" + NL + "\t\t\t\t\t";
  protected final String TEXT_8 = NL + "\t\t\t\t\tif ";
  protected final String TEXT_9 = " then " + NL + "\t\t\t\t\t\tNS <= ";
  protected final String TEXT_10 = ";" + NL + "\t\t\t\t\tend if; ";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
 
	State state = (State) argument; 
	boolean first=true;
	
    stringBuffer.append(TEXT_1);
    stringBuffer.append(state.getLabel());
    stringBuffer.append(TEXT_2);
    
	if (state.getTransitions().size()==0) {
		throw new RuntimeException("Inconsistent FSM model in state "+state.getLabel());
	}
	for(Iterator i = state.getTransitions().iterator(); i.hasNext(); ) {
		Transition t  = (Transition) i.next();
		if(t.getPredicate()!=null) {
			
    stringBuffer.append(TEXT_3);
    stringBuffer.append(t.getPredicate().toString());
    stringBuffer.append(TEXT_4);
    
			if (t.getPredicate().isFalse()) {
			
    stringBuffer.append(TEXT_5);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_4);
    
			} else if (t.getPredicate().isTrue()) {
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_7);
    
			} else if (t.getPredicate().toString().equals("?")) {
				
    stringBuffer.append(TEXT_6);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_7);
    
			} else {
				
    stringBuffer.append(TEXT_8);
    stringBuffer.append(VHDLInputPredicate.create("\n").generate(t.getPredicate()) );
    stringBuffer.append(TEXT_9);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_10);
    
			}
		} else {
			throw new RuntimeException("Transition "+t+" has null predicate");
		}
		
	}

    return stringBuffer.toString();
  }
}
