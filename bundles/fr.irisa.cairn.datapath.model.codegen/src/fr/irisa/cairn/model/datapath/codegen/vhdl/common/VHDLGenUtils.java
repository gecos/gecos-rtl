package fr.irisa.cairn.model.datapath.codegen.vhdl.common;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

public class VHDLGenUtils {

	private int index=0;
	private String sep = ";";
	
	protected void resetSeparator(int size, String sep) {
		index = size;
	}
		
	protected String separator() throws ArrayIndexOutOfBoundsException {
		if (index>=0) {
			index--;
			return sep;
		} else if (index==0){
			index--;
			return "";
		}
		throw new ArrayIndexOutOfBoundsException();
	}

	public static String makeOutputName(SingleOutputDataFlowBlock block) {
		return  makeSinkName(block.getOutput());
	}
	
	public static String makeShlOutputName(SingleOutputDataFlowBlock block) {
		return  makeShlSinkName(block.getOutput());
	}
	
	public static String makeShlSinkName(OutDataPort op) {
		int bw = op.getWidth();
		return op.getParentNode().getName()+"_"+op.getName()+"("+(bw-1)+" downto 1)";
	}
	
	public static String makeShrOutputName(SingleOutputDataFlowBlock block) {
		return  makeShrSinkName(block.getOutput());
	}
	
	public static String makeShrSinkName(OutDataPort op) {
		int bw = op.getWidth();
		return op.getParentNode().getName()+"_"+op.getName()+"("+(bw-2)+" downto 0)";
	}
	
	public static String makeSourceName(DataFlowBlock block, int id) {
		return makeSourceName(block.getInput(id));
	}
	
	public static String makeShlSourceName(DataFlowBlock block, int id) {
		return makeShlSourceName(block.getInput(id));
	}
	
	public static String makeShrSourceName(DataFlowBlock block, int id) {
		return makeShrSourceName(block.getInput(id));
	}
	
	public static String makeSourceName(ActivableBlock block, int id) {
		return makeSourceName(block.getActivate().get(id));
	}

	public static String makeSinkName(OutDataPort op) {
		return op.getParentNode().getName()+"_"+op.getName();
	}

	public static String makeSinkName(OutControlPort op) {
		return op.getParentNode().getName()+"_"+op.getName();
	}

	public static String makeSourceName(InDataPort ip) {
		if (ip.getWire()!=null) {
			OutDataPort op = ip.getWire().getSource();
			DataFlowBlock parentNode = op.getParentNode();
			return parentNode.getName()+"_"+op.getName();
		} else {
			DataFlowBlock parentNode = ip.getParentNode();
			
			throw new NullPointerException("cannot build source name because InputPort "+ip+"("+parentNode.getClass().getSimpleName()+") is not wired");
		}
	}
	
	public static String makeShlSourceName(InDataPort ip) {
		if (ip.getWire()!=null) {
			OutDataPort op = ip.getWire().getSource();
			int bitwidth = op.getWidth();
			if (bitwidth == 1)
				return op.getParentNode().getName()+"_"+op.getName();
			else
				return op.getParentNode().getName()+"_"+op.getName()+"("+(bitwidth-2)+" downto 0)";
		} else {
			throw new NullPointerException("cannot build source name because InputPort "+ip+" is not wired");
		}
	}
	
	public static String makeShrSourceName(InDataPort ip) {
		if (ip.getWire()!=null) {
			OutDataPort op = ip.getWire().getSource();
			int bitwidth = op.getWidth();
			if (bitwidth == 1)
				return op.getParentNode().getName()+"_"+op.getName();
			else
				return op.getParentNode().getName()+"_"+op.getName()+"("+(bitwidth-1)+" downto 1)";
		} else {
			throw new NullPointerException("cannot build source name because InputPort "+ip+" is not wired");
		}
	}
	
	public static String makeSourceName(InControlPort ip) {
		if (ip.getWire()!=null) {
			OutControlPort op = ip.getWire().getSource();
			FlagBearerBlock parentNode = op.getParentNode();
			return parentNode.getName()+"_"+op.getName();
		} else {
			throw new NullPointerException("cannot build source name because InputPort "+ip+" is not wired");
		}
	}
	
	public static int bitwidth (int value){
		String bin = Integer.toBinaryString(value-1);
		int bw = bin.length();
		return bw;
	}
	
	public static String binaryValue(int value, int bitwitdh) {
		String bin = Integer.toBinaryString(value);
		int length= bin.length();
		if (length>bitwitdh) {
			return bin.substring(0, bitwitdh);
		} else {
			StringBuffer res = new StringBuffer(bin);
			for (int j=bitwitdh;j>length;j--) {
				res.append(res.charAt(length-1));
			}
			return res.toString();
			
		}
	}

}
