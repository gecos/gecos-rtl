package fr.irisa.cairn.model.fsm.codegen.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.model.fsm.*;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;


public class FSMModelXML {

	public static EList<EObject> loadFromFile(String filename) throws RuntimeException {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put(FsmPackage.eNS_URI,FsmPackage.eINSTANCE);

		// If there are no arguments, emit an appropriate usage message.
		//
		File file = new File(filename);
		URI uri = URI.createFileURI(file.getAbsolutePath());

		// Demand load resource for this file.
		//
		Resource resource = resourceSet.getResource(uri, true);
		System.out.println("Loaded " + uri);

		// Validate the contents of the loaded resource.
		//
		return resource.getContents();
	}

	public static void saveToFile(FSM fsm, String filename) throws FileNotFoundException, IOException {
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
		(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
		(FsmPackage.eNS_URI, 
				FsmPackage.eINSTANCE);

		File file = new File(filename);
		URI uri = URI.createFileURI(file.getAbsolutePath());

		Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(fsm);
		resource.save(new FileOutputStream(filename),null);
	}

	public static void main(String[] args) throws IOException {

	}
}


