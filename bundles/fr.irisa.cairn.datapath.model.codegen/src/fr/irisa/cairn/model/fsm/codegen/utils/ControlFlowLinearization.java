package fr.irisa.cairn.model.fsm.codegen.utils;
import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.model.fsm.*;


public class ControlFlowLinearization {
	
	boolean done=false;
	ArrayList<State> l;	
	FSM fsm;	

	public ControlFlowLinearization(FSM fsm) {
		l = new ArrayList<State>();
		this.fsm=fsm;
	}

	
	public List<State> getStateList() {
		if (!done) linearize(fsm.getStart(), l);
		return l;
	}
	
	private void linearize(State state,List<State> l) {
		l.add(state);
		List<State> next = state.getNextStates();
		while (next.size()==1) {
			State newState = next.get(0);
			
			if (l.contains(newState)) return;
			
			l.add(newState);
			next = newState.getNextStates();
		}

		for (State newState : next) {
			if (l.contains(newState)) return;
			linearize(newState ,l); 
		}
	}

}
