package fr.irisa.cairn.model.fsm.codegen.tests;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.model.datapath.codegen.DottyFSMExport;
import fr.irisa.cairn.model.fsm.*;


public class TestFSMDottyCodeGen {
	
	public static void main(String[] args) {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();
			
		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
				(Resource.Factory.Registry.DEFAULT_EXTENSION, 
				 new XMIResourceFactoryImpl());

			// Register the package to ensure it is available during loading.
			//
			resourceSet.getPackageRegistry().put
				(FsmPackage.eNS_URI, 
				 FsmPackage.eINSTANCE);
	        
			// If there are no arguments, emit an appropriate usage message.
			//
			if (args.length != 0) {
				// Iterate over all the arguments.
				//
				for (int i = 0; i < args.length; ++i) {
					// Construct the URI for the instance file.
					// The argument is treated as a file path only if it denotes an existing file.
					// Otherwise, it's directly treated as a URL.
					//
					File file = new File(args[i]);
					URI uri = file.isFile() ? URI.createFileURI(file.getAbsolutePath()): URI.createURI(args[0]);

					try {
						// Demand load resource for this file.
						//
						Resource resource = resourceSet.getResource(uri, true);
						System.out.println("Loaded " + uri);

						// Validate the contents of the loaded resource.
						//
						
						for (EObject eObject : resource.getContents()) {
							//DottyFSMExport  toto;//sav (eObject));
							//toto.saveas(filename)
						}
					}
					catch (RuntimeException exception) {
						System.out.println("Problem loading " + uri);
						exception.printStackTrace();
					}
				}
			}
		}
}
