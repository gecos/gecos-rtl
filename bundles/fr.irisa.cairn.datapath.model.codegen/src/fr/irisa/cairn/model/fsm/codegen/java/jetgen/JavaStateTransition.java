package fr.irisa.cairn.model.fsm.codegen.java.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.datapath.custom.BoolExpPrettyPrint;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.port.*;

public class JavaStateTransition
{
  protected static String nl;
  public static synchronized JavaStateTransition create(String lineSeparator)
  {
    nl = lineSeparator;
    JavaStateTransition result = new JavaStateTransition();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t\t\t\tif";
  protected final String TEXT_2 = " \t\t\t{ " + NL + "\t\t\t\t";
  protected final String TEXT_3 = " = ";
  protected final String TEXT_4 = ";" + NL + "\t\t\t";
  protected final String TEXT_5 = NL + "\t\t\t\t\t}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     


	State state = (State) argument; 
	for(Iterator i = state.getActivatedCommands().iterator(); i.hasNext(); ) {
		BooleanCommandValue t  = (BooleanCommandValue) i.next();
			
    stringBuffer.append(TEXT_1);
    
			
    stringBuffer.append(BoolExpPrettyPrint.print(t.getPredicate(),BoolExpPrettyPrint.JAVA) );
    stringBuffer.append(TEXT_2);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(t.getValue());
    stringBuffer.append(TEXT_4);
    
			
    stringBuffer.append(TEXT_5);
    
		}

    return stringBuffer.toString();
  }
}
