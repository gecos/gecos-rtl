package fr.irisa.cairn.model.fsm.codegen.msp430.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;

public class msp430InputPredicate
{
  protected static String nl;
  public static synchronized msp430InputPredicate create(String lineSeparator)
  {
    nl = lineSeparator;
    msp430InputPredicate result = new msp430InputPredicate();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = " false ";
  protected final String TEXT_2 = " true ";
  protected final String TEXT_3 = "(";
  protected final String TEXT_4 = " /* (((IN & ";
  protected final String TEXT_5 = ")==";
  protected final String TEXT_6 = ") && ((IN & ";
  protected final String TEXT_7 = ")==0))   */  ";
  protected final String TEXT_8 = "!";
  protected final String TEXT_9 = ")";
  protected final String TEXT_10 = " && ";
  protected final String TEXT_11 = " " + NL + "\t\t\t\t|| ";
  protected final String TEXT_12 = NL + "\t" + NL + "\t";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
/*

	int nmask= 0xFFFFFFFF;
	int mask= 0x0;
	SumOfProductPredicate predicate = (SumOfProductPredicate) argument;
	if (predicate.getProducts().size()==0) {
		if (predicate.isAlwaysFalse()) {
			
    stringBuffer.append(TEXT_1);
    
		} else {
			
    stringBuffer.append(TEXT_2);
    
		}
	} else {
		for (Iterator i = predicate.getProducts().iterator(); i.hasNext(); ) {
			ProductPredicate prodpred = (ProductPredicate) i.next();
			
			
			
    stringBuffer.append(TEXT_3);
     
			int id=0;
			for (Iterator j = prodpred.getTerms().iterator(); j.hasNext(); ) {
				PredicateTerm bterm = (PredicateTerm) j.next();
				if(bterm.isNegated()) {
					nmask = ((1<<id)) + nmask;
				} else {
					mask = (1<<id) + mask;
				}
				id++;
			}
				
    stringBuffer.append(TEXT_4);
    stringBuffer.append(mask);
    stringBuffer.append(TEXT_5);
    stringBuffer.append(mask);
    stringBuffer.append(TEXT_6);
    stringBuffer.append(nmask);
    stringBuffer.append(TEXT_7);
    

			for (Iterator j = prodpred.getTerms().iterator(); j.hasNext(); ) {
				PredicateTerm bterm = (PredicateTerm) j.next();
				
				
				
    stringBuffer.append(TEXT_3);
    
				if(bterm.isNegated()) {
				
					
    stringBuffer.append(TEXT_8);
    
				}
				
    stringBuffer.append(bterm.getFlag().getName());
    
				
    stringBuffer.append(TEXT_9);
    
				
				if(j.hasNext()) {
					
    stringBuffer.append(TEXT_10);
    
				}
				
			}
			
    stringBuffer.append(TEXT_9);
    
			if(i.hasNext()) {
				
    stringBuffer.append(TEXT_11);
    
			}
		}
	}
*/

    stringBuffer.append(TEXT_12);
    return stringBuffer.toString();
  }
}
