package fr.irisa.cairn.model.fsm.codegen.msp430.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;

public class msp430StateTransition
{
  protected static String nl;
  public static synchronized msp430StateTransition create(String lineSeparator)
  {
    nl = lineSeparator;
    msp430StateTransition result = new msp430StateTransition();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t\t// inconsistency in model ...";
  protected final String TEXT_2 = "\t\t\tgoto label_";
  protected final String TEXT_3 = ";";
  protected final String TEXT_4 = "\tif ";
  protected final String TEXT_5 = " {" + NL;
  protected final String TEXT_6 = "\t\tgoto label_";
  protected final String TEXT_7 = ";" + NL;
  protected final String TEXT_8 = "\t}" + NL;
  protected final String TEXT_9 = "\t// next default state is ";
  protected final String TEXT_10 = " " + NL;
  protected final String TEXT_11 = NL;
  protected final String TEXT_12 = "\tgoto label_";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
/*	State state = (State) ((Object[]) argument)[0]; 
	List<State> list = (List<State>) ((Object[]) argument)[1]; 
	boolean first=true;
	if (state.getTransitions().size()!=1) {
		for(Iterator i = state.getTransitions().iterator(); i.hasNext(); ) {
			Transition t  = (Transition) i.next();
			if (!first)	first=!first;
			if ((!first) || ((list.indexOf(t.getDst())-list.indexOf(state))!=1)) {
				if (t.getPredicate().getProducts().size()==0) {
					if (!t.getPredicate().isAlwaysFalse()) {
						
    		
    stringBuffer.append(TEXT_1);
    		
    stringBuffer.append(TEXT_2);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_3);
    
					}
				} else {
		
    stringBuffer.append(TEXT_4);
    stringBuffer.append(msp430InputPredicate.create("\n").generate(t.getPredicate()) );
    stringBuffer.append(TEXT_5);
    		
    stringBuffer.append(TEXT_6);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_7);
    		
    stringBuffer.append(TEXT_8);
    
				}
				
			} else {
		
    stringBuffer.append(TEXT_9);
    stringBuffer.append(t.getDst().getLabel());
    stringBuffer.append(TEXT_10);
    
			}
		}
	} else {
			
    stringBuffer.append(TEXT_9);
    stringBuffer.append(state.getTransitions().get(0).getDst().getLabel());
    stringBuffer.append(TEXT_11);
    
			if (list.indexOf(state)==(list.size()-1)) {
		
    stringBuffer.append(TEXT_12);
    stringBuffer.append(state.getTransitions().get(0).getDst().getLabel());
    stringBuffer.append(TEXT_7);
    			} 
	}

*/
    return stringBuffer.toString();
  }
}
