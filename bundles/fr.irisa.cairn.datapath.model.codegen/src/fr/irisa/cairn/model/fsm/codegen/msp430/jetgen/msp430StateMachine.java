package fr.irisa.cairn.model.fsm.codegen.msp430.jetgen;

import java.util.*;
import fr.irisa.cairn.model.fsm.codegen.utils.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.port.*;

public class msp430StateMachine
{
  protected static String nl;
  public static synchronized msp430StateMachine create(String lineSeparator)
  {
    nl = lineSeparator;
    msp430StateMachine result = new msp430StateMachine();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "#include<stdio.h>" + NL + "" + NL + "volatile int* pOut;" + NL + "#define OUT(a) ((*pOut)=a)" + NL + "" + NL + "int command;" + NL;
  protected final String TEXT_3 = NL + "volatile int* p";
  protected final String TEXT_4 = ";" + NL + "#define ";
  protected final String TEXT_5 = " (*p";
  protected final String TEXT_6 = ")" + NL + "\t\t";
  protected final String TEXT_7 = NL + "void fsm_fire() {";
  protected final String TEXT_8 = NL + "goto label_";
  protected final String TEXT_9 = ";";
  protected final String TEXT_10 = "label_";
  protected final String TEXT_11 = " :" + NL;
  protected final String TEXT_12 = "\t/* Commands  : */" + NL;
  protected final String TEXT_13 = NL;
  protected final String TEXT_14 = "\tOUT(command);" + NL;
  protected final String TEXT_15 = NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     FSM fsm = (FSM) argument; 
    stringBuffer.append(TEXT_2);
    
	for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
		InControlPort flag = (InControlPort) i.next();
		
    stringBuffer.append(TEXT_3);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_4);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_5);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_6);
    
	}

    stringBuffer.append(TEXT_7);
     	ControlFlowLinearization linearize =  new ControlFlowLinearization(fsm);
	State state = fsm.getStart();	
	List<State> list = linearize.getStateList();

    stringBuffer.append(TEXT_8);
    stringBuffer.append(fsm.getStart().getLabel());
    stringBuffer.append(TEXT_9);
    
			int is=0;
			for(Iterator i = list.iterator(); i.hasNext(); ) {
				state = (State) i.next();
    				
    stringBuffer.append(TEXT_10);
    stringBuffer.append(state.getLabel());
    stringBuffer.append(TEXT_11);
    				
    stringBuffer.append(TEXT_12);
    				
    stringBuffer.append(msp430CommandValue.create("\n").generate(state));
    stringBuffer.append(TEXT_13);
    				
    stringBuffer.append(TEXT_14);
    				
    stringBuffer.append(msp430StateTransition.create("\n").generate((Object)(new Object[]{state,list})));
    stringBuffer.append(TEXT_13);
    			}
    stringBuffer.append(TEXT_15);
    return stringBuffer.toString();
  }
}
