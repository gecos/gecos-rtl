package fr.irisa.cairn.model.fsm.codegen.java.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.datapath.port.*;

public class JavaStateMachine
{
  protected static String nl;
  public static synchronized JavaStateMachine create(String lineSeparator)
  {
    nl = lineSeparator;
    JavaStateMachine result = new JavaStateMachine();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "";
  protected final String TEXT_2 = NL + NL + "import *;" + NL + "" + NL + "public class FSM_";
  protected final String TEXT_3 = " {" + NL + "\t" + NL + "\tpublic enum ";
  protected final String TEXT_4 = "Enum {" + NL + "\t";
  protected final String TEXT_5 = "n";
  protected final String TEXT_6 = ",";
  protected final String TEXT_7 = NL + "\t};" + NL + "" + NL + "\tprotected ";
  protected final String TEXT_8 = "Enum state;" + NL + "\t" + NL + "\t";
  protected final String TEXT_9 = NL + "\tprotected boolean ";
  protected final String TEXT_10 = "Input; " + NL + "\t";
  protected final String TEXT_11 = NL + NL + "\t\t";
  protected final String TEXT_12 = "\tboolean  ";
  protected final String TEXT_13 = " ;";
  protected final String TEXT_14 = "\tint ";
  protected final String TEXT_15 = ";" + NL + "\t " + NL + "\t" + NL + "\tpublic void fire() {" + NL + "\t\t\tswitch (state) {" + NL + "\t\t\tcase idle ;" + NL + "\t\t\t\t\tstate = n";
  protected final String TEXT_16 = ";" + NL + "\t\t";
  protected final String TEXT_17 = NL + "\t\t\t\tbreak; " + NL + "\t\t\tcase n";
  protected final String TEXT_18 = " :";
  protected final String TEXT_19 = NL;
  protected final String TEXT_20 = NL + "\t\t\t\tbreak; " + NL + "\t\t\tcase others =>" + NL + "\t\t\t\t\tstate <= idle;" + NL + "\t\t\t}" + NL + "\t" + NL + "\t}" + NL + "}" + NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    stringBuffer.append(TEXT_1);
     FSM fsm = (FSM) argument; 
    stringBuffer.append(TEXT_2);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_3);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_4);
    
	for (Iterator i = fsm.getStates().iterator(); i.hasNext(); ) {
		State state = (State) i.next();
		
    stringBuffer.append(TEXT_5);
    stringBuffer.append(state.getLabel());
    if(i.hasNext()) {
    stringBuffer.append(TEXT_6);
    }
	}
    stringBuffer.append(TEXT_7);
    stringBuffer.append(fsm.getName());
    stringBuffer.append(TEXT_8);
     
	for (Iterator i = fsm.getActivate().iterator(); i.hasNext(); ) {
		InControlPort flag = (InControlPort) i.next();
		
    stringBuffer.append(TEXT_9);
    stringBuffer.append(flag.getName());
    stringBuffer.append(TEXT_10);
    ;
	}
	
    stringBuffer.append(TEXT_11);
    
	for (Iterator i = fsm.getFlags().iterator(); i.hasNext(); ) {
		OutControlPort command = (OutControlPort) i.next();
		if (command.getWidth()==1) {

    stringBuffer.append(TEXT_12);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_13);
    
		} else {

    stringBuffer.append(TEXT_14);
    stringBuffer.append(command.getName());
    stringBuffer.append(TEXT_13);
    
		}
	}
				
    stringBuffer.append(TEXT_15);
    stringBuffer.append(fsm.getStart().getLabel());
    stringBuffer.append(TEXT_16);
    
			for(Iterator i = fsm.getStates().iterator(); i.hasNext(); ) {
				State state = (State) i.next();
    stringBuffer.append(TEXT_17);
    stringBuffer.append(state.getLabel());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(JavaStateTransition.create("\n").generate(state));
    stringBuffer.append(TEXT_19);
    stringBuffer.append(JavaCommandValue.create("\n").generate(state));
    
		}
		
    stringBuffer.append(TEXT_20);
    stringBuffer.append(TEXT_19);
    return stringBuffer.toString();
  }
}
