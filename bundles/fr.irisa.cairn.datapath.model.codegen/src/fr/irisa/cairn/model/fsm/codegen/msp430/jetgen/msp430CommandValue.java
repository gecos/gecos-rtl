package fr.irisa.cairn.model.fsm.codegen.msp430.jetgen;

import java.util.*;
import org.eclipse.emf.common.util.EList;
import fr.irisa.cairn.model.fsm.*;

public class msp430CommandValue
{
  protected static String nl;
  public static synchronized msp430CommandValue create(String lineSeparator)
  {
    nl = lineSeparator;
    msp430CommandValue result = new msp430CommandValue();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "\t/* ";
  protected final String TEXT_2 = "=";
  protected final String TEXT_3 = " */";
  protected final String TEXT_4 = NL + "\tcommand = ";
  protected final String TEXT_5 = ";";
  protected final String TEXT_6 = NL + "\tif";
  protected final String TEXT_7 = " { " + NL + "\t\tcommand  = command || ";
  protected final String TEXT_8 = ";" + NL + "\t\t";
  protected final String TEXT_9 = NL + "\t}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     
/*
	State state = (State) argument;
	int value=0; 
	for(Iterator i = state.getActivatedCommands().iterator(); i.hasNext(); ) {
			BooleanCommandValue t  = (BooleanCommandValue) i.next();
if (t.getPredicate().getProducts().size()==0) {
		
    stringBuffer.append(TEXT_1);
    stringBuffer.append(t.getCommandName());
    stringBuffer.append(TEXT_2);
    stringBuffer.append(t.getValue());
    stringBuffer.append(TEXT_3);
     
			if (!t.getPredicate().isAlwaysFalse()) {
				int pos =  (1<<(state.getParent().getFlags().indexOf(t.getCommand())));
				value = value + pos;
			}
		}
	}
	
    stringBuffer.append(TEXT_4);
    stringBuffer.append(value);
    stringBuffer.append(TEXT_5);
    
	for(Iterator i = state.getActivatedCommands().iterator(); i.hasNext(); ) {
		BooleanCommandValue t  = (BooleanCommandValue) i.next();
		if (t.getPredicate().getProducts().size()!=0) {
	
    stringBuffer.append(TEXT_6);
    
		
    stringBuffer.append(msp430InputPredicate.create("\n").generate(t.getPredicate()));
     
			int pos = (1<<(state.getParent().getFlags().indexOf(t)));
			
		
    stringBuffer.append(TEXT_7);
    stringBuffer.append(pos);
    stringBuffer.append(TEXT_8);
    
		
    stringBuffer.append(TEXT_9);
    }
	}
	
*/

    return stringBuffer.toString();
  }
}
