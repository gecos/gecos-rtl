SC_MODULE ( clsArith )
{
sc_in <int> a ;
sc_in <int> b ;
sc_in <int> op ;
sc_in <int> S ;
sc_out <int> C ;
sc_out <int> D ;
sc_in <bool> clk ;
void fsm_run ( void ) ;
SC_CTOR (clsArith)
{
SC_METHOD ( fsm_run ) ;
sensitive << clk.pos() ;
InitialState = S_Init;
CurrentState = InitialState ;
}
private:
enum typeState={S_Init,S_OP,S_Add,S_Sub, S_Mul, S_Div, S_ILG, S_Done};
typeState CurrentState,InitialState, NextState;
} ;
void clsArith :: fsm_run ( void )
{
Switch (CurrentState)
{
case S_Add:
//Action
C=a+b;
//Next State
NextState = S_Done ;
break ;
case S_OP:
//Action
//Next State
if ( op==1 ) NextState = S_Add ;
else if (op==3) NextState = S_Mul ;
else if (op==2) NextState = S_Sub ;
else if (op==4) NextState = S_ChkDen ;
else NextState = S_Init ;
break ;
case S_Init:
//Action
D=0;
//Next State
if ( S==1 ) NextState = S_OP ;
else NextState = S_Init ;
break ;
case S_ILG:
//Action
//Next State
NextState = S_Done ;
break ;
case S_Done:
//Action
D=1;
//Next State
NextState = S_Init ;
break ;
case S_ChkDen:
//Action
//Next State
if (b==0) NextState = S_ILG ;
else NextState = S_Div ;
break ;
case S_Mul:
//Action
C=a*b;
//Next State
NextState = S_Done ;
break ;
case S_Sub:
//Action
C=a-b;
//Next State
NextState = S_Done ;
break ;
case S_Div:
//Action
C=a/b;
//Next State
NextState = S_Done ;
break ;
//Trap State, the state machine will
//terminate on any illegal state
default:
NextState = S_Done ;
} //switch
//update Current State
CurrentState = NextState ;
}