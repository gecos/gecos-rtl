package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm;

import com.google.common.base.Objects;
import common.CommonHelper;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanValue;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class FSMVerilogTemplate {
  protected CharSequence _generatePortDefinition(final Port p) {
    return null;
  }
  
  protected CharSequence _generatePortDefinition(final InControlPort p) {
    CharSequence _xifexpression = null;
    int _width = p.getWidth();
    boolean _greaterThan = (_width > 1);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("input [");
      int _width_1 = p.getWidth();
      int _minus = (_width_1 - 1);
      _builder.append(_minus);
      _builder.append(":0] ");
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(" ;");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("input  ");
      String _name_1 = p.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" ;");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePortDefinition(final OutControlPort p) {
    CharSequence _xifexpression = null;
    int _width = p.getWidth();
    boolean _greaterThan = (_width > 1);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("ouput [");
      int _width_1 = p.getWidth();
      int _minus = (_width_1 - 1);
      _builder.append(_minus);
      _builder.append(":0] ");
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(" ;");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("ouput  ");
      String _name_1 = p.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" ;");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePredicate(final AbstractBooleanExpression expr) {
    return null;
  }
  
  protected CharSequence _generatePredicate(final IndexedBooleanFlagTerm expr) {
    CharSequence _xifexpression = null;
    boolean _isNegated = expr.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = expr.getFlag().getName();
      _builder.append(_name);
      _builder.append("[]");
      int _offset = expr.getOffset();
      _builder.append(_offset);
      _builder.append("]==0\'b");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = expr.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("[");
      int _offset_1 = expr.getOffset();
      _builder_1.append(_offset_1);
      _builder_1.append("]==1\'b");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePredicate(final IntegerFlagTerm expr) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = expr.getFlag().getName();
    _builder.append(_name);
    _builder.append("==");
    String _binaryValue = CommonHelper.binaryValue(expr.getValue(), expr.getFlag().getWidth());
    _builder.append(_binaryValue);
    _builder.append("\'b");
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final OrExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractBooleanExpression> _terms = expr.getTerms();
      boolean _hasElements = false;
      for(final AbstractBooleanExpression t : _terms) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(" || ", "");
        }
        Object _generatePredicate = this.generatePredicate(t);
        _builder.append(_generatePredicate);
      }
    }
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final AndExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<AbstractBooleanExpression> _terms = expr.getTerms();
      boolean _hasElements = false;
      for(final AbstractBooleanExpression t : _terms) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(" && ", "");
        }
        Object _generatePredicate = this.generatePredicate(t);
        _builder.append(_generatePredicate);
      }
    }
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final NegateExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("~(");
    Object _generatePredicate = this.generatePredicate(expr.getTerm());
    _builder.append(_generatePredicate);
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final BooleanFlagTerm expr) {
    CharSequence _xifexpression = null;
    boolean _isNegated = expr.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = expr.getFlag().getName();
      _builder.append(_name);
      _builder.append("==0\'b");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = expr.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("==1\'b");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence generateTransition(final Transition t) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = t.getPredicate();
    boolean _equals = Objects.equal(_predicate, null);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("state = ");
      String _label = t.getDst().getLabel();
      _builder.append(_label);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("if ");
      CharSequence _generatePredicate = this.generatePredicate(t.getPredicate());
      _builder_1.append(_generatePredicate);
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("begin");
      _builder_1.newLine();
      _builder_1.append("\t");
      _builder_1.append("state = ");
      String _label_1 = t.getDst().getLabel();
      _builder_1.append(_label_1, "\t");
      _builder_1.append(";");
      _builder_1.newLineIfNotEmpty();
      _builder_1.append("end");
      _builder_1.newLine();
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateCommand(final AbstractCommandValue command) {
    return null;
  }
  
  protected CharSequence _generateCommand(final BooleanCommandValue command) {
    CharSequence _xblockexpression = null;
    {
      String res = "";
      BooleanValue _value = command.getValue();
      if (_value != null) {
        switch (_value) {
          case ONE:
            String _name = command.getCommand().getName();
            String _plus = (_name + " = 1\'b");
            res = _plus;
            break;
          case ZERO:
            String _name_1 = command.getCommand().getName();
            String _plus_1 = (_name_1 + " = 0\'b");
            res = _plus_1;
            break;
          case DONT_CARE:
            String _name_2 = command.getCommand().getName();
            String _plus_2 = (_name_2 + "`DONTCARE");
            res = _plus_2;
            break;
          default:
            break;
        }
      }
      CharSequence _xifexpression = null;
      AbstractBooleanExpression _predicate = command.getPredicate();
      boolean _equals = Objects.equal(_predicate, null);
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(res);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _xifexpression = _builder;
      } else {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("if ");
        CharSequence _generatePredicate = this.generatePredicate(command.getPredicate());
        _builder_1.append(_generatePredicate);
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("begin");
        _builder_1.newLine();
        _builder_1.append("\t");
        _builder_1.append(res, "\t");
        _builder_1.append(";");
        _builder_1.newLineIfNotEmpty();
        _builder_1.append("end");
        _builder_1.newLine();
        _xifexpression = _builder_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateCommand(final IntegerCommandValue command) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = command.getPredicate();
    boolean _equals = Objects.equal(_predicate, null);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(" ");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append(" ");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence genPortList(final List<Port> ports) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _hasElements = false;
      for(final Port p : ports) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "");
        }
        String _name = p.getName();
        _builder.append(_name);
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public void generateCode(final FSM fsm, final String filename) {
    try {
      final PrintStream ps = new PrintStream(filename);
      ps.append(this.generate(fsm));
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void generateCode(final FSM fsm) {
    String _name = fsm.getName();
    String _plus = ("fsm" + _name);
    String _plus_1 = (_plus + ".v");
    this.generateCode(fsm, _plus_1);
  }
  
  public CharSequence generate(final FSM fsm) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("//`define idle   5\'b00000");
    _builder.newLine();
    _builder.append("//`define B410_0 5\'b00001");
    _builder.newLine();
    _builder.append("//`define B421_0 5\'b00010");
    _builder.newLine();
    _builder.append("//`define B421_1 5\'b00011");
    _builder.newLine();
    _builder.append("//`define B412_0 5\'b00100");
    _builder.newLine();
    _builder.append("//`define B412_1 5\'b00101");
    _builder.newLine();
    _builder.append("//`define B413_0 5\'b00110");
    _builder.newLine();
    _builder.append("//`define B413_1 5\'b00111");
    _builder.newLine();
    _builder.append("//`define B413_2 5\'b01000");
    _builder.newLine();
    _builder.append("//`define B413_3 5\'b01001");
    _builder.newLine();
    _builder.append("//`define BB_13_0_0 5\'b01010");
    _builder.newLine();
    _builder.append("//`define BB_14_0_0 5\'b01011");
    _builder.newLine();
    _builder.append("//`define B415_0 5\'b01100");
    _builder.newLine();
    _builder.append("//`define B415_1 5\'b01101");
    _builder.newLine();
    _builder.append("//`define B415_2 5\'b01110");
    _builder.newLine();
    _builder.append("//`define B417_0 5\'b01111");
    _builder.newLine();
    _builder.append("//`define B411_0 5\'b10000");
    _builder.newLine();
    _builder.append("//`define B411_1 5\'b10001");
    _builder.newLine();
    _builder.append("//`define B411_2 5\'b10010");
    _builder.newLine();
    _builder.append("//`define B411_3 5\'b10011");
    _builder.newLine();
    _builder.newLine();
    _builder.append("module ");
    String _name = fsm.getName();
    _builder.append(_name);
    _builder.append(" ( clk, rst, ");
    EList<InControlPort> _activate = fsm.getActivate();
    CharSequence _genPortList = this.genPortList(((List) _activate));
    _builder.append(_genPortList);
    _builder.append(", ");
    EList<OutControlPort> _flags = fsm.getFlags();
    CharSequence _genPortList_1 = this.genPortList(((List) _flags));
    _builder.append(_genPortList_1);
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("input clk, rst;");
    _builder.newLine();
    _builder.newLine();
    {
      EList<InControlPort> _activate_1 = fsm.getActivate();
      for(final InControlPort ip : _activate_1) {
        CharSequence _generatePortDefinition = this.generatePortDefinition(ip);
        _builder.append(_generatePortDefinition);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      EList<OutControlPort> _flags_1 = fsm.getFlags();
      for(final OutControlPort op : _flags_1) {
        CharSequence _generatePortDefinition_1 = this.generatePortDefinition(op);
        _builder.append(_generatePortDefinition_1);
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("reg [`NBITS-1:0] state;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("always @( posedge clk, posedge rst )");
    _builder.newLine();
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if( rst )");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("cs <= ");
    String _label = fsm.getStart().getLabel();
    _builder.append(_label, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("case( state )");
    _builder.newLine();
    {
      EList<State> _states = fsm.getStates();
      for(final State state : _states) {
        _builder.append("\t\t\t");
        String _upperCase = state.getLabel().toUpperCase();
        _builder.append(_upperCase, "\t\t\t");
        _builder.append("_CONST: //2\'b00");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t");
        _builder.append("begin");
        _builder.newLine();
        {
          EList<Transition> _transitions = state.getTransitions();
          for(final Transition t : _transitions) {
            _builder.append("\t\t\t");
            _builder.append("\t");
            CharSequence _generateTransition = this.generateTransition(t);
            _builder.append(_generateTransition, "\t\t\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t\t\t");
        _builder.append("end");
        _builder.newLine();
      }
    }
    _builder.append("\t\t");
    _builder.append("end");
    _builder.newLine();
    _builder.append("end");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("always @(posedge clk, posedge rst)");
    _builder.newLine();
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("if( rst )");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("outp <= 0;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("else");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("begin ");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("case( state )");
    _builder.newLine();
    {
      EList<State> _states_1 = fsm.getStates();
      for(final State state_1 : _states_1) {
        _builder.append("\t\t\t\t");
        String _upperCase_1 = state_1.getLabel().toUpperCase();
        _builder.append(_upperCase_1, "\t\t\t\t");
        _builder.append("_CONST: ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t\t");
        _builder.append("begin");
        _builder.newLine();
        {
          EList<AbstractCommandValue> _activatedCommands = state_1.getActivatedCommands();
          for(final AbstractCommandValue command : _activatedCommands) {
            _builder.append("\t\t\t\t");
            CharSequence _generateCommand = this.generateCommand(command);
            _builder.append(_generateCommand, "\t\t\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
        _builder.append("\t\t\t\t");
        _builder.append("end");
        _builder.newLine();
      }
    }
    _builder.append("\t\t\t");
    _builder.append("endcase");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end");
    _builder.newLine();
    _builder.append("end");
    _builder.newLine();
    _builder.newLine();
    _builder.append("endmodule");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence generatePortDefinition(final Port p) {
    if (p instanceof InControlPort) {
      return _generatePortDefinition((InControlPort)p);
    } else if (p instanceof OutControlPort) {
      return _generatePortDefinition((OutControlPort)p);
    } else if (p != null) {
      return _generatePortDefinition(p);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(p).toString());
    }
  }
  
  public CharSequence generatePredicate(final AbstractBooleanExpression expr) {
    if (expr instanceof IndexedBooleanFlagTerm) {
      return _generatePredicate((IndexedBooleanFlagTerm)expr);
    } else if (expr instanceof AndExpression) {
      return _generatePredicate((AndExpression)expr);
    } else if (expr instanceof BooleanFlagTerm) {
      return _generatePredicate((BooleanFlagTerm)expr);
    } else if (expr instanceof IntegerFlagTerm) {
      return _generatePredicate((IntegerFlagTerm)expr);
    } else if (expr instanceof OrExpression) {
      return _generatePredicate((OrExpression)expr);
    } else if (expr instanceof NegateExpression) {
      return _generatePredicate((NegateExpression)expr);
    } else if (expr != null) {
      return _generatePredicate(expr);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(expr).toString());
    }
  }
  
  public CharSequence generateCommand(final AbstractCommandValue command) {
    if (command instanceof BooleanCommandValue) {
      return _generateCommand((BooleanCommandValue)command);
    } else if (command instanceof IntegerCommandValue) {
      return _generateCommand((IntegerCommandValue)command);
    } else if (command != null) {
      return _generateCommand(command);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(command).toString());
    }
  }
}
