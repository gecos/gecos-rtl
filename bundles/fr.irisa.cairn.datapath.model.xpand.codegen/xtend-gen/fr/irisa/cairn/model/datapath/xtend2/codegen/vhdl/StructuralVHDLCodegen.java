package fr.irisa.cairn.model.datapath.xtend2.codegen.vhdl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import java.util.Arrays;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class StructuralVHDLCodegen {
  public CharSequence stdLogicType(final Integer width) {
    CharSequence _xifexpression = null;
    if (((width).intValue() == 1)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("std_logic");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("std_logic_vector(");
      _builder_1.append(((width).intValue() - 1));
      _builder_1.append(" downto 0)");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _genPortDeclaration(final InControlPort obj) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" : in ");
    CharSequence _stdLogicType = this.stdLogicType(Integer.valueOf(obj.getWidth()));
    _builder.append(_stdLogicType);
    return _builder;
  }
  
  protected CharSequence _genPortDeclaration(final OutControlPort obj) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" : out ");
    CharSequence _stdLogicType = this.stdLogicType(Integer.valueOf(obj.getWidth()));
    _builder.append(_stdLogicType);
    return _builder;
  }
  
  protected CharSequence _genPortDeclaration(final InDataPort obj) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" : in ");
    CharSequence _stdLogicType = this.stdLogicType(Integer.valueOf(obj.getWidth()));
    _builder.append(_stdLogicType);
    return _builder;
  }
  
  protected CharSequence _genPortDeclaration(final OutDataPort obj) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" : out ");
    CharSequence _stdLogicType = this.stdLogicType(Integer.valueOf(obj.getWidth()));
    _builder.append(_stdLogicType);
    return _builder;
  }
  
  public EList<Port> getPortList(final AbstractBlock obj) {
    EList<Port> ports = new BasicEList<Port>();
    if ((obj instanceof ActivableBlock)) {
      ports.addAll(((ActivableBlock) obj).getActivate());
    }
    if ((obj instanceof FlagBearerBlock)) {
      ports.addAll(((FlagBearerBlock) obj).getFlags());
    }
    if ((obj instanceof DataFlowBlock)) {
      ports.addAll(((DataFlowBlock) obj).getIn());
      ports.addAll(((DataFlowBlock) obj).getOut());
    }
    return ports;
  }
  
  public CharSequence generateInterfaceDeclaration(final AbstractBlock obj) {
    CharSequence _xblockexpression = null;
    {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(" ");
      CharSequence res = _builder;
      boolean _isCombinational = obj.isCombinational();
      boolean _not = (!_isCombinational);
      if (_not) {
        StringConcatenation _builder_1 = new StringConcatenation();
        _builder_1.append("-- Global signals");
        _builder_1.newLine();
        _builder_1.append("rst : in std_logic;");
        _builder_1.newLine();
        _builder_1.append("clk : in std_logic");
        _builder_1.newLine();
        res = _builder_1;
      }
      if ((obj instanceof ActivableBlock)) {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append(res);
        _builder_2.newLineIfNotEmpty();
        {
          EList<InControlPort> _activate = ((ActivableBlock) obj).getActivate();
          boolean _hasElements = false;
          for(final Port p : _activate) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder_2.appendImmediate(";", "");
            }
            CharSequence _genPortDeclaration = this.genPortDeclaration(p);
            _builder_2.append(_genPortDeclaration);
          }
        }
        _builder_2.newLineIfNotEmpty();
        return _builder_2.toString();
      }
      if ((obj instanceof FlagBearerBlock)) {
        StringConcatenation _builder_3 = new StringConcatenation();
        _builder_3.append(res);
        _builder_3.newLineIfNotEmpty();
        {
          EList<OutControlPort> _flags = ((FlagBearerBlock) obj).getFlags();
          boolean _hasElements_1 = false;
          for(final Port p_1 : _flags) {
            if (!_hasElements_1) {
              _hasElements_1 = true;
            } else {
              _builder_3.appendImmediate(";", "");
            }
            CharSequence _genPortDeclaration_1 = this.genPortDeclaration(p_1);
            _builder_3.append(_genPortDeclaration_1);
          }
        }
        _builder_3.newLineIfNotEmpty();
        return _builder_3.toString();
      }
      CharSequence _xifexpression = null;
      if ((obj instanceof DataFlowBlock)) {
        StringConcatenation _builder_4 = new StringConcatenation();
        _builder_4.append(res);
        _builder_4.newLineIfNotEmpty();
        {
          EList<InDataPort> _in = ((DataFlowBlock) obj).getIn();
          boolean _hasElements_2 = false;
          for(final Port p_2 : _in) {
            if (!_hasElements_2) {
              _hasElements_2 = true;
            } else {
              _builder_4.appendImmediate(";", "");
            }
            CharSequence _genPortDeclaration_2 = this.genPortDeclaration(p_2);
            _builder_4.append(_genPortDeclaration_2);
          }
        }
        _builder_4.newLineIfNotEmpty();
        {
          EList<OutDataPort> _out = ((DataFlowBlock) obj).getOut();
          boolean _hasElements_3 = false;
          for(final Port p_3 : _out) {
            if (!_hasElements_3) {
              _hasElements_3 = true;
            } else {
              _builder_4.appendImmediate(";", "");
            }
            CharSequence _genPortDeclaration_3 = this.genPortDeclaration(p_3);
            _builder_4.append(_genPortDeclaration_3);
          }
        }
        _builder_4.newLineIfNotEmpty();
        _xifexpression = _builder_4;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateComponentDeclaration(final AbstractBlock obj) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("component ");
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" port (");
    _builder.newLineIfNotEmpty();
    CharSequence _generateInterfaceDeclaration = this.generateInterfaceDeclaration(obj);
    _builder.append(_generateInterfaceDeclaration);
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _generateEntityDeclaration(final AbstractBlock obj) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("entity ");
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" is port (");
    _builder.newLineIfNotEmpty();
    CharSequence _generateInterfaceDeclaration = this.generateInterfaceDeclaration(obj);
    _builder.append(_generateInterfaceDeclaration);
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    return _builder;
  }
  
  /**
   * def dispatch ComponentCreation ( FSM obj) {
   * �EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::fsm::FSM::VHDLModule ( this�
   * }
   * 
   * def dispatch ComponentCreation ( AbstractMemory obj) {
   * }
   * 
   * def dispatch ComponentCreation ( SinglePortRam obj) {
   * �EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::SinglePortRam::VHDLModule ( this�
   * }
   * 
   * def dispatch ComponentCreation ( SinglePortRom obj) {
   * �EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::SinglePortRom::VHDLModule ( this�
   * }
   * 
   * def dispatch ComponentCreation ( DualPortRam obj) {
   * �EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::DualPortRam::VHDLModule ( this�
   * }
   */
  public Object generateWire(final Wire w) {
    return null;
  }
  
  protected CharSequence _generateArchitectureDescription(final Datapath obj) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("architecture RTL of ");
    String _name = obj.getName();
    _builder.append(_name);
    _builder.append(" is");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-- Components ");
    _builder.newLine();
    {
      EList<AbstractBlock> _components = obj.getComponents();
      for(final AbstractBlock b : _components) {
        _builder.append("\t");
        CharSequence _generateComponentDeclaration = this.generateComponentDeclaration(b);
        _builder.append(_generateComponentDeclaration, "\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-- Wires ");
    _builder.newLine();
    {
      EList<Wire> _wires = obj.getWires();
      for(final Wire b_1 : _wires) {
      }
    }
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t ");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t ");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("variable tmp : std_logic_vector(ub downto lb);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("tmp:=  a(ub downto lb);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("return tmp;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("end std_range; ");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("-- Components ");
    _builder.newLine();
    {
      EList<AbstractBlock> _components_1 = obj.getComponents();
      for(final AbstractBlock b_2 : _components_1) {
      }
    }
    _builder.newLine();
    _builder.append("end RTL;");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence genPortDeclaration(final Port obj) {
    if (obj instanceof InControlPort) {
      return _genPortDeclaration((InControlPort)obj);
    } else if (obj instanceof InDataPort) {
      return _genPortDeclaration((InDataPort)obj);
    } else if (obj instanceof OutControlPort) {
      return _genPortDeclaration((OutControlPort)obj);
    } else if (obj instanceof OutDataPort) {
      return _genPortDeclaration((OutDataPort)obj);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(obj).toString());
    }
  }
  
  public CharSequence generateComponentDeclaration(final AbstractBlock obj) {
    return _generateComponentDeclaration(obj);
  }
  
  public CharSequence generateEntityDeclaration(final AbstractBlock obj) {
    return _generateEntityDeclaration(obj);
  }
  
  public CharSequence generateArchitectureDescription(final Datapath obj) {
    return _generateArchitectureDescription(obj);
  }
}
