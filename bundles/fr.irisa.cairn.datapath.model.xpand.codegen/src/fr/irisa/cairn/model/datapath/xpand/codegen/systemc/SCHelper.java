package fr.irisa.cairn.model.datapath.xpand.codegen.systemc;

import java.util.HashMap;


import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.xpand.codegen.vhdl.VHDLGenUtils;
import fr.irisa.cairn.model.datapath.xpand.codegen.vhdl.VHDLOperator;
import fr.irisa.cairn.model.fsm.FSM;

public class SCHelper {
	
	public static String getInstanceNumber(AbstractBlock block){
		String result = "";
		String className = "";
		int instanceNumber = 0;
		HashMap<String, Integer> classes = new HashMap();
		
		className = block.getClass().getSimpleName();
		if(classes.containsKey(className)){
			instanceNumber = (Integer) classes.get(className);
			instanceNumber++;
			classes.put(className, instanceNumber);
		}else{
			classes.put(className, instanceNumber);
			instanceNumber++;
		}
		
		return result;
	}
	
	public static String getConnectedComponent(InControlPort ip){
		String result = "";
		AbstractBlock otherEnd;
		
		otherEnd = ip.getWire().getSource().getParentNode();
		if(otherEnd instanceof Pad){
			
		}
		
		return result;
	}
	
	public static String getSignalName(InDataPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		if(ip.getWire() == null){
			//error - the port is not connected
			result += ip.getParentNode().getName();
			result += "_";
			result += ip.getName();
			result += "_";
		}else{
			result += ip.getWire().getSource().getParentNode().getName();
			result += "_";
			result += ip.getWire().getSource().getName();
			result += "_";
			result += ip.getParentNode().getName();
			result += "_";
			result += ip.getName();		
		}
						
		return result;
	}
	
	public static String getSignalName(OutDataPort op){
		String result = "";
		
		result += op.getParentNode().getName();
		result += "_";
		result += op.getName();
		result += "_";
		//error - the port is not connected
		op.getWires();
		if(op.getWires().isEmpty() == true)
			return result;
		result += op.getWires().get(0).getSink().getParentNode().getName();
		result += "_";
		result += op.getWires().get(0).getSink().getName();
				
		return result;
	}
	
	public static String getSignalName(InControlPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		if(ip.getWire() == null){
			//error - the port is not connected
			result += ip.getParentNode().getName();
			result += "_";
			result += ip.getName();
			result += "_";
		}else{
			result += ip.getWire().getSource().getParentNode().getName();
			result += "_";
			result += ip.getWire().getSource().getName();
			result += "_";
			result += ip.getParentNode().getName();
			result += "_";
			result += ip.getName();		
		}
						
		return result;
	}
	
	public static String getSignalName(OutControlPort op){
		String result = "";
		
		result += op.getParentNode().getName();
		result += "_";
		result += op.getName();
		result += "_";
		//error - the port is not connected
		op.getWires();
		if(op.getWires().isEmpty() == true)
			return result;
		result += op.getWires().get(0).getSink().getParentNode().getName();
		result += "_";
		result += op.getWires().get(0).getSink().getName();
				
		return result;
	}
	
	public static boolean isPad(AbstractBlock block){
		
		return (block instanceof Pad);
	}
	
	public static boolean isFSM(AbstractBlock block){
		
		return (block instanceof FSM);
	}
	
	public static String getPortConnexion(Port port){
		
		if(port instanceof InDataPort)
			return getPortConnexion((InDataPort)port);
		else if(port instanceof OutDataPort)
			return getPortConnexion((OutDataPort)port);
		else if(port instanceof InControlPort)
			return getPortConnexion((InControlPort)port);
		else if(port instanceof OutControlPort)
			return getPortConnexion((OutControlPort)port);
		
		return null;
	}
	
	public static String getPortConnexion(InDataPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		sink = ip.getParentNode();
		source = ip.getWire().getSource().getParentNode();
		if(source instanceof Pad){
			result += ip.getWire().getSource().getName();
		}else if(canCreateProcess(source) || isRegister(source)){
			result += source.getName() + "_" + ip.getWire().getSource().getName();
		}else{
			result += getSignalName(ip.getWire().getSource());
		}
		
		return result;
	}
	
	public static String getPortConnexion(OutDataPort op){
		String result = "";
		AbstractBlock source, sink;
		
		sink = op.getWires().get(0).getSink().getParentNode();
		source = op.getParentNode();
		if(sink instanceof Pad){
			result += op.getWires().get(0).getSink().getName();
		}else if(canCreateProcess(source) || isRegister(source)){
			result += sink.getName() + "_" + op.getWires().get(0).getSink().getName();
		}else{
			result += getSignalName(op);
		}
		
		return result;
	}
	
	public static String getPortConnexion(InControlPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		sink = ip.getParentNode();
		source = ip.getWire().getSource().getParentNode();
		if(source instanceof Pad){
			result += ip.getWire().getSource().getName();
		}else if(canCreateProcess(source) || isRegister(source)){
			result += source.getName() + "_" + ip.getWire().getSource().getName();
		}else{
			result += getSignalName(ip.getWire().getSource());
		}
		
		return result;
	}
	
	public static String getPortConnexion(OutControlPort op){
		String result = "";
		AbstractBlock source, sink;
		
		sink = op.getWires().get(0).getSink().getParentNode();
		source = op.getParentNode();
		if(sink instanceof Pad){
			result += op.getWires().get(0).getSink().getName();
		}else if(canCreateProcess(source) || isRegister(source)){
			result += sink.getName() + "_" + op.getWires().get(0).getSink().getName();
		}else{
			result += getSignalName(op);
		}
		
		return result;
	}
	
	public static boolean canInstantiateComponent(AbstractBlock block){
		boolean result = true;
		
		if((block instanceof Pad)
				|| (block instanceof Port)
				|| (block instanceof Wire)
				|| (block instanceof Register)
				|| (block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.operators.impl"))))
		{
			result = false;
		}
		
		return result;
	}
	
	public static boolean canCreateProcess(AbstractBlock block){
		boolean result = false;
		
		if(block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.operators.impl"))
				|| (block instanceof Register))
		{
			result = true;
		}
		
		return result;
	}
	
	public static boolean isRegister(AbstractBlock block){
		
		return (block instanceof Register);
	}
	
	public static String registerMultiplicity(Register register){
		String result = "";
		
		if( register instanceof ShiftRegister){
			result += "[";
			result += ((ShiftRegister) register).getDepth();
			result += "]";
		}else{
			result = " ";
		}
		
		return result;
	}
	
	public static String romDefinition(SinglePortRom rom){
		String result = "";
		int currentElement;
		int romDepth = (int) Math.pow(2,rom.getAddressPort().getWidth());
		byte[] content = rom.getContent();
		
		for(int i=0; i<romDepth; i++){
			currentElement = content[i];
			result += "\t\tmem[" + i + "] = " + currentElement + ";\n";
		}
		
		return result;
	}
	
	public static String getHeaderName(AbstractBlock block){
		String result = "";
		
		result += "\"";
		if( block instanceof FSM){
			result += "" + block.getName() + "_FSM.h";
		}else{
			result += "" + block.getName() + ".h";
		}
		result += "\"";
		
		return result;
	}
	
	public static boolean canConnectSignals(AbstractBlock block){
		boolean result = false;
		
		if((block instanceof Pad)
				|| (block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.operators.impl")))){
			result = true;
		}
		
		return result;
	}
	
	public static String getSignalConnexion(Port port){
		
		if(port instanceof InDataPort)
			return getSignalConnexion((InDataPort)port);
		else if(port instanceof OutDataPort)
			return getSignalConnexion((OutDataPort)port);
		else if(port instanceof InControlPort)
			return getSignalConnexion((InControlPort)port);
		else if(port instanceof OutControlPort)
			return getSignalConnexion((OutControlPort)port);
		
		return null;
	}
	
	public static String getSignalConnexion(InDataPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		if(canInstantiateComponent(ip.getParentNode())){
			return getSignalName(ip);
		}else if(ip.getWire() != null){
			if(canInstantiateComponent(ip.getWire().getSource().getParentNode())){
				return getSignalName(ip);
			}else{
				//if port not connected
				if(ip.getWire() == null)
					return "" + ip.getParentNode().getName() + "_" + ip.getName();
				sink = ip.getParentNode();
				source = ip.getWire().getSource().getParentNode();
				if(source instanceof Pad){
					result += ip.getWire().getSource().getName() + ".read()";
				}else if(canCreateProcess(source) || isRegister(source)){
					if(canOptimizeBlock(source)){
						result += optimizePath((DataFlowBlock)source);
					}else{
						result += source.getName() + "_" + ip.getWire().getSource().getName();
					} 
				} 
			}
		}
		
		return result;
	}
	
	public static String getSignalConnexion(OutDataPort op){
		String result = "";
		AbstractBlock source, sink;
		
		if(canInstantiateComponent(op.getParentNode())){
			return getSignalName(op);
		}else if(!op.getWires().isEmpty()){
			if(canInstantiateComponent(op.getWires().get(0).getSource().getParentNode())){
				return getSignalName(op);
			}else{
				//if port not connected
				if(op.getWires().isEmpty() == true)
					return "" + op.getParentNode().getName() + "_" + op.getName();
				sink = op.getWires().get(0).getSink().getParentNode();
				source = op.getParentNode();
				if(sink instanceof Pad){
					result += op.getWires().get(0).getSink().getName();
				}else if(canCreateProcess(sink) || isRegister(sink)){
					result += sink.getName() + "_" + op.getWires().get(0).getSink().getName();
				}
			}
		}
				
		return result;
	}
	
	public static String getSignalConnexion(InControlPort ip){
		String result = "";
		AbstractBlock source, sink;
		
		if(canInstantiateComponent(ip.getParentNode())){
			return getSignalName(ip);
		}else if(ip.getWire() != null){
			if(canInstantiateComponent(ip.getWire().getSource().getParentNode())){
				return getSignalName(ip);
			}else{
				//if port not connected
				if(ip.getWire() == null)
					return "" + ip.getParentNode().getName() + "_" + ip.getName();
				sink = ip.getParentNode();
				source = ip.getWire().getSource().getParentNode();
				if(source instanceof Pad){
					result += ip.getWire().getSource().getName() + ".read()";
				}else if(canCreateProcess(source) || isRegister(source)){
					if(canOptimizeBlock(source)){
						result += optimizePath((DataFlowBlock)source);
					}else{
						result += source.getName() + "_" + ip.getWire().getSource().getName();
					}
				} 
			}
		}
		
		return result;
	}
	
	public static String getSignalConnexion(OutControlPort op){
		String result = "";
		AbstractBlock source, sink;
		
		if(canInstantiateComponent(op.getParentNode())){
			return getSignalName(op);
		}else if(!op.getWires().isEmpty()){
			if(canInstantiateComponent(op.getWires().get(0).getSource().getParentNode())){
				return getSignalName(op);
			}else{
				//if port not connected
				if(op.getWires().isEmpty() == true)
					return "" + op.getParentNode().getName() + "_" + op.getName();
				sink = op.getWires().get(0).getSink().getParentNode();
				source = op.getParentNode();
				if(sink instanceof Pad){
					result += op.getWires().get(0).getSink().getName();
				}else if(canCreateProcess(sink) || isRegister(sink)){
					result += sink.getName() + "_" + op.getWires().get(0).getSink().getName();
				}
			}
		}
				
		return result;
	}
	
public static String getSignalConnexion(Wire wire){
		
		if(wire instanceof DataFlowWire)
			return getSignalConnexion((DataFlowWire)wire);
		else if(wire instanceof ControlFlowWire)
			return getSignalConnexion((ControlFlowWire)wire);
		
		return null;
	}
	
	public static String getSignalConnexion(DataFlowWire wire){
		String result = "";
		AbstractBlock source, sink;
		
		//if port not connected 
		if(wire.getSink() == null){
			source = wire.getSource().getParentNode();
			result += source.getName() + "_" + wire.getSource().getName() + "_";
		}else{
			sink = wire.getSink().getParentNode();
			source = wire.getSource().getParentNode();
			if(sink instanceof Pad){
				result += wire.getSink().getName();
			}else if(canCreateProcess(sink) || isRegister(sink)){
				result += sink.getName() + "_" + wire.getSink().getName();
			}else if(canInstantiateComponent(sink)){
				result += source.getName(); 
				result += "_";
				result += wire.getSource().getName(); 
				result += "_";
				result += sink.getName(); 
				result += "_";
				result += wire.getSink().getName();
			}
		}
				
		return result;
	}
	
	public static String getSignalConnexion(ControlFlowWire wire){
		String result = "";
		AbstractBlock source, sink;
		
		//if port not connected 
		if(wire.getSink() == null){
			source = wire.getSource().getParentNode();
			result += source.getName() + "_" + wire.getSource().getName() + "_";
		}else{
			sink = wire.getSink().getParentNode();
			source = wire.getSource().getParentNode();
			if(sink instanceof Pad){
				result += wire.getSink().getName();
			}else if(canCreateProcess(sink) || isRegister(sink)){
				result += sink.getName() + "_" + wire.getSink().getName();
			}else if(canInstantiateComponent(sink)){
				result += source.getName(); 
				result += "_";
				result += wire.getSource().getName(); 
				result += "_";
				result += sink.getName(); 
				result += "_";
				result += wire.getSink().getName();
			}
		}
				
		return result;
	}
	
	public static String getSignalName(Wire wire){
		
		if(wire instanceof DataFlowWire)
			return getSignalName((DataFlowWire)wire);
		else if(wire instanceof ControlFlowWire)
			return getSignalName((ControlFlowWire)wire);
		
		return null;
	}
	
	public static String getSignalName(DataFlowWire wire){
		String result = "";
		AbstractBlock source, sink;
		
		//if port not connected 
		if(wire.getSink() == null){
			source = wire.getSource().getParentNode();
			result += source.getName() + "_" + wire.getSource().getName() + "_";
		}else{
			sink = wire.getSink().getParentNode();
			source = wire.getSource().getParentNode();
			
			result += source.getName(); 
			result += "_";
			result += wire.getSource().getName(); 
			result += "_";
			result += sink.getName(); 
			result += "_";
			result += wire.getSink().getName();
		}
				
		return result;
	}
	
	public static String getSignalName(ControlFlowWire wire){
		String result = "";
		AbstractBlock source, sink;
		
		//if port not connected 
		if(wire.getSink() == null){
			source = wire.getSource().getParentNode();
			result += source.getName() + "_" + wire.getSource().getName() + "_";
		}else{
			sink = wire.getSink().getParentNode();
			source = wire.getSource().getParentNode();
			
			result += source.getName(); 
			result += "_";
			result += wire.getSource().getName(); 
			result += "_";
			result += sink.getName(); 
			result += "_";
			result += wire.getSink().getName();
		}
				
		return result;
	}
	
	public static boolean isConnectedToPad(Port port){
		
		if(port instanceof InDataPort)
			return isConnectedToPad((InDataPort)port);
		else if(port instanceof OutDataPort)
			return isConnectedToPad((OutDataPort)port);
		else if(port instanceof InControlPort)
			return isConnectedToPad((InControlPort)port);
		else if(port instanceof OutControlPort)
			return isConnectedToPad((OutControlPort)port);
		
		return false;
	}
	
	public static boolean isConnectedToPad(InDataPort ip){
		boolean result = false;
		
		ip.getWire();
		if(ip.getWire() == null)
			return false;
		if(ip.getWire().getSource().getParentNode() instanceof Pad)
			result = true;
		
		return result;
	}
	
	public static boolean isConnectedToPad(OutDataPort op){
		boolean result = false;
		
		op.getWires();
		if(op.getWires().isEmpty())
			return false;
		if(op.getWires().get(0).getSink().getParentNode() instanceof Pad)
			result = true;
		
		return result;
	}
	
	public static boolean isConnectedToPad(InControlPort ip){
		boolean result = false;
		
		ip.getWire();
		if(ip.getWire() == null)
			return false;
		if(ip.getWire().getSource().getParentNode() instanceof Pad)
			result = true;
		
		return result;
	}
	
	public static boolean isConnectedToPad(OutControlPort op){
		boolean result = false;
		
		op.getWires();
		if(op.getWires().isEmpty())
			return false;
		if(op.getWires().get(0).getSink().getParentNode() instanceof Pad)
			result = true;
		
		return result;
	}
	
	
	
	public static boolean canOptimizeBlock(AbstractBlock op){
		boolean result = true;
		DataFlowBlock intBlock;
		
		if((op instanceof BinaryOperator) 
				|| (op instanceof UnaryOperator) 
				|| (op instanceof TernaryOperator)
				|| (op instanceof ConstantValue)
				|| (op instanceof BitSelect)
				|| (op instanceof Merge)
				|| (op instanceof ExpandSigned)
				|| (op instanceof ExpandUnsigned))
		{
			if( ((DataFlowBlock)op).getOutput(0).getWires().size() != 1 )
				result = false;
			
			intBlock = ((DataFlowBlock)op).getOutput(0).getWires().get(0).getSink().getParentNode();
			if((intBlock instanceof DataFlowMux)
					|| (intBlock instanceof ControlFlowMux))
			{
				result = false;
			}
		}else{
			result = false;
		}
		
		System.out.println("performed optimization decision on component " + op.getName() + "; result of test is " + result);
		return result;
	}
	
	public static boolean canOptimizeWire(Wire wire){
		boolean result = false;
		AbstractBlock source, sink;
		
		if(wire instanceof DataFlowWire){
			source = ((DataFlowWire)wire).getSource().getParentNode();
			sink = ((DataFlowWire)wire).getSink().getParentNode();
			result = canOptimizeBlock(source) || canOptimizeBlock(sink);
		}else if(wire instanceof ControlFlowWire){
			source = ((ControlFlowWire)wire).getSource().getParentNode();
			sink = ((ControlFlowWire)wire).getSink().getParentNode();
			result = canOptimizeBlock(source) || canOptimizeBlock(sink);
		}
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizable(AbstractBlock op){
		boolean result = false;
		DataFlowBlock intBlock;
		
		intBlock = ((DataFlowBlock)op).getOutput(0).getWires().get(0).getSink().getParentNode();
		if((intBlock instanceof DataFlowMux)
				|| (intBlock instanceof ControlFlowMux))
		{
			result = true;
		}
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizableComplete(AbstractBlock op){
		boolean result = false;
		DataFlowBlock intBlock;
		
		intBlock = ((DataFlowBlock)op).getOutput(0).getWires().get(0).getSink().getParentNode();
		result = (!canOptimizeBlock(intBlock));
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizable(InDataPort ip){
		boolean result = false;
		DataFlowBlock intBlock;
		
		if(ip.getWire() == null)
			result = true;
		else{
			intBlock = ip.getWire().getSource().getParentNode();
			result = (!canOptimizeBlock(intBlock));
		}
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizable(OutDataPort op){
		boolean result = false;
		DataFlowBlock intBlock;
		
		if(op.getWires().isEmpty())
			result = true;
		else{
			for(DataFlowWire wire : op.getWires()){
				intBlock = wire.getSink().getParentNode();
				result = (!canOptimizeBlock(intBlock));
			}
		}
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizable(InControlPort ip){
		boolean result = false;
		FlagBearerBlock intBlock;
		
		if(ip.getWire() == null)
			result = true;
		else{
			intBlock = ip.getWire().getSource().getParentNode();
			result = (!canOptimizeBlock(intBlock));
		}
		
		return result;
	}
	
	public static boolean isConnectedToUnoptimizable(OutControlPort op){
		boolean result = false;
		ActivableBlock intBlock;
		
		if(op.getWires().isEmpty())
			result = true;
		else{
			for(ControlFlowWire wire : op.getWires()){
				intBlock = wire.getSink().getParentNode();
				result = (!canOptimizeBlock(intBlock));
			}
		}
		
		return result;
	}
	
	public static boolean isConnectedToOptimizable(InDataPort ip){
		boolean result = false;
		DataFlowBlock intBlock;
		
		if(ip.getWire() == null)
			result = true;
		else{
			intBlock = ip.getWire().getSource().getParentNode();
			result = canCreateProcess(intBlock) || (intBlock instanceof Pad);
		}
		
		return result;
	}
	
	public static boolean isConnectedToOptimizable(InControlPort ip){
		boolean result = false;
		FlagBearerBlock intBlock;
		
		if(ip.getWire() == null)
			result = true;
		else{
			intBlock = ip.getWire().getSource().getParentNode();
			result = canCreateProcess(intBlock) || (intBlock instanceof Pad);
		}
		
		return result;
	}
	
	public static boolean canOptimizeRegister(Register reg){
		boolean result = true;
		
		result = canOptimizeBlock(reg.getInput().getWire().getSource().getParentNode());
		System.out.println("trying to optimize while at register " + reg.getName() + "; result of test is " + result 
				+ "; tested on the component " + reg.getInput().getWire().getSource().getParentNode());
		
		return result;
	}
	
	
	public static boolean canOptimizePad(DataOutputPad pad){
		boolean result = true;
		
		result = canOptimizeBlock(pad.getInput().getWire().getSource().getParentNode());
		System.out.println("trying to optimize while at pad " + pad.getName() + "; result of test is " + result 
				+ "; tested on the component " + pad.getInput().getWire().getSource().getParentNode());
		
		return result;
	}
	
	public static boolean canOptimizePad(StatusPad pad){
		boolean result = true;
		
		result = canOptimizeBlock(pad.getControl().getWire().getSource().getParentNode());
		System.out.println("trying to optimize while at pad " + pad.getName() + "; result of test is " + result 
				+ "; tested on the component " + pad.getControl().getWire().getSource().getParentNode());
		
		return result;
	}
	
	
	
	public static String optimizePath(DataFlowBlock block){
		String result = "";
		String resultLeft = "";
		String resultRight = "";
		String resultOperator = null;
		DataFlowBlock intBlock, leftTerm, rightTerm;
		
		if(block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))
				|| block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
			intBlock = block.getInput(0).getWire().getSource().getParentNode();
		}else{
			intBlock = block;
		}
		
		try {
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl"))){
				resultOperator = SCOptimize.optimizeOperator(intBlock);
				if(resultOperator != null)
					result += resultOperator;
				return result;
			}
		} catch (ClassNotFoundException e) {
			// Wrong class given as parameter
			e.printStackTrace();
		}
		
		leftTerm = intBlock.getInput(0).getWire().getSource().getParentNode();
		if(leftTerm != null){
			if(leftTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))){
				
				resultLeft += " " + getSignalConnexion(intBlock.getInput(0)) + ".read().to_uint() ";
			}else if(leftTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
				resultLeft += " " + getSignalConnexion(intBlock.getInput(0)) + ".to_uint() ";
			}else{
				resultLeft += " (" + optimizePath(leftTerm) + ") ";
			}
		}else{
			System.err.println("attempt to optimize block with empty left operand");
		}
		
		resultOperator = SCOptimize.optimizeOperator(intBlock);
		
		if(!(intBlock instanceof SingleInputDataFlowBlock)){
			rightTerm = intBlock.getInput(1).getWire().getSource().getParentNode();
			if(rightTerm != null){
				if(rightTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))){
					
					resultRight += " " + getSignalConnexion(intBlock.getInput(1)) + ".read().to_uint() ";
				}else if(rightTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
					resultRight += " " + getSignalConnexion(intBlock.getInput(1)) + ".to_uint() ";
				}else{
					resultRight += " (" + optimizePath(rightTerm) + ") ";
				}
			}else{
				System.err.println("attempt to optimize block with empty right operand");
			}
		}
		
		try {
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl"))){
				switch (((BinaryOperator)intBlock).getOpcode().getValue()) {
				case BinaryOpcode.ADD_VALUE :
				case BinaryOpcode.SUB_VALUE :
				case BinaryOpcode.MUL_VALUE :
				case BinaryOpcode.DIV_VALUE :
				case BinaryOpcode.AND_VALUE :
				case BinaryOpcode.OR_VALUE :
				case BinaryOpcode.XOR_VALUE :
					result += resultLeft + resultOperator + resultRight;
					break;
				case BinaryOpcode.MAX_VALUE :
				case BinaryOpcode.MIN_VALUE :
				case BinaryOpcode.SHL_VALUE :
				case BinaryOpcode.SHR_VALUE :
					result += resultOperator + "(" + resultLeft + "," + resultRight + ")";
					break;
				case BinaryOpcode.NAND_VALUE :
					result += "~(" + resultLeft + resultOperator + resultRight + ")";
					break;
				case BinaryOpcode.CMP_VALUE :
					result += resultLeft + "-" + resultRight + ";\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" <= "+resultRight+") or("+resultLeft+" >= "+resultRight+"))\n";
					result += "\tfsm_lt_GT0 = '0'\n";
					result += "else\n\tfsm_lt_GT0 = '1'\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" > "+resultRight+") or("+resultLeft+" = "+resultRight+") or("+resultLeft+" /= "+resultRight+"))\n";
					result += "\tfsm_lt_GT1 = '0'\n";
					result += "else\n\tfsm_lt_GT1 = ''1\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" <= "+resultRight+") or("+resultLeft+" = "+resultRight+"))\n";
					result += "\tfsm_lt_GT2 = '0'\n";
					result += "else\n\tfsm_lt_GT3 = '1'\n";
					break;

				default:
					break;
				}
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl"))){
				result += resultOperator + resultLeft;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl"))){
				result += "( (sc_lv<" + ((BitSelect)intBlock).getInput().getWidth() + ">) " + resultLeft + ")" + resultOperator;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.MergeImpl"))){
				result += resultLeft + resultOperator + resultRight;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl"))){
				result += resultOperator;
			}
			
		} catch (ClassNotFoundException e) {
			// Wrong class given as parameter
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static boolean hasSingleConnexion(OutDataPort op){
		boolean result = false;
		
		if(op.getWires().isEmpty() == true)
			result = true;
		
		return result;
	}
	
	public static boolean hasSingleConnexion(OutControlPort op){
		boolean result = false;
		
		if(op.getWires().isEmpty() == true)
			result = true;
		
		return result;
	}
}






