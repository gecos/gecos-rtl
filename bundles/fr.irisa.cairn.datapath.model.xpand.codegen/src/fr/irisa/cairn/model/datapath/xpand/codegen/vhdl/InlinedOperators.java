package fr.irisa.cairn.model.datapath.xpand.codegen.vhdl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InDataPort;


public class InlinedOperators extends OperatorsSwitch<String>{

	static InlinedOperators singleton = null;

	public static InlinedOperators getSingleton() {
		if(singleton==null) {
			singleton = new InlinedOperators();
		}
		return singleton;
	}

	public static String inline(AbstractBlock theEObject) {
		return getSingleton().doSwitch(theEObject);
	}

	public InlinedOperators() {
		super();
	}

	@Override
	public String caseExpandSigned(ExpandSigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseExpandUnsigned(ExpandUnsigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseQuantize(Quantize object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseReductionOperator(ReductionOperator object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseBinaryOperator(BinaryOperator binop) { 
		String opA = VHDLGenUtils.makeSourceName(binop,0);
		String opB = VHDLGenUtils.makeSourceName(binop,1);
		int ub = binop.getOutput().getWidth()-1;

		String outputName = VHDLGenUtils.makeOutputName(binop);
		switch (binop.getOpcode().getValue()) {
			case BinaryOpcode.ADD_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= std_range("+opA+" + "+opB+","+ub+",0);"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.SUB_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= std_range("+opA+" - "+opB+","+ub+",0);"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.MUL_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop)) {
						return outputName+" <= std_range("+opA+" * "+opB+","+ub+",0);";
					} else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";  
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.DIV_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= div("+opA+" , "+opB+")"+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.MAX_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= "+opA+" when "+opA+">"+opB+" else "+opB+""+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.MIN_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= "+opA+" when "+opB+">"+opA+" else "+opB+""+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.NAND_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= not ("+opA+" and "+opB+")"+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.XOR_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= "+opA+" xor "+opB+";";  
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.AND_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= "+opA+" and "+opB+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				
			case BinaryOpcode.OR_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return outputName+" <= "+opA+" or "+opB+";"; 
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";  -- this assignemtn is useless ";
					
			case BinaryOpcode.SHL_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
//						return outputName+" <= "+opA+" sll to_integer(unsigned("+opB+"));"; 
						return outputName+" <= std_logic_vector(shift_left(unsigned("+opA+"), to_integer(unsigned("+opB+"))));";
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";  -- this assignemtn is useless ";
					
				
			case BinaryOpcode.SHR_VALUE :
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
//						return outputName+" <= "+opA+" srl to_integer(unsigned("+opB+"));"; 
						return outputName+" <= std_logic_vector(shift_right(unsigned("+opA+"), to_integer(unsigned("+opB+"))));";
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";"; 
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";  -- this assignemtn is useless ";
				
			case BinaryOpcode.CMP_VALUE :
				StringBuffer res2 = new StringBuffer();
				res2.append(outputName+" <= "+opA+" - "+opB+";\n");
				res2.append("\tfsm_lt_GT0 <= '0' when (("+opA+" < "+opB+") or("+opA+" <= "+opB+") or("+opA+" >= "+opB+")) else '1';\n");
				
				res2.append("\tfsm_lt_GT1 <= '0' when (("+opA+" < "+opB+") or("+opA+" > "+opB+") or("+opA+" = "+opB+") or("+opA+" /= "+opB+")) else '1';\n");
				
				res2.append("\tfsm_lt_GT2 <= '0' when (("+opA+" < "+opB+") or("+opA+" <= "+opB+") or("+opA+" = "+opB+")) else '1'"+";");
				
				if(!VHDLHelper.canOptimizeBlock(binop))
					if(!VHDLHelper.isConnectedToUnoptimizable(binop))
						return res2.toString();
					else
						return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
				else
					return outputName+" <= "+VHDLHelper.optimizePath(binop)+";";
					
				
			
			default :
				throw new UnsupportedOperationException("Unsupport BinaryOpcode for VHDL synthesis: "+binop.getOpcode());
		}
	}

	@Override
	public String caseUnaryOperator(UnaryOperator unop) { 
		String sourceName = VHDLGenUtils.makeSourceName(unop,0);
		switch (unop.getOpcode().getValue()) {
			case UnaryOpcode.INV_VALUE:
				if(!VHDLHelper.canOptimizeBlock(unop))
					if(!VHDLHelper.isConnectedToUnoptimizable(unop))
						return VHDLGenUtils.makeOutputName(unop)+" <= inv("+sourceName+")"+";";
					else
						return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				else
					return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				
			case UnaryOpcode.NEG_VALUE :
				if(!VHDLHelper.canOptimizeBlock(unop))
					if(!VHDLHelper.isConnectedToUnoptimizable(unop))
						return VHDLGenUtils.makeOutputName(unop)+" <= -("+sourceName+")"+";";
					else
						return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				else
					return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				
			case UnaryOpcode.NOT_VALUE :
				if(!VHDLHelper.canOptimizeBlock(unop))
					if(!VHDLHelper.isConnectedToUnoptimizable(unop))
						return VHDLGenUtils.makeOutputName(unop)+" <= not("+sourceName+")"+";";
					else
						return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				else
					return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				
			case UnaryOpcode.SQRT_VALUE :
				if(!VHDLHelper.canOptimizeBlock(unop))
					if(!VHDLHelper.isConnectedToUnoptimizable(unop))
						return VHDLGenUtils.makeOutputName(unop)+" <= sqrt("+sourceName+")"+";";
					else
						return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				else
					return VHDLGenUtils.makeOutputName(unop)+" <= "+VHDLHelper.optimizePath(unop)+";";
				
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for VHDL synthesis :"+unop.getOpcode());
		}
		
	}

	@Override
	public String caseBitSelect(BitSelect select) {
		String opA = VHDLGenUtils.makeSourceName(select,0);
		int upperBound = select.getUpperBound();
		int lowerBound = select.getLowerBound();
		if(upperBound!=lowerBound)
			return VHDLGenUtils.makeOutputName(select)+" <= "+opA+" ("+upperBound+" downto "+lowerBound+")"+";";
		else
			return VHDLGenUtils.makeOutputName(select)+" <= "+opA+" ("+upperBound+")"+";";
			
	}
	
	@Override
	public String caseMerge(Merge merge) {
		String source1 = VHDLGenUtils.makeSourceName(merge,1);
		String source0 = VHDLGenUtils.makeSourceName(merge,0);
		
		if(!VHDLHelper.canOptimizeBlock(merge))
			if(!VHDLHelper.isConnectedToUnoptimizable(merge))
				return VHDLGenUtils.makeOutputName(merge)+" <= "+source1+" & "+source0+";";
			else
				return VHDLGenUtils.makeOutputName(merge)+" <= "+VHDLHelper.optimizePath(merge)+";";
		else
			return VHDLGenUtils.makeOutputName(merge)+" <= "+VHDLHelper.optimizePath(merge)+";";
		
	}
	
	@Override
	public String caseConstantValue(ConstantValue cst) {
		String binaryValue = VHDLGenUtils.binaryValue(cst.getValue(),cst.getOutput().getWidth());
		if (cst.getOutput().getWidth()==1) {
			return VHDLGenUtils.makeOutputName(cst)+" <= '"+binaryValue+"'"+"; --"+cst.getValue(); 
		} else {
			return VHDLGenUtils.makeOutputName(cst)+" <= \""+binaryValue+"\""+"; --"+cst.getValue(); 
		}
	}

	@Override
	public String caseDataFlowMux(DataFlowMux dmux ) {
		String source0 = VHDLGenUtils.makeSourceName(dmux,0);
		String source1 = VHDLGenUtils.makeSourceName(dmux,1);
		String source2 = VHDLGenUtils.makeSourceName(dmux,2);
		return VHDLGenUtils.makeOutputName(dmux)+" <= "+source1+" when "+source2+"='1' else "+source0+";";
	}

	@Override
	public String caseControlFlowMux(ControlFlowMux cmux) {
		StringBuffer result = new StringBuffer();
		String select = VHDLGenUtils.makeSourceName((ActivableBlock)cmux,0);
		int ctrl_bw = VHDLGenUtils.bitwidth(cmux.getIn().size());
		if (cmux.getControlPort(0).getWidth()<ctrl_bw) {
			throw new RuntimeException("Inconsistent bitwidth in cmux !");
		}
		int offset=0;
		for (InDataPort ip : cmux.getIn()) {
			String source0 = VHDLGenUtils.makeSourceName((DataFlowBlock)cmux,offset);
			if (offset<cmux.getIn().size()-1) {
				result.append(source0+ " when "+select+ "= \'"+VHDLGenUtils.binaryValue(offset, ctrl_bw)+"\'\t");
			} else {
				result.append(" else "+source0);
			}
			offset++;
		}
		String source1 = VHDLGenUtils.makeSourceName((DataFlowBlock)cmux,1);
		return VHDLGenUtils.makeOutputName(cmux) + " <= " + result.toString() + ";";  
	}

	@Override
	public String caseCtrl2DataBuffer(Ctrl2DataBuffer c2dbuffer) {
		String Source = VHDLGenUtils.makeSourceName(c2dbuffer.getControlPort(0));
		String Destination = VHDLGenUtils.makeOutputName(c2dbuffer);
		return Destination +" <= "+ Source+";";
	}

	@Override
	public String caseData2CtrlBuffer(Data2CtrlBuffer d2cbuffer) {
		String Source = VHDLGenUtils.makeSourceName(d2cbuffer.getInput());
		String Destination = VHDLGenUtils.makeSinkName(d2cbuffer.getFlag(0));
		return Destination +" <= "+ Source+";";
	}

	@Override
	public String caseCompare(Compare cmp) {
		String res = VHDLGenUtils.makeOutputName(cmp)+" <= '1'  when ";
		String source0 = VHDLGenUtils.makeSourceName(cmp,0);
		String source1 = VHDLGenUtils.makeSourceName(cmp,1);
		
		switch (cmp.getOpcode().getValue()) {
			case CompareOpcode.EQU_VALUE: 
				res= res+"("+source0+"="+source1+")";
				break; 
			case CompareOpcode.GT_VALUE:
				res= res+ "("+source0+">"+source1+")";
				break; 
			case CompareOpcode.GTE_VALUE:
				res= res+ "("+source0+">="+source1+")";
				break; 
			case CompareOpcode.LT_VALUE:
				res= res+ "("+source0+"<"+source1+")";
				break; 
			case CompareOpcode.LTE_VALUE:
				res= res+ "("+source0+"<="+source1+")";
				break; 
			case CompareOpcode.NEQ_VALUE:
				res= res+ "("+source0+"/="+source1+")";
				break; 
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+cmp.getOpcode());
		}
		res = res +" else '0';\n" ;
		return res;
	}
					
	@Override
	public String caseTernaryOperator(TernaryOperator terop) {
		String src0 = VHDLGenUtils.makeSourceName(terop,0);
		String src1 = VHDLGenUtils.makeSourceName(terop,1);
		String src2 = VHDLGenUtils.makeSourceName(terop,2);
		switch (terop.getOpcode().getValue()) {
			case TernaryOpcode.MULADD_VALUE :
				if(!VHDLHelper.canOptimizeBlock(terop))
					if(!VHDLHelper.isConnectedToUnoptimizable(terop))
						return VHDLGenUtils.makeOutputName(terop)+" <= std_range("+src0+"*"+src1+","+(terop.getOutput().getWidth()-1)+",0)+"+src2+" ;";
					else
						return VHDLGenUtils.makeOutputName(terop)+" <= "+VHDLHelper.optimizePath(terop)+";";
				else
					return VHDLGenUtils.makeOutputName(terop)+" <= "+VHDLHelper.optimizePath(terop)+";";
				
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+terop.getOpcode());
		}
	}
	
}
