package fr.irisa.cairn.model.datapath.xpand.codegen.modules;

import fr.irisa.cairn.model.datapath.Datapath;

public class XPandVHDLGenerator extends AbstractXpandCodeGenerator{

	public XPandVHDLGenerator(Datapath target, String outdir) {
		super(target,outdir);
		xpandTemplateName = "fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::Datapath::VHDLModule";
	}

	public XPandVHDLGenerator(String target, String outdir) {
		super(target,outdir);
		xpandTemplateName = "fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::Datapath::VHDLModule";
	}
	
}
