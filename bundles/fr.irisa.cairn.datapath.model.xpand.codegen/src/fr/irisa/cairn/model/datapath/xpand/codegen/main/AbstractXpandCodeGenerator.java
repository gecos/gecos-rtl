package fr.irisa.cairn.model.datapath.xpand.codegen.main;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.xpand2.XpandExecutionContextImpl;
import org.eclipse.xpand2.XpandFacade;
import org.eclipse.xpand2.output.Outlet;
import org.eclipse.xpand2.output.OutputImpl;
import org.eclipse.xtend.typesystem.emf.EmfRegistryMetaModel;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLReader;
import fr.irisa.cairn.model.fsm.FsmPackage;
/**
 * need to look at http://github.com/ralfebert/de.ralfebert.xpand.engine
 * @author sderrien
 *
 */
public abstract class AbstractXpandCodeGenerator {

	protected String xpandTemplateName;
	protected String outdir;
	protected Datapath targetDatapath; 

	
//	public static void main(String[] args) throws CoreException {
//		AbstractXpandCodeGenerator sing = new AbstractXpandCodeGenerator();
//		String path = "C:/data/win-workspace/fr.irisa.cairn.model.datapath.examples/";
//		String outdir = path+"/models/";
//		String infile = path+"/models/FIR/seqFIR24.datapath";
//		sing.generate(infile,outdir);
//		
//	} 
	
	protected void generate() throws CoreException {


	    // configure outlets
	    OutputImpl output = new OutputImpl();
	    Outlet outlet = new Outlet(outdir);
	    outlet.setOverwrite(true);
	    output.addOutlet(outlet);

	    // create execution context
	    Map globalVarsMap = new HashMap();
	    XpandExecutionContextImpl execCtx = new XpandExecutionContextImpl(output, null, globalVarsMap, null, null);
	    EmfRegistryMetaModel metamodel = new EmfRegistryMetaModel() {
	        @Override
	        protected EPackage[] allPackages() {
	            return new EPackage[] { 
	            		DatapathPackage.eINSTANCE, 
	            		PortPackage.eINSTANCE, 
	            		PadsPackage.eINSTANCE, 
	            		StoragePackage.eINSTANCE, 
	            		OperatorsPackage.eINSTANCE, 
	            		EcorePackage.eINSTANCE,
	            		FsmPackage.eINSTANCE
	            };
	        }
	    };
	    execCtx.registerMetaModel(metamodel);

	    // generate
	    XpandFacade facade = XpandFacade.create(execCtx);
	    String templatePath = xpandTemplateName;
	    facade.evaluate(templatePath, targetDatapath);

	}
}
