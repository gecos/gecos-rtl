package fr.irisa.cairn.model.datapath.xpand.codegen.vhdl.fsm

import fr.irisa.cairn.model.fsm.FSM
import fr.irisa.cairn.model.fsm.State
import fr.irisa.cairn.model.datapath.port.InControlPort
import fr.irisa.cairn.model.fsm.Transition
import fr.irisa.cairn.model.fsm.OutputDefaultValues
import java.lang.String
import java.lang.Integer
import fr.irisa.cairn.model.fsm.IntegerCommandValue
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression
import fr.irisa.cairn.model.fsm.AbstractCommandValue
import fr.irisa.cairn.model.fsm.BooleanCommandValue
import fr.irisa.cairn.model.fsm.AndExpression
import fr.irisa.cairn.model.fsm.OrExpression
import fr.irisa.cairn.model.fsm.NegateExpression
import fr.irisa.cairn.model.fsm.BooleanFlagTerm
import fr.irisa.cairn.model.fsm.IntegerFlagTerm
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm

class FSMVHDLCodegen {
	 
	def makeBinary(int value, int bitwdith) {
		return String::format("%"+bitwdith+"s", Integer::toBinaryString(value)).replace(" ", "0");
	}

	def generate(FSM fsm) {
		'''
		library ieee;
		library work;
		
		use ieee.std_logic_1164.all;
		use ieee.std_logic_arith.all;
		use ieee.std_logic_unsigned.all;
		
		�
		�

	
		architecture RTL of �fsm.name� is
		
			type T_�fsm.name.toFirstUpper()� is (
				�FOR State state : fsm.states SEPARATOR ","�
				�state.label�
				�ENDFOR�
			);
		
			signal ns,cs : T_�fsm.name.toFirstUpper�;
		
			constant ONE : std_logic:='1';
			constant ZERO : std_logic:='0';
			
			
		begin
		
			SYNC: process(clk,rst) 
			begin
				if (rst='1') then
					cs <= �fsm.start.label�;
				elsif rising_edge(clk) then
					cs <= ns;
				end if; 
			end process;
		
			STATE_TRANSITIONS: process(cs,
				�FOR InControlPort inputPort : fsm.activate SEPARATOR ","�
					�inputPort.name�
				�ENDFOR�)  
			begin
				case cs is
					�FOR State state : fsm.states �
						�FOR Transition t : state.transitions �
							�genTransition(t)�
						�ENDFOR�
					�ENDFOR�
					when others =>
						null;
				end case;
			end process;
		
			STATE_COMMANDS: process(cs,
				�FOR InControlPort inputPort : fsm.activate SEPARATOR ","�
					�inputPort.name�
				�ENDFOR�)  
			begin
				�FOR OutputDefaultValues defaultValue : fsm.defaultValues  �
					�defaultValue.port.name� <= �defaultValue.value�,�defaultValue.port.width�
				�ENDFOR�
				case cs is
					�FOR State state : fsm.states �
						�FOR AbstractCommandValue command : state.activatedCommands�
							�genCommand(command)�
						�ENDFOR�
					�ENDFOR�
					when others =>
						null;
				end case;
			end process;
		end RTL;
		'''
	}
	
	
	def dispatch genPredicate(AbstractBooleanExpression e) {
	}

	/**
	 * Commands
	 * 
	 * 
	 */	
	def dispatch  genCommandStatement(AbstractCommandValue s) {}

	def dispatch genCommandStatement(IntegerCommandValue s) {
		'''�s.command.name� <= �makeBinary(s.value,s.command.width)�'''
	}

	def dispatch genCommandStatement(BooleanCommandValue s) {
		if (s.value.toString().toUpperCase().contains("ONE")) {
			'''�s.command.name� <= '1' '''
		} else {
			'''�s.command.name� <= '0' '''
		}
	}
	
	def genTransition(Transition t) {
		if (t.predicate!=null) {	
			if (t.predicate.^false) {
			'''
					-- Unrable state �t.dst.label�
			'''
			} else if (t.predicate.^true) {
			'''
					-- Predicate �t.predicate.toString()� 
					ns  <= �t.dst.label� ;
			'''
			} else if (!t.predicate.^true && !t.predicate.^false) {
			'''
					-- Predicate �t.predicate.toString()� 
					if (�genPredicate(t.predicate)�) then
						ns  <= �t.dst.label� ;
					end if;
			'''
			} else {		
			'''
					-- Predicate �t.predicate.toString()� 
					ns  <= �t.dst.label� ;
			'''
			}
		} else {
			'''
					-- null predicate  
			'''
		}
	}
	/**
	 * 
	 *  Predicates
	 * 
	 * 
	 */
	def dispatch genPredicate(AndExpression p) {
		if (p.terms.size != 0) {
		 '''�FOR AbstractBooleanExpression term : p.terms SEPARATOR " and "�(�genPredicate(term)�)�ENDFOR�'''
		}
	}
	
	def dispatch genPredicate(OrExpression p) {
		if (p.terms.size != 0) {
		 '''�FOR AbstractBooleanExpression term : p.terms SEPARATOR " or "�(�genPredicate(term)�)�ENDFOR�'''
		}
	}
	
	def dispatch Predicate (NegateExpression n) {
		'''not(�genPredicate(n.term)�)'''
	}
	
	
	def dispatch genPredicate(BooleanFlagTerm bft) {
		if (bft.negated) {
			'''�bft.flag.name�='0' '''
		} else {
			'''�bft.flag.name�='1' '''
		}
	}




	def dispatch genPredicate(IntegerFlagTerm ift) {
		'''�ift.flag.name�=�makeBinary(ift.value,ift.flag.width)�'''
	}



	def dispatch genPredicate(IndexedBooleanFlagTerm ift) {
		if (ift.negated) {
			'''�ift.flag.name�(�ift.offset�)='0' '''
		} else {
			'''�ift.flag.name�(�ift.offset�)='1' '''
		}
	}
	
	
	def genCommand(AbstractCommandValue s) {
		if (s.predicate!=null) {	
			if (s.predicate.^false) {
			return '''
					-- Unsatisfiable predicate for command �genCommandStatement(s)�
			'''
			} else if (s.predicate.^true) {
			return '''
					�genCommandStatement(s)�;
			'''
			} else if (!s.predicate.^true && !s.predicate.^false) {}
			'''
					if (�genPredicate(s.predicate)�) then
					�genCommandStatement(s)�;
					end if;
			'''
			} else {		
			'''
					�genCommandStatement(s)�;
			'''
			}
		}
	}
	
	

