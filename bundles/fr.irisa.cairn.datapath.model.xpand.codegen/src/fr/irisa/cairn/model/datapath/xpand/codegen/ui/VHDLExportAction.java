package fr.irisa.cairn.model.datapath.xpand.codegen.ui;

import java.io.FileNotFoundException;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xml.DatapathXMLReader;

public class VHDLExportAction implements IObjectActionDelegate {

	private Shell shell;
	private IFile file; 

	/**
	 * Constructor for Action1.
	 */
	public VHDLExportAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
        DirectoryDialog dlg = new DirectoryDialog(shell.getShell());
        dlg.setText("Select a directory");
        dlg.setMessage("Select a directory for exporting the VHDL file(s)");
        String targetDirname = dlg.open();
        if (targetDirname != null) {
//        FIXME
//        	try {
//	        	DatapathXMLReader reader = DatapathXMLReader.getXMLReader();
//	        	Datapath datapath = reader.load(file.getName());
//	        	String outfilename  = datapath.getName()+"_gen.vhd";
//        	} catch (Exception e) {
//				MessageBox messageBox = new MessageBox(shell.getShell(), SWT.OK);
//				messageBox.setMessage("Problem during export, could not read "+file.getName());
//				messageBox.open();
//        	}
        }
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (!(selection instanceof IStructuredSelection)) return;
		
		for (Object selected : ((IStructuredSelection) selection).toList()) {
			if (!(selected instanceof IFile)) continue;
			file = (IFile) selected;
		}
	}

}
