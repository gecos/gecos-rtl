package fr.irisa.cairn.model.datapath.xpand.codegen.vhdl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class VHDLHelper {
	final static boolean OPTIMIZE_PATH=false;
	
	public static String getInputPadAssign(DataInputPad pad){
		OutDataPort oport = pad.getOutput(0);
		InDataPort iport = pad.getAssociatedPort();
		String oport_name = oport.getParentNode().getName()+"_"+oport.getName();
		String iport_name = iport.getName(); 
		
		return "" + oport_name + "<=" + iport_name + ";";
	}
	
	public static String getOutputPadAssign(DataOutputPad pad){
		InDataPort iport = pad.getInput(0);
		OutDataPort oport = pad.getAssociatedPort();
		String iport_name = iport.getParentNode().getName()+"_"+iport.getName();
		String oport_name = oport.getName(); 
		
		return "" + oport_name + "<=" + iport_name + ";";
	}
	
	public static String getStatusPadAssign(StatusPad pad){
		InControlPort iport = pad.getControlPort(0);
		OutControlPort oport = pad.getAssociatedPort();
		String iport_name = iport.getParentNode().getName()+"_"+iport.getName();
		String oport_name = oport.getName(); 
		
		return "" + oport_name + "<=" + iport_name + ";";
	}
	
	public static String getControlPadAssign(ControlPad pad){
		OutControlPort oport = pad.getFlag();
		InControlPort iport = pad.getAssociatedPort();
		String oport_name = oport.getParentNode().getName()+"_"+oport.getName();
		String iport_name = iport.getName(); 
		
		return "" + oport_name + "<=" + iport_name + ";" ;
	}
	
	public static boolean isActivableBlock(AbstractBlock block){
		
		return (block instanceof ActivableBlock);
	}
	
	public static boolean isDataFlowBlock(AbstractBlock block){
		
		return (block instanceof DataFlowBlock);
	}
	
	public static boolean isFlagBearerBlock(AbstractBlock block){
		
		return (block instanceof FlagBearerBlock);
	}
	
	public static String romBlockDefinition(SinglePortRom rom){
		String result = "";
		int depth = (int) Math.pow(2,rom.getAddressPort().getWidth());
		int width = rom.getReadPort().getWidth();
		byte[] contents = rom.getContent();
		
		for (int i=0; i<depth; i++){
			if(contents.length == depth){
				String binValueFinal = paddedBinaryString(width, contents[i]);
				result += "\t\t"+ binValueFinal;
				if(i != (depth-1)){
					result += ",\n";
	 			}else{
	 				result += "\n";
				}
			}else{
				if(i < contents.length){ 
					String binValueFinal = paddedBinaryString(width, contents[i]);
					result += binValueFinal;
					if(i != (depth-1)){
						result += ",\n\t\t";
		 			}else{
		 				result += "\n";
					}
	 			}else{
	 				for(int j=0; j<width; j++)
						result += "0";
					if(i != (depth - 1)){
						result += ",\n\t\t"; 
					}else{ 
						result += "\n";
					}
				}
			}	
		} 
		
		return result;
	}

	private static String paddedBinaryString(int width, int value) {
		String binValue = Integer.toBinaryString(value);
		if(binValue.length() > width)
				binValue = binValue.substring(24);
		String binValueFinal = "\"";
		for(int j=width; j>binValue.length(); j--)
			binValueFinal+="0";
		binValueFinal+=binValue+"\"";
		return binValueFinal;
	}
	
	public static Long getDepth(Integer i){
		
		return (long) Math.pow(2, i.intValue());
	}
	
	public static Long getDepthMinusOne(Integer i){
		
		return new Long((long) (Math.pow(2, i.intValue()) - 1));
	}
	
	public static boolean canOptimizeBlock(AbstractBlock op){
		boolean result = true;
		DataFlowBlock intBlock;
		if (op==null) return false;
		
		if((op instanceof BinaryOperator) 
				|| (op instanceof UnaryOperator) 
				|| (op instanceof TernaryOperator)
				|| (op instanceof ConstantValue)
				|| (op instanceof BitSelect)
				|| (op instanceof Merge)
				|| (op instanceof ExpandSigned)
				|| (op instanceof ExpandUnsigned))
		{
			if( ((DataFlowBlock)op).getOutput(0).getWires().size() != 1 )
				result = false;
			
			intBlock = ((DataFlowBlock)op).getOutput(0).getWires().get(0).getSink().getParentNode();
			if((intBlock instanceof DataFlowMux)
					|| (intBlock instanceof ControlFlowMux))
			{
				result = false;
			}
		}else{
			result = false;
		}
		
		System.out.println("performed optimization decision on component " + op.getName() + "; result of test is " + result);
		return OPTIMIZE_PATH &result;
	}
	
	public static boolean canOptimizeWire(Wire wire){
		boolean result = false;
		AbstractBlock source, sink;
		
		if(wire instanceof DataFlowWire){
			source = ((DataFlowWire)wire).getSource().getParentNode();
			sink = ((DataFlowWire)wire).getSink().getParentNode();
			result = canOptimizeBlock(source) || canOptimizeBlock(sink);
		}else if(wire instanceof ControlFlowWire){
			source = ((ControlFlowWire)wire).getSource().getParentNode();
			sink = ((ControlFlowWire)wire).getSink().getParentNode();
			result = canOptimizeBlock(source) || canOptimizeBlock(sink);
		}
		
		return OPTIMIZE_PATH &result;
	}
	
	public static boolean isConnectedToUnoptimizable(AbstractBlock op){
		boolean result = false;
		DataFlowBlock intBlock;
		
		intBlock = ((DataFlowBlock)op).getOutput(0).getWires().get(0).getSink().getParentNode();
		if((intBlock instanceof DataFlowMux)
				|| (intBlock instanceof ControlFlowMux))
		{
			result = true;
		}
		
		return OPTIMIZE_PATH &result;
	}
	
	public static boolean canOptimizeRegister(Register reg){
		boolean result = true;
		
		result = canOptimizeBlock(reg.getInput().getWire().getSource().getParentNode());
		System.out.println("trying to optimize while at register " + reg.getName() + "; result of test is " + result 
				+ "; tested on the component " + reg.getInput().getWire().getSource().getParentNode());
		
		return OPTIMIZE_PATH &result;
	}
	
	
	public static boolean canOptimizePad(DataOutputPad pad){
		boolean result = true;
		
		canOptimizeBlock(pad.getSourceBlock());
		
		
		return OPTIMIZE_PATH & result;
	}
	
	private static boolean canBeBypassed(AbstractBlock blck) {
		if (blck!=null) 
		return OPTIMIZE_PATH &(blck instanceof BitSelect);
		else return false;
	}
	
	public static boolean regularRegisterOptimization(Register reg){
		return OPTIMIZE_PATH &canBeBypassed(reg.getSourceBlock());
	}
	
	public static boolean regularPadOptimization(DataOutputPad pad){
		return OPTIMIZE_PATH & canBeBypassed(pad.getSourceBlock());
	}
	
	public static boolean regularPadOptimization(Register reg){
		return OPTIMIZE_PATH &canBeBypassed(reg.getSourceBlock());
	}
	
	public static String bitSelection(DataOutputPad pad){
		String result = "";
		DataFlowBlock intBlock;
		int upperBound, lowerBound;
		
		intBlock = pad.getInput().getWire().getSource().getParentNode();
		upperBound = ((BitSelect)intBlock).getUpperBound();
		lowerBound = ((BitSelect)intBlock).getLowerBound();
		if(upperBound!=lowerBound)
			result += " ("+upperBound+" downto "+lowerBound+")";
		else
			result += " ("+upperBound+")";
		
		return result;
	}
	
	public static String bitSelection(Register reg){
		String result = "";
		DataFlowBlock intBlock;
		int upperBound, lowerBound;
		
		intBlock = reg.getInput().getWire().getSource().getParentNode();
		upperBound = ((BitSelect)intBlock).getUpperBound();
		lowerBound = ((BitSelect)intBlock).getLowerBound();
		if(upperBound!=lowerBound)
			result += " ("+upperBound+" downto "+lowerBound+")";
		else
			result += " ("+upperBound+")";
		
		return result;
	}
	
	public static String extendedRegisterWidth(Register reg){
		String result = "";
		DataFlowBlock intBlock;
		int bitWidth = 0;
		
		intBlock = reg.getInput().getWire().getSource().getParentNode();
		bitWidth = ((BitSelect)intBlock).getInput().getWidth();
		if( bitWidth == 1 )
			result += "std_logic";
		else
			result += "std_logic_vector(" + (bitWidth - 1) + " downto 0)";
		
		return result;
	}
	
	public static String extendedPadWidth(DataOutputPad pad){
		String result = "";
		try {
			OutDataPort op = pad.getInput().getWire().getSource();

			int bitWidth = op.getWidth();
			if( bitWidth == 1 )
				result += "std_logic";
			else
				result += "std_logic_vector(" + (bitWidth - 1) + " downto 0)";
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static String optimizePath(DataFlowBlock block){
		String result = "";
		String resultLeft = "";
		String resultRight = "";
		String resultOperator = null;
		DataFlowBlock intBlock, leftTerm, rightTerm;
		
		if(block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))
				|| block.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
			intBlock = block.getInput(0).getWire().getSource().getParentNode();
		}else{
			intBlock = block;
		}
		
		try {
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl"))){
				resultOperator = VHDLOperator.makeOperation(intBlock);
				if(resultOperator != null)
					result += resultOperator;
				return result;
			}
		} catch (ClassNotFoundException e) {
			// Wrong class given as parameter
			e.printStackTrace();
		}
		leftTerm = intBlock.getInput(0).getWire().getSource().getParentNode();
		if(leftTerm != null){
			if(leftTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))
					|| leftTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
				
				resultLeft += " " + VHDLGenUtils.makeOutputName((SingleOutputDataFlowBlock) leftTerm) + " ";
			}else{
				resultLeft += " (" + optimizePath(leftTerm) + ") ";
			}
		}else{
			System.err.println("attempt to optimize block with empty left operand");
		}
		
		resultOperator = VHDLOperator.makeOperation(intBlock);
		
		if(!(intBlock instanceof SingleInputDataFlowBlock)){
			rightTerm = intBlock.getInput(1).getWire().getSource().getParentNode();
			if(rightTerm != null){
				if(rightTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.storage.impl"))
						|| rightTerm.getClass().getPackage().equals(Package.getPackage("fr.irisa.cairn.model.datapath.pads.impl"))){
					
					resultRight += " " + VHDLGenUtils.makeOutputName((SingleOutputDataFlowBlock) rightTerm) + " ";
				}else{
					resultRight += " (" + optimizePath(rightTerm) + ") ";
				}
			}else{
				System.err.println("attempt to optimize block with empty right operand");
			}
		}
		
		try {
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl"))){
				switch (((BinaryOperator)intBlock).getOpcode().getValue()) {
				case BinaryOpcode.ADD_VALUE :
				case BinaryOpcode.SUB_VALUE :
				case BinaryOpcode.MUL_VALUE :
				case BinaryOpcode.DIV_VALUE :
				case BinaryOpcode.AND_VALUE :
				case BinaryOpcode.OR_VALUE :
				case BinaryOpcode.XOR_VALUE :
					result += resultLeft + resultOperator + resultRight;
					break;
				case BinaryOpcode.MAX_VALUE :
				case BinaryOpcode.MIN_VALUE :
				case BinaryOpcode.SHL_VALUE :
				case BinaryOpcode.SHR_VALUE :
					result += resultOperator + "(" + resultLeft + "," + resultRight + ")";
					break;
				case BinaryOpcode.NAND_VALUE :
					result += "not(" + resultLeft + resultOperator + resultRight + ")";
					break;
				case BinaryOpcode.CMP_VALUE :
					result += resultLeft + "-" + resultRight + ";\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" <= "+resultRight+") or("+resultLeft+" >= "+resultRight+"))\n";
					result += "\tfsm_lt_GT0 <= '0'\n";
					result += "else\n\tfsm_lt_GT0 <= '1'\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" > "+resultRight+") or("+resultLeft+" = "+resultRight+") or("+resultLeft+" /= "+resultRight+"))\n";
					result += "\tfsm_lt_GT1 <= '0'\n";
					result += "else\n\tfsm_lt_GT1 <= '1'\n";
					result += "if" + "(("+resultLeft+" < "+resultRight+") or("+resultLeft+" <= "+resultRight+") or("+resultLeft+" = "+resultRight+"))\n";
					result += "\tfsm_lt_GT2 <= '0'\n";
					result += "else\n\tfsm_lt_GT3 <= '1'\n";
					break;

				default:
					break;
				}
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl"))){
				result += resultOperator + resultLeft;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl"))){
				result += resultLeft + resultOperator;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.MergeImpl"))){
				result += resultLeft + resultOperator + resultRight;
			}
			if(intBlock.getClass().equals(Class.forName("fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl"))){
				result += resultOperator;
			}
			
		} catch (ClassNotFoundException e) {
			// Wrong class given as parameter
			e.printStackTrace();
		}
		
		return result;
	}
}









