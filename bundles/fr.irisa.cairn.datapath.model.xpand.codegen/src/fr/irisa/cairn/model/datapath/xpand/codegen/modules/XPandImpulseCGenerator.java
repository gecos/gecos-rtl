//package fr.irisa.cairn.model.datapath.xpand.codegen.modules;
//
//import javax.management.RuntimeErrorException;
//
//import org.eclipse.core.runtime.CoreException;
//
//import fr.irisa.cairn.model.datapath.Datapath;
//import fr.irisa.cairn.model.datapath.xml.DatapathXMLReader;
//
//public class XPandImpulseCGenerator extends AbstractXpandCodeGenerator{
//
//	
//	private XPandImpulseCGenerator(String outdir) {
//		xpandTemplateName = "impulsec::Datapath::VHDLModule";
//		this.outdir=outdir;
//	}
//	
//	public XPandImpulseCGenerator(Datapath target, String outdir) {
//		this(outdir);
//		targetDatapath = target;
//	}
//
//	public XPandImpulseCGenerator(String target, String outdir) {
//		this(outdir);
//		DatapathXMLReader reader = new DatapathXMLReader();
//		targetDatapath = reader.load(target);
//	}
//
//	
//	public void generateImpulseC() {
//		try {
//			super.generate();
//		} catch (CoreException e) {
//			e.printStackTrace();
//			throw new RuntimeException(e.getMessage()) ;
//		}
//	}
//}
