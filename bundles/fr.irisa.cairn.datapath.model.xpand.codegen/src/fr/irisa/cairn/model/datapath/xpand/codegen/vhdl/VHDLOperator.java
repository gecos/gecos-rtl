package fr.irisa.cairn.model.datapath.xpand.codegen.vhdl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InDataPort;


public class VHDLOperator extends OperatorsSwitch<String>{

	static VHDLOperator singleton = null;

	public static VHDLOperator getSingleton() {
		if(singleton==null) {
			singleton = new VHDLOperator();
		}
		return singleton;
	}

	public static String makeOperation(AbstractBlock theEObject) {
		return getSingleton().doSwitch(theEObject);
	}

	public VHDLOperator() {
		super();
	}

	@Override
	public String caseExpandSigned(ExpandSigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseExpandUnsigned(ExpandUnsigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseQuantize(Quantize object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseReductionOperator(ReductionOperator object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseBinaryOperator(BinaryOperator binop) { 
		String opA = VHDLGenUtils.makeSourceName(binop,0);
		String opB = VHDLGenUtils.makeSourceName(binop,1);
		switch (binop.getOpcode().getValue()) {
			case BinaryOpcode.ADD_VALUE :
				return "+"; 
			case BinaryOpcode.SUB_VALUE :
				return "-"; 
			case BinaryOpcode.MUL_VALUE :
				return "*"; 
			case BinaryOpcode.DIV_VALUE :
				return "/";
			case BinaryOpcode.MAX_VALUE :
				return "maximum"; 
			case BinaryOpcode.MIN_VALUE :
				return "minimum";
			case BinaryOpcode.NAND_VALUE :
				//further necessary computations will be done in place
				return "and";
			case BinaryOpcode.XOR_VALUE :
				return "xor"; 
			case BinaryOpcode.AND_VALUE :
				return "and";
			case BinaryOpcode.OR_VALUE :
				return "or";	
			case BinaryOpcode.SHL_VALUE :
				return "shl";
			case BinaryOpcode.SHR_VALUE :
				return "shr";
			case BinaryOpcode.CMP_VALUE :
				//further necessary computations will be done in place
				return "";
			default :
				throw new UnsupportedOperationException("Unsupport BinaryOpcode for VHDL synthesis: "+binop.getOpcode());
		}
	}

	@Override
	public String caseUnaryOperator(UnaryOperator unop) { 
		String sourceName = VHDLGenUtils.makeSourceName(unop,0);
		switch (unop.getOpcode().getValue()) {
			case UnaryOpcode.INV_VALUE:
				return "inv";
			case UnaryOpcode.NEG_VALUE :
				return "-";
			case UnaryOpcode.NOT_VALUE :
				return "not";
			case UnaryOpcode.SQRT_VALUE :
				return "sqrt";
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for VHDL synthesis :"+unop.getOpcode());
		}
		
	}

	@Override
	public String caseBitSelect(BitSelect select) {
//		int upperBound = select.getUpperBound();
//		int lowerBound = select.getLowerBound();
//		if(upperBound!=lowerBound)
//			return " ("+upperBound+" downto "+lowerBound+")";
//		else
//			return " ("+upperBound+")";
		//replaced -- now done in place
		return "";
			
	}
	
	@Override
	public String caseMerge(Merge merge) {
		return "&";
	}
	
	@Override
	public String caseConstantValue(ConstantValue cst) {
		String binaryValue = VHDLGenUtils.binaryValue( cst.getValue(), cst.getOutput().getWidth());
		if ( cst.getOutput().getWidth() == 1 ) {
			return " '"+binaryValue+"' "; 
		} else {
			return " \""+binaryValue+"\" "; 
		}
	}

	@Override
	public String caseDataFlowMux(DataFlowMux dmux ) {
		return "";
	}

	@Override
	public String caseControlFlowMux(ControlFlowMux cmux) {
		return "";
	}

	@Override
	public String caseCtrl2DataBuffer(Ctrl2DataBuffer c2dbuffer) {
		return "";
	}

	@Override
	public String caseData2CtrlBuffer(Data2CtrlBuffer d2cbuffer) {
		return "";
	}

	@Override
	public String caseCompare(Compare cmp) {
		return "";
	}
					
	@Override
	public String caseTernaryOperator(TernaryOperator terop) {
		String src0 = VHDLGenUtils.makeSourceName(terop,0);
		String src1 = VHDLGenUtils.makeSourceName(terop,1);
		String src2 = VHDLGenUtils.makeSourceName(terop,2);
		switch (terop.getOpcode().getValue()) {
			case TernaryOpcode.MULADD_VALUE :
				return " ("+src0+"*"+src1+")+"+src2+" ;";
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+terop.getOpcode());
		}
	}
	
}
