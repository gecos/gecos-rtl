package fr.irisa.cairn.model.datapath.xpand.codegen.modules;

import fr.irisa.cairn.model.datapath.Datapath;

public class XPandSCGenerator extends AbstractXpandCodeGenerator{

	
	public XPandSCGenerator(Datapath target, String outdir) {
		super(target,outdir);
		xpandTemplateName = "fr::irisa::cairn::model::datapath::xpand::codegen::systemc::SCModule::SCModule";
	}

	public XPandSCGenerator(String target, String outdir) {
		super(target,outdir);
		xpandTemplateName = "fr::irisa::cairn::model::datapath::xpand::codegen::systemc::SCModule::SCModule";
	}
	
	
}
