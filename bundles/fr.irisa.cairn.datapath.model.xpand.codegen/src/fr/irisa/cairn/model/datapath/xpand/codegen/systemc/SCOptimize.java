package fr.irisa.cairn.model.datapath.xpand.codegen.systemc;


import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InDataPort;


public class SCOptimize extends OperatorsSwitch<String>{

	static SCOptimize singleton = null;

	public static SCOptimize getSingleton() {
		if(singleton==null) {
			singleton = new SCOptimize();
		}
		return singleton;
	}

	public static String optimizeOperator(AbstractBlock theEObject) {
		return getSingleton().doSwitch(theEObject);
	}

	public SCOptimize() {
		super();
	}

	@Override
	public String caseExpandSigned(ExpandSigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseExpandUnsigned(ExpandUnsigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseQuantize(Quantize object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseReductionOperator(ReductionOperator object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseBinaryOperator(BinaryOperator binop) { 
		String opA = binop.getName() + "_" + binop.getInput(0).getName();
		String opB = binop.getName() + "_" + binop.getInput(1).getName();
		
		switch (binop.getOpcode().getValue()) {
			case BinaryOpcode.ADD_VALUE :
				return "+"; 
			case BinaryOpcode.SUB_VALUE :
				return "-"; 
			case BinaryOpcode.MUL_VALUE :
				return "*"; 
			case BinaryOpcode.DIV_VALUE :
				return "/";
			case BinaryOpcode.MAX_VALUE :
				return "fmax"; 
			case BinaryOpcode.MIN_VALUE :
				return "fmin";
			case BinaryOpcode.NAND_VALUE :
				//further necessary computations will be done in place
				return "&";
			case BinaryOpcode.XOR_VALUE :
				return "^"; 
			case BinaryOpcode.AND_VALUE :
				return "&";
			case BinaryOpcode.OR_VALUE :
				return "|";	
			case BinaryOpcode.SHL_VALUE :
				return "<<";
			case BinaryOpcode.SHR_VALUE :
				return ">>";
			case BinaryOpcode.CMP_VALUE :
				//further necessary computations will be done in place
				return "";
			default :
				throw new UnsupportedOperationException("Unsupport BinaryOpcode for VHDL synthesis: "+binop.getOpcode());
		}
	}

	@Override
	public String caseUnaryOperator(UnaryOperator unop) { 
		String sourceName = unop.getName() + "_" + unop.getInput().getName();
		
		switch (unop.getOpcode().getValue()) {
			case UnaryOpcode.INV_VALUE:
				return "~";
			case UnaryOpcode.NEG_VALUE :
				return "-";
			case UnaryOpcode.NOT_VALUE :
				return "~";
			case UnaryOpcode.SQRT_VALUE :
				return "sqrt";
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for VHDL synthesis :"+unop.getOpcode());
		}
		
	}

	@Override
	public String caseBitSelect(BitSelect select) {
		int upperBound = select.getUpperBound();
		int lowerBound = select.getLowerBound();
		
		if(upperBound!=lowerBound)
			return " .range("+upperBound+" , "+lowerBound+")";
		else
			//return " ["+upperBound+"]";
			//first alternative is error prone; prefer second alternative
			return " .range("+upperBound+" , "+upperBound+")";	
	}
	
	@Override
	public String caseMerge(Merge merge) {
		return ",";
	}
	
	@Override
	public String caseConstantValue(ConstantValue cst) {
		String binaryValue = "" + cst.getValue();
		
		return binaryValue;
	}

	@Override
	public String caseDataFlowMux(DataFlowMux dmux ) {
		return "";
	}

	@Override
	public String caseControlFlowMux(ControlFlowMux cmux) {
		return "";
	}

	@Override
	public String caseCtrl2DataBuffer(Ctrl2DataBuffer c2dbuffer) {
		return "";
	}

	@Override
	public String caseData2CtrlBuffer(Data2CtrlBuffer d2cbuffer) {
		return "";
	}

	@Override
	public String caseCompare(Compare cmp) {
		return "";
	}
					
	@Override
	public String caseTernaryOperator(TernaryOperator terop) {
		String src0 = terop.getName() + "_" + terop.getInput(0).getName();
		String src1 = terop.getName() + "_" + terop.getInput(1).getName();
		String src2 = terop.getName() + "_" + terop.getInput(2).getName();
		
		switch (terop.getOpcode().getValue()) {
			case TernaryOpcode.MULADD_VALUE :
				return " ("+src0+"*"+src1+")+"+src2+" ;";
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE synthesis :"+terop.getOpcode());
		}
	}
	
}

