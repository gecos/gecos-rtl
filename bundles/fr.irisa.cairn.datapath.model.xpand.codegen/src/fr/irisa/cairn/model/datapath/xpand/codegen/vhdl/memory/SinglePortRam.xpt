«IMPORT fr::irisa::cairn::model::datapath»
«IMPORT fr::irisa::cairn::model::datapath::storage»
«IMPORT fr::irisa::cairn::model::fsm»

«EXTENSION fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::VHDLHelper»

«DEFINE VHDLModule FOR SinglePortRam»
«FILE this.name+"_ram.vhd"»
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY «this.name» IS
	PORT
	(
		clock			: IN  std_logic;
		din				: IN  std_logic_vector(«this.writePort.width- 1» DOWNTO 0);
		re				: IN  std_logic;
		write_address	: IN  std_logic_vector(«this.addressPort.width-1» DOWNTO 0);
		read_address	: IN  std_logic_vector(«this.addressPort.width-1» DOWNTO 0);
		we				: IN  std_logic;
		dout			: OUT std_logic_vector(«this.readPort.width- 1» DOWNTO 0)
	);
END «this.name»;

ARCHITECTURE rtl OF «this.name» IS
	TYPE RAM IS ARRAY(0 TO 2 ** «this.addressPort.width» - 1) OF std_logic_vector(«this.writePort.width- 1» DOWNTO 0);

	SIGNAL ram_block : RAM;
BEGIN
	PROCESS (clock)
	BEGIN
		IF (clock'event AND clock = '1') THEN
			IF (we = '1') THEN
			    ram_block(to_integer(unsigned(write_address))) <= din;
			END IF;
			IF (re = '1') THEN
				dout <= ram_block(to_integer(unsigned(read_address)));
			END IF;
		END IF;
	END PROCESS;
END rtl;
«ENDFILE»
«ENDDEFINE»

«DEFINE VHDLModulebis FOR SinglePortRam»
«FILE this.name+".vhd"»
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ram IS
	GENERIC
	(
		ADDRESS_WIDTH	: integer := «this.addressPort.width»;
		DATA_WIDTH		: integer := «this.writePort.width»
	);
	PORT
	(
		clk				: IN  std_logic;
		rst				: IN  std_logic;
		din				: IN  std_logic_vector(DATA_WIDTH - 1 DOWNTO 0);
		write_address	: IN  std_logic_vector(ADDRESS_WIDTH - 1 DOWNTO 0);
		read_address	: IN  std_logic_vector(ADDRESS_WIDTH - 1 DOWNTO 0);
		we				: IN  std_logic;
		dout				: OUT std_logic_vector(DATA_WIDTH - 1 DOWNTO 0)
	);
END ram;

ARCHITECTURE rtl OF ram IS
	TYPE RAM IS ARRAY(0 TO 2 ** ADDRESS_WIDTH - 1) OF std_logic_vector(DATA_WIDTH - 1 DOWNTO 0);

	SIGNAL ram_block : RAM;
BEGIN
	PROCESS (clock)
	BEGIN
		IF (clock'event AND clock = '1') THEN
			IF (we = '1') THEN
			    ram_block(to_integer(unsigned(write_address))) <= din;
			END IF;

			dout <= ram_block(to_integer(unsigned(read_address)));
		END IF;
	END PROCESS;
END rtl;
«ENDFILE»
«ENDDEFINE»