package fr.irisa.cairn.model.datapath.xtend2.codegen.vhdl

import fr.irisa.cairn.model.datapath.Port
import fr.irisa.cairn.model.datapath.port.InControlPort
import fr.irisa.cairn.model.datapath.port.OutControlPort
import fr.irisa.cairn.model.datapath.port.InDataPort
import fr.irisa.cairn.model.datapath.port.OutDataPort
import fr.irisa.cairn.model.datapath.Datapath
import org.eclipse.emf.mwe2.runtime.workflow.AbstractCompositeWorkflowComponent
import fr.irisa.cairn.model.datapath.AbstractBlock
import fr.irisa.cairn.model.datapath.Wire
import fr.irisa.cairn.model.datapath.ActivableBlock
import fr.irisa.cairn.model.datapath.FlagBearerBlock
import fr.irisa.cairn.model.datapath.DataFlowBlock
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.BasicEList

class StructuralVHDLCodegen {
	
	def stdLogicType(Integer width) {
		if (width==1) 
			'''std_logic''' 
		else 
			'''std_logic_vector(�(width-1)� downto 0)''';
	}
		 
	
//	def dispatch genPortDeclaration (Port obj) 
	
	def dispatch genPortDeclaration (InControlPort obj) {
		'''�obj.name� : in �stdLogicType(obj.width)�'''
	}
	
	def dispatch genPortDeclaration (OutControlPort obj) {
		'''�obj.name� : out �stdLogicType(obj.width)�'''
	}
	
	def dispatch genPortDeclaration (InDataPort obj) {
		'''�obj.name� : in �stdLogicType(obj.width)�'''
	}
	
	def dispatch genPortDeclaration (OutDataPort obj) {
		'''�obj.name� : out �stdLogicType(obj.width)�'''
	}
	
	def getPortList(AbstractBlock obj) {
		
		var EList<Port> ports  = new BasicEList<Port>();
		if(obj instanceof ActivableBlock) {
			ports.addAll((obj as ActivableBlock).activate);
		}
		
		if(obj instanceof FlagBearerBlock) {
			ports.addAll((obj as FlagBearerBlock).flags);
		}
		
		if(obj instanceof DataFlowBlock) {
			ports.addAll((obj as DataFlowBlock).in);
			ports.addAll((obj as DataFlowBlock).out);
		}
		return ports;
	}
	
	
	
	def generateInterfaceDeclaration ( AbstractBlock obj) {
		var CharSequence res = ''' '''
		if(!obj.combinational) {
			res = '''
			-- Global signals
			rst : in std_logic;
			clk : in std_logic
			'''
		}
		if(obj instanceof ActivableBlock) {
			return '''
			�res�
			�FOR Port p: (obj as ActivableBlock).activate SEPARATOR ";"��genPortDeclaration(p)��ENDFOR�
			''';
		}
		if(obj instanceof FlagBearerBlock) {
			return '''
			�res�
			�FOR Port p: (obj as FlagBearerBlock).flags SEPARATOR ";"��genPortDeclaration(p)��ENDFOR�
			''';
		}
		if(obj instanceof DataFlowBlock) {
			'''
			�res�
			�FOR Port p: (obj as DataFlowBlock).in SEPARATOR ";"��genPortDeclaration(p)��ENDFOR�
			�FOR Port p: (obj as DataFlowBlock).out SEPARATOR ";"��genPortDeclaration(p)��ENDFOR�
			''';
		}
	}


	def dispatch generateComponentDeclaration ( AbstractBlock obj) {
		'''
		component �obj.name� port (
		�generateInterfaceDeclaration(obj)�
		)
		'''
	}
	
	def dispatch generateEntityDeclaration ( AbstractBlock obj) {
		'''
		entity �obj.name� is port (
		�generateInterfaceDeclaration(obj)�
		)
		'''
	}
	
/* 
def dispatch ComponentCreation ( FSM obj) {
	�EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::fsm::FSM::VHDLModule ( this�
}

def dispatch ComponentCreation ( AbstractMemory obj) {
}

def dispatch ComponentCreation ( SinglePortRam obj) {
	�EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::SinglePortRam::VHDLModule ( this�
}

def dispatch ComponentCreation ( SinglePortRom obj) {
	�EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::SinglePortRom::VHDLModule ( this�
}

def dispatch ComponentCreation ( DualPortRam obj) {
	�EXPAND fr::irisa::cairn::model::datapath::xpand::codegen::vhdl::memory::DualPortRam::VHDLModule ( this� 
}
*/
	def generateWire(Wire w) {
		
	}
	def dispatch generateArchitectureDescription(Datapath obj) {
	
	'''
		architecture RTL of �obj.name� is
		
			-- Components 
			�FOR AbstractBlock b : obj.components�
				�generateComponentDeclaration(b)�
			�ENDFOR�
		
			-- Wires 
			�FOR Wire b : obj.wires�
���				�generateWire(b)�
			�ENDFOR�
		
		
			 
		���	-- Shift Logical Left.
		���  	FUNCTION sll (arg1: std_logic_vector; arg2: std_logic_vector) RETURN std_logic_vector is
		���    begin
		���    
		���    end sll;
		���
		���  	-- Shift Logical Right.
		���  	FUNCTION srl (arg1: std_logic_vector; arg2: std_logic_vector)
		���    RETURN std_logic_vector;
		���    begin
		���    end sll;
		���
		���  	-- Shift Arithmetic Right.
		���  	FUNCTION sra (arg1: std_logic_vector; arg2: std_logic_vector)
		���    RETURN std_logic_vector;
		���    begin
		���    end sll;
			 
			
			function std_range(a: in std_logic_vector; ub : in integer; lb : in integer) return std_logic_vector is
			variable tmp : std_logic_vector(ub downto lb);
			begin
				tmp:=  a(ub downto lb);
				return tmp;
			end std_range; 
			
		begin
			-- Components 
			�FOR AbstractBlock b : obj.components�
���				�generateComponentInstance(b)�
			�ENDFOR�
		
���			�generateWiresConnexion(obj)�
���			�generateInlinedRegister (obj)�
���			�generateInlinedPad(obj)�
		end RTL;
	'''
	}
}