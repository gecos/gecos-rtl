package fr.irisa.cairn.model.datapath.xpand.codegen.systemc;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.xpand.codegen.vhdl.VHDLGenUtils;



public class SCOperator extends OperatorsSwitch<String>{

	static SCOperator singleton = null;

	public static SCOperator getSingleton() {
		if(singleton==null) {
			singleton = new SCOperator();
		}
		return singleton;
	}

	public SCOperator() {
		super();
	}
	
	public static String operatorImplement(DataFlowBlock theEObject) {
		return getSingleton().doSwitch(theEObject);
	}
	
	public static String registerImplement(Register theEObject) {
		String result = "";
		
		
		
		return result;
	}

	@Override
	public String caseExpandSigned(ExpandSigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseExpandUnsigned(ExpandUnsigned object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseQuantize(Quantize object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseReductionOperator(ReductionOperator object) {
		throw new UnsupportedOperationException("Unsupported Operator "+object.getClass().getSimpleName());
	}

	@Override
	public String caseBinaryOperator(BinaryOperator binop) {
		String input0 = "" + binop.getInput(0).getName() + "_int";
		String input1 = "" + binop.getInput(1).getName() + "_int";
		String output = "" + binop.getOutput().getName() + "_int";
		String result = "";
		
		switch (binop.getOpcode().getValue()) {
			case BinaryOpcode.ADD_VALUE :
				result += output + " = ";
				result += input0 + " + " + input1;
				break;
				
			case BinaryOpcode.SUB_VALUE :
				result += output + " = ";
				result += input0 + " - " + input1;
				break;
				
			case BinaryOpcode.MUL_VALUE :
				result += output + " = ";
				result += input0 + " * " + input1;
				break;
				
			case BinaryOpcode.DIV_VALUE :
				result += output + " = ";
				result += input0 + " / " + input1;
				break;
				
			case BinaryOpcode.MAX_VALUE :
				result += "if( " + input0 + ">" + input1 + " ) then\n\t";
				result += "" + output + " = " + input0 + ";\n";
				result += "else\n\t";
				result += "" + output + " = " + input1 + "";
				break;
				
			case BinaryOpcode.MIN_VALUE :
				result += "if( " + input0 + "<" + input1 + " ) then\n\t";
				result += "" + output + " = " + input0 + ";\n";
				result += "else\n\t";
				result += "" + output + " = " + input1 + "";
				break;
				
			case BinaryOpcode.NAND_VALUE :
				result += output + " = ";
				result += "~( " + input0 + " & " + input1 + " )";
				break;
				
			case BinaryOpcode.XOR_VALUE :
				result += output + " = ";
				result += input0 + " ^ " + input1;
				break;
				
			case BinaryOpcode.AND_VALUE :
				result += output + " = ";
				result += input0 + " & " + input1;
				break;
				
			case BinaryOpcode.OR_VALUE :
				result += output + " = ";
				result += input0 + " | " + input1;
				break;
					
			case BinaryOpcode.SHL_VALUE :
				result += output + " = ";
				result += input0 + " << " + input1;
				break;
				
			case BinaryOpcode.SHR_VALUE :
				result += output + " = ";
				result += input0 + " >> " + input1;
				break;
				
			case BinaryOpcode.CMP_VALUE :
				throw new UnsupportedOperationException("Unsupported Binary Operator: "+binop.getOpcode());
					
			default :
				throw new UnsupportedOperationException("Unsupport Binary Opcode for SystemC code generation: "+binop.getOpcode());
		}
		
		return result;
	}

	@Override
	public String caseUnaryOperator(UnaryOperator unop) {
		String input0 = "" + unop.getInput().getName() + "_int";
		String output = "" + unop.getOutput().getName() + "_int";
		String result = "";
		
		switch (unop.getOpcode().getValue()) {
			case UnaryOpcode.INV_VALUE:
				result += output + " = ";
				result += "~ " + input0;
				break;
				
			case UnaryOpcode.NEG_VALUE :
				result += output + " = ";
				result += "- " + input0;
				break;
				
			case UnaryOpcode.NOT_VALUE :
				result += output + " = ";
				result += "~ " + input0;
				break;
				
			case UnaryOpcode.SQRT_VALUE :
				result += output + " = ";
				result += "(int)sqrt( " + input0 + " )";
				break;
				
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for VHDL synthesis :"+unop.getOpcode());
		}
		
		return result;
	}

	@Override
	public String caseBitSelect(BitSelect select) {
		String input0 = "" + select.getInput().getName() + "_int";
		String output = "" + select.getOutput().getName() + "_int";
		String result = "";
			
		int upperBound = select.getUpperBound();
		int lowerBound = select.getLowerBound();
		result += output + " = ";
		if(upperBound!=lowerBound){
			result += "" + input0 + ".range( " + upperBound + " , " + lowerBound + " )";
		}else{
			result += "" + input0 + "[" + upperBound + "]";
		}
		
		return result;
	}
	
	@Override
	public String caseMerge(Merge merge) {
		String input0 = "" + merge.getInput(0).getName() + "_int";
		String input1 = "" + merge.getInput(1).getName() + "_int";
		String output = "" + merge.getOutput().getName() + "_int";
		String result = "";
			
		result += output + " = ";
		result += "( " + input1 + " , " + input0 + " )";
		
		return result;
	}
	
	@Override
	public String caseConstantValue(ConstantValue cst) {
		String output = "" + cst.getOutput().getName() + "_int";
		String result = "";
		
		result += output + " = ";
		result += "" + cst.getValue();
		
		return result;
	}

	@Override
	public String caseDataFlowMux(DataFlowMux dmux ) {
		String input0 = "" + dmux.getInput(0).getName() + "_int";
		String input1 = "" + dmux.getInput(1).getName() + "_int";
		String input2 = "" + dmux.getInput(2).getName() + "_int";
		String output = "" + dmux.getOutput().getName() + "_int";
		String result = "";
		
		result += "if( " + input2 + " == 0 ) then\n\t";
		result += "" + output + " = " + input0 + ";\n";
		result += "else\n\t";
		result += "" + output + " = " + input1 + "";
		
		return result;
	}

	@Override
	public String caseControlFlowMux(ControlFlowMux cmux) {
		StringBuffer result = new StringBuffer();
		String select = "" + cmux.getControl().getName() + "_int";
		String input = "";
		String output = "" + cmux.getOutput().getName() + "_int";
		int offset=0;
		
		result.append("switch( " + select + "){\n\t");
		for (InDataPort ip : cmux.getIn()) {
			input = "" + ip.getName() + "_int";
			result.append("case " + offset + ":\n\t\t");
			result.append( "" + output + " = " + input + ";\n\t\t");
			result.append("break;\n\t");
			offset++;
		}
		result.append("}");
		
		return result.toString();  
	}

	@Override
	public String caseCtrl2DataBuffer(Ctrl2DataBuffer c2dbuffer) {
		String input = "" + c2dbuffer.getControl().getName() + "_int";
		String output = "" + c2dbuffer.getOutput().getName() + "_int";
		
		return "" + output +" = "+ input ;
	}

	@Override
	public String caseData2CtrlBuffer(Data2CtrlBuffer d2cbuffer) {
		String input = "" + d2cbuffer.getInput(0).getName() + "_int";
		String output = "" + d2cbuffer.getFlag().getName() + "_int";
		
		return "" + output +" = "+ input ;
	}

	@Override
	public String caseCompare(Compare cmp) {
		String result = "";
		String input0 = "" + cmp.getInput(0).getName() + "_int";
		String input1 = "" + cmp.getInput(1).getName() + "_int";
		String output = "" + cmp.getOutput().getName() + "_int";
		
		result += output + " = ";
		switch (cmp.getOpcode().getValue()) {
			case CompareOpcode.EQU_VALUE:
				result += input0 + " == " + input1;
				break; 
			case CompareOpcode.GT_VALUE:
				result += input0 + " > " + input1;
				break; 
			case CompareOpcode.GTE_VALUE:
				result += input0 + " >= " + input1;
				break; 
			case CompareOpcode.LT_VALUE:
				result += input0 + " < " + input1;
				break; 
			case CompareOpcode.LTE_VALUE:
				result += input0 + " <= " + input1;
				break; 
			case CompareOpcode.NEQ_VALUE:
				result += input0 + " != " + input1;
				break; 
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for COMPARE Operator :"+cmp.getOpcode());
		}
		
		return result;
	}
					
	@Override
	public String caseTernaryOperator(TernaryOperator terop) {
		String result = "";
		String input0 = "" + terop.getInput(0).getName() + "_int";
		String input1 = "" + terop.getInput(1).getName() + "_int";
		String input2 = "" + terop.getInput(2).getName() + "_int";
		String output = "" + terop.getOutput().getName() + "_int";
		
		
		result += output + " = ";
		switch (terop.getOpcode().getValue()) {
			case TernaryOpcode.MULADD_VALUE :
				result += "( " + input0 + " * " + input1 + " ) + " + input2;
				break;
			default :
				throw new UnsupportedOperationException("Unsupport Opcode for Ternary Operator :"+terop.getOpcode());
		}
		
		return result;
	}
	
}
