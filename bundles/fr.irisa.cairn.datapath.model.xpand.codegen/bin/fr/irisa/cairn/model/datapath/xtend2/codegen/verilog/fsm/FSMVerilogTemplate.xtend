package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm

import fr.irisa.cairn.model.fsm.FSM
import fr.irisa.cairn.model.fsm.State
import fr.irisa.cairn.model.fsm.AbstractCommandValue
import fr.irisa.cairn.model.fsm.BooleanCommandValue
import fr.irisa.cairn.model.fsm.IntegerCommandValue
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm
import fr.irisa.cairn.model.fsm.IntegerFlagTerm
import fr.irisa.cairn.model.fsm.OrExpression
import fr.irisa.cairn.model.fsm.AndExpression
import fr.irisa.cairn.model.fsm.NegateExpression
import fr.irisa.cairn.model.fsm.BooleanFlagTerm
import common.CommonHelper
import fr.irisa.cairn.model.fsm.Transition
import fr.irisa.cairn.model.fsm.BooleanValue
import fr.irisa.cairn.model.datapath.port.InputPort
import java.util.List
import fr.irisa.cairn.model.datapath.Port
import fr.irisa.cairn.model.datapath.port.InControlPort
import fr.irisa.cairn.model.datapath.port.OutControlPort
import java.io.PrintStream

class FSMVerilogTemplate {
	
	def dispatch generatePortDefinition(Port p) {
		
	}
	def dispatch generatePortDefinition(InControlPort p) {
		if(p.width>1) {
			'''input [�(p.width-1)�:0] �p.name� ;'''
		} else {
			'''input  �p.name� ;'''
		}
		
	}
	def dispatch generatePortDefinition(OutControlPort p) {
		if(p.width>1) {
			'''ouput [�(p.width-1)�:0] �p.name� ;'''
		} else {
			'''ouput  �p.name� ;'''
		}
		
	}

	def dispatch generatePredicate(AbstractBooleanExpression expr) {
		
	}
	
	def dispatch generatePredicate(IndexedBooleanFlagTerm expr) {
		if(expr.negated) {
			'''�expr.flag.name�[]�expr.offset�]==0'b'''
		} else {
			'''�expr.flag.name�[�expr.offset�]==1'b'''
		}
	}

	def dispatch generatePredicate(IntegerFlagTerm expr) {
		'''�expr.flag.name�==�CommonHelper::binaryValue(expr.value,expr.flag.width)�'b'''
	}

	def dispatch generatePredicate(OrExpression expr) {
		'''�FOR AbstractBooleanExpression t : expr.terms SEPARATOR " || "��generatePredicate(t)��ENDFOR�'''
	}

	def dispatch generatePredicate(AndExpression expr) {
		'''�FOR AbstractBooleanExpression t : expr.terms SEPARATOR " && "��generatePredicate(t)��ENDFOR�'''
	}

	def dispatch generatePredicate(NegateExpression expr) {
		'''~(�generatePredicate(expr.term)�)'''
	}
	

	def dispatch generatePredicate(BooleanFlagTerm expr) {
		if(expr.negated) {
			'''�expr.flag.name�==0'b'''
		} else {
			'''�expr.flag.name�==1'b'''
		}
	}
	

	def generateTransition(Transition t) {
		if(t.predicate==null) {
			'''
			state = �t.dst.label�;
			'''
		} else {
			'''
			if �generatePredicate(t.predicate)�
			begin
				state = �t.dst.label�;
			end
			'''
		}
		
	}
	


	def dispatch generateCommand(AbstractCommandValue command) {
		
	}
	
	def dispatch generateCommand(BooleanCommandValue command) {
		var String res = "";
		switch(command.value) {
			case BooleanValue::ONE : res= command.command.name+" = 1'b"
			case BooleanValue::ZERO: res= command.command.name+" = 0'b"
			case BooleanValue::DONT_CARE : res= command.command.name+"`DONTCARE"
		}
		if(command.predicate==null) {
			'''
			�res�;
			'''
		} else {
			'''
			if �generatePredicate(command.predicate)�
			begin
				�res�;
			end
			'''
		}
	}

	def dispatch generateCommand(IntegerCommandValue command) {
		if(command.predicate==null) {
			''' '''
		} else {
			''' '''
		}
	}

	def genPortList(List<Port> ports) {
		'''
		�FOR Port p: ports SEPARATOR ","��p.name��ENDFOR�
		'''
	}

	def generateCode(FSM fsm, String filename) {
		val PrintStream ps =  new PrintStream(filename);
		ps.append(generate(fsm));
		ps.close();
	}
	
	def generateCode(FSM fsm) {
		generateCode(fsm,"fsm"+fsm.name+".v");
	}

	def generate(FSM fsm) {
	'''
		
	//`define idle   5'b00000
	//`define B410_0 5'b00001
	//`define B421_0 5'b00010
	//`define B421_1 5'b00011
	//`define B412_0 5'b00100
	//`define B412_1 5'b00101
	//`define B413_0 5'b00110
	//`define B413_1 5'b00111
	//`define B413_2 5'b01000
	//`define B413_3 5'b01001
	//`define BB_13_0_0 5'b01010
	//`define BB_14_0_0 5'b01011
	//`define B415_0 5'b01100
	//`define B415_1 5'b01101
	//`define B415_2 5'b01110
	//`define B417_0 5'b01111
	//`define B411_0 5'b10000
	//`define B411_1 5'b10001
	//`define B411_2 5'b10010
	//`define B411_3 5'b10011

	module �fsm.name� ( clk, rst, �genPortList(fsm.activate as List)�, �genPortList(fsm.flags as List)�);
	
	input clk, rst;

	�FOR InControlPort ip : fsm.activate�
		�generatePortDefinition(ip)�
	�ENDFOR� 

	�FOR OutControlPort op : fsm.flags�
		�generatePortDefinition(op)�
	�ENDFOR� 

	reg [`NBITS-1:0] state;

	always @( posedge clk, posedge rst )
	begin
		if( rst )
			cs <= �fsm.start.label�;
		else
			begin
				case( state )
				�FOR State state : fsm.states� 
					�state.label.toUpperCase�_CONST: //2'b00
					begin
						�FOR Transition t : state.transitions�
						�generateTransition(t)�
						�ENDFOR�
					end
				�ENDFOR�
			end
	end


	always @(posedge clk, posedge rst)
	begin
		if( rst )
			begin
				outp <= 0;
			end
		else
			begin 
				case( state )
					�FOR State state : fsm.states� 
					�state.label.toUpperCase�_CONST: 
					begin
					�FOR AbstractCommandValue command : state.activatedCommands�
					�generateCommand(command)�
					�ENDFOR�
					end
					�ENDFOR�
				endcase
			end
	end

	endmodule
		
		'''
		
//`define idle   5'b00000
//`define B410_0 5'b00001
//`define B421_0 5'b00010
//`define B421_1 5'b00011
//`define B412_0 5'b00100
//`define B412_1 5'b00101
//`define B413_0 5'b00110
//`define B413_1 5'b00111
//`define B413_2 5'b01000
//`define B413_3 5'b01001
//`define BB_13_0_0 5'b01010
//`define BB_14_0_0 5'b01011
//`define B415_0 5'b01100
//`define B415_1 5'b01101
//`define B415_2 5'b01110
//`define B417_0 5'b01111
//`define B411_0 5'b10000
//`define B411_1 5'b10001
//`define B411_2 5'b10010
//`define B411_3 5'b10011
//
//module abs_fsm (
//	clk,
//	abs_enable,
//	lt_GT0,
//	lt_GT1,
//	lt_GT2, 
//	cOut, 
//	rdSrcAdr,				
//	rdDstAdr,
//	rdDstW,
//	rfInSel0,
//	rfInSel1,
//	aluOp0,
//	aluOp1,
//	aluOp2,
//	cIn,
//	op1Sel0,
//	op1Sel1,
//	op2Sel0,
//	op2Sel1,
//	ioPortW,
//	ioPortAdr,
//	ioDSel0,
//	ioDSel1,
//	immRAdr,
//	abs_event0,
//	abs_event1,
//	gVMASel0,
//	gVMASel1,
//	gVMDSel,
//	gVMDirAdr,
//	gVMW
//);
//
//input   clk;
//input   abs_enable;
//input	lt_GT0;
//input	lt_GT1;
//input	lt_GT2; 
//input	cOut; 
//output	rdSrcAdr;				
//output	rdDstAdr;
//output	rdDstW;
//output	rfInSel0;
//output	rfInSel1;
//output	aluOp0;
//output	aluOp1;
//output	aluOp2;
//output	cIn;
//output	op1Sel0;
//output	op1Sel1;
//output	op2Sel0;
//output	op2Sel1;
//output	ioPortW;
//output [5:0] ioPortAdr;
//output	ioDSel0;
//output	ioDSel1;
//output	immRAdr;
//output	abs_event0;
//output	abs_event1;
//output	gVMASel0;
//output	gVMASel1;
//output	gVMDSel;
//output	gVMDirAdr;
//output	gVMW;
//
//reg [5:0] CS;
//reg [5:0] NS;
//
//reg rdSrcAdr;
//reg	rdDstAdr;
//reg	rdDstW;
//reg	rfInSel0;
//reg	rfInSel1;
//reg	aluOp0;
//reg	aluOp1;
//reg	aluOp2;
//reg	cIn;
//reg	op1Sel0;
//reg	op1Sel1;
//reg	op2Sel0;
//reg	op2Sel1;
//reg	ioPortW;
//reg [5:0] ioPortAdr;
//reg	ioDSel0;
//reg	ioDSel1;
//reg	immRAdr;
//reg	abs_event0;
//reg	abs_event1;
//reg	gVMASel0;
//reg	gVMASel1;
//reg	gVMDSel;
//reg	gVMDirAdr;
//reg	gVMW;
//
//always@(posedge clk)
//begin
//      if (abs_enable == 1'b1)
//	      CS <= NS;
//	  else
//          CS <= `idle;
//end
//
//always@(abs_enable or lt_GT0 or lt_GT1 or lt_GT2)
//begin
//     case(CS) begin
//	 `idle: NS = `B410_0;
//	 `B410_0: NS = `B421_0;
//	 `B421_0: NS = `B421_1; rdSrcAdr = 1'b0;
//	 `B421_1: NS = `B412_0; rfInSel0 = 1'b0; rfInSel1 = 1'b1; rdDstW = 1'b1; rdDstAdr = 1'b1;
//	 `B412_0: NS = `B412_1; gVMSel_0 = 1'b0; gVMSel_1 = 1'b1;
//     `B412_1: NS = `B413_0; rfInSel0 = 1'b0; rfInSel1 = 1'b0; rdDstW = 1'b1; rdDstAdr = 1'b0;
//     default: NS = `idle;
//     endcase 
//end
//
//
//
//endmodule
		
	}
}