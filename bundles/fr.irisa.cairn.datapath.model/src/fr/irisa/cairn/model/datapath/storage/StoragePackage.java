/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.storage.StorageFactory
 * @model kind="package"
 * @generated
 */
public interface StoragePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "storage";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/storage";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "storage";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StoragePackage eINSTANCE = fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.RegisterImpl <em>Register</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.RegisterImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getRegister()
	 * @generated
	 */
	int REGISTER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER__NAME = DatapathPackage.DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER__PARENT = DatapathPackage.DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER__COMBINATIONAL = DatapathPackage.DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER__IN = DatapathPackage.DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER__OUT = DatapathPackage.DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Register</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FEATURE_COUNT = DatapathPackage.DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.CERegisterImpl <em>CE Register</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.CERegisterImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getCERegister()
	 * @generated
	 */
	int CE_REGISTER = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__NAME = DatapathPackage.ACTIVABLE_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__PARENT = DatapathPackage.ACTIVABLE_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__COMBINATIONAL = DatapathPackage.ACTIVABLE_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__ACTIVATE = DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__IN = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER__OUT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CE Register</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CE_REGISTER_FEATURE_COUNT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.ShiftRegisterImpl <em>Shift Register</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.ShiftRegisterImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getShiftRegister()
	 * @generated
	 */
	int SHIFT_REGISTER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__NAME = CE_REGISTER__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__PARENT = CE_REGISTER__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__COMBINATIONAL = CE_REGISTER__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__ACTIVATE = CE_REGISTER__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__IN = CE_REGISTER__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__OUT = CE_REGISTER__OUT;

	/**
	 * The feature id for the '<em><b>Depth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER__DEPTH = CE_REGISTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Shift Register</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHIFT_REGISTER_FEATURE_COUNT = CE_REGISTER_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.AbstractMemoryImpl <em>Abstract Memory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.AbstractMemoryImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAbstractMemory()
	 * @generated
	 */
	int ABSTRACT_MEMORY = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__NAME = DatapathPackage.ACTIVABLE_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__PARENT = DatapathPackage.ACTIVABLE_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__COMBINATIONAL = DatapathPackage.ACTIVABLE_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__ACTIVATE = DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__IN = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__OUT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY__CONTENT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Memory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MEMORY_FEATURE_COUNT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SinglePortRamImpl <em>Single Port Ram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.SinglePortRamImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSinglePortRam()
	 * @generated
	 */
	int SINGLE_PORT_RAM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__NAME = ABSTRACT_MEMORY__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__PARENT = ABSTRACT_MEMORY__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__COMBINATIONAL = ABSTRACT_MEMORY__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__ACTIVATE = ABSTRACT_MEMORY__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__IN = ABSTRACT_MEMORY__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__OUT = ABSTRACT_MEMORY__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM__CONTENT = ABSTRACT_MEMORY__CONTENT;

	/**
	 * The number of structural features of the '<em>Single Port Ram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_RAM_FEATURE_COUNT = ABSTRACT_MEMORY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.MultiPortRamImpl <em>Multi Port Ram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.MultiPortRamImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getMultiPortRam()
	 * @generated
	 */
	int MULTI_PORT_RAM = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__NAME = ABSTRACT_MEMORY__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__PARENT = ABSTRACT_MEMORY__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__COMBINATIONAL = ABSTRACT_MEMORY__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__ACTIVATE = ABSTRACT_MEMORY__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__IN = ABSTRACT_MEMORY__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__OUT = ABSTRACT_MEMORY__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__CONTENT = ABSTRACT_MEMORY__CONTENT;

	/**
	 * The feature id for the '<em><b>Nb Read Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__NB_READ_PORT = ABSTRACT_MEMORY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nb Write Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM__NB_WRITE_PORT = ABSTRACT_MEMORY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multi Port Ram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_RAM_FEATURE_COUNT = ABSTRACT_MEMORY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.DualPortRamImpl <em>Dual Port Ram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.DualPortRamImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getDualPortRam()
	 * @generated
	 */
	int DUAL_PORT_RAM = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__NAME = MULTI_PORT_RAM__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__PARENT = MULTI_PORT_RAM__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__COMBINATIONAL = MULTI_PORT_RAM__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__ACTIVATE = MULTI_PORT_RAM__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__IN = MULTI_PORT_RAM__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__OUT = MULTI_PORT_RAM__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__CONTENT = MULTI_PORT_RAM__CONTENT;

	/**
	 * The feature id for the '<em><b>Nb Read Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__NB_READ_PORT = MULTI_PORT_RAM__NB_READ_PORT;

	/**
	 * The feature id for the '<em><b>Nb Write Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM__NB_WRITE_PORT = MULTI_PORT_RAM__NB_WRITE_PORT;

	/**
	 * The number of structural features of the '<em>Dual Port Ram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUAL_PORT_RAM_FEATURE_COUNT = MULTI_PORT_RAM_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SinglePortRomImpl <em>Single Port Rom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.SinglePortRomImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSinglePortRom()
	 * @generated
	 */
	int SINGLE_PORT_ROM = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__NAME = ABSTRACT_MEMORY__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__PARENT = ABSTRACT_MEMORY__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__COMBINATIONAL = ABSTRACT_MEMORY__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__ACTIVATE = ABSTRACT_MEMORY__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__IN = ABSTRACT_MEMORY__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__OUT = ABSTRACT_MEMORY__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM__CONTENT = ABSTRACT_MEMORY__CONTENT;

	/**
	 * The number of structural features of the '<em>Single Port Rom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_PORT_ROM_FEATURE_COUNT = ABSTRACT_MEMORY_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.MultiPortRomImpl <em>Multi Port Rom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.MultiPortRomImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getMultiPortRom()
	 * @generated
	 */
	int MULTI_PORT_ROM = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__NAME = ABSTRACT_MEMORY__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__PARENT = ABSTRACT_MEMORY__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__COMBINATIONAL = ABSTRACT_MEMORY__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__ACTIVATE = ABSTRACT_MEMORY__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__IN = ABSTRACT_MEMORY__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__OUT = ABSTRACT_MEMORY__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__CONTENT = ABSTRACT_MEMORY__CONTENT;

	/**
	 * The feature id for the '<em><b>Nb Read Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM__NB_READ_PORT = ABSTRACT_MEMORY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Multi Port Rom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_PORT_ROM_FEATURE_COUNT = ABSTRACT_MEMORY_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.AsyncReadMemory <em>Async Read Memory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.AsyncReadMemory
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAsyncReadMemory()
	 * @generated
	 */
	int ASYNC_READ_MEMORY = 9;

	/**
	 * The number of structural features of the '<em>Async Read Memory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_MEMORY_FEATURE_COUNT = DatapathPackage.MEALY_SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.SyncReadMemory <em>Sync Read Memory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.SyncReadMemory
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSyncReadMemory()
	 * @generated
	 */
	int SYNC_READ_MEMORY = 10;

	/**
	 * The number of structural features of the '<em>Sync Read Memory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_MEMORY_FEATURE_COUNT = DatapathPackage.MOORE_SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SyncReadSPRAMImpl <em>Sync Read SPRAM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.SyncReadSPRAMImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSyncReadSPRAM()
	 * @generated
	 */
	int SYNC_READ_SPRAM = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__NAME = SINGLE_PORT_RAM__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__PARENT = SINGLE_PORT_RAM__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__COMBINATIONAL = SINGLE_PORT_RAM__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__ACTIVATE = SINGLE_PORT_RAM__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__IN = SINGLE_PORT_RAM__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__OUT = SINGLE_PORT_RAM__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM__CONTENT = SINGLE_PORT_RAM__CONTENT;

	/**
	 * The number of structural features of the '<em>Sync Read SPRAM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNC_READ_SPRAM_FEATURE_COUNT = SINGLE_PORT_RAM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.AsyncReadSPRAMImpl <em>Async Read SPRAM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.AsyncReadSPRAMImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAsyncReadSPRAM()
	 * @generated
	 */
	int ASYNC_READ_SPRAM = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__NAME = SINGLE_PORT_RAM__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__PARENT = SINGLE_PORT_RAM__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__COMBINATIONAL = SINGLE_PORT_RAM__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__ACTIVATE = SINGLE_PORT_RAM__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__IN = SINGLE_PORT_RAM__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__OUT = SINGLE_PORT_RAM__OUT;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM__CONTENT = SINGLE_PORT_RAM__CONTENT;

	/**
	 * The number of structural features of the '<em>Async Read SPRAM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASYNC_READ_SPRAM_FEATURE_COUNT = SINGLE_PORT_RAM_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.storage.impl.RegisterFileImpl <em>Register File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.storage.impl.RegisterFileImpl
	 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getRegisterFile()
	 * @generated
	 */
	int REGISTER_FILE = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__NAME = CE_REGISTER__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__PARENT = CE_REGISTER__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__COMBINATIONAL = CE_REGISTER__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__ACTIVATE = CE_REGISTER__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__IN = CE_REGISTER__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE__OUT = CE_REGISTER__OUT;

	/**
	 * The number of structural features of the '<em>Register File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTER_FILE_FEATURE_COUNT = CE_REGISTER_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.Register <em>Register</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Register</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.Register
	 * @generated
	 */
	EClass getRegister();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.CERegister <em>CE Register</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CE Register</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.CERegister
	 * @generated
	 */
	EClass getCERegister();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.ShiftRegister <em>Shift Register</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shift Register</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.ShiftRegister
	 * @generated
	 */
	EClass getShiftRegister();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.storage.ShiftRegister#getDepth <em>Depth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Depth</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.ShiftRegister#getDepth()
	 * @see #getShiftRegister()
	 * @generated
	 */
	EAttribute getShiftRegister_Depth();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.AbstractMemory <em>Abstract Memory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Memory</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.AbstractMemory
	 * @generated
	 */
	EClass getAbstractMemory();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.storage.AbstractMemory#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.AbstractMemory#getContent()
	 * @see #getAbstractMemory()
	 * @generated
	 */
	EAttribute getAbstractMemory_Content();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.SinglePortRam <em>Single Port Ram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Port Ram</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.SinglePortRam
	 * @generated
	 */
	EClass getSinglePortRam();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.DualPortRam <em>Dual Port Ram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dual Port Ram</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.DualPortRam
	 * @generated
	 */
	EClass getDualPortRam();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRam <em>Multi Port Ram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Port Ram</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.MultiPortRam
	 * @generated
	 */
	EClass getMultiPortRam();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRam#getNbReadPort <em>Nb Read Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Read Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.MultiPortRam#getNbReadPort()
	 * @see #getMultiPortRam()
	 * @generated
	 */
	EAttribute getMultiPortRam_NbReadPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRam#getNbWritePort <em>Nb Write Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Write Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.MultiPortRam#getNbWritePort()
	 * @see #getMultiPortRam()
	 * @generated
	 */
	EAttribute getMultiPortRam_NbWritePort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.SinglePortRom <em>Single Port Rom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Port Rom</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.SinglePortRom
	 * @generated
	 */
	EClass getSinglePortRom();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRom <em>Multi Port Rom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Port Rom</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.MultiPortRom
	 * @generated
	 */
	EClass getMultiPortRom();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRom#getNbReadPort <em>Nb Read Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Read Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.MultiPortRom#getNbReadPort()
	 * @see #getMultiPortRom()
	 * @generated
	 */
	EAttribute getMultiPortRom_NbReadPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.AsyncReadMemory <em>Async Read Memory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Async Read Memory</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.AsyncReadMemory
	 * @generated
	 */
	EClass getAsyncReadMemory();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.SyncReadMemory <em>Sync Read Memory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Read Memory</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.SyncReadMemory
	 * @generated
	 */
	EClass getSyncReadMemory();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM <em>Sync Read SPRAM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sync Read SPRAM</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM
	 * @generated
	 */
	EClass getSyncReadSPRAM();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.AsyncReadSPRAM <em>Async Read SPRAM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Async Read SPRAM</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.AsyncReadSPRAM
	 * @generated
	 */
	EClass getAsyncReadSPRAM();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.storage.RegisterFile <em>Register File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Register File</em>'.
	 * @see fr.irisa.cairn.model.datapath.storage.RegisterFile
	 * @generated
	 */
	EClass getRegisterFile();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StorageFactory getStorageFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.RegisterImpl <em>Register</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.RegisterImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getRegister()
		 * @generated
		 */
		EClass REGISTER = eINSTANCE.getRegister();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.CERegisterImpl <em>CE Register</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.CERegisterImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getCERegister()
		 * @generated
		 */
		EClass CE_REGISTER = eINSTANCE.getCERegister();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.ShiftRegisterImpl <em>Shift Register</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.ShiftRegisterImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getShiftRegister()
		 * @generated
		 */
		EClass SHIFT_REGISTER = eINSTANCE.getShiftRegister();

		/**
		 * The meta object literal for the '<em><b>Depth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHIFT_REGISTER__DEPTH = eINSTANCE.getShiftRegister_Depth();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.AbstractMemoryImpl <em>Abstract Memory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.AbstractMemoryImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAbstractMemory()
		 * @generated
		 */
		EClass ABSTRACT_MEMORY = eINSTANCE.getAbstractMemory();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_MEMORY__CONTENT = eINSTANCE.getAbstractMemory_Content();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SinglePortRamImpl <em>Single Port Ram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.SinglePortRamImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSinglePortRam()
		 * @generated
		 */
		EClass SINGLE_PORT_RAM = eINSTANCE.getSinglePortRam();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.DualPortRamImpl <em>Dual Port Ram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.DualPortRamImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getDualPortRam()
		 * @generated
		 */
		EClass DUAL_PORT_RAM = eINSTANCE.getDualPortRam();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.MultiPortRamImpl <em>Multi Port Ram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.MultiPortRamImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getMultiPortRam()
		 * @generated
		 */
		EClass MULTI_PORT_RAM = eINSTANCE.getMultiPortRam();

		/**
		 * The meta object literal for the '<em><b>Nb Read Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_PORT_RAM__NB_READ_PORT = eINSTANCE.getMultiPortRam_NbReadPort();

		/**
		 * The meta object literal for the '<em><b>Nb Write Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_PORT_RAM__NB_WRITE_PORT = eINSTANCE.getMultiPortRam_NbWritePort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SinglePortRomImpl <em>Single Port Rom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.SinglePortRomImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSinglePortRom()
		 * @generated
		 */
		EClass SINGLE_PORT_ROM = eINSTANCE.getSinglePortRom();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.MultiPortRomImpl <em>Multi Port Rom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.MultiPortRomImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getMultiPortRom()
		 * @generated
		 */
		EClass MULTI_PORT_ROM = eINSTANCE.getMultiPortRom();

		/**
		 * The meta object literal for the '<em><b>Nb Read Port</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_PORT_ROM__NB_READ_PORT = eINSTANCE.getMultiPortRom_NbReadPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.AsyncReadMemory <em>Async Read Memory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.AsyncReadMemory
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAsyncReadMemory()
		 * @generated
		 */
		EClass ASYNC_READ_MEMORY = eINSTANCE.getAsyncReadMemory();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.SyncReadMemory <em>Sync Read Memory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.SyncReadMemory
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSyncReadMemory()
		 * @generated
		 */
		EClass SYNC_READ_MEMORY = eINSTANCE.getSyncReadMemory();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.SyncReadSPRAMImpl <em>Sync Read SPRAM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.SyncReadSPRAMImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getSyncReadSPRAM()
		 * @generated
		 */
		EClass SYNC_READ_SPRAM = eINSTANCE.getSyncReadSPRAM();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.AsyncReadSPRAMImpl <em>Async Read SPRAM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.AsyncReadSPRAMImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getAsyncReadSPRAM()
		 * @generated
		 */
		EClass ASYNC_READ_SPRAM = eINSTANCE.getAsyncReadSPRAM();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.storage.impl.RegisterFileImpl <em>Register File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.storage.impl.RegisterFileImpl
		 * @see fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl#getRegisterFile()
		 * @generated
		 */
		EClass REGISTER_FILE = eINSTANCE.getRegisterFile();

	}

} //StoragePackage
