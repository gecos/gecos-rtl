/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fr.irisa.cairn.model.datapath.DatapathPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.port.PortFactory
 * @model kind="package"
 * @generated
 */
public interface PortPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "port";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/port";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "port";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PortPackage eINSTANCE = fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl <em>In Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl
	 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInDataPort()
	 * @generated
	 */
	int IN_DATA_PORT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__NAME = DatapathPackage.PORT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__WIDTH = DatapathPackage.PORT__WIDTH;

	/**
	 * The feature id for the '<em><b>Wire</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__WIRE = DatapathPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Pad</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT__ASSOCIATED_PAD = DatapathPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>In Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_DATA_PORT_FEATURE_COUNT = DatapathPackage.PORT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl <em>Out Data Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl
	 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getOutDataPort()
	 * @generated
	 */
	int OUT_DATA_PORT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl <em>In Control Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl
	 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInControlPort()
	 * @generated
	 */
	int IN_CONTROL_PORT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__NAME = DatapathPackage.PORT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__WIDTH = DatapathPackage.PORT__WIDTH;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__WIRES = DatapathPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Pad</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT__ASSOCIATED_PAD = DatapathPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Out Data Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_DATA_PORT_FEATURE_COUNT = DatapathPackage.PORT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__NAME = DatapathPackage.PORT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__WIDTH = DatapathPackage.PORT__WIDTH;

	/**
	 * The feature id for the '<em><b>Wire</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__WIRE = DatapathPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Pad</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT__ASSOCIATED_PAD = DatapathPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>In Control Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IN_CONTROL_PORT_FEATURE_COUNT = DatapathPackage.PORT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl <em>Out Control Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl
	 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getOutControlPort()
	 * @generated
	 */
	int OUT_CONTROL_PORT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__NAME = DatapathPackage.PORT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__WIDTH = DatapathPackage.PORT__WIDTH;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__WIRES = DatapathPackage.PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Pad</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT__ASSOCIATED_PAD = DatapathPackage.PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Out Control Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUT_CONTROL_PORT_FEATURE_COUNT = DatapathPackage.PORT_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.port.impl.InputPortImpl <em>Input Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.port.impl.InputPortImpl
	 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInputPort()
	 * @generated
	 */
	int INPUT_PORT = 4;

	/**
	 * The number of structural features of the '<em>Input Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PORT_FEATURE_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.port.InDataPort <em>In Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Data Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InDataPort
	 * @generated
	 */
	EClass getInDataPort();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getWire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Wire</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InDataPort#getWire()
	 * @see #getInDataPort()
	 * @generated
	 */
	EReference getInDataPort_Wire();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad()
	 * @see #getInDataPort()
	 * @generated
	 */
	EReference getInDataPort_AssociatedPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.port.OutDataPort <em>Out Data Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Data Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutDataPort
	 * @generated
	 */
	EClass getOutDataPort();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getWires <em>Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wires</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutDataPort#getWires()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EReference getOutDataPort_Wires();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad()
	 * @see #getOutDataPort()
	 * @generated
	 */
	EReference getOutDataPort_AssociatedPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.port.InControlPort <em>In Control Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>In Control Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InControlPort
	 * @generated
	 */
	EClass getInControlPort();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getWire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Wire</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InControlPort#getWire()
	 * @see #getInControlPort()
	 * @generated
	 */
	EReference getInControlPort_Wire();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad()
	 * @see #getInControlPort()
	 * @generated
	 */
	EReference getInControlPort_AssociatedPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.port.OutControlPort <em>Out Control Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Out Control Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutControlPort
	 * @generated
	 */
	EClass getOutControlPort();

	/**
	 * Returns the meta object for the reference list '{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getWires <em>Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wires</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutControlPort#getWires()
	 * @see #getOutControlPort()
	 * @generated
	 */
	EReference getOutControlPort_Wires();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.OutControlPort#getAssociatedPad()
	 * @see #getOutControlPort()
	 * @generated
	 */
	EReference getOutControlPort_AssociatedPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.port.InputPort <em>Input Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.port.InputPort
	 * @generated
	 */
	EClass getInputPort();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PortFactory getPortFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl <em>In Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl
		 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInDataPort()
		 * @generated
		 */
		EClass IN_DATA_PORT = eINSTANCE.getInDataPort();

		/**
		 * The meta object literal for the '<em><b>Wire</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_DATA_PORT__WIRE = eINSTANCE.getInDataPort_Wire();

		/**
		 * The meta object literal for the '<em><b>Associated Pad</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_DATA_PORT__ASSOCIATED_PAD = eINSTANCE.getInDataPort_AssociatedPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl <em>Out Data Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl
		 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getOutDataPort()
		 * @generated
		 */
		EClass OUT_DATA_PORT = eINSTANCE.getOutDataPort();

		/**
		 * The meta object literal for the '<em><b>Wires</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_DATA_PORT__WIRES = eINSTANCE.getOutDataPort_Wires();

		/**
		 * The meta object literal for the '<em><b>Associated Pad</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_DATA_PORT__ASSOCIATED_PAD = eINSTANCE.getOutDataPort_AssociatedPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl <em>In Control Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl
		 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInControlPort()
		 * @generated
		 */
		EClass IN_CONTROL_PORT = eINSTANCE.getInControlPort();

		/**
		 * The meta object literal for the '<em><b>Wire</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_CONTROL_PORT__WIRE = eINSTANCE.getInControlPort_Wire();

		/**
		 * The meta object literal for the '<em><b>Associated Pad</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IN_CONTROL_PORT__ASSOCIATED_PAD = eINSTANCE.getInControlPort_AssociatedPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl <em>Out Control Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl
		 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getOutControlPort()
		 * @generated
		 */
		EClass OUT_CONTROL_PORT = eINSTANCE.getOutControlPort();

		/**
		 * The meta object literal for the '<em><b>Wires</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_CONTROL_PORT__WIRES = eINSTANCE.getOutControlPort_Wires();

		/**
		 * The meta object literal for the '<em><b>Associated Pad</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUT_CONTROL_PORT__ASSOCIATED_PAD = eINSTANCE.getOutControlPort_AssociatedPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.port.impl.InputPortImpl <em>Input Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.port.impl.InputPortImpl
		 * @see fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl#getInputPort()
		 * @generated
		 */
		EClass INPUT_PORT = eINSTANCE.getInputPort();

	}

} //PortPackage
