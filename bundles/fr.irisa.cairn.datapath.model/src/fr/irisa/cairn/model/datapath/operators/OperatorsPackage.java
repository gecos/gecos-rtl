/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.DatapathPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsFactory
 * @model kind="package"
 * @generated
 */
public interface OperatorsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operators";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/operators";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operators";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorsPackage eINSTANCE = fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock <em>Single Input Data Flow Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleInputDataFlowBlock()
	 * @generated
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK__NAME = DatapathPackage.DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK__PARENT = DatapathPackage.DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK__COMBINATIONAL = DatapathPackage.DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK__IN = DatapathPackage.DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK__OUT = DatapathPackage.DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Single Input Data Flow Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_INPUT_DATA_FLOW_BLOCK_FEATURE_COUNT = DatapathPackage.DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock <em>Single Output Data Flow Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleOutputDataFlowBlock()
	 * @generated
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME = DatapathPackage.DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT = DatapathPackage.DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL = DatapathPackage.DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN = DatapathPackage.DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT = DatapathPackage.DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Single Output Data Flow Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT = DatapathPackage.DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock <em>Multi Output Data Flow Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMultiOutputDataFlowBlock()
	 * @generated
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl <em>Unary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 5;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl <em>Binary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBinaryOperator()
	 * @generated
	 */
	int BINARY_OPERATOR = 6;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.PipelinedBinaryOperatorImpl <em>Pipelined Binary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.PipelinedBinaryOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getPipelinedBinaryOperator()
	 * @generated
	 */
	int PIPELINED_BINARY_OPERATOR = 7;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.MultiCycleBinaryOperatorImpl <em>Multi Cycle Binary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.MultiCycleBinaryOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMultiCycleBinaryOperator()
	 * @generated
	 */
	int MULTI_CYCLE_BINARY_OPERATOR = 8;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl <em>Ternary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getTernaryOperator()
	 * @generated
	 */
	int TERNARY_OPERATOR = 9;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl <em>Constant Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getConstantValue()
	 * @generated
	 */
	int CONSTANT_VALUE = 10;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.DataFlowMuxImpl <em>Data Flow Mux</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.DataFlowMuxImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getDataFlowMux()
	 * @generated
	 */
	int DATA_FLOW_MUX = 11;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl <em>Control Flow Mux</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getControlFlowMux()
	 * @generated
	 */
	int CONTROL_FLOW_MUX = 12;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ReductionOperatorImpl <em>Reduction Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.ReductionOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getReductionOperator()
	 * @generated
	 */
	int REDUCTION_OPERATOR = 13;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl <em>Bit Select</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBitSelect()
	 * @generated
	 */
	int BIT_SELECT = 15;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.CompareImpl <em>Compare</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.CompareImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCompare()
	 * @generated
	 */
	int COMPARE = 16;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.MergeImpl <em>Merge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.MergeImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMerge()
	 * @generated
	 */
	int MERGE = 17;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.QuantizeImpl <em>Quantize</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.QuantizeImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getQuantize()
	 * @generated
	 */
	int QUANTIZE = 18;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ExpandUnsignedImpl <em>Expand Unsigned</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.ExpandUnsignedImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getExpandUnsigned()
	 * @generated
	 */
	int EXPAND_UNSIGNED = 19;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ExpandSignedImpl <em>Expand Signed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.ExpandSignedImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getExpandSigned()
	 * @generated
	 */
	int EXPAND_SIGNED = 20;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl <em>Ctrl2 Data Buffer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCtrl2DataBuffer()
	 * @generated
	 */
	int CTRL2_DATA_BUFFER = 21;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.Data2CtrlBufferImpl <em>Data2 Ctrl Buffer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.Data2CtrlBufferImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getData2CtrlBuffer()
	 * @generated
	 */
	int DATA2_CTRL_BUFFER = 22;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.SingleFlagBlock <em>Single Flag Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.SingleFlagBlock
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleFlagBlock()
	 * @generated
	 */
	int SINGLE_FLAG_BLOCK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_FLAG_BLOCK__NAME = DatapathPackage.FLAG_BEARER_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_FLAG_BLOCK__PARENT = DatapathPackage.FLAG_BEARER_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_FLAG_BLOCK__COMBINATIONAL = DatapathPackage.FLAG_BEARER_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_FLAG_BLOCK__FLAGS = DatapathPackage.FLAG_BEARER_BLOCK__FLAGS;

	/**
	 * The number of structural features of the '<em>Single Flag Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_FLAG_BLOCK_FEATURE_COUNT = DatapathPackage.FLAG_BEARER_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock <em>Single Control Port Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleControlPortBlock()
	 * @generated
	 */
	int SINGLE_CONTROL_PORT_BLOCK = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONTROL_PORT_BLOCK__NAME = DatapathPackage.ACTIVABLE_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONTROL_PORT_BLOCK__PARENT = DatapathPackage.ACTIVABLE_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONTROL_PORT_BLOCK__COMBINATIONAL = DatapathPackage.ACTIVABLE_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONTROL_PORT_BLOCK__ACTIVATE = DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;

	/**
	 * The number of structural features of the '<em>Single Control Port Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_CONTROL_PORT_BLOCK_FEATURE_COUNT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK__NAME = DatapathPackage.DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK__PARENT = DatapathPackage.DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL = DatapathPackage.DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK__IN = DatapathPackage.DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK__OUT = DatapathPackage.DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Multi Output Data Flow Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT = DatapathPackage.DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Binary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__LATENCY = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pipelined Binary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BINARY_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__LATENCY = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__THROUGHPUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Multi Cycle Binary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BINARY_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ternary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERNARY_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE__VALUE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_VALUE_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Data Flow Mux</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_MUX_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX__ACTIVATE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Control Flow Mux</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_MUX_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reduction Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REDUCTION_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.impl.PipelinedReductionOperatorImpl <em>Pipelined Reduction Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.impl.PipelinedReductionOperatorImpl
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getPipelinedReductionOperator()
	 * @generated
	 */
	int PIPELINED_REDUCTION_OPERATOR = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__LATENCY = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pipelined Reduction Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_REDUCTION_OPERATOR_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__LOWER_BOUND = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT__UPPER_BOUND = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Bit Select</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_SELECT_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Opcode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE__OPCODE = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compare</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARE_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Merge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MERGE_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__NAME = BIT_SELECT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__PARENT = BIT_SELECT__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__COMBINATIONAL = BIT_SELECT__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__IN = BIT_SELECT__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__OUT = BIT_SELECT__OUT;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__LOWER_BOUND = BIT_SELECT__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE__UPPER_BOUND = BIT_SELECT__UPPER_BOUND;

	/**
	 * The number of structural features of the '<em>Quantize</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIZE_FEATURE_COUNT = BIT_SELECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Outputwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED__OUTPUTWIDTH = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expand Unsigned</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_UNSIGNED_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__NAME = SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__PARENT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__COMBINATIONAL = SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__IN = SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__OUT = SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Outputwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED__OUTPUTWIDTH = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expand Signed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPAND_SIGNED_FEATURE_COUNT = SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__NAME = DatapathPackage.ACTIVABLE_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__PARENT = DatapathPackage.ACTIVABLE_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__COMBINATIONAL = DatapathPackage.ACTIVABLE_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__ACTIVATE = DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__IN = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER__OUT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ctrl2 Data Buffer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CTRL2_DATA_BUFFER_FEATURE_COUNT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__NAME = DatapathPackage.FLAG_BEARER_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__PARENT = DatapathPackage.FLAG_BEARER_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__COMBINATIONAL = DatapathPackage.FLAG_BEARER_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__FLAGS = DatapathPackage.FLAG_BEARER_BLOCK__FLAGS;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__IN = DatapathPackage.FLAG_BEARER_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER__OUT = DatapathPackage.FLAG_BEARER_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data2 Ctrl Buffer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA2_CTRL_BUFFER_FEATURE_COUNT = DatapathPackage.FLAG_BEARER_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.CombinationnalOperator <em>Combinationnal Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.CombinationnalOperator
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCombinationnalOperator()
	 * @generated
	 */
	int COMBINATIONNAL_OPERATOR = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR__NAME = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR__PARENT = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR__COMBINATIONAL = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR__IN = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR__OUT = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Combinationnal Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONNAL_OPERATOR_FEATURE_COUNT = DatapathPackage.COMBINATIONAL_BLOCK_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.UnaryOpcode <em>Unary Opcode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOpcode
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getUnaryOpcode()
	 * @generated
	 */
	int UNARY_OPCODE = 24;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.BinaryOpcode <em>Binary Opcode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.BinaryOpcode
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBinaryOpcode()
	 * @generated
	 */
	int BINARY_OPCODE = 25;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.TernaryOpcode <em>Ternary Opcode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.TernaryOpcode
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getTernaryOpcode()
	 * @generated
	 */
	int TERNARY_OPCODE = 26;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.ReductionOpcode <em>Reduction Opcode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOpcode
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getReductionOpcode()
	 * @generated
	 */
	int REDUCTION_OPCODE = 27;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.operators.CompareOpcode <em>Compare Opcode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCompareOpcode()
	 * @generated
	 */
	int COMPARE_OPCODE = 28;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock <em>Single Input Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Input Data Flow Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock
	 * @generated
	 */
	EClass getSingleInputDataFlowBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock <em>Single Output Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Output Data Flow Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock
	 * @generated
	 */
	EClass getSingleOutputDataFlowBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock <em>Multi Output Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Output Data Flow Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock
	 * @generated
	 */
	EClass getMultiOutputDataFlowBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOperator
	 * @generated
	 */
	EClass getUnaryOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.UnaryOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOperator#getOpcode()
	 * @see #getUnaryOperator()
	 * @generated
	 */
	EAttribute getUnaryOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BinaryOperator
	 * @generated
	 */
	EClass getBinaryOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.BinaryOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BinaryOperator#getOpcode()
	 * @see #getBinaryOperator()
	 * @generated
	 */
	EAttribute getBinaryOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator <em>Pipelined Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pipelined Binary Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator
	 * @generated
	 */
	EClass getPipelinedBinaryOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator#getOpcode()
	 * @see #getPipelinedBinaryOperator()
	 * @generated
	 */
	EAttribute getPipelinedBinaryOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator <em>Multi Cycle Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Cycle Binary Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator
	 * @generated
	 */
	EClass getMultiCycleBinaryOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator#getOpcode()
	 * @see #getMultiCycleBinaryOperator()
	 * @generated
	 */
	EAttribute getMultiCycleBinaryOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.TernaryOperator <em>Ternary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ternary Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.TernaryOperator
	 * @generated
	 */
	EClass getTernaryOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.TernaryOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.TernaryOperator#getOpcode()
	 * @see #getTernaryOperator()
	 * @generated
	 */
	EAttribute getTernaryOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.ConstantValue <em>Constant Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Value</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ConstantValue
	 * @generated
	 */
	EClass getConstantValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.ConstantValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ConstantValue#getValue()
	 * @see #getConstantValue()
	 * @generated
	 */
	EAttribute getConstantValue_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.DataFlowMux <em>Data Flow Mux</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Flow Mux</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.DataFlowMux
	 * @generated
	 */
	EClass getDataFlowMux();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.ControlFlowMux <em>Control Flow Mux</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Flow Mux</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ControlFlowMux
	 * @generated
	 */
	EClass getControlFlowMux();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.ReductionOperator <em>Reduction Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reduction Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOperator
	 * @generated
	 */
	EClass getReductionOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.ReductionOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOperator#getOpcode()
	 * @see #getReductionOperator()
	 * @generated
	 */
	EAttribute getReductionOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator <em>Pipelined Reduction Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pipelined Reduction Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator
	 * @generated
	 */
	EClass getPipelinedReductionOperator();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator#getOpcode()
	 * @see #getPipelinedReductionOperator()
	 * @generated
	 */
	EAttribute getPipelinedReductionOperator_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.BitSelect <em>Bit Select</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bit Select</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BitSelect
	 * @generated
	 */
	EClass getBitSelect();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.BitSelect#getLowerBound <em>Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Bound</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BitSelect#getLowerBound()
	 * @see #getBitSelect()
	 * @generated
	 */
	EAttribute getBitSelect_LowerBound();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.BitSelect#getUpperBound <em>Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BitSelect#getUpperBound()
	 * @see #getBitSelect()
	 * @generated
	 */
	EAttribute getBitSelect_UpperBound();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.Compare <em>Compare</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compare</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Compare
	 * @generated
	 */
	EClass getCompare();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.Compare#getOpcode <em>Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Compare#getOpcode()
	 * @see #getCompare()
	 * @generated
	 */
	EAttribute getCompare_Opcode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.Merge <em>Merge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Merge</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Merge
	 * @generated
	 */
	EClass getMerge();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.Quantize <em>Quantize</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantize</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Quantize
	 * @generated
	 */
	EClass getQuantize();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.ExpandUnsigned <em>Expand Unsigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expand Unsigned</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandUnsigned
	 * @generated
	 */
	EClass getExpandUnsigned();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.ExpandUnsigned#getOutputwidth <em>Outputwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outputwidth</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandUnsigned#getOutputwidth()
	 * @see #getExpandUnsigned()
	 * @generated
	 */
	EAttribute getExpandUnsigned_Outputwidth();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.ExpandSigned <em>Expand Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expand Signed</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandSigned
	 * @generated
	 */
	EClass getExpandSigned();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.operators.ExpandSigned#getOutputwidth <em>Outputwidth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outputwidth</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandSigned#getOutputwidth()
	 * @see #getExpandSigned()
	 * @generated
	 */
	EAttribute getExpandSigned_Outputwidth();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer <em>Ctrl2 Data Buffer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ctrl2 Data Buffer</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer
	 * @generated
	 */
	EClass getCtrl2DataBuffer();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer <em>Data2 Ctrl Buffer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data2 Ctrl Buffer</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer
	 * @generated
	 */
	EClass getData2CtrlBuffer();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.CombinationnalOperator <em>Combinationnal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinationnal Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.CombinationnalOperator
	 * @generated
	 */
	EClass getCombinationnalOperator();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.SingleFlagBlock <em>Single Flag Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Flag Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleFlagBlock
	 * @generated
	 */
	EClass getSingleFlagBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock <em>Single Control Port Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Control Port Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock
	 * @generated
	 */
	EClass getSingleControlPortBlock();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.datapath.operators.UnaryOpcode <em>Unary Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOpcode
	 * @generated
	 */
	EEnum getUnaryOpcode();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.datapath.operators.BinaryOpcode <em>Binary Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binary Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.BinaryOpcode
	 * @generated
	 */
	EEnum getBinaryOpcode();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.datapath.operators.TernaryOpcode <em>Ternary Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ternary Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.TernaryOpcode
	 * @generated
	 */
	EEnum getTernaryOpcode();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.datapath.operators.ReductionOpcode <em>Reduction Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reduction Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOpcode
	 * @generated
	 */
	EEnum getReductionOpcode();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.datapath.operators.CompareOpcode <em>Compare Opcode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Compare Opcode</em>'.
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @generated
	 */
	EEnum getCompareOpcode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperatorsFactory getOperatorsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock <em>Single Input Data Flow Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleInputDataFlowBlock()
		 * @generated
		 */
		EClass SINGLE_INPUT_DATA_FLOW_BLOCK = eINSTANCE.getSingleInputDataFlowBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock <em>Single Output Data Flow Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleOutputDataFlowBlock()
		 * @generated
		 */
		EClass SINGLE_OUTPUT_DATA_FLOW_BLOCK = eINSTANCE.getSingleOutputDataFlowBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock <em>Multi Output Data Flow Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMultiOutputDataFlowBlock()
		 * @generated
		 */
		EClass MULTI_OUTPUT_DATA_FLOW_BLOCK = eINSTANCE.getMultiOutputDataFlowBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl <em>Unary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getUnaryOperator()
		 * @generated
		 */
		EClass UNARY_OPERATOR = eINSTANCE.getUnaryOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_OPERATOR__OPCODE = eINSTANCE.getUnaryOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl <em>Binary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.BinaryOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBinaryOperator()
		 * @generated
		 */
		EClass BINARY_OPERATOR = eINSTANCE.getBinaryOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_OPERATOR__OPCODE = eINSTANCE.getBinaryOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.PipelinedBinaryOperatorImpl <em>Pipelined Binary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.PipelinedBinaryOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getPipelinedBinaryOperator()
		 * @generated
		 */
		EClass PIPELINED_BINARY_OPERATOR = eINSTANCE.getPipelinedBinaryOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIPELINED_BINARY_OPERATOR__OPCODE = eINSTANCE.getPipelinedBinaryOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.MultiCycleBinaryOperatorImpl <em>Multi Cycle Binary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.MultiCycleBinaryOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMultiCycleBinaryOperator()
		 * @generated
		 */
		EClass MULTI_CYCLE_BINARY_OPERATOR = eINSTANCE.getMultiCycleBinaryOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_CYCLE_BINARY_OPERATOR__OPCODE = eINSTANCE.getMultiCycleBinaryOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl <em>Ternary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.TernaryOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getTernaryOperator()
		 * @generated
		 */
		EClass TERNARY_OPERATOR = eINSTANCE.getTernaryOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TERNARY_OPERATOR__OPCODE = eINSTANCE.getTernaryOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl <em>Constant Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.ConstantValueImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getConstantValue()
		 * @generated
		 */
		EClass CONSTANT_VALUE = eINSTANCE.getConstantValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_VALUE__VALUE = eINSTANCE.getConstantValue_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.DataFlowMuxImpl <em>Data Flow Mux</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.DataFlowMuxImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getDataFlowMux()
		 * @generated
		 */
		EClass DATA_FLOW_MUX = eINSTANCE.getDataFlowMux();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl <em>Control Flow Mux</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getControlFlowMux()
		 * @generated
		 */
		EClass CONTROL_FLOW_MUX = eINSTANCE.getControlFlowMux();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ReductionOperatorImpl <em>Reduction Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.ReductionOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getReductionOperator()
		 * @generated
		 */
		EClass REDUCTION_OPERATOR = eINSTANCE.getReductionOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REDUCTION_OPERATOR__OPCODE = eINSTANCE.getReductionOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.PipelinedReductionOperatorImpl <em>Pipelined Reduction Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.PipelinedReductionOperatorImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getPipelinedReductionOperator()
		 * @generated
		 */
		EClass PIPELINED_REDUCTION_OPERATOR = eINSTANCE.getPipelinedReductionOperator();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIPELINED_REDUCTION_OPERATOR__OPCODE = eINSTANCE.getPipelinedReductionOperator_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl <em>Bit Select</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.BitSelectImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBitSelect()
		 * @generated
		 */
		EClass BIT_SELECT = eINSTANCE.getBitSelect();

		/**
		 * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BIT_SELECT__LOWER_BOUND = eINSTANCE.getBitSelect_LowerBound();

		/**
		 * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BIT_SELECT__UPPER_BOUND = eINSTANCE.getBitSelect_UpperBound();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.CompareImpl <em>Compare</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.CompareImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCompare()
		 * @generated
		 */
		EClass COMPARE = eINSTANCE.getCompare();

		/**
		 * The meta object literal for the '<em><b>Opcode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPARE__OPCODE = eINSTANCE.getCompare_Opcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.MergeImpl <em>Merge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.MergeImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getMerge()
		 * @generated
		 */
		EClass MERGE = eINSTANCE.getMerge();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.QuantizeImpl <em>Quantize</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.QuantizeImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getQuantize()
		 * @generated
		 */
		EClass QUANTIZE = eINSTANCE.getQuantize();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ExpandUnsignedImpl <em>Expand Unsigned</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.ExpandUnsignedImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getExpandUnsigned()
		 * @generated
		 */
		EClass EXPAND_UNSIGNED = eINSTANCE.getExpandUnsigned();

		/**
		 * The meta object literal for the '<em><b>Outputwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPAND_UNSIGNED__OUTPUTWIDTH = eINSTANCE.getExpandUnsigned_Outputwidth();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.ExpandSignedImpl <em>Expand Signed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.ExpandSignedImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getExpandSigned()
		 * @generated
		 */
		EClass EXPAND_SIGNED = eINSTANCE.getExpandSigned();

		/**
		 * The meta object literal for the '<em><b>Outputwidth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXPAND_SIGNED__OUTPUTWIDTH = eINSTANCE.getExpandSigned_Outputwidth();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl <em>Ctrl2 Data Buffer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCtrl2DataBuffer()
		 * @generated
		 */
		EClass CTRL2_DATA_BUFFER = eINSTANCE.getCtrl2DataBuffer();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.impl.Data2CtrlBufferImpl <em>Data2 Ctrl Buffer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.impl.Data2CtrlBufferImpl
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getData2CtrlBuffer()
		 * @generated
		 */
		EClass DATA2_CTRL_BUFFER = eINSTANCE.getData2CtrlBuffer();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.CombinationnalOperator <em>Combinationnal Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.CombinationnalOperator
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCombinationnalOperator()
		 * @generated
		 */
		EClass COMBINATIONNAL_OPERATOR = eINSTANCE.getCombinationnalOperator();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.SingleFlagBlock <em>Single Flag Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.SingleFlagBlock
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleFlagBlock()
		 * @generated
		 */
		EClass SINGLE_FLAG_BLOCK = eINSTANCE.getSingleFlagBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock <em>Single Control Port Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getSingleControlPortBlock()
		 * @generated
		 */
		EClass SINGLE_CONTROL_PORT_BLOCK = eINSTANCE.getSingleControlPortBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.UnaryOpcode <em>Unary Opcode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.UnaryOpcode
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getUnaryOpcode()
		 * @generated
		 */
		EEnum UNARY_OPCODE = eINSTANCE.getUnaryOpcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.BinaryOpcode <em>Binary Opcode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.BinaryOpcode
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getBinaryOpcode()
		 * @generated
		 */
		EEnum BINARY_OPCODE = eINSTANCE.getBinaryOpcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.TernaryOpcode <em>Ternary Opcode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.TernaryOpcode
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getTernaryOpcode()
		 * @generated
		 */
		EEnum TERNARY_OPCODE = eINSTANCE.getTernaryOpcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.ReductionOpcode <em>Reduction Opcode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.ReductionOpcode
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getReductionOpcode()
		 * @generated
		 */
		EEnum REDUCTION_OPCODE = eINSTANCE.getReductionOpcode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.operators.CompareOpcode <em>Compare Opcode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
		 * @see fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl#getCompareOpcode()
		 * @generated
		 */
		EEnum COMPARE_OPCODE = eINSTANCE.getCompareOpcode();

	}

} //OperatorsPackage
