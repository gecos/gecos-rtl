/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.impl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.AbstractDatapath;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Datapath</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#getActivate <em>Activate</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl#getFlags <em>Flags</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AbstractDatapathImpl extends NamedElementImpl implements AbstractDatapath {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivate()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> activate;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected EList<OutControlPort> flags;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractDatapathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatapathPackage.Literals.ABSTRACT_DATAPATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != DatapathPackage.ABSTRACT_DATAPATH__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, DatapathPackage.ABSTRACT_DATAPATH__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != DatapathPackage.ABSTRACT_DATAPATH__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatapathPackage.ABSTRACT_DATAPATH__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCombinational() {
		// TODO: implement this method to return the 'Combinational' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getActivate() {
		if (activate == null) {
			activate = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE);
		}
		return activate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, DatapathPackage.ABSTRACT_DATAPATH__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, DatapathPackage.ABSTRACT_DATAPATH__OUT);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutControlPort> getFlags() {
		if (flags == null) {
			flags = new EObjectContainmentEList<OutControlPort>(OutControlPort.class, this, DatapathPackage.ABSTRACT_DATAPATH__FLAGS);
		}
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getFlag(int pos) {
		return getFlags().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControlPort(int pos) {
		return getActivate().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				return basicSetParent(null, msgs);
			case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE:
				return ((InternalEList<?>)getActivate()).basicRemove(otherEnd, msgs);
			case DatapathPackage.ABSTRACT_DATAPATH__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case DatapathPackage.ABSTRACT_DATAPATH__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case DatapathPackage.ABSTRACT_DATAPATH__FLAGS:
				return ((InternalEList<?>)getFlags()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				return getParent();
			case DatapathPackage.ABSTRACT_DATAPATH__COMBINATIONAL:
				return isCombinational();
			case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE:
				return getActivate();
			case DatapathPackage.ABSTRACT_DATAPATH__IN:
				return getIn();
			case DatapathPackage.ABSTRACT_DATAPATH__OUT:
				return getOut();
			case DatapathPackage.ABSTRACT_DATAPATH__FLAGS:
				return getFlags();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				setParent((Datapath)newValue);
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE:
				getActivate().clear();
				getActivate().addAll((Collection<? extends InControlPort>)newValue);
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__FLAGS:
				getFlags().clear();
				getFlags().addAll((Collection<? extends OutControlPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				setParent((Datapath)null);
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE:
				getActivate().clear();
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__IN:
				unsetIn();
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__OUT:
				getOut().clear();
				return;
			case DatapathPackage.ABSTRACT_DATAPATH__FLAGS:
				getFlags().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatapathPackage.ABSTRACT_DATAPATH__PARENT:
				return getParent() != null;
			case DatapathPackage.ABSTRACT_DATAPATH__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE:
				return activate != null && !activate.isEmpty();
			case DatapathPackage.ABSTRACT_DATAPATH__IN:
				return isSetIn();
			case DatapathPackage.ABSTRACT_DATAPATH__OUT:
				return out != null && !out.isEmpty();
			case DatapathPackage.ABSTRACT_DATAPATH__FLAGS:
				return flags != null && !flags.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.ABSTRACT_DATAPATH__PARENT: return DatapathPackage.ABSTRACT_BLOCK__PARENT;
				case DatapathPackage.ABSTRACT_DATAPATH__COMBINATIONAL: return DatapathPackage.ABSTRACT_BLOCK__COMBINATIONAL;
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE: return DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == DataFlowBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.ABSTRACT_DATAPATH__IN: return DatapathPackage.DATA_FLOW_BLOCK__IN;
				case DatapathPackage.ABSTRACT_DATAPATH__OUT: return DatapathPackage.DATA_FLOW_BLOCK__OUT;
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.ABSTRACT_DATAPATH__FLAGS: return DatapathPackage.FLAG_BEARER_BLOCK__FLAGS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ABSTRACT_BLOCK__PARENT: return DatapathPackage.ABSTRACT_DATAPATH__PARENT;
				case DatapathPackage.ABSTRACT_BLOCK__COMBINATIONAL: return DatapathPackage.ABSTRACT_DATAPATH__COMBINATIONAL;
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE: return DatapathPackage.ABSTRACT_DATAPATH__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == DataFlowBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.DATA_FLOW_BLOCK__IN: return DatapathPackage.ABSTRACT_DATAPATH__IN;
				case DatapathPackage.DATA_FLOW_BLOCK__OUT: return DatapathPackage.ABSTRACT_DATAPATH__OUT;
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.FLAG_BEARER_BLOCK__FLAGS: return DatapathPackage.ABSTRACT_DATAPATH__FLAGS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AbstractDatapathImpl
