/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.impl;

import fr.irisa.cairn.model.datapath.operators.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator;
import fr.irisa.cairn.model.datapath.operators.OperatorsFactory;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorsFactoryImpl extends EFactoryImpl implements OperatorsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OperatorsFactory init() {
		try {
			OperatorsFactory theOperatorsFactory = (OperatorsFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.irisa.fr/cairn/datapath/operators"); 
			if (theOperatorsFactory != null) {
				return theOperatorsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OperatorsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OperatorsPackage.UNARY_OPERATOR: return createUnaryOperator();
			case OperatorsPackage.BINARY_OPERATOR: return createBinaryOperator();
			case OperatorsPackage.PIPELINED_BINARY_OPERATOR: return createPipelinedBinaryOperator();
			case OperatorsPackage.MULTI_CYCLE_BINARY_OPERATOR: return createMultiCycleBinaryOperator();
			case OperatorsPackage.TERNARY_OPERATOR: return createTernaryOperator();
			case OperatorsPackage.CONSTANT_VALUE: return createConstantValue();
			case OperatorsPackage.DATA_FLOW_MUX: return createDataFlowMux();
			case OperatorsPackage.CONTROL_FLOW_MUX: return createControlFlowMux();
			case OperatorsPackage.REDUCTION_OPERATOR: return createReductionOperator();
			case OperatorsPackage.PIPELINED_REDUCTION_OPERATOR: return createPipelinedReductionOperator();
			case OperatorsPackage.BIT_SELECT: return createBitSelect();
			case OperatorsPackage.COMPARE: return createCompare();
			case OperatorsPackage.MERGE: return createMerge();
			case OperatorsPackage.QUANTIZE: return createQuantize();
			case OperatorsPackage.EXPAND_UNSIGNED: return createExpandUnsigned();
			case OperatorsPackage.EXPAND_SIGNED: return createExpandSigned();
			case OperatorsPackage.CTRL2_DATA_BUFFER: return createCtrl2DataBuffer();
			case OperatorsPackage.DATA2_CTRL_BUFFER: return createData2CtrlBuffer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case OperatorsPackage.UNARY_OPCODE:
				return createUnaryOpcodeFromString(eDataType, initialValue);
			case OperatorsPackage.BINARY_OPCODE:
				return createBinaryOpcodeFromString(eDataType, initialValue);
			case OperatorsPackage.TERNARY_OPCODE:
				return createTernaryOpcodeFromString(eDataType, initialValue);
			case OperatorsPackage.REDUCTION_OPCODE:
				return createReductionOpcodeFromString(eDataType, initialValue);
			case OperatorsPackage.COMPARE_OPCODE:
				return createCompareOpcodeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case OperatorsPackage.UNARY_OPCODE:
				return convertUnaryOpcodeToString(eDataType, instanceValue);
			case OperatorsPackage.BINARY_OPCODE:
				return convertBinaryOpcodeToString(eDataType, instanceValue);
			case OperatorsPackage.TERNARY_OPCODE:
				return convertTernaryOpcodeToString(eDataType, instanceValue);
			case OperatorsPackage.REDUCTION_OPCODE:
				return convertReductionOpcodeToString(eDataType, instanceValue);
			case OperatorsPackage.COMPARE_OPCODE:
				return convertCompareOpcodeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOperator createUnaryOperator() {
		UnaryOperatorImpl unaryOperator = new UnaryOperatorImpl();
		return unaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOperator createBinaryOperator() {
		BinaryOperatorImpl binaryOperator = new BinaryOperatorImpl();
		return binaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PipelinedBinaryOperator createPipelinedBinaryOperator() {
		PipelinedBinaryOperatorImpl pipelinedBinaryOperator = new PipelinedBinaryOperatorImpl();
		return pipelinedBinaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiCycleBinaryOperator createMultiCycleBinaryOperator() {
		MultiCycleBinaryOperatorImpl multiCycleBinaryOperator = new MultiCycleBinaryOperatorImpl();
		return multiCycleBinaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TernaryOperator createTernaryOperator() {
		TernaryOperatorImpl ternaryOperator = new TernaryOperatorImpl();
		return ternaryOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantValue createConstantValue() {
		ConstantValueImpl constantValue = new ConstantValueImpl();
		return constantValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowMux createDataFlowMux() {
		DataFlowMuxImpl dataFlowMux = new DataFlowMuxImpl();
		return dataFlowMux;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowMux createControlFlowMux() {
		ControlFlowMuxImpl controlFlowMux = new ControlFlowMuxImpl();
		return controlFlowMux;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReductionOperator createReductionOperator() {
		ReductionOperatorImpl reductionOperator = new ReductionOperatorImpl();
		return reductionOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PipelinedReductionOperator createPipelinedReductionOperator() {
		PipelinedReductionOperatorImpl pipelinedReductionOperator = new PipelinedReductionOperatorImpl();
		return pipelinedReductionOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitSelect createBitSelect() {
		BitSelectImpl bitSelect = new BitSelectImpl();
		return bitSelect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Compare createCompare() {
		CompareImpl compare = new CompareImpl();
		return compare;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Merge createMerge() {
		MergeImpl merge = new MergeImpl();
		return merge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Quantize createQuantize() {
		QuantizeImpl quantize = new QuantizeImpl();
		return quantize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpandUnsigned createExpandUnsigned() {
		ExpandUnsignedImpl expandUnsigned = new ExpandUnsignedImpl();
		return expandUnsigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpandSigned createExpandSigned() {
		ExpandSignedImpl expandSigned = new ExpandSignedImpl();
		return expandSigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ctrl2DataBuffer createCtrl2DataBuffer() {
		Ctrl2DataBufferImpl ctrl2DataBuffer = new Ctrl2DataBufferImpl();
		return ctrl2DataBuffer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data2CtrlBuffer createData2CtrlBuffer() {
		Data2CtrlBufferImpl data2CtrlBuffer = new Data2CtrlBufferImpl();
		return data2CtrlBuffer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOpcode createUnaryOpcodeFromString(EDataType eDataType, String initialValue) {
		UnaryOpcode result = UnaryOpcode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertUnaryOpcodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryOpcode createBinaryOpcodeFromString(EDataType eDataType, String initialValue) {
		BinaryOpcode result = BinaryOpcode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBinaryOpcodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TernaryOpcode createTernaryOpcodeFromString(EDataType eDataType, String initialValue) {
		TernaryOpcode result = TernaryOpcode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTernaryOpcodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReductionOpcode createReductionOpcodeFromString(EDataType eDataType, String initialValue) {
		ReductionOpcode result = ReductionOpcode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertReductionOpcodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompareOpcode createCompareOpcodeFromString(EDataType eDataType, String initialValue) {
		CompareOpcode result = CompareOpcode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCompareOpcodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsPackage getOperatorsPackage() {
		return (OperatorsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OperatorsPackage getPackage() {
		return OperatorsPackage.eINSTANCE;
	}

} //OperatorsFactoryImpl
