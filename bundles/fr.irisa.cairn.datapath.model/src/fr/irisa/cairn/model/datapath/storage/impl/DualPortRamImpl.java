/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.impl;

import org.eclipse.emf.ecore.EClass;

import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dual Port Ram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class DualPortRamImpl extends MultiPortRamImpl implements DualPortRam {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DualPortRamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.DUAL_PORT_RAM;
	}

} //DualPortRamImpl
