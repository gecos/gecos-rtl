/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage
 * @generated
 */
public interface PadsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PadsFactory eINSTANCE = fr.irisa.cairn.model.datapath.pads.impl.PadsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Data Input Pad</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Input Pad</em>'.
	 * @generated
	 */
	DataInputPad createDataInputPad();

	/**
	 * Returns a new object of class '<em>Data Output Pad</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Output Pad</em>'.
	 * @generated
	 */
	DataOutputPad createDataOutputPad();

	/**
	 * Returns a new object of class '<em>Control Pad</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Pad</em>'.
	 * @generated
	 */
	ControlPad createControlPad();

	/**
	 * Returns a new object of class '<em>Status Pad</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Status Pad</em>'.
	 * @generated
	 */
	StatusPad createStatusPad();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PadsPackage getPadsPackage();

} //PadsFactory
