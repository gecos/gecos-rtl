/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.ActivableBlock;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;

import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Control Port Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getSingleControlPortBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SingleControlPortBlock extends ActivableBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getActivate().get(0);'"
	 * @generated
	 */
	InControlPort getControl();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	ControlFlowWire connect(SingleFlagBlock source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	ControlFlowWire connect(OutControlPort source);

} // SingleControlPortBlock
