/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.FlagBearerBlock;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Flag Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getSingleFlagBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SingleFlagBlock extends FlagBearerBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getFlags().get(0);'"
	 * @generated
	 */
	OutControlPort getFlag();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	ControlFlowWire connect(SingleControlPortBlock sink);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	ControlFlowWire connect(InControlPort sink);

} // SingleFlagBlock
