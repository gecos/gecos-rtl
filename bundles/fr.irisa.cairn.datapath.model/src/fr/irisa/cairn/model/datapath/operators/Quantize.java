/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantize</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getQuantize()
 * @model
 * @generated
 */
public interface Quantize extends BitSelect {
} // Quantize
