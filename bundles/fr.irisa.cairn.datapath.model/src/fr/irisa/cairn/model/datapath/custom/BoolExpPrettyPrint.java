package fr.irisa.cairn.model.datapath.custom;

import java.util.Iterator;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.util.FsmSwitch;

public class BoolExpPrettyPrint extends FsmSwitch<String>{
	

	public static final int VHDL = 1;
	public static final int C= 2;
	public static final int JAVA= 3;

	private String NEQ_KW = "!=";
	private String EQU_KW = "==";
	private String NOT_KW = "!";
	private String OR_KW = "||";
	private String AND_KW = "&&";
	private String TRUE_KW = "1"; 
	private String FALSE_KW = "0";
	private int language;
	
	private static BoolExpPrettyPrint singleton = new BoolExpPrettyPrint(C);
	
	public BoolExpPrettyPrint(int language) {
		this.language=language;
		switch (language) {
		case VHDL :
			NEQ_KW = "/=";
			EQU_KW = "=";
			NOT_KW = "not";
			OR_KW = "or";
			AND_KW = "and";
			TRUE_KW = "true";
			FALSE_KW = "false";
		case C :
		case JAVA:
			break; 
		default:
			throw new UnsupportedOperationException("Usnupported Language mode");
		}
		
	}

	public void setLanguage(int language) {
		this.language=language;
		switch (language) {
		case VHDL :
			NOT_KW = "not";
			OR_KW = "or";
			AND_KW = "and";
			
		case C :
		case JAVA:
			break; 
		default:
			throw new UnsupportedOperationException("Unsupported Language mode");
		}
	}

	@Override
	public String caseIndexedBooleanFlagTerm(IndexedBooleanFlagTerm object) {
		return caseBooleanFlagTerm(object)+"["+object.getOffset()+"]";
	}

	@Override
	public String caseBooleanConstant(BooleanConstant object) {
		if (object.isValue()) {
			return TRUE_KW;
		} else {
			return FALSE_KW;
		}
	}



	@Override
	public String caseNegateExpression(NegateExpression object) {
		return NOT_KW+"("+doSwitch(object.getTerm())+")";
	}


	public String prettyPrint(AbstractBooleanExpression bexp) {
		return doSwitch(bexp);
	}
	
	public String asList(EList list, String sep) {
		StringBuffer buff = new StringBuffer();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			EObject item = (EObject) iterator.next();
			buff.append(doSwitch(item));
			if (iterator.hasNext()) {
				buff.append(sep);
			}
		}
		return buff.toString();
	}

		@Override
	public String caseAndExpression(AndExpression object) {
		if (object.getTerms().size()==1) 
			return asList(object.getTerms(),  AND_KW );
		else
			return '('+asList(object.getTerms(), ")" + AND_KW + "(")+')';
	}


	@Override
	public String caseOrExpression(OrExpression object) {
		return '('+asList(object.getTerms(), ")" + OR_KW  + "(")+')';
	}

	public static String print(AbstractBooleanExpression exp) {
		if (exp==null) return "null";
		return singleton.prettyPrint(exp);
	}

	public static String print(AbstractBooleanExpression exp, int language) {
		singleton.setLanguage(language);
		return singleton.prettyPrint(exp);
	}

	@Override
	public String caseBooleanFlagTerm(BooleanFlagTerm object) {
		StringBuffer buff = new StringBuffer(); 
		if (object.isNegated()) {
			if (language==VHDL) {
				return ("("+object.getTermName()+"='0')");
			}  else {
				return (NOT_KW+"("+object.getTermName()+")");
			}
		} else {
			if (language==VHDL) {
				return ("("+object.getTermName()+"='1')");
			}  else {
				return (object.getTermName());
			}
		}
	}

	@Override
	public String caseIntegerFlagTerm(IntegerFlagTerm object) {
		StringBuffer buff = new StringBuffer(); 
		if (object.isEqual()) {
			return ("("+object.getTermName()+EQU_KW+object.getValue()+")");
		} else {
			return ("("+object.getTermName()+NEQ_KW+object.getValue()+")");
		}
	}
	

}
