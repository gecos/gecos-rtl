/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequential Datapath</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getSequentialDatapath()
 * @model
 * @generated
 */
public interface SequentialDatapath extends Datapath, SequentialBlock {
} // SequentialDatapath
