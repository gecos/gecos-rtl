/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getOperator()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentBitwidth\r\n'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentBitwidth='self.width>0'"
 * @generated
 */
public interface Operator extends DataFlowBlock {
} // Operator
