/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.ActivableBlock;

import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;

import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RS Register</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getRSRegister()
 * @model
 * @generated
 */
public interface RSRegister extends SingleOutputDataFlowBlock, SingleInputDataFlowBlock, ActivableBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(0);'"
	 * @generated
	 */
	InDataPort getSetPort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getOutput();'"
	 * @generated
	 */
	OutDataPort getOutputPort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(0);'"
	 * @generated
	 */
	InDataPort getResetPort();

} // RSRegister
