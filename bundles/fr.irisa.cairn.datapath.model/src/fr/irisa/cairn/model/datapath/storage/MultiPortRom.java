/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
// killroy was here
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Port Rom</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.storage.MultiPortRom#getNbReadPort <em>Nb Read Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getMultiPortRom()
 * @model
 * @generated
 */
public interface MultiPortRom extends AbstractMemory {
	/**
	 * Returns the value of the '<em><b>Nb Read Port</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nb Read Port</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nb Read Port</em>' attribute.
	 * @see #setNbReadPort(int)
	 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getMultiPortRom_NbReadPort()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getNbReadPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.storage.MultiPortRom#getNbReadPort <em>Nb Read Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nb Read Port</em>' attribute.
	 * @see #getNbReadPort()
	 * @generated
	 */
	void setNbReadPort(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getActivate().get(2*portId);'"
	 * @generated
	 */
	InControlPort getReadEnable(int portId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(2*portId);'"
	 * @generated
	 */
	InDataPort getAddressPort(int portId);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getOut().get(portId);'"
	 * @generated
	 */
	OutDataPort getReadPort(int portId);

} // MultiPortRom
