/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Combinational Datapath</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getCombinationalDatapath()
 * @model
 * @generated
 */
public interface CombinationalDatapath extends Datapath, CombinationalBlock {
} // CombinationalDatapath
