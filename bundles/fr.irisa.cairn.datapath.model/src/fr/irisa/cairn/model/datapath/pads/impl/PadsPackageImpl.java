/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.InputPad;
import fr.irisa.cairn.model.datapath.pads.OutputPad;
import fr.irisa.cairn.model.datapath.pads.PadsFactory;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PadsPackageImpl extends EPackageImpl implements PadsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataInputPadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataOutputPadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlPadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statusPadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputPadEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputPadEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PadsPackageImpl() {
		super(eNS_URI, PadsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PadsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PadsPackage init() {
		if (isInited) return (PadsPackage)EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI);

		// Obtain or create and register package
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PadsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		thePadsPackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		thePadsPackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePadsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PadsPackage.eNS_URI, thePadsPackage);
		return thePadsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataInputPad() {
		return dataInputPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataInputPad_AssociatedPort() {
		return (EReference)dataInputPadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataOutputPad() {
		return dataOutputPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataOutputPad_AssociatedPort() {
		return (EReference)dataOutputPadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlPad() {
		return controlPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlPad_AssociatedPort() {
		return (EReference)controlPadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatusPad() {
		return statusPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStatusPad_AssociatedPort() {
		return (EReference)statusPadEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputPad() {
		return inputPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutputPad() {
		return outputPadEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PadsFactory getPadsFactory() {
		return (PadsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dataInputPadEClass = createEClass(DATA_INPUT_PAD);
		createEReference(dataInputPadEClass, DATA_INPUT_PAD__ASSOCIATED_PORT);

		dataOutputPadEClass = createEClass(DATA_OUTPUT_PAD);
		createEReference(dataOutputPadEClass, DATA_OUTPUT_PAD__ASSOCIATED_PORT);

		controlPadEClass = createEClass(CONTROL_PAD);
		createEReference(controlPadEClass, CONTROL_PAD__ASSOCIATED_PORT);

		statusPadEClass = createEClass(STATUS_PAD);
		createEReference(statusPadEClass, STATUS_PAD__ASSOCIATED_PORT);

		inputPadEClass = createEClass(INPUT_PAD);

		outputPadEClass = createEClass(OUTPUT_PAD);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OperatorsPackage theOperatorsPackage = (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dataInputPadEClass.getESuperTypes().add(theOperatorsPackage.getSingleOutputDataFlowBlock());
		dataInputPadEClass.getESuperTypes().add(this.getInputPad());
		dataOutputPadEClass.getESuperTypes().add(theOperatorsPackage.getSingleInputDataFlowBlock());
		dataOutputPadEClass.getESuperTypes().add(this.getOutputPad());
		controlPadEClass.getESuperTypes().add(this.getInputPad());
		controlPadEClass.getESuperTypes().add(theOperatorsPackage.getSingleFlagBlock());
		statusPadEClass.getESuperTypes().add(this.getOutputPad());
		statusPadEClass.getESuperTypes().add(theOperatorsPackage.getSingleControlPortBlock());
		inputPadEClass.getESuperTypes().add(theDatapathPackage.getPad());
		inputPadEClass.getESuperTypes().add(theDatapathPackage.getCombinationalBlock());
		outputPadEClass.getESuperTypes().add(theDatapathPackage.getPad());
		outputPadEClass.getESuperTypes().add(theDatapathPackage.getCombinationalBlock());

		// Initialize classes and features; add operations and parameters
		initEClass(dataInputPadEClass, DataInputPad.class, "DataInputPad", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataInputPad_AssociatedPort(), thePortPackage.getInDataPort(), thePortPackage.getInDataPort_AssociatedPad(), "associatedPort", null, 1, 1, DataInputPad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataOutputPadEClass, DataOutputPad.class, "DataOutputPad", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataOutputPad_AssociatedPort(), thePortPackage.getOutDataPort(), thePortPackage.getOutDataPort_AssociatedPad(), "associatedPort", null, 1, 1, DataOutputPad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlPadEClass, ControlPad.class, "ControlPad", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControlPad_AssociatedPort(), thePortPackage.getInControlPort(), thePortPackage.getInControlPort_AssociatedPad(), "associatedPort", null, 1, 1, ControlPad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(statusPadEClass, StatusPad.class, "StatusPad", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStatusPad_AssociatedPort(), thePortPackage.getOutControlPort(), null, "associatedPort", null, 1, 1, StatusPad.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputPadEClass, InputPad.class, "InputPad", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(outputPadEClass, OutputPad.class, "OutputPad", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //PadsPackageImpl
