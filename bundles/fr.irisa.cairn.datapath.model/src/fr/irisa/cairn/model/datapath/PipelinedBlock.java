/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pipelined Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.PipelinedBlock#getLatency <em>Latency</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getPipelinedBlock()
 * @model interface="true" abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortWidth consistentOutputPortWidth hasValidSources'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noCombinationalCycles=''"
 * @generated
 */
public interface PipelinedBlock extends SequentialBlock {
	/**
	 * Returns the value of the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latency</em>' attribute.
	 * @see #setLatency(int)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getPipelinedBlock_Latency()
	 * @model
	 * @generated
	 */
	int getLatency();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.PipelinedBlock#getLatency <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latency</em>' attribute.
	 * @see #getLatency()
	 * @generated
	 */
	void setLatency(int value);

} // PipelinedBlock
