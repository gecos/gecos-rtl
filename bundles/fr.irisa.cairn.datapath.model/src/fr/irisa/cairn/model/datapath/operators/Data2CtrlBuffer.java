/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.AbstractBlock;

import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data2 Ctrl Buffer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getData2CtrlBuffer()
 * @model
 * @generated
 */
public interface Data2CtrlBuffer extends FlagBearerBlock, SingleInputDataFlowBlock, SingleFlagBlock {

} // Data2CtrlBuffer
