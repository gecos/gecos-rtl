/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.Register;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.datapath.wires.HybridWire;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datapath</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getComponents <em>Components</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getDataWires <em>Data Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getControlWires <em>Control Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getRegisters <em>Registers</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getMemblocks <em>Memblocks</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getOperators <em>Operators</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.Datapath#getFsms <em>Fsms</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='noCombinationalCycles\r\n'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentPortTypes='in.select(port | not port.instandceOf(InDataPad)).size()=0 \r\nand\r\nout.select(port | not port.instandceOf(OutDataPad)).size()=0 \r\nand \r\nactivate.select(port | not port.instandceOf(InControlPad)).size()=0\r\nand \r\nflags.select(port | not port.instandceOf(OutControlPad)).size()=0\r\n' noCombinationalCycles='false' nonNegativeNumberOfComponents\040='components.size()>0'"
 * @generated
 */
public interface Datapath extends NamedElement, ActivableBlock, DataFlowBlock, FlagBearerBlock {
	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.AbstractBlock}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.AbstractBlock#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Components()
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock#getParent
	 * @model opposite="parent" containment="true" required="true"
	 * @generated
	 */
	EList<AbstractBlock> getComponents();

	/**
	 * Returns the value of the '<em><b>Data Wires</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.wires.DataFlowWire}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Wires</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Wires</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_DataWires()
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<DataFlowWire> getDataWires();

	/**
	 * Returns the value of the '<em><b>Control Wires</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Control Wires</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control Wires</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_ControlWires()
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<ControlFlowWire> getControlWires();

	/**
	 * Returns the value of the '<em><b>Library</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.AbstractBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Library</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Library</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Library()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<AbstractBlock> getLibrary();

	/**
	 * Returns the value of the '<em><b>Wires</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.Wire}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wires</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wires</em>' containment reference list.
	 * @see #isSetWires()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Wires()
	 * @model containment="true" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Wire> getWires();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.Datapath#getWires <em>Wires</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Wires</em>' containment reference list is set.
	 * @see #getWires()
	 * @generated
	 */
	boolean isSetWires();

	/**
	 * Returns the value of the '<em><b>Registers</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.storage.Register}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Registers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Registers</em>' containment reference list.
	 * @see #isSetRegisters()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Registers()
	 * @model containment="true" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<Register> getRegisters();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.Datapath#getRegisters <em>Registers</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Registers</em>' containment reference list is set.
	 * @see #getRegisters()
	 * @generated
	 */
	boolean isSetRegisters();

	/**
	 * Returns the value of the '<em><b>Memblocks</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.AbstractBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memblocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memblocks</em>' containment reference list.
	 * @see #isSetMemblocks()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Memblocks()
	 * @model containment="true" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated NOT
	 */
	EList<AbstractMemory> getMemblocks();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.Datapath#getMemblocks <em>Memblocks</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Memblocks</em>' containment reference list is set.
	 * @see #getMemblocks()
	 * @generated
	 */
	boolean isSetMemblocks();

	/**
	 * Returns the value of the '<em><b>Operators</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.CombinationalBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operators</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operators</em>' containment reference list.
	 * @see #isSetOperators()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Operators()
	 * @model containment="true" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<CombinationalBlock> getOperators();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.Datapath#getOperators <em>Operators</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operators</em>' containment reference list is set.
	 * @see #getOperators()
	 * @generated
	 */
	boolean isSetOperators();

	/**
	 * Returns the value of the '<em><b>Fsms</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.FSM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fsms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fsms</em>' containment reference list.
	 * @see #isSetFsms()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapath_Fsms()
	 * @model containment="true" unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	EList<FSM> getFsms();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.Datapath#getFsms <em>Fsms</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Fsms</em>' containment reference list is set.
	 * @see #getFsms()
	 * @generated
	 */
	boolean isSetFsms();

} // Datapath
