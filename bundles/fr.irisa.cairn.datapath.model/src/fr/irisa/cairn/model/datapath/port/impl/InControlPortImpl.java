/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;


import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.PortImpl;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Control Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl#getWire <em>Wire</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InControlPortImpl extends PortImpl implements InControlPort {


	public String toString() {
		if (getParentNode()!=null) {
			return getParentNode().getName()+"."+getName()+"["+(getWidth()-1)+":0]";
		} else {
			return "#."+getName()+"["+(getWidth()-1)+"]";
		}
	}

	/**
	 * The cached value of the '{@link #getWire() <em>Wire</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWire()
	 * @generated
	 * @ordered
	 */
	protected ControlFlowWire wire;

	/**
	 * The cached value of the '{@link #getAssociatedPad() <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPad()
	 * @generated
	 * @ordered
	 */
	protected ControlPad associatedPad;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InControlPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.IN_CONTROL_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire getWire() {
		if (wire != null && wire.eIsProxy()) {
			InternalEObject oldWire = (InternalEObject)wire;
			wire = (ControlFlowWire)eResolveProxy(oldWire);
			if (wire != oldWire) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.IN_CONTROL_PORT__WIRE, oldWire, wire));
			}
		}
		return wire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire basicGetWire() {
		return wire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWire(ControlFlowWire newWire, NotificationChain msgs) {
		ControlFlowWire oldWire = wire;
		wire = newWire;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortPackage.IN_CONTROL_PORT__WIRE, oldWire, newWire);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWire(ControlFlowWire newWire) {
		if (newWire != wire) {
			NotificationChain msgs = null;
			if (wire != null)
				msgs = ((InternalEObject)wire).eInverseRemove(this, WiresPackage.CONTROL_FLOW_WIRE__SINK, ControlFlowWire.class, msgs);
			if (newWire != null)
				msgs = ((InternalEObject)newWire).eInverseAdd(this, WiresPackage.CONTROL_FLOW_WIRE__SINK, ControlFlowWire.class, msgs);
			msgs = basicSetWire(newWire, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.IN_CONTROL_PORT__WIRE, newWire, newWire));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActivableBlock basicGetParentNode() {
		if(eContainer() instanceof ActivableBlock) {
			return (ActivableBlock) eContainer();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ActivableBlock  getParentNode() {
		return basicGetParentNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlPad getAssociatedPad() {
		if (associatedPad != null && associatedPad.eIsProxy()) {
			InternalEObject oldAssociatedPad = (InternalEObject)associatedPad;
			associatedPad = (ControlPad)eResolveProxy(oldAssociatedPad);
			if (associatedPad != oldAssociatedPad) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, oldAssociatedPad, associatedPad));
			}
		}
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlPad basicGetAssociatedPad() {
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedPad(ControlPad newAssociatedPad, NotificationChain msgs) {
		ControlPad oldAssociatedPad = associatedPad;
		associatedPad = newAssociatedPad;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, oldAssociatedPad, newAssociatedPad);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPad(ControlPad newAssociatedPad) {
		if (newAssociatedPad != associatedPad) {
			NotificationChain msgs = null;
			if (associatedPad != null)
				msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, ControlPad.class, msgs);
			if (newAssociatedPad != null)
				msgs = ((InternalEObject)newAssociatedPad).eInverseAdd(this, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, ControlPad.class, msgs);
			msgs = basicSetAssociatedPad(newAssociatedPad, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, newAssociatedPad, newAssociatedPad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getSourcePort() {
		return wire.getSource();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleFlagBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(OutControlPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				if (wire != null)
					msgs = ((InternalEObject)wire).eInverseRemove(this, WiresPackage.CONTROL_FLOW_WIRE__SINK, ControlFlowWire.class, msgs);
				return basicSetWire((ControlFlowWire)otherEnd, msgs);
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				if (associatedPad != null)
					msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, ControlPad.class, msgs);
				return basicSetAssociatedPad((ControlPad)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				return basicSetWire(null, msgs);
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				return basicSetAssociatedPad(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				if (resolve) return getWire();
				return basicGetWire();
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				if (resolve) return getAssociatedPad();
				return basicGetAssociatedPad();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				setWire((ControlFlowWire)newValue);
				return;
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				setAssociatedPad((ControlPad)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				setWire((ControlFlowWire)null);
				return;
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				setAssociatedPad((ControlPad)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.IN_CONTROL_PORT__WIRE:
				return wire != null;
			case PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD:
				return associatedPad != null;
		}
		return super.eIsSet(featureID);
	}


} //InControlPortImpl
