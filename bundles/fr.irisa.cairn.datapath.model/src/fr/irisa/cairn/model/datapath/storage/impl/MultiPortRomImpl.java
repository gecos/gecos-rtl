/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
// killroy was here
package fr.irisa.cairn.model.datapath.storage.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multi Port Rom</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.storage.impl.MultiPortRomImpl#getNbReadPort <em>Nb Read Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MultiPortRomImpl extends AbstractMemoryImpl implements MultiPortRom {
	/**
	 * The default value of the '{@link #getNbReadPort() <em>Nb Read Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbReadPort()
	 * @generated
	 * @ordered
	 */
	protected static final int NB_READ_PORT_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getNbReadPort() <em>Nb Read Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNbReadPort()
	 * @generated
	 * @ordered
	 */
	protected int nbReadPort = NB_READ_PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiPortRomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.MULTI_PORT_ROM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNbReadPort() {
		return nbReadPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNbReadPort(int newNbReadPort) {
		int oldNbReadPort = nbReadPort;
		nbReadPort = newNbReadPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StoragePackage.MULTI_PORT_ROM__NB_READ_PORT, oldNbReadPort, nbReadPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getReadEnable(int portId) {
		return getActivate().get(2*portId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getAddressPort(int portId) {
		return getIn().get(2*portId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getReadPort(int portId) {
		return getOut().get(portId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StoragePackage.MULTI_PORT_ROM__NB_READ_PORT:
				return getNbReadPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StoragePackage.MULTI_PORT_ROM__NB_READ_PORT:
				setNbReadPort((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StoragePackage.MULTI_PORT_ROM__NB_READ_PORT:
				setNbReadPort(NB_READ_PORT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StoragePackage.MULTI_PORT_ROM__NB_READ_PORT:
				return nbReadPort != NB_READ_PORT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nbReadPort: ");
		result.append(nbReadPort);
		result.append(')');
		return result.toString();
	}

} //MultiPortRomImpl
