/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import fr.irisa.cairn.model.datapath.port.InDataPort;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.Wire#getWidth <em>Width</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getWire()
 * @model abstract="true"
 * @generated
 */
public interface Wire extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getWire_Width()
	 * @model default="1" unique="false" required="true"
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.Wire#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);
	
	/**
	 * @generated NOT
	 */
	<T extends Port> T getSink();
	/**
	 * @generated NOT
	 */
	<T extends Port> T getSource();


} // Wire
