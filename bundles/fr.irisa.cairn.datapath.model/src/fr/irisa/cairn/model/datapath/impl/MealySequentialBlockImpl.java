/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.impl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mealy Sequential Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class MealySequentialBlockImpl extends EObjectImpl implements MealySequentialBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MealySequentialBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatapathPackage.Literals.MEALY_SEQUENTIAL_BLOCK;
	}

} //MealySequentialBlockImpl
