/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.DataFlowBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Output Data Flow Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getMultiOutputDataFlowBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface MultiOutputDataFlowBlock extends DataFlowBlock {
} // MultiOutputDataFlowBlock
