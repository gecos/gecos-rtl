/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.port.InControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activable Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.ActivableBlock#getActivate <em>Activate</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getActivableBlock()
 * @model interface="true" abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortWidth consistentOutputPortWidth hasValidSources'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noCombinationalCycles=''"
 * @generated
 */
public interface ActivableBlock extends AbstractBlock {
	/**
	 * Returns the value of the '<em><b>Activate</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.port.InControlPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activate</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getActivableBlock_Activate()
	 * @model containment="true"
	 * @generated
	 */
	EList<InControlPort> getActivate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getActivate().get(pos);'"
	 * @generated
	 */
	InControlPort getControlPort(int pos);

} // ActivableBlock
