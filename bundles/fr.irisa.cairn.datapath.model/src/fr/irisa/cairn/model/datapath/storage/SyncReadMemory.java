/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.MooreSequentialBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Read Memory</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getSyncReadMemory()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SyncReadMemory extends MooreSequentialBlock {
} // SyncReadMemory
