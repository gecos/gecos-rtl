/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.MultiCycleBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.PipelinedBlock;
import fr.irisa.cairn.model.datapath.SequentialBlock;
import fr.irisa.cairn.model.datapath.operators.*;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage
 * @generated
 */
public class OperatorsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OperatorsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OperatorsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperatorsSwitch<Adapter> modelSwitch =
		new OperatorsSwitch<Adapter>() {
			@Override
			public Adapter caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
				return createSingleInputDataFlowBlockAdapter();
			}
			@Override
			public Adapter caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
				return createSingleOutputDataFlowBlockAdapter();
			}
			@Override
			public Adapter caseSingleFlagBlock(SingleFlagBlock object) {
				return createSingleFlagBlockAdapter();
			}
			@Override
			public Adapter caseSingleControlPortBlock(SingleControlPortBlock object) {
				return createSingleControlPortBlockAdapter();
			}
			@Override
			public Adapter caseMultiOutputDataFlowBlock(MultiOutputDataFlowBlock object) {
				return createMultiOutputDataFlowBlockAdapter();
			}
			@Override
			public Adapter caseUnaryOperator(UnaryOperator object) {
				return createUnaryOperatorAdapter();
			}
			@Override
			public Adapter caseBinaryOperator(BinaryOperator object) {
				return createBinaryOperatorAdapter();
			}
			@Override
			public Adapter casePipelinedBinaryOperator(PipelinedBinaryOperator object) {
				return createPipelinedBinaryOperatorAdapter();
			}
			@Override
			public Adapter caseMultiCycleBinaryOperator(MultiCycleBinaryOperator object) {
				return createMultiCycleBinaryOperatorAdapter();
			}
			@Override
			public Adapter caseTernaryOperator(TernaryOperator object) {
				return createTernaryOperatorAdapter();
			}
			@Override
			public Adapter caseConstantValue(ConstantValue object) {
				return createConstantValueAdapter();
			}
			@Override
			public Adapter caseDataFlowMux(DataFlowMux object) {
				return createDataFlowMuxAdapter();
			}
			@Override
			public Adapter caseControlFlowMux(ControlFlowMux object) {
				return createControlFlowMuxAdapter();
			}
			@Override
			public Adapter caseReductionOperator(ReductionOperator object) {
				return createReductionOperatorAdapter();
			}
			@Override
			public Adapter casePipelinedReductionOperator(PipelinedReductionOperator object) {
				return createPipelinedReductionOperatorAdapter();
			}
			@Override
			public Adapter caseBitSelect(BitSelect object) {
				return createBitSelectAdapter();
			}
			@Override
			public Adapter caseCompare(Compare object) {
				return createCompareAdapter();
			}
			@Override
			public Adapter caseMerge(Merge object) {
				return createMergeAdapter();
			}
			@Override
			public Adapter caseQuantize(Quantize object) {
				return createQuantizeAdapter();
			}
			@Override
			public Adapter caseExpandUnsigned(ExpandUnsigned object) {
				return createExpandUnsignedAdapter();
			}
			@Override
			public Adapter caseExpandSigned(ExpandSigned object) {
				return createExpandSignedAdapter();
			}
			@Override
			public Adapter caseCtrl2DataBuffer(Ctrl2DataBuffer object) {
				return createCtrl2DataBufferAdapter();
			}
			@Override
			public Adapter caseData2CtrlBuffer(Data2CtrlBuffer object) {
				return createData2CtrlBufferAdapter();
			}
			@Override
			public Adapter caseCombinationnalOperator(CombinationnalOperator object) {
				return createCombinationnalOperatorAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseAbstractBlock(AbstractBlock object) {
				return createAbstractBlockAdapter();
			}
			@Override
			public Adapter caseDataFlowBlock(DataFlowBlock object) {
				return createDataFlowBlockAdapter();
			}
			@Override
			public Adapter caseFlagBearerBlock(FlagBearerBlock object) {
				return createFlagBearerBlockAdapter();
			}
			@Override
			public Adapter caseActivableBlock(ActivableBlock object) {
				return createActivableBlockAdapter();
			}
			@Override
			public Adapter caseCombinationalBlock(CombinationalBlock object) {
				return createCombinationalBlockAdapter();
			}
			@Override
			public Adapter caseSequentialBlock(SequentialBlock object) {
				return createSequentialBlockAdapter();
			}
			@Override
			public Adapter casePipelinedBlock(PipelinedBlock object) {
				return createPipelinedBlockAdapter();
			}
			@Override
			public Adapter caseMultiCycleBlock(MultiCycleBlock object) {
				return createMultiCycleBlockAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock <em>Single Input Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock
	 * @generated
	 */
	public Adapter createSingleInputDataFlowBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock <em>Single Output Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock
	 * @generated
	 */
	public Adapter createSingleOutputDataFlowBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock <em>Multi Output Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock
	 * @generated
	 */
	public Adapter createMultiOutputDataFlowBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOperator
	 * @generated
	 */
	public Adapter createUnaryOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.BinaryOperator
	 * @generated
	 */
	public Adapter createBinaryOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator <em>Pipelined Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator
	 * @generated
	 */
	public Adapter createPipelinedBinaryOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator <em>Multi Cycle Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator
	 * @generated
	 */
	public Adapter createMultiCycleBinaryOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.TernaryOperator <em>Ternary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.TernaryOperator
	 * @generated
	 */
	public Adapter createTernaryOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.ConstantValue <em>Constant Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.ConstantValue
	 * @generated
	 */
	public Adapter createConstantValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.DataFlowMux <em>Data Flow Mux</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.DataFlowMux
	 * @generated
	 */
	public Adapter createDataFlowMuxAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.ControlFlowMux <em>Control Flow Mux</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.ControlFlowMux
	 * @generated
	 */
	public Adapter createControlFlowMuxAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.ReductionOperator <em>Reduction Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOperator
	 * @generated
	 */
	public Adapter createReductionOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator <em>Pipelined Reduction Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator
	 * @generated
	 */
	public Adapter createPipelinedReductionOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.BitSelect <em>Bit Select</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.BitSelect
	 * @generated
	 */
	public Adapter createBitSelectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.Compare <em>Compare</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.Compare
	 * @generated
	 */
	public Adapter createCompareAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.Merge <em>Merge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.Merge
	 * @generated
	 */
	public Adapter createMergeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.Quantize <em>Quantize</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.Quantize
	 * @generated
	 */
	public Adapter createQuantizeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.ExpandUnsigned <em>Expand Unsigned</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandUnsigned
	 * @generated
	 */
	public Adapter createExpandUnsignedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.ExpandSigned <em>Expand Signed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.ExpandSigned
	 * @generated
	 */
	public Adapter createExpandSignedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer <em>Ctrl2 Data Buffer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer
	 * @generated
	 */
	public Adapter createCtrl2DataBufferAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer <em>Data2 Ctrl Buffer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer
	 * @generated
	 */
	public Adapter createData2CtrlBufferAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.CombinationnalOperator <em>Combinationnal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.CombinationnalOperator
	 * @generated
	 */
	public Adapter createCombinationnalOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.SingleFlagBlock <em>Single Flag Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleFlagBlock
	 * @generated
	 */
	public Adapter createSingleFlagBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock <em>Single Control Port Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock
	 * @generated
	 */
	public Adapter createSingleControlPortBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.AbstractBlock <em>Abstract Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock
	 * @generated
	 */
	public Adapter createAbstractBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.DataFlowBlock <em>Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock
	 * @generated
	 */
	public Adapter createDataFlowBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.ActivableBlock <em>Activable Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.ActivableBlock
	 * @generated
	 */
	public Adapter createActivableBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.CombinationalBlock <em>Combinational Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.CombinationalBlock
	 * @generated
	 */
	public Adapter createCombinationalBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.SequentialBlock <em>Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.SequentialBlock
	 * @generated
	 */
	public Adapter createSequentialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.PipelinedBlock <em>Pipelined Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.PipelinedBlock
	 * @generated
	 */
	public Adapter createPipelinedBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock <em>Multi Cycle Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock
	 * @generated
	 */
	public Adapter createMultiCycleBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock <em>Flag Bearer Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock
	 * @generated
	 */
	public Adapter createFlagBearerBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OperatorsAdapterFactory
