/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Output Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getDataOutputPad()
 * @model
 * @generated
 */
public interface DataOutputPad extends SingleInputDataFlowBlock, OutputPad {

	/**
	 * Returns the value of the '<em><b>Associated Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Port</em>' reference.
	 * @see #setAssociatedPort(OutDataPort)
	 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getDataOutputPad_AssociatedPort()
	 * @see fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad
	 * @model opposite="associatedPad" required="true"
	 * @generated
	 */
	OutDataPort getAssociatedPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Port</em>' reference.
	 * @see #getAssociatedPort()
	 * @generated
	 */
	void setAssociatedPort(OutDataPort value);

} // DataOutputPad
