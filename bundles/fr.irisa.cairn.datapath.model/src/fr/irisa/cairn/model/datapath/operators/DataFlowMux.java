/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.DataFlowBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Flow Mux</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getDataFlowMux()
 * @model
 * @generated
 */
public interface DataFlowMux extends SingleOutputDataFlowBlock {
} // DataFlowMux
