/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.impl;

import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sync Read SPRAM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SyncReadSPRAMImpl extends SinglePortRamImpl implements SyncReadSPRAM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SyncReadSPRAMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.SYNC_READ_SPRAM;
	}

} //SyncReadSPRAMImpl
