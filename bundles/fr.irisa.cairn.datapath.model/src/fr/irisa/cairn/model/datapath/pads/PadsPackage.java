/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.pads.PadsFactory
 * @model kind="package"
 * @generated
 */
public interface PadsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "pads";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/pads";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "pads";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PadsPackage eINSTANCE = fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.impl.DataInputPadImpl <em>Data Input Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.impl.DataInputPadImpl
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getDataInputPad()
	 * @generated
	 */
	int DATA_INPUT_PAD = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__NAME = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__PARENT = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__COMBINATIONAL = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__IN = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__OUT = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Associated Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD__ASSOCIATED_PORT = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Input Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_INPUT_PAD_FEATURE_COUNT = OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl <em>Data Output Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getDataOutputPad()
	 * @generated
	 */
	int DATA_OUTPUT_PAD = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__NAME = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__PARENT = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__COMBINATIONAL = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__IN = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__OUT = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK__OUT;

	/**
	 * The feature id for the '<em><b>Associated Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD__ASSOCIATED_PORT = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data Output Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_OUTPUT_PAD_FEATURE_COUNT = OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl <em>Control Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getControlPad()
	 * @generated
	 */
	int CONTROL_PAD = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl <em>Status Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getStatusPad()
	 * @generated
	 */
	int STATUS_PAD = 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.InputPad <em>Input Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.InputPad
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getInputPad()
	 * @generated
	 */
	int INPUT_PAD = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PAD__NAME = DatapathPackage.PAD__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PAD__PARENT = DatapathPackage.PAD__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PAD__COMBINATIONAL = DatapathPackage.PAD__COMBINATIONAL;

	/**
	 * The number of structural features of the '<em>Input Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_PAD_FEATURE_COUNT = DatapathPackage.PAD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD__NAME = INPUT_PAD__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD__PARENT = INPUT_PAD__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD__COMBINATIONAL = INPUT_PAD__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD__FLAGS = INPUT_PAD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD__ASSOCIATED_PORT = INPUT_PAD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Control Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_PAD_FEATURE_COUNT = INPUT_PAD_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.pads.OutputPad <em>Output Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.pads.OutputPad
	 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getOutputPad()
	 * @generated
	 */
	int OUTPUT_PAD = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PAD__NAME = DatapathPackage.PAD__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PAD__PARENT = DatapathPackage.PAD__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PAD__COMBINATIONAL = DatapathPackage.PAD__COMBINATIONAL;

	/**
	 * The number of structural features of the '<em>Output Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_PAD_FEATURE_COUNT = DatapathPackage.PAD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD__NAME = OUTPUT_PAD__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD__PARENT = OUTPUT_PAD__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD__COMBINATIONAL = OUTPUT_PAD__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD__ACTIVATE = OUTPUT_PAD_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associated Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD__ASSOCIATED_PORT = OUTPUT_PAD_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Status Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_PAD_FEATURE_COUNT = OUTPUT_PAD_FEATURE_COUNT + 2;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.DataInputPad <em>Data Input Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Input Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.DataInputPad
	 * @generated
	 */
	EClass getDataInputPad();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort()
	 * @see #getDataInputPad()
	 * @generated
	 */
	EReference getDataInputPad_AssociatedPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.DataOutputPad <em>Data Output Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Output Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.DataOutputPad
	 * @generated
	 */
	EClass getDataOutputPad();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort()
	 * @see #getDataOutputPad()
	 * @generated
	 */
	EReference getDataOutputPad_AssociatedPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.ControlPad <em>Control Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.ControlPad
	 * @generated
	 */
	EClass getControlPad();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort()
	 * @see #getControlPad()
	 * @generated
	 */
	EReference getControlPad_AssociatedPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.StatusPad <em>Status Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Status Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.StatusPad
	 * @generated
	 */
	EClass getStatusPad();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.pads.StatusPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.StatusPad#getAssociatedPort()
	 * @see #getStatusPad()
	 * @generated
	 */
	EReference getStatusPad_AssociatedPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.InputPad <em>Input Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.InputPad
	 * @generated
	 */
	EClass getInputPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.pads.OutputPad <em>Output Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.pads.OutputPad
	 * @generated
	 */
	EClass getOutputPad();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PadsFactory getPadsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.impl.DataInputPadImpl <em>Data Input Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.impl.DataInputPadImpl
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getDataInputPad()
		 * @generated
		 */
		EClass DATA_INPUT_PAD = eINSTANCE.getDataInputPad();

		/**
		 * The meta object literal for the '<em><b>Associated Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_INPUT_PAD__ASSOCIATED_PORT = eINSTANCE.getDataInputPad_AssociatedPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl <em>Data Output Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getDataOutputPad()
		 * @generated
		 */
		EClass DATA_OUTPUT_PAD = eINSTANCE.getDataOutputPad();

		/**
		 * The meta object literal for the '<em><b>Associated Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_OUTPUT_PAD__ASSOCIATED_PORT = eINSTANCE.getDataOutputPad_AssociatedPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl <em>Control Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getControlPad()
		 * @generated
		 */
		EClass CONTROL_PAD = eINSTANCE.getControlPad();

		/**
		 * The meta object literal for the '<em><b>Associated Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_PAD__ASSOCIATED_PORT = eINSTANCE.getControlPad_AssociatedPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl <em>Status Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getStatusPad()
		 * @generated
		 */
		EClass STATUS_PAD = eINSTANCE.getStatusPad();

		/**
		 * The meta object literal for the '<em><b>Associated Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATUS_PAD__ASSOCIATED_PORT = eINSTANCE.getStatusPad_AssociatedPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.InputPad <em>Input Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.InputPad
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getInputPad()
		 * @generated
		 */
		EClass INPUT_PAD = eINSTANCE.getInputPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.pads.OutputPad <em>Output Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.pads.OutputPad
		 * @see fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl#getOutputPad()
		 * @generated
		 */
		EClass OUTPUT_PAD = eINSTANCE.getOutputPad();

	}

} //PadsPackage
