/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Flow Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getOut <em>Out</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDataFlowBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface DataFlowBlock extends AbstractBlock {
	/**
	 * Returns the value of the '<em><b>In</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.port.InDataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In</em>' containment reference list.
	 * @see #isSetIn()
	 * @see #unsetIn()
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDataFlowBlock_In()
	 * @model containment="true" unsettable="true"
	 * @generated
	 */
	EList<InDataPort> getIn();

	/**
	 * Unsets the value of the '{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getIn <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIn()
	 * @see #getIn()
	 * @generated
	 */
	void unsetIn();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getIn <em>In</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>In</em>' containment reference list is set.
	 * @see #unsetIn()
	 * @see #getIn()
	 * @generated
	 */
	boolean isSetIn();

	/**
	 * Returns the value of the '<em><b>Out</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.port.OutDataPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDataFlowBlock_Out()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutDataPort> getOut();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(pos);'"
	 * @generated
	 */
	InDataPort getInput(int pos);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getOut().get(pos);'"
	 * @generated
	 */
	OutDataPort getOutput(int pos);

} // DataFlowBlock
