/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compare</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.Compare#getOpcode <em>Opcode</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getCompare()
 * @model
 * @generated
 */
public interface Compare extends SingleOutputDataFlowBlock {
	/**
	 * Returns the value of the '<em><b>Opcode</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.model.datapath.operators.CompareOpcode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @see #setOpcode(CompareOpcode)
	 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getCompare_Opcode()
	 * @model required="true"
	 * @generated
	 */
	CompareOpcode getOpcode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.operators.Compare#getOpcode <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @see #getOpcode()
	 * @generated
	 */
	void setOpcode(CompareOpcode value);

} // Compare
