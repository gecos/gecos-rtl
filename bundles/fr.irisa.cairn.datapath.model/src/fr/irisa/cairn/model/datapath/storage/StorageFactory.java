/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage
 * @generated
 */
public interface StorageFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StorageFactory eINSTANCE = fr.irisa.cairn.model.datapath.storage.impl.StorageFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Register</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Register</em>'.
	 * @generated
	 */
	Register createRegister();

	/**
	 * Returns a new object of class '<em>CE Register</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CE Register</em>'.
	 * @generated
	 */
	CERegister createCERegister();

	/**
	 * Returns a new object of class '<em>Shift Register</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Shift Register</em>'.
	 * @generated
	 */
	ShiftRegister createShiftRegister();

	/**
	 * Returns a new object of class '<em>Abstract Memory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Memory</em>'.
	 * @generated
	 */
	AbstractMemory createAbstractMemory();

	/**
	 * Returns a new object of class '<em>Multi Port Ram</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multi Port Ram</em>'.
	 * @generated
	 */
	MultiPortRam createMultiPortRam();

	/**
	 * Returns a new object of class '<em>Single Port Rom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Port Rom</em>'.
	 * @generated
	 */
	SinglePortRom createSinglePortRom();

	/**
	 * Returns a new object of class '<em>Multi Port Rom</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multi Port Rom</em>'.
	 * @generated
	 */
	MultiPortRom createMultiPortRom();

	/**
	 * Returns a new object of class '<em>Sync Read SPRAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sync Read SPRAM</em>'.
	 * @generated
	 */
	SyncReadSPRAM createSyncReadSPRAM();

	/**
	 * Returns a new object of class '<em>Async Read SPRAM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Async Read SPRAM</em>'.
	 * @generated
	 */
	AsyncReadSPRAM createAsyncReadSPRAM();

	/**
	 * Returns a new object of class '<em>Register File</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Register File</em>'.
	 * @generated
	 */
	RegisterFile createRegisterFile();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	StoragePackage getStoragePackage();

} //StorageFactory
