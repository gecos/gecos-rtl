/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.AbstractBlock#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.AbstractBlock#isCombinational <em>Combinational</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getAbstractBlock()
 * @model interface="true" abstract="true"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noCombinationalCycles=''"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortWidth consistentOutputPortWidth hasValidSources'"
 * @generated
 */
public interface AbstractBlock extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.Datapath#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Datapath)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getAbstractBlock_Parent()
	 * @see fr.irisa.cairn.model.datapath.Datapath#getComponents
	 * @model opposite="components" transient="false"
	 * @generated
	 */
	Datapath getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.AbstractBlock#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Datapath value);

	/**
	 * Returns the value of the '<em><b>Combinational</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Combinational</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Combinational</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getAbstractBlock_Combinational()
	 * @model default="false" unique="false" required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	boolean isCombinational();

} // AbstractBlock
