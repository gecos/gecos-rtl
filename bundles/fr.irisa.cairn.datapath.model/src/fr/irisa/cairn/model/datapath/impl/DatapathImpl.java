/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.HybridWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.fsm.FSM;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Datapath</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getActivate <em>Activate</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getFlags <em>Flags</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getDataWires <em>Data Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getControlWires <em>Control Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getLibrary <em>Library</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getRegisters <em>Registers</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getMemblocks <em>Memblocks</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getOperators <em>Operators</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl#getFsms <em>Fsms</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DatapathImpl extends NamedElementImpl implements Datapath {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivate()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> activate;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected EList<OutControlPort> flags;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractBlock> components;

	/**
	 * The cached value of the '{@link #getDataWires() <em>Data Wires</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataWires()
	 * @generated
	 * @ordered
	 */
	protected EList<DataFlowWire> dataWires;

	/**
	 * The cached value of the '{@link #getControlWires() <em>Control Wires</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlWires()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlFlowWire> controlWires;

	/**
	 * The cached value of the '{@link #getLibrary() <em>Library</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLibrary()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractBlock> library;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatapathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatapathPackage.Literals.DATAPATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != DatapathPackage.DATAPATH__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, DatapathPackage.DATAPATH__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != DatapathPackage.DATAPATH__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatapathPackage.DATAPATH__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isCombinational() {
		//return COMBINATIONAL_EDEFAULT;
		boolean result = true;
		
		for(AbstractBlock block:this.getComponents())
			if((block instanceof Register) || (block instanceof AbstractMemory))
				result = false;
		if(this instanceof FSM)
			result = false;
		if(this instanceof Register)
			result = false;
		if(this instanceof AbstractMemory)
			result = false;
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getActivate() {
		if (activate == null) {
			activate = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, DatapathPackage.DATAPATH__ACTIVATE);
		}
		return activate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, DatapathPackage.DATAPATH__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, DatapathPackage.DATAPATH__OUT);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutControlPort> getFlags() {
		if (flags == null) {
			flags = new EObjectContainmentEList<OutControlPort>(OutControlPort.class, this, DatapathPackage.DATAPATH__FLAGS);
		}
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractBlock> getComponents() {
		if (components == null) {
			components = new EObjectContainmentWithInverseEList<AbstractBlock>(AbstractBlock.class, this, DatapathPackage.DATAPATH__COMPONENTS, DatapathPackage.ABSTRACT_BLOCK__PARENT);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataFlowWire> getDataWires() {
		if (dataWires == null) {
			dataWires = new EObjectContainmentWithInverseEList<DataFlowWire>(DataFlowWire.class, this, DatapathPackage.DATAPATH__DATA_WIRES, WiresPackage.DATA_FLOW_WIRE__PARENT);
		}
		return dataWires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlFlowWire> getControlWires() {
		if (controlWires == null) {
			controlWires = new EObjectContainmentWithInverseEList<ControlFlowWire>(ControlFlowWire.class, this, DatapathPackage.DATAPATH__CONTROL_WIRES, WiresPackage.CONTROL_FLOW_WIRE__PARENT);
		}
		return controlWires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbstractBlock> getLibrary() {
		if (library == null) {
			library = new EObjectContainmentEList<AbstractBlock>(AbstractBlock.class, this, DatapathPackage.DATAPATH__LIBRARY);
		}
		return library;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Wire> getWires() {
		EList<Wire> res = new BasicEList<Wire>();
		res.addAll(getDataWires());
		res.addAll(getControlWires());
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetWires() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Register> getRegisters() {
		EList<Register> res = new BasicEList<Register>();
		for(AbstractBlock blk : getComponents()) {
			if(blk instanceof Register) {
				res.add((Register) blk);
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetRegisters() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<AbstractMemory> getMemblocks() {
		EList<AbstractMemory> res = new BasicEList<AbstractMemory>();
		for(AbstractBlock blk : getComponents()) {
			if(blk instanceof AbstractMemory) {
				res.add((AbstractMemory) blk);
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetMemblocks() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<CombinationalBlock> getOperators() {
		EList<CombinationalBlock> res = new BasicEList<CombinationalBlock>();
		for(AbstractBlock blk : getComponents()) {
			if(blk instanceof CombinationalBlock) {
				res.add((CombinationalBlock) blk);
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetOperators() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FSM> getFsms() {
		EList<FSM> res = new BasicEList<FSM>();
		for(AbstractBlock blk : getComponents()) {
			if(blk instanceof FSM) {
				res.add((FSM) blk);
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFsms() {
		// TODO: implement this method to return whether the 'Fsms' containment reference list is set
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getFlag(int pos) {
		return getFlags().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControlPort(int pos) {
		return getActivate().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
			case DatapathPackage.DATAPATH__COMPONENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getComponents()).basicAdd(otherEnd, msgs);
			case DatapathPackage.DATAPATH__DATA_WIRES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getDataWires()).basicAdd(otherEnd, msgs);
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getControlWires()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				return basicSetParent(null, msgs);
			case DatapathPackage.DATAPATH__ACTIVATE:
				return ((InternalEList<?>)getActivate()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__FLAGS:
				return ((InternalEList<?>)getFlags()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__DATA_WIRES:
				return ((InternalEList<?>)getDataWires()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				return ((InternalEList<?>)getControlWires()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__LIBRARY:
				return ((InternalEList<?>)getLibrary()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__WIRES:
				return ((InternalEList<?>)getWires()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__REGISTERS:
				return ((InternalEList<?>)getRegisters()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__MEMBLOCKS:
				return ((InternalEList<?>)getMemblocks()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__OPERATORS:
				return ((InternalEList<?>)getOperators()).basicRemove(otherEnd, msgs);
			case DatapathPackage.DATAPATH__FSMS:
				return ((InternalEList<?>)getFsms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatapathPackage.DATAPATH__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				return getParent();
			case DatapathPackage.DATAPATH__COMBINATIONAL:
				return isCombinational();
			case DatapathPackage.DATAPATH__ACTIVATE:
				return getActivate();
			case DatapathPackage.DATAPATH__IN:
				return getIn();
			case DatapathPackage.DATAPATH__OUT:
				return getOut();
			case DatapathPackage.DATAPATH__FLAGS:
				return getFlags();
			case DatapathPackage.DATAPATH__COMPONENTS:
				return getComponents();
			case DatapathPackage.DATAPATH__DATA_WIRES:
				return getDataWires();
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				return getControlWires();
			case DatapathPackage.DATAPATH__LIBRARY:
				return getLibrary();
			case DatapathPackage.DATAPATH__WIRES:
				return getWires();
			case DatapathPackage.DATAPATH__REGISTERS:
				return getRegisters();
			case DatapathPackage.DATAPATH__MEMBLOCKS:
				return getMemblocks();
			case DatapathPackage.DATAPATH__OPERATORS:
				return getOperators();
			case DatapathPackage.DATAPATH__FSMS:
				return getFsms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				setParent((Datapath)newValue);
				return;
			case DatapathPackage.DATAPATH__ACTIVATE:
				getActivate().clear();
				getActivate().addAll((Collection<? extends InControlPort>)newValue);
				return;
			case DatapathPackage.DATAPATH__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case DatapathPackage.DATAPATH__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case DatapathPackage.DATAPATH__FLAGS:
				getFlags().clear();
				getFlags().addAll((Collection<? extends OutControlPort>)newValue);
				return;
			case DatapathPackage.DATAPATH__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends AbstractBlock>)newValue);
				return;
			case DatapathPackage.DATAPATH__DATA_WIRES:
				getDataWires().clear();
				getDataWires().addAll((Collection<? extends DataFlowWire>)newValue);
				return;
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				getControlWires().clear();
				getControlWires().addAll((Collection<? extends ControlFlowWire>)newValue);
				return;
			case DatapathPackage.DATAPATH__LIBRARY:
				getLibrary().clear();
				getLibrary().addAll((Collection<? extends AbstractBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				setParent((Datapath)null);
				return;
			case DatapathPackage.DATAPATH__ACTIVATE:
				getActivate().clear();
				return;
			case DatapathPackage.DATAPATH__IN:
				unsetIn();
				return;
			case DatapathPackage.DATAPATH__OUT:
				getOut().clear();
				return;
			case DatapathPackage.DATAPATH__FLAGS:
				getFlags().clear();
				return;
			case DatapathPackage.DATAPATH__COMPONENTS:
				getComponents().clear();
				return;
			case DatapathPackage.DATAPATH__DATA_WIRES:
				getDataWires().clear();
				return;
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				getControlWires().clear();
				return;
			case DatapathPackage.DATAPATH__LIBRARY:
				getLibrary().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatapathPackage.DATAPATH__PARENT:
				return getParent() != null;
			case DatapathPackage.DATAPATH__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case DatapathPackage.DATAPATH__ACTIVATE:
				return activate != null && !activate.isEmpty();
			case DatapathPackage.DATAPATH__IN:
				return isSetIn();
			case DatapathPackage.DATAPATH__OUT:
				return out != null && !out.isEmpty();
			case DatapathPackage.DATAPATH__FLAGS:
				return flags != null && !flags.isEmpty();
			case DatapathPackage.DATAPATH__COMPONENTS:
				return components != null && !components.isEmpty();
			case DatapathPackage.DATAPATH__DATA_WIRES:
				return dataWires != null && !dataWires.isEmpty();
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				return controlWires != null && !controlWires.isEmpty();
			case DatapathPackage.DATAPATH__LIBRARY:
				return library != null && !library.isEmpty();
			case DatapathPackage.DATAPATH__WIRES:
				return isSetWires();
			case DatapathPackage.DATAPATH__REGISTERS:
				return isSetRegisters();
			case DatapathPackage.DATAPATH__MEMBLOCKS:
				return isSetMemblocks();
			case DatapathPackage.DATAPATH__OPERATORS:
				return isSetOperators();
			case DatapathPackage.DATAPATH__FSMS:
				return isSetFsms();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.DATAPATH__PARENT: return DatapathPackage.ABSTRACT_BLOCK__PARENT;
				case DatapathPackage.DATAPATH__COMBINATIONAL: return DatapathPackage.ABSTRACT_BLOCK__COMBINATIONAL;
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.DATAPATH__ACTIVATE: return DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == DataFlowBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.DATAPATH__IN: return DatapathPackage.DATA_FLOW_BLOCK__IN;
				case DatapathPackage.DATAPATH__OUT: return DatapathPackage.DATA_FLOW_BLOCK__OUT;
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (derivedFeatureID) {
				case DatapathPackage.DATAPATH__FLAGS: return DatapathPackage.FLAG_BEARER_BLOCK__FLAGS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ABSTRACT_BLOCK__PARENT: return DatapathPackage.DATAPATH__PARENT;
				case DatapathPackage.ABSTRACT_BLOCK__COMBINATIONAL: return DatapathPackage.DATAPATH__COMBINATIONAL;
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE: return DatapathPackage.DATAPATH__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == DataFlowBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.DATA_FLOW_BLOCK__IN: return DatapathPackage.DATAPATH__IN;
				case DatapathPackage.DATA_FLOW_BLOCK__OUT: return DatapathPackage.DATAPATH__OUT;
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.FLAG_BEARER_BLOCK__FLAGS: return DatapathPackage.DATAPATH__FLAGS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //DatapathImpl
