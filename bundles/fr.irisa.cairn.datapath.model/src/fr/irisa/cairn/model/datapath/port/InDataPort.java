/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.InDataPort#getWire <em>Wire</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInDataPort()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentBitwidth\r\n'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentBitwidth='self.sources.forall (w|w.width<=self.width)'"
 * @generated
 */
public interface InDataPort extends Port {

	/**
	 * Returns the value of the '<em><b>Wire</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wire</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wire</em>' reference.
	 * @see #setWire(DataFlowWire)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInDataPort_Wire()
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink
	 * @model opposite="sink"
	 * @generated
	 */
	DataFlowWire getWire();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getWire <em>Wire</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wire</em>' reference.
	 * @see #getWire()
	 * @generated
	 */
	void setWire(DataFlowWire value);

	/**
	 * Returns the value of the '<em><b>Associated Pad</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Pad</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Pad</em>' reference.
	 * @see #setAssociatedPad(DataInputPad)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInDataPort_AssociatedPad()
	 * @see fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort
	 * @model opposite="associatedPort"
	 * @generated
	 */
	DataInputPad getAssociatedPad();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Pad</em>' reference.
	 * @see #getAssociatedPad()
	 * @generated
	 */
	void setAssociatedPad(DataInputPad value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return wire.getSource();'"
	 * @generated
	 */
	OutDataPort getSourcePort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	DataFlowWire connect(SingleOutputDataFlowBlock source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	DataFlowWire connect(OutDataPort source);
	
	DataFlowBlock getParentNode();

} // InDataPort
