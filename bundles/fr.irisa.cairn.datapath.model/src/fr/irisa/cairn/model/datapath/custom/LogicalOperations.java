package fr.irisa.cairn.model.datapath.custom;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.util.FsmSwitch;

public class LogicalOperations extends FsmSwitch<AbstractBooleanExpression>{

	private static FsmFactory factory = FsmFactory.eINSTANCE;
	private static LogicalOperations singleton = new LogicalOperations();
	
	public FsmFactory getFactory() {
		return factory;
	}

	private LogicalOperations() {
		
	}
	
	private class Simplifier  extends FsmSwitch {

//		public simplify( {
//			
//		}
//		
//		@Override
//		public Object caseAndExpression(AndExpression object) {
//			String res = null;
//			for (AbstractBooleanExpression exp : object.getTerms()) {
//				res = (String) doSwitch(exp);
//				if (res.equals("0")) {
//					return "0";
//				}
//			}
//			return "-";
//		}
//
//		@Override
//		public Object caseBooleanConstant(BooleanConstant object) {
//			if (object.isValue()) return "1"; else return "0";
//		}
//		
//
//
//		@Override
//		public Object caseOrExpression(OrExpression object) {
//			String res = null;
//			for (AbstractBooleanExpression exp : object.getTerms()) {
//				res = (String) doSwitch(exp);
//				if (res.equals("1")) {
//					return "1";
//				}
//			}
//			return "-";
//		}
		
		
	}
	public static AbstractBooleanExpression clone(AbstractBooleanExpression abs) {
		AbstractBooleanExpression clone = (AbstractBooleanExpression) new EcoreUtil().copy(abs);
		return clone;
	}

	public static AbstractBooleanExpression and(AbstractBooleanExpression ... bexps) {
		AndExpression res = singleton.getFactory().createAndExpression();
		for (AbstractBooleanExpression term : bexps) {
			if (term!=null) {
				res.getTerms().add(clone(term));
			} else {
				throw new RuntimeException("Null boolean expression");
			}
		}
		return res;
	}

	public static AbstractBooleanExpression or(AbstractBooleanExpression ... bexps) {
		OrExpression res = singleton.getFactory().createOrExpression();
		for (AbstractBooleanExpression term : bexps) {
			res.getTerms().add(clone(term));
		}
		return res;
	}

	public static AbstractBooleanExpression not(AbstractBooleanExpression bexp) {
		NegateExpression res = singleton.getFactory().createNegateExpression();
		res.setTerm(clone(bexp));
		return res;
	}

	public AbstractBooleanExpression caseAndExpression(AndExpression object) {
		OrExpression res = factory.createOrExpression();
		for (AbstractBooleanExpression term : object.getTerms()) {
			res.getTerms().add(doSwitch(term));
		}
		return res;
	}


	@Override
	public AbstractBooleanExpression caseOrExpression(OrExpression object) {
		AndExpression res = factory.createAndExpression();
		for (AbstractBooleanExpression term : object.getTerms()) {
			res.getTerms().add(doSwitch(term));
		}
		return res;
	}


	@Override
	public AbstractBooleanExpression caseFlagTerm(FlagTerm object) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public AbstractBooleanExpression caseBooleanFlagTerm(BooleanFlagTerm object) {
		BooleanFlagTerm  res = factory.createBooleanFlagTerm();
		res.setFlag(object.getFlag());
		res.setNegated(!object.isNegated());
		return res;
	}

	@Override
	public AbstractBooleanExpression caseIndexedBooleanFlagTerm(
			IndexedBooleanFlagTerm object) {
		throw new UnsupportedOperationException("Not yet implemented");	
	}

	@Override
	public AbstractBooleanExpression caseIntegerFlagTerm(IntegerFlagTerm object) {
		throw new UnsupportedOperationException("Not yet implemented");
		
	}

	
}
