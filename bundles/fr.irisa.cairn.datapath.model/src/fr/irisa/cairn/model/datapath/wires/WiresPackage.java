/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fr.irisa.cairn.model.datapath.DatapathPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.wires.WiresFactory
 * @model kind="package"
 * @generated
 */
public interface WiresPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "wires";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/wires";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "wires";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WiresPackage eINSTANCE = fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.wires.impl.DataFlowWireImpl <em>Data Flow Wire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.wires.impl.DataFlowWireImpl
	 * @see fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl#getDataFlowWire()
	 * @generated
	 */
	int DATA_FLOW_WIRE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE__NAME = DatapathPackage.WIRE__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE__WIDTH = DatapathPackage.WIRE__WIDTH;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE__SOURCE = DatapathPackage.WIRE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE__SINK = DatapathPackage.WIRE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE__PARENT = DatapathPackage.WIRE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Data Flow Wire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_WIRE_FEATURE_COUNT = DatapathPackage.WIRE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.wires.impl.ControlFlowWireImpl <em>Control Flow Wire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.wires.impl.ControlFlowWireImpl
	 * @see fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl#getControlFlowWire()
	 * @generated
	 */
	int CONTROL_FLOW_WIRE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE__NAME = DatapathPackage.WIRE__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE__WIDTH = DatapathPackage.WIRE__WIDTH;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE__SOURCE = DatapathPackage.WIRE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sink</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE__SINK = DatapathPackage.WIRE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE__PARENT = DatapathPackage.WIRE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Control Flow Wire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FLOW_WIRE_FEATURE_COUNT = DatapathPackage.WIRE_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire <em>Data Flow Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Flow Wire</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire
	 * @generated
	 */
	EClass getDataFlowWire();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource()
	 * @see #getDataFlowWire()
	 * @generated
	 */
	EReference getDataFlowWire_Source();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sink</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink()
	 * @see #getDataFlowWire()
	 * @generated
	 */
	EReference getDataFlowWire_Sink();

	/**
	 * Returns the meta object for the container reference '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent()
	 * @see #getDataFlowWire()
	 * @generated
	 */
	EReference getDataFlowWire_Parent();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire <em>Control Flow Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Flow Wire</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire
	 * @generated
	 */
	EClass getControlFlowWire();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource()
	 * @see #getControlFlowWire()
	 * @generated
	 */
	EReference getControlFlowWire_Source();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sink</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink()
	 * @see #getControlFlowWire()
	 * @generated
	 */
	EReference getControlFlowWire_Sink();

	/**
	 * Returns the meta object for the container reference '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent()
	 * @see #getControlFlowWire()
	 * @generated
	 */
	EReference getControlFlowWire_Parent();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WiresFactory getWiresFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.wires.impl.DataFlowWireImpl <em>Data Flow Wire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.wires.impl.DataFlowWireImpl
		 * @see fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl#getDataFlowWire()
		 * @generated
		 */
		EClass DATA_FLOW_WIRE = eINSTANCE.getDataFlowWire();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_WIRE__SOURCE = eINSTANCE.getDataFlowWire_Source();

		/**
		 * The meta object literal for the '<em><b>Sink</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_WIRE__SINK = eINSTANCE.getDataFlowWire_Sink();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_WIRE__PARENT = eINSTANCE.getDataFlowWire_Parent();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.wires.impl.ControlFlowWireImpl <em>Control Flow Wire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.wires.impl.ControlFlowWireImpl
		 * @see fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl#getControlFlowWire()
		 * @generated
		 */
		EClass CONTROL_FLOW_WIRE = eINSTANCE.getControlFlowWire();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_FLOW_WIRE__SOURCE = eINSTANCE.getControlFlowWire_Source();

		/**
		 * The meta object literal for the '<em><b>Sink</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_FLOW_WIRE__SINK = eINSTANCE.getControlFlowWire_Sink();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_FLOW_WIRE__PARENT = eINSTANCE.getControlFlowWire_Parent();

	}

} //WiresPackage
