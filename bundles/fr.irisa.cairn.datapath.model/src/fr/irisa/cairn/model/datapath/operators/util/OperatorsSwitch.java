/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.MultiCycleBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.PipelinedBlock;
import fr.irisa.cairn.model.datapath.SequentialBlock;
import fr.irisa.cairn.model.datapath.operators.*;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage
 * @generated
 */
public class OperatorsSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OperatorsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsSwitch() {
		if (modelPackage == null) {
			modelPackage = OperatorsPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OperatorsPackage.SINGLE_INPUT_DATA_FLOW_BLOCK: {
				SingleInputDataFlowBlock singleInputDataFlowBlock = (SingleInputDataFlowBlock)theEObject;
				T result = caseSingleInputDataFlowBlock(singleInputDataFlowBlock);
				if (result == null) result = caseDataFlowBlock(singleInputDataFlowBlock);
				if (result == null) result = caseAbstractBlock(singleInputDataFlowBlock);
				if (result == null) result = caseNamedElement(singleInputDataFlowBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SINGLE_OUTPUT_DATA_FLOW_BLOCK: {
				SingleOutputDataFlowBlock singleOutputDataFlowBlock = (SingleOutputDataFlowBlock)theEObject;
				T result = caseSingleOutputDataFlowBlock(singleOutputDataFlowBlock);
				if (result == null) result = caseDataFlowBlock(singleOutputDataFlowBlock);
				if (result == null) result = caseAbstractBlock(singleOutputDataFlowBlock);
				if (result == null) result = caseNamedElement(singleOutputDataFlowBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SINGLE_FLAG_BLOCK: {
				SingleFlagBlock singleFlagBlock = (SingleFlagBlock)theEObject;
				T result = caseSingleFlagBlock(singleFlagBlock);
				if (result == null) result = caseFlagBearerBlock(singleFlagBlock);
				if (result == null) result = caseAbstractBlock(singleFlagBlock);
				if (result == null) result = caseNamedElement(singleFlagBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.SINGLE_CONTROL_PORT_BLOCK: {
				SingleControlPortBlock singleControlPortBlock = (SingleControlPortBlock)theEObject;
				T result = caseSingleControlPortBlock(singleControlPortBlock);
				if (result == null) result = caseActivableBlock(singleControlPortBlock);
				if (result == null) result = caseAbstractBlock(singleControlPortBlock);
				if (result == null) result = caseNamedElement(singleControlPortBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.MULTI_OUTPUT_DATA_FLOW_BLOCK: {
				MultiOutputDataFlowBlock multiOutputDataFlowBlock = (MultiOutputDataFlowBlock)theEObject;
				T result = caseMultiOutputDataFlowBlock(multiOutputDataFlowBlock);
				if (result == null) result = caseDataFlowBlock(multiOutputDataFlowBlock);
				if (result == null) result = caseAbstractBlock(multiOutputDataFlowBlock);
				if (result == null) result = caseNamedElement(multiOutputDataFlowBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.UNARY_OPERATOR: {
				UnaryOperator unaryOperator = (UnaryOperator)theEObject;
				T result = caseUnaryOperator(unaryOperator);
				if (result == null) result = caseCombinationnalOperator(unaryOperator);
				if (result == null) result = caseSingleInputDataFlowBlock(unaryOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(unaryOperator);
				if (result == null) result = caseDataFlowBlock(unaryOperator);
				if (result == null) result = caseCombinationalBlock(unaryOperator);
				if (result == null) result = caseAbstractBlock(unaryOperator);
				if (result == null) result = caseNamedElement(unaryOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.BINARY_OPERATOR: {
				BinaryOperator binaryOperator = (BinaryOperator)theEObject;
				T result = caseBinaryOperator(binaryOperator);
				if (result == null) result = caseCombinationnalOperator(binaryOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(binaryOperator);
				if (result == null) result = caseDataFlowBlock(binaryOperator);
				if (result == null) result = caseCombinationalBlock(binaryOperator);
				if (result == null) result = caseAbstractBlock(binaryOperator);
				if (result == null) result = caseNamedElement(binaryOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.PIPELINED_BINARY_OPERATOR: {
				PipelinedBinaryOperator pipelinedBinaryOperator = (PipelinedBinaryOperator)theEObject;
				T result = casePipelinedBinaryOperator(pipelinedBinaryOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(pipelinedBinaryOperator);
				if (result == null) result = casePipelinedBlock(pipelinedBinaryOperator);
				if (result == null) result = caseDataFlowBlock(pipelinedBinaryOperator);
				if (result == null) result = caseSequentialBlock(pipelinedBinaryOperator);
				if (result == null) result = caseAbstractBlock(pipelinedBinaryOperator);
				if (result == null) result = caseNamedElement(pipelinedBinaryOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.MULTI_CYCLE_BINARY_OPERATOR: {
				MultiCycleBinaryOperator multiCycleBinaryOperator = (MultiCycleBinaryOperator)theEObject;
				T result = caseMultiCycleBinaryOperator(multiCycleBinaryOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(multiCycleBinaryOperator);
				if (result == null) result = caseMultiCycleBlock(multiCycleBinaryOperator);
				if (result == null) result = caseDataFlowBlock(multiCycleBinaryOperator);
				if (result == null) result = caseSequentialBlock(multiCycleBinaryOperator);
				if (result == null) result = caseAbstractBlock(multiCycleBinaryOperator);
				if (result == null) result = caseNamedElement(multiCycleBinaryOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.TERNARY_OPERATOR: {
				TernaryOperator ternaryOperator = (TernaryOperator)theEObject;
				T result = caseTernaryOperator(ternaryOperator);
				if (result == null) result = caseCombinationnalOperator(ternaryOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(ternaryOperator);
				if (result == null) result = caseDataFlowBlock(ternaryOperator);
				if (result == null) result = caseCombinationalBlock(ternaryOperator);
				if (result == null) result = caseAbstractBlock(ternaryOperator);
				if (result == null) result = caseNamedElement(ternaryOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.CONSTANT_VALUE: {
				ConstantValue constantValue = (ConstantValue)theEObject;
				T result = caseConstantValue(constantValue);
				if (result == null) result = caseSingleOutputDataFlowBlock(constantValue);
				if (result == null) result = caseDataFlowBlock(constantValue);
				if (result == null) result = caseAbstractBlock(constantValue);
				if (result == null) result = caseNamedElement(constantValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.DATA_FLOW_MUX: {
				DataFlowMux dataFlowMux = (DataFlowMux)theEObject;
				T result = caseDataFlowMux(dataFlowMux);
				if (result == null) result = caseSingleOutputDataFlowBlock(dataFlowMux);
				if (result == null) result = caseDataFlowBlock(dataFlowMux);
				if (result == null) result = caseAbstractBlock(dataFlowMux);
				if (result == null) result = caseNamedElement(dataFlowMux);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.CONTROL_FLOW_MUX: {
				ControlFlowMux controlFlowMux = (ControlFlowMux)theEObject;
				T result = caseControlFlowMux(controlFlowMux);
				if (result == null) result = caseSingleOutputDataFlowBlock(controlFlowMux);
				if (result == null) result = caseSingleControlPortBlock(controlFlowMux);
				if (result == null) result = caseDataFlowBlock(controlFlowMux);
				if (result == null) result = caseActivableBlock(controlFlowMux);
				if (result == null) result = caseAbstractBlock(controlFlowMux);
				if (result == null) result = caseNamedElement(controlFlowMux);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.REDUCTION_OPERATOR: {
				ReductionOperator reductionOperator = (ReductionOperator)theEObject;
				T result = caseReductionOperator(reductionOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(reductionOperator);
				if (result == null) result = caseDataFlowBlock(reductionOperator);
				if (result == null) result = caseAbstractBlock(reductionOperator);
				if (result == null) result = caseNamedElement(reductionOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.PIPELINED_REDUCTION_OPERATOR: {
				PipelinedReductionOperator pipelinedReductionOperator = (PipelinedReductionOperator)theEObject;
				T result = casePipelinedReductionOperator(pipelinedReductionOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(pipelinedReductionOperator);
				if (result == null) result = casePipelinedBlock(pipelinedReductionOperator);
				if (result == null) result = caseDataFlowBlock(pipelinedReductionOperator);
				if (result == null) result = caseSequentialBlock(pipelinedReductionOperator);
				if (result == null) result = caseAbstractBlock(pipelinedReductionOperator);
				if (result == null) result = caseNamedElement(pipelinedReductionOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.BIT_SELECT: {
				BitSelect bitSelect = (BitSelect)theEObject;
				T result = caseBitSelect(bitSelect);
				if (result == null) result = caseSingleOutputDataFlowBlock(bitSelect);
				if (result == null) result = caseSingleInputDataFlowBlock(bitSelect);
				if (result == null) result = caseDataFlowBlock(bitSelect);
				if (result == null) result = caseAbstractBlock(bitSelect);
				if (result == null) result = caseNamedElement(bitSelect);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.COMPARE: {
				Compare compare = (Compare)theEObject;
				T result = caseCompare(compare);
				if (result == null) result = caseSingleOutputDataFlowBlock(compare);
				if (result == null) result = caseDataFlowBlock(compare);
				if (result == null) result = caseAbstractBlock(compare);
				if (result == null) result = caseNamedElement(compare);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.MERGE: {
				Merge merge = (Merge)theEObject;
				T result = caseMerge(merge);
				if (result == null) result = caseSingleOutputDataFlowBlock(merge);
				if (result == null) result = caseDataFlowBlock(merge);
				if (result == null) result = caseAbstractBlock(merge);
				if (result == null) result = caseNamedElement(merge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.QUANTIZE: {
				Quantize quantize = (Quantize)theEObject;
				T result = caseQuantize(quantize);
				if (result == null) result = caseBitSelect(quantize);
				if (result == null) result = caseSingleOutputDataFlowBlock(quantize);
				if (result == null) result = caseSingleInputDataFlowBlock(quantize);
				if (result == null) result = caseDataFlowBlock(quantize);
				if (result == null) result = caseAbstractBlock(quantize);
				if (result == null) result = caseNamedElement(quantize);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.EXPAND_UNSIGNED: {
				ExpandUnsigned expandUnsigned = (ExpandUnsigned)theEObject;
				T result = caseExpandUnsigned(expandUnsigned);
				if (result == null) result = caseSingleOutputDataFlowBlock(expandUnsigned);
				if (result == null) result = caseSingleInputDataFlowBlock(expandUnsigned);
				if (result == null) result = caseDataFlowBlock(expandUnsigned);
				if (result == null) result = caseAbstractBlock(expandUnsigned);
				if (result == null) result = caseNamedElement(expandUnsigned);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.EXPAND_SIGNED: {
				ExpandSigned expandSigned = (ExpandSigned)theEObject;
				T result = caseExpandSigned(expandSigned);
				if (result == null) result = caseSingleOutputDataFlowBlock(expandSigned);
				if (result == null) result = caseSingleInputDataFlowBlock(expandSigned);
				if (result == null) result = caseDataFlowBlock(expandSigned);
				if (result == null) result = caseAbstractBlock(expandSigned);
				if (result == null) result = caseNamedElement(expandSigned);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.CTRL2_DATA_BUFFER: {
				Ctrl2DataBuffer ctrl2DataBuffer = (Ctrl2DataBuffer)theEObject;
				T result = caseCtrl2DataBuffer(ctrl2DataBuffer);
				if (result == null) result = caseSingleOutputDataFlowBlock(ctrl2DataBuffer);
				if (result == null) result = caseSingleControlPortBlock(ctrl2DataBuffer);
				if (result == null) result = caseActivableBlock(ctrl2DataBuffer);
				if (result == null) result = caseDataFlowBlock(ctrl2DataBuffer);
				if (result == null) result = caseAbstractBlock(ctrl2DataBuffer);
				if (result == null) result = caseNamedElement(ctrl2DataBuffer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.DATA2_CTRL_BUFFER: {
				Data2CtrlBuffer data2CtrlBuffer = (Data2CtrlBuffer)theEObject;
				T result = caseData2CtrlBuffer(data2CtrlBuffer);
				if (result == null) result = caseSingleInputDataFlowBlock(data2CtrlBuffer);
				if (result == null) result = caseSingleFlagBlock(data2CtrlBuffer);
				if (result == null) result = caseFlagBearerBlock(data2CtrlBuffer);
				if (result == null) result = caseDataFlowBlock(data2CtrlBuffer);
				if (result == null) result = caseAbstractBlock(data2CtrlBuffer);
				if (result == null) result = caseNamedElement(data2CtrlBuffer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OperatorsPackage.COMBINATIONNAL_OPERATOR: {
				CombinationnalOperator combinationnalOperator = (CombinationnalOperator)theEObject;
				T result = caseCombinationnalOperator(combinationnalOperator);
				if (result == null) result = caseCombinationalBlock(combinationnalOperator);
				if (result == null) result = caseSingleOutputDataFlowBlock(combinationnalOperator);
				if (result == null) result = caseDataFlowBlock(combinationnalOperator);
				if (result == null) result = caseAbstractBlock(combinationnalOperator);
				if (result == null) result = caseNamedElement(combinationnalOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Input Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Input Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Output Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Output Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Output Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Output Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiOutputDataFlowBlock(MultiOutputDataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryOperator(UnaryOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryOperator(BinaryOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pipelined Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pipelined Binary Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePipelinedBinaryOperator(PipelinedBinaryOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Cycle Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Cycle Binary Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiCycleBinaryOperator(MultiCycleBinaryOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ternary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ternary Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTernaryOperator(TernaryOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantValue(ConstantValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Mux</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Mux</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowMux(DataFlowMux object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Flow Mux</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Flow Mux</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlFlowMux(ControlFlowMux object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reduction Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reduction Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReductionOperator(ReductionOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pipelined Reduction Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pipelined Reduction Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePipelinedReductionOperator(PipelinedReductionOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bit Select</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bit Select</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBitSelect(BitSelect object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compare</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compare</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompare(Compare object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Merge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Merge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMerge(Merge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantize</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantize</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantize(Quantize object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expand Unsigned</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expand Unsigned</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpandUnsigned(ExpandUnsigned object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expand Signed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expand Signed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpandSigned(ExpandSigned object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ctrl2 Data Buffer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ctrl2 Data Buffer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCtrl2DataBuffer(Ctrl2DataBuffer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data2 Ctrl Buffer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data2 Ctrl Buffer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseData2CtrlBuffer(Data2CtrlBuffer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinationnal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinationnal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinationnalOperator(CombinationnalOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Flag Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Flag Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleFlagBlock(SingleFlagBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Control Port Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Control Port Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleControlPortBlock(SingleControlPortBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractBlock(AbstractBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowBlock(DataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivableBlock(ActivableBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinational Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinational Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinationalBlock(CombinationalBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialBlock(SequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pipelined Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pipelined Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePipelinedBlock(PipelinedBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Cycle Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Cycle Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiCycleBlock(MultiCycleBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlagBearerBlock(FlagBearerBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //OperatorsSwitch
