/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mealy Sequential Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getMealySequentialBlock()
 * @model
 * @generated
 */
public interface MealySequentialBlock extends SequentialBlock {
} // MealySequentialBlock
