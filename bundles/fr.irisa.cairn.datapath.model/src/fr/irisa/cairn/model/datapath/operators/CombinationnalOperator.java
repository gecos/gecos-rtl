/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.CombinationalBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Combinationnal Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getCombinationnalOperator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface CombinationnalOperator extends CombinationalBlock, SingleOutputDataFlowBlock {
} // CombinationnalOperator
