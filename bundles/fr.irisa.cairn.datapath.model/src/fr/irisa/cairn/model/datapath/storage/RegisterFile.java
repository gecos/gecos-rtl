/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Register File</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getRegisterFile()
 * @model
 * @generated
 */
public interface RegisterFile extends CERegister {
} // RegisterFile
