/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
// killroy was here
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapathVisitor()
 * @model
 * @generated
 */
public interface DatapathVisitor extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EObject visitOperator(Operator operator, EObject object);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EObject visitDataOutputPad(DataOutputPad operator, EObject object);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EObject visitDataInputPad(DataInputPad operator, EObject object);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	EObject visitControlPad(ControlPad operator, EObject object);

} // DatapathVisitor
