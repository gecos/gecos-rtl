/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Datapath</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getAbstractDatapath()
 * @model abstract="true"
 * @generated
 */
public interface AbstractDatapath extends NamedElement, ActivableBlock, DataFlowBlock, FlagBearerBlock {
} // AbstractDatapath
