/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ctrl2 Data Buffer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl#getActivate <em>Activate</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.Ctrl2DataBufferImpl#getOut <em>Out</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Ctrl2DataBufferImpl extends NamedElementImpl implements Ctrl2DataBuffer {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated NOT
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivate()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> activate;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Ctrl2DataBufferImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorsPackage.Literals.CTRL2_DATA_BUFFER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != OperatorsPackage.CTRL2_DATA_BUFFER__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, OperatorsPackage.CTRL2_DATA_BUFFER__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != OperatorsPackage.CTRL2_DATA_BUFFER__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.CTRL2_DATA_BUFFER__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isCombinational() {
		return COMBINATIONAL_EDEFAULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getActivate() {
		if (activate == null) {
			activate = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE);
		}
		return activate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, OperatorsPackage.CTRL2_DATA_BUFFER__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, OperatorsPackage.CTRL2_DATA_BUFFER__OUT);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControl() {
		return getActivate().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleFlagBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(OutControlPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput() {
		return getOut().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleInputDataFlowBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(InDataPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControlPort(int pos) {
		return getActivate().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				return basicSetParent(null, msgs);
			case OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE:
				return ((InternalEList<?>)getActivate()).basicRemove(otherEnd, msgs);
			case OperatorsPackage.CTRL2_DATA_BUFFER__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case OperatorsPackage.CTRL2_DATA_BUFFER__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				return getParent();
			case OperatorsPackage.CTRL2_DATA_BUFFER__COMBINATIONAL:
				return isCombinational();
			case OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE:
				return getActivate();
			case OperatorsPackage.CTRL2_DATA_BUFFER__IN:
				return getIn();
			case OperatorsPackage.CTRL2_DATA_BUFFER__OUT:
				return getOut();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				setParent((Datapath)newValue);
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE:
				getActivate().clear();
				getActivate().addAll((Collection<? extends InControlPort>)newValue);
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				setParent((Datapath)null);
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE:
				getActivate().clear();
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__IN:
				unsetIn();
				return;
			case OperatorsPackage.CTRL2_DATA_BUFFER__OUT:
				getOut().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorsPackage.CTRL2_DATA_BUFFER__PARENT:
				return getParent() != null;
			case OperatorsPackage.CTRL2_DATA_BUFFER__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case OperatorsPackage.CTRL2_DATA_BUFFER__ACTIVATE:
				return activate != null && !activate.isEmpty();
			case OperatorsPackage.CTRL2_DATA_BUFFER__IN:
				return isSetIn();
			case OperatorsPackage.CTRL2_DATA_BUFFER__OUT:
				return out != null && !out.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == DataFlowBlock.class) {
			switch (derivedFeatureID) {
				case OperatorsPackage.CTRL2_DATA_BUFFER__IN: return DatapathPackage.DATA_FLOW_BLOCK__IN;
				case OperatorsPackage.CTRL2_DATA_BUFFER__OUT: return DatapathPackage.DATA_FLOW_BLOCK__OUT;
				default: return -1;
			}
		}
		if (baseClass == SingleOutputDataFlowBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == DataFlowBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.DATA_FLOW_BLOCK__IN: return OperatorsPackage.CTRL2_DATA_BUFFER__IN;
				case DatapathPackage.DATA_FLOW_BLOCK__OUT: return OperatorsPackage.CTRL2_DATA_BUFFER__OUT;
				default: return -1;
			}
		}
		if (baseClass == SingleOutputDataFlowBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //Ctrl2DataBufferImpl
