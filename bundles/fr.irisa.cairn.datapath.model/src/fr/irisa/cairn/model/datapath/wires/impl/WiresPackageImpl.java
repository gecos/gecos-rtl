/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.HybridWire;
import fr.irisa.cairn.model.datapath.wires.WiresFactory;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.util.WiresValidator;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WiresPackageImpl extends EPackageImpl implements WiresPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFlowWireEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlFlowWireEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WiresPackageImpl() {
		super(eNS_URI, WiresFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link WiresPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WiresPackage init() {
		if (isInited) return (WiresPackage)EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI);

		// Obtain or create and register package
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new WiresPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		theWiresPackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		thePadsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		theWiresPackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		thePadsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theWiresPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return WiresValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theWiresPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WiresPackage.eNS_URI, theWiresPackage);
		return theWiresPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataFlowWire() {
		return dataFlowWireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFlowWire_Source() {
		return (EReference)dataFlowWireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFlowWire_Sink() {
		return (EReference)dataFlowWireEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFlowWire_Parent() {
		return (EReference)dataFlowWireEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlFlowWire() {
		return controlFlowWireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowWire_Source() {
		return (EReference)controlFlowWireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowWire_Sink() {
		return (EReference)controlFlowWireEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getControlFlowWire_Parent() {
		return (EReference)controlFlowWireEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WiresFactory getWiresFactory() {
		return (WiresFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dataFlowWireEClass = createEClass(DATA_FLOW_WIRE);
		createEReference(dataFlowWireEClass, DATA_FLOW_WIRE__SOURCE);
		createEReference(dataFlowWireEClass, DATA_FLOW_WIRE__SINK);
		createEReference(dataFlowWireEClass, DATA_FLOW_WIRE__PARENT);

		controlFlowWireEClass = createEClass(CONTROL_FLOW_WIRE);
		createEReference(controlFlowWireEClass, CONTROL_FLOW_WIRE__SOURCE);
		createEReference(controlFlowWireEClass, CONTROL_FLOW_WIRE__SINK);
		createEReference(controlFlowWireEClass, CONTROL_FLOW_WIRE__PARENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		dataFlowWireEClass.getESuperTypes().add(theDatapathPackage.getWire());
		controlFlowWireEClass.getESuperTypes().add(theDatapathPackage.getWire());

		// Initialize classes and features; add operations and parameters
		initEClass(dataFlowWireEClass, DataFlowWire.class, "DataFlowWire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataFlowWire_Source(), thePortPackage.getOutDataPort(), thePortPackage.getOutDataPort_Wires(), "source", null, 1, 1, DataFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataFlowWire_Sink(), thePortPackage.getInDataPort(), thePortPackage.getInDataPort_Wire(), "sink", null, 1, 1, DataFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataFlowWire_Parent(), theDatapathPackage.getDatapath(), theDatapathPackage.getDatapath_DataWires(), "parent", null, 1, 1, DataFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(controlFlowWireEClass, ControlFlowWire.class, "ControlFlowWire", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getControlFlowWire_Source(), thePortPackage.getOutControlPort(), thePortPackage.getOutControlPort_Wires(), "source", null, 1, 1, ControlFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlFlowWire_Sink(), thePortPackage.getInControlPort(), thePortPackage.getInControlPort_Wire(), "sink", null, 1, 1, ControlFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getControlFlowWire_Parent(), theDatapathPackage.getDatapath(), theDatapathPackage.getDatapath_ControlWires(), "parent", null, 1, 1, ControlFlowWire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (dataFlowWireEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentBitwidth consistentConnection\r\n"
		   });			
		addAnnotation
		  (controlFlowWireEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortType consistentOutputPortType"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";			
		addAnnotation
		  (dataFlowWireEClass, 
		   source, 
		   new String[] {
			 "consistentBiwidth", "sef.source.isOLCDefined() and sef.source.isOLCDefined() implies (self.source.getWidth()=self.sink.getWidth())"
		   });	
	}

} //WiresPackageImpl
