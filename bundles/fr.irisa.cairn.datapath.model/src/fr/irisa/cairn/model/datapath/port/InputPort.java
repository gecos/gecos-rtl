/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import org.eclipse.emf.ecore.EObject;
import fr.irisa.cairn.model.datapath.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInputPort()
 * @model
 * @generated
 */
public interface InputPort extends EObject {

} // InputPort
