/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.Pad;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getInputPad()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface InputPad extends Pad, CombinationalBlock {
} // InputPad
