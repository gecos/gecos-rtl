/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage
 * @generated
 */
public interface OperatorsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorsFactory eINSTANCE = fr.irisa.cairn.model.datapath.operators.impl.OperatorsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Unary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Operator</em>'.
	 * @generated
	 */
	UnaryOperator createUnaryOperator();

	/**
	 * Returns a new object of class '<em>Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Binary Operator</em>'.
	 * @generated
	 */
	BinaryOperator createBinaryOperator();

	/**
	 * Returns a new object of class '<em>Pipelined Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pipelined Binary Operator</em>'.
	 * @generated
	 */
	PipelinedBinaryOperator createPipelinedBinaryOperator();

	/**
	 * Returns a new object of class '<em>Multi Cycle Binary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multi Cycle Binary Operator</em>'.
	 * @generated
	 */
	MultiCycleBinaryOperator createMultiCycleBinaryOperator();

	/**
	 * Returns a new object of class '<em>Ternary Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ternary Operator</em>'.
	 * @generated
	 */
	TernaryOperator createTernaryOperator();

	/**
	 * Returns a new object of class '<em>Constant Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Value</em>'.
	 * @generated
	 */
	ConstantValue createConstantValue();

	/**
	 * Returns a new object of class '<em>Data Flow Mux</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Flow Mux</em>'.
	 * @generated
	 */
	DataFlowMux createDataFlowMux();

	/**
	 * Returns a new object of class '<em>Control Flow Mux</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Control Flow Mux</em>'.
	 * @generated
	 */
	ControlFlowMux createControlFlowMux();

	/**
	 * Returns a new object of class '<em>Reduction Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reduction Operator</em>'.
	 * @generated
	 */
	ReductionOperator createReductionOperator();

	/**
	 * Returns a new object of class '<em>Pipelined Reduction Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pipelined Reduction Operator</em>'.
	 * @generated
	 */
	PipelinedReductionOperator createPipelinedReductionOperator();

	/**
	 * Returns a new object of class '<em>Bit Select</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bit Select</em>'.
	 * @generated
	 */
	BitSelect createBitSelect();

	/**
	 * Returns a new object of class '<em>Compare</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compare</em>'.
	 * @generated
	 */
	Compare createCompare();

	/**
	 * Returns a new object of class '<em>Merge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Merge</em>'.
	 * @generated
	 */
	Merge createMerge();

	/**
	 * Returns a new object of class '<em>Quantize</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quantize</em>'.
	 * @generated
	 */
	Quantize createQuantize();

	/**
	 * Returns a new object of class '<em>Expand Unsigned</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expand Unsigned</em>'.
	 * @generated
	 */
	ExpandUnsigned createExpandUnsigned();

	/**
	 * Returns a new object of class '<em>Expand Signed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expand Signed</em>'.
	 * @generated
	 */
	ExpandSigned createExpandSigned();

	/**
	 * Returns a new object of class '<em>Ctrl2 Data Buffer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ctrl2 Data Buffer</em>'.
	 * @generated
	 */
	Ctrl2DataBuffer createCtrl2DataBuffer();

	/**
	 * Returns a new object of class '<em>Data2 Ctrl Buffer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data2 Ctrl Buffer</em>'.
	 * @generated
	 */
	Data2CtrlBuffer createData2CtrlBuffer();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OperatorsPackage getOperatorsPackage();

} //OperatorsFactory
