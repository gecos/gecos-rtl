/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.impl;

import fr.irisa.cairn.model.datapath.storage.AsyncReadSPRAM;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Async Read SPRAM</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AsyncReadSPRAMImpl extends SinglePortRamImpl implements AsyncReadSPRAM {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AsyncReadSPRAMImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StoragePackage.Literals.ASYNC_READ_SPRAM;
	}

} //AsyncReadSPRAMImpl
