/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expand Signed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.ExpandSigned#getOutputwidth <em>Outputwidth</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getExpandSigned()
 * @model
 * @generated
 */
public interface ExpandSigned extends SingleOutputDataFlowBlock, SingleInputDataFlowBlock {
	/**
	 * Returns the value of the '<em><b>Outputwidth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputwidth</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputwidth</em>' attribute.
	 * @see #setOutputwidth(int)
	 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getExpandSigned_Outputwidth()
	 * @model
	 * @generated
	 */
	int getOutputwidth();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.operators.ExpandSigned#getOutputwidth <em>Outputwidth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outputwidth</em>' attribute.
	 * @see #getOutputwidth()
	 * @generated
	 */
	void setOutputwidth(int value);

} // ExpandSigned
