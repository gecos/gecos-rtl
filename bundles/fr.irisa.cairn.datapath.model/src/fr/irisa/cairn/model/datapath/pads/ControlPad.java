/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getControlPad()
 * @model
 * @generated
 */
public interface ControlPad extends InputPad, SingleFlagBlock {

	/**
	 * Returns the value of the '<em><b>Associated Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Port</em>' reference.
	 * @see #setAssociatedPort(InControlPort)
	 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getControlPad_AssociatedPort()
	 * @see fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad
	 * @model opposite="associatedPad" required="true"
	 * @generated
	 */
	InControlPort getAssociatedPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Port</em>' reference.
	 * @see #getAssociatedPort()
	 * @generated
	 */
	void setAssociatedPort(InControlPort value);

} // ControlPad
