/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Input Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getDataInputPad()
 * @model
 * @generated
 */
public interface DataInputPad extends SingleOutputDataFlowBlock, InputPad {

	/**
	 * Returns the value of the '<em><b>Associated Port</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad <em>Associated Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Port</em>' reference.
	 * @see #setAssociatedPort(InDataPort)
	 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getDataInputPad_AssociatedPort()
	 * @see fr.irisa.cairn.model.datapath.port.InDataPort#getAssociatedPad
	 * @model opposite="associatedPad" required="true"
	 * @generated
	 */
	InDataPort getAssociatedPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.pads.DataInputPad#getAssociatedPort <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Port</em>' reference.
	 * @see #getAssociatedPort()
	 * @generated
	 */
	void setAssociatedPort(InDataPort value);

} // DataInputPad
