/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.AbstractDatapath;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.BlackBoxBlock;
import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.CombinationalDatapath;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathFactory;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;
import fr.irisa.cairn.model.datapath.MooreSequentialBlock;
import fr.irisa.cairn.model.datapath.MultiCycleBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.Operator;
import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.PipelinedBlock;
import fr.irisa.cairn.model.datapath.PipelinedDatapath;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.SequentialBlock;
import fr.irisa.cairn.model.datapath.SequentialDatapath;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DatapathPackageImpl extends EPackageImpl implements DatapathPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractDatapathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass datapathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activableBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFlowBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flagBearerBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass wireEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass combinationalDatapathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass combinationalBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pipelinedBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiCycleBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass padEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pipelinedDatapathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequentialDatapathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass blackBoxBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mooreSequentialBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mealySequentialBlockEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DatapathPackageImpl() {
		super(eNS_URI, DatapathFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DatapathPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DatapathPackage init() {
		if (isInited) return (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);

		// Obtain or create and register package
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DatapathPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		thePadsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		thePadsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();


		// Mark meta-data to indicate it can't be changed
		theDatapathPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DatapathPackage.eNS_URI, theDatapathPackage);
		return theDatapathPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractDatapath() {
		return abstractDatapathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDatapath() {
		return datapathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Components() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_DataWires() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_ControlWires() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Library() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Wires() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Registers() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Memblocks() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Operators() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatapath_Fsms() {
		return (EReference)datapathEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractBlock() {
		return abstractBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractBlock_Parent() {
		return (EReference)abstractBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractBlock_Combinational() {
		return (EAttribute)abstractBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivableBlock() {
		return activableBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActivableBlock_Activate() {
		return (EReference)activableBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataFlowBlock() {
		return dataFlowBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFlowBlock_In() {
		return (EReference)dataFlowBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataFlowBlock_Out() {
		return (EReference)dataFlowBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlagBearerBlock() {
		return flagBearerBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFlagBearerBlock_Flags() {
		return (EReference)flagBearerBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWire() {
		return wireEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWire_Width() {
		return (EAttribute)wireEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort() {
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPort_Width() {
		return (EAttribute)portEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperator() {
		return operatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCombinationalDatapath() {
		return combinationalDatapathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCombinationalBlock() {
		return combinationalBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialBlock() {
		return sequentialBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPipelinedBlock() {
		return pipelinedBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPipelinedBlock_Latency() {
		return (EAttribute)pipelinedBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiCycleBlock() {
		return multiCycleBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiCycleBlock_Latency() {
		return (EAttribute)multiCycleBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiCycleBlock_Throughput() {
		return (EAttribute)multiCycleBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPad() {
		return padEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPipelinedDatapath() {
		return pipelinedDatapathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequentialDatapath() {
		return sequentialDatapathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBlackBoxBlock() {
		return blackBoxBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBlackBoxBlock_Implementation() {
		return (EReference)blackBoxBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMooreSequentialBlock() {
		return mooreSequentialBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMealySequentialBlock() {
		return mealySequentialBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatapathFactory getDatapathFactory() {
		return (DatapathFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		abstractDatapathEClass = createEClass(ABSTRACT_DATAPATH);

		datapathEClass = createEClass(DATAPATH);
		createEReference(datapathEClass, DATAPATH__COMPONENTS);
		createEReference(datapathEClass, DATAPATH__DATA_WIRES);
		createEReference(datapathEClass, DATAPATH__CONTROL_WIRES);
		createEReference(datapathEClass, DATAPATH__LIBRARY);
		createEReference(datapathEClass, DATAPATH__WIRES);
		createEReference(datapathEClass, DATAPATH__REGISTERS);
		createEReference(datapathEClass, DATAPATH__MEMBLOCKS);
		createEReference(datapathEClass, DATAPATH__OPERATORS);
		createEReference(datapathEClass, DATAPATH__FSMS);

		abstractBlockEClass = createEClass(ABSTRACT_BLOCK);
		createEReference(abstractBlockEClass, ABSTRACT_BLOCK__PARENT);
		createEAttribute(abstractBlockEClass, ABSTRACT_BLOCK__COMBINATIONAL);

		activableBlockEClass = createEClass(ACTIVABLE_BLOCK);
		createEReference(activableBlockEClass, ACTIVABLE_BLOCK__ACTIVATE);

		dataFlowBlockEClass = createEClass(DATA_FLOW_BLOCK);
		createEReference(dataFlowBlockEClass, DATA_FLOW_BLOCK__IN);
		createEReference(dataFlowBlockEClass, DATA_FLOW_BLOCK__OUT);

		flagBearerBlockEClass = createEClass(FLAG_BEARER_BLOCK);
		createEReference(flagBearerBlockEClass, FLAG_BEARER_BLOCK__FLAGS);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		wireEClass = createEClass(WIRE);
		createEAttribute(wireEClass, WIRE__WIDTH);

		portEClass = createEClass(PORT);
		createEAttribute(portEClass, PORT__WIDTH);

		operatorEClass = createEClass(OPERATOR);

		combinationalDatapathEClass = createEClass(COMBINATIONAL_DATAPATH);

		combinationalBlockEClass = createEClass(COMBINATIONAL_BLOCK);

		sequentialBlockEClass = createEClass(SEQUENTIAL_BLOCK);

		pipelinedBlockEClass = createEClass(PIPELINED_BLOCK);
		createEAttribute(pipelinedBlockEClass, PIPELINED_BLOCK__LATENCY);

		multiCycleBlockEClass = createEClass(MULTI_CYCLE_BLOCK);
		createEAttribute(multiCycleBlockEClass, MULTI_CYCLE_BLOCK__LATENCY);
		createEAttribute(multiCycleBlockEClass, MULTI_CYCLE_BLOCK__THROUGHPUT);

		padEClass = createEClass(PAD);

		pipelinedDatapathEClass = createEClass(PIPELINED_DATAPATH);

		sequentialDatapathEClass = createEClass(SEQUENTIAL_DATAPATH);

		blackBoxBlockEClass = createEClass(BLACK_BOX_BLOCK);
		createEReference(blackBoxBlockEClass, BLACK_BOX_BLOCK__IMPLEMENTATION);

		mooreSequentialBlockEClass = createEClass(MOORE_SEQUENTIAL_BLOCK);

		mealySequentialBlockEClass = createEClass(MEALY_SEQUENTIAL_BLOCK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
		WiresPackage theWiresPackage = (WiresPackage)EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI);
		StoragePackage theStoragePackage = (StoragePackage)EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI);
		OperatorsPackage theOperatorsPackage = (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		PadsPackage thePadsPackage = (PadsPackage)EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI);
		FsmPackage theFsmPackage = (FsmPackage)EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(thePortPackage);
		getESubpackages().add(theWiresPackage);
		getESubpackages().add(theStoragePackage);
		getESubpackages().add(theOperatorsPackage);
		getESubpackages().add(thePadsPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		abstractDatapathEClass.getESuperTypes().add(this.getNamedElement());
		abstractDatapathEClass.getESuperTypes().add(this.getActivableBlock());
		abstractDatapathEClass.getESuperTypes().add(this.getDataFlowBlock());
		abstractDatapathEClass.getESuperTypes().add(this.getFlagBearerBlock());
		datapathEClass.getESuperTypes().add(this.getNamedElement());
		datapathEClass.getESuperTypes().add(this.getActivableBlock());
		datapathEClass.getESuperTypes().add(this.getDataFlowBlock());
		datapathEClass.getESuperTypes().add(this.getFlagBearerBlock());
		abstractBlockEClass.getESuperTypes().add(this.getNamedElement());
		activableBlockEClass.getESuperTypes().add(this.getAbstractBlock());
		dataFlowBlockEClass.getESuperTypes().add(this.getAbstractBlock());
		flagBearerBlockEClass.getESuperTypes().add(this.getAbstractBlock());
		wireEClass.getESuperTypes().add(this.getNamedElement());
		portEClass.getESuperTypes().add(this.getNamedElement());
		operatorEClass.getESuperTypes().add(this.getDataFlowBlock());
		combinationalDatapathEClass.getESuperTypes().add(this.getDatapath());
		combinationalDatapathEClass.getESuperTypes().add(this.getCombinationalBlock());
		pipelinedBlockEClass.getESuperTypes().add(this.getSequentialBlock());
		multiCycleBlockEClass.getESuperTypes().add(this.getSequentialBlock());
		padEClass.getESuperTypes().add(this.getAbstractBlock());
		pipelinedDatapathEClass.getESuperTypes().add(this.getDatapath());
		pipelinedDatapathEClass.getESuperTypes().add(this.getPipelinedBlock());
		sequentialDatapathEClass.getESuperTypes().add(this.getDatapath());
		sequentialDatapathEClass.getESuperTypes().add(this.getSequentialBlock());
		blackBoxBlockEClass.getESuperTypes().add(this.getAbstractDatapath());
		mooreSequentialBlockEClass.getESuperTypes().add(this.getSequentialBlock());
		mealySequentialBlockEClass.getESuperTypes().add(this.getSequentialBlock());

		// Initialize classes and features; add operations and parameters
		initEClass(abstractDatapathEClass, AbstractDatapath.class, "AbstractDatapath", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(datapathEClass, Datapath.class, "Datapath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDatapath_Components(), this.getAbstractBlock(), this.getAbstractBlock_Parent(), "components", null, 1, -1, Datapath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_DataWires(), theWiresPackage.getDataFlowWire(), theWiresPackage.getDataFlowWire_Parent(), "dataWires", null, 0, -1, Datapath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_ControlWires(), theWiresPackage.getControlFlowWire(), theWiresPackage.getControlFlowWire_Parent(), "controlWires", null, 0, -1, Datapath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Library(), this.getAbstractBlock(), null, "library", null, 1, -1, Datapath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Wires(), this.getWire(), null, "wires", null, 0, -1, Datapath.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Registers(), theStoragePackage.getRegister(), null, "registers", null, 0, -1, Datapath.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Memblocks(), this.getAbstractBlock(), null, "memblocks", null, 0, -1, Datapath.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Operators(), this.getCombinationalBlock(), null, "operators", null, 0, -1, Datapath.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getDatapath_Fsms(), theFsmPackage.getFSM(), null, "fsms", null, 0, -1, Datapath.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(abstractBlockEClass, AbstractBlock.class, "AbstractBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractBlock_Parent(), this.getDatapath(), this.getDatapath_Components(), "parent", null, 0, 1, AbstractBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractBlock_Combinational(), ecorePackage.getEBoolean(), "combinational", "false", 1, 1, AbstractBlock.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(activableBlockEClass, ActivableBlock.class, "ActivableBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActivableBlock_Activate(), thePortPackage.getInControlPort(), null, "activate", null, 0, -1, ActivableBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(activableBlockEClass, thePortPackage.getInControlPort(), "getControlPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pos", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dataFlowBlockEClass, DataFlowBlock.class, "DataFlowBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataFlowBlock_In(), thePortPackage.getInDataPort(), null, "in", null, 0, -1, DataFlowBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataFlowBlock_Out(), thePortPackage.getOutDataPort(), null, "out", null, 0, -1, DataFlowBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(dataFlowBlockEClass, thePortPackage.getInDataPort(), "getInput", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pos", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(dataFlowBlockEClass, thePortPackage.getOutDataPort(), "getOutput", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pos", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(flagBearerBlockEClass, FlagBearerBlock.class, "FlagBearerBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFlagBearerBlock_Flags(), thePortPackage.getOutControlPort(), null, "flags", null, 0, -1, FlagBearerBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(flagBearerBlockEClass, thePortPackage.getOutControlPort(), "getFlag", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "pos", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wireEClass, Wire.class, "Wire", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWire_Width(), ecorePackage.getEInt(), "width", "1", 1, 1, Wire.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPort_Width(), ecorePackage.getEInt(), "width", "1", 0, 1, Port.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operatorEClass, Operator.class, "Operator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(combinationalDatapathEClass, CombinationalDatapath.class, "CombinationalDatapath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(combinationalBlockEClass, CombinationalBlock.class, "CombinationalBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sequentialBlockEClass, SequentialBlock.class, "SequentialBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pipelinedBlockEClass, PipelinedBlock.class, "PipelinedBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPipelinedBlock_Latency(), ecorePackage.getEInt(), "latency", null, 0, 1, PipelinedBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiCycleBlockEClass, MultiCycleBlock.class, "MultiCycleBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiCycleBlock_Latency(), ecorePackage.getEInt(), "latency", null, 0, 1, MultiCycleBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMultiCycleBlock_Throughput(), ecorePackage.getEInt(), "throughput", null, 0, 1, MultiCycleBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(padEClass, Pad.class, "Pad", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pipelinedDatapathEClass, PipelinedDatapath.class, "PipelinedDatapath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(sequentialDatapathEClass, SequentialDatapath.class, "SequentialDatapath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(blackBoxBlockEClass, BlackBoxBlock.class, "BlackBoxBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBlackBoxBlock_Implementation(), this.getDatapath(), null, "implementation", null, 0, 1, BlackBoxBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mooreSequentialBlockEClass, MooreSequentialBlock.class, "MooreSequentialBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mealySequentialBlockEClass, MealySequentialBlock.class, "MealySequentialBlock", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
		// null
		createNullAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (datapathEClass, 
		   source, 
		   new String[] {
			 "constraints", "noCombinationalCycles\r\n"
		   });				
		addAnnotation
		  (abstractBlockEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortWidth consistentOutputPortWidth hasValidSources"
		   });		
		addAnnotation
		  (activableBlockEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortWidth consistentOutputPortWidth hasValidSources"
		   });						
		addAnnotation
		  (flagBearerBlockEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortWidth consistentOutputPortWidth hasValidSources"
		   });					
		addAnnotation
		  (operatorEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentBitwidth\r\n"
		   });			
		addAnnotation
		  (pipelinedBlockEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortWidth consistentOutputPortWidth hasValidSources"
		   });			
		addAnnotation
		  (multiCycleBlockEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentInputPortWidth consistentOutputPortWidth hasValidSources"
		   });	
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";			
		addAnnotation
		  (datapathEClass, 
		   source, 
		   new String[] {
			 "consistentPortTypes", "in.select(port | not port.instandceOf(InDataPad)).size()=0 \r\nand\r\nout.select(port | not port.instandceOf(OutDataPad)).size()=0 \r\nand \r\nactivate.select(port | not port.instandceOf(InControlPad)).size()=0\r\nand \r\nflags.select(port | not port.instandceOf(OutControlPad)).size()=0\r\n",
			 "noCombinationalCycles", "false",
			 "nonNegativeNumberOfComponents ", "components.size()>0"
		   });		
		addAnnotation
		  (abstractBlockEClass, 
		   source, 
		   new String[] {
			 "noCombinationalCycles", ""
		   });				
		addAnnotation
		  (activableBlockEClass, 
		   source, 
		   new String[] {
			 "noCombinationalCycles", ""
		   });						
		addAnnotation
		  (flagBearerBlockEClass, 
		   source, 
		   new String[] {
			 "noCombinationalCycles", ""
		   });					
		addAnnotation
		  (operatorEClass, 
		   source, 
		   new String[] {
			 "consistentBitwidth", "self.width>0"
		   });			
		addAnnotation
		  (pipelinedBlockEClass, 
		   source, 
		   new String[] {
			 "noCombinationalCycles", ""
		   });			
		addAnnotation
		  (multiCycleBlockEClass, 
		   source, 
		   new String[] {
			 "noCombinationalCycles", ""
		   });
	}

	/**
	 * Initializes the annotations for <b>null</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createNullAnnotations() {
		String source = null;														
		addAnnotation
		  (namedElementEClass, 
		   source, 
		   new String[] {
		   });						
	}

} //DatapathPackageImpl
