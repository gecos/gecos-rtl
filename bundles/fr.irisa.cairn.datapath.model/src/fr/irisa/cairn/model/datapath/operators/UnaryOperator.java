/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.Operator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.UnaryOperator#getOpcode <em>Opcode</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getUnaryOperator()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints=' consistentPortNumber'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentPortNumber='self.in.size()=2 and self.out.size()=1 '"
 *        annotation="GMFMapping type='NodeWithPorts' label='{opcode,label}' ports='in.north'"
 * @generated
 */
public interface UnaryOperator extends SingleOutputDataFlowBlock, CombinationnalOperator, SingleInputDataFlowBlock {
	/**
	 * Returns the value of the '<em><b>Opcode</b></em>' attribute.
	 * The default value is <code>"\"add\""</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.model.datapath.operators.UnaryOpcode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOpcode
	 * @see #setOpcode(UnaryOpcode)
	 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getUnaryOperator_Opcode()
	 * @model default="\"add\"" required="true"
	 * @generated
	 */
	UnaryOpcode getOpcode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.operators.UnaryOperator#getOpcode <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.UnaryOpcode
	 * @see #getOpcode()
	 * @generated
	 */
	void setOpcode(UnaryOpcode value);

} // UnaryOperator
