/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.CombinationnalOperator;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.MultiCycleBinaryOperator;
import fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.OperatorsFactory;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.PipelinedBinaryOperator;
import fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OperatorsPackageImpl extends EPackageImpl implements OperatorsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleInputDataFlowBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleOutputDataFlowBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiOutputDataFlowBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pipelinedBinaryOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiCycleBinaryOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ternaryOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataFlowMuxEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass controlFlowMuxEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reductionOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pipelinedReductionOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bitSelectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compareEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mergeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quantizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expandUnsignedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expandSignedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ctrl2DataBufferEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass data2CtrlBufferEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass combinationnalOperatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleFlagBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleControlPortBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unaryOpcodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum binaryOpcodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ternaryOpcodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum reductionOpcodeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum compareOpcodeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OperatorsPackageImpl() {
		super(eNS_URI, OperatorsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OperatorsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OperatorsPackage init() {
		if (isInited) return (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);

		// Obtain or create and register package
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OperatorsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		theOperatorsPackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		thePadsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		theOperatorsPackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		thePadsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();

			// Mark meta-data to indicate it can't be changed
		theOperatorsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OperatorsPackage.eNS_URI, theOperatorsPackage);
		return theOperatorsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleInputDataFlowBlock() {
		return singleInputDataFlowBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleOutputDataFlowBlock() {
		return singleOutputDataFlowBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiOutputDataFlowBlock() {
		return multiOutputDataFlowBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnaryOperator() {
		return unaryOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnaryOperator_Opcode() {
		return (EAttribute)unaryOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryOperator() {
		return binaryOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryOperator_Opcode() {
		return (EAttribute)binaryOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPipelinedBinaryOperator() {
		return pipelinedBinaryOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPipelinedBinaryOperator_Opcode() {
		return (EAttribute)pipelinedBinaryOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiCycleBinaryOperator() {
		return multiCycleBinaryOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiCycleBinaryOperator_Opcode() {
		return (EAttribute)multiCycleBinaryOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTernaryOperator() {
		return ternaryOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTernaryOperator_Opcode() {
		return (EAttribute)ternaryOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstantValue() {
		return constantValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstantValue_Value() {
		return (EAttribute)constantValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataFlowMux() {
		return dataFlowMuxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getControlFlowMux() {
		return controlFlowMuxEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReductionOperator() {
		return reductionOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReductionOperator_Opcode() {
		return (EAttribute)reductionOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPipelinedReductionOperator() {
		return pipelinedReductionOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPipelinedReductionOperator_Opcode() {
		return (EAttribute)pipelinedReductionOperatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBitSelect() {
		return bitSelectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBitSelect_LowerBound() {
		return (EAttribute)bitSelectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBitSelect_UpperBound() {
		return (EAttribute)bitSelectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompare() {
		return compareEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompare_Opcode() {
		return (EAttribute)compareEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMerge() {
		return mergeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuantize() {
		return quantizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpandUnsigned() {
		return expandUnsignedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpandUnsigned_Outputwidth() {
		return (EAttribute)expandUnsignedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpandSigned() {
		return expandSignedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExpandSigned_Outputwidth() {
		return (EAttribute)expandSignedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCtrl2DataBuffer() {
		return ctrl2DataBufferEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getData2CtrlBuffer() {
		return data2CtrlBufferEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCombinationnalOperator() {
		return combinationnalOperatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleFlagBlock() {
		return singleFlagBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleControlPortBlock() {
		return singleControlPortBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUnaryOpcode() {
		return unaryOpcodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBinaryOpcode() {
		return binaryOpcodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTernaryOpcode() {
		return ternaryOpcodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReductionOpcode() {
		return reductionOpcodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCompareOpcode() {
		return compareOpcodeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperatorsFactory getOperatorsFactory() {
		return (OperatorsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		singleInputDataFlowBlockEClass = createEClass(SINGLE_INPUT_DATA_FLOW_BLOCK);

		singleOutputDataFlowBlockEClass = createEClass(SINGLE_OUTPUT_DATA_FLOW_BLOCK);

		singleFlagBlockEClass = createEClass(SINGLE_FLAG_BLOCK);

		singleControlPortBlockEClass = createEClass(SINGLE_CONTROL_PORT_BLOCK);

		multiOutputDataFlowBlockEClass = createEClass(MULTI_OUTPUT_DATA_FLOW_BLOCK);

		unaryOperatorEClass = createEClass(UNARY_OPERATOR);
		createEAttribute(unaryOperatorEClass, UNARY_OPERATOR__OPCODE);

		binaryOperatorEClass = createEClass(BINARY_OPERATOR);
		createEAttribute(binaryOperatorEClass, BINARY_OPERATOR__OPCODE);

		pipelinedBinaryOperatorEClass = createEClass(PIPELINED_BINARY_OPERATOR);
		createEAttribute(pipelinedBinaryOperatorEClass, PIPELINED_BINARY_OPERATOR__OPCODE);

		multiCycleBinaryOperatorEClass = createEClass(MULTI_CYCLE_BINARY_OPERATOR);
		createEAttribute(multiCycleBinaryOperatorEClass, MULTI_CYCLE_BINARY_OPERATOR__OPCODE);

		ternaryOperatorEClass = createEClass(TERNARY_OPERATOR);
		createEAttribute(ternaryOperatorEClass, TERNARY_OPERATOR__OPCODE);

		constantValueEClass = createEClass(CONSTANT_VALUE);
		createEAttribute(constantValueEClass, CONSTANT_VALUE__VALUE);

		dataFlowMuxEClass = createEClass(DATA_FLOW_MUX);

		controlFlowMuxEClass = createEClass(CONTROL_FLOW_MUX);

		reductionOperatorEClass = createEClass(REDUCTION_OPERATOR);
		createEAttribute(reductionOperatorEClass, REDUCTION_OPERATOR__OPCODE);

		pipelinedReductionOperatorEClass = createEClass(PIPELINED_REDUCTION_OPERATOR);
		createEAttribute(pipelinedReductionOperatorEClass, PIPELINED_REDUCTION_OPERATOR__OPCODE);

		bitSelectEClass = createEClass(BIT_SELECT);
		createEAttribute(bitSelectEClass, BIT_SELECT__LOWER_BOUND);
		createEAttribute(bitSelectEClass, BIT_SELECT__UPPER_BOUND);

		compareEClass = createEClass(COMPARE);
		createEAttribute(compareEClass, COMPARE__OPCODE);

		mergeEClass = createEClass(MERGE);

		quantizeEClass = createEClass(QUANTIZE);

		expandUnsignedEClass = createEClass(EXPAND_UNSIGNED);
		createEAttribute(expandUnsignedEClass, EXPAND_UNSIGNED__OUTPUTWIDTH);

		expandSignedEClass = createEClass(EXPAND_SIGNED);
		createEAttribute(expandSignedEClass, EXPAND_SIGNED__OUTPUTWIDTH);

		ctrl2DataBufferEClass = createEClass(CTRL2_DATA_BUFFER);

		data2CtrlBufferEClass = createEClass(DATA2_CTRL_BUFFER);

		combinationnalOperatorEClass = createEClass(COMBINATIONNAL_OPERATOR);

		// Create enums
		unaryOpcodeEEnum = createEEnum(UNARY_OPCODE);
		binaryOpcodeEEnum = createEEnum(BINARY_OPCODE);
		ternaryOpcodeEEnum = createEEnum(TERNARY_OPCODE);
		reductionOpcodeEEnum = createEEnum(REDUCTION_OPCODE);
		compareOpcodeEEnum = createEEnum(COMPARE_OPCODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
		WiresPackage theWiresPackage = (WiresPackage)EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		singleInputDataFlowBlockEClass.getESuperTypes().add(theDatapathPackage.getDataFlowBlock());
		singleOutputDataFlowBlockEClass.getESuperTypes().add(theDatapathPackage.getDataFlowBlock());
		singleFlagBlockEClass.getESuperTypes().add(theDatapathPackage.getFlagBearerBlock());
		singleControlPortBlockEClass.getESuperTypes().add(theDatapathPackage.getActivableBlock());
		multiOutputDataFlowBlockEClass.getESuperTypes().add(theDatapathPackage.getDataFlowBlock());
		unaryOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		unaryOperatorEClass.getESuperTypes().add(this.getCombinationnalOperator());
		unaryOperatorEClass.getESuperTypes().add(this.getSingleInputDataFlowBlock());
		binaryOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		binaryOperatorEClass.getESuperTypes().add(this.getCombinationnalOperator());
		pipelinedBinaryOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		pipelinedBinaryOperatorEClass.getESuperTypes().add(theDatapathPackage.getPipelinedBlock());
		multiCycleBinaryOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		multiCycleBinaryOperatorEClass.getESuperTypes().add(theDatapathPackage.getMultiCycleBlock());
		ternaryOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		ternaryOperatorEClass.getESuperTypes().add(this.getCombinationnalOperator());
		constantValueEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		dataFlowMuxEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		controlFlowMuxEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		controlFlowMuxEClass.getESuperTypes().add(this.getSingleControlPortBlock());
		reductionOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		pipelinedReductionOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		pipelinedReductionOperatorEClass.getESuperTypes().add(theDatapathPackage.getPipelinedBlock());
		bitSelectEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		bitSelectEClass.getESuperTypes().add(this.getSingleInputDataFlowBlock());
		compareEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		mergeEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		quantizeEClass.getESuperTypes().add(this.getBitSelect());
		expandUnsignedEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		expandUnsignedEClass.getESuperTypes().add(this.getSingleInputDataFlowBlock());
		expandSignedEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		expandSignedEClass.getESuperTypes().add(this.getSingleInputDataFlowBlock());
		ctrl2DataBufferEClass.getESuperTypes().add(theDatapathPackage.getActivableBlock());
		ctrl2DataBufferEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());
		ctrl2DataBufferEClass.getESuperTypes().add(this.getSingleControlPortBlock());
		data2CtrlBufferEClass.getESuperTypes().add(theDatapathPackage.getFlagBearerBlock());
		data2CtrlBufferEClass.getESuperTypes().add(this.getSingleInputDataFlowBlock());
		data2CtrlBufferEClass.getESuperTypes().add(this.getSingleFlagBlock());
		combinationnalOperatorEClass.getESuperTypes().add(theDatapathPackage.getCombinationalBlock());
		combinationnalOperatorEClass.getESuperTypes().add(this.getSingleOutputDataFlowBlock());

		// Initialize classes and features; add operations and parameters
		initEClass(singleInputDataFlowBlockEClass, SingleInputDataFlowBlock.class, "SingleInputDataFlowBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singleInputDataFlowBlockEClass, thePortPackage.getInDataPort(), "getInput", 0, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(singleInputDataFlowBlockEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleOutputDataFlowBlock(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleInputDataFlowBlockEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePortPackage.getOutDataPort(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singleInputDataFlowBlockEClass, theDatapathPackage.getAbstractBlock(), "getSourceBlock", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(singleOutputDataFlowBlockEClass, SingleOutputDataFlowBlock.class, "SingleOutputDataFlowBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singleOutputDataFlowBlockEClass, thePortPackage.getOutDataPort(), "getOutput", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleOutputDataFlowBlockEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleInputDataFlowBlock(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleOutputDataFlowBlockEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePortPackage.getInDataPort(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(singleFlagBlockEClass, SingleFlagBlock.class, "SingleFlagBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singleFlagBlockEClass, thePortPackage.getOutControlPort(), "getFlag", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleFlagBlockEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleControlPortBlock(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleFlagBlockEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePortPackage.getInControlPort(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(singleControlPortBlockEClass, SingleControlPortBlock.class, "SingleControlPortBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singleControlPortBlockEClass, thePortPackage.getInControlPort(), "getControl", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleControlPortBlockEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleFlagBlock(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(singleControlPortBlockEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, thePortPackage.getOutControlPort(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(multiOutputDataFlowBlockEClass, MultiOutputDataFlowBlock.class, "MultiOutputDataFlowBlock", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(unaryOperatorEClass, UnaryOperator.class, "UnaryOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnaryOperator_Opcode(), this.getUnaryOpcode(), "opcode", "\"add\"", 1, 1, UnaryOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(binaryOperatorEClass, BinaryOperator.class, "BinaryOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBinaryOperator_Opcode(), this.getBinaryOpcode(), "opcode", "\"add\"", 1, 1, BinaryOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pipelinedBinaryOperatorEClass, PipelinedBinaryOperator.class, "PipelinedBinaryOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPipelinedBinaryOperator_Opcode(), this.getBinaryOpcode(), "opcode", "\"add\"", 1, 1, PipelinedBinaryOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multiCycleBinaryOperatorEClass, MultiCycleBinaryOperator.class, "MultiCycleBinaryOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiCycleBinaryOperator_Opcode(), this.getBinaryOpcode(), "opcode", "\"add\"", 1, 1, MultiCycleBinaryOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ternaryOperatorEClass, TernaryOperator.class, "TernaryOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTernaryOperator_Opcode(), this.getTernaryOpcode(), "opcode", "\"add\"", 1, 1, TernaryOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constantValueEClass, ConstantValue.class, "ConstantValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstantValue_Value(), ecorePackage.getEInt(), "value", null, 1, 1, ConstantValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataFlowMuxEClass, DataFlowMux.class, "DataFlowMux", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(controlFlowMuxEClass, ControlFlowMux.class, "ControlFlowMux", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(reductionOperatorEClass, ReductionOperator.class, "ReductionOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReductionOperator_Opcode(), this.getReductionOpcode(), "opcode", "\"add\"", 1, 1, ReductionOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pipelinedReductionOperatorEClass, PipelinedReductionOperator.class, "PipelinedReductionOperator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPipelinedReductionOperator_Opcode(), this.getReductionOpcode(), "opcode", "\"add\"", 1, 1, PipelinedReductionOperator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bitSelectEClass, BitSelect.class, "BitSelect", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBitSelect_LowerBound(), ecorePackage.getEInt(), "lowerBound", null, 1, 1, BitSelect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBitSelect_UpperBound(), ecorePackage.getEInt(), "upperBound", null, 1, 1, BitSelect.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compareEClass, Compare.class, "Compare", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCompare_Opcode(), this.getCompareOpcode(), "opcode", null, 1, 1, Compare.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mergeEClass, Merge.class, "Merge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(quantizeEClass, Quantize.class, "Quantize", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(expandUnsignedEClass, ExpandUnsigned.class, "ExpandUnsigned", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpandUnsigned_Outputwidth(), ecorePackage.getEInt(), "outputwidth", null, 0, 1, ExpandUnsigned.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(expandSignedEClass, ExpandSigned.class, "ExpandSigned", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExpandSigned_Outputwidth(), ecorePackage.getEInt(), "outputwidth", null, 0, 1, ExpandSigned.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ctrl2DataBufferEClass, Ctrl2DataBuffer.class, "Ctrl2DataBuffer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(data2CtrlBufferEClass, Data2CtrlBuffer.class, "Data2CtrlBuffer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(combinationnalOperatorEClass, CombinationnalOperator.class, "CombinationnalOperator", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(unaryOpcodeEEnum, UnaryOpcode.class, "UnaryOpcode");
		addEEnumLiteral(unaryOpcodeEEnum, UnaryOpcode.SQRT);
		addEEnumLiteral(unaryOpcodeEEnum, UnaryOpcode.INV);
		addEEnumLiteral(unaryOpcodeEEnum, UnaryOpcode.NEG);
		addEEnumLiteral(unaryOpcodeEEnum, UnaryOpcode.NOT);
		addEEnumLiteral(unaryOpcodeEEnum, UnaryOpcode.IDENTITY);

		initEEnum(binaryOpcodeEEnum, BinaryOpcode.class, "BinaryOpcode");
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.ADDU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MULU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.SUBU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.DIVU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MAXU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MINU);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MUX);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.SHL);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.SHR);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.XOR);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.AND);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.NAND);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.ADD);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MUL);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.SUB);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.DIV);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MAX);
		addEEnumLiteral(binaryOpcodeEEnum, BinaryOpcode.MIN);

		initEEnum(ternaryOpcodeEEnum, TernaryOpcode.class, "TernaryOpcode");
		addEEnumLiteral(ternaryOpcodeEEnum, TernaryOpcode.MULADD);
		addEEnumLiteral(ternaryOpcodeEEnum, TernaryOpcode.ADDC);

		initEEnum(reductionOpcodeEEnum, ReductionOpcode.class, "ReductionOpcode");
		addEEnumLiteral(reductionOpcodeEEnum, ReductionOpcode.PROD);
		addEEnumLiteral(reductionOpcodeEEnum, ReductionOpcode.SIGMA);
		addEEnumLiteral(reductionOpcodeEEnum, ReductionOpcode.MAX);
		addEEnumLiteral(reductionOpcodeEEnum, ReductionOpcode.MIN);

		initEEnum(compareOpcodeEEnum, CompareOpcode.class, "CompareOpcode");
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.EQU);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.GT);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.LT);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.NEQ);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.LTE);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.GTE);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.ZERO);
		addEEnumLiteral(compareOpcodeEEnum, CompareOpcode.NEG);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
		// GMFMapping
		createGMFMappingAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";															
		addAnnotation
		  (unaryOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber"
		   });				
		addAnnotation
		  (binaryOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber consistentBitwidth"
		   });			
		addAnnotation
		  (pipelinedBinaryOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber consistentBitwidth"
		   });			
		addAnnotation
		  (multiCycleBinaryOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber consistentBitwidth"
		   });			
		addAnnotation
		  (ternaryOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber bitwidthConstraint"
		   });			
		addAnnotation
		  (reductionOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber"
		   });			
		addAnnotation
		  (pipelinedReductionOperatorEClass, 
		   source, 
		   new String[] {
			 "constraints", " consistentPortNumber"
		   });	
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";																
		addAnnotation
		  (unaryOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 "
		   });				
		addAnnotation
		  (binaryOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 and self.in.get(0).getName()=\'I0\' and self.in.get(1).getName()=\'I1\' self.getOuput().getName()=\'O\' "
		   });			
		addAnnotation
		  (pipelinedBinaryOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 and self.in.get(0).getName()=\'I0\' and self.in.get(1).getName()=\'I1\' self.getOuput().getName()=\'O\' "
		   });			
		addAnnotation
		  (multiCycleBinaryOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 and self.in.get(0).getName()=\'I0\' and self.in.get(1).getName()=\'I1\' self.getOuput().getName()=\'O\' "
		   });			
		addAnnotation
		  (ternaryOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=3 and self.out.size()=1 ",
			 "bitwidthConstraint", "self.in.forall(p | p.name=\'S\' implies p.width=1)"
		   });			
		addAnnotation
		  (reductionOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 "
		   });			
		addAnnotation
		  (pipelinedReductionOperatorEClass, 
		   source, 
		   new String[] {
			 "consistentPortNumber", "self.in.size()=2 and self.out.size()=1 "
		   });
	}

	/**
	 * Initializes the annotations for <b>GMFMapping</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGMFMappingAnnotations() {
		String source = "GMFMapping";																	
		addAnnotation
		  (unaryOperatorEClass, 
		   source, 
		   new String[] {
			 "type", "NodeWithPorts",
			 "label", "{opcode,label}",
			 "ports", "in.north"
		   });												
	}

} //OperatorsPackageImpl
