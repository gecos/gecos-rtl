/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.Wire;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hybrid Wire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSource2 <em>Source2</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSink2 <em>Sink2</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSource1 <em>Source1</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSink1 <em>Sink1</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortType consistentOutputPortType'"
 * @generated
 */
public interface HybridWire extends Wire {
	/**
	 * Returns the value of the '<em><b>Source2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source2</em>' reference.
	 * @see #setSource2(OutDataPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire_Source2()
	 * @model required="true"
	 * @generated
	 */
	OutDataPort getSource2();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSource2 <em>Source2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source2</em>' reference.
	 * @see #getSource2()
	 * @generated
	 */
	void setSource2(OutDataPort value);

	/**
	 * Returns the value of the '<em><b>Sink2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink2</em>' reference.
	 * @see #setSink2(InDataPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire_Sink2()
	 * @model required="true"
	 * @generated
	 */
	InDataPort getSink2();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSink2 <em>Sink2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink2</em>' reference.
	 * @see #getSink2()
	 * @generated
	 */
	void setSink2(InDataPort value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(Datapath)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire_Parent()
	 * @model required="true"
	 * @generated
	 */
	Datapath getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Datapath value);

	/**
	 * Returns the value of the '<em><b>Source1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source1</em>' reference.
	 * @see #setSource1(OutControlPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire_Source1()
	 * @model required="true"
	 * @generated
	 */
	OutControlPort getSource1();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSource1 <em>Source1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source1</em>' reference.
	 * @see #getSource1()
	 * @generated
	 */
	void setSource1(OutControlPort value);

	/**
	 * Returns the value of the '<em><b>Sink1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink1</em>' reference.
	 * @see #setSink1(InControlPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getHybridWire_Sink1()
	 * @model required="true"
	 * @generated
	 */
	InControlPort getSink1();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.HybridWire#getSink1 <em>Sink1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink1</em>' reference.
	 * @see #getSink1()
	 * @generated
	 */
	void setSink1(InControlPort value);

} // HybridWire
