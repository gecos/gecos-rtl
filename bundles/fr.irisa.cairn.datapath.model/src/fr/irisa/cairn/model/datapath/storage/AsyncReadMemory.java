/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.MealySequentialBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Async Read Memory</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getAsyncReadMemory()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface AsyncReadMemory extends MealySequentialBlock {
} // AsyncReadMemory
