/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Async Read SPRAM</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getAsyncReadSPRAM()
 * @model
 * @generated
 */
public interface AsyncReadSPRAM extends SinglePortRam, AsyncReadMemory {
} // AsyncReadSPRAM
