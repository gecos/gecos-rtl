/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import fr.irisa.cairn.model.datapath.port.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortFactory;
import fr.irisa.cairn.model.datapath.port.PortPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PortFactoryImpl extends EFactoryImpl implements PortFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PortFactory init() {
		try {
			PortFactory thePortFactory = (PortFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.irisa.fr/cairn/datapath/port"); 
			if (thePortFactory != null) {
				return thePortFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PortFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PortPackage.IN_DATA_PORT: return createInDataPort();
			case PortPackage.OUT_DATA_PORT: return createOutDataPort();
			case PortPackage.IN_CONTROL_PORT: return createInControlPort();
			case PortPackage.OUT_CONTROL_PORT: return createOutControlPort();
			case PortPackage.INPUT_PORT: return createInputPort();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort createInDataPort() {
		InDataPortImpl inDataPort = new InDataPortImpl();
		return inDataPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort createOutDataPort() {
		OutDataPortImpl outDataPort = new OutDataPortImpl();
		return outDataPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort createInControlPort() {
		InControlPortImpl inControlPort = new InControlPortImpl();
		return inControlPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort createOutControlPort() {
		OutControlPortImpl outControlPort = new OutControlPortImpl();
		return outControlPort;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputPort createInputPort() {
		InputPortImpl inputPort = new InputPortImpl();
		return inputPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortPackage getPortPackage() {
		return (PortPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PortPackage getPackage() {
		return PortPackage.eINSTANCE;
	}

} //PortFactoryImpl
