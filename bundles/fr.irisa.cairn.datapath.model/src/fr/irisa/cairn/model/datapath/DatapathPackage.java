/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.DatapathFactory
 * @model kind="package"
 * @generated
 */
public interface DatapathPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "datapath";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.model.datapath";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatapathPackage eINSTANCE = fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.NamedElementImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl <em>Abstract Datapath</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getAbstractDatapath()
	 * @generated
	 */
	int ABSTRACT_DATAPATH = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__PARENT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__COMBINATIONAL = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__ACTIVATE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__IN = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__OUT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH__FLAGS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Abstract Datapath</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_DATAPATH_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl <em>Datapath</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getDatapath()
	 * @generated
	 */
	int DATAPATH = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__PARENT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__COMBINATIONAL = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__ACTIVATE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__IN = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__OUT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__FLAGS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__COMPONENTS = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Data Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__DATA_WIRES = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Control Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__CONTROL_WIRES = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Library</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__LIBRARY = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__WIRES = NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Registers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__REGISTERS = NAMED_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Memblocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__MEMBLOCKS = NAMED_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__OPERATORS = NAMED_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Fsms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH__FSMS = NAMED_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Datapath</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATAPATH_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.AbstractBlock <em>Abstract Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getAbstractBlock()
	 * @generated
	 */
	int ABSTRACT_BLOCK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BLOCK__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BLOCK__PARENT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BLOCK__COMBINATIONAL = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BLOCK_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.ActivableBlock <em>Activable Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.ActivableBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getActivableBlock()
	 * @generated
	 */
	int ACTIVABLE_BLOCK = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVABLE_BLOCK__NAME = ABSTRACT_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVABLE_BLOCK__PARENT = ABSTRACT_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVABLE_BLOCK__COMBINATIONAL = ABSTRACT_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVABLE_BLOCK__ACTIVATE = ABSTRACT_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Activable Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVABLE_BLOCK_FEATURE_COUNT = ABSTRACT_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.DataFlowBlock <em>Data Flow Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getDataFlowBlock()
	 * @generated
	 */
	int DATA_FLOW_BLOCK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK__NAME = ABSTRACT_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK__PARENT = ABSTRACT_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK__COMBINATIONAL = ABSTRACT_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK__IN = ABSTRACT_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK__OUT = ABSTRACT_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Flow Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_FLOW_BLOCK_FEATURE_COUNT = ABSTRACT_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock <em>Flag Bearer Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getFlagBearerBlock()
	 * @generated
	 */
	int FLAG_BEARER_BLOCK = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_BEARER_BLOCK__NAME = ABSTRACT_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_BEARER_BLOCK__PARENT = ABSTRACT_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_BEARER_BLOCK__COMBINATIONAL = ABSTRACT_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_BEARER_BLOCK__FLAGS = ABSTRACT_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Flag Bearer Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_BEARER_BLOCK_FEATURE_COUNT = ABSTRACT_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.WireImpl <em>Wire</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.WireImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getWire()
	 * @generated
	 */
	int WIRE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE__WIDTH = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Wire</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIRE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.PortImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__WIDTH = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.OperatorImpl <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.OperatorImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__NAME = DATA_FLOW_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__PARENT = DATA_FLOW_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__COMBINATIONAL = DATA_FLOW_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__IN = DATA_FLOW_BLOCK__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR__OUT = DATA_FLOW_BLOCK__OUT;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = DATA_FLOW_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.CombinationalDatapathImpl <em>Combinational Datapath</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.CombinationalDatapathImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getCombinationalDatapath()
	 * @generated
	 */
	int COMBINATIONAL_DATAPATH = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__NAME = DATAPATH__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__PARENT = DATAPATH__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__COMBINATIONAL = DATAPATH__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__ACTIVATE = DATAPATH__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__IN = DATAPATH__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__OUT = DATAPATH__OUT;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__FLAGS = DATAPATH__FLAGS;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__COMPONENTS = DATAPATH__COMPONENTS;

	/**
	 * The feature id for the '<em><b>Data Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__DATA_WIRES = DATAPATH__DATA_WIRES;

	/**
	 * The feature id for the '<em><b>Control Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__CONTROL_WIRES = DATAPATH__CONTROL_WIRES;

	/**
	 * The feature id for the '<em><b>Library</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__LIBRARY = DATAPATH__LIBRARY;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__WIRES = DATAPATH__WIRES;

	/**
	 * The feature id for the '<em><b>Registers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__REGISTERS = DATAPATH__REGISTERS;

	/**
	 * The feature id for the '<em><b>Memblocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__MEMBLOCKS = DATAPATH__MEMBLOCKS;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__OPERATORS = DATAPATH__OPERATORS;

	/**
	 * The feature id for the '<em><b>Fsms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH__FSMS = DATAPATH__FSMS;

	/**
	 * The number of structural features of the '<em>Combinational Datapath</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_DATAPATH_FEATURE_COUNT = DATAPATH_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.CombinationalBlock <em>Combinational Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.CombinationalBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getCombinationalBlock()
	 * @generated
	 */
	int COMBINATIONAL_BLOCK = 11;

	/**
	 * The number of structural features of the '<em>Combinational Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMBINATIONAL_BLOCK_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.SequentialBlock <em>Sequential Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.SequentialBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getSequentialBlock()
	 * @generated
	 */
	int SEQUENTIAL_BLOCK = 12;

	/**
	 * The number of structural features of the '<em>Sequential Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_BLOCK_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.PipelinedBlock <em>Pipelined Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.PipelinedBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPipelinedBlock()
	 * @generated
	 */
	int PIPELINED_BLOCK = 13;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BLOCK__LATENCY = SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pipelined Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_BLOCK_FEATURE_COUNT = SEQUENTIAL_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock <em>Multi Cycle Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMultiCycleBlock()
	 * @generated
	 */
	int MULTI_CYCLE_BLOCK = 14;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BLOCK__LATENCY = SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BLOCK__THROUGHPUT = SEQUENTIAL_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Multi Cycle Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTI_CYCLE_BLOCK_FEATURE_COUNT = SEQUENTIAL_BLOCK_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.Pad <em>Pad</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.Pad
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPad()
	 * @generated
	 */
	int PAD = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAD__NAME = ABSTRACT_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAD__PARENT = ABSTRACT_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAD__COMBINATIONAL = ABSTRACT_BLOCK__COMBINATIONAL;

	/**
	 * The number of structural features of the '<em>Pad</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAD_FEATURE_COUNT = ABSTRACT_BLOCK_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.PipelinedDatapathImpl <em>Pipelined Datapath</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.PipelinedDatapathImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPipelinedDatapath()
	 * @generated
	 */
	int PIPELINED_DATAPATH = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__NAME = DATAPATH__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__PARENT = DATAPATH__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__COMBINATIONAL = DATAPATH__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__ACTIVATE = DATAPATH__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__IN = DATAPATH__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__OUT = DATAPATH__OUT;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__FLAGS = DATAPATH__FLAGS;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__COMPONENTS = DATAPATH__COMPONENTS;

	/**
	 * The feature id for the '<em><b>Data Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__DATA_WIRES = DATAPATH__DATA_WIRES;

	/**
	 * The feature id for the '<em><b>Control Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__CONTROL_WIRES = DATAPATH__CONTROL_WIRES;

	/**
	 * The feature id for the '<em><b>Library</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__LIBRARY = DATAPATH__LIBRARY;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__WIRES = DATAPATH__WIRES;

	/**
	 * The feature id for the '<em><b>Registers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__REGISTERS = DATAPATH__REGISTERS;

	/**
	 * The feature id for the '<em><b>Memblocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__MEMBLOCKS = DATAPATH__MEMBLOCKS;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__OPERATORS = DATAPATH__OPERATORS;

	/**
	 * The feature id for the '<em><b>Fsms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__FSMS = DATAPATH__FSMS;

	/**
	 * The feature id for the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH__LATENCY = DATAPATH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pipelined Datapath</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIPELINED_DATAPATH_FEATURE_COUNT = DATAPATH_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.SequentialDatapathImpl <em>Sequential Datapath</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.SequentialDatapathImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getSequentialDatapath()
	 * @generated
	 */
	int SEQUENTIAL_DATAPATH = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__NAME = DATAPATH__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__PARENT = DATAPATH__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__COMBINATIONAL = DATAPATH__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__ACTIVATE = DATAPATH__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__IN = DATAPATH__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__OUT = DATAPATH__OUT;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__FLAGS = DATAPATH__FLAGS;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__COMPONENTS = DATAPATH__COMPONENTS;

	/**
	 * The feature id for the '<em><b>Data Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__DATA_WIRES = DATAPATH__DATA_WIRES;

	/**
	 * The feature id for the '<em><b>Control Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__CONTROL_WIRES = DATAPATH__CONTROL_WIRES;

	/**
	 * The feature id for the '<em><b>Library</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__LIBRARY = DATAPATH__LIBRARY;

	/**
	 * The feature id for the '<em><b>Wires</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__WIRES = DATAPATH__WIRES;

	/**
	 * The feature id for the '<em><b>Registers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__REGISTERS = DATAPATH__REGISTERS;

	/**
	 * The feature id for the '<em><b>Memblocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__MEMBLOCKS = DATAPATH__MEMBLOCKS;

	/**
	 * The feature id for the '<em><b>Operators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__OPERATORS = DATAPATH__OPERATORS;

	/**
	 * The feature id for the '<em><b>Fsms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH__FSMS = DATAPATH__FSMS;

	/**
	 * The number of structural features of the '<em>Sequential Datapath</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENTIAL_DATAPATH_FEATURE_COUNT = DATAPATH_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.BlackBoxBlockImpl <em>Black Box Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.BlackBoxBlockImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getBlackBoxBlock()
	 * @generated
	 */
	int BLACK_BOX_BLOCK = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__NAME = ABSTRACT_DATAPATH__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__PARENT = ABSTRACT_DATAPATH__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__COMBINATIONAL = ABSTRACT_DATAPATH__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__ACTIVATE = ABSTRACT_DATAPATH__ACTIVATE;

	/**
	 * The feature id for the '<em><b>In</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__IN = ABSTRACT_DATAPATH__IN;

	/**
	 * The feature id for the '<em><b>Out</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__OUT = ABSTRACT_DATAPATH__OUT;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__FLAGS = ABSTRACT_DATAPATH__FLAGS;

	/**
	 * The feature id for the '<em><b>Implementation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK__IMPLEMENTATION = ABSTRACT_DATAPATH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Black Box Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLACK_BOX_BLOCK_FEATURE_COUNT = ABSTRACT_DATAPATH_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.MooreSequentialBlockImpl <em>Moore Sequential Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.MooreSequentialBlockImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMooreSequentialBlock()
	 * @generated
	 */
	int MOORE_SEQUENTIAL_BLOCK = 19;

	/**
	 * The number of structural features of the '<em>Moore Sequential Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOORE_SEQUENTIAL_BLOCK_FEATURE_COUNT = SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.datapath.impl.MealySequentialBlockImpl <em>Mealy Sequential Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.datapath.impl.MealySequentialBlockImpl
	 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMealySequentialBlock()
	 * @generated
	 */
	int MEALY_SEQUENTIAL_BLOCK = 20;

	/**
	 * The number of structural features of the '<em>Mealy Sequential Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEALY_SEQUENTIAL_BLOCK_FEATURE_COUNT = SEQUENTIAL_BLOCK_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.AbstractDatapath <em>Abstract Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.AbstractDatapath
	 * @generated
	 */
	EClass getAbstractDatapath();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.Datapath <em>Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath
	 * @generated
	 */
	EClass getDatapath();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getComponents()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Components();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getDataWires <em>Data Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Wires</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getDataWires()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_DataWires();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getControlWires <em>Control Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Control Wires</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getControlWires()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_ControlWires();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getLibrary <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Library</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getLibrary()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Library();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getWires <em>Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Wires</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getWires()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Wires();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getRegisters <em>Registers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Registers</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getRegisters()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Registers();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getMemblocks <em>Memblocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Memblocks</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getMemblocks()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Memblocks();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getOperators <em>Operators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operators</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getOperators()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Operators();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.Datapath#getFsms <em>Fsms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fsms</em>'.
	 * @see fr.irisa.cairn.model.datapath.Datapath#getFsms()
	 * @see #getDatapath()
	 * @generated
	 */
	EReference getDatapath_Fsms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.AbstractBlock <em>Abstract Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock
	 * @generated
	 */
	EClass getAbstractBlock();

	/**
	 * Returns the meta object for the container reference '{@link fr.irisa.cairn.model.datapath.AbstractBlock#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock#getParent()
	 * @see #getAbstractBlock()
	 * @generated
	 */
	EReference getAbstractBlock_Parent();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.AbstractBlock#isCombinational <em>Combinational</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Combinational</em>'.
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock#isCombinational()
	 * @see #getAbstractBlock()
	 * @generated
	 */
	EAttribute getAbstractBlock_Combinational();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.ActivableBlock <em>Activable Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activable Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.ActivableBlock
	 * @generated
	 */
	EClass getActivableBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.ActivableBlock#getActivate <em>Activate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activate</em>'.
	 * @see fr.irisa.cairn.model.datapath.ActivableBlock#getActivate()
	 * @see #getActivableBlock()
	 * @generated
	 */
	EReference getActivableBlock_Activate();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.DataFlowBlock <em>Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Flow Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock
	 * @generated
	 */
	EClass getDataFlowBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>In</em>'.
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock#getIn()
	 * @see #getDataFlowBlock()
	 * @generated
	 */
	EReference getDataFlowBlock_In();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.DataFlowBlock#getOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Out</em>'.
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock#getOut()
	 * @see #getDataFlowBlock()
	 * @generated
	 */
	EReference getDataFlowBlock_Out();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock <em>Flag Bearer Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flag Bearer Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock
	 * @generated
	 */
	EClass getFlagBearerBlock();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock#getFlags <em>Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Flags</em>'.
	 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock#getFlags()
	 * @see #getFlagBearerBlock()
	 * @generated
	 */
	EReference getFlagBearerBlock_Flags();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see fr.irisa.cairn.model.datapath.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see fr.irisa.cairn.model.datapath.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.Wire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wire</em>'.
	 * @see fr.irisa.cairn.model.datapath.Wire
	 * @generated
	 */
	EClass getWire();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.Wire#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see fr.irisa.cairn.model.datapath.Wire#getWidth()
	 * @see #getWire()
	 * @generated
	 */
	EAttribute getWire_Width();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see fr.irisa.cairn.model.datapath.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.Port#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see fr.irisa.cairn.model.datapath.Port#getWidth()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Width();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see fr.irisa.cairn.model.datapath.Operator
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.CombinationalDatapath <em>Combinational Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinational Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.CombinationalDatapath
	 * @generated
	 */
	EClass getCombinationalDatapath();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.CombinationalBlock <em>Combinational Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Combinational Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.CombinationalBlock
	 * @generated
	 */
	EClass getCombinationalBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.SequentialBlock <em>Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.SequentialBlock
	 * @generated
	 */
	EClass getSequentialBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.PipelinedBlock <em>Pipelined Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pipelined Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.PipelinedBlock
	 * @generated
	 */
	EClass getPipelinedBlock();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.PipelinedBlock#getLatency <em>Latency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latency</em>'.
	 * @see fr.irisa.cairn.model.datapath.PipelinedBlock#getLatency()
	 * @see #getPipelinedBlock()
	 * @generated
	 */
	EAttribute getPipelinedBlock_Latency();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock <em>Multi Cycle Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multi Cycle Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock
	 * @generated
	 */
	EClass getMultiCycleBlock();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getLatency <em>Latency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Latency</em>'.
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock#getLatency()
	 * @see #getMultiCycleBlock()
	 * @generated
	 */
	EAttribute getMultiCycleBlock_Latency();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getThroughput <em>Throughput</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Throughput</em>'.
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock#getThroughput()
	 * @see #getMultiCycleBlock()
	 * @generated
	 */
	EAttribute getMultiCycleBlock_Throughput();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.Pad <em>Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pad</em>'.
	 * @see fr.irisa.cairn.model.datapath.Pad
	 * @generated
	 */
	EClass getPad();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.PipelinedDatapath <em>Pipelined Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pipelined Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.PipelinedDatapath
	 * @generated
	 */
	EClass getPipelinedDatapath();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.SequentialDatapath <em>Sequential Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequential Datapath</em>'.
	 * @see fr.irisa.cairn.model.datapath.SequentialDatapath
	 * @generated
	 */
	EClass getSequentialDatapath();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.BlackBoxBlock <em>Black Box Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Black Box Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.BlackBoxBlock
	 * @generated
	 */
	EClass getBlackBoxBlock();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.datapath.BlackBoxBlock#getImplementation <em>Implementation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Implementation</em>'.
	 * @see fr.irisa.cairn.model.datapath.BlackBoxBlock#getImplementation()
	 * @see #getBlackBoxBlock()
	 * @generated
	 */
	EReference getBlackBoxBlock_Implementation();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.MooreSequentialBlock <em>Moore Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Moore Sequential Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.MooreSequentialBlock
	 * @generated
	 */
	EClass getMooreSequentialBlock();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.datapath.MealySequentialBlock <em>Mealy Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mealy Sequential Block</em>'.
	 * @see fr.irisa.cairn.model.datapath.MealySequentialBlock
	 * @generated
	 */
	EClass getMealySequentialBlock();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DatapathFactory getDatapathFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl <em>Abstract Datapath</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.AbstractDatapathImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getAbstractDatapath()
		 * @generated
		 */
		EClass ABSTRACT_DATAPATH = eINSTANCE.getAbstractDatapath();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.DatapathImpl <em>Datapath</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getDatapath()
		 * @generated
		 */
		EClass DATAPATH = eINSTANCE.getDatapath();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__COMPONENTS = eINSTANCE.getDatapath_Components();

		/**
		 * The meta object literal for the '<em><b>Data Wires</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__DATA_WIRES = eINSTANCE.getDatapath_DataWires();

		/**
		 * The meta object literal for the '<em><b>Control Wires</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__CONTROL_WIRES = eINSTANCE.getDatapath_ControlWires();

		/**
		 * The meta object literal for the '<em><b>Library</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__LIBRARY = eINSTANCE.getDatapath_Library();

		/**
		 * The meta object literal for the '<em><b>Wires</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__WIRES = eINSTANCE.getDatapath_Wires();

		/**
		 * The meta object literal for the '<em><b>Registers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__REGISTERS = eINSTANCE.getDatapath_Registers();

		/**
		 * The meta object literal for the '<em><b>Memblocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__MEMBLOCKS = eINSTANCE.getDatapath_Memblocks();

		/**
		 * The meta object literal for the '<em><b>Operators</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__OPERATORS = eINSTANCE.getDatapath_Operators();

		/**
		 * The meta object literal for the '<em><b>Fsms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATAPATH__FSMS = eINSTANCE.getDatapath_Fsms();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.AbstractBlock <em>Abstract Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.AbstractBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getAbstractBlock()
		 * @generated
		 */
		EClass ABSTRACT_BLOCK = eINSTANCE.getAbstractBlock();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_BLOCK__PARENT = eINSTANCE.getAbstractBlock_Parent();

		/**
		 * The meta object literal for the '<em><b>Combinational</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_BLOCK__COMBINATIONAL = eINSTANCE.getAbstractBlock_Combinational();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.ActivableBlock <em>Activable Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.ActivableBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getActivableBlock()
		 * @generated
		 */
		EClass ACTIVABLE_BLOCK = eINSTANCE.getActivableBlock();

		/**
		 * The meta object literal for the '<em><b>Activate</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVABLE_BLOCK__ACTIVATE = eINSTANCE.getActivableBlock_Activate();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.DataFlowBlock <em>Data Flow Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.DataFlowBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getDataFlowBlock()
		 * @generated
		 */
		EClass DATA_FLOW_BLOCK = eINSTANCE.getDataFlowBlock();

		/**
		 * The meta object literal for the '<em><b>In</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_BLOCK__IN = eINSTANCE.getDataFlowBlock_In();

		/**
		 * The meta object literal for the '<em><b>Out</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_FLOW_BLOCK__OUT = eINSTANCE.getDataFlowBlock_Out();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock <em>Flag Bearer Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getFlagBearerBlock()
		 * @generated
		 */
		EClass FLAG_BEARER_BLOCK = eINSTANCE.getFlagBearerBlock();

		/**
		 * The meta object literal for the '<em><b>Flags</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLAG_BEARER_BLOCK__FLAGS = eINSTANCE.getFlagBearerBlock_Flags();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.NamedElementImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.WireImpl <em>Wire</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.WireImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getWire()
		 * @generated
		 */
		EClass WIRE = eINSTANCE.getWire();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WIRE__WIDTH = eINSTANCE.getWire_Width();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.PortImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__WIDTH = eINSTANCE.getPort_Width();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.OperatorImpl <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.OperatorImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.CombinationalDatapathImpl <em>Combinational Datapath</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.CombinationalDatapathImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getCombinationalDatapath()
		 * @generated
		 */
		EClass COMBINATIONAL_DATAPATH = eINSTANCE.getCombinationalDatapath();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.CombinationalBlock <em>Combinational Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.CombinationalBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getCombinationalBlock()
		 * @generated
		 */
		EClass COMBINATIONAL_BLOCK = eINSTANCE.getCombinationalBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.SequentialBlock <em>Sequential Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.SequentialBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getSequentialBlock()
		 * @generated
		 */
		EClass SEQUENTIAL_BLOCK = eINSTANCE.getSequentialBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.PipelinedBlock <em>Pipelined Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.PipelinedBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPipelinedBlock()
		 * @generated
		 */
		EClass PIPELINED_BLOCK = eINSTANCE.getPipelinedBlock();

		/**
		 * The meta object literal for the '<em><b>Latency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIPELINED_BLOCK__LATENCY = eINSTANCE.getPipelinedBlock_Latency();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock <em>Multi Cycle Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMultiCycleBlock()
		 * @generated
		 */
		EClass MULTI_CYCLE_BLOCK = eINSTANCE.getMultiCycleBlock();

		/**
		 * The meta object literal for the '<em><b>Latency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_CYCLE_BLOCK__LATENCY = eINSTANCE.getMultiCycleBlock_Latency();

		/**
		 * The meta object literal for the '<em><b>Throughput</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTI_CYCLE_BLOCK__THROUGHPUT = eINSTANCE.getMultiCycleBlock_Throughput();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.Pad <em>Pad</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.Pad
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPad()
		 * @generated
		 */
		EClass PAD = eINSTANCE.getPad();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.PipelinedDatapathImpl <em>Pipelined Datapath</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.PipelinedDatapathImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getPipelinedDatapath()
		 * @generated
		 */
		EClass PIPELINED_DATAPATH = eINSTANCE.getPipelinedDatapath();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.SequentialDatapathImpl <em>Sequential Datapath</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.SequentialDatapathImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getSequentialDatapath()
		 * @generated
		 */
		EClass SEQUENTIAL_DATAPATH = eINSTANCE.getSequentialDatapath();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.BlackBoxBlockImpl <em>Black Box Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.BlackBoxBlockImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getBlackBoxBlock()
		 * @generated
		 */
		EClass BLACK_BOX_BLOCK = eINSTANCE.getBlackBoxBlock();

		/**
		 * The meta object literal for the '<em><b>Implementation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLACK_BOX_BLOCK__IMPLEMENTATION = eINSTANCE.getBlackBoxBlock_Implementation();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.MooreSequentialBlockImpl <em>Moore Sequential Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.MooreSequentialBlockImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMooreSequentialBlock()
		 * @generated
		 */
		EClass MOORE_SEQUENTIAL_BLOCK = eINSTANCE.getMooreSequentialBlock();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.datapath.impl.MealySequentialBlockImpl <em>Mealy Sequential Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.datapath.impl.MealySequentialBlockImpl
		 * @see fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl#getMealySequentialBlock()
		 * @generated
		 */
		EClass MEALY_SEQUENTIAL_BLOCK = eINSTANCE.getMealySequentialBlock();

	}

} //DatapathPackage
