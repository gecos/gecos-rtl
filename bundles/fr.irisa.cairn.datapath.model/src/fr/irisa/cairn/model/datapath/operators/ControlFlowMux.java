/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Flow Mux</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getControlFlowMux()
 * @model
 * @generated
 */
public interface ControlFlowMux extends SingleOutputDataFlowBlock, SingleControlPortBlock {
} // ControlFlowMux
