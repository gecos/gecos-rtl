/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Cycle Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getLatency <em>Latency</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getThroughput <em>Throughput</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getMultiCycleBlock()
 * @model interface="true" abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortWidth consistentOutputPortWidth hasValidSources'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noCombinationalCycles=''"
 * @generated
 */
public interface MultiCycleBlock extends SequentialBlock {
	/**
	 * Returns the value of the '<em><b>Latency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Latency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Latency</em>' attribute.
	 * @see #setLatency(int)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getMultiCycleBlock_Latency()
	 * @model
	 * @generated
	 */
	int getLatency();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getLatency <em>Latency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Latency</em>' attribute.
	 * @see #getLatency()
	 * @generated
	 */
	void setLatency(int value);

	/**
	 * Returns the value of the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Throughput</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Throughput</em>' attribute.
	 * @see #setThroughput(int)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getMultiCycleBlock_Throughput()
	 * @model
	 * @generated
	 */
	int getThroughput();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock#getThroughput <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Throughput</em>' attribute.
	 * @see #getThroughput()
	 * @generated
	 */
	void setThroughput(int value);

} // MultiCycleBlock
