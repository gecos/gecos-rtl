/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flag Bearer Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.FlagBearerBlock#getFlags <em>Flags</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getFlagBearerBlock()
 * @model interface="true" abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortWidth consistentOutputPortWidth hasValidSources'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noCombinationalCycles=''"
 * @generated
 */
public interface FlagBearerBlock extends AbstractBlock {
	/**
	 * Returns the value of the '<em><b>Flags</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.port.OutControlPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Flags</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flags</em>' containment reference list.
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getFlagBearerBlock_Flags()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutControlPort> getFlags();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return getFlags().get(pos);'"
	 * @generated
	 */
	OutControlPort getFlag(int pos);

} // FlagBearerBlock
