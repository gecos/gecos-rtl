/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Status Pad</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl#getActivate <em>Activate</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.StatusPadImpl#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class StatusPadImpl extends NamedElementImpl implements StatusPad {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivate()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> activate;

	/**
	 * The cached value of the '{@link #getAssociatedPort() <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPort()
	 * @generated
	 * @ordered
	 */
	protected OutControlPort associatedPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatusPadImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PadsPackage.Literals.STATUS_PAD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != PadsPackage.STATUS_PAD__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, PadsPackage.STATUS_PAD__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != PadsPackage.STATUS_PAD__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PadsPackage.STATUS_PAD__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCombinational() {
		// TODO: implement this method to return the 'Combinational' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getActivate() {
		if (activate == null) {
			activate = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, PadsPackage.STATUS_PAD__ACTIVATE);
		}
		return activate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getAssociatedPort() {
		if (associatedPort != null && associatedPort.eIsProxy()) {
			InternalEObject oldAssociatedPort = (InternalEObject)associatedPort;
			associatedPort = (OutControlPort)eResolveProxy(oldAssociatedPort);
			if (associatedPort != oldAssociatedPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PadsPackage.STATUS_PAD__ASSOCIATED_PORT, oldAssociatedPort, associatedPort));
			}
		}
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort basicGetAssociatedPort() {
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPort(OutControlPort newAssociatedPort) {
		OutControlPort oldAssociatedPort = associatedPort;
		associatedPort = newAssociatedPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PadsPackage.STATUS_PAD__ASSOCIATED_PORT, oldAssociatedPort, associatedPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControl() {
		return getActivate().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleFlagBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(OutControlPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControlPort(int pos) {
		return getActivate().get(pos);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				return basicSetParent(null, msgs);
			case PadsPackage.STATUS_PAD__ACTIVATE:
				return ((InternalEList<?>)getActivate()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case PadsPackage.STATUS_PAD__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				return getParent();
			case PadsPackage.STATUS_PAD__COMBINATIONAL:
				return isCombinational();
			case PadsPackage.STATUS_PAD__ACTIVATE:
				return getActivate();
			case PadsPackage.STATUS_PAD__ASSOCIATED_PORT:
				if (resolve) return getAssociatedPort();
				return basicGetAssociatedPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				setParent((Datapath)newValue);
				return;
			case PadsPackage.STATUS_PAD__ACTIVATE:
				getActivate().clear();
				getActivate().addAll((Collection<? extends InControlPort>)newValue);
				return;
			case PadsPackage.STATUS_PAD__ASSOCIATED_PORT:
				setAssociatedPort((OutControlPort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				setParent((Datapath)null);
				return;
			case PadsPackage.STATUS_PAD__ACTIVATE:
				getActivate().clear();
				return;
			case PadsPackage.STATUS_PAD__ASSOCIATED_PORT:
				setAssociatedPort((OutControlPort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PadsPackage.STATUS_PAD__PARENT:
				return getParent() != null;
			case PadsPackage.STATUS_PAD__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case PadsPackage.STATUS_PAD__ACTIVATE:
				return activate != null && !activate.isEmpty();
			case PadsPackage.STATUS_PAD__ASSOCIATED_PORT:
				return associatedPort != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == CombinationalBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (derivedFeatureID) {
				case PadsPackage.STATUS_PAD__ACTIVATE: return DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == CombinationalBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ActivableBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE: return PadsPackage.STATUS_PAD__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("Control output Pad "+name+"["+getActivate().get(0).getWidth());
		return result.toString();
	}

} //StatusPadImpl
