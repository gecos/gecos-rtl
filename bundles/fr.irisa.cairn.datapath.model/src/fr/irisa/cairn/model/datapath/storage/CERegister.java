/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CE Register</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getCERegister()
 * @model
 * @generated
 */
public interface CERegister extends ActivableBlock, Register, SingleControlPortBlock {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getActivate().get(0);'"
	 * @generated
	 */
	InControlPort getLoadEnable();
} // CERegister
