/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import fr.irisa.cairn.model.datapath.Port;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutputPort()
 * @model abstract="true"
 * @generated
 */
public interface OutputPort extends Port {

} // OutputPort
