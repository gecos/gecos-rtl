/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sync Read SPRAM</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getSyncReadSPRAM()
 * @model
 * @generated
 */
public interface SyncReadSPRAM extends SinglePortRam, SyncReadMemory {
} // SyncReadSPRAM
