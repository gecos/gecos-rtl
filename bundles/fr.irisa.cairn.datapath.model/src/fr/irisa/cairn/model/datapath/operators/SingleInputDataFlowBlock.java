/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Input Data Flow Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getSingleInputDataFlowBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SingleInputDataFlowBlock extends DataFlowBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(0);'"
	 * @generated
	 */
	InDataPort getInput();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	DataFlowWire connect(SingleOutputDataFlowBlock source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	DataFlowWire connect(OutDataPort source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='try { \r\n\treturn getInput().getWire().getSource().getParentNode(); \r\n} catch(NullPointerException e) {\r\n\treturn null;\r\n}'"
	 * @generated
	 */
	AbstractBlock getSourceBlock();

} // SingleInputDataFlowBlock
