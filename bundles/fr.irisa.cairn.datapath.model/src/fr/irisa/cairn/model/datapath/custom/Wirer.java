package fr.irisa.cairn.model.datapath.custom;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.impl.InControlPortImpl;
import fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl;
import fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresFactory;

public class Wirer {

	static int wireId =0;
	private static DataFlowWire dwire(Datapath container, InDataPort in, OutDataPort out) {
		if(container==null){
			throw new RuntimeException("Cannot connect with a null Datapath");
		}
		if(in.getWidth()>0 && out.getWidth()>0 && in.getWidth()!=out.getWidth()) {
			throw new RuntimeException("Cannot wire ports "+in+" and "+out+" as they have different bitwidth ");
		} else {
			if (in.getWidth()<=0 && out.getWidth()<=0) {
				throw new RuntimeException("Neither ports "+in+" and "+out+" have their width set to a non default value");
			} 
			int width = Math.max(in.getWidth(),out.getWidth());
			DataFlowWire dwire = WiresFactory.eINSTANCE.createDataFlowWire();
			dwire.setName("dwire_"+(wireId++));
			dwire.setWidth(width);
			dwire.setSource(out);
			dwire.setSink(in);
			container.getDataWires().add(dwire);
			return dwire;
		}
	}


	private static ControlFlowWire cwire(Datapath container, InControlPort in, OutControlPort out) {
		if(container==null){
			throw new RuntimeException("Cannot connect with a null Datapath");
		}
		if(in.getWidth()>0 && out.getWidth()>0 && in.getWidth()!=out.getWidth()) {
			throw new RuntimeException("Cannot wire ports "+in+" and "+out+" as they have different bitwidth ");
		} else {
			if (in.getWidth()<=0 && out.getWidth()<=0) {
				throw new RuntimeException("Neither ports "+in+" and "+out+" have their width set to a non default value");
			} 
			int width = Math.max(in.getWidth(),out.getWidth());
			ControlFlowWire cwire = WiresFactory.eINSTANCE.createControlFlowWire();
			cwire.setName("dwire_"+(wireId++));
			cwire.setWidth(width);
			cwire.setSource(out);
			cwire.setSink(in);
			container.getControlWires().add(cwire);
			return cwire;
		}
	}

	private static Datapath getDatapath(EObject port) {
		EObject current = port;
		while (current!=null && (!(current instanceof Datapath))) {
			current=current.eContainer();
		}
		return (Datapath) current;
		
	}

	public static DataFlowWire wire(InDataPort in,	OutDataPort source) {
		return dwire(getDatapath(source), in, source);
	}

	public static DataFlowWire wire(InDataPort in,	SingleOutputDataFlowBlock source) {
		return dwire(getDatapath(source), in, source.getOutput());
	}

	public static ControlFlowWire wire(InControlPort in,SingleFlagBlock source) {
		return cwire(getDatapath(source), in, source.getFlag());
	}

	public static ControlFlowWire wire(InControlPort in,	OutControlPort source) {
		ControlFlowWire cwire = cwire(getDatapath(source), in, source);
		return cwire;
	}

	public static ControlFlowWire wire(OutControlPort out, SingleControlPortBlock sink) {
		return cwire(getDatapath(sink), sink.getControl(),out);
	}

	public static ControlFlowWire wire(OutControlPort out, InControlPort sink) {
		return cwire(getDatapath(sink), sink,out);
	}

	public static DataFlowWire wire(OutDataPort out, SingleInputDataFlowBlock sink) {
		return dwire(getDatapath(sink), sink.getInput(), out);
	}

	public static DataFlowWire wire(OutDataPort out, InDataPort sink) {
		return dwire(getDatapath(sink), sink, out);
	}

	//*****************************************************************
	
	
	public static DataFlowWire wire(SingleInputDataFlowBlock in,	OutDataPort source) {
		return dwire(getDatapath(source), in.getInput(), source);
	}

	public static DataFlowWire wire(SingleInputDataFlowBlock in,	SingleOutputDataFlowBlock source) {
		return dwire(getDatapath(source), in.getInput(), source.getOutput());
	}

	public static ControlFlowWire wire(SingleControlPortBlock in,SingleFlagBlock source) {
		return cwire(getDatapath(source), in.getControl(), source.getFlag());
	}

	public static ControlFlowWire wire(SingleControlPortBlock in,	OutControlPort source) {
		return cwire(getDatapath(source), in.getControl(), source);
	}

	public static ControlFlowWire wire(SingleFlagBlock out, SingleControlPortBlock sink) {
		return cwire(getDatapath(sink), sink.getControl(),out.getFlag());
	}

	public static ControlFlowWire wire(SingleFlagBlock out, InControlPort sink) {
		return cwire(getDatapath(sink), sink,out.getFlag());
	}

	public static DataFlowWire wire(SingleOutputDataFlowBlock out, SingleInputDataFlowBlock sink) {
		return dwire(getDatapath(sink), sink.getInput(), out.getOutput());
	}

	public static DataFlowWire wire(SingleOutputDataFlowBlock out, InDataPort sink) {
		return dwire(getDatapath(sink), sink, out.getOutput());
	}


}
