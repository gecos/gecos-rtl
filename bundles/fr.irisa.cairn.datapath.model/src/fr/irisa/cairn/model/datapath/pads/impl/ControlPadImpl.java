/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads.impl;

import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Pad</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl#getFlags <em>Flags</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.ControlPadImpl#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControlPadImpl extends NamedElementImpl implements ControlPad {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected EList<OutControlPort> flags;
	/**
	 * The cached value of the '{@link #getAssociatedPort() <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPort()
	 * @generated
	 * @ordered
	 */
	protected InControlPort associatedPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlPadImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PadsPackage.Literals.CONTROL_PAD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != PadsPackage.CONTROL_PAD__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, PadsPackage.CONTROL_PAD__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID != PadsPackage.CONTROL_PAD__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PadsPackage.CONTROL_PAD__PARENT, newParent, newParent));
		newParent.getActivate().add(getAssociatedPort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCombinational() {
		// TODO: implement this method to return the 'Combinational' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutControlPort> getFlags() {
		if (flags == null) {
			flags = new EObjectContainmentEList<OutControlPort>(OutControlPort.class, this, PadsPackage.CONTROL_PAD__FLAGS);
		}
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getAssociatedPort() {
		if (associatedPort != null && associatedPort.eIsProxy()) {
			InternalEObject oldAssociatedPort = (InternalEObject)associatedPort;
			associatedPort = (InControlPort)eResolveProxy(oldAssociatedPort);
			if (associatedPort != oldAssociatedPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, oldAssociatedPort, associatedPort));
			}
		}
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort basicGetAssociatedPort() {
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedPort(InControlPort newAssociatedPort, NotificationChain msgs) {
		InControlPort oldAssociatedPort = associatedPort;
		associatedPort = newAssociatedPort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, oldAssociatedPort, newAssociatedPort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPort(InControlPort newAssociatedPort) {
		if (newAssociatedPort != associatedPort) {
			NotificationChain msgs = null;
			if (associatedPort != null)
				msgs = ((InternalEObject)associatedPort).eInverseRemove(this, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, InControlPort.class, msgs);
			if (newAssociatedPort != null)
				msgs = ((InternalEObject)newAssociatedPort).eInverseAdd(this, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, InControlPort.class, msgs);
			msgs = basicSetAssociatedPort(newAssociatedPort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PadsPackage.CONTROL_PAD__ASSOCIATED_PORT, newAssociatedPort, newAssociatedPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getFlag() {
		return getFlags().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleControlPortBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(InControlPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutControlPort getFlag(int pos) {
		return getFlags().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				if (associatedPort != null)
					msgs = ((InternalEObject)associatedPort).eInverseRemove(this, PortPackage.IN_CONTROL_PORT__ASSOCIATED_PAD, InControlPort.class, msgs);
				return basicSetAssociatedPort((InControlPort)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				return basicSetParent(null, msgs);
			case PadsPackage.CONTROL_PAD__FLAGS:
				return ((InternalEList<?>)getFlags()).basicRemove(otherEnd, msgs);
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				return basicSetAssociatedPort(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case PadsPackage.CONTROL_PAD__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				return getParent();
			case PadsPackage.CONTROL_PAD__COMBINATIONAL:
				return isCombinational();
			case PadsPackage.CONTROL_PAD__FLAGS:
				return getFlags();
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				if (resolve) return getAssociatedPort();
				return basicGetAssociatedPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				setParent((Datapath)newValue);
				return;
			case PadsPackage.CONTROL_PAD__FLAGS:
				getFlags().clear();
				getFlags().addAll((Collection<? extends OutControlPort>)newValue);
				return;
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				setAssociatedPort((InControlPort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				setParent((Datapath)null);
				return;
			case PadsPackage.CONTROL_PAD__FLAGS:
				getFlags().clear();
				return;
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				setAssociatedPort((InControlPort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PadsPackage.CONTROL_PAD__PARENT:
				return getParent() != null;
			case PadsPackage.CONTROL_PAD__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case PadsPackage.CONTROL_PAD__FLAGS:
				return flags != null && !flags.isEmpty();
			case PadsPackage.CONTROL_PAD__ASSOCIATED_PORT:
				return associatedPort != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == CombinationalBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (derivedFeatureID) {
				case PadsPackage.CONTROL_PAD__FLAGS: return DatapathPackage.FLAG_BEARER_BLOCK__FLAGS;
				default: return -1;
			}
		}
		if (baseClass == SingleFlagBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == CombinationalBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == FlagBearerBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.FLAG_BEARER_BLOCK__FLAGS: return PadsPackage.CONTROL_PAD__FLAGS;
				default: return -1;
			}
		}
		if (baseClass == SingleFlagBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		String width = "?";
		if (getFlags().size() > 0) {
			int w = getFlag(0).getWidth();
			width = Integer.toString(w);
		}
		result.append("Control Input Pad "+name+"["+width+"]");
		return result.toString();
	}

} //ControlPadImpl
