/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.util;

import fr.irisa.cairn.model.datapath.*;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.Operator;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.SubDatapath;
import fr.irisa.cairn.model.datapath.Wire;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.DatapathPackage
 * @generated
 */
public class DatapathSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DatapathPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatapathSwitch() {
		if (modelPackage == null) {
			modelPackage = DatapathPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DatapathPackage.ABSTRACT_DATAPATH: {
				AbstractDatapath abstractDatapath = (AbstractDatapath)theEObject;
				T result = caseAbstractDatapath(abstractDatapath);
				if (result == null) result = caseActivableBlock(abstractDatapath);
				if (result == null) result = caseDataFlowBlock(abstractDatapath);
				if (result == null) result = caseFlagBearerBlock(abstractDatapath);
				if (result == null) result = caseAbstractBlock(abstractDatapath);
				if (result == null) result = caseNamedElement(abstractDatapath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.DATAPATH: {
				Datapath datapath = (Datapath)theEObject;
				T result = caseDatapath(datapath);
				if (result == null) result = caseActivableBlock(datapath);
				if (result == null) result = caseDataFlowBlock(datapath);
				if (result == null) result = caseFlagBearerBlock(datapath);
				if (result == null) result = caseAbstractBlock(datapath);
				if (result == null) result = caseNamedElement(datapath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.ABSTRACT_BLOCK: {
				AbstractBlock abstractBlock = (AbstractBlock)theEObject;
				T result = caseAbstractBlock(abstractBlock);
				if (result == null) result = caseNamedElement(abstractBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.ACTIVABLE_BLOCK: {
				ActivableBlock activableBlock = (ActivableBlock)theEObject;
				T result = caseActivableBlock(activableBlock);
				if (result == null) result = caseAbstractBlock(activableBlock);
				if (result == null) result = caseNamedElement(activableBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.DATA_FLOW_BLOCK: {
				DataFlowBlock dataFlowBlock = (DataFlowBlock)theEObject;
				T result = caseDataFlowBlock(dataFlowBlock);
				if (result == null) result = caseAbstractBlock(dataFlowBlock);
				if (result == null) result = caseNamedElement(dataFlowBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.FLAG_BEARER_BLOCK: {
				FlagBearerBlock flagBearerBlock = (FlagBearerBlock)theEObject;
				T result = caseFlagBearerBlock(flagBearerBlock);
				if (result == null) result = caseAbstractBlock(flagBearerBlock);
				if (result == null) result = caseNamedElement(flagBearerBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.WIRE: {
				Wire wire = (Wire)theEObject;
				T result = caseWire(wire);
				if (result == null) result = caseNamedElement(wire);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.PORT: {
				Port port = (Port)theEObject;
				T result = casePort(port);
				if (result == null) result = caseNamedElement(port);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.OPERATOR: {
				Operator operator = (Operator)theEObject;
				T result = caseOperator(operator);
				if (result == null) result = caseDataFlowBlock(operator);
				if (result == null) result = caseAbstractBlock(operator);
				if (result == null) result = caseNamedElement(operator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.COMBINATIONAL_DATAPATH: {
				CombinationalDatapath combinationalDatapath = (CombinationalDatapath)theEObject;
				T result = caseCombinationalDatapath(combinationalDatapath);
				if (result == null) result = caseDatapath(combinationalDatapath);
				if (result == null) result = caseCombinationalBlock(combinationalDatapath);
				if (result == null) result = caseActivableBlock(combinationalDatapath);
				if (result == null) result = caseDataFlowBlock(combinationalDatapath);
				if (result == null) result = caseFlagBearerBlock(combinationalDatapath);
				if (result == null) result = caseAbstractBlock(combinationalDatapath);
				if (result == null) result = caseNamedElement(combinationalDatapath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.COMBINATIONAL_BLOCK: {
				CombinationalBlock combinationalBlock = (CombinationalBlock)theEObject;
				T result = caseCombinationalBlock(combinationalBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.SEQUENTIAL_BLOCK: {
				SequentialBlock sequentialBlock = (SequentialBlock)theEObject;
				T result = caseSequentialBlock(sequentialBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.PIPELINED_BLOCK: {
				PipelinedBlock pipelinedBlock = (PipelinedBlock)theEObject;
				T result = casePipelinedBlock(pipelinedBlock);
				if (result == null) result = caseSequentialBlock(pipelinedBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.MULTI_CYCLE_BLOCK: {
				MultiCycleBlock multiCycleBlock = (MultiCycleBlock)theEObject;
				T result = caseMultiCycleBlock(multiCycleBlock);
				if (result == null) result = caseSequentialBlock(multiCycleBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.PAD: {
				Pad pad = (Pad)theEObject;
				T result = casePad(pad);
				if (result == null) result = caseAbstractBlock(pad);
				if (result == null) result = caseNamedElement(pad);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.PIPELINED_DATAPATH: {
				PipelinedDatapath pipelinedDatapath = (PipelinedDatapath)theEObject;
				T result = casePipelinedDatapath(pipelinedDatapath);
				if (result == null) result = caseDatapath(pipelinedDatapath);
				if (result == null) result = casePipelinedBlock(pipelinedDatapath);
				if (result == null) result = caseActivableBlock(pipelinedDatapath);
				if (result == null) result = caseDataFlowBlock(pipelinedDatapath);
				if (result == null) result = caseFlagBearerBlock(pipelinedDatapath);
				if (result == null) result = caseSequentialBlock(pipelinedDatapath);
				if (result == null) result = caseAbstractBlock(pipelinedDatapath);
				if (result == null) result = caseNamedElement(pipelinedDatapath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.SEQUENTIAL_DATAPATH: {
				SequentialDatapath sequentialDatapath = (SequentialDatapath)theEObject;
				T result = caseSequentialDatapath(sequentialDatapath);
				if (result == null) result = caseDatapath(sequentialDatapath);
				if (result == null) result = caseSequentialBlock(sequentialDatapath);
				if (result == null) result = caseActivableBlock(sequentialDatapath);
				if (result == null) result = caseDataFlowBlock(sequentialDatapath);
				if (result == null) result = caseFlagBearerBlock(sequentialDatapath);
				if (result == null) result = caseAbstractBlock(sequentialDatapath);
				if (result == null) result = caseNamedElement(sequentialDatapath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.BLACK_BOX_BLOCK: {
				BlackBoxBlock blackBoxBlock = (BlackBoxBlock)theEObject;
				T result = caseBlackBoxBlock(blackBoxBlock);
				if (result == null) result = caseAbstractDatapath(blackBoxBlock);
				if (result == null) result = caseActivableBlock(blackBoxBlock);
				if (result == null) result = caseDataFlowBlock(blackBoxBlock);
				if (result == null) result = caseFlagBearerBlock(blackBoxBlock);
				if (result == null) result = caseAbstractBlock(blackBoxBlock);
				if (result == null) result = caseNamedElement(blackBoxBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.MOORE_SEQUENTIAL_BLOCK: {
				MooreSequentialBlock mooreSequentialBlock = (MooreSequentialBlock)theEObject;
				T result = caseMooreSequentialBlock(mooreSequentialBlock);
				if (result == null) result = caseSequentialBlock(mooreSequentialBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatapathPackage.MEALY_SEQUENTIAL_BLOCK: {
				MealySequentialBlock mealySequentialBlock = (MealySequentialBlock)theEObject;
				T result = caseMealySequentialBlock(mealySequentialBlock);
				if (result == null) result = caseSequentialBlock(mealySequentialBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Datapath</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractDatapath(AbstractDatapath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Datapath</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatapath(Datapath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractBlock(AbstractBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivableBlock(ActivableBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowBlock(DataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlagBearerBlock(FlagBearerBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wire</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wire</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWire(Wire object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperator(Operator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinational Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinational Datapath</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinationalDatapath(CombinationalDatapath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Combinational Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Combinational Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCombinationalBlock(CombinationalBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialBlock(SequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pipelined Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pipelined Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePipelinedBlock(PipelinedBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Cycle Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Cycle Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiCycleBlock(MultiCycleBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pad</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pad</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePad(Pad object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pipelined Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pipelined Datapath</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePipelinedDatapath(PipelinedDatapath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Datapath</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialDatapath(SequentialDatapath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Black Box Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Black Box Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlackBoxBlock(BlackBoxBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Moore Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Moore Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMooreSequentialBlock(MooreSequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMealySequentialBlock(MealySequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //DatapathSwitch
