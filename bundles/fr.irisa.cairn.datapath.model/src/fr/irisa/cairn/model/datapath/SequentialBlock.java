/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import fr.irisa.cairn.model.datapath.port.OutControlPort;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequential Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getSequentialBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SequentialBlock extends EObject {

} // SequentialBlock
