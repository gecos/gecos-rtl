/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pipelined Datapath</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getPipelinedDatapath()
 * @model
 * @generated
 */
public interface PipelinedDatapath extends Datapath, PipelinedBlock {
} // PipelinedDatapath
