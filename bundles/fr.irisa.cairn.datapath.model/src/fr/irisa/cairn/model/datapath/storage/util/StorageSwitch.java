/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;
import fr.irisa.cairn.model.datapath.MooreSequentialBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.SequentialBlock;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.storage.*;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage
 * @generated
 */
public class StorageSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static StoragePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSwitch() {
		if (modelPackage == null) {
			modelPackage = StoragePackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case StoragePackage.REGISTER: {
				Register register = (Register)theEObject;
				T result = caseRegister(register);
				if (result == null) result = caseSingleOutputDataFlowBlock(register);
				if (result == null) result = caseSingleInputDataFlowBlock(register);
				if (result == null) result = caseDataFlowBlock(register);
				if (result == null) result = caseAbstractBlock(register);
				if (result == null) result = caseNamedElement(register);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.CE_REGISTER: {
				CERegister ceRegister = (CERegister)theEObject;
				T result = caseCERegister(ceRegister);
				if (result == null) result = caseRegister(ceRegister);
				if (result == null) result = caseSingleControlPortBlock(ceRegister);
				if (result == null) result = caseActivableBlock(ceRegister);
				if (result == null) result = caseSingleOutputDataFlowBlock(ceRegister);
				if (result == null) result = caseSingleInputDataFlowBlock(ceRegister);
				if (result == null) result = caseAbstractBlock(ceRegister);
				if (result == null) result = caseNamedElement(ceRegister);
				if (result == null) result = caseDataFlowBlock(ceRegister);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.SHIFT_REGISTER: {
				ShiftRegister shiftRegister = (ShiftRegister)theEObject;
				T result = caseShiftRegister(shiftRegister);
				if (result == null) result = caseCERegister(shiftRegister);
				if (result == null) result = caseRegister(shiftRegister);
				if (result == null) result = caseSingleControlPortBlock(shiftRegister);
				if (result == null) result = caseActivableBlock(shiftRegister);
				if (result == null) result = caseSingleOutputDataFlowBlock(shiftRegister);
				if (result == null) result = caseSingleInputDataFlowBlock(shiftRegister);
				if (result == null) result = caseAbstractBlock(shiftRegister);
				if (result == null) result = caseNamedElement(shiftRegister);
				if (result == null) result = caseDataFlowBlock(shiftRegister);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.ABSTRACT_MEMORY: {
				AbstractMemory abstractMemory = (AbstractMemory)theEObject;
				T result = caseAbstractMemory(abstractMemory);
				if (result == null) result = caseActivableBlock(abstractMemory);
				if (result == null) result = caseDataFlowBlock(abstractMemory);
				if (result == null) result = caseAbstractBlock(abstractMemory);
				if (result == null) result = caseNamedElement(abstractMemory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.SINGLE_PORT_RAM: {
				SinglePortRam singlePortRam = (SinglePortRam)theEObject;
				T result = caseSinglePortRam(singlePortRam);
				if (result == null) result = caseAbstractMemory(singlePortRam);
				if (result == null) result = caseSingleOutputDataFlowBlock(singlePortRam);
				if (result == null) result = caseSyncReadMemory(singlePortRam);
				if (result == null) result = caseActivableBlock(singlePortRam);
				if (result == null) result = caseDataFlowBlock(singlePortRam);
				if (result == null) result = caseMooreSequentialBlock(singlePortRam);
				if (result == null) result = caseAbstractBlock(singlePortRam);
				if (result == null) result = caseSequentialBlock(singlePortRam);
				if (result == null) result = caseNamedElement(singlePortRam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.DUAL_PORT_RAM: {
				DualPortRam dualPortRam = (DualPortRam)theEObject;
				T result = caseDualPortRam(dualPortRam);
				if (result == null) result = caseMultiPortRam(dualPortRam);
				if (result == null) result = caseAbstractMemory(dualPortRam);
				if (result == null) result = caseActivableBlock(dualPortRam);
				if (result == null) result = caseDataFlowBlock(dualPortRam);
				if (result == null) result = caseAbstractBlock(dualPortRam);
				if (result == null) result = caseNamedElement(dualPortRam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.MULTI_PORT_RAM: {
				MultiPortRam multiPortRam = (MultiPortRam)theEObject;
				T result = caseMultiPortRam(multiPortRam);
				if (result == null) result = caseAbstractMemory(multiPortRam);
				if (result == null) result = caseActivableBlock(multiPortRam);
				if (result == null) result = caseDataFlowBlock(multiPortRam);
				if (result == null) result = caseAbstractBlock(multiPortRam);
				if (result == null) result = caseNamedElement(multiPortRam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.SINGLE_PORT_ROM: {
				SinglePortRom singlePortRom = (SinglePortRom)theEObject;
				T result = caseSinglePortRom(singlePortRom);
				if (result == null) result = caseAbstractMemory(singlePortRom);
				if (result == null) result = caseSingleOutputDataFlowBlock(singlePortRom);
				if (result == null) result = caseActivableBlock(singlePortRom);
				if (result == null) result = caseDataFlowBlock(singlePortRom);
				if (result == null) result = caseAbstractBlock(singlePortRom);
				if (result == null) result = caseNamedElement(singlePortRom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.MULTI_PORT_ROM: {
				MultiPortRom multiPortRom = (MultiPortRom)theEObject;
				T result = caseMultiPortRom(multiPortRom);
				if (result == null) result = caseAbstractMemory(multiPortRom);
				if (result == null) result = caseActivableBlock(multiPortRom);
				if (result == null) result = caseDataFlowBlock(multiPortRom);
				if (result == null) result = caseAbstractBlock(multiPortRom);
				if (result == null) result = caseNamedElement(multiPortRom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.ASYNC_READ_MEMORY: {
				AsyncReadMemory asyncReadMemory = (AsyncReadMemory)theEObject;
				T result = caseAsyncReadMemory(asyncReadMemory);
				if (result == null) result = caseMealySequentialBlock(asyncReadMemory);
				if (result == null) result = caseSequentialBlock(asyncReadMemory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.SYNC_READ_MEMORY: {
				SyncReadMemory syncReadMemory = (SyncReadMemory)theEObject;
				T result = caseSyncReadMemory(syncReadMemory);
				if (result == null) result = caseMooreSequentialBlock(syncReadMemory);
				if (result == null) result = caseSequentialBlock(syncReadMemory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.SYNC_READ_SPRAM: {
				SyncReadSPRAM syncReadSPRAM = (SyncReadSPRAM)theEObject;
				T result = caseSyncReadSPRAM(syncReadSPRAM);
				if (result == null) result = caseSinglePortRam(syncReadSPRAM);
				if (result == null) result = caseAbstractMemory(syncReadSPRAM);
				if (result == null) result = caseSingleOutputDataFlowBlock(syncReadSPRAM);
				if (result == null) result = caseSyncReadMemory(syncReadSPRAM);
				if (result == null) result = caseActivableBlock(syncReadSPRAM);
				if (result == null) result = caseDataFlowBlock(syncReadSPRAM);
				if (result == null) result = caseMooreSequentialBlock(syncReadSPRAM);
				if (result == null) result = caseAbstractBlock(syncReadSPRAM);
				if (result == null) result = caseSequentialBlock(syncReadSPRAM);
				if (result == null) result = caseNamedElement(syncReadSPRAM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.ASYNC_READ_SPRAM: {
				AsyncReadSPRAM asyncReadSPRAM = (AsyncReadSPRAM)theEObject;
				T result = caseAsyncReadSPRAM(asyncReadSPRAM);
				if (result == null) result = caseSinglePortRam(asyncReadSPRAM);
				if (result == null) result = caseAsyncReadMemory(asyncReadSPRAM);
				if (result == null) result = caseAbstractMemory(asyncReadSPRAM);
				if (result == null) result = caseSingleOutputDataFlowBlock(asyncReadSPRAM);
				if (result == null) result = caseSyncReadMemory(asyncReadSPRAM);
				if (result == null) result = caseMealySequentialBlock(asyncReadSPRAM);
				if (result == null) result = caseActivableBlock(asyncReadSPRAM);
				if (result == null) result = caseDataFlowBlock(asyncReadSPRAM);
				if (result == null) result = caseMooreSequentialBlock(asyncReadSPRAM);
				if (result == null) result = caseAbstractBlock(asyncReadSPRAM);
				if (result == null) result = caseSequentialBlock(asyncReadSPRAM);
				if (result == null) result = caseNamedElement(asyncReadSPRAM);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case StoragePackage.REGISTER_FILE: {
				RegisterFile registerFile = (RegisterFile)theEObject;
				T result = caseRegisterFile(registerFile);
				if (result == null) result = caseCERegister(registerFile);
				if (result == null) result = caseRegister(registerFile);
				if (result == null) result = caseSingleControlPortBlock(registerFile);
				if (result == null) result = caseActivableBlock(registerFile);
				if (result == null) result = caseSingleOutputDataFlowBlock(registerFile);
				if (result == null) result = caseSingleInputDataFlowBlock(registerFile);
				if (result == null) result = caseAbstractBlock(registerFile);
				if (result == null) result = caseNamedElement(registerFile);
				if (result == null) result = caseDataFlowBlock(registerFile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Register</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Register</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegister(Register object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CE Register</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CE Register</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCERegister(CERegister object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shift Register</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shift Register</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShiftRegister(ShiftRegister object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Memory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Memory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractMemory(AbstractMemory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Port Ram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Port Ram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSinglePortRam(SinglePortRam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dual Port Ram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dual Port Ram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDualPortRam(DualPortRam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Port Ram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Port Ram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiPortRam(MultiPortRam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Port Rom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Port Rom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSinglePortRom(SinglePortRom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multi Port Rom</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multi Port Rom</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiPortRom(MultiPortRom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Async Read Memory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Async Read Memory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsyncReadMemory(AsyncReadMemory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Read Memory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Read Memory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncReadMemory(SyncReadMemory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sync Read SPRAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sync Read SPRAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSyncReadSPRAM(SyncReadSPRAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Async Read SPRAM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Async Read SPRAM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsyncReadSPRAM(AsyncReadSPRAM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Register File</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Register File</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegisterFile(RegisterFile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractBlock(AbstractBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivableBlock(ActivableBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Control Port Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Control Port Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleControlPortBlock(SingleControlPortBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialBlock(SequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Moore Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Moore Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMooreSequentialBlock(MooreSequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMealySequentialBlock(MealySequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataFlowBlock(DataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Output Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Output Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Input Data Flow Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Input Data Flow Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //StorageSwitch
