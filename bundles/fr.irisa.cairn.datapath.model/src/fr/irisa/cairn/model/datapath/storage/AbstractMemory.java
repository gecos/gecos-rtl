/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Memory</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.storage.AbstractMemory#getContent <em>Content</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getAbstractMemory()
 * @model
 * @generated
 */
public interface AbstractMemory extends ActivableBlock, DataFlowBlock {
	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(byte[])
	 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getAbstractMemory_Content()
	 * @model required="true"
	 * @generated
	 */
	byte[] getContent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.storage.AbstractMemory#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(byte[] value);

} // AbstractMemory
