/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Status Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.StatusPad#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getStatusPad()
 * @model
 * @generated
 */
public interface StatusPad extends OutputPad, SingleControlPortBlock {
	/**
	 * Returns the value of the '<em><b>Associated Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Port</em>' reference.
	 * @see #setAssociatedPort(OutControlPort)
	 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getStatusPad_AssociatedPort()
	 * @model required="true"
	 * @generated
	 */
	OutControlPort getAssociatedPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.pads.StatusPad#getAssociatedPort <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Port</em>' reference.
	 * @see #getAssociatedPort()
	 * @generated
	 */
	void setAssociatedPort(OutControlPort value);

} // StatusPad
