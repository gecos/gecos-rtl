/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Data Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutDataPort()
 * @model
 * @generated
 */
public interface OutDataPort extends Port {

	/**
	 * Returns the value of the '<em><b>Wires</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.wires.DataFlowWire}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wires</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wires</em>' reference list.
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutDataPort_Wires()
	 * @see fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<DataFlowWire> getWires();

	/**
	 * Returns the value of the '<em><b>Associated Pad</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Pad</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Pad</em>' reference.
	 * @see #setAssociatedPad(DataOutputPad)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutDataPort_AssociatedPad()
	 * @see fr.irisa.cairn.model.datapath.pads.DataOutputPad#getAssociatedPort
	 * @model opposite="associatedPort"
	 * @generated
	 */
	DataOutputPad getAssociatedPad();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getAssociatedPad <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Pad</em>' reference.
	 * @see #getAssociatedPad()
	 * @generated
	 */
	void setAssociatedPad(DataOutputPad value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	DataFlowWire connect(SingleInputDataFlowBlock sink);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	DataFlowWire connect(InDataPort sink);

	DataFlowBlock getParentNode();

} // OutDataPort
