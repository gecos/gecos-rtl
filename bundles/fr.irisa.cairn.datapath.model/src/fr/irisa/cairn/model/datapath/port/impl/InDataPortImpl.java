/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;


import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.PortImpl;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>In Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl#getWire <em>Wire</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.InDataPortImpl#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InDataPortImpl extends PortImpl implements InDataPort {

	public String toString() {
		if (getParentNode()!=null) {
			return getParentNode().getName()+"."+getName()+"["+(getWidth()-1)+":0]";
		} else {
			return "#."+getName()+"["+(getWidth()-1)+":0]";
		}
	}


	/**
	 * The cached value of the '{@link #getWire() <em>Wire</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWire()
	 * @generated
	 * @ordered
	 */
	protected DataFlowWire wire;

	/**
	 * The cached value of the '{@link #getAssociatedPad() <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPad()
	 * @generated
	 * @ordered
	 */
	protected DataInputPad associatedPad;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.IN_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire getWire() {
		if (wire != null && wire.eIsProxy()) {
			InternalEObject oldWire = (InternalEObject)wire;
			wire = (DataFlowWire)eResolveProxy(oldWire);
			if (wire != oldWire) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.IN_DATA_PORT__WIRE, oldWire, wire));
			}
		}
		return wire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire basicGetWire() {
		return wire;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWire(DataFlowWire newWire, NotificationChain msgs) {
		DataFlowWire oldWire = wire;
		wire = newWire;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortPackage.IN_DATA_PORT__WIRE, oldWire, newWire);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWire(DataFlowWire newWire) {
		if (newWire != wire) {
			NotificationChain msgs = null;
			if (wire != null)
				msgs = ((InternalEObject)wire).eInverseRemove(this, WiresPackage.DATA_FLOW_WIRE__SINK, DataFlowWire.class, msgs);
			if (newWire != null)
				msgs = ((InternalEObject)newWire).eInverseAdd(this, WiresPackage.DATA_FLOW_WIRE__SINK, DataFlowWire.class, msgs);
			msgs = basicSetWire(newWire, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.IN_DATA_PORT__WIRE, newWire, newWire));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DataFlowBlock basicGetParentNode() {
		if(eContainer() instanceof DataFlowBlock) {
			return (DataFlowBlock) eContainer();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DataFlowBlock  getParentNode() {
		return basicGetParentNode();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInputPad getAssociatedPad() {
		if (associatedPad != null && associatedPad.eIsProxy()) {
			InternalEObject oldAssociatedPad = (InternalEObject)associatedPad;
			associatedPad = (DataInputPad)eResolveProxy(oldAssociatedPad);
			if (associatedPad != oldAssociatedPad) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.IN_DATA_PORT__ASSOCIATED_PAD, oldAssociatedPad, associatedPad));
			}
		}
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInputPad basicGetAssociatedPad() {
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedPad(DataInputPad newAssociatedPad, NotificationChain msgs) {
		DataInputPad oldAssociatedPad = associatedPad;
		associatedPad = newAssociatedPad;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortPackage.IN_DATA_PORT__ASSOCIATED_PAD, oldAssociatedPad, newAssociatedPad);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPad(DataInputPad newAssociatedPad) {
		if (newAssociatedPad != associatedPad) {
			NotificationChain msgs = null;
			if (associatedPad != null)
				msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.DATA_INPUT_PAD__ASSOCIATED_PORT, DataInputPad.class, msgs);
			if (newAssociatedPad != null)
				msgs = ((InternalEObject)newAssociatedPad).eInverseAdd(this, PadsPackage.DATA_INPUT_PAD__ASSOCIATED_PORT, DataInputPad.class, msgs);
			msgs = basicSetAssociatedPad(newAssociatedPad, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.IN_DATA_PORT__ASSOCIATED_PAD, newAssociatedPad, newAssociatedPad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getSourcePort() {
		return wire.getSource();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleOutputDataFlowBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(OutDataPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				if (wire != null)
					msgs = ((InternalEObject)wire).eInverseRemove(this, WiresPackage.DATA_FLOW_WIRE__SINK, DataFlowWire.class, msgs);
				return basicSetWire((DataFlowWire)otherEnd, msgs);
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				if (associatedPad != null)
					msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.DATA_INPUT_PAD__ASSOCIATED_PORT, DataInputPad.class, msgs);
				return basicSetAssociatedPad((DataInputPad)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				return basicSetWire(null, msgs);
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				return basicSetAssociatedPad(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				if (resolve) return getWire();
				return basicGetWire();
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				if (resolve) return getAssociatedPad();
				return basicGetAssociatedPad();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				setWire((DataFlowWire)newValue);
				return;
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				setAssociatedPad((DataInputPad)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				setWire((DataFlowWire)null);
				return;
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				setAssociatedPad((DataInputPad)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.IN_DATA_PORT__WIRE:
				return wire != null;
			case PortPackage.IN_DATA_PORT__ASSOCIATED_PAD:
				return associatedPad != null;
		}
		return super.eIsSet(featureID);
	}


} //InDataPortImpl
