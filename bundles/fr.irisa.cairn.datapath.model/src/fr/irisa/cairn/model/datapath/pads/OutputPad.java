/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads;

import fr.irisa.cairn.model.datapath.CombinationalBlock;
import fr.irisa.cairn.model.datapath.Pad;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Pad</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.pads.PadsPackage#getOutputPad()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface OutputPad extends Pad, CombinationalBlock {
} // OutputPad
