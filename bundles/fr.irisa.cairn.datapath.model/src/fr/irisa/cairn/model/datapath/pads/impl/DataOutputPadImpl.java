/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.pads.impl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Output Pad</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.pads.impl.DataOutputPadImpl#getAssociatedPort <em>Associated Port</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataOutputPadImpl extends NamedElementImpl implements DataOutputPad {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;
	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;
	/**
	 * The cached value of the '{@link #getAssociatedPort() <em>Associated Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPort()
	 * @generated
	 * @ordered
	 */
	protected OutDataPort associatedPort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataOutputPadImpl() {
		super();
	}

	public void setParent(Datapath newParent) {
		newParent.getComponents().add(this);
		newParent.getOut().add(getAssociatedPort());
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCombinational() {
		// TODO: implement this method to return the 'Combinational' attribute
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, PadsPackage.DATA_OUTPUT_PAD__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, PadsPackage.DATA_OUTPUT_PAD__OUT);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PadsPackage.Literals.DATA_OUTPUT_PAD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != PadsPackage.DATA_OUTPUT_PAD__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, PadsPackage.DATA_OUTPUT_PAD__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getAssociatedPort() {
		if (associatedPort != null && associatedPort.eIsProxy()) {
			InternalEObject oldAssociatedPort = (InternalEObject)associatedPort;
			associatedPort = (OutDataPort)eResolveProxy(oldAssociatedPort);
			if (associatedPort != oldAssociatedPort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, oldAssociatedPort, associatedPort));
			}
		}
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort basicGetAssociatedPort() {
		return associatedPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedPort(OutDataPort newAssociatedPort, NotificationChain msgs) {
		OutDataPort oldAssociatedPort = associatedPort;
		associatedPort = newAssociatedPort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, oldAssociatedPort, newAssociatedPort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPort(OutDataPort newAssociatedPort) {
		if (newAssociatedPort != associatedPort) {
			NotificationChain msgs = null;
			if (associatedPort != null)
				msgs = ((InternalEObject)associatedPort).eInverseRemove(this, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, OutDataPort.class, msgs);
			if (newAssociatedPort != null)
				msgs = ((InternalEObject)newAssociatedPort).eInverseAdd(this, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, OutDataPort.class, msgs);
			msgs = basicSetAssociatedPort(newAssociatedPort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, newAssociatedPort, newAssociatedPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput() {
		return getIn().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleOutputDataFlowBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(OutDataPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBlock getSourceBlock() {
		try { 
			return getInput().getWire().getSource().getParentNode(); 
		} catch(NullPointerException e) {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				if (associatedPort != null)
					msgs = ((InternalEObject)associatedPort).eInverseRemove(this, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, OutDataPort.class, msgs);
				return basicSetAssociatedPort((OutDataPort)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				return basicSetParent(null, msgs);
			case PadsPackage.DATA_OUTPUT_PAD__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case PadsPackage.DATA_OUTPUT_PAD__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				return basicSetAssociatedPort(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				return getParent();
			case PadsPackage.DATA_OUTPUT_PAD__COMBINATIONAL:
				return isCombinational();
			case PadsPackage.DATA_OUTPUT_PAD__IN:
				return getIn();
			case PadsPackage.DATA_OUTPUT_PAD__OUT:
				return getOut();
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				if (resolve) return getAssociatedPort();
				return basicGetAssociatedPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				setParent((Datapath)newValue);
				return;
			case PadsPackage.DATA_OUTPUT_PAD__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case PadsPackage.DATA_OUTPUT_PAD__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				setAssociatedPort((OutDataPort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				setParent((Datapath)null);
				return;
			case PadsPackage.DATA_OUTPUT_PAD__IN:
				unsetIn();
				return;
			case PadsPackage.DATA_OUTPUT_PAD__OUT:
				getOut().clear();
				return;
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				setAssociatedPort((OutDataPort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PadsPackage.DATA_OUTPUT_PAD__PARENT:
				return getParent() != null;
			case PadsPackage.DATA_OUTPUT_PAD__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case PadsPackage.DATA_OUTPUT_PAD__IN:
				return isSetIn();
			case PadsPackage.DATA_OUTPUT_PAD__OUT:
				return out != null && !out.isEmpty();
			case PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT:
				return associatedPort != null;
		}
		return super.eIsSet(featureID);
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("Data Output Pad "+name+"["+getInput().getWidth());
		return result.toString();
	}

} //DataOutputPadImpl
