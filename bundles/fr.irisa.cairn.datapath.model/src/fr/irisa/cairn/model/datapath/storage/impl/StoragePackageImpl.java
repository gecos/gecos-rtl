/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.AsyncReadMemory;
import fr.irisa.cairn.model.datapath.storage.AsyncReadSPRAM;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.RegisterFile;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.StorageFactory;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.SyncReadMemory;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StoragePackageImpl extends EPackageImpl implements StoragePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass registerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ceRegisterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shiftRegisterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractMemoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singlePortRamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dualPortRamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiPortRamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singlePortRomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiPortRomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asyncReadMemoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncReadMemoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass syncReadSPRAMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass asyncReadSPRAMEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass registerFileEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StoragePackageImpl() {
		super(eNS_URI, StorageFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link StoragePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StoragePackage init() {
		if (isInited) return (StoragePackage)EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI);

		// Obtain or create and register package
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new StoragePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		theStoragePackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		thePadsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		theStoragePackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		thePadsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theStoragePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StoragePackage.eNS_URI, theStoragePackage);
		return theStoragePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegister() {
		return registerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCERegister() {
		return ceRegisterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShiftRegister() {
		return shiftRegisterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShiftRegister_Depth() {
		return (EAttribute)shiftRegisterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractMemory() {
		return abstractMemoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractMemory_Content() {
		return (EAttribute)abstractMemoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSinglePortRam() {
		return singlePortRamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDualPortRam() {
		return dualPortRamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiPortRam() {
		return multiPortRamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiPortRam_NbReadPort() {
		return (EAttribute)multiPortRamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiPortRam_NbWritePort() {
		return (EAttribute)multiPortRamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSinglePortRom() {
		return singlePortRomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiPortRom() {
		return multiPortRomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiPortRom_NbReadPort() {
		return (EAttribute)multiPortRomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsyncReadMemory() {
		return asyncReadMemoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncReadMemory() {
		return syncReadMemoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSyncReadSPRAM() {
		return syncReadSPRAMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsyncReadSPRAM() {
		return asyncReadSPRAMEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegisterFile() {
		return registerFileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageFactory getStorageFactory() {
		return (StorageFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		registerEClass = createEClass(REGISTER);

		ceRegisterEClass = createEClass(CE_REGISTER);

		shiftRegisterEClass = createEClass(SHIFT_REGISTER);
		createEAttribute(shiftRegisterEClass, SHIFT_REGISTER__DEPTH);

		abstractMemoryEClass = createEClass(ABSTRACT_MEMORY);
		createEAttribute(abstractMemoryEClass, ABSTRACT_MEMORY__CONTENT);

		singlePortRamEClass = createEClass(SINGLE_PORT_RAM);

		dualPortRamEClass = createEClass(DUAL_PORT_RAM);

		multiPortRamEClass = createEClass(MULTI_PORT_RAM);
		createEAttribute(multiPortRamEClass, MULTI_PORT_RAM__NB_READ_PORT);
		createEAttribute(multiPortRamEClass, MULTI_PORT_RAM__NB_WRITE_PORT);

		singlePortRomEClass = createEClass(SINGLE_PORT_ROM);

		multiPortRomEClass = createEClass(MULTI_PORT_ROM);
		createEAttribute(multiPortRomEClass, MULTI_PORT_ROM__NB_READ_PORT);

		asyncReadMemoryEClass = createEClass(ASYNC_READ_MEMORY);

		syncReadMemoryEClass = createEClass(SYNC_READ_MEMORY);

		syncReadSPRAMEClass = createEClass(SYNC_READ_SPRAM);

		asyncReadSPRAMEClass = createEClass(ASYNC_READ_SPRAM);

		registerFileEClass = createEClass(REGISTER_FILE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);
		OperatorsPackage theOperatorsPackage = (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		registerEClass.getESuperTypes().add(theDatapathPackage.getDataFlowBlock());
		registerEClass.getESuperTypes().add(theOperatorsPackage.getSingleOutputDataFlowBlock());
		registerEClass.getESuperTypes().add(theOperatorsPackage.getSingleInputDataFlowBlock());
		ceRegisterEClass.getESuperTypes().add(theDatapathPackage.getActivableBlock());
		ceRegisterEClass.getESuperTypes().add(this.getRegister());
		ceRegisterEClass.getESuperTypes().add(theOperatorsPackage.getSingleControlPortBlock());
		shiftRegisterEClass.getESuperTypes().add(this.getCERegister());
		abstractMemoryEClass.getESuperTypes().add(theDatapathPackage.getActivableBlock());
		abstractMemoryEClass.getESuperTypes().add(theDatapathPackage.getDataFlowBlock());
		singlePortRamEClass.getESuperTypes().add(this.getAbstractMemory());
		singlePortRamEClass.getESuperTypes().add(theOperatorsPackage.getSingleOutputDataFlowBlock());
		singlePortRamEClass.getESuperTypes().add(this.getSyncReadMemory());
		dualPortRamEClass.getESuperTypes().add(this.getMultiPortRam());
		multiPortRamEClass.getESuperTypes().add(this.getAbstractMemory());
		singlePortRomEClass.getESuperTypes().add(this.getAbstractMemory());
		singlePortRomEClass.getESuperTypes().add(theOperatorsPackage.getSingleOutputDataFlowBlock());
		multiPortRomEClass.getESuperTypes().add(this.getAbstractMemory());
		asyncReadMemoryEClass.getESuperTypes().add(theDatapathPackage.getMealySequentialBlock());
		syncReadMemoryEClass.getESuperTypes().add(theDatapathPackage.getMooreSequentialBlock());
		syncReadSPRAMEClass.getESuperTypes().add(this.getSinglePortRam());
		syncReadSPRAMEClass.getESuperTypes().add(this.getSyncReadMemory());
		asyncReadSPRAMEClass.getESuperTypes().add(this.getSinglePortRam());
		asyncReadSPRAMEClass.getESuperTypes().add(this.getAsyncReadMemory());
		registerFileEClass.getESuperTypes().add(this.getCERegister());

		// Initialize classes and features; add operations and parameters
		initEClass(registerEClass, Register.class, "Register", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(registerEClass, thePortPackage.getInDataPort(), "getWritePort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(registerEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ceRegisterEClass, CERegister.class, "CERegister", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(ceRegisterEClass, thePortPackage.getInControlPort(), "getLoadEnable", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(shiftRegisterEClass, ShiftRegister.class, "ShiftRegister", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getShiftRegister_Depth(), ecorePackage.getEInt(), "depth", null, 1, 1, ShiftRegister.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(shiftRegisterEClass, thePortPackage.getInDataPort(), "getWritePort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(shiftRegisterEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(shiftRegisterEClass, thePortPackage.getInControlPort(), "getShiftEnable", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(abstractMemoryEClass, AbstractMemory.class, "AbstractMemory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractMemory_Content(), ecorePackage.getEByteArray(), "content", null, 1, 1, AbstractMemory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(singlePortRamEClass, SinglePortRam.class, "SinglePortRam", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singlePortRamEClass, thePortPackage.getInControlPort(), "getWriteEnable", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRamEClass, thePortPackage.getInControlPort(), "getReadEnable", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRamEClass, thePortPackage.getInDataPort(), "getAddressPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRamEClass, thePortPackage.getInDataPort(), "getWritePort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRamEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dualPortRamEClass, DualPortRam.class, "DualPortRam", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(multiPortRamEClass, MultiPortRam.class, "MultiPortRam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiPortRam_NbReadPort(), ecorePackage.getEInt(), "nbReadPort", "1", 1, 1, MultiPortRam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMultiPortRam_NbWritePort(), ecorePackage.getEInt(), "nbWritePort", "1", 1, 1, MultiPortRam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(multiPortRamEClass, thePortPackage.getInControlPort(), "getWriteEnable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRamEClass, thePortPackage.getInControlPort(), "getReadEnable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRamEClass, thePortPackage.getInDataPort(), "getAddressPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRamEClass, thePortPackage.getInDataPort(), "getWritePort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRamEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(singlePortRomEClass, SinglePortRom.class, "SinglePortRom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(singlePortRomEClass, thePortPackage.getInControlPort(), "getReadEnable", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRomEClass, thePortPackage.getInDataPort(), "getAddressPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(singlePortRomEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(multiPortRomEClass, MultiPortRom.class, "MultiPortRom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiPortRom_NbReadPort(), ecorePackage.getEInt(), "nbReadPort", "1", 1, 1, MultiPortRom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(multiPortRomEClass, thePortPackage.getInControlPort(), "getReadEnable", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRomEClass, thePortPackage.getInDataPort(), "getAddressPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(multiPortRomEClass, thePortPackage.getOutDataPort(), "getReadPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "portId", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(asyncReadMemoryEClass, AsyncReadMemory.class, "AsyncReadMemory", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(syncReadMemoryEClass, SyncReadMemory.class, "SyncReadMemory", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(syncReadSPRAMEClass, SyncReadSPRAM.class, "SyncReadSPRAM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(asyncReadSPRAMEClass, AsyncReadSPRAM.class, "AsyncReadSPRAM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(registerFileEClass, RegisterFile.class, "RegisterFile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //StoragePackageImpl
