/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>In Control Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.InControlPort#getWire <em>Wire</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInControlPort()
 * @model
 * @generated
 */
public interface InControlPort extends Port {

	/**
	 * Returns the value of the '<em><b>Wire</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink <em>Sink</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wire</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wire</em>' reference.
	 * @see #setWire(ControlFlowWire)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInControlPort_Wire()
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink
	 * @model opposite="sink"
	 * @generated
	 */
	ControlFlowWire getWire();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getWire <em>Wire</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wire</em>' reference.
	 * @see #getWire()
	 * @generated
	 */
	void setWire(ControlFlowWire value);

	/**
	 * Returns the value of the '<em><b>Associated Pad</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort <em>Associated Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Pad</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Pad</em>' reference.
	 * @see #setAssociatedPad(ControlPad)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getInControlPort_AssociatedPad()
	 * @see fr.irisa.cairn.model.datapath.pads.ControlPad#getAssociatedPort
	 * @model opposite="associatedPort"
	 * @generated
	 */
	ControlPad getAssociatedPad();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getAssociatedPad <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Pad</em>' reference.
	 * @see #getAssociatedPad()
	 * @generated
	 */
	void setAssociatedPad(ControlPad value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return wire.getSource();'"
	 * @generated
	 */
	OutControlPort getSourcePort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	ControlFlowWire connect(SingleFlagBlock source);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,source);'"
	 * @generated
	 */
	ControlFlowWire connect(OutControlPort source);
	
	ActivableBlock getParentNode();

} // InControlPort
