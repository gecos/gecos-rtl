/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.PipelinedBlock;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pipelined Reduction Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator#getOpcode <em>Opcode</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getPipelinedReductionOperator()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints=' consistentPortNumber'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentPortNumber='self.in.size()=2 and self.out.size()=1 '"
 * @generated
 */
public interface PipelinedReductionOperator extends SingleOutputDataFlowBlock, PipelinedBlock {
	/**
	 * Returns the value of the '<em><b>Opcode</b></em>' attribute.
	 * The default value is <code>"\"add\""</code>.
	 * The literals are from the enumeration {@link fr.irisa.cairn.model.datapath.operators.ReductionOpcode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Opcode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOpcode
	 * @see #setOpcode(ReductionOpcode)
	 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getPipelinedReductionOperator_Opcode()
	 * @model default="\"add\"" required="true"
	 * @generated
	 */
	ReductionOpcode getOpcode();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.operators.PipelinedReductionOperator#getOpcode <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opcode</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.ReductionOpcode
	 * @see #getOpcode()
	 * @generated
	 */
	void setOpcode(ReductionOpcode value);

} // PipelinedReductionOperator
