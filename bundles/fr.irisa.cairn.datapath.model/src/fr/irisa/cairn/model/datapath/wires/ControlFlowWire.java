/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Flow Wire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink <em>Sink</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getControlFlowWire()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentInputPortType consistentOutputPortType'"
 * @generated
 */
public interface ControlFlowWire extends Wire {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getWires <em>Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(OutControlPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getControlFlowWire_Source()
	 * @see fr.irisa.cairn.model.datapath.port.OutControlPort#getWires
	 * @model opposite="wires" required="true"
	 * @generated
	 */
	OutControlPort getSource();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(OutControlPort value);

	/**
	 * Returns the value of the '<em><b>Sink</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.InControlPort#getWire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink</em>' reference.
	 * @see #setSink(InControlPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getControlFlowWire_Sink()
	 * @see fr.irisa.cairn.model.datapath.port.InControlPort#getWire
	 * @model opposite="wire" required="true"
	 * @generated
	 */
	InControlPort getSink();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSink <em>Sink</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink</em>' reference.
	 * @see #getSink()
	 * @generated
	 */
	void setSink(InControlPort value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.Datapath#getControlWires <em>Control Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Datapath)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getControlFlowWire_Parent()
	 * @see fr.irisa.cairn.model.datapath.Datapath#getControlWires
	 * @model opposite="controlWires" required="true" transient="false"
	 * @generated
	 */
	Datapath getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Datapath value);

} // ControlFlowWire
