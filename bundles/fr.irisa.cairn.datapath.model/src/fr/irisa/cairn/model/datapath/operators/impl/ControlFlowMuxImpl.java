/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.impl;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Flow Mux</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.ControlFlowMuxImpl#getActivate <em>Activate</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ControlFlowMuxImpl extends NamedElementImpl implements ControlFlowMux {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated NOT
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;
	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;
	/**
	 * The cached value of the '{@link #getActivate() <em>Activate</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivate()
	 * @generated
	 * @ordered
	 */
	protected EList<InControlPort> activate;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ControlFlowMuxImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorsPackage.Literals.CONTROL_FLOW_MUX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != OperatorsPackage.CONTROL_FLOW_MUX__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, OperatorsPackage.CONTROL_FLOW_MUX__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != OperatorsPackage.CONTROL_FLOW_MUX__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.CONTROL_FLOW_MUX__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isCombinational() {
		return COMBINATIONAL_EDEFAULT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InControlPort> getActivate() {
		if (activate == null) {
			activate = new EObjectContainmentEList<InControlPort>(InControlPort.class, this, OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE);
		}
		return activate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControl() {
		return getActivate().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleFlagBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(OutControlPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, OperatorsPackage.CONTROL_FLOW_MUX__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, OperatorsPackage.CONTROL_FLOW_MUX__OUT);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput() {
		return getOut().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleInputDataFlowBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(InDataPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InControlPort getControlPort(int pos) {
		return getActivate().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				return basicSetParent(null, msgs);
			case OperatorsPackage.CONTROL_FLOW_MUX__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case OperatorsPackage.CONTROL_FLOW_MUX__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE:
				return ((InternalEList<?>)getActivate()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				return getParent();
			case OperatorsPackage.CONTROL_FLOW_MUX__COMBINATIONAL:
				return isCombinational();
			case OperatorsPackage.CONTROL_FLOW_MUX__IN:
				return getIn();
			case OperatorsPackage.CONTROL_FLOW_MUX__OUT:
				return getOut();
			case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE:
				return getActivate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				setParent((Datapath)newValue);
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE:
				getActivate().clear();
				getActivate().addAll((Collection<? extends InControlPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				setParent((Datapath)null);
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__IN:
				unsetIn();
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__OUT:
				getOut().clear();
				return;
			case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE:
				getActivate().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorsPackage.CONTROL_FLOW_MUX__PARENT:
				return getParent() != null;
			case OperatorsPackage.CONTROL_FLOW_MUX__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case OperatorsPackage.CONTROL_FLOW_MUX__IN:
				return isSetIn();
			case OperatorsPackage.CONTROL_FLOW_MUX__OUT:
				return out != null && !out.isEmpty();
			case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE:
				return activate != null && !activate.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ActivableBlock.class) {
			switch (derivedFeatureID) {
				case OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE: return DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ActivableBlock.class) {
			switch (baseFeatureID) {
				case DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE: return OperatorsPackage.CONTROL_FLOW_MUX__ACTIVATE;
				default: return -1;
			}
		}
		if (baseClass == SingleControlPortBlock.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ControlFlowMuxImpl
