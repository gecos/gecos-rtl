/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.PortImpl;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Data Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.OutDataPortImpl#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OutDataPortImpl extends PortImpl implements OutDataPort {
	public String toString() {
		if (getParentNode()!=null) {
			return getParentNode().getName()+"."+getName()+"["+(getWidth()-1)+":0]";
		} else {
			return "#."+getName()+"["+(getWidth()-1)+":0]";
		}
	}

	/**
	 * The cached value of the '{@link #getWires() <em>Wires</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWires()
	 * @generated
	 * @ordered
	 */
	protected EList<DataFlowWire> wires;

	/**
	 * The cached value of the '{@link #getAssociatedPad() <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPad()
	 * @generated
	 * @ordered
	 */
	protected DataOutputPad associatedPad;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutDataPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.OUT_DATA_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataFlowWire> getWires() {
		if (wires == null) {
			wires = new EObjectWithInverseResolvingEList<DataFlowWire>(DataFlowWire.class, this, PortPackage.OUT_DATA_PORT__WIRES, WiresPackage.DATA_FLOW_WIRE__SOURCE);
		}
		return wires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DataFlowBlock getParentNode() {
		if(eContainer() instanceof DataFlowBlock) {
			return (DataFlowBlock) eContainer();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataOutputPad getAssociatedPad() {
		if (associatedPad != null && associatedPad.eIsProxy()) {
			InternalEObject oldAssociatedPad = (InternalEObject)associatedPad;
			associatedPad = (DataOutputPad)eResolveProxy(oldAssociatedPad);
			if (associatedPad != oldAssociatedPad) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, oldAssociatedPad, associatedPad));
			}
		}
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataOutputPad basicGetAssociatedPad() {
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedPad(DataOutputPad newAssociatedPad, NotificationChain msgs) {
		DataOutputPad oldAssociatedPad = associatedPad;
		associatedPad = newAssociatedPad;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, oldAssociatedPad, newAssociatedPad);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPad(DataOutputPad newAssociatedPad) {
		if (newAssociatedPad != associatedPad) {
			NotificationChain msgs = null;
			if (associatedPad != null)
				msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, DataOutputPad.class, msgs);
			if (newAssociatedPad != null)
				msgs = ((InternalEObject)newAssociatedPad).eInverseAdd(this, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, DataOutputPad.class, msgs);
			msgs = basicSetAssociatedPad(newAssociatedPad, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD, newAssociatedPad, newAssociatedPad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleInputDataFlowBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(InDataPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getWires()).basicAdd(otherEnd, msgs);
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				if (associatedPad != null)
					msgs = ((InternalEObject)associatedPad).eInverseRemove(this, PadsPackage.DATA_OUTPUT_PAD__ASSOCIATED_PORT, DataOutputPad.class, msgs);
				return basicSetAssociatedPad((DataOutputPad)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				return ((InternalEList<?>)getWires()).basicRemove(otherEnd, msgs);
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				return basicSetAssociatedPad(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				return getWires();
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				if (resolve) return getAssociatedPad();
				return basicGetAssociatedPad();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				getWires().clear();
				getWires().addAll((Collection<? extends DataFlowWire>)newValue);
				return;
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				setAssociatedPad((DataOutputPad)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				getWires().clear();
				return;
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				setAssociatedPad((DataOutputPad)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.OUT_DATA_PORT__WIRES:
				return wires != null && !wires.isEmpty();
			case PortPackage.OUT_DATA_PORT__ASSOCIATED_PAD:
				return associatedPad != null;
		}
		return super.eIsSet(featureID);
	}


} //OutDataPortImpl
