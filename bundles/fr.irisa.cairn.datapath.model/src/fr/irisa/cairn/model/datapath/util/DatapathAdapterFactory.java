/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.util;

import fr.irisa.cairn.model.datapath.*;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.Operator;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.SubDatapath;
import fr.irisa.cairn.model.datapath.Wire;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.DatapathPackage
 * @generated
 */
public class DatapathAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DatapathPackage modelPackage;
	

	
/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public DatapathAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DatapathPackage.eINSTANCE;
		}
		
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated NOT
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatapathSwitch<Adapter> modelSwitch =
		new DatapathSwitch<Adapter>() {
			@Override
			public Adapter caseAbstractDatapath(AbstractDatapath object) {
				return createAbstractDatapathAdapter();
			}
			@Override
			public Adapter caseDatapath(Datapath object) {
				return createDatapathAdapter();
			}
			@Override
			public Adapter caseAbstractBlock(AbstractBlock object) {
				return createAbstractBlockAdapter();
			}
			@Override
			public Adapter caseActivableBlock(ActivableBlock object) {
				return createActivableBlockAdapter();
			}
			@Override
			public Adapter caseDataFlowBlock(DataFlowBlock object) {
				return createDataFlowBlockAdapter();
			}
			@Override
			public Adapter caseFlagBearerBlock(FlagBearerBlock object) {
				return createFlagBearerBlockAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseWire(Wire object) {
				return createWireAdapter();
			}
			@Override
			public Adapter casePort(Port object) {
				return createPortAdapter();
			}
			@Override
			public Adapter caseOperator(Operator object) {
				return createOperatorAdapter();
			}
			@Override
			public Adapter caseCombinationalDatapath(CombinationalDatapath object) {
				return createCombinationalDatapathAdapter();
			}
			@Override
			public Adapter caseCombinationalBlock(CombinationalBlock object) {
				return createCombinationalBlockAdapter();
			}
			@Override
			public Adapter caseSequentialBlock(SequentialBlock object) {
				return createSequentialBlockAdapter();
			}
			@Override
			public Adapter casePipelinedBlock(PipelinedBlock object) {
				return createPipelinedBlockAdapter();
			}
			@Override
			public Adapter caseMultiCycleBlock(MultiCycleBlock object) {
				return createMultiCycleBlockAdapter();
			}
			@Override
			public Adapter casePad(Pad object) {
				return createPadAdapter();
			}
			@Override
			public Adapter casePipelinedDatapath(PipelinedDatapath object) {
				return createPipelinedDatapathAdapter();
			}
			@Override
			public Adapter caseSequentialDatapath(SequentialDatapath object) {
				return createSequentialDatapathAdapter();
			}
			@Override
			public Adapter caseBlackBoxBlock(BlackBoxBlock object) {
				return createBlackBoxBlockAdapter();
			}
			@Override
			public Adapter caseMooreSequentialBlock(MooreSequentialBlock object) {
				return createMooreSequentialBlockAdapter();
			}
			@Override
			public Adapter caseMealySequentialBlock(MealySequentialBlock object) {
				return createMealySequentialBlockAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.AbstractDatapath <em>Abstract Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.AbstractDatapath
	 * @generated
	 */
	public Adapter createAbstractDatapathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.Datapath <em>Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.Datapath
	 * @generated
	 */
	public Adapter createDatapathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.AbstractBlock <em>Abstract Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.AbstractBlock
	 * @generated
	 */
	public Adapter createAbstractBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.ActivableBlock <em>Activable Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.ActivableBlock
	 * @generated
	 */
	public Adapter createActivableBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.DataFlowBlock <em>Data Flow Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.DataFlowBlock
	 * @generated
	 */
	public Adapter createDataFlowBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.FlagBearerBlock <em>Flag Bearer Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.FlagBearerBlock
	 * @generated
	 */
	public Adapter createFlagBearerBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.Wire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.Wire
	 * @generated
	 */
	public Adapter createWireAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.Port
	 * @generated
	 */
	public Adapter createPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.Operator
	 * @generated
	 */
	public Adapter createOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.CombinationalDatapath <em>Combinational Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.CombinationalDatapath
	 * @generated
	 */
	public Adapter createCombinationalDatapathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.CombinationalBlock <em>Combinational Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.CombinationalBlock
	 * @generated
	 */
	public Adapter createCombinationalBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.SequentialBlock <em>Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.SequentialBlock
	 * @generated
	 */
	public Adapter createSequentialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.PipelinedBlock <em>Pipelined Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.PipelinedBlock
	 * @generated
	 */
	public Adapter createPipelinedBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.MultiCycleBlock <em>Multi Cycle Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.MultiCycleBlock
	 * @generated
	 */
	public Adapter createMultiCycleBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.Pad <em>Pad</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.Pad
	 * @generated
	 */
	public Adapter createPadAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.PipelinedDatapath <em>Pipelined Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.PipelinedDatapath
	 * @generated
	 */
	public Adapter createPipelinedDatapathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.SequentialDatapath <em>Sequential Datapath</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.SequentialDatapath
	 * @generated
	 */
	public Adapter createSequentialDatapathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.BlackBoxBlock <em>Black Box Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.BlackBoxBlock
	 * @generated
	 */
	public Adapter createBlackBoxBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.MooreSequentialBlock <em>Moore Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.MooreSequentialBlock
	 * @generated
	 */
	public Adapter createMooreSequentialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.irisa.cairn.model.datapath.MealySequentialBlock <em>Mealy Sequential Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.irisa.cairn.model.datapath.MealySequentialBlock
	 * @generated
	 */
	public Adapter createMealySequentialBlockAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DatapathAdapterFactory
