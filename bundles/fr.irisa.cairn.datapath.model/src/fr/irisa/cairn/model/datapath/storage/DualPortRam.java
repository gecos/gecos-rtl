/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dual Port Ram</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getDualPortRam()
 * @model abstract="true"
 * @generated
 */
public interface DualPortRam extends MultiPortRam {
} // DualPortRam
