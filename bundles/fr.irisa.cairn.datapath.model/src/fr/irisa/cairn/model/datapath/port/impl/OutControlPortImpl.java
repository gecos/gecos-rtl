/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.PortImpl;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Out Control Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.impl.OutControlPortImpl#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OutControlPortImpl extends PortImpl implements OutControlPort {
	public String toString() {
		if (getParentNode()!=null) {
			return getParentNode().getName()+"."+getName()+"["+(getWidth()-1)+":0]";
		} else {
			return "#."+getName()+"["+(getWidth()-1)+":0]";
		}
	}

	/**
	 * The cached value of the '{@link #getWires() <em>Wires</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWires()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlFlowWire> wires;

	/**
	 * The cached value of the '{@link #getAssociatedPad() <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedPad()
	 * @generated
	 * @ordered
	 */
	protected StatusPad associatedPad;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutControlPortImpl() {
		super();
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.OUT_CONTROL_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlFlowWire> getWires() {
		if (wires == null) {
			wires = new EObjectWithInverseResolvingEList<ControlFlowWire>(ControlFlowWire.class, this, PortPackage.OUT_CONTROL_PORT__WIRES, WiresPackage.CONTROL_FLOW_WIRE__SOURCE);
		}
		return wires;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FlagBearerBlock getParentNode() {
		return basicGetParentNode();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public FlagBearerBlock basicGetParentNode() {
		if(eContainer() instanceof FlagBearerBlock) {
			return (FlagBearerBlock) eContainer();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isSetParentNode() {
		return (getParentNode()!=null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusPad getAssociatedPad() {
		if (associatedPad != null && associatedPad.eIsProxy()) {
			InternalEObject oldAssociatedPad = (InternalEObject)associatedPad;
			associatedPad = (StatusPad)eResolveProxy(oldAssociatedPad);
			if (associatedPad != oldAssociatedPad) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD, oldAssociatedPad, associatedPad));
			}
		}
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusPad basicGetAssociatedPad() {
		return associatedPad;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedPad(StatusPad newAssociatedPad) {
		StatusPad oldAssociatedPad = associatedPad;
		associatedPad = newAssociatedPad;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD, oldAssociatedPad, associatedPad));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(SingleControlPortBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlowWire connect(InControlPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getWires()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				return ((InternalEList<?>)getWires()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				return getWires();
			case PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD:
				if (resolve) return getAssociatedPad();
				return basicGetAssociatedPad();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				getWires().clear();
				getWires().addAll((Collection<? extends ControlFlowWire>)newValue);
				return;
			case PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD:
				setAssociatedPad((StatusPad)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				getWires().clear();
				return;
			case PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD:
				setAssociatedPad((StatusPad)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.OUT_CONTROL_PORT__WIRES:
				return wires != null && !wires.isEmpty();
			case PortPackage.OUT_CONTROL_PORT__ASSOCIATED_PAD:
				return associatedPad != null;
		}
		return super.eIsSet(featureID);
	}


} //OutControlPortImpl
