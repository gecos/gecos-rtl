/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Output Data Flow Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getSingleOutputDataFlowBlock()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SingleOutputDataFlowBlock extends DataFlowBlock {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getOut().get(0);'"
	 * @generated
	 */
	OutDataPort getOutput();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	DataFlowWire connect(SingleInputDataFlowBlock sink);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	DataFlowWire connect(InDataPort sink);
} // SingleOutputDataFlowBlock
