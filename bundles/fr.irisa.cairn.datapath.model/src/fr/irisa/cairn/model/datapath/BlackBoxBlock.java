/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Black Box Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.BlackBoxBlock#getImplementation <em>Implementation</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getBlackBoxBlock()
 * @model
 * @generated
 */
public interface BlackBoxBlock extends AbstractDatapath {
	/**
	 * Returns the value of the '<em><b>Implementation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation</em>' reference.
	 * @see #setImplementation(Datapath)
	 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getBlackBoxBlock_Implementation()
	 * @model
	 * @generated
	 */
	Datapath getImplementation();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.BlackBoxBlock#getImplementation <em>Implementation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation</em>' reference.
	 * @see #getImplementation()
	 * @generated
	 */
	void setImplementation(Datapath value);

} // BlackBoxBlock
