/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
// killroy was here
package fr.irisa.cairn.model.datapath.storage;

import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Port Rom</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.storage.StoragePackage#getSinglePortRom()
 * @model
 * @generated
 */
public interface SinglePortRom extends AbstractMemory, SingleOutputDataFlowBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getActivate().get(0);'"
	 * @generated
	 */
	InControlPort getReadEnable();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getIn().get(0);'"
	 * @generated
	 */
	InDataPort getAddressPort();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getOutput();'"
	 * @generated
	 */
	OutDataPort getReadPort();

} // SinglePortRom
