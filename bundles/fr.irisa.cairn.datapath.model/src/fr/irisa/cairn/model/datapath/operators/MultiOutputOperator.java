/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators;

import fr.irisa.cairn.model.datapath.Operator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multi Output Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.operators.OperatorsPackage#getMultiOutputOperator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface MultiOutputOperator extends Operator {
} // MultiOutputOperator
