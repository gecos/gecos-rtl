/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Moore Sequential Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getMooreSequentialBlock()
 * @model
 * @generated
 */
public interface MooreSequentialBlock extends SequentialBlock {
} // MooreSequentialBlock
