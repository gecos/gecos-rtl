/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.storage.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.AsyncReadSPRAM;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.RegisterFile;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.StorageFactory;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StorageFactoryImpl extends EFactoryImpl implements StorageFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StorageFactory init() {
		try {
			StorageFactory theStorageFactory = (StorageFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.irisa.fr/cairn/datapath/storage"); 
			if (theStorageFactory != null) {
				return theStorageFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StorageFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StoragePackage.REGISTER: return createRegister();
			case StoragePackage.CE_REGISTER: return createCERegister();
			case StoragePackage.SHIFT_REGISTER: return createShiftRegister();
			case StoragePackage.ABSTRACT_MEMORY: return createAbstractMemory();
			case StoragePackage.MULTI_PORT_RAM: return createMultiPortRam();
			case StoragePackage.SINGLE_PORT_ROM: return createSinglePortRom();
			case StoragePackage.MULTI_PORT_ROM: return createMultiPortRom();
			case StoragePackage.SYNC_READ_SPRAM: return createSyncReadSPRAM();
			case StoragePackage.ASYNC_READ_SPRAM: return createAsyncReadSPRAM();
			case StoragePackage.REGISTER_FILE: return createRegisterFile();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Register createRegister() {
		RegisterImpl register = new RegisterImpl();
		return register;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CERegister createCERegister() {
		CERegisterImpl ceRegister = new CERegisterImpl();
		return ceRegister;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShiftRegister createShiftRegister() {
		ShiftRegisterImpl shiftRegister = new ShiftRegisterImpl();
		return shiftRegister;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractMemory createAbstractMemory() {
		AbstractMemoryImpl abstractMemory = new AbstractMemoryImpl();
		return abstractMemory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiPortRam createMultiPortRam() {
		MultiPortRamImpl multiPortRam = new MultiPortRamImpl();
		return multiPortRam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SinglePortRom createSinglePortRom() {
		SinglePortRomImpl singlePortRom = new SinglePortRomImpl();
		return singlePortRom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiPortRom createMultiPortRom() {
		MultiPortRomImpl multiPortRom = new MultiPortRomImpl();
		return multiPortRom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SyncReadSPRAM createSyncReadSPRAM() {
		SyncReadSPRAMImpl syncReadSPRAM = new SyncReadSPRAMImpl();
		return syncReadSPRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsyncReadSPRAM createAsyncReadSPRAM() {
		AsyncReadSPRAMImpl asyncReadSPRAM = new AsyncReadSPRAMImpl();
		return asyncReadSPRAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RegisterFile createRegisterFile() {
		RegisterFileImpl registerFile = new RegisterFileImpl();
		return registerFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoragePackage getStoragePackage() {
		return (StoragePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StoragePackage getPackage() {
		return StoragePackage.eINSTANCE;
	}

} //StorageFactoryImpl
