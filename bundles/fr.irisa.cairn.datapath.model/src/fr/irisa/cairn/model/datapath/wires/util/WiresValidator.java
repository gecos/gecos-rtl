/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires.util;

import fr.irisa.cairn.model.datapath.wires.*;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectValidator;

import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage
 * @generated
 */
public class WiresValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final WiresValidator INSTANCE = new WiresValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "fr.irisa.cairn.model.datapath.wires";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WiresValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return WiresPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case WiresPackage.DATA_FLOW_WIRE:
				return validateDataFlowWire((DataFlowWire)value, diagnostics, context);
			case WiresPackage.CONTROL_FLOW_WIRE:
				return validateControlFlowWire((ControlFlowWire)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataFlowWire(DataFlowWire dataFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dataFlowWire, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataFlowWire_consistentBitwidth(dataFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataFlowWire_consistentConnection(dataFlowWire, diagnostics, context);
		return result;
	}

	/**
	 * Validates the consistentBitwidth constraint of '<em>Data Flow Wire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataFlowWire_consistentBitwidth(DataFlowWire dataFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "consistentBitwidth", getObjectLabel(dataFlowWire, context) },
						 new Object[] { dataFlowWire },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the consistentConnection constraint of '<em>Data Flow Wire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateDataFlowWire_consistentConnection(DataFlowWire dataFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (dataFlowWire.getSink()==null || dataFlowWire.getSource()==null) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 EcorePlugin.INSTANCE.getString("_UI_GenericConstraint_diagnostic", new Object[] { "consistentConnection", getObjectLabel(dataFlowWire, context) }),
						 new Object[] { dataFlowWire }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlFlowWire(ControlFlowWire controlFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(controlFlowWire, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validateControlFlowWire_consistentInputPortType(controlFlowWire, diagnostics, context);
		if (result || diagnostics != null) result &= validateControlFlowWire_consistentOutputPortType(controlFlowWire, diagnostics, context);
		return result;
	}

	/**
	 * Validates the consistentInputPortType constraint of '<em>Control Flow Wire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlFlowWire_consistentInputPortType(ControlFlowWire controlFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "consistentInputPortType", getObjectLabel(controlFlowWire, context) },
						 new Object[] { controlFlowWire },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the consistentOutputPortType constraint of '<em>Control Flow Wire</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlFlowWire_consistentOutputPortType(ControlFlowWire controlFlowWire, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "consistentOutputPortType", getObjectLabel(controlFlowWire, context) },
						 new Object[] { controlFlowWire },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //WiresValidator
