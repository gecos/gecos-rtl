package fr.irisa.cairn.model.datapath.custom;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreSwitch;

public class ClassFinder<T>  {

	EList<EObject> res;

	public EList<EObject> searchModel(EObject model, EClass target) {
		res = new BasicEList<EObject>();
		searchRec(model, target);
		return res;
	}

	public void searchRec(EObject model, EClass target) {
		if (model.eClass()==target) {
			res.add(model);
		} else {
			TreeIterator<EObject> i =model.eAllContents();
			while (i.hasNext()) {
				searchRec(i.next(), target);
			}
		}
	}

}
