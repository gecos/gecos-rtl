/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.wires;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Flow Wire</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource <em>Source</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink <em>Sink</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getDataFlowWire()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='consistentBitwidth consistentConnection\r\n'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL consistentBiwidth='sef.source.isOLCDefined() and sef.source.isOLCDefined() implies (self.source.getWidth()=self.sink.getWidth())'"
 * @generated
 */
public interface DataFlowWire extends Wire {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.OutDataPort#getWires <em>Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(OutDataPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getDataFlowWire_Source()
	 * @see fr.irisa.cairn.model.datapath.port.OutDataPort#getWires
	 * @model opposite="wires" required="true"
	 * @generated
	 */
	OutDataPort getSource();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(OutDataPort value);

	/**
	 * Returns the value of the '<em><b>Sink</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.port.InDataPort#getWire <em>Wire</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sink</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sink</em>' reference.
	 * @see #setSink(InDataPort)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getDataFlowWire_Sink()
	 * @see fr.irisa.cairn.model.datapath.port.InDataPort#getWire
	 * @model opposite="wire" required="true"
	 * @generated
	 */
	InDataPort getSink();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getSink <em>Sink</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sink</em>' reference.
	 * @see #getSink()
	 * @generated
	 */
	void setSink(InDataPort value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.Datapath#getDataWires <em>Data Wires</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(Datapath)
	 * @see fr.irisa.cairn.model.datapath.wires.WiresPackage#getDataFlowWire_Parent()
	 * @see fr.irisa.cairn.model.datapath.Datapath#getDataWires
	 * @model opposite="dataWires" required="true" transient="false"
	 * @generated
	 */
	Datapath getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.wires.DataFlowWire#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Datapath value);

} // DataFlowWire
