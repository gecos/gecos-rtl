/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.InputPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortFactory;
import fr.irisa.cairn.model.datapath.port.PortPackage;

import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.impl.FsmPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PortPackageImpl extends EPackageImpl implements PortPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inDataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outDataPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inControlPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outControlPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputPortEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PortPackageImpl() {
		super(eNS_URI, PortFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PortPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PortPackage init() {
		if (isInited) return (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);

		// Obtain or create and register package
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PortPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI) : FsmPackage.eINSTANCE);

		// Create package meta-data objects
		thePortPackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		thePadsPackage.createPackageContents();
		theFsmPackage.createPackageContents();

		// Initialize created meta-data
		thePortPackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		thePadsPackage.initializePackageContents();
		theFsmPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePortPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PortPackage.eNS_URI, thePortPackage);
		return thePortPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInDataPort() {
		return inDataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInDataPort_Wire() {
		return (EReference)inDataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInDataPort_AssociatedPad() {
		return (EReference)inDataPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutDataPort() {
		return outDataPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutDataPort_Wires() {
		return (EReference)outDataPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutDataPort_AssociatedPad() {
		return (EReference)outDataPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInControlPort() {
		return inControlPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInControlPort_Wire() {
		return (EReference)inControlPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInControlPort_AssociatedPad() {
		return (EReference)inControlPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutControlPort() {
		return outControlPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutControlPort_Wires() {
		return (EReference)outControlPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutControlPort_AssociatedPad() {
		return (EReference)outControlPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputPort() {
		return inputPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortFactory getPortFactory() {
		return (PortFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		inDataPortEClass = createEClass(IN_DATA_PORT);
		createEReference(inDataPortEClass, IN_DATA_PORT__WIRE);
		createEReference(inDataPortEClass, IN_DATA_PORT__ASSOCIATED_PAD);

		outDataPortEClass = createEClass(OUT_DATA_PORT);
		createEReference(outDataPortEClass, OUT_DATA_PORT__WIRES);
		createEReference(outDataPortEClass, OUT_DATA_PORT__ASSOCIATED_PAD);

		inControlPortEClass = createEClass(IN_CONTROL_PORT);
		createEReference(inControlPortEClass, IN_CONTROL_PORT__WIRE);
		createEReference(inControlPortEClass, IN_CONTROL_PORT__ASSOCIATED_PAD);

		outControlPortEClass = createEClass(OUT_CONTROL_PORT);
		createEReference(outControlPortEClass, OUT_CONTROL_PORT__WIRES);
		createEReference(outControlPortEClass, OUT_CONTROL_PORT__ASSOCIATED_PAD);

		inputPortEClass = createEClass(INPUT_PORT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);
		WiresPackage theWiresPackage = (WiresPackage)EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI);
		PadsPackage thePadsPackage = (PadsPackage)EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI);
		OperatorsPackage theOperatorsPackage = (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		inDataPortEClass.getESuperTypes().add(theDatapathPackage.getPort());
		outDataPortEClass.getESuperTypes().add(theDatapathPackage.getPort());
		inControlPortEClass.getESuperTypes().add(theDatapathPackage.getPort());
		outControlPortEClass.getESuperTypes().add(theDatapathPackage.getPort());

		// Initialize classes and features; add operations and parameters
		initEClass(inDataPortEClass, InDataPort.class, "InDataPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInDataPort_Wire(), theWiresPackage.getDataFlowWire(), theWiresPackage.getDataFlowWire_Sink(), "wire", null, 0, 1, InDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInDataPort_AssociatedPad(), thePadsPackage.getDataInputPad(), thePadsPackage.getDataInputPad_AssociatedPort(), "associatedPad", null, 0, 1, InDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(inDataPortEClass, this.getOutDataPort(), "getSourcePort", 1, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(inDataPortEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOperatorsPackage.getSingleOutputDataFlowBlock(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(inDataPortEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOutDataPort(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(outDataPortEClass, OutDataPort.class, "OutDataPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutDataPort_Wires(), theWiresPackage.getDataFlowWire(), theWiresPackage.getDataFlowWire_Source(), "wires", null, 0, -1, OutDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOutDataPort_AssociatedPad(), thePadsPackage.getDataOutputPad(), thePadsPackage.getDataOutputPad_AssociatedPort(), "associatedPad", null, 0, 1, OutDataPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(outDataPortEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOperatorsPackage.getSingleInputDataFlowBlock(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(outDataPortEClass, theWiresPackage.getDataFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInDataPort(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(inControlPortEClass, InControlPort.class, "InControlPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInControlPort_Wire(), theWiresPackage.getControlFlowWire(), theWiresPackage.getControlFlowWire_Sink(), "wire", null, 0, 1, InControlPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInControlPort_AssociatedPad(), thePadsPackage.getControlPad(), thePadsPackage.getControlPad_AssociatedPort(), "associatedPad", null, 0, 1, InControlPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(inControlPortEClass, this.getOutControlPort(), "getSourcePort", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(inControlPortEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOperatorsPackage.getSingleFlagBlock(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(inControlPortEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOutControlPort(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(outControlPortEClass, OutControlPort.class, "OutControlPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutControlPort_Wires(), theWiresPackage.getControlFlowWire(), theWiresPackage.getControlFlowWire_Source(), "wires", null, 0, -1, OutControlPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOutControlPort_AssociatedPad(), thePadsPackage.getStatusPad(), null, "associatedPad", null, 0, 1, OutControlPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(outControlPortEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOperatorsPackage.getSingleControlPortBlock(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(outControlPortEClass, theWiresPackage.getControlFlowWire(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInControlPort(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(inputPortEClass, InputPort.class, "InputPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (inDataPortEClass, 
		   source, 
		   new String[] {
			 "constraints", "consistentBitwidth\r\n"
		   });											
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";			
		addAnnotation
		  (inDataPortEClass, 
		   source, 
		   new String[] {
			 "consistentBitwidth", "self.sources.forall (w|w.width<=self.width)"
		   });										
	}

} //PortPackageImpl
