/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.operators.impl;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;


import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.custom.Wirer;
import fr.irisa.cairn.model.datapath.impl.NamedElementImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Operator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl#isCombinational <em>Combinational</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl#getIn <em>In</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl#getOut <em>Out</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.operators.impl.UnaryOperatorImpl#getOpcode <em>Opcode</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UnaryOperatorImpl extends NamedElementImpl implements UnaryOperator {
	/**
	 * The default value of the '{@link #isCombinational() <em>Combinational</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCombinational()
	 * @generated
	 * @ordered
	 */
	protected static final boolean COMBINATIONAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<InDataPort> in;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<OutDataPort> out;

	public boolean isCombinational() {
	
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InDataPort> getIn() {
		if (in == null) {
			in = new EObjectContainmentEList.Unsettable<InDataPort>(InDataPort.class, this, OperatorsPackage.UNARY_OPERATOR__IN);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIn() {
		if (in != null) ((InternalEList.Unsettable<?>)in).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIn() {
		return in != null && ((InternalEList.Unsettable<?>)in).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutDataPort> getOut() {
		if (out == null) {
			out = new EObjectContainmentEList<OutDataPort>(OutDataPort.class, this, OperatorsPackage.UNARY_OPERATOR__OUT);
		}
		return out;
	}

	/**
	 * The default value of the '{@link #getOpcode() <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpcode()
	 * @generated
	 * @ordered
	 */
	protected static final UnaryOpcode OPCODE_EDEFAULT = UnaryOpcode.SQRT;

	/**
	 * The cached value of the '{@link #getOpcode() <em>Opcode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpcode()
	 * @generated
	 * @ordered
	 */
	protected UnaryOpcode opcode = OPCODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryOperatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OperatorsPackage.Literals.UNARY_OPERATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath getParent() {
		if (eContainerFeatureID() != OperatorsPackage.UNARY_OPERATOR__PARENT) return null;
		return (Datapath)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParent(Datapath newParent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParent, OperatorsPackage.UNARY_OPERATOR__PARENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(Datapath newParent) {
		if (newParent != eInternalContainer() || (eContainerFeatureID() != OperatorsPackage.UNARY_OPERATOR__PARENT && newParent != null)) {
			if (EcoreUtil.isAncestor(this, newParent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParent != null)
				msgs = ((InternalEObject)newParent).eInverseAdd(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
			msgs = basicSetParent(newParent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.UNARY_OPERATOR__PARENT, newParent, newParent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryOpcode getOpcode() {
		return opcode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpcode(UnaryOpcode newOpcode) {
		UnaryOpcode oldOpcode = opcode;
		opcode = newOpcode == null ? OPCODE_EDEFAULT : newOpcode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OperatorsPackage.UNARY_OPERATOR__OPCODE, oldOpcode, opcode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput() {
		return getIn().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleOutputDataFlowBlock source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(OutDataPort source) {
		return Wirer.wire(this,source);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBlock getSourceBlock() {
		try { 
			return getInput().getWire().getSource().getParentNode(); 
		} catch(NullPointerException e) {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput() {
		return getOut().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(SingleInputDataFlowBlock sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFlowWire connect(InDataPort sink) {
		return Wirer.wire(this,sink);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InDataPort getInput(int pos) {
		return getIn().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutDataPort getOutput(int pos) {
		return getOut().get(pos);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParent((Datapath)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				return basicSetParent(null, msgs);
			case OperatorsPackage.UNARY_OPERATOR__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case OperatorsPackage.UNARY_OPERATOR__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				return eInternalContainer().eInverseRemove(this, DatapathPackage.DATAPATH__COMPONENTS, Datapath.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				return getParent();
			case OperatorsPackage.UNARY_OPERATOR__COMBINATIONAL:
				return isCombinational();
			case OperatorsPackage.UNARY_OPERATOR__IN:
				return getIn();
			case OperatorsPackage.UNARY_OPERATOR__OUT:
				return getOut();
			case OperatorsPackage.UNARY_OPERATOR__OPCODE:
				return getOpcode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				setParent((Datapath)newValue);
				return;
			case OperatorsPackage.UNARY_OPERATOR__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends InDataPort>)newValue);
				return;
			case OperatorsPackage.UNARY_OPERATOR__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends OutDataPort>)newValue);
				return;
			case OperatorsPackage.UNARY_OPERATOR__OPCODE:
				setOpcode((UnaryOpcode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				setParent((Datapath)null);
				return;
			case OperatorsPackage.UNARY_OPERATOR__IN:
				unsetIn();
				return;
			case OperatorsPackage.UNARY_OPERATOR__OUT:
				getOut().clear();
				return;
			case OperatorsPackage.UNARY_OPERATOR__OPCODE:
				setOpcode(OPCODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OperatorsPackage.UNARY_OPERATOR__PARENT:
				return getParent() != null;
			case OperatorsPackage.UNARY_OPERATOR__COMBINATIONAL:
				return isCombinational() != COMBINATIONAL_EDEFAULT;
			case OperatorsPackage.UNARY_OPERATOR__IN:
				return isSetIn();
			case OperatorsPackage.UNARY_OPERATOR__OUT:
				return out != null && !out.isEmpty();
			case OperatorsPackage.UNARY_OPERATOR__OPCODE:
				return opcode != OPCODE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer(super.toString());
		result.append(name+":"+opcode+"["+getOutput().getWidth()+"]");
		return result.toString();
	}

} //UnaryOperatorImpl
