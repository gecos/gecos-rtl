/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.impl;

import fr.irisa.cairn.model.datapath.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathFactory;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.SubDatapath;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DatapathFactoryImpl extends EFactoryImpl implements DatapathFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DatapathFactory init() {
		try {
			DatapathFactory theDatapathFactory = (DatapathFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.irisa.fr/cairn/datapath"); 
			if (theDatapathFactory != null) {
				return theDatapathFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DatapathFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatapathFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DatapathPackage.DATAPATH: return createDatapath();
			case DatapathPackage.COMBINATIONAL_DATAPATH: return createCombinationalDatapath();
			case DatapathPackage.PIPELINED_DATAPATH: return createPipelinedDatapath();
			case DatapathPackage.SEQUENTIAL_DATAPATH: return createSequentialDatapath();
			case DatapathPackage.BLACK_BOX_BLOCK: return createBlackBoxBlock();
			case DatapathPackage.MOORE_SEQUENTIAL_BLOCK: return createMooreSequentialBlock();
			case DatapathPackage.MEALY_SEQUENTIAL_BLOCK: return createMealySequentialBlock();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Datapath createDatapath() {
		DatapathImpl datapath = new DatapathImpl();
		return datapath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CombinationalDatapath createCombinationalDatapath() {
		CombinationalDatapathImpl combinationalDatapath = new CombinationalDatapathImpl();
		return combinationalDatapath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PipelinedDatapath createPipelinedDatapath() {
		PipelinedDatapathImpl pipelinedDatapath = new PipelinedDatapathImpl();
		return pipelinedDatapath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequentialDatapath createSequentialDatapath() {
		SequentialDatapathImpl sequentialDatapath = new SequentialDatapathImpl();
		return sequentialDatapath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlackBoxBlock createBlackBoxBlock() {
		BlackBoxBlockImpl blackBoxBlock = new BlackBoxBlockImpl();
		return blackBoxBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MooreSequentialBlock createMooreSequentialBlock() {
		MooreSequentialBlockImpl mooreSequentialBlock = new MooreSequentialBlockImpl();
		return mooreSequentialBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MealySequentialBlock createMealySequentialBlock() {
		MealySequentialBlockImpl mealySequentialBlock = new MealySequentialBlockImpl();
		return mealySequentialBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatapathPackage getDatapathPackage() {
		return (DatapathPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DatapathPackage getPackage() {
		return DatapathPackage.eINSTANCE;
	}

} //DatapathFactoryImpl
