/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.port;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.operators.SingleControlPortBlock;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Out Control Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getWires <em>Wires</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getAssociatedPad <em>Associated Pad</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutControlPort()
 * @model
 * @generated
 */
public interface OutControlPort extends Port {

	/**
	 * Returns the value of the '<em><b>Wires</b></em>' reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wires</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wires</em>' reference list.
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutControlPort_Wires()
	 * @see fr.irisa.cairn.model.datapath.wires.ControlFlowWire#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<ControlFlowWire> getWires();

	/**
	 * Returns the value of the '<em><b>Associated Pad</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Pad</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Pad</em>' reference.
	 * @see #setAssociatedPad(StatusPad)
	 * @see fr.irisa.cairn.model.datapath.port.PortPackage#getOutControlPort_AssociatedPad()
	 * @model
	 * @generated
	 */
	StatusPad getAssociatedPad();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.datapath.port.OutControlPort#getAssociatedPad <em>Associated Pad</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Pad</em>' reference.
	 * @see #getAssociatedPad()
	 * @generated
	 */
	void setAssociatedPad(StatusPad value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	ControlFlowWire connect(SingleControlPortBlock sink);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='return Wirer.wire(this,sink);'"
	 * @generated
	 */
	ControlFlowWire connect(InControlPort sink);
	
	FlagBearerBlock getParentNode();

} // OutControlPort
