/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.datapath.DatapathPackage
 * @generated
 */
public interface DatapathFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatapathFactory eINSTANCE = fr.irisa.cairn.model.datapath.impl.DatapathFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Datapath</em>'.
	 * @generated
	 */
	Datapath createDatapath();

	/**
	 * Returns a new object of class '<em>Combinational Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Combinational Datapath</em>'.
	 * @generated
	 */
	CombinationalDatapath createCombinationalDatapath();

	/**
	 * Returns a new object of class '<em>Pipelined Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pipelined Datapath</em>'.
	 * @generated
	 */
	PipelinedDatapath createPipelinedDatapath();

	/**
	 * Returns a new object of class '<em>Sequential Datapath</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequential Datapath</em>'.
	 * @generated
	 */
	SequentialDatapath createSequentialDatapath();

	/**
	 * Returns a new object of class '<em>Black Box Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Black Box Block</em>'.
	 * @generated
	 */
	BlackBoxBlock createBlackBoxBlock();

	/**
	 * Returns a new object of class '<em>Moore Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Moore Sequential Block</em>'.
	 * @generated
	 */
	MooreSequentialBlock createMooreSequentialBlock();

	/**
	 * Returns a new object of class '<em>Mealy Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mealy Sequential Block</em>'.
	 * @generated
	 */
	MealySequentialBlock createMealySequentialBlock();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DatapathPackage getDatapathPackage();

} //DatapathFactory
