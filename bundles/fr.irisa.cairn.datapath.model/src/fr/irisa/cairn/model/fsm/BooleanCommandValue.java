/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Command Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.BooleanCommandValue#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanCommandValue()
 * @model
 * @generated
 */
public interface BooleanCommandValue extends AbstractCommandValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.model.fsm.BooleanValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see fr.irisa.cairn.model.fsm.BooleanValue
	 * @see #setValue(BooleanValue)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanCommandValue_Value()
	 * @model required="true"
	 * @generated
	 */
	BooleanValue getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.BooleanCommandValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see fr.irisa.cairn.model.fsm.BooleanValue
	 * @see #getValue()
	 * @generated
	 */
	void setValue(BooleanValue value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String toString();

} // BooleanCommandValue
