/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Indexed Boolean Flag Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm#getOffset <em>Offset</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIndexedBooleanFlagTerm()
 * @model
 * @generated
 */
public interface IndexedBooleanFlagTerm extends BooleanFlagTerm {
	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(int)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIndexedBooleanFlagTerm_Offset()
	 * @model default="-1" required="true"
	 * @generated
	 */
	int getOffset();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\r\n\t\t\treturn (isNegated()?\"!\":\"\")+getTermName();\r\n\t\t} else {\r\n\t\t\treturn (isNegated()?\"!\":\"\")+\"(?)\";\r\n\t\t}\r\n'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\r\n\t\t\treturn getFlag().getName()+\"[\"+getOffset()+\"]\";\r\n\t\t} else {\r\n\t\t\treturn \"(?)[\"+getOffset()+\"]\";\r\n\t\t}\r\n'"
	 * @generated
	 */
	String getTermName();

} // IndexedBooleanFlagTerm
