/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm.impl;

import java.util.Iterator;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.eclipse.emf.ecore.EClass;


import fr.irisa.cairn.model.datapath.custom.BoolExpPrettyPrint;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.FsmPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>And Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AndExpressionImpl extends BooleanOperatorExpressionImpl implements AndExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AndExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FsmPackage.Literals.AND_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String toString() {
		return BoolExpPrettyPrint.print(this);
	}

} //AndExpressionImpl
