/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Command Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommand <em>Command</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommandName <em>Command Name</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getPredicate <em>Predicate</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAbstractCommandValue()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='satisfiablePredicate'"
 * @generated
 */
public interface AbstractCommandValue extends EObject {
	/**
	 * Returns the value of the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command</em>' reference.
	 * @see #setCommand(OutControlPort)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAbstractCommandValue_Command()
	 * @model required="true"
	 * @generated
	 */
	OutControlPort getCommand();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommand <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command</em>' reference.
	 * @see #getCommand()
	 * @generated
	 */
	void setCommand(OutControlPort value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #isSetParent()
	 * @see #unsetParent()
	 * @see #setParent(State)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAbstractCommandValue_Parent()
	 * @model unsettable="true" transient="true" volatile="true" derived="true"
	 * @generated
	 */
	State getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #isSetParent()
	 * @see #unsetParent()
	 * @see #getParent()
	 * @generated
	 */
	void setParent(State value);

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #isSetParent()
	 * @see #unsetParent()
	 * @see #getParent()
	 * @generated
	 */
	//void setParent(State value);

	/**
	 * Unsets the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParent()
	 * @see #getParent()
	 * @see #setParent(State)
	 * @generated
	 */
	void unsetParent();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parent</em>' reference is set.
	 * @see #unsetParent()
	 * @see #getParent()
	 * @see #setParent(State)
	 * @generated
	 */
	boolean isSetParent();

	/**
	 * Returns the value of the '<em><b>Command Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Name</em>' attribute.
	 * @see #isSetCommandName()
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAbstractCommandValue_CommandName()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getCommandName();

	/**
	 * Returns whether the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommandName <em>Command Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Command Name</em>' attribute is set.
	 * @see #getCommandName()
	 * @generated
	 */
	boolean isSetCommandName();

	/**
	 * Returns the value of the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate</em>' containment reference.
	 * @see #setPredicate(AbstractBooleanExpression)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAbstractCommandValue_Predicate()
	 * @model containment="true"
	 * @generated
	 */
	AbstractBooleanExpression getPredicate();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getPredicate <em>Predicate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predicate</em>' containment reference.
	 * @see #getPredicate()
	 * @generated
	 */
	void setPredicate(AbstractBooleanExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String toString();

} // AbstractCommandValue
