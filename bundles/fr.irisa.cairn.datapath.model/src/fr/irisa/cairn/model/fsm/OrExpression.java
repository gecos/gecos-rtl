/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getOrExpression()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='satisfiablePredicate'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL satisfiablePredicate='not self.isAlwaysFalse()'"
 * @generated
 */
public interface OrExpression extends BooleanOperatorExpression {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return BoolExpPrettyPrint.print(this);'"
	 * @generated
	 */
	String toString();

} // OrExpression
