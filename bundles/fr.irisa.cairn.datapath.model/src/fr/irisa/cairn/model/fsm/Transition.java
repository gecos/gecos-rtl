/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.Transition#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.Transition#getSrc <em>Src</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.Transition#getDst <em>Dst</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.Transition#getPredicate <em>Predicate</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getTransition()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validName noDuplicateDestination isReachableFromInitialState completePredicateCover'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL noDuplicateDestination='self.transitions->forAll( p1, p2 | p1.dst = p2.dst implies p1=p2 )' isReachableFromInitialState='self.parent.start->closure(transitions.dst)->includes(self)' validName='self.label<>\"\"'"
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * The default value is <code>"transition_0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getTransition_Label()
	 * @model default="transition_0" id="true" required="true"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.Transition#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Src</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.fsm.State#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' container reference.
	 * @see #setSrc(State)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getTransition_Src()
	 * @see fr.irisa.cairn.model.fsm.State#getTransitions
	 * @model opposite="transitions" required="true" transient="false"
	 * @generated
	 */
	State getSrc();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.Transition#getSrc <em>Src</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' container reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(State value);

	/**
	 * Returns the value of the '<em><b>Dst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dst</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dst</em>' reference.
	 * @see #setDst(State)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getTransition_Dst()
	 * @model required="true"
	 * @generated
	 */
	State getDst();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.Transition#getDst <em>Dst</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dst</em>' reference.
	 * @see #getDst()
	 * @generated
	 */
	void setDst(State value);

	/**
	 * Returns the value of the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate</em>' containment reference.
	 * @see #setPredicate(AbstractBooleanExpression)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getTransition_Predicate()
	 * @model containment="true"
	 * @generated
	 */
	AbstractBooleanExpression getPredicate();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.Transition#getPredicate <em>Predicate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predicate</em>' containment reference.
	 * @see #getPredicate()
	 * @generated
	 */
	void setPredicate(AbstractBooleanExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return getSrc() +\" -> \"+ getDst() + \" when \" +BoolExpPrettyPrint.print(getPredicate());'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dstRequired="true"
	 * @generated
	 */
	Transition reconnectTo(State dst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dstRequired="true"
	 * @generated
	 */
	Transition reconnectFrom(State dst);

} // Transition
