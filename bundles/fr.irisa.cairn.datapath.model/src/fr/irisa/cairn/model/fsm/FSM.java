/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.FSM#getStates <em>States</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.FSM#getStart <em>Start</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.FSM#getDefaultValues <em>Default Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getFSM()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validName uniqueStateLabel uniqueFlagName uniqueCommandName uniqueTransitionLabel\r\n'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL uniqueFlagName='self.flags->forAll(c1:FlagPort , c2:FlagPort | c1.name=c2.name implies c1=c2)' uniqueCommandName='self.commands->forAll(c1:CommandPort , c2:CommandPort | c1.name=c2.name implies c1=c2)' uniqueStateLabel='self.states->forAll(c1:State , c2:State | c1.label=c2.label implies c1=c2)' uniqueTransitionLabel='self.transitions->forAll(c1:Transition , c2:Transition | c1.label=c2.label implies c1=c2)' validName='self.name<>\"\"'"
 * @generated
 */
public interface FSM extends ActivableBlock, FlagBearerBlock, MealySequentialBlock {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.State}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.fsm.State#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getFSM_States()
	 * @see fr.irisa.cairn.model.fsm.State#getParent
	 * @model opposite="parent" containment="true" required="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' reference.
	 * @see #setStart(State)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getFSM_Start()
	 * @model required="true"
	 * @generated
	 */
	State getStart();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.FSM#getStart <em>Start</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(State value);

	/**
	 * Returns the value of the '<em><b>Default Values</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.OutputDefaultValues}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Values</em>' containment reference list.
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getFSM_DefaultValues()
	 * @model containment="true"
	 * @generated
	 */
	EList<OutputDefaultValues> getDefaultValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void removeState(State state);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" nameRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tfor (OutControlPort oport : getFlags()) {\r\n\t\t\tif (oport.getName().equals(name)) return oport;\r\n\t\t}\r\n\t\treturn null;\r\n'"
	 * @generated
	 */
	OutControlPort getCommandFromName(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" nameRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tfor (InControlPort iport : getActivate()) {\r\n\t\t\tif (iport.getName().equals(name)) return iport;\r\n\t\t}\r\n\t\treturn null;\r\n'"
	 * @generated
	 */
	InControlPort getFlagFromName(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" nameRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tfor (State state : getStates()) {\r\n\t\t\tif (state.getLabel().equals(name)) return state;\r\n\t\t}\r\n\t\treturn null;\r\n'"
	 * @generated
	 */
	State getStateFromLabel(String name);
	State getState(int pos);

} // FSM
