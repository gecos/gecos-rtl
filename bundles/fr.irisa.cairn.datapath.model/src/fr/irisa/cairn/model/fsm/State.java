/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;



import java.util.List;
import java.util.Set;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.State#getLabel <em>Label</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.State#getParent <em>Parent</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.State#getActivatedCommands <em>Activated Commands</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.State#getTransitions <em>Transitions</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getState()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='validName noDuplicateDestination isReachableFromInitialState'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL validName='self.label<>\"\"' noDuplicateDestination='self.transitions->forAll( p1, p2 | p1.dst = p2.dst implies p1=p2 )' isReachableFromInitialState='self.parent.start->closure(transitions.dst)->includes(self)'"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * The default value is <code>"S0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getState_Label()
	 * @model default="S0" id="true" required="true"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.State#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.fsm.FSM#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(FSM)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getState_Parent()
	 * @see fr.irisa.cairn.model.fsm.FSM#getStates
	 * @model opposite="states" required="true" transient="false"
	 * @generated
	 */
	FSM getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.State#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(FSM value);

	/**
	 * Returns the value of the '<em><b>Activated Commands</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.AbstractCommandValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activated Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activated Commands</em>' containment reference list.
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getState_ActivatedCommands()
	 * @model containment="true" derived="true"
	 * @generated
	 */
	EList<AbstractCommandValue> getActivatedCommands();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.Transition}.
	 * It is bidirectional and its opposite is '{@link fr.irisa.cairn.model.fsm.Transition#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getState_Transitions()
	 * @see fr.irisa.cairn.model.fsm.Transition#getSrc
	 * @model opposite="src" containment="true" required="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getParent()!=null) { \r\n\t\t\treturn \"state \"+getParent().getName()+\"::\"+getLabel();\r\n\t\t} else {\r\n\t\t\treturn \"state (unknown)::\"+getLabel();\r\n\t\t}\r\n'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dstRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();'"
	 * @generated
	 */
	Transition connectTo(State dst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dstRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();'"
	 * @generated
	 */
	Transition disconnectFrom(State dst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();'"
	 * @generated
	 */
	EList<State> getNextStates();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" dstRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();'"
	 * @generated
	 */
	boolean isReachableFrom(State dst);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='throw new UnsupportedOperationException();'"
	 * @generated
	 */
	boolean isConsistent();


} // State
