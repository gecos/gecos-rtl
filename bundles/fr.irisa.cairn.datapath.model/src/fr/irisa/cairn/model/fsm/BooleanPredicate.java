/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanPredicate()
 * @model abstract="true"
 * @generated
 */
public interface BooleanPredicate extends EObject {
} // BooleanPredicate
