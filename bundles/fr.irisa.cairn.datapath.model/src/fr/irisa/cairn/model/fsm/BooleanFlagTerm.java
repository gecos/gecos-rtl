/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Flag Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.BooleanFlagTerm#isNegated <em>Negated</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanFlagTerm()
 * @model
 * @generated
 */
public interface BooleanFlagTerm extends FlagTerm {
	/**
	 * Returns the value of the '<em><b>Negated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Negated</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Negated</em>' attribute.
	 * @see #setNegated(boolean)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanFlagTerm_Negated()
	 * @model required="true"
	 * @generated
	 */
	boolean isNegated();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.BooleanFlagTerm#isNegated <em>Negated</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Negated</em>' attribute.
	 * @see #isNegated()
	 * @generated
	 */
	void setNegated(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\r\n\t\t\treturn (isNegated()?\"!\":\"\")+getTermName();\r\n\t\t} else {\r\n\t\t\treturn (isNegated()?\"!\":\"\")+\"(?)\";\r\n\t\t}\r\n'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\r\n\t\t\treturn getFlag().getName();\r\n\t\t} else {\r\n\t\t\treturn \"(?)\";\r\n\t\t}\r\n'"
	 * @generated
	 */
	String getTermName();

} // BooleanFlagTerm
