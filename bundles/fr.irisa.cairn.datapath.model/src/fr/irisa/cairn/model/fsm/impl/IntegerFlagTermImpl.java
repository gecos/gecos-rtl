/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm.impl;

import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Integer Flag Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl#isEqual <em>Equal</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl#getOpType <em>Op Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IntegerFlagTermImpl extends FlagTermImpl implements IntegerFlagTerm {
	/**
	 * The default value of the '{@link #isEqual() <em>Equal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEqual()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EQUAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isEqual() <em>Equal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEqual()
	 * @generated
	 * @ordered
	 */
	protected boolean equal = EQUAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final int VALUE_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected int value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOpType() <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpType()
	 * @generated
	 * @ordered
	 */
	protected static final CompareOpcode OP_TYPE_EDEFAULT = CompareOpcode.EQU;

	/**
	 * The cached value of the '{@link #getOpType() <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpType()
	 * @generated
	 * @ordered
	 */
	protected CompareOpcode opType = OP_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerFlagTermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FsmPackage.Literals.INTEGER_FLAG_TERM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEqual() {
		return equal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEqual(boolean newEqual) {
		boolean oldEqual = equal;
		equal = newEqual;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FsmPackage.INTEGER_FLAG_TERM__EQUAL, oldEqual, equal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(int newValue) {
		int oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FsmPackage.INTEGER_FLAG_TERM__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompareOpcode getOpType() {
		return opType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpType(CompareOpcode newOpType) {
		CompareOpcode oldOpType = opType;
		opType = newOpType == null ? OP_TYPE_EDEFAULT : newOpType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FsmPackage.INTEGER_FLAG_TERM__OP_TYPE, oldOpType, opType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
				if (getFlag()!=null) {
					return (isEqual()?"!=":"")+getTermName();
				} else {
					return (isEqual()?"==":"")+"(?)";
				}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTermName() {
				if (getFlag()!=null) {
					return getFlag().getName();
				} else {
					return "(?)";
				}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FsmPackage.INTEGER_FLAG_TERM__EQUAL:
				return isEqual();
			case FsmPackage.INTEGER_FLAG_TERM__VALUE:
				return getValue();
			case FsmPackage.INTEGER_FLAG_TERM__OP_TYPE:
				return getOpType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FsmPackage.INTEGER_FLAG_TERM__EQUAL:
				setEqual((Boolean)newValue);
				return;
			case FsmPackage.INTEGER_FLAG_TERM__VALUE:
				setValue((Integer)newValue);
				return;
			case FsmPackage.INTEGER_FLAG_TERM__OP_TYPE:
				setOpType((CompareOpcode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FsmPackage.INTEGER_FLAG_TERM__EQUAL:
				setEqual(EQUAL_EDEFAULT);
				return;
			case FsmPackage.INTEGER_FLAG_TERM__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case FsmPackage.INTEGER_FLAG_TERM__OP_TYPE:
				setOpType(OP_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FsmPackage.INTEGER_FLAG_TERM__EQUAL:
				return equal != EQUAL_EDEFAULT;
			case FsmPackage.INTEGER_FLAG_TERM__VALUE:
				return value != VALUE_EDEFAULT;
			case FsmPackage.INTEGER_FLAG_TERM__OP_TYPE:
				return opType != OP_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //IntegerFlagTermImpl
