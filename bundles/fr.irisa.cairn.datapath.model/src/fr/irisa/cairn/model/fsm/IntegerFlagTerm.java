/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import fr.irisa.cairn.model.datapath.operators.CompareOpcode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Integer Flag Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#isEqual <em>Equal</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getValue <em>Value</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getOpType <em>Op Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIntegerFlagTerm()
 * @model
 * @generated
 */
public interface IntegerFlagTerm extends FlagTerm {
	/**
	 * Returns the value of the '<em><b>Equal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equal</em>' attribute.
	 * @see #setEqual(boolean)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIntegerFlagTerm_Equal()
	 * @model required="true"
	 * @generated
	 */
	boolean isEqual();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#isEqual <em>Equal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equal</em>' attribute.
	 * @see #isEqual()
	 * @generated
	 */
	void setEqual(boolean value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIntegerFlagTerm_Value()
	 * @model default="-1"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

	/**
	 * Returns the value of the '<em><b>Op Type</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.irisa.cairn.model.datapath.operators.CompareOpcode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Op Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Op Type</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @see #setOpType(CompareOpcode)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getIntegerFlagTerm_OpType()
	 * @model
	 * @generated
	 */
	CompareOpcode getOpType();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getOpType <em>Op Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Op Type</em>' attribute.
	 * @see fr.irisa.cairn.model.datapath.operators.CompareOpcode
	 * @see #getOpType()
	 * @generated
	 */
	void setOpType(CompareOpcode value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\n\t\t\treturn (isEqual()?\"!=\":\"\")+getTermName();\n\t\t} else {\n\t\t\treturn (isEqual()?\"==\":\"\")+\"(?)\";\n\t\t}\n'"
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='\t\tif (getFlag()!=null) {\r\n\t\t\treturn getFlag().getName();\r\n\t\t} else {\r\n\t\t\treturn \"(?)\";\r\n\t\t}\r\n'"
	 * @generated
	 */
	String getTermName();

} // IntegerFlagTerm
