/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.impl.DatapathPackageImpl;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsPackageImpl;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.impl.PadsPackageImpl;
import fr.irisa.cairn.model.datapath.port.PortPackage;
import fr.irisa.cairn.model.datapath.port.impl.PortPackageImpl;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.storage.impl.StoragePackageImpl;
import fr.irisa.cairn.model.datapath.wires.WiresPackage;
import fr.irisa.cairn.model.datapath.wires.impl.WiresPackageImpl;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanOperatorExpression;
import fr.irisa.cairn.model.fsm.BooleanValue;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FsmPackageImpl extends EPackageImpl implements FsmPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fsmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractCommandValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanCommandValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerCommandValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractBooleanExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flagTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass integerFlagTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass indexedBooleanFlagTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputDefaultValuesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanOperatorExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass negateExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanFlagTermEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum booleanValueEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FsmPackageImpl() {
		super(eNS_URI, FsmFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FsmPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FsmPackage init() {
		if (isInited) return (FsmPackage)EPackage.Registry.INSTANCE.getEPackage(FsmPackage.eNS_URI);

		// Obtain or create and register package
		FsmPackageImpl theFsmPackage = (FsmPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FsmPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FsmPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		DatapathPackageImpl theDatapathPackage = (DatapathPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) instanceof DatapathPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI) : DatapathPackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		WiresPackageImpl theWiresPackage = (WiresPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) instanceof WiresPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(WiresPackage.eNS_URI) : WiresPackage.eINSTANCE);
		StoragePackageImpl theStoragePackage = (StoragePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) instanceof StoragePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(StoragePackage.eNS_URI) : StoragePackage.eINSTANCE);
		OperatorsPackageImpl theOperatorsPackage = (OperatorsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) instanceof OperatorsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI) : OperatorsPackage.eINSTANCE);
		PadsPackageImpl thePadsPackage = (PadsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) instanceof PadsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PadsPackage.eNS_URI) : PadsPackage.eINSTANCE);

		// Create package meta-data objects
		theFsmPackage.createPackageContents();
		theDatapathPackage.createPackageContents();
		thePortPackage.createPackageContents();
		theWiresPackage.createPackageContents();
		theStoragePackage.createPackageContents();
		theOperatorsPackage.createPackageContents();
		thePadsPackage.createPackageContents();

		// Initialize created meta-data
		theFsmPackage.initializePackageContents();
		theDatapathPackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theWiresPackage.initializePackageContents();
		theStoragePackage.initializePackageContents();
		theOperatorsPackage.initializePackageContents();
		thePadsPackage.initializePackageContents();


		// Mark meta-data to indicate it can't be changed
		theFsmPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FsmPackage.eNS_URI, theFsmPackage);
		return theFsmPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFSM() {
		return fsmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_States() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_Start() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFSM_DefaultValues() {
		return (EReference)fsmEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getState() {
		return stateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getState_Label() {
		return (EAttribute)stateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_Parent() {
		return (EReference)stateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_ActivatedCommands() {
		return (EReference)stateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getState_Transitions() {
		return (EReference)stateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransition() {
		return transitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransition_Label() {
		return (EAttribute)transitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Src() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Dst() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransition_Predicate() {
		return (EReference)transitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractCommandValue() {
		return abstractCommandValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractCommandValue_Command() {
		return (EReference)abstractCommandValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractCommandValue_Parent() {
		return (EReference)abstractCommandValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractCommandValue_CommandName() {
		return (EAttribute)abstractCommandValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractCommandValue_Predicate() {
		return (EReference)abstractCommandValueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanCommandValue() {
		return booleanCommandValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanCommandValue_Value() {
		return (EAttribute)booleanCommandValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerCommandValue() {
		return integerCommandValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerCommandValue_Value() {
		return (EAttribute)integerCommandValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractBooleanExpression() {
		return abstractBooleanExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractBooleanExpression_Expression() {
		return (EAttribute)abstractBooleanExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAndExpression() {
		return andExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOrExpression() {
		return orExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFlagTerm() {
		return flagTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFlagTerm_Flag() {
		return (EReference)flagTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntegerFlagTerm() {
		return integerFlagTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerFlagTerm_Equal() {
		return (EAttribute)integerFlagTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerFlagTerm_Value() {
		return (EAttribute)integerFlagTermEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntegerFlagTerm_OpType() {
		return (EAttribute)integerFlagTermEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIndexedBooleanFlagTerm() {
		return indexedBooleanFlagTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIndexedBooleanFlagTerm_Offset() {
		return (EAttribute)indexedBooleanFlagTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutputDefaultValues() {
		return outputDefaultValuesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOutputDefaultValues_Port() {
		return (EReference)outputDefaultValuesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOutputDefaultValues_Value() {
		return (EAttribute)outputDefaultValuesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanConstant() {
		return booleanConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanConstant_Value() {
		return (EAttribute)booleanConstantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanOperatorExpression() {
		return booleanOperatorExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooleanOperatorExpression_Terms() {
		return (EReference)booleanOperatorExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNegateExpression() {
		return negateExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNegateExpression_Term() {
		return (EReference)negateExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanFlagTerm() {
		return booleanFlagTermEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanFlagTerm_Negated() {
		return (EAttribute)booleanFlagTermEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBooleanValue() {
		return booleanValueEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FsmFactory getFsmFactory() {
		return (FsmFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		fsmEClass = createEClass(FSM);
		createEReference(fsmEClass, FSM__STATES);
		createEReference(fsmEClass, FSM__START);
		createEReference(fsmEClass, FSM__DEFAULT_VALUES);

		stateEClass = createEClass(STATE);
		createEAttribute(stateEClass, STATE__LABEL);
		createEReference(stateEClass, STATE__PARENT);
		createEReference(stateEClass, STATE__ACTIVATED_COMMANDS);
		createEReference(stateEClass, STATE__TRANSITIONS);

		transitionEClass = createEClass(TRANSITION);
		createEAttribute(transitionEClass, TRANSITION__LABEL);
		createEReference(transitionEClass, TRANSITION__SRC);
		createEReference(transitionEClass, TRANSITION__DST);
		createEReference(transitionEClass, TRANSITION__PREDICATE);

		abstractCommandValueEClass = createEClass(ABSTRACT_COMMAND_VALUE);
		createEReference(abstractCommandValueEClass, ABSTRACT_COMMAND_VALUE__COMMAND);
		createEReference(abstractCommandValueEClass, ABSTRACT_COMMAND_VALUE__PARENT);
		createEAttribute(abstractCommandValueEClass, ABSTRACT_COMMAND_VALUE__COMMAND_NAME);
		createEReference(abstractCommandValueEClass, ABSTRACT_COMMAND_VALUE__PREDICATE);

		booleanCommandValueEClass = createEClass(BOOLEAN_COMMAND_VALUE);
		createEAttribute(booleanCommandValueEClass, BOOLEAN_COMMAND_VALUE__VALUE);

		integerCommandValueEClass = createEClass(INTEGER_COMMAND_VALUE);
		createEAttribute(integerCommandValueEClass, INTEGER_COMMAND_VALUE__VALUE);

		abstractBooleanExpressionEClass = createEClass(ABSTRACT_BOOLEAN_EXPRESSION);
		createEAttribute(abstractBooleanExpressionEClass, ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION);

		andExpressionEClass = createEClass(AND_EXPRESSION);

		orExpressionEClass = createEClass(OR_EXPRESSION);

		outputDefaultValuesEClass = createEClass(OUTPUT_DEFAULT_VALUES);
		createEReference(outputDefaultValuesEClass, OUTPUT_DEFAULT_VALUES__PORT);
		createEAttribute(outputDefaultValuesEClass, OUTPUT_DEFAULT_VALUES__VALUE);

		booleanConstantEClass = createEClass(BOOLEAN_CONSTANT);
		createEAttribute(booleanConstantEClass, BOOLEAN_CONSTANT__VALUE);

		booleanOperatorExpressionEClass = createEClass(BOOLEAN_OPERATOR_EXPRESSION);
		createEReference(booleanOperatorExpressionEClass, BOOLEAN_OPERATOR_EXPRESSION__TERMS);

		negateExpressionEClass = createEClass(NEGATE_EXPRESSION);
		createEReference(negateExpressionEClass, NEGATE_EXPRESSION__TERM);

		booleanFlagTermEClass = createEClass(BOOLEAN_FLAG_TERM);
		createEAttribute(booleanFlagTermEClass, BOOLEAN_FLAG_TERM__NEGATED);

		flagTermEClass = createEClass(FLAG_TERM);
		createEReference(flagTermEClass, FLAG_TERM__FLAG);

		integerFlagTermEClass = createEClass(INTEGER_FLAG_TERM);
		createEAttribute(integerFlagTermEClass, INTEGER_FLAG_TERM__EQUAL);
		createEAttribute(integerFlagTermEClass, INTEGER_FLAG_TERM__VALUE);
		createEAttribute(integerFlagTermEClass, INTEGER_FLAG_TERM__OP_TYPE);

		indexedBooleanFlagTermEClass = createEClass(INDEXED_BOOLEAN_FLAG_TERM);
		createEAttribute(indexedBooleanFlagTermEClass, INDEXED_BOOLEAN_FLAG_TERM__OFFSET);

		// Create enums
		booleanValueEEnum = createEEnum(BOOLEAN_VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DatapathPackage theDatapathPackage = (DatapathPackage)EPackage.Registry.INSTANCE.getEPackage(DatapathPackage.eNS_URI);
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
		OperatorsPackage theOperatorsPackage = (OperatorsPackage)EPackage.Registry.INSTANCE.getEPackage(OperatorsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		fsmEClass.getESuperTypes().add(theDatapathPackage.getActivableBlock());
		fsmEClass.getESuperTypes().add(theDatapathPackage.getFlagBearerBlock());
		fsmEClass.getESuperTypes().add(theDatapathPackage.getMealySequentialBlock());
		booleanCommandValueEClass.getESuperTypes().add(this.getAbstractCommandValue());
		integerCommandValueEClass.getESuperTypes().add(this.getAbstractCommandValue());
		andExpressionEClass.getESuperTypes().add(this.getBooleanOperatorExpression());
		orExpressionEClass.getESuperTypes().add(this.getBooleanOperatorExpression());
		booleanConstantEClass.getESuperTypes().add(this.getAbstractBooleanExpression());
		booleanOperatorExpressionEClass.getESuperTypes().add(this.getAbstractBooleanExpression());
		negateExpressionEClass.getESuperTypes().add(this.getAbstractBooleanExpression());
		booleanFlagTermEClass.getESuperTypes().add(this.getFlagTerm());
		flagTermEClass.getESuperTypes().add(this.getAbstractBooleanExpression());
		integerFlagTermEClass.getESuperTypes().add(this.getFlagTerm());
		indexedBooleanFlagTermEClass.getESuperTypes().add(this.getBooleanFlagTerm());

		// Initialize classes and features; add operations and parameters
		initEClass(fsmEClass, fr.irisa.cairn.model.fsm.FSM.class, "FSM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFSM_States(), this.getState(), this.getState_Parent(), "states", null, 1, -1, fr.irisa.cairn.model.fsm.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_Start(), this.getState(), null, "start", null, 1, 1, fr.irisa.cairn.model.fsm.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFSM_DefaultValues(), this.getOutputDefaultValues(), null, "defaultValues", null, 0, -1, fr.irisa.cairn.model.fsm.FSM.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(fsmEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		EOperation op = addEOperation(fsmEClass, null, "removeState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "state", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fsmEClass, thePortPackage.getOutControlPort(), "getCommandFromName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fsmEClass, thePortPackage.getInControlPort(), "getFlagFromName", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(fsmEClass, this.getState(), "getStateFromLabel", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(stateEClass, State.class, "State", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getState_Label(), ecorePackage.getEString(), "label", "S0", 1, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getState_Parent(), this.getFSM(), this.getFSM_States(), "parent", null, 1, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getState_ActivatedCommands(), this.getAbstractCommandValue(), null, "activatedCommands", null, 0, -1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getState_Transitions(), this.getTransition(), this.getTransition_Src(), "transitions", null, 1, -1, State.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(stateEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(stateEClass, this.getTransition(), "connectTo", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "dst", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(stateEClass, this.getTransition(), "disconnectFrom", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "dst", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(stateEClass, this.getState(), "getNextStates", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(stateEClass, ecorePackage.getEBoolean(), "isReachableFrom", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "dst", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(stateEClass, ecorePackage.getEBoolean(), "isConsistent", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTransition_Label(), ecorePackage.getEString(), "label", "transition_0", 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Src(), this.getState(), this.getState_Transitions(), "src", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Dst(), this.getState(), null, "dst", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransition_Predicate(), this.getAbstractBooleanExpression(), null, "predicate", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(transitionEClass, this.getTransition(), "reconnectTo", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "dst", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(transitionEClass, this.getTransition(), "reconnectFrom", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getState(), "dst", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(transitionEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(abstractCommandValueEClass, AbstractCommandValue.class, "AbstractCommandValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractCommandValue_Command(), thePortPackage.getOutControlPort(), null, "command", null, 1, 1, AbstractCommandValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractCommandValue_Parent(), this.getState(), null, "parent", null, 0, 1, AbstractCommandValue.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractCommandValue_CommandName(), ecorePackage.getEString(), "commandName", null, 0, 1, AbstractCommandValue.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractCommandValue_Predicate(), this.getAbstractBooleanExpression(), null, "predicate", null, 0, 1, AbstractCommandValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(abstractCommandValueEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(booleanCommandValueEClass, BooleanCommandValue.class, "BooleanCommandValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanCommandValue_Value(), this.getBooleanValue(), "value", null, 1, 1, BooleanCommandValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(booleanCommandValueEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(integerCommandValueEClass, IntegerCommandValue.class, "IntegerCommandValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerCommandValue_Value(), ecorePackage.getEInt(), "value", null, 1, 1, IntegerCommandValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(integerCommandValueEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(abstractBooleanExpressionEClass, AbstractBooleanExpression.class, "AbstractBooleanExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractBooleanExpression_Expression(), ecorePackage.getEString(), "expression", "\"[?]\"", 0, 1, AbstractBooleanExpression.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		addEOperation(abstractBooleanExpressionEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(abstractBooleanExpressionEClass, ecorePackage.getEBoolean(), "isFalse", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(abstractBooleanExpressionEClass, ecorePackage.getEBoolean(), "isTrue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(andExpressionEClass, AndExpression.class, "AndExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(andExpressionEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(orExpressionEClass, OrExpression.class, "OrExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		addEOperation(orExpressionEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(outputDefaultValuesEClass, OutputDefaultValues.class, "OutputDefaultValues", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOutputDefaultValues_Port(), thePortPackage.getOutControlPort(), null, "port", null, 0, 1, OutputDefaultValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOutputDefaultValues_Value(), ecorePackage.getEInt(), "value", null, 0, 1, OutputDefaultValues.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanConstantEClass, BooleanConstant.class, "BooleanConstant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanConstant_Value(), ecorePackage.getEBoolean(), "value", "true", 1, 1, BooleanConstant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanOperatorExpressionEClass, BooleanOperatorExpression.class, "BooleanOperatorExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooleanOperatorExpression_Terms(), this.getAbstractBooleanExpression(), null, "terms", null, 1, -1, BooleanOperatorExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(negateExpressionEClass, NegateExpression.class, "NegateExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNegateExpression_Term(), this.getAbstractBooleanExpression(), null, "term", null, 1, 1, NegateExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(negateExpressionEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(booleanFlagTermEClass, BooleanFlagTerm.class, "BooleanFlagTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanFlagTerm_Negated(), ecorePackage.getEBoolean(), "negated", null, 1, 1, BooleanFlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(booleanFlagTermEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(booleanFlagTermEClass, ecorePackage.getEString(), "getTermName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(flagTermEClass, FlagTerm.class, "FlagTerm", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFlagTerm_Flag(), thePortPackage.getInControlPort(), null, "flag", null, 0, 1, FlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(integerFlagTermEClass, IntegerFlagTerm.class, "IntegerFlagTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntegerFlagTerm_Equal(), ecorePackage.getEBoolean(), "equal", null, 1, 1, IntegerFlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerFlagTerm_Value(), ecorePackage.getEInt(), "value", "-1", 0, 1, IntegerFlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntegerFlagTerm_OpType(), theOperatorsPackage.getCompareOpcode(), "opType", null, 0, 1, IntegerFlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(integerFlagTermEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(integerFlagTermEClass, ecorePackage.getEString(), "getTermName", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(indexedBooleanFlagTermEClass, IndexedBooleanFlagTerm.class, "IndexedBooleanFlagTerm", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIndexedBooleanFlagTerm_Offset(), ecorePackage.getEInt(), "offset", "-1", 1, 1, IndexedBooleanFlagTerm.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(indexedBooleanFlagTermEClass, ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(indexedBooleanFlagTermEClass, ecorePackage.getEString(), "getTermName", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(booleanValueEEnum, BooleanValue.class, "BooleanValue");
		addEEnumLiteral(booleanValueEEnum, BooleanValue.ZERO);
		addEEnumLiteral(booleanValueEEnum, BooleanValue.ONE);
		addEEnumLiteral(booleanValueEEnum, BooleanValue.DONT_CARE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/ocl/examples/OCL
		createOCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";		
		addAnnotation
		  (fsmEClass, 
		   source, 
		   new String[] {
			 "constraints", "validName uniqueStateLabel uniqueFlagName uniqueCommandName uniqueTransitionLabel\r\n"
		   });						
		addAnnotation
		  (stateEClass, 
		   source, 
		   new String[] {
			 "constraints", "validName noDuplicateDestination isReachableFromInitialState"
		   });									
		addAnnotation
		  (transitionEClass, 
		   source, 
		   new String[] {
			 "constraints", "validName noDuplicateDestination isReachableFromInitialState completePredicateCover"
		   });				
		addAnnotation
		  (abstractCommandValueEClass, 
		   source, 
		   new String[] {
			 "constraints", "satisfiablePredicate"
		   });		
		addAnnotation
		  (integerCommandValueEClass, 
		   source, 
		   new String[] {
			 "constraints", "legalCommandValue"
		   });			
		addAnnotation
		  (orExpressionEClass, 
		   source, 
		   new String[] {
			 "constraints", "satisfiablePredicate"
		   });									
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/ocl/examples/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.eclipse.org/ocl/examples/OCL";			
		addAnnotation
		  (fsmEClass, 
		   source, 
		   new String[] {
			 "uniqueFlagName", "self.flags->forAll(c1:FlagPort , c2:FlagPort | c1.name=c2.name implies c1=c2)",
			 "uniqueCommandName", "self.commands->forAll(c1:CommandPort , c2:CommandPort | c1.name=c2.name implies c1=c2)",
			 "uniqueStateLabel", "self.states->forAll(c1:State , c2:State | c1.label=c2.label implies c1=c2)",
			 "uniqueTransitionLabel", "self.transitions->forAll(c1:Transition , c2:Transition | c1.label=c2.label implies c1=c2)",
			 "validName", "self.name<>\"\""
		   });						
		addAnnotation
		  (stateEClass, 
		   source, 
		   new String[] {
			 "validName", "self.label<>\"\"",
			 "noDuplicateDestination", "self.transitions->forAll( p1, p2 | p1.dst = p2.dst implies p1=p2 )",
			 "isReachableFromInitialState", "self.parent.start->closure(transitions.dst)->includes(self)"
		   });									
		addAnnotation
		  (transitionEClass, 
		   source, 
		   new String[] {
			 "noDuplicateDestination", "self.transitions->forAll( p1, p2 | p1.dst = p2.dst implies p1=p2 )",
			 "isReachableFromInitialState", "self.parent.start->closure(transitions.dst)->includes(self)",
			 "validName", "self.label<>\"\""
		   });							
		addAnnotation
		  (orExpressionEClass, 
		   source, 
		   new String[] {
			 "satisfiablePredicate", "not self.isAlwaysFalse()"
		   });								
	}

} //FsmPackageImpl
