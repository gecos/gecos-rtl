/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Negate Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.NegateExpression#getTerm <em>Term</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getNegateExpression()
 * @model
 * @generated
 */
public interface NegateExpression extends AbstractBooleanExpression {
	/**
	 * Returns the value of the '<em><b>Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Term</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Term</em>' containment reference.
	 * @see #setTerm(AbstractBooleanExpression)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getNegateExpression_Term()
	 * @model containment="true" required="true"
	 * @generated
	 */
	AbstractBooleanExpression getTerm();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.NegateExpression#getTerm <em>Term</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Term</em>' containment reference.
	 * @see #getTerm()
	 * @generated
	 */
	void setTerm(AbstractBooleanExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return BoolExpPrettyPrint.print(this);'"
	 * @generated
	 */
	String toString();

} // NegateExpression
