/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Operator Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.BooleanOperatorExpression#getTerms <em>Terms</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanOperatorExpression()
 * @model abstract="true"
 * @generated
 */
public interface BooleanOperatorExpression extends AbstractBooleanExpression {
	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link fr.irisa.cairn.model.fsm.AbstractBooleanExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getBooleanOperatorExpression_Terms()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<AbstractBooleanExpression> getTerms();

} // BooleanOperatorExpression
