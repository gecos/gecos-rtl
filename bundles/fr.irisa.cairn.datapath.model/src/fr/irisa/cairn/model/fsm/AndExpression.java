/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getAndExpression()
 * @model
 * @generated
 */
public interface AndExpression extends BooleanOperatorExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return BoolExpPrettyPrint.print(this);'"
	 * @generated
	 */
	String toString();

} // AndExpression
