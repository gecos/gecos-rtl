/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.MealySequentialBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.SequentialBlock;
import fr.irisa.cairn.model.fsm.*;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanOperatorExpression;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.fsm.FsmPackage
 * @generated
 */
public class FsmSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FsmPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FsmSwitch() {
		if (modelPackage == null) {
			modelPackage = FsmPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case FsmPackage.FSM: {
				FSM fsm = (FSM)theEObject;
				T result = caseFSM(fsm);
				if (result == null) result = caseActivableBlock(fsm);
				if (result == null) result = caseFlagBearerBlock(fsm);
				if (result == null) result = caseMealySequentialBlock(fsm);
				if (result == null) result = caseAbstractBlock(fsm);
				if (result == null) result = caseSequentialBlock(fsm);
				if (result == null) result = caseNamedElement(fsm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.STATE: {
				State state = (State)theEObject;
				T result = caseState(state);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.TRANSITION: {
				Transition transition = (Transition)theEObject;
				T result = caseTransition(transition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.ABSTRACT_COMMAND_VALUE: {
				AbstractCommandValue abstractCommandValue = (AbstractCommandValue)theEObject;
				T result = caseAbstractCommandValue(abstractCommandValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.BOOLEAN_COMMAND_VALUE: {
				BooleanCommandValue booleanCommandValue = (BooleanCommandValue)theEObject;
				T result = caseBooleanCommandValue(booleanCommandValue);
				if (result == null) result = caseAbstractCommandValue(booleanCommandValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.INTEGER_COMMAND_VALUE: {
				IntegerCommandValue integerCommandValue = (IntegerCommandValue)theEObject;
				T result = caseIntegerCommandValue(integerCommandValue);
				if (result == null) result = caseAbstractCommandValue(integerCommandValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.ABSTRACT_BOOLEAN_EXPRESSION: {
				AbstractBooleanExpression abstractBooleanExpression = (AbstractBooleanExpression)theEObject;
				T result = caseAbstractBooleanExpression(abstractBooleanExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.AND_EXPRESSION: {
				AndExpression andExpression = (AndExpression)theEObject;
				T result = caseAndExpression(andExpression);
				if (result == null) result = caseBooleanOperatorExpression(andExpression);
				if (result == null) result = caseAbstractBooleanExpression(andExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.OR_EXPRESSION: {
				OrExpression orExpression = (OrExpression)theEObject;
				T result = caseOrExpression(orExpression);
				if (result == null) result = caseBooleanOperatorExpression(orExpression);
				if (result == null) result = caseAbstractBooleanExpression(orExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.OUTPUT_DEFAULT_VALUES: {
				OutputDefaultValues outputDefaultValues = (OutputDefaultValues)theEObject;
				T result = caseOutputDefaultValues(outputDefaultValues);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.BOOLEAN_CONSTANT: {
				BooleanConstant booleanConstant = (BooleanConstant)theEObject;
				T result = caseBooleanConstant(booleanConstant);
				if (result == null) result = caseAbstractBooleanExpression(booleanConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.BOOLEAN_OPERATOR_EXPRESSION: {
				BooleanOperatorExpression booleanOperatorExpression = (BooleanOperatorExpression)theEObject;
				T result = caseBooleanOperatorExpression(booleanOperatorExpression);
				if (result == null) result = caseAbstractBooleanExpression(booleanOperatorExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.NEGATE_EXPRESSION: {
				NegateExpression negateExpression = (NegateExpression)theEObject;
				T result = caseNegateExpression(negateExpression);
				if (result == null) result = caseAbstractBooleanExpression(negateExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.BOOLEAN_FLAG_TERM: {
				BooleanFlagTerm booleanFlagTerm = (BooleanFlagTerm)theEObject;
				T result = caseBooleanFlagTerm(booleanFlagTerm);
				if (result == null) result = caseFlagTerm(booleanFlagTerm);
				if (result == null) result = caseAbstractBooleanExpression(booleanFlagTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.FLAG_TERM: {
				FlagTerm flagTerm = (FlagTerm)theEObject;
				T result = caseFlagTerm(flagTerm);
				if (result == null) result = caseAbstractBooleanExpression(flagTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.INTEGER_FLAG_TERM: {
				IntegerFlagTerm integerFlagTerm = (IntegerFlagTerm)theEObject;
				T result = caseIntegerFlagTerm(integerFlagTerm);
				if (result == null) result = caseFlagTerm(integerFlagTerm);
				if (result == null) result = caseAbstractBooleanExpression(integerFlagTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case FsmPackage.INDEXED_BOOLEAN_FLAG_TERM: {
				IndexedBooleanFlagTerm indexedBooleanFlagTerm = (IndexedBooleanFlagTerm)theEObject;
				T result = caseIndexedBooleanFlagTerm(indexedBooleanFlagTerm);
				if (result == null) result = caseBooleanFlagTerm(indexedBooleanFlagTerm);
				if (result == null) result = caseFlagTerm(indexedBooleanFlagTerm);
				if (result == null) result = caseAbstractBooleanExpression(indexedBooleanFlagTerm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FSM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFSM(FSM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseState(State object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Command Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Command Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractCommandValue(AbstractCommandValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Command Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Command Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanCommandValue(BooleanCommandValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Command Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Command Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerCommandValue(IntegerCommandValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Boolean Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Boolean Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractBooleanExpression(AbstractBooleanExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndExpression(AndExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrExpression(OrExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flag Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flag Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlagTerm(FlagTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Flag Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Flag Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerFlagTerm(IntegerFlagTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Indexed Boolean Flag Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Indexed Boolean Flag Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndexedBooleanFlagTerm(IndexedBooleanFlagTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Default Values</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Default Values</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputDefaultValues(OutputDefaultValues object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanConstant(BooleanConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Operator Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Operator Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanOperatorExpression(BooleanOperatorExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Negate Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Negate Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNegateExpression(NegateExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Flag Term</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Flag Term</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanFlagTerm(BooleanFlagTerm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractBlock(AbstractBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activable Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivableBlock(ActivableBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Flag Bearer Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFlagBearerBlock(FlagBearerBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequentialBlock(SequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mealy Sequential Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMealySequentialBlock(MealySequentialBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //FsmSwitch
