/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sum Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.SumPredicate#getParent <em>Parent</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getSumPredicate()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='nonEmptyTermList'"
 *        annotation="http://www.eclipse.org/ocl/examples/OCL nonEmptyTermList='self.super.terms.size()<>0'"
 * @generated
 */
public interface SumPredicate extends EObject {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(AndExpression)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getSumPredicate_Parent()
	 * @model required="true"
	 * @generated
	 */
	AndExpression getParent();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.SumPredicate#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(AndExpression value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 * @generated
	 */
	String toString();

} // SumPredicate
