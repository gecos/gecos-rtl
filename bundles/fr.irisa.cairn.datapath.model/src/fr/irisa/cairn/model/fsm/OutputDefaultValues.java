/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.port.OutControlPort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Default Values</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getPort <em>Port</em>}</li>
 *   <li>{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see fr.irisa.cairn.model.fsm.FsmPackage#getOutputDefaultValues()
 * @model
 * @generated
 */
public interface OutputDefaultValues extends EObject {
	/**
	 * Returns the value of the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' reference.
	 * @see #setPort(OutControlPort)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getOutputDefaultValues_Port()
	 * @model
	 * @generated
	 */
	OutControlPort getPort();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getPort <em>Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(OutControlPort value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see fr.irisa.cairn.model.fsm.FsmPackage#getOutputDefaultValues_Value()
	 * @model
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

} // OutputDefaultValues
