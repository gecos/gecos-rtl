/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.fsm;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fr.irisa.cairn.model.datapath.DatapathPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.model.fsm.FsmFactory
 * @model kind="package"
 * @generated
 */
public interface FsmPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "fsm";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.irisa.fr/cairn/datapath/fsm";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.model.fsm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FsmPackage eINSTANCE = fr.irisa.cairn.model.fsm.impl.FsmPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.FSMImpl <em>FSM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.FSMImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getFSM()
	 * @generated
	 */
	int FSM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__NAME = DatapathPackage.ACTIVABLE_BLOCK__NAME;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__PARENT = DatapathPackage.ACTIVABLE_BLOCK__PARENT;

	/**
	 * The feature id for the '<em><b>Combinational</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__COMBINATIONAL = DatapathPackage.ACTIVABLE_BLOCK__COMBINATIONAL;

	/**
	 * The feature id for the '<em><b>Activate</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__ACTIVATE = DatapathPackage.ACTIVABLE_BLOCK__ACTIVATE;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__FLAGS = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__STATES = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__START = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Default Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM__DEFAULT_VALUES = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FSM_FEATURE_COUNT = DatapathPackage.ACTIVABLE_BLOCK_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.StateImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getState()
	 * @generated
	 */
	int STATE = 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__PARENT = 1;

	/**
	 * The feature id for the '<em><b>Activated Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ACTIVATED_COMMANDS = 2;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__TRANSITIONS = 3;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.TransitionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LABEL = 0;

	/**
	 * The feature id for the '<em><b>Src</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SRC = 1;

	/**
	 * The feature id for the '<em><b>Dst</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DST = 2;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__PREDICATE = 3;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.AbstractCommandValueImpl <em>Abstract Command Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.AbstractCommandValueImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAbstractCommandValue()
	 * @generated
	 */
	int ABSTRACT_COMMAND_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMMAND_VALUE__COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMMAND_VALUE__PARENT = 1;

	/**
	 * The feature id for the '<em><b>Command Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMMAND_VALUE__COMMAND_NAME = 2;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMMAND_VALUE__PREDICATE = 3;

	/**
	 * The number of structural features of the '<em>Abstract Command Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_COMMAND_VALUE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanCommandValueImpl <em>Boolean Command Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.BooleanCommandValueImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanCommandValue()
	 * @generated
	 */
	int BOOLEAN_COMMAND_VALUE = 4;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE__COMMAND = ABSTRACT_COMMAND_VALUE__COMMAND;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE__PARENT = ABSTRACT_COMMAND_VALUE__PARENT;

	/**
	 * The feature id for the '<em><b>Command Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE__COMMAND_NAME = ABSTRACT_COMMAND_VALUE__COMMAND_NAME;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE__PREDICATE = ABSTRACT_COMMAND_VALUE__PREDICATE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE__VALUE = ABSTRACT_COMMAND_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Command Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_COMMAND_VALUE_FEATURE_COUNT = ABSTRACT_COMMAND_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.IntegerCommandValueImpl <em>Integer Command Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.IntegerCommandValueImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIntegerCommandValue()
	 * @generated
	 */
	int INTEGER_COMMAND_VALUE = 5;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE__COMMAND = ABSTRACT_COMMAND_VALUE__COMMAND;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE__PARENT = ABSTRACT_COMMAND_VALUE__PARENT;

	/**
	 * The feature id for the '<em><b>Command Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE__COMMAND_NAME = ABSTRACT_COMMAND_VALUE__COMMAND_NAME;

	/**
	 * The feature id for the '<em><b>Predicate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE__PREDICATE = ABSTRACT_COMMAND_VALUE__PREDICATE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE__VALUE = ABSTRACT_COMMAND_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Command Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_COMMAND_VALUE_FEATURE_COUNT = ABSTRACT_COMMAND_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.AbstractBooleanExpressionImpl <em>Abstract Boolean Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.AbstractBooleanExpressionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAbstractBooleanExpression()
	 * @generated
	 */
	int ABSTRACT_BOOLEAN_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>Abstract Boolean Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanOperatorExpressionImpl <em>Boolean Operator Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.BooleanOperatorExpressionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanOperatorExpression()
	 * @generated
	 */
	int BOOLEAN_OPERATOR_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_OPERATOR_EXPRESSION__EXPRESSION = ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_OPERATOR_EXPRESSION__TERMS = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Operator Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_OPERATOR_EXPRESSION_FEATURE_COUNT = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.AndExpressionImpl <em>And Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.AndExpressionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAndExpression()
	 * @generated
	 */
	int AND_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION__EXPRESSION = BOOLEAN_OPERATOR_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION__TERMS = BOOLEAN_OPERATOR_EXPRESSION__TERMS;

	/**
	 * The number of structural features of the '<em>And Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_EXPRESSION_FEATURE_COUNT = BOOLEAN_OPERATOR_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.OrExpressionImpl <em>Or Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.OrExpressionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getOrExpression()
	 * @generated
	 */
	int OR_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION__EXPRESSION = BOOLEAN_OPERATOR_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION__TERMS = BOOLEAN_OPERATOR_EXPRESSION__TERMS;

	/**
	 * The number of structural features of the '<em>Or Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_EXPRESSION_FEATURE_COUNT = BOOLEAN_OPERATOR_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.FlagTermImpl <em>Flag Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.FlagTermImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getFlagTerm()
	 * @generated
	 */
	int FLAG_TERM = 14;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanFlagTermImpl <em>Boolean Flag Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.BooleanFlagTermImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanFlagTerm()
	 * @generated
	 */
	int BOOLEAN_FLAG_TERM = 13;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl <em>Integer Flag Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIntegerFlagTerm()
	 * @generated
	 */
	int INTEGER_FLAG_TERM = 15;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.OutputDefaultValuesImpl <em>Output Default Values</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.OutputDefaultValuesImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getOutputDefaultValues()
	 * @generated
	 */
	int OUTPUT_DEFAULT_VALUES = 9;

	/**
	 * The feature id for the '<em><b>Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_DEFAULT_VALUES__PORT = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_DEFAULT_VALUES__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Output Default Values</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_DEFAULT_VALUES_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanConstantImpl <em>Boolean Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.BooleanConstantImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanConstant()
	 * @generated
	 */
	int BOOLEAN_CONSTANT = 10;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT__EXPRESSION = ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT__VALUE = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_CONSTANT_FEATURE_COUNT = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.NegateExpressionImpl <em>Negate Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.NegateExpressionImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getNegateExpression()
	 * @generated
	 */
	int NEGATE_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATE_EXPRESSION__EXPRESSION = ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Term</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATE_EXPRESSION__TERM = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Negate Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATE_EXPRESSION_FEATURE_COUNT = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_TERM__EXPRESSION = ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Flag</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_TERM__FLAG = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Flag Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLAG_TERM_FEATURE_COUNT = ABSTRACT_BOOLEAN_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_FLAG_TERM__EXPRESSION = FLAG_TERM__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Flag</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_FLAG_TERM__FLAG = FLAG_TERM__FLAG;

	/**
	 * The feature id for the '<em><b>Negated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_FLAG_TERM__NEGATED = FLAG_TERM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Flag Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_FLAG_TERM_FEATURE_COUNT = FLAG_TERM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM__EXPRESSION = FLAG_TERM__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Flag</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM__FLAG = FLAG_TERM__FLAG;

	/**
	 * The feature id for the '<em><b>Equal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM__EQUAL = FLAG_TERM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM__VALUE = FLAG_TERM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Op Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM__OP_TYPE = FLAG_TERM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Integer Flag Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_FLAG_TERM_FEATURE_COUNT = FLAG_TERM_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.impl.IndexedBooleanFlagTermImpl <em>Indexed Boolean Flag Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.impl.IndexedBooleanFlagTermImpl
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIndexedBooleanFlagTerm()
	 * @generated
	 */
	int INDEXED_BOOLEAN_FLAG_TERM = 16;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDEXED_BOOLEAN_FLAG_TERM__EXPRESSION = BOOLEAN_FLAG_TERM__EXPRESSION;

	/**
	 * The feature id for the '<em><b>Flag</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDEXED_BOOLEAN_FLAG_TERM__FLAG = BOOLEAN_FLAG_TERM__FLAG;

	/**
	 * The feature id for the '<em><b>Negated</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDEXED_BOOLEAN_FLAG_TERM__NEGATED = BOOLEAN_FLAG_TERM__NEGATED;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDEXED_BOOLEAN_FLAG_TERM__OFFSET = BOOLEAN_FLAG_TERM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Indexed Boolean Flag Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INDEXED_BOOLEAN_FLAG_TERM_FEATURE_COUNT = BOOLEAN_FLAG_TERM_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.model.fsm.BooleanValue <em>Boolean Value</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.model.fsm.BooleanValue
	 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanValue()
	 * @generated
	 */
	int BOOLEAN_VALUE = 17;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.FSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM</em>'.
	 * @see fr.irisa.cairn.model.fsm.FSM
	 * @generated
	 */
	EClass getFSM();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.fsm.FSM#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see fr.irisa.cairn.model.fsm.FSM#getStates()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_States();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.FSM#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Start</em>'.
	 * @see fr.irisa.cairn.model.fsm.FSM#getStart()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_Start();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.fsm.FSM#getDefaultValues <em>Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Default Values</em>'.
	 * @see fr.irisa.cairn.model.fsm.FSM#getDefaultValues()
	 * @see #getFSM()
	 * @generated
	 */
	EReference getFSM_DefaultValues();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see fr.irisa.cairn.model.fsm.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.State#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see fr.irisa.cairn.model.fsm.State#getLabel()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Label();

	/**
	 * Returns the meta object for the container reference '{@link fr.irisa.cairn.model.fsm.State#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see fr.irisa.cairn.model.fsm.State#getParent()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.fsm.State#getActivatedCommands <em>Activated Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activated Commands</em>'.
	 * @see fr.irisa.cairn.model.fsm.State#getActivatedCommands()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_ActivatedCommands();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.fsm.State#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see fr.irisa.cairn.model.fsm.State#getTransitions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Transitions();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see fr.irisa.cairn.model.fsm.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.Transition#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see fr.irisa.cairn.model.fsm.Transition#getLabel()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Label();

	/**
	 * Returns the meta object for the container reference '{@link fr.irisa.cairn.model.fsm.Transition#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Src</em>'.
	 * @see fr.irisa.cairn.model.fsm.Transition#getSrc()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Src();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.Transition#getDst <em>Dst</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dst</em>'.
	 * @see fr.irisa.cairn.model.fsm.Transition#getDst()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Dst();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.model.fsm.Transition#getPredicate <em>Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Predicate</em>'.
	 * @see fr.irisa.cairn.model.fsm.Transition#getPredicate()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Predicate();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue <em>Abstract Command Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Command Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractCommandValue
	 * @generated
	 */
	EClass getAbstractCommandValue();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Command</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommand()
	 * @see #getAbstractCommandValue()
	 * @generated
	 */
	EReference getAbstractCommandValue_Command();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractCommandValue#getParent()
	 * @see #getAbstractCommandValue()
	 * @generated
	 */
	EReference getAbstractCommandValue_Parent();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommandName <em>Command Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Command Name</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractCommandValue#getCommandName()
	 * @see #getAbstractCommandValue()
	 * @generated
	 */
	EAttribute getAbstractCommandValue_CommandName();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.model.fsm.AbstractCommandValue#getPredicate <em>Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Predicate</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractCommandValue#getPredicate()
	 * @see #getAbstractCommandValue()
	 * @generated
	 */
	EReference getAbstractCommandValue_Predicate();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.BooleanCommandValue <em>Boolean Command Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Command Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanCommandValue
	 * @generated
	 */
	EClass getBooleanCommandValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.BooleanCommandValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanCommandValue#getValue()
	 * @see #getBooleanCommandValue()
	 * @generated
	 */
	EAttribute getBooleanCommandValue_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.IntegerCommandValue <em>Integer Command Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Command Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerCommandValue
	 * @generated
	 */
	EClass getIntegerCommandValue();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.IntegerCommandValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerCommandValue#getValue()
	 * @see #getIntegerCommandValue()
	 * @generated
	 */
	EAttribute getIntegerCommandValue_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.AbstractBooleanExpression <em>Abstract Boolean Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Boolean Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractBooleanExpression
	 * @generated
	 */
	EClass getAbstractBooleanExpression();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.AbstractBooleanExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.AbstractBooleanExpression#getExpression()
	 * @see #getAbstractBooleanExpression()
	 * @generated
	 */
	EAttribute getAbstractBooleanExpression_Expression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.AndExpression <em>And Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.AndExpression
	 * @generated
	 */
	EClass getAndExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.OrExpression <em>Or Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.OrExpression
	 * @generated
	 */
	EClass getOrExpression();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.FlagTerm <em>Flag Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flag Term</em>'.
	 * @see fr.irisa.cairn.model.fsm.FlagTerm
	 * @generated
	 */
	EClass getFlagTerm();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.FlagTerm#getFlag <em>Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Flag</em>'.
	 * @see fr.irisa.cairn.model.fsm.FlagTerm#getFlag()
	 * @see #getFlagTerm()
	 * @generated
	 */
	EReference getFlagTerm_Flag();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm <em>Integer Flag Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Flag Term</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerFlagTerm
	 * @generated
	 */
	EClass getIntegerFlagTerm();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#isEqual <em>Equal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Equal</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerFlagTerm#isEqual()
	 * @see #getIntegerFlagTerm()
	 * @generated
	 */
	EAttribute getIntegerFlagTerm_Equal();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerFlagTerm#getValue()
	 * @see #getIntegerFlagTerm()
	 * @generated
	 */
	EAttribute getIntegerFlagTerm_Value();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.IntegerFlagTerm#getOpType <em>Op Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Op Type</em>'.
	 * @see fr.irisa.cairn.model.fsm.IntegerFlagTerm#getOpType()
	 * @see #getIntegerFlagTerm()
	 * @generated
	 */
	EAttribute getIntegerFlagTerm_OpType();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm <em>Indexed Boolean Flag Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Indexed Boolean Flag Term</em>'.
	 * @see fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm
	 * @generated
	 */
	EClass getIndexedBooleanFlagTerm();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm#getOffset()
	 * @see #getIndexedBooleanFlagTerm()
	 * @generated
	 */
	EAttribute getIndexedBooleanFlagTerm_Offset();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.OutputDefaultValues <em>Output Default Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Default Values</em>'.
	 * @see fr.irisa.cairn.model.fsm.OutputDefaultValues
	 * @generated
	 */
	EClass getOutputDefaultValues();

	/**
	 * Returns the meta object for the reference '{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Port</em>'.
	 * @see fr.irisa.cairn.model.fsm.OutputDefaultValues#getPort()
	 * @see #getOutputDefaultValues()
	 * @generated
	 */
	EReference getOutputDefaultValues_Port();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.OutputDefaultValues#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.OutputDefaultValues#getValue()
	 * @see #getOutputDefaultValues()
	 * @generated
	 */
	EAttribute getOutputDefaultValues_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.BooleanConstant <em>Boolean Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Constant</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanConstant
	 * @generated
	 */
	EClass getBooleanConstant();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.BooleanConstant#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanConstant#isValue()
	 * @see #getBooleanConstant()
	 * @generated
	 */
	EAttribute getBooleanConstant_Value();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.BooleanOperatorExpression <em>Boolean Operator Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Operator Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanOperatorExpression
	 * @generated
	 */
	EClass getBooleanOperatorExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.irisa.cairn.model.fsm.BooleanOperatorExpression#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Terms</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanOperatorExpression#getTerms()
	 * @see #getBooleanOperatorExpression()
	 * @generated
	 */
	EReference getBooleanOperatorExpression_Terms();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.NegateExpression <em>Negate Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negate Expression</em>'.
	 * @see fr.irisa.cairn.model.fsm.NegateExpression
	 * @generated
	 */
	EClass getNegateExpression();

	/**
	 * Returns the meta object for the containment reference '{@link fr.irisa.cairn.model.fsm.NegateExpression#getTerm <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Term</em>'.
	 * @see fr.irisa.cairn.model.fsm.NegateExpression#getTerm()
	 * @see #getNegateExpression()
	 * @generated
	 */
	EReference getNegateExpression_Term();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.model.fsm.BooleanFlagTerm <em>Boolean Flag Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Flag Term</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanFlagTerm
	 * @generated
	 */
	EClass getBooleanFlagTerm();

	/**
	 * Returns the meta object for the attribute '{@link fr.irisa.cairn.model.fsm.BooleanFlagTerm#isNegated <em>Negated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Negated</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanFlagTerm#isNegated()
	 * @see #getBooleanFlagTerm()
	 * @generated
	 */
	EAttribute getBooleanFlagTerm_Negated();

	/**
	 * Returns the meta object for enum '{@link fr.irisa.cairn.model.fsm.BooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Boolean Value</em>'.
	 * @see fr.irisa.cairn.model.fsm.BooleanValue
	 * @generated
	 */
	EEnum getBooleanValue();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FsmFactory getFsmFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.FSMImpl <em>FSM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.FSMImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getFSM()
		 * @generated
		 */
		EClass FSM = eINSTANCE.getFSM();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__STATES = eINSTANCE.getFSM_States();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__START = eINSTANCE.getFSM_Start();

		/**
		 * The meta object literal for the '<em><b>Default Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FSM__DEFAULT_VALUES = eINSTANCE.getFSM_DefaultValues();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.StateImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__LABEL = eINSTANCE.getState_Label();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__PARENT = eINSTANCE.getState_Parent();

		/**
		 * The meta object literal for the '<em><b>Activated Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__ACTIVATED_COMMANDS = eINSTANCE.getState_ActivatedCommands();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__TRANSITIONS = eINSTANCE.getState_Transitions();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.TransitionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__LABEL = eINSTANCE.getTransition_Label();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SRC = eINSTANCE.getTransition_Src();

		/**
		 * The meta object literal for the '<em><b>Dst</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__DST = eINSTANCE.getTransition_Dst();

		/**
		 * The meta object literal for the '<em><b>Predicate</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__PREDICATE = eINSTANCE.getTransition_Predicate();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.AbstractCommandValueImpl <em>Abstract Command Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.AbstractCommandValueImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAbstractCommandValue()
		 * @generated
		 */
		EClass ABSTRACT_COMMAND_VALUE = eINSTANCE.getAbstractCommandValue();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMMAND_VALUE__COMMAND = eINSTANCE.getAbstractCommandValue_Command();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMMAND_VALUE__PARENT = eINSTANCE.getAbstractCommandValue_Parent();

		/**
		 * The meta object literal for the '<em><b>Command Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_COMMAND_VALUE__COMMAND_NAME = eINSTANCE.getAbstractCommandValue_CommandName();

		/**
		 * The meta object literal for the '<em><b>Predicate</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_COMMAND_VALUE__PREDICATE = eINSTANCE.getAbstractCommandValue_Predicate();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanCommandValueImpl <em>Boolean Command Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.BooleanCommandValueImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanCommandValue()
		 * @generated
		 */
		EClass BOOLEAN_COMMAND_VALUE = eINSTANCE.getBooleanCommandValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_COMMAND_VALUE__VALUE = eINSTANCE.getBooleanCommandValue_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.IntegerCommandValueImpl <em>Integer Command Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.IntegerCommandValueImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIntegerCommandValue()
		 * @generated
		 */
		EClass INTEGER_COMMAND_VALUE = eINSTANCE.getIntegerCommandValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_COMMAND_VALUE__VALUE = eINSTANCE.getIntegerCommandValue_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.AbstractBooleanExpressionImpl <em>Abstract Boolean Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.AbstractBooleanExpressionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAbstractBooleanExpression()
		 * @generated
		 */
		EClass ABSTRACT_BOOLEAN_EXPRESSION = eINSTANCE.getAbstractBooleanExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_BOOLEAN_EXPRESSION__EXPRESSION = eINSTANCE.getAbstractBooleanExpression_Expression();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.AndExpressionImpl <em>And Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.AndExpressionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getAndExpression()
		 * @generated
		 */
		EClass AND_EXPRESSION = eINSTANCE.getAndExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.OrExpressionImpl <em>Or Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.OrExpressionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getOrExpression()
		 * @generated
		 */
		EClass OR_EXPRESSION = eINSTANCE.getOrExpression();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.FlagTermImpl <em>Flag Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.FlagTermImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getFlagTerm()
		 * @generated
		 */
		EClass FLAG_TERM = eINSTANCE.getFlagTerm();

		/**
		 * The meta object literal for the '<em><b>Flag</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FLAG_TERM__FLAG = eINSTANCE.getFlagTerm_Flag();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl <em>Integer Flag Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.IntegerFlagTermImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIntegerFlagTerm()
		 * @generated
		 */
		EClass INTEGER_FLAG_TERM = eINSTANCE.getIntegerFlagTerm();

		/**
		 * The meta object literal for the '<em><b>Equal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_FLAG_TERM__EQUAL = eINSTANCE.getIntegerFlagTerm_Equal();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_FLAG_TERM__VALUE = eINSTANCE.getIntegerFlagTerm_Value();

		/**
		 * The meta object literal for the '<em><b>Op Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_FLAG_TERM__OP_TYPE = eINSTANCE.getIntegerFlagTerm_OpType();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.IndexedBooleanFlagTermImpl <em>Indexed Boolean Flag Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.IndexedBooleanFlagTermImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getIndexedBooleanFlagTerm()
		 * @generated
		 */
		EClass INDEXED_BOOLEAN_FLAG_TERM = eINSTANCE.getIndexedBooleanFlagTerm();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INDEXED_BOOLEAN_FLAG_TERM__OFFSET = eINSTANCE.getIndexedBooleanFlagTerm_Offset();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.OutputDefaultValuesImpl <em>Output Default Values</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.OutputDefaultValuesImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getOutputDefaultValues()
		 * @generated
		 */
		EClass OUTPUT_DEFAULT_VALUES = eINSTANCE.getOutputDefaultValues();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUTPUT_DEFAULT_VALUES__PORT = eINSTANCE.getOutputDefaultValues_Port();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_DEFAULT_VALUES__VALUE = eINSTANCE.getOutputDefaultValues_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanConstantImpl <em>Boolean Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.BooleanConstantImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanConstant()
		 * @generated
		 */
		EClass BOOLEAN_CONSTANT = eINSTANCE.getBooleanConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_CONSTANT__VALUE = eINSTANCE.getBooleanConstant_Value();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanOperatorExpressionImpl <em>Boolean Operator Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.BooleanOperatorExpressionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanOperatorExpression()
		 * @generated
		 */
		EClass BOOLEAN_OPERATOR_EXPRESSION = eINSTANCE.getBooleanOperatorExpression();

		/**
		 * The meta object literal for the '<em><b>Terms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOLEAN_OPERATOR_EXPRESSION__TERMS = eINSTANCE.getBooleanOperatorExpression_Terms();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.NegateExpressionImpl <em>Negate Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.NegateExpressionImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getNegateExpression()
		 * @generated
		 */
		EClass NEGATE_EXPRESSION = eINSTANCE.getNegateExpression();

		/**
		 * The meta object literal for the '<em><b>Term</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEGATE_EXPRESSION__TERM = eINSTANCE.getNegateExpression_Term();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.impl.BooleanFlagTermImpl <em>Boolean Flag Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.impl.BooleanFlagTermImpl
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanFlagTerm()
		 * @generated
		 */
		EClass BOOLEAN_FLAG_TERM = eINSTANCE.getBooleanFlagTerm();

		/**
		 * The meta object literal for the '<em><b>Negated</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_FLAG_TERM__NEGATED = eINSTANCE.getBooleanFlagTerm_Negated();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.model.fsm.BooleanValue <em>Boolean Value</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.model.fsm.BooleanValue
		 * @see fr.irisa.cairn.model.fsm.impl.FsmPackageImpl#getBooleanValue()
		 * @generated
		 */
		EEnum BOOLEAN_VALUE = eINSTANCE.getBooleanValue();

	}

} //FsmPackage
