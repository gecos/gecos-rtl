package fr.irisa.cairn.model.custom.provider.fsm;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;

import fr.irisa.cairn.model.custom.provider.TransientItemProvider;
import fr.irisa.cairn.model.fsm.FsmPackage;
import fr.irisa.cairn.model.fsm.State;

public class StateCommandsItemProvider extends TransientItemProvider {

	public StateCommandsItemProvider(AdapterFactory adapterFactory, State scope) {
		super(adapterFactory, scope);
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(FsmPackage.Literals.STATE__ACTIVATED_COMMANDS);
		}
		return childrenFeatures;
	}

	@Override
	public String getText(Object object) {
		return "Commands";
	}

	@Override
	public Object getParent(Object object) {
		Object scope = super.getParent(object);
		CustomFSMItemProvider scopeItemProvider = (CustomFSMItemProvider) adapterFactory
				.adapt(scope, IEditingDomainItemProvider.class);
		return scopeItemProvider != null ? scopeItemProvider.getWires() : null;
	}

}
