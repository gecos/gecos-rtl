/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.custom.provider.datapath;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.irisa.cairn.model.custom.provider.TransientItemProvider;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.datapath.editor.factory.EditorOperatorsFactory;
import fr.irisa.cairn.model.datapath.editor.factory.EditorPadsFactory;
import fr.irisa.cairn.model.datapath.editor.factory.EditorPortFactory;
import fr.irisa.cairn.model.datapath.editor.factory.EditorStorageFactory;
import fr.irisa.cairn.model.datapath.editor.factory.EditorWiresFactory;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.provider.DatapathEditPlugin;
import fr.irisa.cairn.model.datapath.provider.NamedElementItemProvider;
import fr.irisa.cairn.model.fsm.FsmFactory;

/**
 * This is the item provider adapter for a {@link fr.irisa.cairn.model.datapath.Datapath} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CustomDatapathItemProvider
	extends NamedElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomDatapathItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCombinationalPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Combinational feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCombinationalPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractBlock_combinational_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractBlock_combinational_feature", "_UI_AbstractBlock_type"),
				 DatapathPackage.Literals.ABSTRACT_BLOCK__COMBINATIONAL,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}


	protected List<TransientItemProvider> children = null;

	@Override
	public Collection<?> getChildren(Object object) {

		Datapath s = (Datapath) object;
		children = new ArrayList<TransientItemProvider>();
		children.add(new DatapathPadsItemProvider(adapterFactory, s));
		children.add(new DatapathMemoryItemProvider(adapterFactory, s));
		children.add(new DatapathFSMItemProvider(adapterFactory, s));
		children.add(new DatapathRegistersItemProvider(adapterFactory, s));
		children.add(new DatapathOperatorsItemProvider(adapterFactory, s));
		children.add(new DatapathWiresItemProvider(adapterFactory, s));

		return children;
	}


	/**
	 * This returns Datapath.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		try {
			return getResourceLocator().getImage("full/obj16/Datapath");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		Datapath dp =  (Datapath) object;
		if (dp!=null) {
			try {
				return "Datapath :"+dp.getName()+"("+dp.toString()+")";
			} catch (Exception e) {
				return "Datapath :"+dp.getName();
			}
		} else {
			return "null";
		}
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Datapath.class)) {
			case DatapathPackage.DATAPATH__COMBINATIONAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case DatapathPackage.DATAPATH__ACTIVATE:
			case DatapathPackage.DATAPATH__IN:
			case DatapathPackage.DATAPATH__OUT:
			case DatapathPackage.DATAPATH__FLAGS:
			case DatapathPackage.DATAPATH__COMPONENTS:
			case DatapathPackage.DATAPATH__DATA_WIRES:
			case DatapathPackage.DATAPATH__CONTROL_WIRES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
		Datapath dp = (Datapath) object; 

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATA_FLOW_BLOCK__IN,
			 EditorPortFactory.getFactory().createInDataPort()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATA_FLOW_BLOCK__OUT,
			 EditorPortFactory.getFactory().createOutDataPort()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.ACTIVABLE_BLOCK__ACTIVATE,
			 EditorPortFactory.getFactory().createInControlPort()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.FLAG_BEARER_BLOCK__FLAGS,
			 EditorPortFactory.getFactory().createOutControlPort()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
				 EditorPadsFactory.getFactory().createControlPad()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorPadsFactory.getFactory().createDataInputPad()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorPadsFactory.getFactory().createDataOutputPad()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorPadsFactory.getFactory().createStatusPad()));

			newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorStorageFactory.getFactory().createRegister()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorStorageFactory.getFactory().createCERegister()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorStorageFactory.getFactory().createShiftRegister()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorOperatorsFactory.getFactory().createUnaryOperator()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					FsmFactory.eINSTANCE.createFSM()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.MUL)));
		
		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.MULU)));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.MAX)));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.MIN)));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.SUB)));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createBinaryOperator(BinaryOpcode.ADD)));

//		newChildDescriptors.add
//			(createChildParameter
//				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
//				 OperatorsFactory.getFactory().createTernaryOperator()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorOperatorsFactory.getFactory().createConstantValue()));

		newChildDescriptors.add
		(createChildParameter
			(DatapathPackage.Literals.DATAPATH__COMPONENTS,
					EditorStorageFactory.getFactory().createSinglePortRam()));

//				(createChildParameter
//				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
//				 OperatorsFactory.getFactory().createDataFlowMux()));
//
//		newChildDescriptors.add
//			(createChildParameter
//				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
//				 OperatorsFactory.getFactory().createControlFlowMux()));

//		newChildDescriptors.add
//			(createChildParameter
//				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
//				 OperatorsFactory.getFactory().createReductionOperator()));
//
		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorOperatorsFactory.getFactory().createBitSelect()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorOperatorsFactory.getFactory().createCompare()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__COMPONENTS,
						EditorOperatorsFactory.getFactory().createMerge()));


		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__DATA_WIRES,
						EditorWiresFactory.getFactory().createDataFlowWire()));

		newChildDescriptors.add
			(createChildParameter
				(DatapathPackage.Literals.DATAPATH__CONTROL_WIRES,
						EditorWiresFactory.getFactory().createControlFlowWire()));
	}


	/**
	 * @generated NOT
	 * @return
	 */
	public Object getPads() {
		return children.get(0);
	}

	/**
	 * @generated NOT
	 * @return
	 */
	public Object getOperators() {
		return children.get(1);
	}

	/**
	 * @generated NOT
	 * @return
	 */
	public Object getWires() {
		return children.get(1);
	}
	


}
