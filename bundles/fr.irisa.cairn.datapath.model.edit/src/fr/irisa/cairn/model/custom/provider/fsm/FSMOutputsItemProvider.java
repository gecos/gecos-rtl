package fr.irisa.cairn.model.custom.provider.fsm;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;

import fr.irisa.cairn.model.custom.provider.TransientItemProvider;
import fr.irisa.cairn.model.datapath.DatapathPackage;
import fr.irisa.cairn.model.fsm.FSM;

public class FSMOutputsItemProvider extends TransientItemProvider {

	public FSMOutputsItemProvider(AdapterFactory adapterFactory, FSM scope) {
		super(adapterFactory, scope);
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatapathPackage.Literals.FLAG_BEARER_BLOCK__FLAGS);
		}
		return childrenFeatures;
	}

	@Override
	public String getText(Object object) {
		return "FSM Output";
	}

	@Override
	public Object getParent(Object object) {
		Object scope = super.getParent(object);
		CustomFSMItemProvider scopeItemProvider = (CustomFSMItemProvider) adapterFactory
				.adapt(scope, IEditingDomainItemProvider.class);
		return scopeItemProvider != null ? scopeItemProvider.getWires() : null;
	}

}
