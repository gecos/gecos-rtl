package fr.irisa.cairn.model.custom.provider.datapath;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;

import fr.irisa.cairn.model.custom.provider.TransientItemProvider;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;

public class DatapathPadsItemProvider extends TransientItemProvider{

	public DatapathPadsItemProvider(AdapterFactory adapterFactory,Datapath scope) {
		super(adapterFactory,scope);
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if(childrenFeatures==null){
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatapathPackage.Literals.ACTIVABLE_BLOCK__ACTIVATE);
			childrenFeatures.add(DatapathPackage.Literals.DATA_FLOW_BLOCK__IN);
			childrenFeatures.add(DatapathPackage.Literals.DATA_FLOW_BLOCK__OUT);
			childrenFeatures.add(DatapathPackage.Literals.FLAG_BEARER_BLOCK__FLAGS);
		}
		return childrenFeatures;
	}
	
	@Override
	public String getText(Object object) {
		return "IO Ports";
	}
	
	@Override
	public Object getParent(Object object) {
		Object scope = super.getParent(object);
		CustomDatapathItemProvider scopeItemProvider = (CustomDatapathItemProvider)adapterFactory.adapt(scope, IEditingDomainItemProvider.class);
		return scopeItemProvider !=null ? scopeItemProvider.getPads():null;
	}
	
}
