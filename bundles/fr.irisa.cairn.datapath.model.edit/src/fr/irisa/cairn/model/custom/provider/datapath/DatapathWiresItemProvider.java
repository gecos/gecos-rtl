package fr.irisa.cairn.model.custom.provider.datapath;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;

import fr.irisa.cairn.model.custom.provider.TransientItemProvider;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;

public class DatapathWiresItemProvider extends TransientItemProvider {

	public DatapathWiresItemProvider(AdapterFactory adapterFactory, Datapath scope) {
		super(adapterFactory, scope);
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatapathPackage.Literals.DATAPATH__DATA_WIRES);
			childrenFeatures.add(DatapathPackage.Literals.DATAPATH__CONTROL_WIRES);
		}
		return childrenFeatures;
	}

	@Override
	public String getText(Object object) {
		return "Wires";
	}

	@Override
	public Object getParent(Object object) {
		Object scope = super.getParent(object);
		CustomDatapathItemProvider scopeItemProvider = (CustomDatapathItemProvider) adapterFactory
				.adapt(scope, IEditingDomainItemProvider.class);
		return scopeItemProvider != null ? scopeItemProvider.getWires() : null;
	}

}
