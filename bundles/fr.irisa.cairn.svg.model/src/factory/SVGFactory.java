package factory;


import top.Circle;
import top.Color;
import top.Diagram;
import top.Line;
import top.Point;
import top.TopFactory;

public class SVGFactory {
	static TopFactory f = TopFactory.eINSTANCE;
	private static int Id =0;
	
	public static Diagram diagram(int witdth, int height) {
		Diagram res = f.createDiagram();
		res.setWidth(witdth);
		res.setHeight(height);
		return res;
	}

	public static Point point(int x, int y) {
		Point p = f.createPoint();
		p.setX(x);
		p.setY(y);
		p.setId("Point_"+(Id ++));
		return p;
	}

	public static Circle circle(int x,int y, int r) {
		Circle res= f.createCircle();
		res.setCenter(point(x, y));
		res.setR(r);
		res.setId("Circle_"+(Id++));
		return res;
	}

	public static Circle circle(int x,int y, int r, Color fillColor) {
		Circle res= f.createCircle();
		res.setCenter(point(x, y));
		res.setR(r);
		res.setId("Circle_"+(Id++));
		res.setFillColor(fillColor);
		return res;
	}

	public static Line line(int x0,int y0, int x1,int y1) {
		Line res= f.createLine();
		res.setStart(point(x0, y0));
		res.setEnd(point(x1, y1));
		res.setId("Line_"+(Id++));
		return res;
	}

}
