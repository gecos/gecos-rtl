package prettyprint;

import java.io.FileNotFoundException;
import java.io.PrintStream;

import top.Circle;
import top.Diagram;
import top.Element;
import top.Line;
import top.Point;
import top.Rectangle;
import top.Text;
import top.util.TopSwitch;

public class SVGPrettyPrinter extends TopSwitch<Object> {
	Diagram top;
	PrintStream ps;
	
	private String qt(String mess) {
		return  "\""+mess+"\"";
	}

	private String qt(int mess) {
		return  "\""+mess+"\"";
	}

	public SVGPrettyPrinter(Diagram top, String filename) throws FileNotFoundException {
		this.top=top;
		ps = new PrintStream(filename+".svg");
	}
	
	public void generate() {
		doSwitch(top);
		ps.close();
	}

	@Override
	public Object caseDiagram(Diagram object) {
		ps.print("<?xml version=\"1.0\" standalone=\"no\"?>\n");
		ps.print("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n"); 
		ps.print("\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
		ps.print("<svg width=\""+object.getWidth()+"\" height=\""+object.getHeight()+"\" version=\"1.1\"");
		ps.print(" xmlns=\"http://www.w3.org/2000/svg\">\n");
		for (Element element : object.getElements()) {
			doSwitch(element);
		}
		ps.println("</svg>");
		return ps;
		
	}

	@Override
	public String caseElement(Element object) {
		throw new UnsupportedOperationException("Unnsuported Element "+object);
	}

	@Override
	public String casePoint(Point object) {
		throw new UnsupportedOperationException("Unnsuported Element "+object);
	}

	@Override
	public Object caseCircle(Circle c) {
		ps.append("<circle cx="+qt(c.getCenter().getX())+" cy="+qt(c.getCenter().getY())+" r="+qt(c.getR())+" stroke="+qt("black")+ " stroke-width="+qt(2)+" fill="+qt(c.getFillColor().toString())+"/>");
		return ps;
	}

	@Override
	public Object caseLine(Line object) {
		int x0 = object.getStart().getX();
		int y0 = object.getStart().getY();
		int x1 = object.getEnd().getX();
		int y1 = object.getEnd().getY();
		ps.print("<line x1=\""+x0+"\" y1=\""+y0+"\" x2=\""+x1+"\" y2=\""+y1+"\"\n");
		ps.print(" stroke=\"");
		ps.print("black");
		ps.print("\" stroke-width=\"1\"/>\n");
		return ps;
	}

	@Override
	public Object caseRectangle(Rectangle object) {
		// TODO Auto-generated method stub
		// <rect width="300" height="100" style="fill:rgb(0,0,255);stroke-width:1; 	stroke:rgb(0,0,0)"/>
		int x0 = object.getStart().getX();
		int y0 = object.getStart().getY();
		int x1 = object.getEnd().getX();
		int y1 = object.getEnd().getY();
		ps.print("<rect  x=\""+x0+"\" y=\""+y0+"\" width=\""+(x1-x0)+"\" height=\""+(y1-y0)+"\"\n");
		ps.print(" stroke=\"");
		ps.print("black");
		ps.print("\" stroke-width=\"1\"/>\n");
		return ps;
	}

	@Override
	public Object caseText(Text object) {
		return super.caseText(object);
	}

}
