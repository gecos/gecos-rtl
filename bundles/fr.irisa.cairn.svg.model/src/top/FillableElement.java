/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package top;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fillable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link top.FillableElement#getFillColor <em>Fill Color</em>}</li>
 * </ul>
 * </p>
 *
 * @see top.TopPackage#getFillableElement()
 * @model abstract="true"
 * @generated
 */
public interface FillableElement extends Element {
	/**
	 * Returns the value of the '<em><b>Fill Color</b></em>' attribute.
	 * The literals are from the enumeration {@link top.Color}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fill Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fill Color</em>' attribute.
	 * @see top.Color
	 * @see #setFillColor(Color)
	 * @see top.TopPackage#getFillableElement_FillColor()
	 * @model
	 * @generated
	 */
	Color getFillColor();

	/**
	 * Sets the value of the '{@link top.FillableElement#getFillColor <em>Fill Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fill Color</em>' attribute.
	 * @see top.Color
	 * @see #getFillColor()
	 * @generated
	 */
	void setFillColor(Color value);

} // FillableElement
