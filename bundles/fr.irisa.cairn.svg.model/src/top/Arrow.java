/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package top;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Arrow</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see top.TopPackage#getArrow()
 * @model
 * @generated
 */
public interface Arrow extends Line {
} // Arrow
