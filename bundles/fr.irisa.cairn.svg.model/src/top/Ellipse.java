/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package top;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ellipse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link top.Ellipse#getCenter <em>Center</em>}</li>
 *   <li>{@link top.Ellipse#getRx <em>Rx</em>}</li>
 *   <li>{@link top.Ellipse#getRy <em>Ry</em>}</li>
 * </ul>
 * </p>
 *
 * @see top.TopPackage#getEllipse()
 * @model
 * @generated
 */
public interface Ellipse extends FillableElement {
	/**
	 * Returns the value of the '<em><b>Center</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Center</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Center</em>' containment reference.
	 * @see #setCenter(Point)
	 * @see top.TopPackage#getEllipse_Center()
	 * @model containment="true"
	 * @generated
	 */
	Point getCenter();

	/**
	 * Sets the value of the '{@link top.Ellipse#getCenter <em>Center</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Center</em>' containment reference.
	 * @see #getCenter()
	 * @generated
	 */
	void setCenter(Point value);

	/**
	 * Returns the value of the '<em><b>Rx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rx</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rx</em>' attribute.
	 * @see #setRx(int)
	 * @see top.TopPackage#getEllipse_Rx()
	 * @model
	 * @generated
	 */
	int getRx();

	/**
	 * Sets the value of the '{@link top.Ellipse#getRx <em>Rx</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rx</em>' attribute.
	 * @see #getRx()
	 * @generated
	 */
	void setRx(int value);

	/**
	 * Returns the value of the '<em><b>Ry</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ry</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ry</em>' attribute.
	 * @see #setRy(int)
	 * @see top.TopPackage#getEllipse_Ry()
	 * @model
	 * @generated
	 */
	int getRy();

	/**
	 * Sets the value of the '{@link top.Ellipse#getRy <em>Ry</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ry</em>' attribute.
	 * @see #getRy()
	 * @generated
	 */
	void setRy(int value);

} // Ellipse
