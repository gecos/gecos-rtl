package test;

import static factory.SVGFactory.circle;
import static factory.SVGFactory.diagram;
import static factory.SVGFactory.line;

import java.io.FileNotFoundException;

import prettyprint.SVGPrettyPrinter;
import top.Color;
import top.Diagram;

public class SVGModelTest {

	public static void main(String[] args) throws FileNotFoundException {
		Diagram res = diagram(500,500);
		for(int i=0;i<10;i++) {
			res.getElements().add(circle(50*i+10,10*i,5,Color.BLACK));
			res.getElements().add(line(100,40*i,40*i,5*i*i));
		}
		SVGPrettyPrinter pp = new SVGPrettyPrinter(res, "test");
		pp.generate();
	}
}
