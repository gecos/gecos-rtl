/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.sequencerDSL.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import fr.irisa.cairn.datapath.model.sequencerDSL.IfBlock;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLFactory;
import fr.irisa.cairn.datapath.model.sequencerDSL.SequencerDSLPackage;
import fr.irisa.cairn.model.fsm.FsmFactory;

/**
 * This is the item provider adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.IfBlock} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class IfBlockItemProvider
	extends BlockItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfBlockItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SequencerDSLPackage.Literals.IF_BLOCK__COND);
			childrenFeatures.add(SequencerDSLPackage.Literals.IF_BLOCK__THEN);
			childrenFeatures.add(SequencerDSLPackage.Literals.IF_BLOCK__ELSE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns IfBlock.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/IfBlock"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_IfBlock_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(IfBlock.class)) {
			case SequencerDSLPackage.IF_BLOCK__COND:
			case SequencerDSLPackage.IF_BLOCK__THEN:
			case SequencerDSLPackage.IF_BLOCK__ELSE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createAndExpression()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createOrExpression()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createBooleanConstant()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createNegateExpression()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createBooleanFlagTerm()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createIntegerFlagTerm()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__COND,
				 FsmFactory.eINSTANCE.createIndexedBooleanFlagTerm()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createSimpleBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createWhileBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createRepeatBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createIfBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__THEN,
				 SequencerDSLFactory.eINSTANCE.createSwitchBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createSimpleBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createWhileBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createRepeatBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createIfBlock()));

		newChildDescriptors.add
			(createChildParameter
				(SequencerDSLPackage.Literals.IF_BLOCK__ELSE,
				 SequencerDSLFactory.eINSTANCE.createSwitchBlock()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == SequencerDSLPackage.Literals.IF_BLOCK__THEN ||
			childFeature == SequencerDSLPackage.Literals.IF_BLOCK__ELSE;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
