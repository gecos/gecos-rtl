/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.sequencerDSL.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import fr.irisa.cairn.datapath.model.sequencerDSL.util.SequencerDSLAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SequencerDSLItemProviderAdapterFactory extends SequencerDSLAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequencerDSLItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.Sequencer} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SequencerItemProvider sequencerItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.Sequencer}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSequencerAdapter() {
		if (sequencerItemProvider == null) {
			sequencerItemProvider = new SequencerItemProvider(this);
		}

		return sequencerItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.Block} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockItemProvider blockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.Block}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBlockAdapter() {
		if (blockItemProvider == null) {
			blockItemProvider = new BlockItemProvider(this);
		}

		return blockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.SubSequencer} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubSequencerItemProvider subSequencerItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.SubSequencer}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSubSequencerAdapter() {
		if (subSequencerItemProvider == null) {
			subSequencerItemProvider = new SubSequencerItemProvider(this);
		}

		return subSequencerItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.SimpleBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleBlockItemProvider simpleBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.SimpleBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSimpleBlockAdapter() {
		if (simpleBlockItemProvider == null) {
			simpleBlockItemProvider = new SimpleBlockItemProvider(this);
		}

		return simpleBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.WhileBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WhileBlockItemProvider whileBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.WhileBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createWhileBlockAdapter() {
		if (whileBlockItemProvider == null) {
			whileBlockItemProvider = new WhileBlockItemProvider(this);
		}

		return whileBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.RepeatBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RepeatBlockItemProvider repeatBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.RepeatBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRepeatBlockAdapter() {
		if (repeatBlockItemProvider == null) {
			repeatBlockItemProvider = new RepeatBlockItemProvider(this);
		}

		return repeatBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.IfBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfBlockItemProvider ifBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.IfBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createIfBlockAdapter() {
		if (ifBlockItemProvider == null) {
			ifBlockItemProvider = new IfBlockItemProvider(this);
		}

		return ifBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.SwitchBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SwitchBlockItemProvider switchBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.SwitchBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSwitchBlockAdapter() {
		if (switchBlockItemProvider == null) {
			switchBlockItemProvider = new SwitchBlockItemProvider(this);
		}

		return switchBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.CaseBlock} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseBlockItemProvider caseBlockItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.CaseBlock}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCaseBlockAdapter() {
		if (caseBlockItemProvider == null) {
			caseBlockItemProvider = new CaseBlockItemProvider(this);
		}

		return caseBlockItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.OutputPortRule} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutputPortRuleItemProvider outputPortRuleItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.OutputPortRule}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createOutputPortRuleAdapter() {
		if (outputPortRuleItemProvider == null) {
			outputPortRuleItemProvider = new OutputPortRuleItemProvider(this);
		}

		return outputPortRuleItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.IntegerOutputDef} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerOutputDefItemProvider integerOutputDefItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.IntegerOutputDef}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createIntegerOutputDefAdapter() {
		if (integerOutputDefItemProvider == null) {
			integerOutputDefItemProvider = new IntegerOutputDefItemProvider(this);
		}

		return integerOutputDefItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.BooleanOutputDef} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanOutputDefItemProvider booleanOutputDefItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.BooleanOutputDef}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBooleanOutputDefAdapter() {
		if (booleanOutputDefItemProvider == null) {
			booleanOutputDefItemProvider = new BooleanOutputDefItemProvider(this);
		}

		return booleanOutputDefItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.AbstractState} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractStateItemProvider abstractStateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.AbstractState}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAbstractStateAdapter() {
		if (abstractStateItemProvider == null) {
			abstractStateItemProvider = new AbstractStateItemProvider(this);
		}

		return abstractStateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.SimpleState} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleStateItemProvider simpleStateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.SimpleState}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSimpleStateAdapter() {
		if (simpleStateItemProvider == null) {
			simpleStateItemProvider = new SimpleStateItemProvider(this);
		}

		return simpleStateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.CallState} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallStateItemProvider callStateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.CallState}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCallStateAdapter() {
		if (callStateItemProvider == null) {
			callStateItemProvider = new CallStateItemProvider(this);
		}

		return callStateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.NopState} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NopStateItemProvider nopStateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.NopState}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createNopStateAdapter() {
		if (nopStateItemProvider == null) {
			nopStateItemProvider = new NopStateItemProvider(this);
		}

		return nopStateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.SetState} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SetStateItemProvider setStateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.SetState}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSetStateAdapter() {
		if (setStateItemProvider == null) {
			setStateItemProvider = new SetStateItemProvider(this);
		}

		return setStateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.Jump} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JumpItemProvider jumpItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.Jump}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createJumpAdapter() {
		if (jumpItemProvider == null) {
			jumpItemProvider = new JumpItemProvider(this);
		}

		return jumpItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.ImmediateValue} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImmediateValueItemProvider immediateValueItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.ImmediateValue}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createImmediateValueAdapter() {
		if (immediateValueItemProvider == null) {
			immediateValueItemProvider = new ImmediateValueItemProvider(this);
		}

		return immediateValueItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.BooleanImmediate} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BooleanImmediateItemProvider booleanImmediateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.BooleanImmediate}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createBooleanImmediateAdapter() {
		if (booleanImmediateItemProvider == null) {
			booleanImmediateItemProvider = new BooleanImmediateItemProvider(this);
		}

		return booleanImmediateItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.irisa.cairn.model.datapath.sequencerDSL.IntegerImmediate} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntegerImmediateItemProvider integerImmediateItemProvider;

	/**
	 * This creates an adapter for a {@link fr.irisa.cairn.model.datapath.sequencerDSL.IntegerImmediate}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createIntegerImmediateAdapter() {
		if (integerImmediateItemProvider == null) {
			integerImmediateItemProvider = new IntegerImmediateItemProvider(this);
		}

		return integerImmediateItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (sequencerItemProvider != null) sequencerItemProvider.dispose();
		if (blockItemProvider != null) blockItemProvider.dispose();
		if (subSequencerItemProvider != null) subSequencerItemProvider.dispose();
		if (simpleBlockItemProvider != null) simpleBlockItemProvider.dispose();
		if (whileBlockItemProvider != null) whileBlockItemProvider.dispose();
		if (repeatBlockItemProvider != null) repeatBlockItemProvider.dispose();
		if (ifBlockItemProvider != null) ifBlockItemProvider.dispose();
		if (switchBlockItemProvider != null) switchBlockItemProvider.dispose();
		if (caseBlockItemProvider != null) caseBlockItemProvider.dispose();
		if (outputPortRuleItemProvider != null) outputPortRuleItemProvider.dispose();
		if (integerOutputDefItemProvider != null) integerOutputDefItemProvider.dispose();
		if (booleanOutputDefItemProvider != null) booleanOutputDefItemProvider.dispose();
		if (abstractStateItemProvider != null) abstractStateItemProvider.dispose();
		if (simpleStateItemProvider != null) simpleStateItemProvider.dispose();
		if (callStateItemProvider != null) callStateItemProvider.dispose();
		if (nopStateItemProvider != null) nopStateItemProvider.dispose();
		if (setStateItemProvider != null) setStateItemProvider.dispose();
		if (jumpItemProvider != null) jumpItemProvider.dispose();
		if (immediateValueItemProvider != null) immediateValueItemProvider.dispose();
		if (booleanImmediateItemProvider != null) booleanImmediateItemProvider.dispose();
		if (integerImmediateItemProvider != null) integerImmediateItemProvider.dispose();
	}

}
