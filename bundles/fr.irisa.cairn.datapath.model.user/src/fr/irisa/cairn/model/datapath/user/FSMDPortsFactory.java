package fr.irisa.cairn.model.datapath.user;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.PortFactory;


public class FSMDPortsFactory {

	private static int defaultPortBitwidth = 32;
	private static PortFactory factory =PortFactory.eINSTANCE; 
	
	
	public FSMDPortsFactory(int defaultwidth) {
		super();
		defaultPortBitwidth=defaultwidth;
	}
	

	public static InDataPort IDP(String string) {
		InDataPort ip =factory.createInDataPort();
		ip.setName(string);
		ip.setWidth(defaultPortBitwidth);
		return ip;
	}

	public static OutDataPort ODP(String string) {
		OutDataPort op =factory.createOutDataPort();
		op.setName(string);
		op.setWidth(defaultPortBitwidth);
		return op;
	}

	public static  InControlPort ICP(String name) {
		InControlPort cp =factory.createInControlPort();
		cp.setName(name);
		cp.setWidth(defaultPortBitwidth);
		return cp;
	}

	public static OutControlPort OCP(String name) {
		OutControlPort cp =factory.createOutControlPort();
		cp.setName(name);
		cp.setWidth(defaultPortBitwidth);
		return cp;
	}

	/**
	 * Creates a new input dataflow port with a given name and bitwidth
	 * @param Name of the port
	 * @param bitwidth of the input data port
	 * @return newly created InDataPort object 
	 */
	public static InDataPort IDP(String string, int bitwidth) {
		InDataPort ip =IDP(string);
		ip.setWidth(bitwidth);
		return ip;
	}

	/**
	 * Creates a new ouput dataflow port with a given name and bitwidth
	 * @param Name of the port
	 * @param bitwidth of the input data port
	 * @return newly created OutDataPort object 
	 */
	public static OutDataPort ODP(String string, int bitwidth) {
		OutDataPort op =ODP(string);
		op.setWidth(bitwidth);
		return op;
	}


	/**
	 * Creates a new input control flow port with a given name and bitwidth
	 * @param Name of the port
	 * @param bitwidth of the input data port
	 * @return newly created InControlPort object 
	 */
	public static  InControlPort ICP(String name, int width) {
		InControlPort cp =ICP(name);
		cp.setWidth(width);
		return cp;
	}

	/**
	 * Creates a new output control flow port with a given name and bitwidth
	 * @param Name of the port
	 * @param bitwidth of the input data port
	 * @return newly created OutControlPort object 
	 */
	public static OutControlPort OCP(String name, int width) {
		OutControlPort cp =  OCP(name);
		cp.setWidth(width);
		return cp;
	}

	/**
	 * Creates a new input dataflow port with a given name and bitwidth, and binds 
	 * it to the DataFlowBlock object passed as parameter
	 * @param DataFlowBlock object that is to containe the created port
	 * @param Name of the port
	 * @param bitwidth of the input data port
	 * @return newly created InDataPort object 
	 */
	public static InDataPort IDP(DataFlowBlock blk, String string, int bitwidth) {
		InDataPort ip =IDP(string,bitwidth);
		blk.getIn().add(ip);
		return ip;
	}

	public static OutDataPort ODP(DataFlowBlock blk, String string, int bitwidth) {
		OutDataPort op =ODP(string,bitwidth);
		blk.getOut().add(op);
		return op;
	}

	public static  InControlPort ICP(ActivableBlock blk,String name, int width) {
		InControlPort cp =ICP(name,width);
		blk.getActivate().add(cp);
		return cp;
	}

	public static OutControlPort OCP(FlagBearerBlock blk, String name, int width) {
		OutControlPort cp =  OCP(name,width);
		blk.getFlags().add(cp);
		return cp;
	}

	public static int getDefaultPortBitwidth() {
		return defaultPortBitwidth;
	}


	public static void setDefaultPortBitwidth(int defaultPortBitwidth) {
		FSMDPortsFactory.defaultPortBitwidth = defaultPortBitwidth;
	}


}
