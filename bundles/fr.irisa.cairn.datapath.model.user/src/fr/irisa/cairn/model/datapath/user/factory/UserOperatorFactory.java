package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.editor.factory.EditorOperatorsFactory;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class UserOperatorFactory {
	

	static int labelId = 0;
	
	private static String defaultLabel() {
		return "op"+(labelId++);
	}

	private static final int defaultOperatorBitwidth = 32;
	
	private static UserOperatorFactory instance =null; 

	private static Datapath dp;
	private static UserPortFactory portFactory;
	private static UserWireFactory wireFactory;	

	private UserOperatorFactory(Datapath dp) {
		instance.dp=dp;
		portFactory= UserPortFactory.getFactory();
		wireFactory= UserWireFactory.getInstance(dp);	
	}

	
	public static UserOperatorFactory init(Datapath dp) {
		if( instance==null) {
			instance = new UserOperatorFactory(dp);
		} else {
			if (dp==null) {
				instance.dp=dp;
			}
		}
		return instance;
	}

	private static EditorOperatorsFactory getFactory() {
		return EditorOperatorsFactory.getFactory();
	}

	public static UserOperatorFactory getFactory(Datapath dp) {
		if (instance==null) {
			instance = new UserOperatorFactory(dp);
		} else {
			if (instance.dp!=dp) {
				System.out.println("Changing "+instance.getClass().getSimpleName()+ "for new DP "+dp.getName());
				instance.dp=dp;
			}
		}
		return instance;
	}

	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.factory.IUserOperatorFactory#cMux(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.port.OutControlPort)
	 */
	public static ControlFlowMux cMux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, OutControlPort c) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		cmux.getOutput().setWidth(Math.max(A.getOutput().getWidth(),B.getOutput().getWidth()));
		getDp().getComponents().add(cmux);
		cmux.getInput(0).setWidth(A.getOutput().getWidth());
		cmux.getInput(1).setWidth(B.getOutput().getWidth());
		wireFactory.connect(cmux, A, B,c);
		return cmux;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.factory.IUserOperatorFactory#cMux(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.port.OutControlPort)
	 */
	public static ControlFlowMux cMux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleFlagBlock c) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		cmux.getOutput().setWidth(Math.max(A.getOutput().getWidth(),B.getOutput().getWidth()));
		getDp().getComponents().add(cmux);
		cmux.getInput(0).setWidth(A.getOutput().getWidth());
		cmux.getInput(1).setWidth(B.getOutput().getWidth());
		wireFactory.connect(cmux, A, B,c.getFlag(0));
		return cmux;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.factory.IUserOperatorFactory#cMux(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.port.OutControlPort)
	 */
	public static ControlFlowMux cMux(OutDataPort A, SingleOutputDataFlowBlock B, OutControlPort c) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		cmux.getOutput().setWidth(Math.max(A.getWidth(),B.getOutput().getWidth()));
		getDp().getComponents().add(cmux);
		cmux.getInput(0).setWidth(A.getWidth());
		cmux.getInput(1).setWidth(B.getOutput().getWidth());
		wireFactory.connect(cmux, A, B,c);
		return cmux;
	}


	public static ControlFlowMux cMux(OutControlPort ctrl, SingleOutputDataFlowBlock... A) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		//throw new UnsupportedOperationException("Not yet supported !");
		int width =0;
		int offset=0;
		wireFactory.connect(cmux.getControlPort(0),ctrl);
		for (SingleOutputDataFlowBlock a : A) {
			width = Math.max(width,a.getOutput().getWidth());
			wireFactory.connect( cmux.getIn().get(offset),a.getOutput());
			offset++;
			
		}
		cmux.getOutput().setWidth(width);
		getDp().getComponents().add(cmux);
		return cmux;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#dMux(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static DataFlowMux dMux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleOutputDataFlowBlock c) {
		DataFlowMux dmux = getFactory().createDataFlowMux();
		int widthA = A.getOutput().getWidth();
		int widthB = B.getOutput().getWidth();
		dmux.getInput(0).setWidth(widthA);
		dmux.getInput(1).setWidth(widthB);
		dmux.getOutput().setWidth(Math.max(widthA,widthB));
		getDp().getComponents().add(dmux);
		wireFactory.connect(dmux, A, B,c);
		return dmux;
	}



	
	

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBinaryOperator(java.lang.String, int, fr.irisa.cairn.model.datapath.operators.BinaryOpcode)
	 */
	public static BinaryOperator createBinaryOperator(String label, int bitwidth, BinaryOpcode opcode) {
		BinaryOperator op =getFactory().createBinaryOperator(label,bitwidth,opcode);
		
		getDp().getComponents().add(op);
		return op;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createCompare(java.lang.String, int, fr.irisa.cairn.model.datapath.operators.CompareOpcode)
	 */
	public static Compare createCompare(String label, int bitwidth, CompareOpcode opcode) {
		Compare op =getFactory().createCompare(label,bitwidth,opcode);
		getDp().getComponents().add(op);
		return op;
	}
	
	public static Compare createCompare(int bitwidth, CompareOpcode opcode) {
		return createCompare(defaultLabel(),bitwidth, opcode);
	}
	
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBinaryOperator(java.lang.String, fr.irisa.cairn.model.datapath.operators.BinaryOpcode)
	 */
	public static BinaryOperator createBinaryOperator(String label, BinaryOpcode opcode) {
		return createBinaryOperator(label, defaultOperatorBitwidth, opcode);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBinaryOperator(fr.irisa.cairn.model.datapath.operators.BinaryOpcode)
	 */
	public static BinaryOperator createBinaryOperator(BinaryOpcode opcode) {
		return createBinaryOperator(defaultLabel(),defaultOperatorBitwidth, opcode);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBinaryOperator(int, fr.irisa.cairn.model.datapath.operators.BinaryOpcode)
	 */
	public static BinaryOperator createBinaryOperator(int bitwidth, BinaryOpcode opcode) {
		return createBinaryOperator(defaultLabel(),bitwidth, opcode);
	}
	
	public static Ctrl2DataBuffer createCtrl2DataBuffer(int bitwidth) {
		return createCtrl2DataBuffer(defaultLabel(),bitwidth);
	}
	
	public static Data2CtrlBuffer createData2CtrlBuffer(int bitwidth) {
		return createData2CtrlBuffer(defaultLabel(),bitwidth);
	}

	
	public static Ctrl2DataBuffer createCtrl2DataBuffer(String defaultLabel, int bitwidth) {
		Ctrl2DataBuffer c2b = getFactory().createCtrl2DataBuffer(defaultLabel, bitwidth);
		getDp().getComponents().add(c2b);
		return c2b;
	}
	
	public static Data2CtrlBuffer createData2CtrlBuffer(String defaultLabel, int bitwidth) {
		Data2CtrlBuffer d2c = getFactory().createData2CtrlBuffer(defaultLabel, bitwidth);
		getDp().getComponents().add(d2c);
		return d2c;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBitSelect(java.lang.String, int, int, int)
	 */
	public static BitSelect createBitSelect(String string, int width,int lb, int ub) {
		BitSelect op =getFactory().createBitSelect(string, width, lb, ub);
		getDp().getComponents().add(op);
		return op;
	}

	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBitSelect(int, int)
	 */
	public static BitSelect createBitSelect(int lb, int ub) {
		return createBitSelect(defaultLabel(), defaultOperatorBitwidth,lb,ub );
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBitSelect(int, int, java.lang.String)
	 */
	public static BitSelect createBitSelect(int lb, int ub,String string) {
		return createBitSelect(string, defaultOperatorBitwidth,lb,ub);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBitSelect(int, int, int)
	 */
	public static BitSelect createBitSelect(int bitwidth,int lb, int ub) {
		return createBitSelect(defaultLabel(), bitwidth,lb,ub);
	}



	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createConstantValue(java.lang.String, int, int)
	 */
	public static ConstantValue createConstantValue(String name, int value, int bitwidth) {
		ConstantValue op =getFactory().createConstantValue(name,value, bitwidth);
		getDp().getComponents().add(op);
		return op;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createConstantValue(java.lang.String, int)
	 */
	public static ConstantValue createConstantValue(String name, int value) {
		ConstantValue createConstantValue = getFactory().createConstantValue(name, value, getFactory().bitwidthForValue(value));
		getDp().getComponents().add(createConstantValue);
		return createConstantValue;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#constant(int)
	 */
	public static ConstantValue constant(int value) {
		return createConstantValue(defaultLabel(), value, getFactory().bitwidthForValue(value));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#constant(int, int)
	 */
	public static ConstantValue constant(int value, int bw) {
		return createConstantValue(defaultLabel(), value, bw);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createTernaryOperator(java.lang.String, int, fr.irisa.cairn.model.datapath.operators.TernaryOpcode)
	 */
	public static TernaryOperator createTernaryOperator(String label, int bitwidth, TernaryOpcode opcode) {
		TernaryOperator op =createTernaryOperator(label, bitwidth,opcode);
		getDp().getComponents().add(op);
		return op;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createTernaryOperator(int, fr.irisa.cairn.model.datapath.operators.TernaryOpcode)
	 */
	public static TernaryOperator createTernaryOperator(int bitwidth, TernaryOpcode opcode) {
			return createTernaryOperator(defaultLabel(),bitwidth,opcode);
	}	

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createTernaryOperator(fr.irisa.cairn.model.datapath.operators.TernaryOpcode)
	 */
	public static TernaryOperator createTernaryOperator(TernaryOpcode opcode) {
		return createTernaryOperator(defaultLabel(),defaultOperatorBitwidth,opcode);
	}	

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator dmux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return add(A.getOutput(), B.getOutput());
	}
	

	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator add(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return add(A.getOutput(), B.getOutput());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator add(OutDataPort A, SingleOutputDataFlowBlock B) {
		return add(A, B.getOutput());
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator add(SingleOutputDataFlowBlock A, OutDataPort B) {
		return add(A.getOutput(), B);
	}
	
	public static BinaryOperator add(OutDataPort A, OutDataPort B) {
		int widthA = A.getWidth();
		int Bwidth = B.getWidth();
		int bitwidth = Math.max(widthA,Bwidth);
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.ADD);
		newop.getInput(0).setWidth(widthA);
		newop.getInput(1).setWidth(Bwidth);
		wireFactory.connect(newop.getInput(0), A);
		wireFactory.connect(newop.getInput(1), B);
		return newop;
	}
	
	public static Ctrl2DataBuffer c2d(OutControlPort A, InDataPort B) {
		int widthA = A.getWidth();
		int widthB = B.getWidth();
		int bitwidth = Math.max(widthA,widthB);
		Ctrl2DataBuffer c2d = createCtrl2DataBuffer(bitwidth);
		c2d.getActivate().get(0).setWidth(widthA);
		wireFactory.connect(c2d.getActivate().get(0), A);
		return c2d;
		
	}
	
	public static Data2CtrlBuffer d2c(OutDataPort A, InControlPort B) {
		int widthA = A.getWidth();
		int widthB = B.getWidth();
		int bitwidth = Math.max(widthA,widthB);
		Data2CtrlBuffer d2c = createData2CtrlBuffer(bitwidth);
		d2c.getInput(0).setWidth(widthA);
		wireFactory.connect(d2c.getInput(0), A);
		return d2c;
		
	}

	public static Data2CtrlBuffer d2c(SingleOutputDataFlowBlock A) {
		int widthA = A.getOutput().getWidth();
		Data2CtrlBuffer d2c = createData2CtrlBuffer(widthA);
		d2c.getInput(0).setWidth(widthA);
		wireFactory.connect(d2c.getInput(0), A);
		return d2c;
	}

	public static SingleOutputDataFlowBlock c2d(SingleFlagBlock A) {
		int widthA = A.getFlag().getWidth();
		Ctrl2DataBuffer d2c = createCtrl2DataBuffer(widthA);
		d2c.getInput(0).setWidth(widthA);
		wireFactory.connect(d2c.getActivate().get(0), A.getFlag(0));
		return d2c;
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator and(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.AND);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator or(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.OR);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#eq(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static Compare eq(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.EQU);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	public static Compare lt(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.LT);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	public static Compare lte(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.LTE);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	public static Compare gt(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.GT);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	public static Compare gte(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.GTE);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	public static Compare neq(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		Compare newop = createCompare( bitwidth, CompareOpcode.NEQ);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator xor(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.XOR);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
		
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator shl(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.SHL);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator cmp(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.CMP);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator shr(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.SHR);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator add(SingleOutputDataFlowBlock A, int value) {
		return add(A,constant(value));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator shl(SingleOutputDataFlowBlock A, int value) {
		return shl(A,constant(value));
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator shr(SingleOutputDataFlowBlock A, int value) {
		return shr(A,constant(value));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sub(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator sub(SingleOutputDataFlowBlock A, int value) {
		return sub(A,constant(value));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#mulu(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator mulu(SingleOutputDataFlowBlock A, int value) {
		return mulu(A,constant(value));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#muls(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator muls(SingleOutputDataFlowBlock A, int value) {
		return muls(A,constant(value));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#max(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BinaryOperator max(SingleOutputDataFlowBlock A, int value) {
		return max(A,constant(value));
	}



	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sub(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator sub(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOut().get(0).getWidth(),B.getOut().get(0).getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.SUB);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sub(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static UnaryOperator not(SingleOutputDataFlowBlock A) {
		int bitwidth = A.getOut().get(0).getWidth();
		UnaryOperator newop = createUnaryOperator(bitwidth, UnaryOpcode.NOT);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		getDp().getComponents().add(newop);
		return wireFactory.connect(newop, A);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sub(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static SingleFlagBlock not(SingleFlagBlock A) {
		return d2c(not(c2d(A)));
	}

	public static UnaryOperator createUnaryOperator(int bitwidth, UnaryOpcode opcode) {
		
		return getFactory().createUnaryOperator(defaultLabel(),bitwidth, opcode);
	}


	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#mulu(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator mulu(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = A.getOutput().getWidth()+B.getOut().get(0).getWidth();
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.MUL);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#muls(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator muls(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = A.getOutput().getWidth()+B.getOutput().getWidth()-1;
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.MUL);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#max(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator max(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.max(A.getOutput().getWidth(),B.getOutput().getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.MAX);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#min(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator min(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		int bitwidth = Math.min(A.getOutput().getWidth(),B.getOutput().getWidth());
		BinaryOperator newop = createBinaryOperator( bitwidth, BinaryOpcode.MIN);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		newop.getInput(1).setWidth(B.getOutput().getWidth());
		return wireFactory.connect(newop, A, B);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#bitSelect(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int, int)
	 */
	public static BitSelect bitSelect(SingleOutputDataFlowBlock A, int lb,int ub) {
		assert((ub>=lb) && (ub<A.getOutput().getWidth()));
		BitSelect newop = createBitSelect( A.getOutput().getWidth(),lb,ub);
		newop.getInput(0).setWidth(A.getOutput().getWidth());
		return wireFactory.connect(newop, A);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createReductionOperator(fr.irisa.cairn.model.datapath.operators.ReductionOpcode, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock[])
	 */
	public static ReductionOperator createReductionOperator(ReductionOpcode opcode, SingleOutputDataFlowBlock A[]) {
		ReductionOperator op =getFactory().createReductionOperator(opcode);
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			portFactory.addInDataPort(op, "I0",block.getOutput().getWidth());
			wireFactory.connectAt(op,i, block);
			i++;
		}
		return op;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#max(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static ReductionOperator max(SingleOutputDataFlowBlock... A) {
		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.MAX);
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
			wireFactory.connectAt(op,i, block);
			i++;
		}
		return op;
	}

//	public static ReductionOperator max(SingleOutputDataFlowBlock... A) {
//		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.MAX);
//		int i=0;
//		for (SingleOutputDataFlowBlock block : A) {
//			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
//			wireFactory.connectAt(op,i, block);
//			i++;
//		}
//		return op;
//	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#min(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static ReductionOperator min(SingleOutputDataFlowBlock... A) {
		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.MIN);
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
			wireFactory.connectAt(op,i, block);
			i++;
		}
		return op;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sum(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static ReductionOperator sum(SingleOutputDataFlowBlock... A) {
		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.SIGMA);
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
			wireFactory.connectAt(op,i, block);
			i++;
		}
		return op;
	}

//	/* (non-Javadoc)
//	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#sum(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
//	 */
//	public static ReductionOperator mux(SingleOutputDataFlowBlock... A) {
//		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.SIGMA);
//		int i=0;
//		for (SingleOutputDataFlowBlock block : A) {
//			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
//			wireFactory.connectAt(op,i, block);
//			i++;
//		}
//		return op;
//	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#prod(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static ReductionOperator prod(SingleOutputDataFlowBlock... A) {
		ReductionOperator op =getFactory().createReductionOperator(ReductionOpcode.PROD);
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			portFactory.addInDataPort(op, "I"+i,block.getOutput().getWidth());
			wireFactory.connectAt(op,i, block);
			i++;
		}
		return op;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#selectBit(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BitSelect selectBit(SingleOutputDataFlowBlock A, int pos) {
		return bitSelect(A,pos,pos);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#selectBitsZeroToN(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BitSelect selectBitsZeroToN(SingleOutputDataFlowBlock A, int pos) {
		return bitSelect(A,pos,A.getOutput().getWidth()-1);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#selectBitsNtoLast(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BitSelect selectBitsNtoLast(SingleOutputDataFlowBlock A, int pos) {
		return bitSelect(A,0,pos);
	}


	public static Datapath getDp() {
		if (instance==null) {
			throw new RuntimeException("Error in UserOperationFactory");
		}
		return instance.dp;
	}


	private static UserPortFactory getPortFactory() {
		return portFactory;
	}


	private static UserWireFactory getWireFactory() {
		return wireFactory;
	}


	public ControlFlowMux cMux(OutDataPort A, OutDataPort B,
			OutControlPort c) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		cmux.getOutput().setWidth(Math.max(A.getWidth(),B.getWidth()));
		getDp().getComponents().add(cmux);
		cmux.getInput(0).setWidth(A.getWidth());
		cmux.getInput(1).setWidth(B.getWidth());
		wireFactory.connect(cmux, A, B,c);
		return cmux;
	}


	public ControlFlowMux cMux(SingleOutputDataFlowBlock A, OutDataPort B,
			OutControlPort c) {
		ControlFlowMux cmux =getFactory().createControlFlowMux();
		cmux.getOutput().setWidth(Math.max(A.getOutput().getWidth(),B.getWidth()));
		getDp().getComponents().add(cmux);
		cmux.getInput(0).setWidth(A.getOutput().getWidth());
		cmux.getInput(1).setWidth(B.getWidth());
		wireFactory.connect(cmux, A, B,c);
		return cmux;
	}


}
