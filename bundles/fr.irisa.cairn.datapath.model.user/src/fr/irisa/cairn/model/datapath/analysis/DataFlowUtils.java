package fr.irisa.cairn.model.datapath.analysis;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class DataFlowUtils {
	
	public static List<DataFlowBlock> getPrecedingNodes(DataFlowBlock current) {
		List<DataFlowBlock> res = new ArrayList<DataFlowBlock> (); 
		for (InDataPort ip : current.getIn())  {
			DataFlowWire wire = ip.getWire();
			if(wire!=null) {
				if(wire.getSource()!=null) {
					DataFlowBlock parentNode = wire.getSource().getParentNode();
					if (parentNode!=null) {
						res.add(parentNode);
					}
				}
			}
		}
		return res;
	}

	public static DataFlowBlock getPrecedingNode(InDataPort ip) {
		DataFlowWire wire = ip.getWire();
		if(wire!=null) {
			if(wire.getSource()!=null) {
				DataFlowBlock parentNode = wire.getSource().getParentNode();
				if (parentNode!=null) {
					return parentNode;
				}
			}
		}
		return null;
	}

	public static List<DataFlowBlock> getSuccessorNodes(OutDataPort op) {
		List<DataFlowBlock> res = new ArrayList<DataFlowBlock> (); 
		List<DataFlowWire> wires = op.getWires();
		for (DataFlowWire wire : wires) {
			if(wire!=null) {
				if(wire.getSink()!=null) {
					DataFlowBlock parentNode = wire.getSource().getParentNode();
					if (parentNode!=null) {
						res.add(parentNode);
					}
				}
			}
		}
		return res;
	}

	public static List<DataFlowBlock> getSuccessorNodes(DataFlowBlock current) {
		List<DataFlowBlock> res = new ArrayList<DataFlowBlock> (); 
		for (OutDataPort op: current.getOut()) {
			List<DataFlowWire> wires = op.getWires();
			for (DataFlowWire wire : wires) {
				if(wire!=null) {
					if(wire.getSink()!=null) {
						DataFlowBlock parentNode = wire.getSource().getParentNode();
						if (parentNode!=null) {
							res.add(parentNode);
						}
					}
				}
			}
		}
		return res;
	}

}
