package fr.irisa.cairn.model.datapath.xml;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;

public class DatapathXMLWriter {

	private static DatapathXMLWriter singleton = null;
	
	public static DatapathXMLWriter getXMLWriter() {
		if (singleton==null) {
			singleton = new DatapathXMLWriter();
		}
		return singleton;
	}
	
		
	public void save(Datapath datapath ,String modelfilename) throws IOException {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());

		resourceSet.getPackageRegistry().put
			(DatapathPackage.eNS_URI, 
			 DatapathPackage.eINSTANCE);

		Resource resource = resourceSet.createResource(URI.createFileURI(modelfilename));
		resource.getContents().add(datapath);
		resource.save(null);
	}
}
