package fr.irisa.cairn.model.datapath.analysis;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class DanglingPortDatapathAnalyzer extends OperatorsSwitch {

	private boolean stopOnDanglingOutput=false;
	

	public DanglingPortDatapathAnalyzer(boolean stopOnDangling) {
		this.stopOnDanglingOutput=stopOnDangling;
	}
	
	private void danglingInputPortHandler(Port op) {
		throw new RuntimeException(op.eContainer()+":"+op+" has a dangling input port:"+op);
	}

	private void danglingOutputPortHandler(Port op) {
		if(stopOnDanglingOutput) {
			throw new RuntimeException(op.eContainer()+" has a dangling output :"+op);
		} else {
			System.err.println(op.eContainer()+":"+op+" has a dangling output :"+op);
		}
		
	}

	public boolean analyze(Datapath dp) {
		for(AbstractBlock ab  :dp.getComponents()) {
			doSwitch(ab);
		}
		for(DataFlowWire  wire  :dp.getDataWires()) {
			if(wire.getSink()==null) danglingWireHandler(wire);
			if(wire.getSource()==null) danglingWireHandler(wire);
		}
		for(ControlFlowWire  wire  :dp.getControlWires()) {
			if(wire.getSink()==null) danglingWireHandler(wire);
			if(wire.getSource()==null) danglingWireHandler(wire);
		}
		return true;
	}

	private void danglingWireHandler(Wire wire) {
		String mess = "Wire "+wire+" has a dangling port";
		if(stopOnDanglingOutput) {
			throw new RuntimeException(mess);
		} else {
			System.err.println(mess);
		}
	}

	@Override
	public Object caseDataFlowBlock(DataFlowBlock object) {
		for(OutDataPort op : object.getOut()) {
			if(op.getWires().size()==0) danglingOutputPortHandler(op);
		}
		for(InDataPort ip : object.getIn()) {
			if(ip.getWire()==null) danglingInputPortHandler(ip);
		}
		return this;
	}


	@Override
	public Object caseActivableBlock(ActivableBlock object) {
		for(InControlPort ip : object.getActivate()) {
			if(ip.getWire()==null) danglingInputPortHandler(ip);
		}
		return this;
	}

	@Override
	public Object caseFlagBearerBlock(FlagBearerBlock object) {
		for(OutControlPort op : object.getFlags()) {
			if(op.getWires().size()==0) danglingOutputPortHandler(op);
		}
		return this;
	}

}
