package fr.irisa.cairn.model.datapath.user;

import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.StorageFactory;


public class FSMDRegisterFactory  {

	static StorageFactory factory = StorageFactory.eINSTANCE;
	
	static int labelId=0;

	private static String defaultLabel() {
		return "reg_"+(labelId++);
	}

	public static Register reg(String label, int bitwidth) {
		Register op = factory.createRegister();
		op.setName(label);
		FSMDPortsFactory.IDP(op, "D",bitwidth);
		FSMDPortsFactory.ODP(op, "Q",bitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		return op;
	}

	public static CERegister cereg(String label, int bitwidth) {
		CERegister op = factory.createCERegister();
		op.setName(label);
		FSMDPortsFactory.ICP(op, "CE",1);
		FSMDPortsFactory.IDP(op, "D",bitwidth);
		FSMDPortsFactory.ODP(op, "Q",bitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		return op;
	}

	public static ShiftRegister shiftreg(String label, int bitwidth, int depth) {
		ShiftRegister op = factory.createShiftRegister();
		op.setName(label);
		op.setDepth(depth);
		FSMDPortsFactory.ICP(op, "CE",1);
		FSMDPortsFactory.IDP(op, "D",bitwidth);
		FSMDPortsFactory.ODP(op, "Q",bitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		return op;
	}

	public static Register reg(String label, SingleOutputDataFlowBlock D) {
		Register reg = reg(label,D.getOutput().getWidth());
		reg.connect(D);
		return reg;
	}
	
	public static Register reg(SingleOutputDataFlowBlock D) {
		return reg(defaultLabel(),D);
	}

	public static Register reg(String label, OutDataPort D) {
		Register reg = reg(label,D.getWidth());
		reg.connect(D);
		return reg;
	}


	public static Register reg(OutDataPort D) {
		return reg(defaultLabel(),D.getWidth());
	}


	public static CERegister cereg(String label, OutDataPort odp, OutControlPort ocp) {
		CERegister cereg = cereg(label,odp.getWidth());
		odp.connect(cereg);
		ocp.connect(cereg);
		return cereg;
	}

	public static CERegister cereg(String label, SingleOutputDataFlowBlock odp, OutControlPort ocp) {
		return cereg(label,odp.getOutput(),ocp);
	}


	public static CERegister cereg(String label, OutControlPort ocp) {
		CERegister cereg = cereg(label,-1);
		ocp.connect(cereg);
		return cereg;
	}


	public static CERegister cereg(String label, int width,OutControlPort ocp) {
		CERegister cereg = cereg(label,width);
		ocp.connect(cereg);
		return cereg;
	}

	public static CERegister cereg(String label, int width,SingleFlagBlock ocp) {
		return cereg(label,width,ocp.getFlag());
	}

	public static CERegister cereg(SingleFlagBlock CE, int bitwidth) {
		CERegister cereg = cereg(defaultLabel(),bitwidth);
		CE.connect(cereg);
		return cereg;
	}

	@Deprecated
	public static CERegister cereg(OutControlPort CE) {
		return cereg(CE,-1);
	}

	@Deprecated
	public static CERegister cereg(SingleFlagBlock CE) {
		return cereg(CE,-1);
	}

	public static CERegister[] ceregVector(String name, int bitwidth, int size, OutControlPort CE) {
		CERegister[] res = new CERegister[size];
		for(int i=0;i<size;i++) {
			res[i]=cereg(name+"_"+i,bitwidth,CE);
		}
		return res;
	}

	public static CERegister[] ceregVector(String name, int bitwidth, int size, SingleFlagBlock CE) {
		return ceregVector(name, bitwidth, size, CE.getFlag());
	}
	public static CERegister cereg(OutControlPort CE, int bitwidth) {
		CERegister cereg = cereg(defaultLabel(),bitwidth);
		CE.connect(cereg);
		return cereg;
	}


	public static CERegister cereg(String label, SingleOutputDataFlowBlock odp, SingleFlagBlock ocp) {
		return cereg(label,odp.getOutput(),ocp.getFlag());
	}

	public static CERegister cereg(OutDataPort odp, OutControlPort ocp) {
		return cereg(defaultLabel(),odp,ocp);
	}

	public static CERegister cereg(SingleOutputDataFlowBlock odp, SingleFlagBlock CEBlock) {
		return cereg(defaultLabel(),odp,CEBlock);
	}

	public static CERegister cereg(int bitwidth) {
		return cereg(defaultLabel(),bitwidth);
	}


	public static Register reg(int bitwidth) {
		return reg(defaultLabel(),bitwidth);
	}


	public static ShiftRegister shiftreg(String label, OutDataPort odp, OutControlPort icp,int depth) {
		ShiftRegister op = shiftreg(label, odp.getWidth(), depth);
		op.connect(odp);
		op.connect(icp);
		return op;
	}

	public static ShiftRegister shiftreg(String label,  OutControlPort icp,int depth) {
		ShiftRegister op = shiftreg(label, -1, depth);
		op.connect(icp);
		return op;
	}

	public static ShiftRegister shiftreg(OutDataPort odp, OutControlPort icp,int depth) {
		return shiftreg(defaultLabel(), odp,icp,depth);
	}

	public static ShiftRegister shiftreg(String label, SingleOutputDataFlowBlock odp, SingleFlagBlock cdp,int depth) {
		return shiftreg(label, odp.getOutput(), cdp.getFlag(),depth);
	}

	public static ShiftRegister shiftreg(SingleOutputDataFlowBlock odp, SingleFlagBlock cdp,int depth) {
		return shiftreg(defaultLabel(), odp.getOutput(),  cdp.getFlag(),depth);
	}
}
