package fr.irisa.cairn.model.datapath.xml;

import java.io.IOException;

import fr.irisa.cairn.model.datapath.Datapath;

public class SaveDatapathModel {

	Datapath datapath;
	String filename;
	
	public SaveDatapathModel(Datapath datapath, String filename) {
		this.datapath=datapath;
		this.filename=filename;
	}

	public void compute() throws IOException {
		DatapathXMLWriter writer = new DatapathXMLWriter();
		writer.save(datapath, filename);
	}
}
