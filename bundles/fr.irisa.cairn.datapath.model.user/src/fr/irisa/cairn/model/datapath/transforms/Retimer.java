package fr.irisa.cairn.model.datapath.transforms;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.analysis.DataFlowUtils;
import fr.irisa.cairn.model.datapath.operators.CombinationnalOperator;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.util.StorageSwitch;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class Retimer extends StorageSwitch<Object>{
	
	private InControlPort clockEnable;
	List<Register> outputRegisters;
	
	enum RetimingMode {Unset,SimpleRegister,CERegister,Impossible};
	
	RetimingMode  mode;
	
	public Retimer() {
		outputRegisters = new ArrayList<Register>();
	}

	public void backardRetime(CombinationnalOperator op) {
		
		resetLegalityCheck();
		for(DataFlowBlock nextBlk : DataFlowUtils.getSuccessorNodes(op)) {
			doSwitch(nextBlk);
		}
		switch (mode) {
			case SimpleRegister:
						
				break;
			case CERegister:
				for(Register reg : outputRegisters) {
					CERegister cereg = (CERegister) reg;
					for(DataFlowWire wires : cereg.getOutput().getWires()) {
						op.connect(wires.getSink());
					}
					cereg.getParent().getComponents().remove(cereg);
				}
				
				break;

			default:
			break;
		}
	}

	@Override
	public Object doSwitch(EObject theEObject) {
		return false;
	}

	@Override
	public Object caseRegister(Register object) {
		switch (mode) {
		case Unset:
			outputRegisters.add(object);
			mode=RetimingMode.SimpleRegister;
			break;
		case CERegister:
			mode=RetimingMode.Impossible;
			break;

		default:
			break;
		}
		return null;
	}

	@Override
	public Object caseCERegister(CERegister object) {
		switch (mode) {
		case Unset:
			outputRegisters.add(object);
			mode=RetimingMode.CERegister;
			clockEnable=object.getLoadEnable();
			break;
		case CERegister:
			if(object.getLoadEnable()!=clockEnable) {
				mode=RetimingMode.Impossible;
			} 
			break;

		case SimpleRegister:
			mode=RetimingMode.Impossible;
			break;

		default:
			break;
		}
		return null;
	}

	private void resetLegalityCheck() {
		outputRegisters.clear();
		mode=RetimingMode.Unset;
		clockEnable=null;
	}
	
	
}
