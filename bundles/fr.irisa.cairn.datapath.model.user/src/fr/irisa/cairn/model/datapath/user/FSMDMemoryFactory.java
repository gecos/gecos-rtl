package fr.irisa.cairn.model.datapath.user;

import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.ICP;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.IDP;
import static fr.irisa.cairn.model.datapath.user.FSMDPortsFactory.ODP;

import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.StorageFactory;
import fr.irisa.cairn.model.datapath.storage.SyncReadSPRAM;

public class FSMDMemoryFactory  {

	static StorageFactory factory = StorageFactory.eINSTANCE;
	
	static int labelId=0;


	public static int bitwidthForValue(int d) {
		if( d==1 )
			return 1;
		return (int)(Math.ceil(Math.log(d)/Math.log(2.0)));
	}

	public static DualPortRam dpram(String label, 
									int bitwidth, 
									int depth, 
									SingleOutputDataFlowBlock add0,
									SingleOutputDataFlowBlock din0,
									SingleInputDataFlowBlock  dout0,
									SingleOutputDataFlowBlock add1,
									SingleOutputDataFlowBlock din1,
									SingleInputDataFlowBlock  dout1,
									SingleFlagBlock re0,
									SingleFlagBlock re1,
									SingleFlagBlock we0,
									SingleFlagBlock we1
								   ) 
	{
		DualPortRam dpram = dpram(label, bitwidth, depth);
		dpram.getAddressPort(0).connect(add0);
		dpram.getAddressPort(1).connect(add1);
		dpram.getReadPort(0).connect(dout0);
		dpram.getReadPort(1).connect(dout1);
		dpram.getWritePort(0).connect(din0);
		dpram.getWritePort(1).connect(din1);
		dpram.getReadEnable(0).connect(re0);
		dpram.getReadEnable(1).connect(re1);
		dpram.getWriteEnable(0).connect(we0);
		dpram.getWriteEnable(1).connect(we1);
		return dpram;
	}

	@Deprecated
	public static DualPortRam dpram(String label, int bitwidth, int depth) {
		throw new UnsupportedOperationException("Not yet implemented");
//		DualPortRam mem = StorageFactory.eINSTANCE.createDualPortRam();
//		FSMDFactory.getCurrentDatapath().getComponents().add(mem);
//		mem.setName(label);
//		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
//		ICP(mem,"address_0",bitwidthForValue(depth));
//		IDP(mem,"dataIn_0",bitwidth);
//		IDP(mem,"address_1",bitwidthForValue(depth));
//		IDP(mem,"dataIn_1",bitwidth);
//		ICP(mem,"re_0",1);
//		ICP(mem,"we_0",1);
//		ICP(mem,"re_1",1);
//		ICP(mem,"we_1",1);
//		ODP(mem,"dataOut_0",bitwidth);
//		ODP(mem,"dataOut_1",bitwidth);
//		return mem;
	}

	public static SyncReadSPRAM syncReadSPRam(String label, int bitwidth, int depth) {
		SyncReadSPRAM mem = StorageFactory.eINSTANCE.createSyncReadSPRAM();
		mem.setName(label);
		IDP(mem,"address",bitwidthForValue(depth));
		IDP(mem,"dataIn",bitwidth);
		ICP(mem,"re",1);
		ICP(mem,"we",1);
		ODP(mem,"dataOut",bitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(mem);
		return mem;
	}
	public static SyncReadSPRAM[] syncReadSPRamVector(String name, int bitwidth, int depth, int size) {
		SyncReadSPRAM[] res = new SyncReadSPRAM[size];
		for(int i=0;i<size;i++) {
			res[i]=syncReadSPRam(name+"_"+i,bitwidth,depth);
		}
		return res;
	}
	

	public static SyncReadSPRAM syncReadSPRam(String label, int bitwidth, int depth, OutDataPort addr, OutDataPort din, OutControlPort re, OutControlPort we, InDataPort dout) {
		SyncReadSPRAM mem = syncReadSPRam(label, bitwidth, depth);
		mem.getAddressPort().connect(addr);
		mem.getReadPort().connect(dout);
		mem.getWritePort().connect(din);
		mem.getWriteEnable().connect(we);
		mem.getReadEnable().connect(re);
		return mem;
	}

	public static SyncReadSPRAM syncReadSPRam(String label, int bitwidth, int depth, OutDataPort addr, OutDataPort din, OutControlPort re, OutControlPort we) {
		SyncReadSPRAM mem = syncReadSPRam(label, bitwidth, depth);
		mem.getAddressPort().connect(addr);
		mem.getWritePort().connect(din);
		mem.getWriteEnable().connect(we);
		mem.getReadEnable().connect(re);
		return mem;
	}

	public static SyncReadSPRAM syncReadSPRam(String label, int bitwidth, int depth, SingleOutputDataFlowBlock addr, SingleOutputDataFlowBlock din, SingleFlagBlock re, SingleFlagBlock we, SingleInputDataFlowBlock dout) {
		return syncReadSPRam(label, bitwidth, depth, addr.getOutput(), din.getOutput(), re.getFlag(), we.getFlag(), dout.getInput());
	}

	public static SyncReadSPRAM syncReadSPRam(String label, int bitwidth, int depth, SingleOutputDataFlowBlock addr, SingleOutputDataFlowBlock din, SingleFlagBlock re, SingleFlagBlock we) {
		return syncReadSPRam(label, bitwidth, depth, addr.getOutput(), din.getOutput(), re.getFlag(), we.getFlag());
	}

	public static SinglePortRom syncRom(String label, 
										int bitwidth, 
										int depth, 
										OutDataPort addr, 
										OutControlPort re, 
										InDataPort dout
									   ) 
	{
		SinglePortRom mem = syncRom(label,bitwidth,depth);
		addr.connect(IDP(mem,"address",bitwidthForValue(depth)));
		re.connect(ICP(mem,"re",1));
		dout.connect(ODP(mem,"dataOut",bitwidth));
		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
		FSMDFactory.getCurrentDatapath().getComponents().add(mem);
		return mem;
	}

	public static SinglePortRom syncRom(String label, int bitwidth, int depth) {
		SinglePortRom mem = StorageFactory.eINSTANCE.createSinglePortRom();
		mem.setName(label);
		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
		FSMDFactory.getCurrentDatapath().getComponents().add(mem);
		return mem;
	}

	public static SinglePortRom rom(String label, int bitwidth, int depth) {
		SinglePortRom mem = StorageFactory.eINSTANCE.createSinglePortRom();
		mem.setName(label);
		FSMDFactory.getCurrentDatapath().getComponents().add(mem);
		return mem;
	}


}
