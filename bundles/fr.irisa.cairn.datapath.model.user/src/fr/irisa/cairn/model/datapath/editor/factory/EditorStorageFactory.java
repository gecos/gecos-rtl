package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.impl.StorageFactoryImpl;


public class EditorStorageFactory extends StorageFactoryImpl {


	public static int bitwidthForValue(int d) {
		if(d==1)
			return 1;
		return (int)(Math.ceil(Math.log(d)/Math.log(2.0)));
	}

//	public DualPortRam createDualPortRam(String label, int bitwidth, int depth) {
//		DualPortRam mem = super.createDualPortRam();
//		mem.setName(label);
//		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
//		portFactory.addInDataPort(mem,"address_0",bitwidthForValue(depth));
//		portFactory.addInDataPort(mem,"dataIn_0",bitwidth);
//		portFactory.addInDataPort(mem,"address_1",bitwidthForValue(depth));
//		portFactory.addInDataPort(mem,"dataIn_1",bitwidth);
//		portFactory.addInControlPort(mem,"re_0",1);
//		portFactory.addInControlPort(mem,"we_0",1);
//		portFactory.addInControlPort(mem,"re_1",1);
//		portFactory.addInControlPort(mem,"we_1",1);
//		portFactory.addOutDataPort(mem,"dataOut_0",bitwidth);
//		portFactory.addOutDataPort(mem,"dataOut_1",bitwidth);
//		return mem;
//	}

	@Deprecated
	public SinglePortRam createSinglePortRam(String label, int bitwidth, int depth) {
		SinglePortRam mem = super.createSyncReadSPRAM();
		mem.setName(label);
		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
		portFactory.addInDataPort(mem,"address",bitwidthForValue(depth));
		portFactory.addInDataPort(mem,"dataIn",bitwidth);
		portFactory.addInControlPort(mem,"re",1);
		portFactory.addInControlPort(mem,"we",1);
		portFactory.addOutDataPort(mem,"dataOut",bitwidth);
		return mem;
	}
	
	public SinglePortRom createSinglePortRom(String label, int bitwidth, int depth) {
		SinglePortRom mem = super.createSinglePortRom();
		mem.setName(label);
		portFactory.addInDataPort(mem,"address",bitwidthForValue(depth));
		portFactory.addInControlPort(mem,"re",1);
		portFactory.addOutDataPort(mem,"dataOut",bitwidth);
		return mem;
	}

	public SinglePortRam createSinglePortRam() {
		return createSinglePortRam(defaultMemoryLabel(), defaultPortBitwidth, 256);
	}
	
	public MultiPortRam createDualPortRam() {
		MultiPortRam mem = super.createMultiPortRam();
		mem.setName("default_name");
//		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
		for(int i=0;i<2;i++) {
			portFactory.addInDataPort(mem,"address_"+i,8);
			portFactory.addInDataPort(mem,"dataIn_"+i,16);
			portFactory.addInControlPort(mem,"re_"+i,1);
			portFactory.addInControlPort(mem,"we_"+i,1);
			portFactory.addOutDataPort(mem,"dataOut_"+i,16);
		}
		return mem;
	}

	


	private static int defaultPortBitwidth = 32;
	private static EditorStorageFactory instance =null; 

	protected EditorPortFactory portFactory;
	private EditorWiresFactory wireFactory;

	static int labelId=0;
	private String defaultLabel() {
		return "reg_"+(labelId++);
	}

	protected String defaultMemoryLabel() {
		return "mem_"+(labelId++);
	}

	static public EditorStorageFactory getFactory() {
		if (instance==null) {
			instance = new EditorStorageFactory();
		}
		return instance;
	}
	
	public EditorStorageFactory() {
		super();
		portFactory = EditorPortFactory.getFactory();
		wireFactory = EditorWiresFactory.getFactory();
	}
	

	public Register createRegister() {
		return createRegister(defaultLabel(), defaultPortBitwidth);
	}

	public Register createRegister(String label, int bitwidth) {
		Register op = super.createRegister();
		op.setName(label);
		portFactory.addInDataPort(op, "D",bitwidth);
		portFactory.addOutDataPort(op, "Q",bitwidth);
		return op;
	}


	public CERegister createCERegister(String label, int bitwidth) {
		CERegister op = super.createCERegister();
		op.setName(label);
		portFactory.addInDataPort(op,"D",bitwidth);
		portFactory.addInControlPort(op,"CE",1);
		portFactory.addOutDataPort(op,"Q",bitwidth);
		return op;
	}

	public CERegister createCERegister(int bitwidth) {
		return createCERegister(defaultLabel(),bitwidth);
	}
	

	public CERegister createCERegister() {
		return createCERegister(defaultLabel(),defaultPortBitwidth);
	}

	public ShiftRegister createShiftRegister() {
		return createShiftRegister(defaultLabel(), defaultPortBitwidth, 16);
	}

	public ShiftRegister createShiftRegister(String label, int bitwidth, int depth) {
		ShiftRegister op = super.createShiftRegister();
		op.setName(label);
		op.setDepth(depth);
		portFactory.addInDataPort(op, "D",bitwidth);
		portFactory.addInControlPort(op, "CE",1);
		portFactory.addOutDataPort(op, "Q",bitwidth);
		return op;
	}

}
