package fr.irisa.cairn.model.datapath.user.utils;

import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.util.StorageSwitch;

public class UserStorageSwitch<T> extends StorageSwitch<T> {

	private UserDatapathVisitor<T> root;

	public UserStorageSwitch(UserDatapathVisitor<T> root) {
		this.root = root;
	}

	@Override
	public T caseCERegister(CERegister object) {
		return root.caseCERegister(object);
	}

	@Override
	public T caseDualPortRam(DualPortRam object) {
		return root.caseDualPortRam(object);
	}

	@Override
	public T caseMultiPortRam(MultiPortRam object) {
		return root.caseMultiPortRam(object);
	}

	@Override
	public T caseMultiPortRom(MultiPortRom object) {
		return root.caseMultiPortRom(object);
	}

	@Override
	public T caseRegister(Register object) {
		return root.caseRegister(object);
	}

	@Override
	public T caseShiftRegister(ShiftRegister object) {
		return root.caseShiftRegister(object);
	}

	@Override
	public T caseSinglePortRam(SinglePortRam object) {
		return root.caseSinglePortRam(object);
	}

	@Override
	public T caseSinglePortRom(SinglePortRom object) {
		return root.caseSinglePortRom(object);
	}

}
