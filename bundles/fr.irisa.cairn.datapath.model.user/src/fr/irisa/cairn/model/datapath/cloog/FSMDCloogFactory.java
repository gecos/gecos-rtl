package fr.irisa.cairn.model.datapath.cloog;


import static fr.irisa.cairn.model.datapath.user.FSMDFactory.getCurrentDatapath;
import static fr.irisa.cairn.model.datapath.user.FSMDOperatorFactory.constant;

import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory;

public class FSMDCloogFactory {
	
	@Deprecated
	public static CERegister progCounter(String name, int width , int[] incrementValue, SingleOutputDataFlowBlock Din, SingleFlagBlock command, SingleFlagBlock enable) {
		getCurrentDatapath();
		ConstantValue values[] = new ConstantValue[incrementValue.length];
		for (int i = 0; i < values.length; i++) {
			values[i]= constant(incrementValue[i],width);
		}
		ControlFlowMux muxtree = cmuxtree(command, values);
		CERegister cereg = FSMDRegisterFactory.cereg("prog",muxtree,enable);
		throw new UnsupportedOperationException("Not yet completed");
		
	}

	@Deprecated
	private static ControlFlowMux cmuxtree(SingleFlagBlock command,SingleOutputDataFlowBlock[] values) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

}
