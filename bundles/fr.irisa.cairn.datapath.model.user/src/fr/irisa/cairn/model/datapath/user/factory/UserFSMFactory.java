package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.custom.LogicalOperations;
import fr.irisa.cairn.model.datapath.editor.factory.EditorFSMFactory;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanValue;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;

public class UserFSMFactory extends EditorFSMFactory{

	@Override
	public State createState() {
		
		State state =super.createState();
		currentFSM.getStates().add(state);
		return state;
		
	}

	private static UserFSMFactory instance;

	private FSM currentFSM;
	
	static public UserFSMFactory getFactory() {
		if (instance==null) {
			instance = new UserFSMFactory();
		}
		return  instance;
	}
	

	public void setFSM(FSM fsm) {
		currentFSM=fsm;
	}


	public State createSimpleState(String statename) {
		State state = createState();
		state.setLabel(statename);
		currentFSM.getStates().add(state);
		return state;
	}


	public void Wait(State srcState, State dstState, AbstractBooleanExpression proceed) {
		if (srcState.getTransitions().size()!=0) {
			throw new RuntimeException("Cannot wait on state "+srcState.getLabel()+" since is already connected to "+srcState.getTransitions().get(0));
		} 
		String statename = srcState.getLabel();
		Transition waitTransition = createTransition(statename +"Wait",srcState,srcState,LogicalOperations.not(proceed));
		Transition goTransition = createTransition(statename+"Proceed",srcState,dstState,proceed);
		srcState.getTransitions().add(waitTransition);
		srcState.getTransitions().add(goTransition);
	}

	public void Loop(State srcState, State branchState, State notbranchState, AbstractBooleanExpression branch) {
		if (srcState.getTransitions().size()!=0) {
			throw new RuntimeException("Cannot loop on state "+srcState.getLabel()+" since is already connected to "+srcState.getTransitions().get(0));
		} 
		String statename = srcState.getLabel();
		Transition waitTransition = createTransition(statename +"LoopNotTaken",srcState,notbranchState,LogicalOperations.not(branch));
		Transition goTransition = createTransition(statename+"LoopTaken",srcState,branchState,branch);
		srcState.getTransitions().add(waitTransition);
		srcState.getTransitions().add(goTransition);
	}

	public void Proceed(State srcState, State nextState) {
		if (srcState.getTransitions().size()!=0) {
			throw new RuntimeException("Cannot connect "+srcState.getLabel()+" to "+nextState.getLabel()+" because it is already connected to "+srcState.getTransitions().get(0));
		} 
		String statename = srcState.getLabel();
		Transition transition = createTransition(statename +"Proceed",srcState,nextState,createOrExpression());
		srcState.getTransitions().add(transition);
	}
	

	public static OrExpression or(AbstractBooleanExpression... pps) {
		OrExpression sop =  getFactory().createOrExpression();
		for (AbstractBooleanExpression productPredicate : pps) {
			sop.getTerms().add(productPredicate);
		}
		return sop;
	}

	public static AndExpression and(AbstractBooleanExpression... pts) {
		AndExpression pp = getFactory().createAndExpression();
		for (AbstractBooleanExpression predicateTerm : pts) {
				pp.getTerms().add(predicateTerm);
		}
		return pp;
	}
	
	public static BooleanFlagTerm T(InControlPort ip) {
		if (ip.getWidth()!=1) {
			throw new UnsupportedOperationException("Cannot create BooleanFlagTerm on InControlPort which width is not 1");
		}  else {
			BooleanFlagTerm term= getFactory().createBooleanFlagTerm();
			term.setFlag(ip);
			term.setNegated(false);
			return term;
		}
	}

	public static BooleanFlagTerm nT(InControlPort ip) {
		BooleanFlagTerm term= T(ip);
		term.setNegated(true);
		return term;
	}
	
	public static IndexedBooleanFlagTerm T(InControlPort ip, int offset) {
		if (offset>=ip.getWidth()) {
			throw new UnsupportedOperationException("Illegal offset value for "+ip.getName()+"["+offset+"] since "+ip.getName()+" has only "+ip.getWidth()+" bits");
		}  else {
			IndexedBooleanFlagTerm term= getFactory().createIndexedBooleanFlagTerm();
			term.setFlag(ip);
			term.setNegated(false);
			term.setOffset(offset);
			return term;
		}
	}

	public static IndexedBooleanFlagTerm nT(InControlPort ip, int offset) {
		IndexedBooleanFlagTerm term= T(ip, offset);
		term.setNegated(true);
		return term;
	}

	public static IntegerFlagTerm NEQ(InControlPort ip, int value) {
		IntegerFlagTerm term= EQU(ip,value);
		term.setEqual(false);
		return term;
	}

	public static IntegerFlagTerm EQU(InControlPort ip, int value) {
		if ((1<<ip.getWidth())<value) {
			throw new UnsupportedOperationException("Value "+value+" is out of range for "+ip.getName()+"");
		}  else {
			IntegerFlagTerm term= getFactory().createIntegerFlagTerm();
			term.setFlag(ip);
			term.setEqual(true);
			term.setValue(value);
			return term;
		}
	}

	public FSM createNewFSM(String name) {
		FSM fsm = super.createFSM();
		fsm.setName(name);
		setFSM(fsm);
		return fsm;
	}

	public FSM createNewFSM(String name, String startName) {
		FSM fsm = super.createFSM();
		setFSM(fsm);
		fsm.setName(name);
		State start  = createSimpleState("start");
		fsm.getStates().add(start);
		fsm.setStart(start);
		return fsm;
	}

	public InControlPort addInControlPort(String name, int width) {
		InControlPort ip = UserPortFactory.getFactory().createInControlPort(name, width);
		currentFSM.getActivate().add(ip);
		return ip;
	}
	
	public InControlPort inCtrl(String name, int width) {
		return addInControlPort(name, width);
	}

	@Deprecated
	public OutControlPort  addOutControlPort(String name, int width) {
		OutControlPort op = UserPortFactory.getFactory().createOutControlPort(name,width);
		currentFSM.getFlags().add(op);
		return op;
	}

	public OutControlPort  addOutControlPort(String name, int width, int defaultValue) {
		OutControlPort op = UserPortFactory.getFactory().createOutControlPort(name,width);
		currentFSM.getFlags().add(op);
		OutputDefaultValues odv = super.createOutputDefaultValues();
		odv.setPort(op);
		odv.setValue(defaultValue);
		currentFSM.getDefaultValues().add(odv);
		return op;
	}

	

	public IntegerCommandValue addCommand(State state, OrExpression sop, OutControlPort op, int value) {
		IntegerCommandValue icv = super.createIntegerCommandValue();
		icv.setValue(value);
		icv.setCommand(op);
		state.getActivatedCommands().add(icv);
		icv.setPredicate(sop);
		return icv;
	}

	public BooleanCommandValue addCommand(State state, OrExpression sop, OutControlPort op, boolean value) {
		BooleanCommandValue bcv = super.createBooleanCommandValue();
		bcv.setValue(value?BooleanValue.ONE:BooleanValue.ZERO);
		bcv.setCommand(op);
		state.getActivatedCommands().add(bcv);
		bcv.setPredicate(sop);
		return bcv;
	}

	public IntegerCommandValue addCommand(State state, OutControlPort op, int value) {
		return addCommand(state, createOrExpression(), op, value);
	}

	public BooleanCommandValue addCommand(State state, OutControlPort op, boolean value) {
		return addCommand(state, createOrExpression(), op, value);
	}

	
}
