package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.pads.impl.PadsFactoryImpl;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class EditorPadsFactory extends PadsFactoryImpl{

	private static int padId =0;

	protected static final int defaultOperatorBitwidth = 32;
	
	private static EditorPadsFactory instance =null; 

	
	protected EditorPortFactory portFactory;
	private EditorWiresFactory wireFactory;

	protected String defaultLabel() {
		return "pad"+(padId ++);
	}


	
	static public EditorPadsFactory getFactory() {
		if (instance==null) {
			instance = new EditorPadsFactory();
		}
		return instance;
	}
	
	public EditorPadsFactory() {
		super();
		portFactory = EditorPortFactory.getFactory();
		wireFactory = EditorWiresFactory.getFactory();
	}
	
	public DataInputPad createDataInputPad() {
		return createDataInputPad(defaultLabel(),defaultOperatorBitwidth);
	}

	public DataInputPad createDataInputPad(int bitwidth) {
		return createDataInputPad(defaultLabel(),bitwidth);
	}

	public DataInputPad createDataInputPad(Datapath dp) {
		DataInputPad pad = createDataInputPad(defaultLabel(),defaultOperatorBitwidth);
		InDataPort oport = portFactory.createInDataPort(defaultLabel(), defaultOperatorBitwidth);
		pad.setAssociatedPort(oport);
		dp.getIn().add(oport);
		return pad;
	}

	public DataOutputPad createDataOutputPad(Datapath dp) {
		DataOutputPad pad = createDataOutputPad(defaultLabel(),defaultOperatorBitwidth);
		OutDataPort oport = portFactory.createOutDataPort(defaultLabel(), defaultOperatorBitwidth);
		pad.setAssociatedPort(oport);
		dp.getOut().add(oport);
		return pad;
	}

	public DataOutputPad createDataOutputPad() {
		return createDataOutputPad(defaultLabel(),defaultOperatorBitwidth);
	}

	public DataOutputPad createDataOutputPad(int bitwidth) {
		return createDataOutputPad(defaultLabel(),bitwidth);
	}

	public ControlPad createControlPad() {
		return createControlPad(defaultLabel(),defaultOperatorBitwidth);
	}

	public ControlPad createControlPad(int bitwidth) {
		return createControlPad(defaultLabel(),bitwidth);
	}

	public ControlPad createControlPad(Datapath dp) {
		ControlPad pad = createControlPad(defaultLabel(),defaultOperatorBitwidth);
		InControlPort oport = portFactory.createInControlPort(defaultLabel(), defaultOperatorBitwidth);
		pad.setAssociatedPort(oport);
		dp.getActivate().add(oport);
		return pad;
	}

	public StatusPad createStatusPad() {
		return createStatusPad(defaultLabel(),defaultOperatorBitwidth);
	}
	
	public StatusPad createStatusPad(int bitwidth) {
		return createStatusPad(defaultLabel(),bitwidth);
	}

	public StatusPad createStatusPad(Datapath dp) {
		StatusPad pad = createStatusPad(defaultLabel(),defaultOperatorBitwidth);
		OutControlPort oport = portFactory.createOutControlPort(defaultLabel(), defaultOperatorBitwidth);
		pad.setAssociatedPort(oport);
		dp.getFlags().add(oport);
		return pad;
	}

	
	public DataInputPad createDataInputPad(String name, int bitwidth) {
		DataInputPad pad = super.createDataInputPad();
		portFactory.addOutDataPort(pad, "O",bitwidth);
		pad.setName(name);
		return pad;
	}

	public DataOutputPad createDataOutputPad(String name, int bitwidth) {
		DataOutputPad pad = super.createDataOutputPad();
		portFactory.addInDataPort(pad, "I",bitwidth);
		pad.setName(name);
		return pad;
	}

	public ControlPad createControlPad(String name, int bitwidth) {
		ControlPad pad = super.createControlPad();
		portFactory.addOutControlPort(pad, "O",bitwidth);
//		OutControlPort op = portFactory.createOutControlPort(name, bitwidth);
//		pad.setAssociatedPort(op);
		pad.setName(name);
		return pad;
	}

	public StatusPad createStatusPad(String name, int bitwidth) {
		StatusPad pad = super.createStatusPad();
		portFactory.addInControlPort(pad, "I",bitwidth);
		pad.setName(name);
//		InControlPort op = portFactory.createInControlPort(name, bitwidth);
//		pad.setAssociatedPort(op);
		return pad;
	}

	

}
