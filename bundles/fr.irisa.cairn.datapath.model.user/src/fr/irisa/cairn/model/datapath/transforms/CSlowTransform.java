package fr.irisa.cairn.model.datapath.transforms;

import java.util.List;

import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.util.StorageSwitch;
import fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class CSlowTransform {
	int slowdown = 1;

	
	public CSlowTransform(int cslow) {
		slowdown = cslow;
	}
	
	public class MemoryCSlow extends StorageSwitch {

		@Override
		public Object caseRegister(Register object) {
			List<DataFlowWire> wires = object.getOutput().getWires();
			appendRegisters(object);
			return super.caseRegister(object);
		}

		private void appendRegisters(Register object) {
			Register prev= object;
			Register reg = object;
			OutDataPort source = prev.getInput().getWire().getSource();
			for (int i=1; i<slowdown;i++) {
				if(i==1)
					reg = FSMDRegisterFactory.reg(object+"_cslow_"+i,source);
				else
					reg = FSMDRegisterFactory.reg(object+"_cslow_"+i,reg);
			}
			object.connect(reg.getOutput());
		}

		@Override
		public Object caseCERegister(CERegister object) {
			// TODO Auto-generated method stub
			return super.caseCERegister(object);
		}
		
		
		
	}
}
