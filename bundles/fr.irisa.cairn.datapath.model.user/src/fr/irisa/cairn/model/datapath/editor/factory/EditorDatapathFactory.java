package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.impl.DatapathFactoryImpl;


public class EditorDatapathFactory extends  DatapathFactoryImpl {

	private static int datapathId =0;


	
	protected String defaultLabel() {
		return "datapath"+(datapathId ++);
	}

	public Datapath createDatapath() {
		// TODO Auto-generated method stub
		return super.createDatapath();
	}



	public Datapath createDatapath(String name) {
		Datapath d=  super.createDatapath();
		d.setName(name);
		return d;
	}


} //DatapathFactory
