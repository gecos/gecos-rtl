package fr.irisa.cairn.model.datapath.user;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.PadsFactory;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class FSMDPadsFactory {

	static PadsFactory facory = PadsFactory.eINSTANCE;
	

	public static DataInputPad IDPad(String name, int bitwidth) {
		DataInputPad pad = facory.createDataInputPad();
		pad.setName(name);
		InDataPort ip = FSMDPortsFactory.IDP(name, bitwidth);
		OutDataPort op = FSMDPortsFactory.ODP(pad,name, bitwidth);
		pad.setAssociatedPort(ip);
		return pad;
	}

	public static DataInputPad IDPad(Datapath dp, String name, int bitwidth) {
		DataInputPad pad = IDPad(name, bitwidth);
		dp.getComponents().add(pad);
		dp.getIn().add(pad.getAssociatedPort());
		return pad;
	}

	public static  DataOutputPad ODPad(String name, int bitwidth) {
		DataOutputPad pad = facory.createDataOutputPad();
		pad.setName(name);
		InDataPort ip = FSMDPortsFactory.IDP(pad,name, bitwidth);
		OutDataPort op = FSMDPortsFactory.ODP(name, bitwidth);
		pad.setAssociatedPort(op);
		return pad;
	}

	public static  DataOutputPad ODPad(Datapath dp, String name, int bitwidth) {
		DataOutputPad pad = ODPad(name, bitwidth);
		dp.getComponents().add(pad);
		dp.getOut().add(pad.getAssociatedPort());
		return pad;
	}
	

	
	public static  ControlPad ICPad(String name, int bitwidth) {
		ControlPad pad = facory.createControlPad();
		pad.setName(name); 
		InControlPort ip = FSMDPortsFactory.ICP(name, bitwidth); 
		OutControlPort op = FSMDPortsFactory.OCP(pad,name, bitwidth); 
		pad.setAssociatedPort(ip);
		return pad;
	}


	public static ControlPad ICPad(Datapath dp, String name, int bitwidth) {
		ControlPad pad = ICPad(name, bitwidth);
		dp.getComponents().add(pad);
		dp.getActivate().add(pad.getAssociatedPort());
		return pad;
	}

	public static  StatusPad OCPad(String name, int bitwidth) {
		StatusPad pad = facory.createStatusPad();
		pad.setName(name);
		InControlPort ip = FSMDPortsFactory.ICP(pad,name, bitwidth);
		OutControlPort op = FSMDPortsFactory.OCP(name, bitwidth); 
		pad.setAssociatedPort(op);
		return pad;
	}

	public static StatusPad OCPad(Datapath dp, String name, int bitwidth) {
		StatusPad pad = OCPad(name, bitwidth);
		dp.getComponents().add(pad);
		dp.getFlags().add(pad.getAssociatedPort());
		return pad;
	}

	
}
