/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;
import fr.irisa.cairn.model.fsm.impl.FsmFactoryImpl;


public class EditorFSMFactory extends FsmFactoryImpl implements
		FsmFactory {

	static public EditorFSMFactory getFactory() {
		if (instance==null) {
			instance = new EditorFSMFactory();
		}
		return instance;
	}
	
	private static EditorFSMFactory instance =null;
	
	@Override
	public FSM createFSM() {
		// TODO Auto-generated method stub
		return super.createFSM();
	}


	@Override
	public IntegerCommandValue createIntegerCommandValue() {
		// TODO Auto-generated method stub
		return super.createIntegerCommandValue();
	}


	@Override
	public State createState() {
		// TODO Auto-generated method stub
		return super.createState();
	}


	@Override
	public Transition createTransition() {
		// TODO Auto-generated method stub
		return super.createTransition();
	}

	public Transition createTransition(String label,State start, State dest) {
		Transition t = createUnconditionnedTransition();
		t.setLabel(label);
		t.setDst(dest);
		t.setSrc(start);
		t.setPredicate(createBooleanConstant());
		return t;
	}

	public Transition createTransition(String label,State start, State dest, AbstractBooleanExpression pred) {
		Transition t = super.createTransition();
		t.setLabel(label);
		t.setDst(dest);
		t.setSrc(start);
		t.setPredicate(pred);
		return t;
	}

	public Transition createUnconditionnedTransition() {
		Transition t = createTransition();
		BooleanConstant bc = createBooleanConstant();
		bc.setValue(true);
		t.setPredicate(bc);
		return t;
	}

} //FSMModelFactoryImpl
