package fr.irisa.cairn.model.datapath.xml;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathPackage;

public class DatapathXMLReader {

	private static DatapathXMLReader singleton = null;
	
	public static DatapathXMLReader getXMLReader() {
		if (singleton==null) {
			singleton = new DatapathXMLReader();
		}
		return singleton;
	}

	public Datapath load(String modelfilename) {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION, 
			 new XMIResourceFactoryImpl());

		resourceSet.getPackageRegistry().put
			(DatapathPackage.eNS_URI, 
			 DatapathPackage.eINSTANCE);

		Resource resource = resourceSet.getResource(URI.createFileURI(modelfilename), true);
		return (Datapath) resource.getContents().get(0);
	}
	
}
