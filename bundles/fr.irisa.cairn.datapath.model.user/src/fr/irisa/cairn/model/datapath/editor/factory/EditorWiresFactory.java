package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.impl.WiresFactoryImpl;

public class EditorWiresFactory extends WiresFactoryImpl {
	
	static int controlWireCount = 0;
	static int hybridWireCount = 0;

	private String defaultControlWireLabel() {
		return "c_"+(controlWireCount++);
	}
	
	private String defaultHybridWireLabel() {
		return "h_"+( hybridWireCount++);
	}

	static int dataWireCount = 0;
	private static int defaultPortBitwidth = 32;
	
	private static EditorWiresFactory instance =null; 
	

	public ControlFlowWire createControlFlowWire(String label, int width) {
		ControlFlowWire wire = super.createControlFlowWire();
		wire.setName(label);
		wire.setWidth(width);
		return wire;
	}
	
	public ControlFlowWire createControlFlowWire() {
		ControlFlowWire wire = createControlFlowWire(
				defaultControlWireLabel(),
				defaultPortBitwidth);
		return wire;
	}
	
	public ControlFlowWire createControlFlowWire(int width) {
		ControlFlowWire wire = createControlFlowWire(
				defaultControlWireLabel(),
				width);
		return wire;
	}
	



	private String defaultDataWireLabel() {
		return "e_"+(dataWireCount++);
	}


	public DataFlowWire createDataFlowWire(String label, int width) {
		DataFlowWire wire = super.createDataFlowWire();
		wire.setName(label);
		wire.setWidth(width);
		return wire;
	}

	public DataFlowWire createDataFlowWire(int width) {
		DataFlowWire wire = createDataFlowWire(
				defaultDataWireLabel(),width);
		return wire;
	}

	public DataFlowWire createDataFlowWire() {
		DataFlowWire wire = createDataFlowWire(
				defaultDataWireLabel(),defaultPortBitwidth);
		return wire;
	}

	
	static public EditorWiresFactory getFactory() {
		if (instance==null) instance = new EditorWiresFactory();
		return instance;
	}
	
	public EditorWiresFactory() {
		super();
	}
	





}
