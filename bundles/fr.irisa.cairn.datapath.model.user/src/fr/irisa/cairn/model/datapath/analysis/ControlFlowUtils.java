package fr.irisa.cairn.model.datapath.analysis;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;

public class ControlFlowUtils {
	
	public static List<FlagBearerBlock> getPrecedingNodes(ActivableBlock current) {
		List<FlagBearerBlock> res = new ArrayList<FlagBearerBlock> (); 
		for (InControlPort ip : current.getActivate())  {
			ControlFlowWire wire = ip.getWire();
			if(wire!=null) {
				if(wire.getSource()!=null) {
					FlagBearerBlock parentNode = wire.getSource().getParentNode();
					if (parentNode!=null) {
						res.add(parentNode);
					}
				}
			}
		}
		return res;
	}

	public static FlagBearerBlock getPrecedingNode(InControlPort ip) {
		ControlFlowWire wire = ip.getWire();
		if(wire!=null) {
			if(wire.getSource()!=null) {
				FlagBearerBlock parentNode = wire.getSource().getParentNode();
				if (parentNode!=null) {
					return parentNode;
				}
			}
		}
		return null;
	}

	public static List<ActivableBlock> getSuccessorNodes(OutControlPort op) {
		List<ActivableBlock> res = new ArrayList<ActivableBlock> (); 
		List<ControlFlowWire> wires = op.getWires();
		for (ControlFlowWire wire : wires) {
			if(wire!=null) {
				if(wire.getSink()!=null) {
					ActivableBlock parentNode = wire.getSink().getParentNode();
					if (parentNode!=null) {
						res.add(parentNode);
					}
				}
			}
		}
		return res;
	}

	public static List<ActivableBlock> getSuccessorNodes(FlagBearerBlock current) {
		List<ActivableBlock> res = new ArrayList<ActivableBlock> (); 
		for (OutControlPort op: current.getFlags()) {
			List<ControlFlowWire> wires = op.getWires();
			for (ControlFlowWire wire : wires) {
				if(wire!=null) {
					if(wire.getSink()!=null) {
						ActivableBlock parentNode = wire.getSink().getParentNode();
						if (parentNode!=null) {
							res.add(parentNode);
						}
					}
				}
			}
		}
		return res;
	}

}
