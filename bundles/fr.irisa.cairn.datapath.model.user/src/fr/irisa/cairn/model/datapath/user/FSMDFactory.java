package fr.irisa.cairn.model.datapath.user;

import java.util.Stack;

import fr.irisa.cairn.model.datapath.BlackBoxBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.DatapathFactory;
import fr.irisa.cairn.model.datapath.Pad;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.PadsPackage;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class FSMDFactory {
	
	private static Datapath currentDP;
	private static Stack<Datapath> DPStack = new Stack<Datapath>();
	
	
	public static Datapath datapath(String name) {
		Datapath dp;
		
		dp = DatapathFactory.eINSTANCE.createDatapath();
		dp.setName(name);
		
		return dp;
	}
	
	public static Datapath datapath(String name, Pad... pads) {
		Datapath dp = DatapathFactory.eINSTANCE.createDatapath();
		dp.setName(name);
		for (Pad pad : pads) {
			switch (pad.eClass().getClassifierID()) {
				case PadsPackage.DATA_INPUT_PAD:
					dp.getComponents().add(pad);
					dp.getIn().add(((DataInputPad)pad).getAssociatedPort());
					break;
				case PadsPackage.DATA_OUTPUT_PAD:
					dp.getComponents().add(pad);
					dp.getOut().add(((DataOutputPad)pad).getAssociatedPort());
					break;
				case PadsPackage.CONTROL_PAD:
					dp.getComponents().add(pad);
					dp.getActivate().add(((ControlPad)pad).getAssociatedPort());
					break;
				case PadsPackage.STATUS_PAD:
					dp.getComponents().add(pad);
					dp.getFlags().add(((StatusPad)pad).getAssociatedPort());
					break;
				default:
					throw new UnsupportedOperationException("Not yet implemented");
			}
		}
		return dp;
		
	}

	public static void pushDatapath(String name) {
		if(currentDP==null) {
			currentDP = DatapathFactory.eINSTANCE.createDatapath();
			currentDP.setName(name);
		} else {
			DPStack.push(currentDP);
			currentDP = DatapathFactory.eINSTANCE.createDatapath();
			currentDP.setName(name);
			DPStack.peek().getComponents().add(currentDP);
		}
	}

	public static void pushDatapath(Datapath dp) {
		if(currentDP==null) {
			currentDP = dp;
		} else {
			DPStack.push(currentDP);
			currentDP = dp;
			DPStack.peek().getComponents().add(currentDP);
		}
	}

	public static void setAsLibraryComponent(Datapath dp) {
		getCurrentDatapath().getLibrary().add(dp);
	}

	public static BlackBoxBlock instanciateLibraryElement(Datapath ref, String instname) {
		if(getCurrentDatapath().getLibrary().contains(ref)) {
			BlackBoxBlock bb = DatapathFactory.eINSTANCE.createBlackBoxBlock();
			getCurrentDatapath().getLibrary().add(bb);
			bb.setImplementation(ref);
			bb.setName(instname);
			for(int i=0;i<ref.getActivate().size();i++) {
				InControlPort refPort = ref.getActivate().get(i); 
				bb.getActivate().add(FSMDPortsFactory.ICP(refPort.getName(), refPort.getWidth()));
			}
			for(int i=0;i<ref.getIn().size();i++) {
				InDataPort refPort = ref.getIn().get(i); 
				bb.getIn().add(FSMDPortsFactory.IDP(refPort.getName(), refPort.getWidth()));
			}
			for(int i=0;i<ref.getOut().size();i++) {
				OutDataPort refPort = ref.getOut().get(i); 
				bb.getOut().add(FSMDPortsFactory.ODP(refPort.getName(), refPort.getWidth()));
			}
			for(int i=0;i<ref.getFlags().size();i++) {
				OutControlPort refPort = ref.getFlags().get(i); 
				bb.getFlags().add(FSMDPortsFactory.OCP(refPort.getName(), refPort.getWidth()));
			}
			return bb;
		} else {
			throw new UnsupportedOperationException("Library for "+getCurrentDatapath()+" does not contain "+ref);
		}
	}

	public static BlackBoxBlock instanciateLibraryElement(Datapath ref, String instname, 
			OutDataPort[] odps,
			OutControlPort[] ocps,
			InDataPort[] idps,
			InControlPort[] icps
			
	) {
			BlackBoxBlock newInst= instanciateLibraryElement(ref, instname);
			
			int size = newInst.getIn().size();
			if(size!=odps.length) throw new UnsupportedOperationException("Interface mismatch for InDataPorts");
			for(int i=0;i<size;i++) {
				InDataPort refPort = newInst.getIn().get(i); 
				refPort.connect(odps[i]);
			}
			size = newInst.getActivate().size();
			if(size!=ocps.length) throw new UnsupportedOperationException("Interface mismatch for InControlPorts");
			for(int i=0;i<size;i++) {
				InControlPort refPort = newInst.getActivate().get(i); 
				refPort.connect(ocps[i]);
			}
			size = newInst.getOut().size();
			if(size!=idps.length) throw new UnsupportedOperationException("Interface mismatch for OutDataPorts");
			for(int i=0;i<size;i++) {
				OutDataPort refPort =newInst.getOut().get(i); 
				refPort.connect(idps[i]);
			}
			size = newInst.getFlags().size();
			if(size!=icps.length) throw new UnsupportedOperationException("Interface mismatch for OutControlPorts ");
			for(int i=0;i<size;i++) {
				OutControlPort refPort = newInst.getFlags().get(i); 
				refPort.connect(icps[i]);
			}
			return newInst;
	}
	
	public static Datapath popDatapath() {
		Datapath tmp =currentDP;
		if (DPStack.size()==0) {
			currentDP=null;
		} else {
			currentDP=DPStack.pop();
		}
		return tmp;
	}

	/***************************************************************************************************
	 *  Combinational Arithmetic factory
	 **************************************************************************************************/


	/***************************************************************************************************
	 *  Combinational Boolean logic factory (bitwise)
	 **************************************************************************************************/


	public static Datapath getCurrentDatapath() {
		if (currentDP==null) throw new RuntimeException("Empty datapath container");
		return currentDP;
	}

}
