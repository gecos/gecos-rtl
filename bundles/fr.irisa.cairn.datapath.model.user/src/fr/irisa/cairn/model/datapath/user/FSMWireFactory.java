package fr.irisa.cairn.model.datapath.user;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.editor.factory.EditorWiresFactory;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;

public class FSMWireFactory extends EditorWiresFactory {
	

	static int controlWireCount = 0;
	static int dataWireCount = 0;
	static int hybridWireCount = 0;
	private static int defaultDataWireBitwidth = 32;
	private static int defaultControlWireBitwidth=1;

	private static FSMWireFactory instance =null; 

	private Datapath dp;
	
	private String getControlWireLabel() {
		return "c_"+(controlWireCount++);
	}

	private String getDataWireLabel() {
		return "d_"+(dataWireCount++);
	}
	
	private String getHybridWireLabel() {
		return "h_"+(hybridWireCount++);
	}
	
	private FSMWireFactory(Datapath dp) {
		super();
		this.dp=dp;
	}
	
	public static FSMWireFactory getInstance(Datapath dp) {
		if( instance==null) {
			instance = new FSMWireFactory(dp);
		} else {
			if (dp==null) {
				instance.dp=dp;
			}
		}
		return instance;
	}


	public ControlFlowWire createControlFlowWire(String name, int width) {
		ControlFlowWire wire  = super.createControlFlowWire(name,width);
		dp.getControlWires().add(wire);
		return wire;

	}
	public ControlFlowWire createControlFlowWire(int width) {
		ControlFlowWire wire  = super.createControlFlowWire(width);
		dp.getControlWires().add(wire);
		return wire;
	}

	public ControlFlowWire createControlFlowWire() {
		ControlFlowWire wire  = super.createControlFlowWire();
		dp.getControlWires().add(wire);
		return wire;
	}

	public DataFlowWire createDataFlowWire(String name, int width) {
		DataFlowWire wire  = super.createDataFlowWire(name,width);
		dp.getDataWires().add(wire);
		return wire;
	}

	public DataFlowWire createDataFlowWire(int width) {
		DataFlowWire wire  = super.createDataFlowWire(width);
		dp.getDataWires().add(wire);
		return wire;
	}

	public DataFlowWire createDataFlowWire() {
		DataFlowWire wire  = super.createDataFlowWire();
		dp.getDataWires().add(wire);
		return wire;
	}

	public boolean checkConnection(InDataPort src, OutDataPort sink) throws UnsupportedOperationException {
		AbstractBlock srcNode = src.getParentNode();
		AbstractBlock sinkNode = sink.getParentNode();
		Datapath srcContainer = srcNode.getParent();
		Datapath sinkContainer = sinkNode.getParent();

		if (srcContainer!=dp) {
			throw new UnsupportedOperationException("source port "+src+" of block "+srcNode+"("+(srcContainer)+") does not belong to current container "+dp.getName());
		}
		if (sinkContainer!=dp) {
			throw new UnsupportedOperationException("sink port container ("+sinkContainer+") belongs to "+sinkContainer+" which is not then current container "+dp);
		}
		
		if (src.getWidth()!=sink.getWidth()) {
			String srcName = src.getParentNode().getName()+"."+src.getName();
			String sinkName = sink.getParentNode().getName()+"."+sink.getName();
			throw new UnsupportedOperationException("Port bitwidth mismatch. Source is " + srcName + " and is " + src.getWidth() + " bit wide, sink is " + sinkName + " and " + sink.getWidth()
					+ " bit wide");
		}
		return true;
	}
	
	public boolean checkConnection(OutDataPort src, InDataPort sink) throws UnsupportedOperationException {
		AbstractBlock srcNode = src.getParentNode();
		AbstractBlock sinkNode = sink.getParentNode();
		Datapath srcContainer = srcNode.getParent();
		Datapath sinkContainer = sinkNode.getParent();

		if (srcContainer!=dp) {
			throw new UnsupportedOperationException("source port "+src+" of block "+srcNode+"("+(srcContainer)+") does not belong to current container "+dp.getName());
		}
		if (sinkContainer!=dp) {
			throw new UnsupportedOperationException("sink port container ("+sinkContainer+") does not belong to current container "+dp);
		}
		
		if (src.getWidth()!=sink.getWidth()) {
			throw new UnsupportedOperationException("port bitwidth mismatch. Source is "+src.getName()+" and is "+src.getWidth()+" bit wide, sink is "+sink.getName()+" and "+sink.getWidth() +" bit wide");
		}
		return true;
	}

	public DataFlowWire connect(InDataPort src, OutDataPort sink) throws UnsupportedOperationException {
		checkConnection(src, sink);
		DataFlowWire wire = createDataFlowWire();
		wire.setSink(src);
		wire.setSource(sink);
		wire.setWidth(sink.getWidth());
		return wire;
	}

	public DataFlowWire connect(InDataPort src, SingleOutputDataFlowBlock sink) throws UnsupportedOperationException {
		return connect(src,sink.getOutput());
	}
	
	public DataFlowWire connect(OutDataPort src, InDataPort sink) throws UnsupportedOperationException {
		checkConnection(src, sink);
		DataFlowWire wire = createDataFlowWire();
		wire.setSink(sink);
		wire.setSource(src);
		wire.setWidth(sink.getWidth());
		return wire;
	}

	public boolean checkConnection(InControlPort src, OutControlPort sink) throws UnsupportedOperationException {
		Datapath srcContainer = src.getParentNode().getParent();
		Datapath sinkContainer = sink.getParentNode().getParent();
		
		if (srcContainer!=dp) {
			throw new UnsupportedOperationException("source port does not belong to current container "+dp.getName());
		}
		if (sinkContainer!=dp) {
			throw new UnsupportedOperationException("sink port container ("+sinkContainer+") does not belong to current container "+dp);
		}
		if (src.getWidth()!=sink.getWidth()) {
			throw new UnsupportedOperationException("port bitwidth mismatch. Source is "+srcContainer+"."+src.getName()+" and is "+src.getWidth()+" bit wide, sink is "+sinkContainer+"."+sink.getName()+" and "+sink.getWidth() +" bit wide");
		}
		return true;
	}

	public ControlFlowWire connect(InControlPort src, OutControlPort sink) throws UnsupportedOperationException {
		checkConnection(src, sink);
		ControlFlowWire wire = createControlFlowWire();
		wire.setSink(src);
		wire.setSource(sink);
		wire.setWidth(sink.getWidth());
		return wire;
	}

	public BinaryOperator connect(BinaryOperator binop, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		connect(binop.getIn().get(0),A.getOutput());
		connect(binop.getIn().get(1),B.getOutput());
		return binop;
	}
	
	public Compare connect(Compare cmpop, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		connect(cmpop.getIn().get(0),A.getOutput());
		connect(cmpop.getIn().get(1),B.getOutput());
		return cmpop;
	}
	
	public DataOutputPad connect(DataOutputPad opad, SingleOutputDataFlowBlock A) {
		connect(opad.getInput(0),A.getOutput());
		return opad;
	}

	public ControlFlowMux connect(ControlFlowMux cmux, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, OutControlPort c) {
		connect(cmux.getIn().get(0),A.getOutput());
		connect(cmux.getIn().get(1),B.getOutput());
		connect(cmux.getControlPort(0),c);
		return cmux;
	}
	
	public ControlFlowMux connect(ControlFlowMux cmux, OutDataPort A, SingleOutputDataFlowBlock B, OutControlPort c) {
		connect(cmux.getIn().get(0),A);
		connect(cmux.getIn().get(1),B.getOutput());
		connect(cmux.getControlPort(0),c);
		return cmux;
	}

	public DataFlowMux connect(DataFlowMux cmux, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleOutputDataFlowBlock  c) {
		connect(cmux.getIn().get(0),A.getOutput());
		connect(cmux.getIn().get(1),B.getOutput());
		connect(cmux.getIn().get(2),c.getOutput());
		return cmux;
	}

	public UnaryOperator connect(UnaryOperator unop, SingleOutputDataFlowBlock A) {
		connect(unop.getIn().get(0),A.getOutput());
		return unop;
	}

	public ReductionOperator connectAt(ReductionOperator unop, int pos, SingleOutputDataFlowBlock A) {
		connect(unop.getIn().get(pos),A.getOutput());
		return unop;
	}

	public SingleInputDataFlowBlock connect(SingleInputDataFlowBlock reg, SingleOutputDataFlowBlock A) {
		connect(reg.getInput(),A.getOutput());
		return reg;
	}

	public BitSelect connect(BitSelect bs, SingleOutputDataFlowBlock A) {
		connect(bs.getIn().get(0),A.getOutput());
		return bs;
	}

	public Merge connect(Merge bs, SingleOutputDataFlowBlock A,SingleOutputDataFlowBlock B) {
		connect(bs.getIn().get(0),A.getOutput());
		return bs;
	}

	public TernaryOperator connect(TernaryOperator ternop, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B,SingleOutputDataFlowBlock C) {
		connect(ternop.getIn().get(0),A.getOutput());
		connect(ternop.getIn().get(1),B.getOut().get(0));
		connect(ternop.getIn().get(2),C.getOut().get(0));
		return ternop;
	}

	public ControlFlowMux connect(ControlFlowMux cmux, OutDataPort A, OutDataPort B, OutControlPort c) {
		connect(cmux.getIn().get(0),A);
		connect(cmux.getIn().get(1),B);
		connect(cmux.getControlPort(0),c);
		return cmux;
	}

	public ControlFlowMux connect(ControlFlowMux cmux, SingleOutputDataFlowBlock A,
			OutDataPort B, OutControlPort c) {
		connect(cmux.getIn().get(0),A.getOutput());
		connect(cmux.getIn().get(1),B);
		connect(cmux.getControlPort(0),c);
		return cmux;
		
	}

	
	
	public boolean checkConnection(InDataPort src, OutControlPort sink) throws UnsupportedOperationException {
		AbstractBlock srcNode = src.getParentNode();
		AbstractBlock sinkNode = sink.getParentNode();
		Datapath srcContainer = srcNode.getParent();
		Datapath sinkContainer = sinkNode.getParent();

		if (srcContainer!=dp) {
			throw new UnsupportedOperationException("source port "+src+" of block "+srcNode+"("+(srcContainer)+") does not belong to current container "+dp.getName());
		}
		if (sinkContainer!=dp) {
			throw new UnsupportedOperationException("sink port container ("+sinkContainer+") belongs to "+sinkContainer+" which is not then current container "+dp);
		}
		
		if (src.getWidth()!=sink.getWidth()) {
			String srcName = src.getParentNode().getName()+"."+src.getName();
			String sinkName = sink.getParentNode().getName()+"."+sink.getName();
			throw new UnsupportedOperationException("Port bitwidth mismatch. Source is " + srcName + " and is " + src.getWidth() + " bit wide, sink is " + sinkName + " and " + sink.getWidth()
					+ " bit wide");
		}
		return true;
	}
	
	
	public boolean checkConnection(OutDataPort src, InControlPort sink) throws UnsupportedOperationException {
		AbstractBlock srcNode = src.getParentNode();
		AbstractBlock sinkNode = sink.getParentNode();
		Datapath srcContainer = srcNode.getParent();
		Datapath sinkContainer = sinkNode.getParent();

		if (srcContainer!=dp) {
			throw new UnsupportedOperationException("source port "+src+" of block "+srcNode+"("+(srcContainer)+") does not belong to current container "+dp.getName());
		}
		if (sinkContainer!=dp) {
			throw new UnsupportedOperationException("sink port container ("+sinkContainer+") belongs to "+sinkContainer+" which is not then current container "+dp);
		}
		
		if (src.getWidth()!=sink.getWidth()) {
			String srcName = src.getParentNode().getName()+"."+src.getName();
			String sinkName = sink.getParentNode().getName()+"."+sink.getName();
			throw new UnsupportedOperationException("Port bitwidth mismatch. Source is " + srcName + " and is " + src.getWidth() + " bit wide, sink is " + sinkName + " and " + sink.getWidth()
					+ " bit wide");
		}
		return true;
	}

}

