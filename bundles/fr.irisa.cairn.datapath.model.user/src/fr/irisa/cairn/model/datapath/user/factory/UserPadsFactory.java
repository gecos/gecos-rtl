package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.editor.factory.EditorPadsFactory;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class UserPadsFactory extends EditorPadsFactory{

	
	public UserWireFactory getWireFactory() {
		return wireFactory;
	}

	private static UserPadsFactory instance =null; 

	private Datapath dp;
	
	private UserWireFactory wireFactory;
	
	static public UserPadsFactory getFactory(Datapath dp) {
		if (instance==null) {
			instance = new UserPadsFactory(dp);
		} else {
			if (instance.dp!=dp) {
				System.out.println("Changin "+instance.getClass().getSimpleName()+ "for new DP "+dp.getName());
				instance.dp=dp;
			}
		}
		return instance;

	}
	
	public UserPadsFactory(Datapath adp) {
		super();
		this.dp=adp;
		wireFactory = UserWireFactory.getInstance(dp);
	}
	

	public DataInputPad createDataInputPad(Datapath dp, String name, int bitwidth) {
		DataInputPad pad = super.createDataInputPad(bitwidth);
		pad.setName(name);
		dp.getComponents().add(pad);
		InDataPort ip = portFactory.createInDataPort(name, bitwidth);
		dp.getIn().add(ip);
		pad.setAssociatedPort(ip);
		return pad;
	}

	public DataOutputPad createDataOutputPad(Datapath dp, String name, int bitwidth) {
		DataOutputPad pad = super.createDataOutputPad(bitwidth);
		dp.getComponents().add(pad);
		pad.setName(name);
		OutDataPort op = portFactory.createOutDataPort(name, bitwidth);
		dp.getOut().add(op);
		pad.setAssociatedPort(op);
		return pad;
	}

	public ControlPad createControlPad(Datapath dp, String name, int bitwidth) {
		/// FIXME : Constructor must manage bitwidth  
		ControlPad pad = super.createControlPad(bitwidth);
		pad.setName(name);
		dp.getComponents().add(pad);
		InControlPort op = portFactory.createInControlPort(name, bitwidth);
		dp.getActivate().add(op);
		pad.setAssociatedPort(op);
		return pad;
	}

	public StatusPad createStatusPad(Datapath dp, String name, int bitwidth) {
		StatusPad pad = super.createStatusPad(bitwidth);
		pad.setName(name);
		dp.getComponents().add(pad);
		OutControlPort op = portFactory.createOutControlPort(name, bitwidth);
		dp.getFlags().add(op);
		pad.setAssociatedPort(op);
		return pad;
	}

	public DataInputPad createInputDataPad(String name, int bitwidth) {
		DataInputPad pad = createDataInputPad(dp,name,bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}
	
	public DataInputPad createDataInputPad(int bitwidth) {
		DataInputPad pad = super.createDataInputPad(bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}
	
	public DataOutputPad createDataOutputPad(int bitwidth) {
		DataOutputPad pad = super.createDataOutputPad(defaultLabel(),bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}
	
	public StatusPad createStatusPad(int bitwidth) {
		StatusPad pad = super.createStatusPad(bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}

	public ControlPad createControlPad(Datapath dp) {
		ControlPad pad = createControlPad(dp,defaultLabel(), 1);
		return pad;
	}

	public DataInputPad createDataInputPad(Datapath dp) {
		return createDataInputPad(dp,defaultLabel(), defaultOperatorBitwidth);
	}
	
	public DataOutputPad createDataOutputPad(Datapath dp) {
		return createDataOutputPad(dp,defaultLabel(), defaultOperatorBitwidth);
	}

	public DataOutputPad createOutputDataPad(String name, int bitwidth) {
		DataOutputPad pad = createDataOutputPad(dp,name,bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}

	public StatusPad createOutputControlPad(String name, int bitwidth) {
		StatusPad pad = createStatusPad(dp,name,bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}

	public ControlPad createInputControlPad(String name, int bitwidth) {
		ControlPad pad = createControlPad(dp,name,bitwidth);
		dp.getComponents().add(pad);
		return pad;
	}

	
	public DataOutputPad createAndConnectPad(SingleOutputDataFlowBlock block, String name) {
		return createAndConnectPad(block.getOutput(), name);
	}
//
//	public DataInputPad createAndConnectPad(SingleInputDataFlowBlock block, String name) {
//		return createAndConnectPad(block.getInput(), name);
//	}

	public DataOutputPad createAndConnectPad(OutDataPort op, String name) {
		DataOutputPad pad = createOutputDataPad(name, op.getWidth());
		wireFactory.connect(pad.getInput(), op);
		return pad;
	}
//
//	public DataInputPad createAndConnectPad(InDataPort ip, String name) {
//		DataInputPad pad = createInputDataPad(name, ip.getWidth());
//		wireFactory.connect(ip, pad.getOutput());
//		return pad;
//	}

}
