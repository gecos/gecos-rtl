package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.impl.PortFactoryImpl;


public class UserPortFactory extends PortFactoryImpl {

	private static int defaultPortBitwidth = 32;
	private static UserPortFactory instance =null; 
	
	static public UserPortFactory getFactory() {
		if (instance==null) instance = new UserPortFactory();
		return instance;
	}
	
	public UserPortFactory() {
		super();
	}
	
	public UserPortFactory(int defaultwidth) {
		super();
		defaultPortBitwidth=defaultwidth;
	}
	

	public DataFlowBlock addInDataPort(DataFlowBlock block, String string, int bitwidth) {
		InDataPort ip = createInDataPort();
		ip.setName(string);
		ip.setWidth(bitwidth);
		block.getIn().add(ip);
		return block;
	}

	public DataFlowBlock addOutDataPort(DataFlowBlock block, String string, int bitwidth) {
		OutDataPort ip = createOutDataPort();
		ip.setName(string);
		ip.setWidth(bitwidth);
		block.getOut().add(ip);
		return block;
	}

	public InControlPort createInControlPort(String name, int width) {
		InControlPort cp = createInControlPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}

	public OutControlPort createOutControlPort(String name, int width) {
		OutControlPort cp = createOutControlPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}

	public ActivableBlock addInControlPort(ActivableBlock block, String string, int bitwidth) {
		InControlPort ip = createInControlPort(string, bitwidth);
		block.getActivate().add(ip);
		return block;
	}

	public ActivableBlock addInBooleanControlPort(ActivableBlock block, String string) {
		return addInControlPort(block, string,1);
	}

	public OutControlPort createstratusPortOutControl(String name, int width) {
		OutControlPort sp = createOutControlPort();
		sp.setName(name);
		sp.setWidth(width);
		return sp;
	}

	public FlagBearerBlock addStatusPort(FlagBearerBlock block, String string, int bitwidth) {
		OutControlPort ip = createstratusPortOutControl(string,bitwidth);
		block.getFlags().add(ip);
		return block;
	}

	public FlagBearerBlock addBooleanStatusPort(FlagBearerBlock block, String string) {
		return addStatusPort(block, string,1);
	}


}
