package fr.irisa.cairn.model.datapath.transforms;

import static fr.irisa.cairn.model.datapath.analysis.DataFlowUtils.getPrecedingNode;

import java.util.HashMap;
import java.util.Map;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.analysis.ControlFlowUtils;
import fr.irisa.cairn.model.datapath.analysis.DataFlowUtils;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.InputPad;
import fr.irisa.cairn.model.datapath.pads.OutputPad;
import fr.irisa.cairn.model.datapath.pads.util.PadsSwitch;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SyncReadMemory;
import fr.irisa.cairn.model.datapath.storage.util.StorageSwitch;

public class CriticalPathDataBuilder extends OperatorsSwitch<Integer> {

	private Map<AbstractBlock, Integer> map;
	private Datapath datapath;
	private PadDelayProvider padDelayProvider;
	private OperatorDelayProvider operatorDelayProvider;
	private StorageDelayProvider storageDelayProvider;
	
	@Deprecated //This method must be checked
	private int log2(int value) {
		return (int)Math.log(2.0/Math.E*value);
	}

	public CriticalPathDataBuilder(Datapath datapath) {
		map = new HashMap<AbstractBlock, Integer>();
		this.datapath=datapath;
		padDelayProvider = new PadDelayProvider();
		operatorDelayProvider = new OperatorDelayProvider();
		storageDelayProvider = new StorageDelayProvider();
	}

	public void buildDelayMap() {
		for (AbstractBlock blck : datapath.getComponents()) {
			computeDelay(blck);
		}
	}
	
	private int getMaxInputDelay(AbstractBlock blk) {
		int res =0;
		if(blk instanceof DataFlowBlock) {
			DataFlowBlock ablkc = (DataFlowBlock) blk;
			for(InDataPort ip : ablkc.getIn()) {
				res = Math.max(res,computeDelay(DataFlowUtils.getPrecedingNode(ip)));
			}
		}
		if(blk instanceof ActivableBlock) {
			ActivableBlock ablkc = (ActivableBlock) blk;
			for(InControlPort ip : ablkc.getActivate()) {
				res = Math.max(res,computeDelay(ControlFlowUtils.getPrecedingNode(ip)));
			}
		}
		return res;
	}
	
	
	private int computeDelay(AbstractBlock parentNode) {
		if(map.containsKey(parentNode)) {
			return map.get(parentNode);
		} else {
			Integer delay = operatorDelayProvider.doSwitch(parentNode);
			if (delay==null) {
				delay = storageDelayProvider.doSwitch(parentNode);
			}
			if (delay==null) {
				delay = padDelayProvider.doSwitch(parentNode);
			}
			if (delay==null) {
				throw new UnsupportedOperationException();
			}
			map.put(parentNode,delay);
			return delay;
		}
	}

	private class PadDelayProvider extends PadsSwitch<Integer> {

		@Override
		public Integer caseDataInputPad(DataInputPad object) {
			return 0;
		}

		@Override
		public Integer caseControlPad(ControlPad object) {
			return 0;
		}

		@Override
		public Integer caseInputPad(InputPad object) {
			return 0;
		}

		@Override
		public Integer caseOutputPad(OutputPad object) {
			return 0;
		}
		
	}
	private class StorageDelayProvider extends StorageSwitch<Integer> {

		private static final int ClkToOutput = 1;

		@Override
		public Integer caseRegister(Register object) {
			getMaxInputDelay(object);
			return ClkToOutput;
		}

		@Override
		public Integer caseCERegister(CERegister object) {
			getMaxInputDelay(object);
			return ClkToOutput;
		}

		@Override
		public Integer caseShiftRegister(ShiftRegister object) {
			getMaxInputDelay(object);
			return ClkToOutput;
		}

		@Override
		public Integer caseAbstractMemory(AbstractMemory object) {
			if(object instanceof SyncReadMemory) {
				getMaxInputDelay(object);
				return ClkToOutput;
			} else {
				return ClkToOutput + getMaxInputDelay(object);
			}
		}

		
	}
	private class OperatorDelayProvider extends OperatorsSwitch<Integer> {

		
		public Integer caseUnaryOperator(UnaryOperator object) {
			int delay = getMaxInputDelay(object);

			switch (object.getOpcode()) {
			
			case IDENTITY:
				return delay;

			case INV:
				return delay+object.getInput().getWidth()*object.getInput().getWidth();

			case NEG:
				return delay+object.getInput().getWidth();

			case NOT:
				return delay +1;

			default:
				throw new UnsupportedOperationException();
			} 
		}


		@Override
		public Integer caseBinaryOperator(BinaryOperator object) {
			
			InDataPort ip0 = object.getInput(0);
			InDataPort ip1 = object.getInput(1);
			int res =  getMaxInputDelay(object);

			switch (object.getOpcode()) {
			case ADD:
			case ADDU:
				return res+Math.max(ip0.getWidth(),ip1.getWidth());

			case MUL:
			case MULU:
				return res+ip0.getWidth()+ip1.getWidth();

			case SHL:
			case SHR:
				if(getPrecedingNode(ip1) instanceof ConstantValue) {
					// Constant shift
					return res;
				} else {
					// Barrel shifter
					return (int) log2(ip1.getWidth());
				}

			case MAX:
			case MAXU:
			case MIN:
			case MINU:
				// We assume a tres base comparator implementation
				return  res+ (int)log2(Math.max(ip0.getWidth(),ip1.getWidth()));

			case OR:
			case XOR:
			case AND:
				return 1+res;

			default:
				break;
			}
			
			return super.caseBinaryOperator(object);
		}

		@Override
		public Integer caseTernaryOperator(TernaryOperator object) {
			
			InDataPort ip0 = object.getInput(0);
			InDataPort ip1 = object.getInput(1);
			InDataPort ip2 = object.getInput(2);

			int res =  getMaxInputDelay(object);

			switch (object.getOpcode()) {
			case MULADD:
				return res+ip0.getWidth()+ip1.getWidth();
			case ADDC:
				return res+Math.max(ip0.getWidth(),ip1.getWidth());
			default:
				break;
			}
			return super.caseTernaryOperator(object);
		}

		@Override
		public Integer caseConstantValue(ConstantValue object) {
			return 0;
		}

		@Override
		public Integer caseDataFlowMux(DataFlowMux object) {
			return 1+getMaxInputDelay(object);
		}

		@Override
		public Integer caseControlFlowMux(ControlFlowMux object) {
			return 1+getMaxInputDelay(object);
		}

		@Override
		public Integer caseReductionOperator(ReductionOperator object) {
			int maxInputDelay = getMaxInputDelay(object);
			switch (object.getOpcode()) {
			case MAX:
				// TODO : we should visit all nodes
				return maxInputDelay+log2(object.getIn().get(0).getWidth());
			case SIGMA:
				// TODO : we should visit all nodes
				return maxInputDelay+log2(object.getIn().get(0).getWidth());
			case PROD:
				// TODO : we should visit all nodes
				//return maxInputDelay+log2(object.getIn().get(0));
				throw new UnsupportedOperationException();
			default:
				break;
			}
			return 1+maxInputDelay;
		}



		@Override
		public Integer caseBitSelect(BitSelect object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseCompare(Compare object) {
			InDataPort ip0 = object.getInput(0);
			InDataPort ip1 = object.getInput(1);
			int res = getMaxInputDelay(object);
			return res+Math.max(ip0.getWidth(), ip1.getWidth());
		}

		@Override
		public Integer caseMerge(Merge object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseQuantize(Quantize object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseExpandUnsigned(ExpandUnsigned object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseExpandSigned(ExpandSigned object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseCtrl2DataBuffer(Ctrl2DataBuffer object) {
			return getMaxInputDelay(object);
		}

		@Override
		public Integer caseData2CtrlBuffer(Data2CtrlBuffer object) {
			return getMaxInputDelay(object);
		}
		
		
	}

}
