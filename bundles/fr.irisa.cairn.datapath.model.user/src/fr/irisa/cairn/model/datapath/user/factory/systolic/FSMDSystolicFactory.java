package fr.irisa.cairn.model.datapath.user.factory.systolic;

import static fr.irisa.cairn.model.datapath.user.factory.UserOperatorFactory.cMux;

import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.user.FSMDRegisterFactory;

public class FSMDSystolicFactory {
	
	public static CERegister[] inputPipeline(String name, SingleOutputDataFlowBlock input, SingleFlagBlock shift, int size) {
		CERegister regs[] = new CERegister[size];
		for (int i=0;i<size;i++) {
			if(i==0) {
				regs[i]=FSMDRegisterFactory.cereg(name+i,input,shift);
			} else {
				regs[i]=FSMDRegisterFactory.cereg(name+i,regs[i-1],shift);
			}
		}
		return regs;
	}

	public static CERegister outputPipeline(String name, SingleOutputDataFlowBlock[] input, SingleFlagBlock shift, SingleFlagBlock CE, int size) {
		CERegister regs[] = new CERegister[size];
		for (int i=0;i<size;i++) {
			if(i<(size-1)) {
				regs[i]=FSMDRegisterFactory.cereg(name+i,cMux(input[i],regs[i+1],shift),CE);
			} else {
				regs[i]=FSMDRegisterFactory.cereg(name+i,input[i],CE);
			}
		}
		return regs[0];
	}

}
