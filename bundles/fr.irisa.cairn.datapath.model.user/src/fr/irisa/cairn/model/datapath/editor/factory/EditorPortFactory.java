package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.impl.PortFactoryImpl;


public class EditorPortFactory extends PortFactoryImpl {

	private static int portId =0;

	private String defaultLabel() {
		return "pad"+(portId ++);
	}


	@Override
	public InControlPort createInControlPort() {
		return createInControlPort(defaultLabel(),defaultPortBitwidth);
	}

	@Override
	public InDataPort createInDataPort() {
		return createInDataPort(defaultLabel(),defaultPortBitwidth);
	}

	@Override
	public OutControlPort createOutControlPort() {
		return createOutControlPort(defaultLabel(),defaultPortBitwidth);
	}

	@Override
	public OutDataPort createOutDataPort() {
		return createOutDataPort(defaultLabel(),defaultPortBitwidth);
	}

	private static int defaultPortBitwidth = 32;
	private static EditorPortFactory instance =null; 
	
	static public EditorPortFactory getFactory() {
		if (instance==null) instance = new EditorPortFactory();
		return instance;
	}
	
	public EditorPortFactory() {
		super();
	}
	
	public EditorPortFactory(int defaultwidth) {
		super();
		defaultPortBitwidth=defaultwidth;
	}
	

	
	public InDataPort createInDataPort(String name, int width) {
		InDataPort cp = super.createInDataPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}

	public InDataPort createInDataPort(String name) {
		return createInDataPort(name,defaultPortBitwidth);
	}

	public OutDataPort createOutDataPort(String name, int width) {
		OutDataPort cp = super.createOutDataPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}

	
	public InControlPort createInControlPort(String name, int width) {
		InControlPort cp = super.createInControlPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}

	public OutControlPort createOutControlPort(String name, int width) {
		OutControlPort cp = super.createOutControlPort();
		cp.setName(name);
		cp.setWidth(width);
		return cp;
	}


	

	public void addOutDataPort(DataFlowBlock block, String string, int bitwidth) {
		block.getOut().add(createOutDataPort(string, bitwidth));
	}


	public void addInDataPort(DataFlowBlock block, String string, int bitwidth) {
		block.getIn().add(createInDataPort(string, bitwidth));
	}


	public void addInControlPort(ActivableBlock block, String string, int bitwidth) {
		block.getActivate().add(createInControlPort(string, bitwidth));
	}
	
	public void addOutControlPort(FlagBearerBlock block, String string, int bitwidth) {
		block.getFlags().add(createOutControlPort(string, bitwidth));
		
	}


	public void addInControlPort(Ctrl2DataBuffer op, String string, int bitwidth) {
		op.getActivate().add(createInControlPort(string, bitwidth));
		
	}


	public void addOutDataPort(Ctrl2DataBuffer op, String string, int bitwidth) {
		op.getOut().add(createOutDataPort(string, bitwidth));		
	}


	public void addInDataPort(Data2CtrlBuffer op, String string, int bitwidth) {
		op.getIn().add(createInDataPort(string, bitwidth));
		
	}
	
	public void addOutControlPort(Data2CtrlBuffer op, String string, int bitwidth) {
		op.getFlags().add(createOutControlPort(string, bitwidth));
		
	}
 


}
