package fr.irisa.cairn.model.datapath.user;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanValue;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FsmFactory;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;

public class FSMDFSMFactory {
	
	public static FSM fsm(String name, InControlPort[] icps, OutControlPort[] ocps, State[] states) {
		FSM res = FsmFactory.eINSTANCE.createFSM();
		FSMDFactory.getCurrentDatapath().getComponents().add(res);
		res.setName(name);
		boolean first=true;
		for (State state : states) {
			res.getStates().add(state);
			if(first) res.setStart(state);
			first = false; 
		}
		for (InControlPort icp: icps) {
			res.getActivate().add(icp);
		}
		for (OutControlPort ocp: ocps) {
			res.getFlags().add(ocp);
		}
		return res;
	}

	public static State[] states(State... states) {
		return states;
	}

	public static InControlPort[] icps(InControlPort... icps) {
		return icps;
	}

	public static OutControlPort[] ocps(OutControlPort... ocps) {
		return ocps;
	}

	public static State state(String label, AbstractCommandValue[] commands, Transition[] transitions) {
		State res = state(label);
		for (Transition transition : transitions) {
			res.getTransitions().add(transition);
		}
		for (AbstractCommandValue command : commands) {
			res.getActivatedCommands().add(command);
		}
		return res;
	}

	public static State branchState(String label, State truecond, State falsecond,  AbstractBooleanExpression predicate) {
		EcoreUtil.Copier copier = new Copier();
		AbstractBooleanExpression copy=  (AbstractBooleanExpression) copier.copy(predicate);
		State res = state(label);
		res.getTransitions().add(transition(truecond,predicate));
		res.getTransitions().add(transition(truecond,neg(copy)));
		return res;
	}

	public static void branch(State start, State truecond, State falsecond,  AbstractBooleanExpression predicate) {
		EcoreUtil.Copier copier = new Copier();
		AbstractBooleanExpression copy=  (AbstractBooleanExpression) copier.copy(predicate);
		copier.copyReferences();
		start.getTransitions().add(transition(truecond,predicate));
		start.getTransitions().add(transition(falsecond,neg(copy)));
	}

	public static State proceedState(String label, State next) {
		State res = state(label);
		res.getTransitions().add(transition(next,trueconst()));
		return res;
	}

	public static State proceedState(String label, State next, AbstractCommandValue... commands) {
		State res = proceedState(label, next);
		for (AbstractCommandValue command : commands) {
			res.getActivatedCommands().add(command);
		}
		return res;
	}

	public static State state(String label,  Transition[] transitions) {
		State res = state(label);
		for (Transition transition : transitions) {
			res.getTransitions().add(transition);
		}
		return res;
	}
    
	/**
	 * Creates a new FSM State, with its name and associated commands passed as parameters
	 * @param label 
	 * @param commands
	 * @return newly created FSM State
	 */
	public static State state(String label, AbstractCommandValue... commands) {
		State res = state(label);
		for (AbstractCommandValue command : commands) {
			res.getActivatedCommands().add(command);
		}
		return res;
	}

	/**
	 * Create a sequential execution flow between all the states passed as parameters 
	 * @param enumerated array of states 
	 * @return array of linked states
	 */
	public static State[] sequence(State... list) {
		State prev= null;
		for (State state: list) {
			if(prev!=null) {
				proceed(prev, state);
			} 
			prev=state;
		}
		return list;
	}

	
	/**
	 * Creates a new FSM State, with its name and associated commands passed as parameters
	 * @param label 
	 * @param commands
	 * @return newly created FSM State
	 */
	public static State state(String label) {
		State res = FsmFactory.eINSTANCE.createState();
		res.setLabel(label);
		return res;
	}

	public static AbstractCommandValue[] commands(AbstractCommandValue... commands) {
		return commands;
	}

	public static Transition transition(State next, AbstractBooleanExpression predicate) {
		Transition res = FsmFactory.eINSTANCE.createTransition();
		res.setDst(next);
		res.setPredicate(predicate);
		return res;
	}

	public static Transition proceed(State src, State next) {
		Transition res = FsmFactory.eINSTANCE.createTransition();
		res.setDst(next);
		res.setSrc(src);
		res.setPredicate(trueconst());
		return res;
	}

	private static BooleanConstant trueconst() {
		BooleanConstant createBooleanConstant = FsmFactory.eINSTANCE.createBooleanConstant();
		createBooleanConstant.setValue(true);
		return createBooleanConstant;
	}

	public static IntegerCommandValue intCommand(AbstractBooleanExpression predicate, OutControlPort port,int value) {
		IntegerCommandValue res = FsmFactory.eINSTANCE.createIntegerCommandValue();
		res.setPredicate(predicate);
		res.setCommand(port);
		res.setValue(value);
		return res;
	}
	
	public static IntegerCommandValue intCommand(OutControlPort port, int value) {
		return intCommand(trueconst(),port, value);
	}
	
	public static BooleanCommandValue boolCommand(AbstractBooleanExpression predicate, OutControlPort port, boolean value) {
		BooleanCommandValue res = FsmFactory.eINSTANCE.createBooleanCommandValue();
		res.setPredicate(predicate);
		res.setCommand(port);
		res.setValue(value?BooleanValue.ONE:BooleanValue.ZERO);
		return res;
	}
	
	
	public static BooleanCommandValue boolCommand(OutControlPort port, boolean value) {
		return boolCommand(trueconst(),port, value);
	}
	

	public static OrExpression or(AbstractBooleanExpression... pps) {
		OrExpression sop = FsmFactory.eINSTANCE.createOrExpression();
		for (AbstractBooleanExpression productPredicate : pps) {
			sop.getTerms().add(productPredicate);
		}
		return sop;
	}

	public static AndExpression and(AbstractBooleanExpression... pts) {
		AndExpression pp = FsmFactory.eINSTANCE.createAndExpression();
		for (AbstractBooleanExpression predicateTerm : pts) {
				pp.getTerms().add(predicateTerm);
		}
		return pp;
	}
	
	public static BooleanFlagTerm T(InControlPort ip) {
		if (ip.getWidth()!=1) {
			throw new UnsupportedOperationException("Cannot create BooleanFlagTerm on InControlPort who's width is not 1");
		}  else {
			BooleanFlagTerm term= FsmFactory.eINSTANCE.createBooleanFlagTerm();
			term.setFlag(ip);
			term.setNegated(false);
			return term;
		}
	}

	public static BooleanFlagTerm nT(InControlPort ip) {
		BooleanFlagTerm term= T(ip);
		term.setNegated(true);
		return term;
	}
	
	public static NegateExpression neg(AbstractBooleanExpression ip) {
		NegateExpression neg= FsmFactory.eINSTANCE.createNegateExpression();
		if (ip.eContainer()!=null) {
			EcoreUtil.Copier copier= new EcoreUtil.Copier();
			ip = (AbstractBooleanExpression) copier.copy(ip);
			copier.copyReferences();
		}
		neg.setTerm(ip);
		return neg;
	}

	public static IndexedBooleanFlagTerm T(InControlPort ip, int offset) {
		if (offset>=ip.getWidth()) {
			throw new UnsupportedOperationException("Illegal offset value for "+ip.getName()+"["+offset+"] since "+ip.getName()+" has only "+ip.getWidth()+" bits");
		}  else {
			IndexedBooleanFlagTerm term= FsmFactory.eINSTANCE.createIndexedBooleanFlagTerm();
			term.setFlag(ip);
			term.setNegated(false);
			term.setOffset(offset);
			return term;
		}
	}

	public static IndexedBooleanFlagTerm nT(InControlPort ip, int offset) {
		IndexedBooleanFlagTerm term= T(ip, offset);
		term.setNegated(true);
		return term;
	}

	public static IntegerFlagTerm NEQ(InControlPort ip, int value) {
		IntegerFlagTerm term= EQU(ip,value);
		term.setEqual(false);
		return term;
	}

	public static IntegerFlagTerm EQU(InControlPort ip, int value) {
		if ((1<<ip.getWidth())<value) {
			throw new UnsupportedOperationException("Value "+value+" is out of range for "+ip.getName()+"");
		}  else {
			IntegerFlagTerm term= FsmFactory.eINSTANCE.createIntegerFlagTerm();
			term.setFlag(ip);
			term.setEqual(true);
			term.setValue(value);
			return term;
		}
	}


}
