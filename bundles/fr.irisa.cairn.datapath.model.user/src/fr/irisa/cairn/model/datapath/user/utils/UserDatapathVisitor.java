package fr.irisa.cairn.model.datapath.user.utils;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.OperatorsPackage;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.StoragePackage;
import fr.irisa.cairn.model.datapath.util.DatapathSwitch;

public class UserDatapathVisitor<T> extends DatapathSwitch<T>{
	
	private UserOperatorSwitch<T> opSwitch;
	private UserStorageSwitch<T> storageSwitch;
	//private UserFSMSwitch<T> fsmSwitch;
	
	public T defaultCase(EObject object) {
		String nsuri =object.eClass().getEPackage().getNsURI(); 
		if (nsuri.equals(StoragePackage.eNS_URI)) {
			return storageSwitch.doSwitch(object);
		} else if (nsuri.equals(OperatorsPackage.eNS_URI)) {
			return opSwitch.doSwitch(object);
		} else  {
			throw new UnsupportedOperationException("packkage " +nsuri+" is not supported by "+this.getClass().getSimpleName());
		} 
	}

	public T caseCERegister(CERegister object) {
		return null;
	}

	public T caseDualPortRam(DualPortRam object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseMultiPortRam(MultiPortRam object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseMultiPortRom(MultiPortRom object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseRegister(Register object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseShiftRegister(ShiftRegister object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseSinglePortRam(SinglePortRam object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseSinglePortRom(SinglePortRom object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseBinaryOperator(BinaryOperator object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseBitSelect(BitSelect object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseCompare(Compare object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseConstantValue(ConstantValue object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseControlFlowMux(ControlFlowMux object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseDataFlowMux(DataFlowMux object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseExpandSigned(ExpandSigned object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseExpandUnsigned(ExpandUnsigned object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseMerge(Merge object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseQuantize(Quantize object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseReductionOperator(ReductionOperator object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseTernaryOperator(TernaryOperator object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseUnaryOperator(UnaryOperator object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseDataInputPad(DataInputPad object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseDataOutputPad(DataOutputPad object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseControlPad(ControlPad object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseInControlPort(InControlPort object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseInDataPort(InDataPort object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseOutControlPort(OutControlPort object) {
		// TODO Auto-generated method stub
		return null;
	}

	public T caseOutDataPort(OutDataPort object) {
		// TODO Auto-generated method stub
		return null;
	}


	
}
