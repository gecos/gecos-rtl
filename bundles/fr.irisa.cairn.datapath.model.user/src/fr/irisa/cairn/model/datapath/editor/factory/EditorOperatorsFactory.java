package fr.irisa.cairn.model.datapath.editor.factory;

import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.TernaryOpcode;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.impl.OperatorsFactoryImpl;


public class EditorOperatorsFactory extends OperatorsFactoryImpl{



	
	private static final int defaultOperatorBitwidth = 32;
	
	private static EditorOperatorsFactory instance =null; 

	private EditorPortFactory portFactory;
	private EditorWiresFactory wireFactory;
	
	static public EditorOperatorsFactory getFactory() {
		if (instance==null) {
			instance = new EditorOperatorsFactory();
		}
		return instance;
	}
	
	static int labelId = 0;
	
	private String defaultLabel() {
		return "op"+(labelId++);
	}

	
	public EditorOperatorsFactory() {
		super();
		portFactory = EditorPortFactory.getFactory();
		wireFactory = EditorWiresFactory.getFactory();
	}
	
	@Override
	public UnaryOperator createUnaryOperator() {
		return createUnaryOperator(defaultLabel(),defaultOperatorBitwidth,UnaryOpcode.NOT);
	}

	public UnaryOperator createUnaryOperator(String label, int bitwidth, UnaryOpcode opcode) {
		UnaryOperator op = super.createUnaryOperator();
		op.setName(label);
		op.setOpcode(opcode);
		portFactory.addInDataPort(op, "I",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}
	
	public UnaryOperator createCtr(String label, int bitwidth, UnaryOpcode opcode) {
		UnaryOperator op = super.createUnaryOperator();
		op.setName(label);
		op.setOpcode(opcode);
		portFactory.addInDataPort(op, "I",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}

	@Override
	public Ctrl2DataBuffer createCtrl2DataBuffer() {
		return createCtrl2DataBuffer(defaultLabel(),defaultOperatorBitwidth);
	}


	@Override
	public Data2CtrlBuffer createData2CtrlBuffer() {
		return createData2CtrlBuffer(defaultLabel(),defaultOperatorBitwidth);
	}


	public BinaryOperator createBinaryOperator(String label, int bitwidth, BinaryOpcode opcode) {
		BinaryOperator op = super.createBinaryOperator();
		op.setName(label);
		op.setOpcode(opcode);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}
	
	
	public BinaryOperator createBinaryOperator(String label, BinaryOpcode opcode) {
		return createBinaryOperator(label, defaultOperatorBitwidth, opcode);
	}

	public BinaryOperator createBinaryOperator(BinaryOpcode opcode) {
		return createBinaryOperator(defaultLabel(),defaultOperatorBitwidth, opcode);
	}

	public BinaryOperator createBinaryOperator() {
		return createBinaryOperator(BinaryOpcode.ADD);
	}

	
	public BitSelect createBitSelect() {
		return createBitSelect(defaultLabel(), defaultOperatorBitwidth,0,defaultOperatorBitwidth-1);
	}


	public BitSelect createBitSelect(String string, int width,int lb, int ub) {
		BitSelect op = super.createBitSelect();
		op.setUpperBound(ub);
		op.setLowerBound(lb);
		op.setName(string);
		portFactory.addInDataPort(op, "I",width);
		portFactory.addOutDataPort(op, "O",ub-lb+1);
		return op;
	}

	
	public BitSelect createBitSelect(String string, int lb, int ub) {
		return createBitSelect(string, defaultOperatorBitwidth,lb,ub);
	}


	public BitSelect createBitSelect(int lb, int ub) {
		return createBitSelect(defaultLabel(), defaultOperatorBitwidth,lb,ub );
	}

	public BitSelect createBitSelect(int bitwidth,int lb, int ub) {
		return createBitSelect(defaultLabel(), bitwidth,lb,ub);
	}

	public static int bitwidthForValue(int d) {
	      return (int)(Math.ceil(Math.log(d)/Math.log(2.0))+1);
	}


	public ConstantValue createConstantValue(String name, int value, int bitwidth) {
		if (bitwidthForValue(value)>bitwidth) throw new UnsupportedOperationException("Constant Node value "+value+" cannot be encoded oon "+ bitwidth +" bits");
		ConstantValue op = super.createConstantValue();
		portFactory.addOutDataPort(op, "O",bitwidth);
		op.setName(name);
		op.setValue(value);
		return op;
	}

	public ConstantValue createConstantValue(String name, int value) {
		return createConstantValue(name, value, bitwidthForValue(value));
	}

	public ConstantValue createConstantValue(int value) {
		return createConstantValue(defaultLabel(), value, bitwidthForValue(value));
	}

	public ConstantValue createConstantValue() {
		return createConstantValue(1);
	}

	public TernaryOperator createTernaryOperator(String label, int bitwidth, TernaryOpcode opcode) {
		TernaryOperator op = super.createTernaryOperator();
		op.setOpcode(opcode);
		op.setName(label);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addInDataPort(op, "I2",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}
	
	public TernaryOperator createTernaryOperator(int bitwidth, TernaryOpcode opcode) {
			return createTernaryOperator(defaultLabel(),bitwidth,opcode);
	}	

	public TernaryOperator createTernaryOperator(TernaryOpcode opcode) {
		return createTernaryOperator(defaultLabel(),defaultOperatorBitwidth,opcode);
	}	

	public TernaryOperator createTernaryOperator() {
		return createTernaryOperator(defaultLabel(),defaultOperatorBitwidth,TernaryOpcode.MULADD);
	}	

	public Compare createCompare(String label) {
		return createCompare(label,defaultOperatorBitwidth);
	}

	public Compare createCompare(int bitwidth) {
		return createCompare(defaultLabel(),bitwidth);
	}

	public Compare createCompare() {
		return createCompare(defaultLabel(),defaultOperatorBitwidth);
	}

	public Compare createCompare(String label, int bitwidth) {
		Compare op = super.createCompare();
		op.setOpcode(CompareOpcode.EQU);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addOutDataPort(op, "O",1);
		return op;
	}
	
	public Compare createCompare(String label, int bitwidth, CompareOpcode opcode) {
		Compare op = super.createCompare();
		op.setOpcode(opcode);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addOutDataPort(op, "O",1);
		return op;
	}

	public Merge createMerge(String label) {
		return createMerge(label,defaultOperatorBitwidth,defaultOperatorBitwidth);
	}

	public Merge createMerge(int bitwidth0,int bitwidth1) {
		return createMerge(defaultLabel(),bitwidth0,bitwidth1);
	}

	public Merge createMerge() {
		return createMerge(defaultLabel(),defaultOperatorBitwidth,defaultOperatorBitwidth);
	}

	public Merge createMerge(String label, int bitwidth0,int bitwidth1) {
		Merge op = super.createMerge();
		portFactory.addInDataPort(op, "I0",bitwidth0);
		portFactory.addInDataPort(op, "I1",bitwidth1);
		portFactory.addOutDataPort(op, "O",bitwidth0+bitwidth1);
		return op;
	}


	public ControlFlowMux createControlFlowMux() {
		return createControlFlowMux(defaultLabel(),defaultOperatorBitwidth);
		
	}

	public ControlFlowMux createControlFlowMux(String label, int bitwidth) {
		ControlFlowMux op = super.createControlFlowMux();
		op.setName(label);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInControlPort(op, "S",1);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}
	
	public Ctrl2DataBuffer createCtrl2DataBuffer(String label, int bitwidth) {
		Ctrl2DataBuffer op = super.createCtrl2DataBuffer();
		op.setName(label);
		portFactory.addInControlPort(op, "I",bitwidth);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}
	
	public Data2CtrlBuffer createData2CtrlBuffer(String label, int bitwidth) {
		Data2CtrlBuffer op = super.createData2CtrlBuffer();
		op.setName(label);
		portFactory.addInDataPort(op, "I",bitwidth);
		portFactory.addOutControlPort(op, "O",bitwidth);
		return op;
	}


	public DataFlowMux createDataFlowMux() {
		return createDataFlowMux(defaultLabel(),defaultOperatorBitwidth);
	}

	public DataFlowMux createDataFlowMux(String label, int bitwidth) {
		DataFlowMux op = super.createDataFlowMux();
		op.setName(label);
		portFactory.addInDataPort(op, "I0",bitwidth);
		portFactory.addInDataPort(op, "I1",bitwidth);
		portFactory.addInDataPort(op, "S",1);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}



	public ExpandSigned createExpandSigned() {
		// TODO Auto-generated method stub
		return super.createExpandSigned();
	}


	public ExpandUnsigned createExpandUnsigned() {
		// TODO Auto-generated method stub
		return super.createExpandUnsigned();
	}


	@Override
	public Quantize createQuantize() {
		// TODO Auto-generated method stub
		return super.createQuantize();
	}


	public ReductionOperator createReductionOperator(String label, ReductionOpcode opcode) {
		return createReductionOperator(label,opcode,defaultOperatorBitwidth);
	}

	public ReductionOperator createReductionOperator(int bitwidth) {
		return createReductionOperator(defaultLabel(),ReductionOpcode.SIGMA,bitwidth);
	}

	public ReductionOperator createReductionOperator(String label) {
		return createReductionOperator(label,ReductionOpcode.SIGMA,defaultOperatorBitwidth);
	}

	public ReductionOperator createReductionOperator(ReductionOpcode opcode) {
		return createReductionOperator(defaultLabel(),opcode,defaultOperatorBitwidth);
	}

	public ReductionOperator createReductionOperator(String label, ReductionOpcode opcode, int bitwidth) {
		ReductionOperator op = super.createReductionOperator();
		op.setOpcode(opcode);
		op.setName(label);
		portFactory.addOutDataPort(op, "O",bitwidth);
		return op;
	}



}
