package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.editor.factory.EditorStorageFactory;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;


public class UserStorageFactory extends EditorStorageFactory {

//	@Deprecated
//	public DualPortRam createDualPortRam(String label, int bitwidth, int depth) {
//		DualPortRam  mem = super.createDualPortRam(label, bitwidth, depth);
//		dp.getComponents().add(mem);
//		return mem;
//	}


	public SinglePortRam createSyncSPRam(String label, int bitwidth, int depth) {
		SinglePortRam  mem = super.createSinglePortRam(label, bitwidth, depth);
		dp.getComponents().add(mem);
		return mem;
	}
	
	public SinglePortRom createSinglePortRom(String label, int bitwidth, int depth) {
		SinglePortRom  mem = super.createSinglePortRom(label, bitwidth, depth);
		dp.getComponents().add(mem);
		return mem;
	}



	public MultiPortRam createDualPortRam(String label, int bitwidth, int depth) {
		MultiPortRam mem = super.createMultiPortRam();
		mem.setName(label);
//		mem.setContent(new byte[depth*((int)Math.ceil(bitwidth/2))]);
		for(int i=0;i<2;i++) {
			super.portFactory.addInDataPort(mem,"address_"+i,bitwidthForValue(depth));
			super.portFactory.addInDataPort(mem,"dataIn_"+i,bitwidth);
			super.portFactory.addInControlPort(mem,"re_"+i,1);
			super.portFactory.addInControlPort(mem,"we_"+i,1);
			super.portFactory.addOutDataPort(mem,"dataOut_"+i,bitwidth);
		}
		return mem;
	}


	public SinglePortRam createSinglePortRam() {
		SinglePortRam  mem = super.createSinglePortRam();
		dp.getComponents().add(mem);
		return mem;
	}


	private static UserStorageFactory instance =null; 

	private Datapath dp;
	private UserWireFactory wireFactory;

	static int labelId=0;

	private String defaultLabel() {
		return "reg_"+(labelId++);
	}

	
	static public UserStorageFactory getFactory(Datapath dp) {
		if (instance==null) {
			instance = new UserStorageFactory(dp);
		} else {
			if (instance.dp!=dp) {
				System.out.println("Changin "+instance.getClass().getSimpleName()+ "for new DP "+dp.getName());
				instance.dp=dp;
			}
		}
		return instance;

	}
	
	public UserStorageFactory(Datapath adp) {
		super();
		this.dp=adp;
		wireFactory = UserWireFactory.getInstance(dp);
	}
	

	public Register createRegister(String label, int bitwidth) {
		Register op = super.createRegister(label,bitwidth);
		dp.getComponents().add(op);
		return op;
	}

	public Register createRegister(int bitwidth) {
		return createRegister(defaultLabel(),bitwidth);
	}

	public CERegister createCERegister(String label, int bitwidth) {
		CERegister op = super.createCERegister(label, bitwidth);
		dp.getComponents().add(op);
		return op;
	}
	

	public CERegister createCERegister(int bitwidth) {
		return createCERegister(defaultLabel(),bitwidth);
	}

	public ShiftRegister createShiftRegister(String label, int bitwidth, int depth) {
		ShiftRegister op = super.createShiftRegister(label,bitwidth,depth);
		dp.getComponents().add(op);
		return op;
	}

	public ShiftRegister createShiftRegister(int bitwidth, int depth) {
		return createShiftRegister(defaultLabel(),bitwidth,depth);
	}
	
	public Register register(SingleOutputDataFlowBlock block) {
		Register reg = createRegister(block.getOutput().getWidth());
		wireFactory.connect(reg,block);
		return reg;
	}

	public CERegister registerWithEnable(SingleOutputDataFlowBlock block) {
		CERegister reg = createCERegister(block.getOutput().getWidth());
		wireFactory.connect(reg,block);
		return reg;
	}


	public Register reg(SingleOutputDataFlowBlock block) {
		return register(block);
	}

	public CERegister regce(SingleOutputDataFlowBlock block, OutControlPort ce) {
		CERegister reg = registerWithEnable(block);
		wireFactory.connect(reg.getActivate().get(0),ce);
		return reg;
	}

	public CERegister regce(OutControlPort ce) {
		CERegister reg = createCERegister();
		wireFactory.connect(reg.getActivate().get(0),ce);
		return reg;
	}

	public CERegister regce(SingleFlagBlock ce) {
		return regce(ce.getFlag(0));
	}

	public CERegister regce(SingleOutputDataFlowBlock block, SingleFlagBlock ce) {
		CERegister reg = registerWithEnable(block);
		wireFactory.connect(reg.getActivate().get(0),ce.getFlag(0));
		return reg;
	}
	
	public CERegister shiftRegisterWithEnable(SingleOutputDataFlowBlock block, int depth) {
		ShiftRegister reg = createShiftRegister(defaultLabel(),block.getOutput().getWidth(),depth);
		wireFactory.connect(reg,block);
		return reg;
	}

	public ShiftRegister shiftRegisterWithEnable(int width, int depth) {
		ShiftRegister reg = createShiftRegister(defaultLabel(),width,depth);
		return reg;
	}

	public ShiftRegister shiftRegisterWithEnable(String label,int width, int depth) {
		ShiftRegister reg = createShiftRegister(label,width,depth);
		
		return reg;
	}

	public SinglePortRam makeSinglePortRam(
			SingleOutputDataFlowBlock address, 
			SingleOutputDataFlowBlock Datain,
			OutControlPort read,
			OutControlPort write
	) {
		SinglePortRam mem = createSinglePortRam(
				defaultMemoryLabel(),
				Datain.getOutput().getWidth(),
				(int)Math.pow(2, address.getOutput().getWidth()));
		
		wireFactory.connect(mem.getInput(1),address.getOutput());
		wireFactory.connect(mem.getInput(0),Datain.getOutput());
		wireFactory.connect(mem.getActivate().get(0),read);
		wireFactory.connect(mem.getActivate().get(1),write);
		return mem;
	}



}
