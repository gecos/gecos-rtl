package fr.irisa.cairn.model.datapath.xml;

import java.io.IOException;

import fr.irisa.cairn.model.datapath.Datapath;

public class LoadDatapathModel {

	String filename;
	
	public LoadDatapathModel(String filename) {
		this.filename=filename;
	}

	public Datapath compute() throws IOException {
		DatapathXMLReader reader = new DatapathXMLReader();
		Datapath datapath = reader.load(filename);
		return datapath;
	}
}
