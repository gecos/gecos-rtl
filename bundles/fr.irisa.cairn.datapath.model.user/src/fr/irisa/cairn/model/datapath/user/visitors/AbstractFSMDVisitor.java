/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
// killroy was here
package fr.irisa.cairn.model.datapath.user.visitors;

import org.eclipse.emf.ecore.EObject;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.NamedElement;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.Wire;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.MultiOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.StatusPad;
import fr.irisa.cairn.model.datapath.pads.util.PadsSwitch;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.util.PortSwitch;
import fr.irisa.cairn.model.datapath.storage.AbstractMemory;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.DualPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRam;
import fr.irisa.cairn.model.datapath.storage.MultiPortRom;
import fr.irisa.cairn.model.datapath.storage.Register;
import fr.irisa.cairn.model.datapath.storage.ShiftRegister;
import fr.irisa.cairn.model.datapath.storage.SinglePortRam;
import fr.irisa.cairn.model.datapath.storage.SinglePortRom;
import fr.irisa.cairn.model.datapath.storage.util.StorageSwitch;
import fr.irisa.cairn.model.datapath.util.DatapathSwitch;
import fr.irisa.cairn.model.datapath.wires.ControlFlowWire;
import fr.irisa.cairn.model.datapath.wires.DataFlowWire;
import fr.irisa.cairn.model.datapath.wires.util.WiresSwitch;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanOperatorExpression;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;
import fr.irisa.cairn.model.fsm.util.FsmSwitch;

/**
 * This is a visitor which visit all objects registered in the datapath ecore metamodel
 * 
 * @see fr.irisa.cairn.model.datapath.DatapathPackage#getDatapathVisitor()
 * @model
 * @generated
 */
public class AbstractFSMDVisitor extends DatapathSwitch<Object> {
	private OperatorVisitor opVisitor;
	private PadsVisitor padsVisitor;
	private StorageVisitor storageVisitor;
	private WireVisitor wireVisitor;
	private PortsVisitor portsVisitor;
	private FSMVisitor fsmVisitor;

	public AbstractFSMDVisitor() {
		opVisitor = new OperatorVisitor(this);
		padsVisitor = new PadsVisitor(this);
		storageVisitor = new StorageVisitor(this);
		wireVisitor = new WireVisitor(this);
		portsVisitor = new PortsVisitor(this);
		fsmVisitor = new FSMVisitor(this);
	}
	
	@Override
	public Object defaultCase(EObject object) {
		
		Object res = opVisitor.doSwitch(object);
		if(res!=null) return res;
		
		res= fsmVisitor.doSwitch(object);
		if(res!=null) return res;

		res= storageVisitor.doSwitch(object);
		if(res!=null) return res;

		res= padsVisitor.doSwitch(object);
		if(res!=null) return res;
		

		res= wireVisitor.doSwitch(object);
		if(res!=null) return res;
		
		res= portsVisitor.doSwitch(object);
		if(res!=null) return res;

		return res;
	} 

	public class FSMVisitor extends FsmSwitch<Object> {
		private AbstractFSMDVisitor parent;
		
		public FSMVisitor(AbstractFSMDVisitor parent) {
			this.parent = parent;
		}

		@Override
		public Object caseAbstractBlock(AbstractBlock object) {
			return parent.caseAbstractBlock(object);
		}

		@Override
		public Object caseAbstractBooleanExpression(
				AbstractBooleanExpression object) {
			return parent.caseAbstractBooleanExpression(object);
		}

		@Override
		public Object caseAbstractCommandValue(AbstractCommandValue object) {
			return parent.caseAbstractCommandValue(object);
		}

		@Override
		public Object caseActivableBlock(ActivableBlock object) {
			return parent.caseActivableBlock(object);
		}

		@Override
		public Object caseAndExpression(AndExpression object) {
			return parent.caseAndExpression(object);
		}

		@Override
		public Object caseBooleanCommandValue(BooleanCommandValue object) {
			return parent.caseBooleanCommandValue(object);
		}

		@Override
		public Object caseBooleanConstant(BooleanConstant object) {
			return parent.caseBooleanConstant(object);
		}

		@Override
		public Object caseBooleanFlagTerm(BooleanFlagTerm object) {
			return parent.caseBooleanFlagTerm(object);
		}

		@Override
		public Object caseBooleanOperatorExpression(BooleanOperatorExpression object) {
			return parent.caseBooleanOperatorExpression(object);
		}

		@Override
		public Object caseFlagBearerBlock(FlagBearerBlock object) {
			return parent.caseFlagBearerBlock(object);
		}

		@Override
		public Object caseFlagTerm(FlagTerm object) {
			return parent.caseFlagTerm(object);
		}

		@Override
		public Object caseFSM(FSM object) {
			return parent.caseFSM(object);
		}

		@Override
		public Object caseIndexedBooleanFlagTerm(IndexedBooleanFlagTerm object) {
			return parent.caseIndexedBooleanFlagTerm(object);
		}

		@Override
		public Object caseIntegerCommandValue(IntegerCommandValue object) {
			return parent.caseIntegerCommandValue(object);
		}

		@Override
		public Object caseIntegerFlagTerm(IntegerFlagTerm object) {
			return parent.caseIntegerFlagTerm(object);
		}

		@Override
		public Object caseNamedElement(NamedElement object) {
			return parent.caseNamedElement(object);
		}

		@Override
		public Object caseNegateExpression(NegateExpression object) {
			return parent.caseNegateExpression(object);
		}

		@Override
		public Object caseOrExpression(OrExpression object) {
			return parent.caseOrExpression(object);
		}

		@Override
		public Object caseOutputDefaultValues(OutputDefaultValues object) {
			return parent.caseOutputDefaultValues(object);
		}

		@Override
		public Object caseState(State object) {
			return parent.caseState(object);
		}

		@Override
		public Object caseTransition(Transition object) {
			return parent.caseTransition(object);
		}
		
	}
	
 	public class OperatorVisitor extends OperatorsSwitch<Object> {

		private AbstractFSMDVisitor parent;

		public OperatorVisitor(AbstractFSMDVisitor parent) {
			this.parent = parent;
		}

		@Override
		public Object caseAbstractBlock(AbstractBlock object) {
			return parent.caseAbstractBlock(object);
		}

		@Override
		public Object caseActivableBlock(ActivableBlock object) {
			return parent.caseActivableBlock(object);
		}

		@Override
		public Object caseBinaryOperator(BinaryOperator object) {
			return parent.caseBinaryOperator(object);
		}

		@Override
		public Object caseBitSelect(BitSelect object) {
			return parent.caseBitSelect(object);
		}

		@Override
		public Object caseCompare(Compare object) {
			return parent.caseCompare(object);
		}

		@Override
		public Object caseConstantValue(ConstantValue object) {
			return parent.caseConstantValue(object);
		}

		@Override
		public Object caseControlFlowMux(ControlFlowMux object) {
			return parent.caseControlFlowMux(object);
		}

		@Override
		public Object caseCtrl2DataBuffer(Ctrl2DataBuffer object) {
			return parent.caseCtrl2DataBuffer(object);
		}

		@Override
		public Object caseData2CtrlBuffer(Data2CtrlBuffer object) {
			return parent.caseData2CtrlBuffer(object);
		}

		@Override
		public Object caseDataFlowBlock(DataFlowBlock object) {
			return parent.caseDataFlowBlock(object);
		}

		@Override
		public Object caseDataFlowMux(DataFlowMux object) {
			return parent.caseDataFlowMux(object);
		}

		@Override
		public Object caseExpandSigned(ExpandSigned object) {
			return parent.caseExpandSigned(object);
		}

		@Override
		public Object caseExpandUnsigned(ExpandUnsigned object) {
			return parent.caseExpandUnsigned(object);
		}

		@Override
		public Object caseFlagBearerBlock(FlagBearerBlock object) {
			return parent.caseFlagBearerBlock(object);
		}

		@Override
		public Object caseMerge(Merge object) {
			return parent.caseMerge(object);
		}

		@Override
		public Object caseMultiOutputDataFlowBlock(
				MultiOutputDataFlowBlock object) {
			return parent.caseMultiOutputDataFlowBlock(object);
		}

		@Override
		public Object caseQuantize(Quantize object) {
			return parent.caseQuantize(object);
		}

		@Override
		public Object caseReductionOperator(ReductionOperator object) {
			return parent.caseReductionOperator(object);
		}

		@Override
		public Object caseSingleFlagBlock(SingleFlagBlock object) {
			return parent.caseSingleFlagBlock(object);
		}

		@Override
		public Object caseSingleInputDataFlowBlock(
				SingleInputDataFlowBlock object) {
			return parent.caseSingleInputDataFlowBlock(object);
		}

		@Override
		public Object caseSingleOutputDataFlowBlock(
				SingleOutputDataFlowBlock object) {
			return parent.caseSingleOutputDataFlowBlock(object);
		}

		@Override
		public Object caseTernaryOperator(TernaryOperator object) {
			return parent.caseTernaryOperator(object);
		}

		@Override
		public Object caseUnaryOperator(UnaryOperator object) {
			return parent.caseUnaryOperator(object);
		}

	}

	public class StorageVisitor extends StorageSwitch<Object> {
		private AbstractFSMDVisitor parent;

		public StorageVisitor(AbstractFSMDVisitor parent) {
			this.parent = parent;
		}
		
		@Override
		public Object caseAbstractBlock(AbstractBlock object) {
			return parent.caseAbstractBlock(object);
		}

		@Override
		public Object caseAbstractMemory(AbstractMemory object) {
			return parent.caseAbstractMemory(object);
		}

		@Override
		public Object caseActivableBlock(ActivableBlock object) {
			return parent.caseActivableBlock(object);
		}

		@Override
		public Object caseCERegister(CERegister object) {
			return parent.caseCERegister(object);
		}

		@Override
		public Object caseDataFlowBlock(DataFlowBlock object) {
			return parent.caseDataFlowBlock(object);
		}

		@Override
		public Object caseDualPortRam(DualPortRam object) {
			return parent.caseDualPortRam(object);
		}

		@Override
		public Object caseMultiPortRam(MultiPortRam object) {
			return parent.caseMultiPortRam(object);
		}

		@Override
		public Object caseMultiPortRom(MultiPortRom object) {
			return parent.caseMultiPortRom(object);
		}

		@Override
		public Object caseRegister(Register object) {
			return parent.caseRegister(object);
		}

		@Override
		public Object caseShiftRegister(ShiftRegister object) {
			return parent.caseShiftRegister(object);
		}

		@Override
		public Object caseSingleInputDataFlowBlock(
				SingleInputDataFlowBlock object) {
			return parent.caseSingleInputDataFlowBlock(object);
		}

		@Override
		public Object caseSingleOutputDataFlowBlock(
				SingleOutputDataFlowBlock object) {
			return parent.caseSingleOutputDataFlowBlock(object);
		}

		@Override
		public Object caseSinglePortRam(SinglePortRam object) {
			return parent.caseSinglePortRam(object);
		}

		@Override
		public Object caseSinglePortRom(SinglePortRom object) {
			return parent.caseSinglePortRom(object);
		}
		
	}
	
	public class WireVisitor extends WiresSwitch<Object> {
		private AbstractFSMDVisitor parent;

		public WireVisitor(AbstractFSMDVisitor parent) {
			this.parent=parent;
		}
		
		@Override
		public Object caseControlFlowWire(ControlFlowWire object) {
			return parent.caseControlFlowWire(object);
		}

		@Override
		public Object caseDataFlowWire(DataFlowWire object) {
			return parent.caseDataFlowWire(object);
		}

		@Override
		public Object caseNamedElement(NamedElement object) {
			return parent.caseNamedElement(object);
		}

		@Override
		public Object caseWire(Wire object) {
			return parent.caseWire(object);
		}
		
	}

	public class PadsVisitor extends PadsSwitch<Object> {
		private AbstractFSMDVisitor parent;

		public PadsVisitor(AbstractFSMDVisitor parent) {
			this.parent=parent;
		}

		@Override
		public Object caseAbstractBlock(AbstractBlock object) {
			return parent.caseAbstractBlock(object);
		}

		@Override
		public Object caseActivableBlock(ActivableBlock object) {
			return parent.caseActivableBlock(object);
		}

		@Override
		public Object caseControlPad(ControlPad object) {
			return parent.caseControlPad(object);
		}

		@Override
		public Object caseDataFlowBlock(DataFlowBlock object) {
			return parent.caseDataFlowBlock(object);
		}

		@Override
		public Object caseDataInputPad(DataInputPad object) {
			return parent.caseDataInputPad(object);
		}

		@Override
		public Object caseDataOutputPad(DataOutputPad object) {
			return parent.caseDataOutputPad(object);
		}

		@Override
		public Object caseFlagBearerBlock(FlagBearerBlock object) {
			return parent.caseFlagBearerBlock(object);
		}

		@Override
		public Object caseNamedElement(NamedElement object) {
			return parent.caseNamedElement(object);
		}

		@Override
		public Object caseSingleFlagBlock(SingleFlagBlock object) {
			return parent.caseSingleFlagBlock(object);
		}

		@Override
		public Object caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
			return parent.caseSingleInputDataFlowBlock(object);
		}

		@Override
		public Object caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
			return parent.caseSingleOutputDataFlowBlock(object);
		}

		@Override
		public Object caseStatusPad(StatusPad object) {
			return parent.caseStatusPad(object);
		}
		
	}

	public class PortsVisitor extends PortSwitch<Object> {
		private AbstractFSMDVisitor parent;

		public PortsVisitor(AbstractFSMDVisitor parent) {
			this.parent=parent;
		}

		@Override
		public Object caseInControlPort(InControlPort object) {
			return parent.caseInControlPort(object);
		}

		@Override
		public Object caseInDataPort(InDataPort object) {
			return parent.caseInDataPort(object);
		}

		@Override
		public Object caseOutControlPort(OutControlPort object) {
			return parent.caseOutControlPort(object);
		}

		@Override
		public Object caseOutDataPort(OutDataPort object) {
			return parent.caseOutDataPort(object);
		}

		@Override
		public Object casePort(Port object) {
			return parent.casePort(object);
		}
		
	}
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model
	 * @generated
	 */

	public Object caseUnaryOperator(UnaryOperator object) {
		return null;
	}

	public Object caseTransition(Transition object) {
		return null;
	}

	public Object caseState(State object) {
		return null;
	}

	public Object caseOutputDefaultValues(OutputDefaultValues object) {
		return null;
	}

	public Object caseOrExpression(OrExpression object) {
		return null;
	}

	public Object caseNegateExpression(NegateExpression object) {
		return null;
	}

	public Object caseIntegerCommandValue(IntegerCommandValue object) {
		return null;
	}

	public Object caseIntegerFlagTerm(IntegerFlagTerm object) {
		return null;
	}

	public Object caseIndexedBooleanFlagTerm(IndexedBooleanFlagTerm object) {
		return null;
	}

	public Object caseFSM(FSM object) {
		return null;
	}

	public Object caseFlagTerm(FlagTerm object) {
		return null;
	}

	public Object caseBooleanOperatorExpression(BooleanOperatorExpression object) {
		return null;
	}

	public Object caseBooleanFlagTerm(BooleanFlagTerm object) {
		return null;
	}

	public Object caseBooleanConstant(BooleanConstant object) {
		return null;
	}

	public Object caseBooleanCommandValue(BooleanCommandValue object) {
		return null;
	}

	public Object caseAndExpression(AndExpression object) {
		return null;
	}

	public Object caseAbstractCommandValue(AbstractCommandValue object) {
		return null;
	}

	public Object caseAbstractBooleanExpression(
			AbstractBooleanExpression object) {
		return null;
	}

	public Object caseOutDataPort(OutDataPort object) {
		return null;
	}

	public Object caseOutControlPort(OutControlPort object) {
		return null;
	}

	public Object caseInDataPort(InDataPort object) {
		return null;
	}

	public Object caseInControlPort(InControlPort object) {
		return null;
	}

	public Object caseDataFlowWire(DataFlowWire object) {
		return null;
	}

	public Object caseControlFlowWire(ControlFlowWire object) {
		return null;
	}

	public Object caseControlPad(ControlPad object) {
		return null;
	}

	public Object caseDataInputPad(DataInputPad object) {
		return null;
	}

	public Object caseDataOutputPad(DataOutputPad object) {
		return null;
	}

	public Object caseStatusPad(StatusPad object) {
		return null;
	}

	public Object caseSinglePortRom(SinglePortRom object) {
		return null;
	}

	public Object caseSinglePortRam(SinglePortRam object) {
		return null;
	}

	public Object caseShiftRegister(ShiftRegister object) {
		return null;
	}

	public Object caseRegister(Register object) {
		return null;
	}

	public Object caseMultiPortRom(MultiPortRom object) {
		return null;
	}

	public Object caseMultiPortRam(MultiPortRam object) {
		return null;
	}

	public Object caseDualPortRam(DualPortRam object) {
		return null;
	}

	public Object caseTernaryOperator(TernaryOperator object) {
		return null;
	}

	public Object caseSingleOutputDataFlowBlock(
			SingleOutputDataFlowBlock object) {
		return null;
	}

	public Object caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
		return null;
	}

	public Object caseSingleFlagBlock(SingleFlagBlock object) {
		return null;
	}

	public Object caseReductionOperator(ReductionOperator object) {
		return null;
	}

	public Object caseQuantize(Quantize object) {
		return null;
	}

	public Object caseMultiOutputDataFlowBlock(MultiOutputDataFlowBlock object) {
		return null;
	}

	public Object caseMerge(Merge object) {
		return null;
	}

	public Object caseExpandUnsigned(ExpandUnsigned object) {
		return null;
	}

	public Object caseFlagBearerBlock(FlagBearerBlock object) {
		return null;
	}

	public Object caseControlFlowMux(ControlFlowMux object) {
		return null;
	}

	public Object caseCtrl2DataBuffer(Ctrl2DataBuffer object) {
		return null;
	}

	public Object caseData2CtrlBuffer(Data2CtrlBuffer object) {
		return null;
	}

	public Object caseDataFlowBlock(DataFlowBlock object) {
		return null;
	}

	public Object caseDataFlowMux(DataFlowMux object) {
		return null;
	}

	public Object caseExpandSigned(ExpandSigned object) {
		return null;
	}

	public Object caseConstantValue(ConstantValue object) {
		return null;
	}

	public Object caseCompare(Compare object) {
		return null;
	}

	public Object caseActivableBlock(ActivableBlock object) {
		return null;
	}

	public Object caseAbstractBlock(AbstractBlock object) {
		return null;
	}

	public Object caseAbstractMemory(AbstractMemory object) {
		return null;
	}

	public Object caseBitSelect(BitSelect object) {
		return null;
	}

	public Object caseBinaryOperator(BinaryOperator object) {
		return null;
	}

	public Object caseCERegister(CERegister object) {
		return null;
	}

}