package fr.irisa.cairn.model.datapath.user.factory;

import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.storage.CERegister;
import fr.irisa.cairn.model.datapath.storage.Register;

public class UserFactory {

	public static ControlFlowMux cMux(SingleOutputDataFlowBlock A,	SingleOutputDataFlowBlock B, OutControlPort c) {
		throw new UnsupportedOperationException();
	}

	public static DataFlowMux dMux(SingleOutputDataFlowBlock A,	SingleOutputDataFlowBlock B, SingleOutputDataFlowBlock c) {
		throw new UnsupportedOperationException();
	}

	public static ConstantValue constant(int value) {
		throw new UnsupportedOperationException();
	}

	public static ConstantValue constant(int value, int bw) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator add(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static Register regAdd(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static Register regAdd(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static Register reg(SingleOutputDataFlowBlock A) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regce(SingleOutputDataFlowBlock A, SingleFlagBlock sf) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regceAdd(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator add(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator sub(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator mulu(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator muls(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regMulu(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regMuls(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator mulu(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator muls(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regmulu(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regmuls(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator sub(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}


	public static BinaryOperator max(SingleOutputDataFlowBlock A, int value) {
		throw new UnsupportedOperationException();
	}


	public static BinaryOperator max(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static BinaryOperator min(SingleOutputDataFlowBlock A,
			SingleOutputDataFlowBlock B) {
		throw new UnsupportedOperationException();
	}

	public static BitSelect bitSelect(SingleOutputDataFlowBlock A, int lb,
			int ub) {
		throw new UnsupportedOperationException();
	}

	public static ReductionOperator max(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static ReductionOperator min(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static ReductionOperator sum(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regSum(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static CERegister regProd(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static ReductionOperator prod(SingleOutputDataFlowBlock... A) {
		throw new UnsupportedOperationException();
	}

	public static BitSelect selectBit(SingleOutputDataFlowBlock A, int pos) {
		throw new UnsupportedOperationException();
	}

	public static BitSelect selectBitsZeroToN(SingleOutputDataFlowBlock A,int pos) {
		throw new UnsupportedOperationException();
	}

	public static BitSelect selectBitsNtoLast(SingleOutputDataFlowBlock A,int pos) {
		throw new UnsupportedOperationException();
	}

}
