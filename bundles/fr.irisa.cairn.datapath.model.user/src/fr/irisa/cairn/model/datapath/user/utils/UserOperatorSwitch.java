package fr.irisa.cairn.model.datapath.user.utils;

import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.ExpandSigned;
import fr.irisa.cairn.model.datapath.operators.ExpandUnsigned;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.Quantize;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleInputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.TernaryOperator;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.operators.util.OperatorsSwitch;

public class UserOperatorSwitch<T> extends OperatorsSwitch<T>{

	private UserDatapathVisitor<T> root;

	public UserOperatorSwitch(UserDatapathVisitor<T> root) {
		this.root = root;
	}

	@Override
	public T caseBinaryOperator(BinaryOperator object) {
		// TODO Auto-generated method stub
		return root.caseBinaryOperator(object);
	}

	@Override
	public T caseBitSelect(BitSelect object) {
		// TODO Auto-generated method stub
		return root.caseBitSelect(object);
	}

	@Override
	public T caseCompare(Compare object) {
		// TODO Auto-generated method stub
		return root.caseCompare(object);
	}

	@Override
	public T caseConstantValue(ConstantValue object) {
		// TODO Auto-generated method stub
		return root.caseConstantValue(object);
	}

	@Override
	public T caseControlFlowMux(ControlFlowMux object) {
		// TODO Auto-generated method stub
		return root.caseControlFlowMux(object);
	}

	@Override
	public T caseDataFlowMux(DataFlowMux object) {
		// TODO Auto-generated method stub
		return root.caseDataFlowMux(object);
	}

	@Override
	public T caseExpandSigned(ExpandSigned object) {
		// TODO Auto-generated method stub
		return root.caseExpandSigned(object);
	}

	@Override
	public T caseExpandUnsigned(ExpandUnsigned object) {
		// TODO Auto-generated method stub
		return root.caseExpandUnsigned(object);
	}

	@Override
	public T caseMerge(Merge object) {
		// TODO Auto-generated method stub
		return root.caseMerge(object);
	}

	@Override
	public T caseQuantize(Quantize object) {
		// TODO Auto-generated method stub
		return root.caseQuantize(object);
	}

	@Override
	public T caseReductionOperator(ReductionOperator object) {
		// TODO Auto-generated method stub
		return root.caseReductionOperator(object);
	}

	@Override
	public T caseSingleInputDataFlowBlock(SingleInputDataFlowBlock object) {
		// TODO Auto-generated method stub
		return root.caseSingleInputDataFlowBlock(object);
	}

	@Override
	public T caseSingleOutputDataFlowBlock(SingleOutputDataFlowBlock object) {
		// TODO Auto-generated method stub
		return root.caseSingleOutputDataFlowBlock(object);
	}

	@Override
	public T caseTernaryOperator(TernaryOperator object) {
		// TODO Auto-generated method stub
		return root.caseTernaryOperator(object);
	}

	@Override
	public T caseUnaryOperator(UnaryOperator object) {
		// TODO Auto-generated method stub
		return root.caseUnaryOperator(object);
	}


	
	

}
