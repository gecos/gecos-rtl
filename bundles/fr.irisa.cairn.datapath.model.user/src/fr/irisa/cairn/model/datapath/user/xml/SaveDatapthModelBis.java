package fr.irisa.cairn.model.datapath.user.xml;

/**
 * SaveDatapthModelBis is a sample module. When the script evaluator encounters
 * the 'SaveDatapthModel' function, it calls the compute method.
 */
public class SaveDatapthModelBis {
	
	private String inArg;

	public SaveDatapthModelBis (
String inArg
) {
		this.inArg = inArg;
	}
	
	public void compute() {
		System.out.println("I'm a the SaveDatapthModel module called with argument: \""
				+ inArg +"\".");
	}
}