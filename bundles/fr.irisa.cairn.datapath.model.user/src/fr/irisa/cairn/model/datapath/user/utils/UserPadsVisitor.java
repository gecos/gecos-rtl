package fr.irisa.cairn.model.datapath.user.utils;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.pads.ControlPad;
import fr.irisa.cairn.model.datapath.pads.DataInputPad;
import fr.irisa.cairn.model.datapath.pads.DataOutputPad;
import fr.irisa.cairn.model.datapath.pads.util.PadsSwitch;

public class UserPadsVisitor<T> extends PadsSwitch<T> {

	public T caseControlPad(ControlPad object) {
		return  root.caseControlPad(object);
	}

	public T caseDataFlowBlock(DataFlowBlock object) {
		return  root.caseDataFlowBlock(object);
	}

	public T caseDataInputPad(DataInputPad object) {
		return  root.caseDataInputPad(object);
	}

	public T caseDataOutputPad(DataOutputPad object) {
		return root.caseDataOutputPad(object);
	}

	private UserDatapathVisitor<T> root;

	public UserPadsVisitor(UserDatapathVisitor<T> root) {
		this.root = root;
	}




}
