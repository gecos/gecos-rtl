package fr.irisa.cairn.model.datapath.user;

import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.BinaryOpcode;
import fr.irisa.cairn.model.datapath.operators.BinaryOperator;
import fr.irisa.cairn.model.datapath.operators.BitSelect;
import fr.irisa.cairn.model.datapath.operators.Compare;
import fr.irisa.cairn.model.datapath.operators.CompareOpcode;
import fr.irisa.cairn.model.datapath.operators.ConstantValue;
import fr.irisa.cairn.model.datapath.operators.ControlFlowMux;
import fr.irisa.cairn.model.datapath.operators.Ctrl2DataBuffer;
import fr.irisa.cairn.model.datapath.operators.Data2CtrlBuffer;
import fr.irisa.cairn.model.datapath.operators.DataFlowMux;
import fr.irisa.cairn.model.datapath.operators.Merge;
import fr.irisa.cairn.model.datapath.operators.OperatorsFactory;
import fr.irisa.cairn.model.datapath.operators.ReductionOpcode;
import fr.irisa.cairn.model.datapath.operators.ReductionOperator;
import fr.irisa.cairn.model.datapath.operators.SingleFlagBlock;
import fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock;
import fr.irisa.cairn.model.datapath.operators.UnaryOpcode;
import fr.irisa.cairn.model.datapath.operators.UnaryOperator;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;


public class FSMDOperatorFactory {
	

	static int labelId = 0;
	
	private static String defaultLabel() {
		return "op"+(labelId++);
	}
	public static int getLabelId() {
		return labelId;
	}

	public static int consummeLabelId() {
		return labelId++;
	}
	
	private static final int defaultOperatorBitwidth = 32;
	
	private static OperatorsFactory factory =OperatorsFactory.eINSTANCE; 

	
	/**
	 * Operand Binding functions
	 * 
	 * @param A
	 * @param B
	 * @param newop
	 */

	private static void bindOperands(DataFlowBlock blk, SingleOutputDataFlowBlock... A) {
		if(A.length!=blk.getIn().size()) {
			throw new RuntimeException("Inconsistent number of ports");
		} else {
			for (int i = 0; i < A.length; i++) {
				bindOperand(i,A[i], blk);
			}
		}
	}


	private static void bindOperand(int pos,OutDataPort A,DataFlowBlock newop) {
		InDataPort input = newop.getInput(pos);
		input.setWidth(A.getWidth());
		input.connect(A);
	}

	private static void bindOperands(DataFlowBlock blk, OutDataPort... A) {
		if(A.length!=blk.getIn().size()) {
			throw new RuntimeException("Inconsistent number of ports");
		} else {
			for (int i = 0; i < A.length; i++) {
				bindOperand(i,A[i], blk);
			}
		}
	}


	private static void bindOperand(int pos,SingleOutputDataFlowBlock A,DataFlowBlock newop) {
		InDataPort input = newop.getInput(pos);
		input.setWidth(A.getOutput().getWidth());
		input.connect(A);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#createBinaryOperator(int, fr.irisa.cairn.model.datapath.operators.BinaryOpcode)
	 */
	public static BinaryOperator binOp(int bitwidth, BinaryOpcode opcode, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator newop = binOp(opcode+"_"+getLabelId(),bitwidth, opcode);
		bindOperands(newop, A,B);
		return newop;
	}
	

	public static BinaryOperator binOp(String prefix, int bitwidth, BinaryOpcode opcode, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator newop = binOp(prefix+"_"+opcode+"_"+getLabelId(),bitwidth, opcode);
		bindOperands(newop, A,B);
		return newop;
	}

	public static BinaryOperator binOp(String prefix, int bitwidth, BinaryOpcode opcode, OutDataPort A, OutDataPort B) {
		BinaryOperator newop = binOp(prefix+"_"+opcode+"_"+getLabelId(),bitwidth, opcode);
		bindOperands(newop, A,B);
		return newop;
	}

private static BinaryOperator binOp(String name, int bitwidth,	BinaryOpcode opcode) {
		BinaryOperator binop = binOp(bitwidth,opcode);
		binop.setName(name);
		return binop;
	}

	private static BinaryOperator binOp(int bitwidth,	BinaryOpcode opcode) {
		BinaryOperator binop = binOp(opcode);
		binop.getOutput().setWidth(bitwidth);
		return binop;
	}

	private static BinaryOperator binOp(BinaryOpcode opcode) {
		BinaryOperator binop = factory.createBinaryOperator();
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",defaultOperatorBitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",defaultOperatorBitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",defaultOperatorBitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		binop.setOpcode(opcode);
		binop.setName(defaultLabel());

		return binop;
	}
	
	public static Compare compOp(int bitwidth, CompareOpcode opcode, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp("",bitwidth, opcode, A.getOutput(), B.getOutput());
	}
	
	public static Compare compOp(String name, int bitwidth, CompareOpcode opcode, OutDataPort A, OutDataPort B) {
		Compare newop = compOp(name+"_"+opcode+"_"+getLabelId(),bitwidth, opcode);
		bindOperands(newop, A,B);
		return newop;
	}

	public static Compare compOp(int bitwidth, CompareOpcode opcode, OutDataPort  A, SingleOutputDataFlowBlock B) {
		return compOp("",bitwidth, opcode, A, B.getOutput());
	}
	
	public static Compare compOp(int bitwidth, CompareOpcode opcode, SingleOutputDataFlowBlock A, OutDataPort  B) {
		return compOp("",bitwidth, opcode, A.getOutput(), B);
	}

	public static Compare compOp(String prefix, int bitwidth, CompareOpcode opcode, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		Compare newop = compOp(prefix+"_"+opcode+"_"+getLabelId(),bitwidth, opcode);
		bindOperands(newop, A,B);
		return newop;
	}

	private static Compare compOp(String name, int bitwidth, CompareOpcode opcode) {
		Compare compop = compOp(bitwidth,opcode);
		compop.setName(name);
		return compop;
	}

	private static Compare compOp(int bitwidth, CompareOpcode opcode) {
		Compare compop = compOp(opcode);
		compop.getOutput().setWidth(bitwidth);
		return compop;
	}

	private static Compare compOp(CompareOpcode opcode) {
		Compare compop = factory.createCompare();
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",defaultOperatorBitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",defaultOperatorBitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",defaultOperatorBitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		compop.setOpcode(opcode);
		compop.setName(defaultLabel());

		return compop;
	}
	
	public static Merge mergeOp(int bitwidth, OutDataPort A, OutDataPort B) {
		Merge newop = mergeOp("Merge_"+getLabelId(),bitwidth);
		bindOperands(newop, A,B);
		return newop;
	}
	
	public static Merge mergeOp(String prefix, int bitwidth, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		Merge newop = mergeOp(prefix+"_"+getLabelId(),bitwidth);
		bindOperands(newop, A,B);
		return newop;
	}

	private static Merge mergeOp(String name, int bitwidth) {
		Merge mergeop = mergeOp(bitwidth);
		mergeop.setName(name);
		return mergeop;
	}

	private static Merge mergeOp(int bitwidth) {
		Merge mergeop = mergeOp();
		mergeop.getOutput().setWidth(bitwidth);
		return mergeop;
	}

	private static Merge mergeOp() {
		Merge merge = factory.createMerge();
		InDataPort I0 = FSMDPortsFactory.IDP(merge,"I0",defaultOperatorBitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(merge,"I1",defaultOperatorBitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(merge,"O",defaultOperatorBitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(merge);
		merge.setName(defaultLabel());

		return merge;
	}

	private static int maxBitwidth(SingleOutputDataFlowBlock... A) {
		if(A[0]!= null) {
			OutDataPort output = A[0].getOutput();
			if (output==null) {
				throw new RuntimeException("error: null block");
			}
			int res = output.getWidth();
			for (int i = 1; i < A.length; i++) {
				if (A[i] == null) {
					throw new RuntimeException("error: null block");
				}
				res = Math.max(res,A[i].getOutput().getWidth());
			}
			return res;
		} else {
			throw new RuntimeException("maxBitwidth error: null block" );
		}
	}
	
	private static int maxBitwidth(OutDataPort... A) {
		int res = A[0].getWidth();
		for (int i = 1; i < A.length; i++) {
			res= Math.max(res,A[i].getWidth());
		}
		return res;
	}
	
	private static int sumBitwidth(SingleOutputDataFlowBlock... A) {
		if(A[0]!= null) {
			OutDataPort output = A[0].getOutput();
			if (output==null) {
				throw new RuntimeException("error: null block");
			}
			int res = output.getWidth();
			for (int i = 1; i < A.length; i++) {
				if (A[i] == null) {
					throw new RuntimeException("error: null block");
				}
				res = (res+A[i].getOutput().getWidth());
			}
			return res;
		} else {
			throw new RuntimeException("maxBitwidth error: null block" );
		}
	}
	
	private static int sumBitwidth(OutDataPort... A) {
		int res = A[0].getWidth();
		for (int i = 1; i < A.length; i++) {
			res= res+A[i].getWidth();
		}
		return res;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#add(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock)
	 */
	public static BinaryOperator cmp(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.CMP, A, B);
	}
	
	public static BinaryOperator[] vector_add(SingleOutputDataFlowBlock A[], SingleOutputDataFlowBlock B[]) {
		if(A.length!=B.length) 
			throw new UnsupportedOperationException("Vectors A["+A.length+"] and B["+B.length+"] have different sizes ");
		BinaryOperator res[] = new BinaryOperator[A.length];
		for(int i=0;i<res.length;i++) {
			res[i]= binOp(maxBitwidth(A[i], B[i]), BinaryOpcode.ADD,A[i],B[i]);
		}
		return res;
	}

	public static SingleOutputDataFlowBlock[] vector_range(SingleOutputDataFlowBlock A[], int start, int end) {
		if(end>=A.length||start<0||end<start) 
			throw new UnsupportedOperationException("Inconsistent range");
		int newSize = end-start+1;
		SingleOutputDataFlowBlock res[] = new SingleOutputDataFlowBlock[newSize];
		for(int i=0;i<res.length;i++) {
			res[i]= A[i+start];
		}
		return res;
	}


	public static BinaryOperator[] vector_add(SingleOutputDataFlowBlock A[], SingleOutputDataFlowBlock B) {
		
		BinaryOperator res[] = new BinaryOperator[A.length];
		for(int i=0;i<res.length;i++) {
			res[i]= binOp(maxBitwidth(A[i], B), BinaryOpcode.ADD,A[i],B);
		}
		return res;
	}
	
	public static BinaryOperator add(OutDataPort A, OutDataPort B) {
		return binOp("",maxBitwidth(A, B), BinaryOpcode.ADD, A, B);
	}

	public static BinaryOperator add(OutDataPort A, SingleOutputDataFlowBlock B) {
		return binOp("",maxBitwidth(A, B.getOutput()), BinaryOpcode.ADD, A, B.getOutput());
	}

	public static BinaryOperator add(SingleOutputDataFlowBlock A, OutDataPort B) {
		return binOp("",maxBitwidth(A.getOutput(), B), BinaryOpcode.ADD, A.getOutput(), B);
	}

	public static BinaryOperator add(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.ADD, A, B);
	}

	public static BinaryOperator add(String name,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator binOp = binOp(maxBitwidth(A, B), BinaryOpcode.ADD,A,B);
		binOp.setName(name);
		return binOp;
	}
	
	public static BinaryOperator add(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.ADD);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("ADD_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator addu(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.ADDU,A,B);
	}
	
	public static BinaryOperator addu(String name,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator binOp = binOp(maxBitwidth(A, B), BinaryOpcode.ADDU,A,B);
		binOp.setName(name);
		return binOp;
	}
	
	public static BinaryOperator addu(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.ADDU);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("ADDU_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator sub(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.SUB,A,B);
	}
	
	public static BinaryOperator sub(OutDataPort A, OutDataPort B) {
		return binOp("",maxBitwidth(A, B), BinaryOpcode.SUB,A,B);
	}

	public static BinaryOperator sub(String name,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator binOp = binOp(maxBitwidth(A, B), BinaryOpcode.SUB,A,B);
		binOp.setName(name);
		return binOp;
	}
	
	public static BinaryOperator sub(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.SUB);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("SUB_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator subu(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.SUBU,A,B);
	}
	
	public static BinaryOperator subu(String name,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		BinaryOperator binOp = binOp(maxBitwidth(A, B), BinaryOpcode.SUBU,A,B);
		binOp.setName(name);
		return binOp;
	}
	
	public static BinaryOperator subu(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.SUBU);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("SUBU_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator max2(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.MAX,A,B);
	}

	public static BinaryOperator max2(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.MAX,A,B);
	}
	
	public static BinaryOperator max2(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.MAX);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("MAX_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator min2(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.MIN,A,B);
	}
	
	public static BinaryOperator min2(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.MIN,A,B);
	}
	
	public static BinaryOperator min2(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.MIN);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("MIN_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator and(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.AND,A,B);
	}
	
	public static BinaryOperator and(OutDataPort A, OutDataPort B) {
		return binOp("",maxBitwidth(A, B), BinaryOpcode.AND,A,B);
	}

	public static BinaryOperator and(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.AND,A,B);
	}
	
	public static BinaryOperator and(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.AND);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("AND_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static BinaryOperator or(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.OR,A,B);
	}
	
	public static BinaryOperator or(OutDataPort A, OutDataPort B) {
		return binOp("",maxBitwidth(A, B), BinaryOpcode.OR,A,B);
	}

	public static BinaryOperator or(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.OR,A,B);
	}
	
	public static BinaryOperator or(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.OR);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("OR_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static BinaryOperator xor(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.XOR,A,B);
	}
	
	public static BinaryOperator xor(OutDataPort A, OutDataPort B) {
		return binOp("",maxBitwidth(A, B), BinaryOpcode.XOR,A,B);
	}

	public static BinaryOperator xor(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.XOR,A,B);
	}
	
	public static BinaryOperator xor(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.XOR);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("XOR_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static BinaryOperator nand(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.NAND,A,B);
	}
	
	public static BinaryOperator nand(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.NAND,A,B);
	}
	
	public static BinaryOperator nand(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.NAND);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("NAND_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator mul(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(A.getOutput().getWidth() + B.getOutput().getWidth(), BinaryOpcode.MUL, A, B);
	}

	/**
	 * @param bitwidth of the input operand, output bitwidth is set to 2*bitwidth 
	 * @return
	 */
	public static BinaryOperator mul(int bitwidth) {
		BinaryOperator binOp = binOp(BinaryOpcode.MUL);
		binOp.getOutput().setWidth(2*bitwidth);
		binOp.getInput(0).setWidth(bitwidth);
		binOp.getInput(1).setWidth(bitwidth);
		return binOp;
	}

	/**
	 * @param bitwidths of the input operands, output bitwidth is set to bitwidth_operand_1 + bitwidth_operand_2 
	 * @return
	 */
	public static BinaryOperator mul(int bitwidth_A,int bitwidth_B) {
		BinaryOperator binOp = binOp(BinaryOpcode.MUL);
		binOp.getOutput().setWidth(bitwidth_A + bitwidth_B);
		binOp.getInput(0).setWidth(bitwidth_A);
		binOp.getInput(1).setWidth(bitwidth_B);
		return binOp;
	}

	public static BinaryOperator mulu(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(A.getOutput().getWidth() + B.getOutput().getWidth(), BinaryOpcode.MULU, A, B);
	}
	
	public static BinaryOperator mulu(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.MULU,A,B);
	}
	
	public static BinaryOperator mulu(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.MULU);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("MULU_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator div(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.DIV,A,B);
	}
	
	public static BinaryOperator div(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.DIV,A,B);
	}
	
	public static BinaryOperator div(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.DIV);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("DIV_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}

	public static BinaryOperator divu(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.DIVU,A,B);
	}
	
	public static BinaryOperator divu(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.DIVU,A,B);
	}
	
	public static BinaryOperator divu(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.DIVU);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("DIVU_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static BinaryOperator shl(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.SHL,A,B);
	}
	
	public static BinaryOperator shl(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.SHL,A,B);
	}
	
	public static BinaryOperator shl(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.SHL);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("SHL_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static BinaryOperator shr(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(maxBitwidth(A, B), BinaryOpcode.SHR,A,B);
	}
	
	public static BinaryOperator shr(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return binOp(lab,maxBitwidth(A, B), BinaryOpcode.SHR,A,B);
	}
	
	public static BinaryOperator shr(int bitwidth){
		BinaryOperator binop = OperatorsFactory.eINSTANCE.createBinaryOperator();
		
		binop.setOpcode(BinaryOpcode.SHR);
		InDataPort I0 = FSMDPortsFactory.IDP(binop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(binop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(binop,"O",bitwidth);
		binop.setName("SHR_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(binop);
		
		return binop;
	}
	
	public static Compare eq(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.EQU,A,B);
	}

	public static Compare eq(SingleOutputDataFlowBlock A, OutDataPort B) {
		return compOp("",maxBitwidth(A.getOutput(), B), CompareOpcode.EQU,A.getOutput(),B);
	}
	
	public static Compare eq(OutDataPort A, SingleOutputDataFlowBlock B) {
		return compOp("",maxBitwidth(A, B.getOutput()), CompareOpcode.EQU,A,B.getOutput());
	}

	public static Compare eq(OutDataPort A, OutDataPort B) {
		return compOp("",maxBitwidth(A, B), CompareOpcode.EQU,A,B);
	}

	public static Compare eq(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.EQU,A,B);
	}
	
	public static Compare eq(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.EQU);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("EQU_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Compare neq(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.NEQ,A,B);
	}
	
	public static Compare neq(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.NEQ,A,B);
	}
	
	public static Compare neq(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.NEQ);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("NEQ_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Compare gt(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.GT,A,B);
	}
	
	public static Compare gt(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.GT,A,B);
	}
	
	public static Compare gt(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.GT);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("GT_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Compare gte(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.GTE,A,B);
	}
	
	public static Compare gte(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.GTE,A,B);
	}
	
	public static Compare gte(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.GTE);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("GTE_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Compare lt(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.LT,A,B);
	}
	
	public static Compare lt(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.LT,A,B);
	}
	
	public static Compare lt(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.LT);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("LT_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Compare lte(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(maxBitwidth(A, B), CompareOpcode.LTE,A,B);
	}
	
	public static Compare lte(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return compOp(lab,maxBitwidth(A, B), CompareOpcode.LTE,A,B);
	}
	
	public static Compare lte(int bitwidth){
		Compare compop = OperatorsFactory.eINSTANCE.createCompare();
		
		compop.setOpcode(CompareOpcode.LTE);
		InDataPort I0 = FSMDPortsFactory.IDP(compop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(compop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(compop,"O",bitwidth);
		compop.setName("LTE_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(compop);
		
		return compop;
	}
	
	public static Merge merge(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return mergeOp(sumBitwidth(A, B), A.getOutput(), B.getOutput());
	}
	
	public static Merge merge(OutDataPort A, OutDataPort B) {
		return mergeOp(sumBitwidth(A, B), A, B);
	}

	public static Merge merge(String lab,SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B) {
		return mergeOp(lab,sumBitwidth(A, B), A,B);
	}
	
	public static Merge merge(String lab,SingleOutputDataFlowBlock... A) {
		Merge res = merge("",A[0],A[1]); 
		for(int i=2;i<A.length;i++) {
			res = merge(res,A[i]);
		}
		return res;
	}

	
	public static SingleOutputDataFlowBlock decode(String lab, OutDataPort A, Integer... values) {
		ConstantValue[] val = new ConstantValue[values.length];
		SingleOutputDataFlowBlock res =  eq(A,constant(values[0], A.getWidth()));
		for(int i=1;i<values.length;i++) {
			res = or(res,eq(A,constant(values[i],A.getWidth())));
		}
		return res;
	}

	public static Merge merge(String lab,OutDataPort... A) {
		Merge res = merge("",A[0],A[1]); 
		for(int i=2;i<A.length;i++) {
			res = merge(res.getOutput(),A[i]);
		}
		return res;
	}
	
	public static Merge merge(int bitwidth){
		Merge mergeop = OperatorsFactory.eINSTANCE.createMerge();
		
		InDataPort I0 = FSMDPortsFactory.IDP(mergeop,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(mergeop,"I1",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(mergeop,"O",bitwidth);
		mergeop.setName("Merge_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(mergeop);
		
		return mergeop;
	}

	public static DataFlowMux dmux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleOutputDataFlowBlock S) {
		return dmux(defaultLabel(),A.getOutput(), B.getOutput(), S.getOutput());
	}

	public static DataFlowMux dmux(OutDataPort A, OutDataPort B, OutDataPort select) {
		return dmux(defaultLabel(),A, B,select);
	}

	public static DataFlowMux dmux(String label,OutDataPort A, OutDataPort B, OutDataPort select) {
		DataFlowMux cmux = dmux(label,maxBitwidth(A, B));
		A.connect(cmux.getIn().get(0));
		B.connect(cmux.getIn().get(1));
		select.connect(cmux.getInput(0));
		return cmux;
	}

	public static DataFlowMux dmux(String label,int bitwidth) {
		DataFlowMux res= OperatorsFactory.eINSTANCE.createDataFlowMux();
		InDataPort I0 = FSMDPortsFactory.IDP(res,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(res,"I1",bitwidth);
		InDataPort C = FSMDPortsFactory.IDP(res,"S",1);
		OutDataPort O = FSMDPortsFactory.ODP(res,"O",bitwidth);
		res.setName("smux_"+label);
		FSMDFactory.getCurrentDatapath().getComponents().add(res);
		return res;
	}

	public static ControlFlowMux cmux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleFlagBlock S) {
		return cmux(defaultLabel(),A.getOutput(), B.getOutput(), S.getFlag());
	}

	public static ControlFlowMux cmux(String label, SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, SingleFlagBlock S) {
		return cmux(label,A.getOutput(), B.getOutput(), S.getFlag());
	}

	public static ControlFlowMux cmux(SingleOutputDataFlowBlock A, SingleOutputDataFlowBlock B, OutControlPort S) {
		return cmux(defaultLabel(),A.getOutput(), B.getOutput(), S);
	}

	
	public static ControlFlowMux cmux(OutDataPort A, OutDataPort B, OutControlPort S) {
		return cmux(defaultLabel(),A, B, S);
	}
	
	/**
	 * Creates a N->1 Multiplexer Tree (Log2(N) depth). Command bitwidth must be consistent 
	 * with the number of mux inputs. 
	 * @param S command
	 * @param A mux input
	 * @return
	 */
	public static ControlFlowMux cmuxtree(OutControlPort S, OutDataPort... A) {
		assert(Math.pow(2, S.getWidth())>=A.length);
		ControlFlowMux matrix[][] = new ControlFlowMux[A.length][2];
		int size = A.length;
		int index =0;
		int oddeven =1;
		while(size>0){
			for (int i = 0; i < size/2; i++) {
				matrix[i][oddeven]=cmux(matrix[2*i][1-oddeven],matrix[2*i+1][1-oddeven],select(S, index));
			}
			oddeven=1-oddeven;
			index++;
		}
		return cmux(matrix[0][1-oddeven],matrix[1][1-oddeven],select(S, index));
	}
	
	public static ControlFlowMux cmuxtree(OutControlPort S, SingleOutputDataFlowBlock... A) {
		ControlFlowMux cmux= OperatorsFactory.eINSTANCE.createControlFlowMux();
		FSMDFactory.getCurrentDatapath().getComponents().add(cmux);
		int width = 0;
		int offset = 0;
		
		S.connect(FSMDPortsFactory.ICP(cmux,"S",S.getWidth()));
		for (SingleOutputDataFlowBlock a : A) {
			width = Math.max(width,a.getOutput().getWidth());
			a.getOutput().connect(FSMDPortsFactory.IDP(cmux, "I" + offset, a.getOutput().getWidth()));
			offset++;
			
		}
		OutDataPort O = FSMDPortsFactory.ODP( cmux, "O" , width);
		cmux.getOutput().setWidth(width);
		
		return cmux;
	}

	public static ControlFlowMux cmux(String label,OutDataPort A, OutDataPort B, OutControlPort select) {
		ControlFlowMux cmux = cmux(label,maxBitwidth(A, B));
		A.connect(cmux.getIn().get(0));
		B.connect(cmux.getIn().get(1));
		select.connect(cmux.getControl());
		return cmux;
	}

	public static ControlFlowMux cmux(String label,int bitwidth) {
		ControlFlowMux res= OperatorsFactory.eINSTANCE.createControlFlowMux();
		InDataPort I0 = FSMDPortsFactory.IDP(res,"I0",bitwidth);
		InDataPort I1 = FSMDPortsFactory.IDP(res,"I1",bitwidth);
		InControlPort C = FSMDPortsFactory.ICP(res,"S",1);
		OutDataPort O = FSMDPortsFactory.ODP(res,"O",bitwidth);
		res.setName("cmux_"+label);
		FSMDFactory.getCurrentDatapath().getComponents().add(res);
		return res;
	}
// To be completed by Istoan 
	
	
	public static ConstantValue constant(String name, int value, int bitwidth) {
		ConstantValue op =factory.createConstantValue();
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		if(value>(1<<bitwidth)) throw new RuntimeException("Invalid bitwithd");
		op.setValue(value);
		op.setName("C_V"+Math.abs(value)+"_"+name);
		FSMDPortsFactory.ODP(op, "O", bitwidth);
		op.getOutput().setWidth(bitwidth);
		return op;
	}

	public static ConstantValue constant( int value, int bitwidth) {
		return constant(defaultLabel(),value, bitwidth);
	}


	public static Data2CtrlBuffer D_C(SingleOutputDataFlowBlock in) {
		return D_C(in.getOutput());
	}

	public static Data2CtrlBuffer D_C(OutDataPort in) {
		Data2CtrlBuffer op =factory.createData2CtrlBuffer();
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		op.setName("D2C"+consummeLabelId()+"_"+in.getParentNode().getName());
		int width = in.getWidth();
		InDataPort I = FSMDPortsFactory.IDP(op, "I", width);
		OutControlPort O = FSMDPortsFactory.OCP(op, "O", width);
		I.connect(in);
		return op;
	}

	public static Ctrl2DataBuffer C_D(SingleFlagBlock in) {
		return C_D(in.getFlag());
	}
	
	public static Ctrl2DataBuffer C_D(OutControlPort in) {
		Ctrl2DataBuffer op =factory.createCtrl2DataBuffer();
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		op.setName("C2D"+consummeLabelId()+"_"+in.getParentNode().getName());
		InControlPort I = FSMDPortsFactory.ICP(op, "I", in.getWidth());
		FSMDPortsFactory.ODP(op, "O", in.getWidth());
		I.connect(in);
		return op;
	}

	/**
	 * 
	 * Reduction factory
	 * 
	 */


	public static ReductionOperator reduction(ReductionOpcode opcode, int bitwidth, SingleOutputDataFlowBlock... A) {
		ReductionOperator op = reduction();
		op.setOpcode(opcode);
		op.getOutput().setWidth(bitwidth);
		op.setName(opcode+"_"+defaultLabel());
		int i=0;
		for (SingleOutputDataFlowBlock block : A) {
			OutDataPort output = block.getOutput();
			InDataPort idp = FSMDPortsFactory.IDP(op, "I"+(i++), output.getWidth());
			idp.connect(block);
		}
		return op;
	}

	public static ReductionOperator reduction(ReductionOpcode opcode, int bitwidth, OutDataPort... A) {
		ReductionOperator op = reduction();
		int i=0;
		for (OutDataPort port : A) {
			FSMDPortsFactory.IDP(op, "I"+(i++), port.getWidth()).connect(port);
		}
		return op;
	}

	public static UnaryOperator not(int bitwidth, OutDataPort A) {
		UnaryOperator op = unaryOp("Not"+getLabelId(),UnaryOpcode.NOT,A.getWidth());
		return op;
	}
	
	public static UnaryOperator not(int bitwidth){
		UnaryOperator unop = OperatorsFactory.eINSTANCE.createUnaryOperator();
		
		unop.setOpcode(UnaryOpcode.NOT);
		InDataPort I0 = FSMDPortsFactory.IDP(unop,"I0",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(unop,"O",bitwidth);
		unop.setName("NOT_"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(unop);
		
		return unop;
	}

	public static UnaryOperator identity(int bitwidth, OutDataPort A) {
		UnaryOperator op = unaryOp("Ident"+getLabelId(),UnaryOpcode.IDENTITY,A.getWidth());
		return op;
	}

	public static UnaryOperator identity(int bitwidth) {
		UnaryOperator op = unaryOp("Ident"+getLabelId(),UnaryOpcode.IDENTITY,bitwidth);
		return op;
	}

	public static UnaryOperator neg(int bitwidth, OutDataPort A) {
		UnaryOperator op = unaryOp("Neg"+getLabelId(),UnaryOpcode.NEG,A.getWidth());
		return op;
	}
	
	public static UnaryOperator neg(int bitwidth){
		UnaryOperator unop = OperatorsFactory.eINSTANCE.createUnaryOperator();
		
		unop.setOpcode(UnaryOpcode.NEG);
		InDataPort I0 = FSMDPortsFactory.IDP(unop,"I0",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(unop,"O",bitwidth);
		unop.setName("Neg"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(unop);
		
		return unop;
	}

	public static UnaryOperator inv(int bitwidth, OutDataPort A) {
		UnaryOperator op = unaryOp("Inv"+getLabelId(),UnaryOpcode.INV,A.getWidth());
		return op;
	}
	
	public static UnaryOperator inv(int bitwidth){
		UnaryOperator unop = OperatorsFactory.eINSTANCE.createUnaryOperator();
		
		unop.setOpcode(UnaryOpcode.INV);
		InDataPort I0 = FSMDPortsFactory.IDP(unop,"I0",bitwidth);
		OutDataPort O = FSMDPortsFactory.ODP(unop,"O",bitwidth);
		unop.setName("Inv"+consummeLabelId());
		FSMDFactory.getCurrentDatapath().getComponents().add(unop);
		
		return unop;
	}

	private static UnaryOperator unaryOp(String string, UnaryOpcode opcod,int width) {
		UnaryOperator op = factory.createUnaryOperator();
		op.setOpcode(opcod);
		op.setName(string);
		FSMDPortsFactory.IDP(op,"I",width);
		FSMDPortsFactory.ODP(op,"O",width);
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		return op;
	}
	
	private static ReductionOperator reduction() {
		ReductionOperator op =factory.createReductionOperator();
		FSMDPortsFactory.ODP(op, "O", defaultOperatorBitwidth);
		FSMDFactory.getCurrentDatapath().getComponents().add(op);
		return op;
	}

	public static ReductionOperator sum(SingleOutputDataFlowBlock... A) {
		return reduction(ReductionOpcode.SIGMA, maxBitwidth(A),A);
	}

	public static ReductionOperator min(SingleOutputDataFlowBlock... A) {
		return reduction(ReductionOpcode.MIN, maxBitwidth(A),A);
	}

	public static ReductionOperator max(SingleOutputDataFlowBlock... A) {
		return reduction(ReductionOpcode.MAX, maxBitwidth(A),A);
	}

	public static ReductionOperator prod(SingleOutputDataFlowBlock... A) {
		return reduction(ReductionOpcode.PROD, maxBitwidth(A),A);
	}

	public static ReductionOperator sum(OutDataPort... A) {
		return reduction(ReductionOpcode.SIGMA, maxBitwidth(A),A);
	}

	public static ReductionOperator min(OutDataPort... A) {
		return reduction(ReductionOpcode.MIN, maxBitwidth(A),A);
	}

	public static ReductionOperator max(OutDataPort... A) {
		return reduction(ReductionOpcode.MAX, maxBitwidth(A),A);
	}

	public static ReductionOperator prod(OutDataPort... A) {
		return reduction(ReductionOpcode.PROD, maxBitwidth(A),A);
	}

	
	/**
	 * 
	 * Rewiring factory
	 * 
	 */
	/* (non-Javadoc)
	 * @see fr.irisa.cairn.model.user.getFactory().IUserOperatorFactory#selectBit(fr.irisa.cairn.model.datapath.operators.SingleOutputDataFlowBlock, int)
	 */
	public static BitSelect select(String label , OutDataPort A, int start, int end) {
		assert end>start && start>=0;
		BitSelect  bs = factory.createBitSelect();
		bs.setLowerBound(Math.min(start,end));
		bs.setUpperBound(Math.max(start,end));
		InDataPort I = FSMDPortsFactory.IDP(bs,"I",A.getWidth());
		bs.getIn().add(I);
		bs.getOut().add(FSMDPortsFactory.ODP(bs,"O",end-start+1));
		I.connect(A);
		FSMDFactory.getCurrentDatapath().getComponents().add(bs);
		bs.setName("sel_"+label);
		bs.getOutput().setWidth(end-start+1);
		return bs;
	}

	public static BitSelect select(OutDataPort A, int start, int end) {
		return select(A.getName()+"_"+start+"_"+end+"_"+consummeLabelId(), A, start, end);
	}

    public static BitSelect select(SingleOutputDataFlowBlock A, int start, int end) {
		return select(defaultLabel(), A.getOutput(), start, end);
	}

	public static BitSelect select(String name,SingleOutputDataFlowBlock A, int start, int end) {
		return select(name+"_"+getLabelId(), A.getOutput(), start, end);
	}

	public static BitSelect select(String name,SingleOutputDataFlowBlock A, int start) {
		return select(name,A, start, start);
	}

	public static BitSelect select(SingleOutputDataFlowBlock A, int pos) {
		if(A.getOutput().getWidth()>pos)
		return select(A.getOutput(), pos, pos);
		else throw new UnsupportedOperationException("Out of range");
	}

	public static SingleFlagBlock select(SingleFlagBlock A, int pos) {
		return D_C(select(defaultLabel(), C_D(A).getOutput(), pos, pos));
	}

	public static SingleFlagBlock select(OutControlPort A, int pos) {
		return D_C(select(defaultLabel(), C_D(A).getOutput(), pos, pos));
	}

	public static SingleFlagBlock select(SingleFlagBlock A,  int start, int end) {
		return D_C(select(defaultLabel(), C_D(A).getOutput(), start, end));
	}

}
