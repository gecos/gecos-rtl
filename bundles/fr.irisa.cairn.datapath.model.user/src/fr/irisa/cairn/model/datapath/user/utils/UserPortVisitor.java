package fr.irisa.cairn.model.datapath.user.utils;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.InDataPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.port.OutDataPort;
import fr.irisa.cairn.model.datapath.port.util.PortSwitch;

public class UserPortVisitor<T> extends PortSwitch<T> {

	private UserDatapathVisitor<T> root;

	public UserPortVisitor(UserDatapathVisitor<T> root) {
		this.root = root;
	}

	@Override
	public T caseInControlPort(InControlPort object) {
		// TODO Auto-generated method stub
		return root.caseInControlPort(object);
	}

	@Override
	public T caseInDataPort(InDataPort object) {
		// TODO Auto-generated method stub
		return root.caseInDataPort(object);
	}

	@Override
	public T caseOutControlPort(OutControlPort object) {
		// TODO Auto-generated method stub
		return root.caseOutControlPort(object);
	}

	@Override
	public T caseOutDataPort(OutDataPort object) {
		// TODO Auto-generated method stub
		return root.caseOutDataPort(object);
	}




}
