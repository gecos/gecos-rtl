package fr.irisa.cairn.bdd.analysis;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.State;

public class RemoveUnreachableStates {

	EList<State> reachList;
	FSM fsm;
	
	public RemoveUnreachableStates(FSM fsm) {
		reachList = new BasicEList<State>();
		this.fsm=fsm;
	}

	public void compute() {
		EList<State> removeList = new BasicEList<State>();

		visitSuccessors(fsm.getStart());

		for (State current : fsm.getStates()) {
			if(!reachList.contains(current)) {
				removeList.add(current);
			}
		}
		fsm.getStates().removeAll(removeList);

	}
	
	public void visitSuccessors(State state) {
		reachList.add(state);
		for (State next : state.getNextStates()) {
			if (!reachList.contains(next))
				visitSuccessors(next);
		}
	}
}
