package fr.irisa.cairn.bdd.analysis;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.user.factory.UserFSMFactory;
import fr.irisa.cairn.model.datapath.user.factory.UserPortFactory;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import junit.framework.TestCase;

public class TestBDDOptimizer extends TestCase{
	
	static UserPortFactory pf = UserPortFactory.getFactory();
	static InControlPort x[] = new InControlPort[] {
		pf.createInControlPort("x0",1),
		pf.createInControlPort("x1",1),
		pf.createInControlPort("x2",1),
		pf.createInControlPort("x3",1),
		pf.createInControlPort("x4",1),
		pf.createInControlPort("x5",1),
		pf.createInControlPort("x6",1),
		pf.createInControlPort("x7",8),
	};
		
	static UserFSMFactory f= UserFSMFactory.getFactory();
	
	static final AbstractBooleanExpression exp0 = 
		f.or(
				f.nT(x[0]),
				f.T(x[1]),
				f.nT(x[2]),
				f.or(f.nT(x[1]),f.nT(x[4])),
				f.nT(x[3])
		);

	static final AbstractBooleanExpression exp1 = 
		f.and(
				f.nT(x[0]),
				f.and(f.nT(x[1]),f.nT(x[4])),
				f.T(x[2]),
				f.and(f.nT(x[1]),f.nT(x[4])),
				f.nT(x[3]),
				f.T(x[2]),
				f.T(x[2]),
				f.NEQ(x[7],13),
				f.EQU(x[7],12)
		);
	
	static int rand(int max) {
		return (int) (Math.random()*max);
	}
	public static void main(String[] args) {
		BDDOptimizer optimizer = new BDDOptimizer(exp0);
		System.out.println("Test 1 :");
		System.out.println("Before simplification"+exp0.toString());
		System.out.println("After simplification"+optimizer.simplify().toString());
		
		optimizer = new BDDOptimizer(exp1);
		System.out.println("Before simplification"+exp1.toString());
		System.out.println("After simplification"+optimizer.simplify().toString());
	}


}
