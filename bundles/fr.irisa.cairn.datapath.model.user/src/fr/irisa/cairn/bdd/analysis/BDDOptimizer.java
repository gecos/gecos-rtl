package fr.irisa.cairn.bdd.analysis;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.user.factory.UserFSMFactory;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.FlagTerm;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.util.FsmSwitch;
import jdd.bdd.BDD;

/*  implement with switch visit ... !!! */

public class BDDOptimizer extends FsmSwitch<Integer> {

	private static final boolean VERBOSE = false;
	EList<InControlPort> in;

	private BDD bdd;
	private Integer root;
	private BDDVariableMapping map;

	UserFSMFactory factory = UserFSMFactory.getFactory();
	List<AbstractBooleanExpression> list;

	private void debug(String str) {
		if (VERBOSE)
			System.out.print(str + "\n");
	}

	public BDDOptimizer(AbstractBooleanExpression bexp) {

		in = new CollectFlags().collect(bexp);
		int bddsize = in.size() * in.size();
		bdd = new BDD(bddsize);
		map = new BDDVariableMapping(bdd);

		for (InControlPort icp : in) {
			map.map(icp);
		}

		root = doSwitch(bexp);
		debug("Root is " + root);
	}

	@Override
	public Integer caseAndExpression(AndExpression object) {
		int varBDDAnd = 0;
		;
		boolean first = true;

		for (AbstractBooleanExpression bexp : object.getTerms()) {
			doSwitch(bexp);
			if (first) {
				first = false;
				varBDDAnd = map.getBDDVarFor(bexp);
			} else {
				int old = varBDDAnd;
				varBDDAnd = bdd.and(varBDDAnd, map.getBDDVarFor(bexp));
				debug("New node id=" + varBDDAnd + "=and(" + old + ","
						+ map.getBDDVarFor(bexp) + ")");
			}
		}
		map.map(object, varBDDAnd);
		return varBDDAnd;
	}

	@Override
	public Integer caseBooleanConstant(BooleanConstant object) {
		if (object.isValue()) {
			map.map(object, bdd.getOne());
			return bdd.getOne();
		} else {
			map.map(object, bdd.getZero());
			return bdd.getZero();
		}
	}

	@Override
	public Integer caseBooleanFlagTerm(BooleanFlagTerm object) {
		InControlPort icp = object.getFlag();
		if (!in.contains(icp)) {
			throw new RuntimeException("Inconsistency in "+ this.getClass().getSimpleName());
		} else {
			int varProduct = map.getBDDVarFor(icp, 0);
			if (object.isNegated()) {
				int old = varProduct;
				varProduct = bdd.not(varProduct);
				debug("New node id=" + varProduct + "=!(" + old + ")");
			}
			map.map(object, varProduct);
			return varProduct;
		}
	}

	/** Not checked */
	@Override
	public Integer caseIntegerFlagTerm(IntegerFlagTerm object) {
		InControlPort icp = object.getFlag();
		if (!in.contains(icp)) {
			throw new RuntimeException("Inconsistency in "
					+ this.getClass().getSimpleName());
		} else {
			int bddExpressionVar = -1;
			int value = object.getValue();
			for (int i = 0; i < icp.getWidth(); i++) {
				int portBddVar = map.getBDDVarFor(icp, i);
				switch (object.getOpType()) {
				case EQU:
					if (!isBitSetAt(value, i)) {
						portBddVar = bdd.not(portBddVar);
					}
					if (i == 0) {
						bddExpressionVar = portBddVar;
					} else {
						bddExpressionVar = bdd
								.and(bddExpressionVar, portBddVar);
					}
					break;

				case NEQ:
					// NEQ
					if (isBitSetAt(value, i)) {
						portBddVar = bdd.not(portBddVar);
					}
					if (i == 0) {
						bddExpressionVar = portBddVar;
					} else {
						bddExpressionVar = bdd.or(bddExpressionVar, portBddVar);
					}
					break;
				default:
					throw new UnsupportedOperationException(
							"Not yet implemented");

				}
			}
			map.map(object, bddExpressionVar);
			return bddExpressionVar;
		}
	}

	private boolean isBitSetAt(int value, int i) {
		return ((value >> i) & 0x1) == 0x1;
	}

	@Override
	public Integer caseIndexedBooleanFlagTerm(IndexedBooleanFlagTerm object) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public Integer caseOrExpression(OrExpression object) {
		int orBDDExression = 0;
		;
		boolean first = true;
		for (AbstractBooleanExpression bexp : object.getTerms()) {
			doSwitch(bexp);
			if (first) {
				first = false;
				orBDDExression = map.getBDDVarFor(bexp);
			} else {
				int old = orBDDExression;
				orBDDExression = bdd.or(orBDDExression, map.getBDDVarFor(bexp));
				debug("New node id=" + orBDDExression + "=or(" + old + ","
						+ map.getBDDVarFor(bexp) + ")");
			}
		}
		map.map(object, orBDDExression);
		return orBDDExression;
	}

	@Override
	public Integer caseNegateExpression(NegateExpression object) {
		int notBDDExpression0;
		AbstractBooleanExpression bexp = object.getTerm();
		doSwitch(bexp);
		notBDDExpression0 = bdd.not(map.getBDDVarFor(bexp));
		map.map(object, notBDDExpression0);
		return notBDDExpression0;
	}

	public AbstractBooleanExpression simplify() {
		list = new ArrayList<AbstractBooleanExpression>();
		rebuildPredicateFromSimplifiedBDD(bdd, root, null);
		if (list.size() == 0) {
			debug("No solution -> always false");
			BooleanConstant bc = factory.createBooleanConstant();
			bc.setValue(false);
			return bc;
		}
		OrExpression orExp = factory.createOrExpression();
		for (AbstractBooleanExpression bexp : list) {
			if (bexp != null)
				orExp.getTerms().add(bexp);
		}
		return orExp;

	}

	public boolean isAlwaysTrue() {
		return root == 1;
	}

	public boolean isAlwaysFalse() {
		return root == 0;
	}

	/** Translates the simplified BDD boolean expression into a FSM predicates **/

	private void rebuildPredicateFromSimplifiedBDD(BDD bdd, int root,
			AbstractBooleanExpression current) {
		BDDDotExport dot;
		try {
			dot = new BDDDotExport("bdd.dot", bdd, map);
			dot.save(root);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (root >= 2) {

			int bddInputVar = bdd.getVar(root);
			debug("Root=" + root + " var=" + bddInputVar + " high="
					+ bdd.getHigh(root) + " low=" + bdd.getLow(root) + "");
			/**
			 * 
			 * 
			 */

			// if (bddVar<=1) throw new
			// UnsupportedOperationException("Not yet implemented");
			Entry<InControlPort, Integer> entry = map
					.getICPortForBDDvar(bddInputVar);
			InControlPort port = entry.getKey();

			BooleanFlagTerm t;
			BooleanFlagTerm nT;

			if (port.getWidth() == 1) {
				t = factory.T(port);
				nT = factory.nT(port);
			} else if (port.getWidth() > 1) {
				t = factory.T(port, entry.getValue());
				nT = factory.nT(port, entry.getValue());
			} else {
				throw new RuntimeException("Illegal port width");
			}

			/**
			 * 
			 */
			if (current != null) {
				AbstractBooleanExpression ResHigh = (AbstractBooleanExpression) EcoreUtil
						.copy(current);
				rebuildPredicateFromSimplifiedBDD(bdd, bdd.getHigh(root),
						factory.and(t, ResHigh));
			} else {
				rebuildPredicateFromSimplifiedBDD(bdd, bdd.getHigh(root), t);
			}

			/**
			 * 
			 */
			if (current != null) {
				AbstractBooleanExpression ResLow = (AbstractBooleanExpression) EcoreUtil
						.copy(current);
				rebuildPredicateFromSimplifiedBDD(bdd, bdd.getLow(root),
						factory.and(nT, ResLow));
			} else {
				rebuildPredicateFromSimplifiedBDD(bdd, bdd.getLow(root), nT);
			}
		} else if (root == 1) {
			// BDD is simplified as a constant to true
			if (current == null) {
				BooleanConstant bc = factory.createBooleanConstant();
				bc.setValue(true);
				current = bc;
			}
			list.add(current);
			debug("Adding " + current + " to predicate");
		} else if (root == 0) {
			// BDD is simplified as a constant to false
			debug("Adding " + current + " to predicate");
		}
	}

	private class CollectFlags extends FsmSwitch<Object> {
		EList<InControlPort> list;

		public EList<InControlPort> collect(AbstractBooleanExpression bexp) {
			list = new BasicEList<InControlPort>();
			doSwitch(bexp);
			return list;
		}

		@Override
		public Object caseAndExpression(AndExpression object) {
			for (AbstractBooleanExpression bexp : object.getTerms()) {
				doSwitch(bexp);
			}
			return super.caseAndExpression(object);
		}

		@Override
		public Object caseOrExpression(OrExpression object) {
			for (AbstractBooleanExpression bexp : object.getTerms()) {
				doSwitch(bexp);
			}
			return super.caseOrExpression(object);
		}



		@Override
		public Object caseNegateExpression(NegateExpression object) {
			doSwitch(object.getTerm());
			return super.caseNegateExpression(object);
		}

		public Object caseFlagTerm(FlagTerm object) {
			if (!list.contains(object.getFlag()))
				list.add(object.getFlag());
			return super.caseFlagTerm(object);
		}

	}

}
