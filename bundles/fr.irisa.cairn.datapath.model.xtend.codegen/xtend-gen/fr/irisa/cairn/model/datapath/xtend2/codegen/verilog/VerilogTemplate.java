package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog;

import com.google.common.collect.Iterables;
import fr.irisa.cairn.model.datapath.Datapath;
import fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm.FSMVerilogTemplate;
import fr.irisa.cairn.model.fsm.FSM;

@SuppressWarnings("all")
public class VerilogTemplate {
  private Datapath root;
  
  private String path;
  
  public VerilogTemplate(final Datapath toplevel, final String path) {
    this.root = toplevel;
    this.path = path;
  }
  
  public void compute() {
    final Iterable<FSM> lists = Iterables.<FSM>filter(this.root.getComponents(), FSM.class);
    final FSMVerilogTemplate gen = new FSMVerilogTemplate();
    for (final FSM fsm : lists) {
      String _name = fsm.getName();
      String _plus = ((this.path + "/fsm_") + _name);
      String _plus_1 = (_plus + ".v");
      gen.generateCode(fsm, _plus_1);
    }
  }
}
