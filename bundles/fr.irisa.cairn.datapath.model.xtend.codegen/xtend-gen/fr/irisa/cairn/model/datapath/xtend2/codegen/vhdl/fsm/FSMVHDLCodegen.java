package fr.irisa.cairn.model.datapath.xtend2.codegen.vhdl.fsm;

import com.google.common.base.Objects;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.xtend2.codegen.common.HDLCodegeneratorHelper;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class FSMVHDLCodegen {
  public String makeBinary(final int value, final int bitwdith) {
    return String.format((("%" + Integer.valueOf(bitwdith)) + "s"), Integer.toBinaryString(value)).replace(" ", "0");
  }
  
  public CharSequence generate(final FSM fsm) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("library ieee;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("library work;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("use ieee.std_logic_1164.all;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("use ieee.std_logic_arith.all;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("use ieee.std_logic_unsigned.all;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("architecture RTL of ");
    String _name = fsm.getName();
    _builder.append(_name, "\t");
    _builder.append(" is");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("type T_");
    String _firstUpper = StringExtensions.toFirstUpper(fsm.getName());
    _builder.append(_firstUpper, "\t\t");
    _builder.append(" is (");
    _builder.newLineIfNotEmpty();
    {
      EList<State> _states = fsm.getStates();
      boolean _hasElements = false;
      for(final State state : _states) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",", "\t\t\t");
        }
        _builder.append("\t\t\t");
        String _label = state.getLabel();
        _builder.append(_label, "\t\t\t");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t");
    _builder.append(");");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("signal ns,cs : T_");
    String _firstUpper_1 = StringExtensions.toFirstUpper(fsm.getName());
    _builder.append(_firstUpper_1, "\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("constant ONE : std_logic:=\'1\';");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("constant ZERO : std_logic:=\'0\';");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("SYNC: process(clk,rst) ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (rst=\'1\') then");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("cs <= ");
    String _label_1 = fsm.getStart().getLabel();
    _builder.append(_label_1, "\t\t\t\t");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("elsif rising_edge(clk) then");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("cs <= ns;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("end if; ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end process;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("STATE_TRANSITIONS: process(cs,");
    _builder.newLine();
    {
      EList<InControlPort> _activate = fsm.getActivate();
      boolean _hasElements_1 = false;
      for(final InControlPort inputPort : _activate) {
        if (!_hasElements_1) {
          _hasElements_1 = true;
        } else {
          _builder.appendImmediate(",", "\t\t\t");
        }
        _builder.append("\t\t\t");
        String _name_1 = inputPort.getName();
        _builder.append(_name_1, "\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t\t");
      }
    }
    _builder.append(")  ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("begin");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("case cs is");
    _builder.newLine();
    {
      EList<State> _states_1 = fsm.getStates();
      for(final State state_1 : _states_1) {
        {
          EList<Transition> _transitions = state_1.getTransitions();
          for(final Transition t : _transitions) {
            _builder.append("\t\t\t\t");
            CharSequence _genTransition = this.genTransition(t);
            _builder.append(_genTransition, "\t\t\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t\t\t\t");
    _builder.append("when others =>");
    _builder.newLine();
    _builder.append("\t\t\t\t\t");
    _builder.append("null;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("end case;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end process;");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("STATE_COMMANDS: process(cs,");
    _builder.newLine();
    {
      EList<InControlPort> _activate_1 = fsm.getActivate();
      boolean _hasElements_2 = false;
      for(final InControlPort inputPort_1 : _activate_1) {
        if (!_hasElements_2) {
          _hasElements_2 = true;
        } else {
          _builder.appendImmediate(",", "\t\t\t");
        }
        _builder.append("\t\t\t");
        String _name_2 = inputPort_1.getName();
        _builder.append(_name_2, "\t\t\t");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t\t\t");
      }
    }
    _builder.append(")  ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("begin");
    _builder.newLine();
    {
      EList<OutputDefaultValues> _defaultValues = fsm.getDefaultValues();
      for(final OutputDefaultValues defaultValue : _defaultValues) {
        _builder.append("\t\t\t");
        String _name_3 = defaultValue.getPort().getName();
        _builder.append(_name_3, "\t\t\t");
        _builder.append(" <= \"\"");
        String _binaryValue = HDLCodegeneratorHelper.binaryValue(defaultValue.getValue(), defaultValue.getPort().getWidth());
        _builder.append(_binaryValue, "\t\t\t");
        _builder.append("\";");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t\t\t");
    _builder.append("case cs is");
    _builder.newLine();
    {
      EList<State> _states_2 = fsm.getStates();
      for(final State state_2 : _states_2) {
        {
          EList<AbstractCommandValue> _activatedCommands = state_2.getActivatedCommands();
          for(final AbstractCommandValue command : _activatedCommands) {
            _builder.append("\t\t\t\t");
            CharSequence _genCommand = this.genCommand(command);
            _builder.append(_genCommand, "\t\t\t\t");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t\t\t\t");
    _builder.append("when others =>");
    _builder.newLine();
    _builder.append("\t\t\t\t\t");
    _builder.append("null;");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("end case;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("end process;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("end RTL;");
    _builder.newLine();
    return _builder;
  }
  
  protected String _genPredicate(final AbstractBooleanExpression e) {
    return null;
  }
  
  /**
   * Commands
   */
  protected CharSequence _genCommandStatement(final AbstractCommandValue s) {
    return null;
  }
  
  protected CharSequence _genCommandStatement(final IntegerCommandValue s) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = s.getCommand().getName();
    _builder.append(_name);
    _builder.append(" <= ");
    String _makeBinary = this.makeBinary(s.getValue(), s.getCommand().getWidth());
    _builder.append(_makeBinary);
    return _builder;
  }
  
  protected CharSequence _genCommandStatement(final BooleanCommandValue s) {
    CharSequence _xifexpression = null;
    boolean _contains = s.getValue().toString().toUpperCase().contains("ONE");
    if (_contains) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = s.getCommand().getName();
      _builder.append(_name);
      _builder.append(" <= \'1\' ");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = s.getCommand().getName();
      _builder_1.append(_name_1);
      _builder_1.append(" <= \'0\' ");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  public CharSequence genTransition(final Transition t) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = t.getPredicate();
    boolean _notEquals = (!Objects.equal(_predicate, null));
    if (_notEquals) {
      CharSequence _xifexpression_1 = null;
      boolean _isFalse = t.getPredicate().isFalse();
      if (_isFalse) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("-- Unreachable state ");
        String _label = t.getDst().getLabel();
        _builder.append(_label);
        _builder.newLineIfNotEmpty();
        _xifexpression_1 = _builder;
      } else {
        CharSequence _xifexpression_2 = null;
        boolean _isTrue = t.getPredicate().isTrue();
        if (_isTrue) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("-- Predicate ");
          String _string = t.getPredicate().toString();
          _builder_1.append(_string);
          _builder_1.append(" ");
          _builder_1.newLineIfNotEmpty();
          _builder_1.append("ns  <= ");
          String _label_1 = t.getDst().getLabel();
          _builder_1.append(_label_1);
          _builder_1.append(" ;");
          _builder_1.newLineIfNotEmpty();
          _xifexpression_2 = _builder_1;
        } else {
          CharSequence _xifexpression_3 = null;
          if (((!t.getPredicate().isTrue()) && (!t.getPredicate().isFalse()))) {
            StringConcatenation _builder_2 = new StringConcatenation();
            _builder_2.append("-- Predicate ");
            String _string_1 = t.getPredicate().toString();
            _builder_2.append(_string_1);
            _builder_2.append(" ");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("if (");
            String _genPredicate = this.genPredicate(t.getPredicate());
            _builder_2.append(_genPredicate);
            _builder_2.append(") then");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("\t");
            _builder_2.append("ns  <= ");
            String _label_2 = t.getDst().getLabel();
            _builder_2.append(_label_2, "\t");
            _builder_2.append(" ;");
            _builder_2.newLineIfNotEmpty();
            _builder_2.append("end if;");
            _builder_2.newLine();
            _xifexpression_3 = _builder_2;
          } else {
            StringConcatenation _builder_3 = new StringConcatenation();
            _builder_3.append("-- Predicate ");
            String _string_2 = t.getPredicate().toString();
            _builder_3.append(_string_2);
            _builder_3.append(" ");
            _builder_3.newLineIfNotEmpty();
            _builder_3.append("ns  <= ");
            String _label_3 = t.getDst().getLabel();
            _builder_3.append(_label_3);
            _builder_3.append(" ;");
            _builder_3.newLineIfNotEmpty();
            _xifexpression_3 = _builder_3;
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    } else {
      StringConcatenation _builder_4 = new StringConcatenation();
      _builder_4.append("-- null predicate  ");
      _builder_4.newLine();
      _xifexpression = _builder_4;
    }
    return _xifexpression;
  }
  
  /**
   * Predicates
   */
  protected String _genPredicate(final AndExpression p) {
    String _xifexpression = null;
    int _size = p.getTerms().size();
    boolean _notEquals = (_size != 0);
    if (_notEquals) {
      StringConcatenation _builder = new StringConcatenation();
      {
        EList<AbstractBooleanExpression> _terms = p.getTerms();
        boolean _hasElements = false;
        for(final AbstractBooleanExpression term : _terms) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(" and ", "");
          }
          _builder.append("(");
          String _genPredicate = this.genPredicate(term);
          _builder.append(_genPredicate);
          _builder.append(")");
        }
      }
      _xifexpression = _builder.toString();
    }
    return _xifexpression;
  }
  
  protected String _genPredicate(final OrExpression p) {
    String _xifexpression = null;
    int _size = p.getTerms().size();
    boolean _notEquals = (_size != 0);
    if (_notEquals) {
      StringConcatenation _builder = new StringConcatenation();
      {
        EList<AbstractBooleanExpression> _terms = p.getTerms();
        boolean _hasElements = false;
        for(final AbstractBooleanExpression term : _terms) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(" or ", "");
          }
          _builder.append("(");
          String _genPredicate = this.genPredicate(term);
          _builder.append(_genPredicate);
          _builder.append(")");
        }
      }
      _xifexpression = _builder.toString();
    }
    return _xifexpression;
  }
  
  public CharSequence Predicate(final NegateExpression n) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("not(");
    String _genPredicate = this.genPredicate(n.getTerm());
    _builder.append(_genPredicate);
    _builder.append(")");
    return _builder;
  }
  
  protected String _genPredicate(final BooleanFlagTerm bft) {
    String _xifexpression = null;
    boolean _isNegated = bft.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = bft.getFlag().getName();
      _builder.append(_name);
      _builder.append("=\'0\' ");
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = bft.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("=\'1\' ");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  protected String _genPredicate(final IntegerFlagTerm ift) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = ift.getFlag().getName();
    _builder.append(_name);
    _builder.append("=");
    String _makeBinary = this.makeBinary(ift.getValue(), ift.getFlag().getWidth());
    _builder.append(_makeBinary);
    return _builder.toString();
  }
  
  protected String _genPredicate(final IndexedBooleanFlagTerm ift) {
    String _xifexpression = null;
    boolean _isNegated = ift.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = ift.getFlag().getName();
      _builder.append(_name);
      _builder.append("(");
      int _offset = ift.getOffset();
      _builder.append(_offset);
      _builder.append(")=\'0\' ");
      _xifexpression = _builder.toString();
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = ift.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("(");
      int _offset_1 = ift.getOffset();
      _builder_1.append(_offset_1);
      _builder_1.append(")=\'1\' ");
      _xifexpression = _builder_1.toString();
    }
    return _xifexpression;
  }
  
  public CharSequence genCommand(final AbstractCommandValue s) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = s.getPredicate();
    boolean _notEquals = (!Objects.equal(_predicate, null));
    if (_notEquals) {
      CharSequence _xblockexpression = null;
      {
        boolean _isFalse = s.getPredicate().isFalse();
        if (_isFalse) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("-- Unsatisfiable predicate for command ");
          CharSequence _genCommandStatement = this.genCommandStatement(s);
          _builder.append(_genCommandStatement);
          _builder.newLineIfNotEmpty();
          return _builder.toString();
        } else {
          boolean _isTrue = s.getPredicate().isTrue();
          if (_isTrue) {
            StringConcatenation _builder_1 = new StringConcatenation();
            CharSequence _genCommandStatement_1 = this.genCommandStatement(s);
            _builder_1.append(_genCommandStatement_1);
            _builder_1.append(";");
            _builder_1.newLineIfNotEmpty();
            return _builder_1.toString();
          } else {
            if (((!s.getPredicate().isTrue()) && (!s.getPredicate().isFalse()))) {
            }
          }
        }
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("if (");
        String _genPredicate = this.genPredicate(s.getPredicate());
        _builder_2.append(_genPredicate);
        _builder_2.append(") then");
        _builder_2.newLineIfNotEmpty();
        CharSequence _genCommandStatement_2 = this.genCommandStatement(s);
        _builder_2.append(_genCommandStatement_2);
        _builder_2.append(";");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("end if;");
        _builder_2.newLine();
        _xblockexpression = _builder_2;
      }
      _xifexpression = _xblockexpression;
    } else {
      StringConcatenation _builder = new StringConcatenation();
      CharSequence _genCommandStatement = this.genCommandStatement(s);
      _builder.append(_genCommandStatement);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    }
    return _xifexpression;
  }
  
  public String genPredicate(final AbstractBooleanExpression ift) {
    if (ift instanceof IndexedBooleanFlagTerm) {
      return _genPredicate((IndexedBooleanFlagTerm)ift);
    } else if (ift instanceof AndExpression) {
      return _genPredicate((AndExpression)ift);
    } else if (ift instanceof BooleanFlagTerm) {
      return _genPredicate((BooleanFlagTerm)ift);
    } else if (ift instanceof IntegerFlagTerm) {
      return _genPredicate((IntegerFlagTerm)ift);
    } else if (ift instanceof OrExpression) {
      return _genPredicate((OrExpression)ift);
    } else if (ift != null) {
      return _genPredicate(ift);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(ift).toString());
    }
  }
  
  public CharSequence genCommandStatement(final AbstractCommandValue s) {
    if (s instanceof BooleanCommandValue) {
      return _genCommandStatement((BooleanCommandValue)s);
    } else if (s instanceof IntegerCommandValue) {
      return _genCommandStatement((IntegerCommandValue)s);
    } else if (s != null) {
      return _genCommandStatement(s);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(s).toString());
    }
  }
}
