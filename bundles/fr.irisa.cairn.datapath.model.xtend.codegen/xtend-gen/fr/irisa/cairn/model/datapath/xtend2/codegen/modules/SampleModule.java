package fr.irisa.cairn.model.datapath.xtend2.codegen.modules;

/**
 * SampleModule is a sample module. When the script evaluator encounters
 * the 'Sample' function, it calls the compute method.
 */
public class SampleModule {
	
	private String inArg;

	public SampleModule (
String inArg
) {
		this.inArg = inArg;
	}
	
	public void compute() {
		System.out.println("I'm a the Sample module called with argument: \""
				+ inArg +"\".");
	}
}