package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm;

import com.google.common.base.Objects;
import fr.irisa.cairn.model.datapath.Port;
import fr.irisa.cairn.model.datapath.port.InControlPort;
import fr.irisa.cairn.model.datapath.port.OutControlPort;
import fr.irisa.cairn.model.datapath.xtend2.codegen.common.HDLCodegeneratorHelper;
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression;
import fr.irisa.cairn.model.fsm.AbstractCommandValue;
import fr.irisa.cairn.model.fsm.AndExpression;
import fr.irisa.cairn.model.fsm.BooleanCommandValue;
import fr.irisa.cairn.model.fsm.BooleanConstant;
import fr.irisa.cairn.model.fsm.BooleanFlagTerm;
import fr.irisa.cairn.model.fsm.BooleanValue;
import fr.irisa.cairn.model.fsm.FSM;
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm;
import fr.irisa.cairn.model.fsm.IntegerCommandValue;
import fr.irisa.cairn.model.fsm.IntegerFlagTerm;
import fr.irisa.cairn.model.fsm.NegateExpression;
import fr.irisa.cairn.model.fsm.OrExpression;
import fr.irisa.cairn.model.fsm.OutputDefaultValues;
import fr.irisa.cairn.model.fsm.State;
import fr.irisa.cairn.model.fsm.Transition;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class FSMVerilogTemplate {
  private FSM fsm;
  
  protected CharSequence _generatePortDefinition(final Port p) {
    return null;
  }
  
  protected CharSequence _generatePortDefinition(final InControlPort p) {
    CharSequence _xifexpression = null;
    int _width = p.getWidth();
    boolean _greaterThan = (_width > 1);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("input [");
      int _width_1 = p.getWidth();
      int _minus = (_width_1 - 1);
      _builder.append(_minus);
      _builder.append(":0] ");
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(" ;");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("input  ");
      String _name_1 = p.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" ;");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePortDefinition(final OutControlPort p) {
    CharSequence _xifexpression = null;
    int _width = p.getWidth();
    boolean _greaterThan = (_width > 1);
    if (_greaterThan) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("output [");
      int _width_1 = p.getWidth();
      int _minus = (_width_1 - 1);
      _builder.append(_minus);
      _builder.append(":0] ");
      String _name = p.getName();
      _builder.append(_name);
      _builder.append(" ;");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("output  ");
      String _name_1 = p.getName();
      _builder_1.append(_name_1);
      _builder_1.append(" ;");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePredicate(final AbstractBooleanExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/// ");
    _builder.append(expr);
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final IndexedBooleanFlagTerm expr) {
    CharSequence _xifexpression = null;
    boolean _isNegated = expr.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = expr.getFlag().getName();
      _builder.append(_name);
      _builder.append("[]");
      int _offset = expr.getOffset();
      _builder.append(_offset);
      _builder.append("]==1\'b0");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = expr.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("[");
      int _offset_1 = expr.getOffset();
      _builder_1.append(_offset_1);
      _builder_1.append("]==1\'b1");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePredicate(final BooleanConstant c) {
    StringConcatenation _builder = new StringConcatenation();
    boolean _isValue = c.isValue();
    _builder.append(_isValue);
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final IntegerFlagTerm expr) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = expr.getFlag().getName();
    _builder.append(_name);
    _builder.append("==");
    String _binaryValue = HDLCodegeneratorHelper.binaryValue(expr.getValue(), expr.getFlag().getWidth());
    _builder.append(_binaryValue);
    _builder.append("\'b");
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final BooleanFlagTerm expr) {
    CharSequence _xifexpression = null;
    boolean _isNegated = expr.isNegated();
    if (_isNegated) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = expr.getFlag().getName();
      _builder.append(_name);
      _builder.append("==1\'b0");
      _xifexpression = _builder;
    } else {
      StringConcatenation _builder_1 = new StringConcatenation();
      String _name_1 = expr.getFlag().getName();
      _builder_1.append(_name_1);
      _builder_1.append("==1\'b1");
      _xifexpression = _builder_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generatePredicate(final OrExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    {
      EList<AbstractBooleanExpression> _terms = expr.getTerms();
      boolean _hasElements = false;
      for(final AbstractBooleanExpression t : _terms) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(" || ", "");
        }
        Object _generatePredicate = this.generatePredicate(t);
        _builder.append(_generatePredicate);
      }
    }
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final AndExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(");
    {
      EList<AbstractBooleanExpression> _terms = expr.getTerms();
      boolean _hasElements = false;
      for(final AbstractBooleanExpression t : _terms) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(" && ", "");
        }
        Object _generatePredicate = this.generatePredicate(t);
        _builder.append(_generatePredicate);
      }
    }
    _builder.append(")");
    return _builder;
  }
  
  protected CharSequence _generatePredicate(final NegateExpression expr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("~(");
    Object _generatePredicate = this.generatePredicate(expr.getTerm());
    _builder.append(_generatePredicate);
    _builder.append(")");
    return _builder;
  }
  
  public CharSequence generateTransition(final Transition t) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = t.getPredicate();
    boolean _equals = Objects.equal(_predicate, null);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("NS = `");
      String _upperCase = t.getDst().getLabel().toUpperCase();
      _builder.append(_upperCase);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    } else {
      CharSequence _xifexpression_1 = null;
      AbstractBooleanExpression _predicate_1 = t.getPredicate();
      if ((_predicate_1 instanceof BooleanConstant)) {
        CharSequence _xifexpression_2 = null;
        AbstractBooleanExpression _predicate_2 = t.getPredicate();
        boolean _isValue = ((BooleanConstant) _predicate_2).isValue();
        if (_isValue) {
          StringConcatenation _builder_1 = new StringConcatenation();
          _builder_1.append("// if (true)");
          _builder_1.newLine();
          _builder_1.append("NS = `");
          String _upperCase_1 = t.getDst().getLabel().toUpperCase();
          _builder_1.append(_upperCase_1);
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
          _xifexpression_2 = _builder_1;
        }
        _xifexpression_1 = _xifexpression_2;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("if (");
        CharSequence _generatePredicate = this.generatePredicate(t.getPredicate());
        _builder_2.append(_generatePredicate);
        _builder_2.append(")");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("\t");
        _builder_2.append("NS = `");
        String _upperCase_2 = t.getDst().getLabel().toUpperCase();
        _builder_2.append(_upperCase_2, "\t");
        _builder_2.append(";");
        _builder_2.newLineIfNotEmpty();
        _xifexpression_1 = _builder_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  protected CharSequence _generateCommand(final AbstractCommandValue command) {
    return null;
  }
  
  protected CharSequence _generateCommand(final BooleanCommandValue command) {
    CharSequence _xblockexpression = null;
    {
      String res = "";
      BooleanValue _value = command.getValue();
      if (_value != null) {
        switch (_value) {
          case ONE:
            String _name = command.getCommand().getName();
            String _plus = (_name + " = 1\'b1");
            res = _plus;
            break;
          case ZERO:
            String _name_1 = command.getCommand().getName();
            String _plus_1 = (_name_1 + " = 1\'b0");
            res = _plus_1;
            break;
          case DONT_CARE:
            String _name_2 = command.getCommand().getName();
            String _plus_2 = (_name_2 + "`DONTCARE");
            res = _plus_2;
            break;
          default:
            res = "// DUNNO";
            break;
        }
      } else {
        res = "// DUNNO";
      }
      CharSequence _xifexpression = null;
      AbstractBooleanExpression _predicate = command.getPredicate();
      boolean _equals = Objects.equal(_predicate, null);
      if (_equals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(res);
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _xifexpression = _builder;
      } else {
        CharSequence _xifexpression_1 = null;
        AbstractBooleanExpression _predicate_1 = command.getPredicate();
        if ((_predicate_1 instanceof BooleanConstant)) {
          CharSequence _xifexpression_2 = null;
          AbstractBooleanExpression _predicate_2 = command.getPredicate();
          boolean _isValue = ((BooleanConstant) _predicate_2).isValue();
          if (_isValue) {
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append(res);
            _builder_1.append(";");
            _builder_1.newLineIfNotEmpty();
            _xifexpression_2 = _builder_1;
          }
          _xifexpression_1 = _xifexpression_2;
        } else {
          StringConcatenation _builder_2 = new StringConcatenation();
          _builder_2.append("if (");
          CharSequence _generatePredicate = this.generatePredicate(command.getPredicate());
          _builder_2.append(_generatePredicate);
          _builder_2.append(")");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("begin");
          _builder_2.newLine();
          _builder_2.append("\t");
          _builder_2.append(res, "\t");
          _builder_2.append(";");
          _builder_2.newLineIfNotEmpty();
          _builder_2.append("end");
          _builder_2.newLine();
          _xifexpression_1 = _builder_2;
        }
        _xifexpression = _xifexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generateCommand(final IntegerCommandValue command) {
    CharSequence _xifexpression = null;
    AbstractBooleanExpression _predicate = command.getPredicate();
    boolean _equals = Objects.equal(_predicate, null);
    if (_equals) {
      StringConcatenation _builder = new StringConcatenation();
      String _name = command.getCommand().getName();
      String _plus = (_name + " = ");
      int _width = command.getCommand().getWidth();
      String _plus_1 = (_plus + Integer.valueOf(_width));
      _builder.append(_plus_1);
      _builder.append("\'b");
      String _binaryValue = HDLCodegeneratorHelper.binaryValue(command.getValue(), command.getCommand().getWidth());
      String _plus_2 = (_binaryValue + "");
      _builder.append(_plus_2);
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _xifexpression = _builder;
    } else {
      CharSequence _xifexpression_1 = null;
      AbstractBooleanExpression _predicate_1 = command.getPredicate();
      if ((_predicate_1 instanceof BooleanConstant)) {
        CharSequence _xifexpression_2 = null;
        AbstractBooleanExpression _predicate_2 = command.getPredicate();
        boolean _isValue = ((BooleanConstant) _predicate_2).isValue();
        if (_isValue) {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _name_1 = command.getCommand().getName();
          String _plus_3 = (_name_1 + " = ");
          int _width_1 = command.getCommand().getWidth();
          String _plus_4 = (_plus_3 + Integer.valueOf(_width_1));
          _builder_1.append(_plus_4);
          _builder_1.append("\'b");
          String _binaryValue_1 = HDLCodegeneratorHelper.binaryValue(command.getValue(), command.getCommand().getWidth());
          String _plus_5 = (_binaryValue_1 + "");
          _builder_1.append(_plus_5);
          _builder_1.append(";");
          _builder_1.newLineIfNotEmpty();
          _xifexpression_2 = _builder_1;
        }
        _xifexpression_1 = _xifexpression_2;
      } else {
        StringConcatenation _builder_2 = new StringConcatenation();
        _builder_2.append("if (");
        CharSequence _generatePredicate = this.generatePredicate(command.getPredicate());
        _builder_2.append(_generatePredicate);
        _builder_2.append(")");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("begin");
        _builder_2.newLine();
        _builder_2.append("\t");
        String _name_2 = command.getCommand().getName();
        String _plus_6 = (_name_2 + " = ");
        int _width_2 = command.getCommand().getWidth();
        String _plus_7 = (_plus_6 + Integer.valueOf(_width_2));
        _builder_2.append(_plus_7, "\t");
        _builder_2.append("\'b");
        String _binaryValue_2 = HDLCodegeneratorHelper.binaryValue(command.getValue(), command.getCommand().getWidth());
        String _plus_8 = (_binaryValue_2 + "");
        _builder_2.append(_plus_8, "\t");
        _builder_2.append(";");
        _builder_2.newLineIfNotEmpty();
        _builder_2.append("end");
        _builder_2.newLine();
        _xifexpression_1 = _builder_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public CharSequence genPortList(final List<Port> ports) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _hasElements = false;
      for(final Port p : ports) {
        if (!_hasElements) {
          _hasElements = true;
        } else {
          _builder.appendImmediate(",\n", "");
        }
        String _name = p.getName();
        _builder.append(_name);
      }
    }
    _builder.newLineIfNotEmpty();
    return _builder;
  }
  
  public void generateCode(final FSM fsm, final String filename) {
    try {
      final PrintStream ps = new PrintStream(filename);
      this.fsm = fsm;
      ps.append(this.generate(fsm));
      ps.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public void generateCode(final FSM fsm) {
    String _name = fsm.getName();
    String _plus = ("fsm" + _name);
    String _plus_1 = (_plus + ".v");
    this.generateCode(fsm, _plus_1);
  }
  
  public CharSequence generateDefaultOutput(final OutControlPort op) {
    CharSequence _xblockexpression = null;
    {
      final Function1<OutputDefaultValues, Boolean> _function = (OutputDefaultValues s) -> {
        OutControlPort _port = s.getPort();
        return Boolean.valueOf(Objects.equal(_port, op));
      };
      final OutputDefaultValues first = IterableExtensions.<OutputDefaultValues>findFirst(this.fsm.getDefaultValues(), _function);
      int value = 0;
      boolean _notEquals = (!Objects.equal(first, null));
      if (_notEquals) {
        value = ((OutputDefaultValues) first).getValue();
      }
      StringConcatenation _builder = new StringConcatenation();
      String _name = op.getName();
      _builder.append(_name);
      _builder.append(" = ");
      String _name_1 = op.getName();
      _builder.append(_name_1);
      _builder.append("\'b");
      String _binaryValue = HDLCodegeneratorHelper.binaryValue(value, op.getWidth());
      _builder.append(_binaryValue);
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence generate(final FSM fsm) {
    CharSequence _xblockexpression = null;
    {
      final int wordlength = HDLCodegeneratorHelper.wordlength(fsm.getStates().size());
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("// To move in some header files ?");
      _builder.newLine();
      {
        EList<State> _states = fsm.getStates();
        for(final State state : _states) {
          _builder.append("`define ");
          String _upperCase = state.getLabel().toUpperCase();
          _builder.append(_upperCase);
          _builder.append(" ");
          _builder.append(wordlength);
          _builder.append("\'b");
          String _binaryValue = HDLCodegeneratorHelper.binaryValue(fsm.getStates().indexOf(state), wordlength);
          _builder.append(_binaryValue);
          _builder.append("  ");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("module ");
      String _name = fsm.getName();
      _builder.append(_name);
      _builder.append(" ( clk, rst, ");
      _builder.newLineIfNotEmpty();
      _builder.append("// Input signals");
      _builder.newLine();
      EList<InControlPort> _activate = fsm.getActivate();
      CharSequence _genPortList = this.genPortList(((List) _activate));
      _builder.append(_genPortList);
      _builder.append(", ");
      _builder.newLineIfNotEmpty();
      _builder.append("// Output commands");
      _builder.newLine();
      EList<OutControlPort> _flags = fsm.getFlags();
      CharSequence _genPortList_1 = this.genPortList(((List) _flags));
      _builder.append(_genPortList_1);
      _builder.newLineIfNotEmpty();
      _builder.append(");");
      _builder.newLine();
      _builder.newLine();
      _builder.append("input clk, rst;");
      _builder.newLine();
      _builder.newLine();
      {
        EList<InControlPort> _activate_1 = fsm.getActivate();
        for(final InControlPort ip : _activate_1) {
          CharSequence _generatePortDefinition = this.generatePortDefinition(ip);
          _builder.append(_generatePortDefinition);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      {
        EList<OutControlPort> _flags_1 = fsm.getFlags();
        for(final OutControlPort op : _flags_1) {
          CharSequence _generatePortDefinition_1 = this.generatePortDefinition(op);
          _builder.append(_generatePortDefinition_1);
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.newLine();
      _builder.append("\t");
      _builder.append("reg [`NBITS-1:0] CS;");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("wire [`NBITS-1:0] NS;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// async reset (active high tyo be consistent the ");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("// remaining of the flow)");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("always@(posedge clk or posedge reset)");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("begin ");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("if (reset) ");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("CS <= `");
      String _upperCase_1 = fsm.getStart().getLabel().toUpperCase();
      _builder.append(_upperCase_1, "\t\t\t");
      _builder.append(";");
      _builder.newLineIfNotEmpty();
      _builder.append("\t\t");
      _builder.append("else ");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("CS <= NS;");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("end");
      _builder.newLine();
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("always @(CS or ");
      {
        EList<InControlPort> _activate_2 = fsm.getActivate();
        boolean _hasElements = false;
        for(final Port p : _activate_2) {
          if (!_hasElements) {
            _hasElements = true;
          } else {
            _builder.appendImmediate(" or ", "\t");
          }
          String _name_1 = p.getName();
          _builder.append(_name_1, "\t");
        }
      }
      _builder.append(")");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("begin");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("case(CS)");
      _builder.newLine();
      {
        EList<State> _states_1 = fsm.getStates();
        for(final State state_1 : _states_1) {
          _builder.append("\t\t");
          _builder.append("`");
          String _upperCase_2 = state_1.getLabel().toUpperCase();
          _builder.append(_upperCase_2, "\t\t");
          _builder.append(": //2\'b00");
          _builder.newLineIfNotEmpty();
          _builder.append("\t\t");
          _builder.append("begin");
          _builder.newLine();
          {
            EList<Transition> _transitions = state_1.getTransitions();
            for(final Transition t : _transitions) {
              _builder.append("\t\t");
              _builder.append("\t");
              CharSequence _generateTransition = this.generateTransition(t);
              _builder.append(_generateTransition, "\t\t\t");
              _builder.newLineIfNotEmpty();
            }
          }
          _builder.append("\t\t");
          _builder.append("end");
          _builder.newLine();
        }
      }
      _builder.append("\t\t");
      _builder.append("endcase");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("end");
      _builder.newLine();
      _builder.newLine();
      _builder.append("\t");
      _builder.append("always @(CS or ");
      {
        EList<InControlPort> _activate_3 = fsm.getActivate();
        boolean _hasElements_1 = false;
        for(final Port p_1 : _activate_3) {
          if (!_hasElements_1) {
            _hasElements_1 = true;
          } else {
            _builder.appendImmediate(" or ", "\t");
          }
          String _name_2 = p_1.getName();
          _builder.append(_name_2, "\t");
        }
      }
      _builder.append(")");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.append("begin ");
      _builder.newLine();
      {
        EList<OutControlPort> _flags_2 = fsm.getFlags();
        for(final OutControlPort op_1 : _flags_2) {
          _builder.append("\t\t");
          CharSequence _generateDefaultOutput = this.generateDefaultOutput(op_1);
          _builder.append(_generateDefaultOutput, "\t\t");
          _builder.append("; ");
          _builder.newLineIfNotEmpty();
        }
      }
      _builder.append("\t\t");
      _builder.append("case( CS )");
      _builder.newLine();
      {
        EList<State> _states_2 = fsm.getStates();
        for(final State state_2 : _states_2) {
          {
            int _size = state_2.getActivatedCommands().size();
            boolean _greaterThan = (_size > 0);
            if (_greaterThan) {
              _builder.append("\t\t\t");
              _builder.append("`");
              String _upperCase_3 = state_2.getLabel().toUpperCase();
              _builder.append(_upperCase_3, "\t\t\t");
              _builder.append(": ");
              _builder.newLineIfNotEmpty();
              {
                int _size_1 = state_2.getActivatedCommands().size();
                boolean _greaterThan_1 = (_size_1 > 1);
                if (_greaterThan_1) {
                  _builder.append("\t\t\t");
                  _builder.append("\t");
                  _builder.append("begin");
                  _builder.newLine();
                  {
                    EList<AbstractCommandValue> _activatedCommands = state_2.getActivatedCommands();
                    for(final AbstractCommandValue command : _activatedCommands) {
                      _builder.append("\t\t\t");
                      _builder.append("\t");
                      _builder.append("\t");
                      CharSequence _generateCommand = this.generateCommand(command);
                      _builder.append(_generateCommand, "\t\t\t\t\t");
                      _builder.newLineIfNotEmpty();
                    }
                  }
                  _builder.append("\t\t\t");
                  _builder.append("\t");
                  _builder.append("end");
                  _builder.newLine();
                } else {
                  _builder.append("\t\t\t");
                  _builder.append("\t");
                  _builder.append("// ");
                  int _size_2 = state_2.getActivatedCommands().size();
                  _builder.append(_size_2, "\t\t\t\t");
                  _builder.append(" command");
                  _builder.newLineIfNotEmpty();
                  {
                    EList<AbstractCommandValue> _activatedCommands_1 = state_2.getActivatedCommands();
                    for(final AbstractCommandValue command_1 : _activatedCommands_1) {
                      _builder.append("\t\t\t");
                      _builder.append("\t");
                      CharSequence _generateCommand_1 = this.generateCommand(command_1);
                      _builder.append(_generateCommand_1, "\t\t\t\t");
                      _builder.newLineIfNotEmpty();
                    }
                  }
                }
              }
            }
          }
        }
      }
      _builder.append("\t\t");
      _builder.append("endcase");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("end");
      _builder.newLine();
      _builder.newLine();
      _builder.append("endmodule");
      _builder.newLine();
      _builder.append("\t");
      _builder.newLine();
      _xblockexpression = _builder;
    }
    return _xblockexpression;
  }
  
  public CharSequence generatePortDefinition(final Port p) {
    if (p instanceof InControlPort) {
      return _generatePortDefinition((InControlPort)p);
    } else if (p instanceof OutControlPort) {
      return _generatePortDefinition((OutControlPort)p);
    } else if (p != null) {
      return _generatePortDefinition(p);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(p).toString());
    }
  }
  
  public CharSequence generatePredicate(final AbstractBooleanExpression expr) {
    if (expr instanceof IndexedBooleanFlagTerm) {
      return _generatePredicate((IndexedBooleanFlagTerm)expr);
    } else if (expr instanceof AndExpression) {
      return _generatePredicate((AndExpression)expr);
    } else if (expr instanceof BooleanFlagTerm) {
      return _generatePredicate((BooleanFlagTerm)expr);
    } else if (expr instanceof IntegerFlagTerm) {
      return _generatePredicate((IntegerFlagTerm)expr);
    } else if (expr instanceof OrExpression) {
      return _generatePredicate((OrExpression)expr);
    } else if (expr instanceof BooleanConstant) {
      return _generatePredicate((BooleanConstant)expr);
    } else if (expr instanceof NegateExpression) {
      return _generatePredicate((NegateExpression)expr);
    } else if (expr != null) {
      return _generatePredicate(expr);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(expr).toString());
    }
  }
  
  public CharSequence generateCommand(final AbstractCommandValue command) {
    if (command instanceof BooleanCommandValue) {
      return _generateCommand((BooleanCommandValue)command);
    } else if (command instanceof IntegerCommandValue) {
      return _generateCommand((IntegerCommandValue)command);
    } else if (command != null) {
      return _generateCommand(command);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(command).toString());
    }
  }
}
