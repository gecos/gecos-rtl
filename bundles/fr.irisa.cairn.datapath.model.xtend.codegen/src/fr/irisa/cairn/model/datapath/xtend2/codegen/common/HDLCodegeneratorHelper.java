package fr.irisa.cairn.model.datapath.xtend2.codegen.common;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.model.datapath.AbstractBlock;
import fr.irisa.cairn.model.datapath.ActivableBlock;
import fr.irisa.cairn.model.datapath.DataFlowBlock;
import fr.irisa.cairn.model.datapath.FlagBearerBlock;
import fr.irisa.cairn.model.datapath.Port;

public class HDLCodegeneratorHelper {
	
	public static List<Port> getAllPorts(AbstractBlock dp) {
		List<Port> res = new ArrayList<Port>();
		if(dp instanceof ActivableBlock) res.addAll(((ActivableBlock)dp).getActivate());
		if(dp instanceof DataFlowBlock) {
			DataFlowBlock dp2 = (DataFlowBlock)dp;
			res.addAll(dp2.getIn());
			res.addAll(dp2.getOut());
		}
		if(dp instanceof FlagBearerBlock) res.addAll(((FlagBearerBlock)dp).getFlags());
		return res;
	}
	
	
	public static int wordlength(int val) {
		String bin = Long.toBinaryString(val);
		return  bin.length();
	}
	

	public static String binaryValue(long value, int bitwitdh) {
		String bin = Long.toBinaryString(value);
		int length= bin.length();
		if (length>bitwitdh) {
			return bin.substring(length-bitwitdh, length-1);
		} else {
			StringBuffer res = new StringBuffer(bin);
			for (int j=bitwitdh;j>length;j--) {
				res.insert(0, "0");
			}
			return res.toString();
		}
	}

}
