package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm

import fr.irisa.cairn.model.fsm.FSM 
import fr.irisa.cairn.model.fsm.State
import fr.irisa.cairn.model.fsm.AbstractCommandValue
import fr.irisa.cairn.model.fsm.BooleanCommandValue
import fr.irisa.cairn.model.fsm.IntegerCommandValue
import fr.irisa.cairn.model.fsm.AbstractBooleanExpression
import fr.irisa.cairn.model.fsm.IndexedBooleanFlagTerm
import fr.irisa.cairn.model.fsm.IntegerFlagTerm
import fr.irisa.cairn.model.fsm.OrExpression
import fr.irisa.cairn.model.fsm.AndExpression
import fr.irisa.cairn.model.fsm.NegateExpression
import fr.irisa.cairn.model.fsm.BooleanFlagTerm
import fr.irisa.cairn.model.datapath.xtend2.codegen.common.HDLCodegeneratorHelper
import fr.irisa.cairn.model.fsm.Transition
import fr.irisa.cairn.model.fsm.BooleanValue
import fr.irisa.cairn.model.datapath.port.InputPort
import java.util.List
import fr.irisa.cairn.model.datapath.Port
import fr.irisa.cairn.model.datapath.port.InControlPort
import fr.irisa.cairn.model.datapath.port.OutControlPort
import java.io.PrintStream
import fr.irisa.cairn.model.fsm.BooleanConstant
import fr.irisa.cairn.model.fsm.OutputDefaultValues

class FSMVerilogTemplate {
	
	FSM fsm
	def dispatch generatePortDefinition(Port p) {
		
	}
	def dispatch generatePortDefinition(InControlPort p) {
		if(p.width>1) {
			'''input [�(p.width-1)�:0] �p.name� ;'''
		} else {
			'''input  �p.name� ;'''
		}
		
	}
	def dispatch generatePortDefinition(OutControlPort p) {
		if(p.width>1) {
			'''output [�(p.width-1)�:0] �p.name� ;'''
		} else {
			'''output  �p.name� ;'''
		}
		
	}

	def dispatch generatePredicate(AbstractBooleanExpression expr) {
		'''/// �expr�'''
//		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	def dispatch generatePredicate(IndexedBooleanFlagTerm expr) {
		if(expr.negated) {
			'''�expr.flag.name�[]�expr.offset�]==1'b0'''
		} else {
			'''�expr.flag.name�[�expr.offset�]==1'b1'''
		}
	}

	def dispatch generatePredicate(BooleanConstant c) {
		'''�c.value�'''
	}

	def dispatch generatePredicate(IntegerFlagTerm expr) {
		'''�expr.flag.name�==�HDLCodegeneratorHelper::binaryValue(expr.value,expr.flag.width)�'b'''
	}

	def dispatch generatePredicate(BooleanFlagTerm expr) {
		if(expr.negated) {
			'''�expr.flag.name�==1'b0'''
		} else {
			'''�expr.flag.name�==1'b1'''
		}
	}

	def dispatch generatePredicate(OrExpression expr) {
		'''(�FOR AbstractBooleanExpression t : expr.terms SEPARATOR " || "��generatePredicate(t)��ENDFOR�)'''
	}

	def dispatch generatePredicate(AndExpression expr) {
		'''(�FOR AbstractBooleanExpression t : expr.terms SEPARATOR " && "��generatePredicate(t)��ENDFOR�)'''
	}

	def dispatch generatePredicate(NegateExpression expr) {
		'''~(�generatePredicate(expr.term)�)'''
	}
	

	

	def generateTransition(Transition t) {
		if(t.predicate==null) {
			'''
			NS = `�t.dst.label.toUpperCase�;
			'''
		}  else if(t.predicate instanceof BooleanConstant) {
			if((t.predicate as BooleanConstant).value) {
			'''
			// if (true)
			NS = `�t.dst.label.toUpperCase�;
			'''
			}
		} else {
			'''
			if (�generatePredicate(t.predicate)�)
				NS = `�t.dst.label.toUpperCase�;
			'''
		}
		
	}
	


	def dispatch generateCommand(AbstractCommandValue command) {
		
	}
	
	def dispatch generateCommand(BooleanCommandValue command) {
		var String res = "";
		switch(command.value) {
			case BooleanValue::ONE : res= command.command.name+" = 1'b1"
			case BooleanValue::ZERO: res= command.command.name+" = 1'b0"
			case BooleanValue::DONT_CARE : res= command.command.name+"`DONTCARE"
			default : res = "// DUNNO"
		}
		if(command.predicate==null) {
		'''
				�res�;
		'''
		} else if(command.predicate instanceof BooleanConstant) {
			if((command.predicate as BooleanConstant).value) {
		'''		
				�res�;
		'''
			} 
		} else {
		'''
				if (�generatePredicate(command.predicate)�)
				begin
					�res�;
				end
		'''
		}
	}

	def dispatch generateCommand(IntegerCommandValue command) {
		if(command.predicate==null) {
			'''
			�command.command.name+" = " +command.command.width�'b�HDLCodegeneratorHelper::binaryValue(command.value,command.command.width)+""�;
			'''
		} else if(command.predicate instanceof BooleanConstant) {
			if((command.predicate as BooleanConstant).value) {
		'''		
			�command.command.name+" = " +command.command.width�'b�HDLCodegeneratorHelper::binaryValue(command.value,command.command.width)+""�;
		'''
			} 
		} else {
			'''
			if (�generatePredicate(command.predicate)�)
			begin
				�command.command.name+" = " +command.command.width�'b�HDLCodegeneratorHelper::binaryValue(command.value,command.command.width)+""�;
			end
			'''
		}
	
	}

	def genPortList(List<Port> ports) {
			
		'''
		�FOR Port p: ports SEPARATOR ",\n"��p.name��ENDFOR�
		'''
	}

	def generateCode(FSM fsm, String filename) {
		val PrintStream ps =  new PrintStream(filename);
		this.fsm=fsm
		ps.append(generate(fsm));
		ps.close();
	}
	
	def generateCode(FSM fsm) {
		generateCode(fsm,"fsm"+fsm.name+".v");
	}

	def generateDefaultOutput(OutControlPort op) {
		val first = fsm.defaultValues.findFirst([s|s.port==op]);
		var value =0;
		 
		if(first!=null) {
			value = (first as OutputDefaultValues).value; 
		}
		'''�op.name� = �op.name�'b�HDLCodegeneratorHelper::binaryValue(value,op.width)�'''
		
	}
	def generate(FSM fsm) {
		val int wordlength =  HDLCodegeneratorHelper::wordlength(fsm.states.size)
	'''
		
	// To move in some header files ?
	�FOR State state : fsm.states� 
		`define �state.label.toUpperCase� �wordlength�'b�HDLCodegeneratorHelper::binaryValue(fsm.states.indexOf(state),wordlength)�  
	�ENDFOR�

	module �fsm.name� ( clk, rst, 
	// Input signals
	�genPortList(fsm.activate as List)�, 
	// Output commands
	�genPortList(fsm.flags as List)�
	);
	
	input clk, rst;

	�FOR InControlPort ip : fsm.activate�
		�generatePortDefinition(ip)�
	�ENDFOR� 

	�FOR OutControlPort op : fsm.flags�
		�generatePortDefinition(op)�
	�ENDFOR� 

		reg [`NBITS-1:0] CS;
		wire [`NBITS-1:0] NS;
	
		// async reset (active high tyo be consistent the 
		// remaining of the flow)
		always@(posedge clk or posedge reset)
		begin 
			if (reset) 
				CS <= `�fsm.start.label.toUpperCase�;
			else 
				CS <= NS;
			
		end

���			if( rst )
���				cs <= �fsm.start.label�;
���			else

		always @(CS or �FOR Port p : fsm.activate SEPARATOR " or "��p.name��ENDFOR�)
		begin
			case(CS)
			�FOR State state : fsm.states� 
				`�state.label.toUpperCase�: //2'b00
				begin
					�FOR Transition t : state.transitions�
					�generateTransition(t)�
					�ENDFOR�
				end
			�ENDFOR�
			endcase
		end

		always @(CS or �FOR Port p : fsm.activate SEPARATOR " or "��p.name��ENDFOR�)
		begin 
			�FOR OutControlPort op : fsm.flags� 
			�generateDefaultOutput(op)�; 
			�ENDFOR�
			case( CS )
				�FOR State state : fsm.states�
					�IF state.activatedCommands.size>0�
					`�state.label.toUpperCase�: 
						�IF state.activatedCommands.size>1�
						begin
							�FOR AbstractCommandValue command : state.activatedCommands�
							�generateCommand(command)�
							�ENDFOR�
						end
					�ELSE�
						// �state.activatedCommands.size� command
						�FOR AbstractCommandValue command : state.activatedCommands�
							�generateCommand(command)�
						�ENDFOR�
					�ENDIF�
					�ENDIF�
				�ENDFOR�
			endcase
		end

	endmodule
		
	'''

		
	}
}