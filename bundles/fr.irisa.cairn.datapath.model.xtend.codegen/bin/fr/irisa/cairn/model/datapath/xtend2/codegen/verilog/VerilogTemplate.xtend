package fr.irisa.cairn.model.datapath.xtend2.codegen.verilog

import fr.irisa.cairn.model.datapath.Datapath
import fr.irisa.cairn.model.fsm.FSM
import fr.irisa.cairn.model.datapath.xtend2.codegen.verilog.fsm.FSMVerilogTemplate


class VerilogTemplate {
	Datapath root
	String path 
	
	new(Datapath toplevel , String path) {
		root=toplevel;
		this.path=path;
	}
	
	def compute() {
		val lists = root.components.filter(typeof(FSM));
		val FSMVerilogTemplate gen = new FSMVerilogTemplate();
		for(FSM fsm : lists) {
			gen.generateCode(fsm,path+"/fsm_"+fsm.name+".v") 
		}
	}
	
}